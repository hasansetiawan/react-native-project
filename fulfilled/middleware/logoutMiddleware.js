// @flow
import {AsyncStorage} from 'react-native';

import {USER_ID_KEY, USER_TOKEN_KEY, SELECTED_LANGUAGE_KEY} from '../constants/AsyncStorageKey';

import type {Action} from '../types/Action';
import type {Store} from '../types/Store';

export default function logoutMiddleware(store: Store) {
  let {dispatch} = store;
  return (next: (action: Action) => void) => async (action: Action) => {
    if (action.type !== 'LOGOUT') {
      next(action);
      return;
    }
    try {
      let keys = [USER_ID_KEY, USER_TOKEN_KEY, SELECTED_LANGUAGE_KEY];
      await AsyncStorage.multiRemove(keys);
      dispatch({
        type: 'CHECK_LOGIN_FINISHED',
        isLogin: false,
      });
      dispatch({type: 'RESET'});
    } catch (error) {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: error.message,
      });
      return;
    }
  };
}
