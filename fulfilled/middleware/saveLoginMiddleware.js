// @flow
import {AsyncStorage} from 'react-native';

import {USER_ID_KEY, USER_TOKEN_KEY} from '../constants/AsyncStorageKey';

import type {Action} from '../types/Action';
import type {Store} from '../types/Store';

export default function saveLoginMiddleware(store: Store) {
  let {dispatch} = store;
  return (next: (action: Action) => void) => async (action: Action) => {
    if (action.type !== 'SAVE_LOGIN_REQUESTED') {
      next(action);
      return;
    }
    try {
      await AsyncStorage.setItem(USER_ID_KEY, action.userID);
      await AsyncStorage.setItem(USER_TOKEN_KEY, action.token);
    } catch (error) {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: error.message,
      });
      return;
    }
  };
}
