// @flow
import {BackAndroid} from 'react-native';

import {INITIAL_NAVBAR_INDEX, CHALLENGE_PAGE} from '../constants/NavBarPageName';

import type {Action} from '../types/Action';
import type {Store} from '../types/Store';

export default function navigationMiddleware(store: Store) {
  let {getState, dispatch} = store;
  return (next: (action: Action) => void) => (action: Action) => {
    let {navigation, isDrawerOpen} = getState();

    switch (action.type) {
      case 'APP_STATE_CHANGED': {
        let {newAppState} = action;
        // On Android, when we press back to exit the app goes to background. We
        // should hide the dialog if/when it becomes active again.
        if (navigation.showExitDialog && newAppState === 'active') {
          dispatch({
            type: 'EXIT_DIALOG_HIDE',
          });
        }
        next(action);
        return;
      }
      case 'ROUTE_PUSHED': {
        if (navigation.detail.showDetailView === false) {
          dispatch({
            type: 'ROUTE_REPLACED',
            route: action.route,
          });
        } else {
          next(action);
        }
        break;
      }
      case 'HARDWARE_BACK_PRESSED': {
        if (navigation.detail.showDetailView === true && navigation.detail.routes.length >= 1) {
          dispatch({type: 'ROUTE_POPPED'});
        } else if (navigation.detail.showDetailView === false && navigation.navbar.selectedIndex !== INITIAL_NAVBAR_INDEX) {
          dispatch({
            type: 'NAVBAR_CHANGED',
            route: {
              key: CHALLENGE_PAGE,
            },
            index: INITIAL_NAVBAR_INDEX,
          });
        } else if (isDrawerOpen === true) {
          dispatch({
            type: 'DRAWER_CLOSED',
          });
        } else if (navigation.showExitDialog === true) {
          dispatch({
            type: 'EXIT_DIALOG_HIDE',
          });
        } else if (navigation.detail.showDetailView === false && navigation.navbar.selectedIndex === INITIAL_NAVBAR_INDEX) {
          dispatch({
            type: 'EXIT_DIALOG_OPENED',
          });
        }
        next(action);
        return;
      }
      case 'EXIT_APP': {
        BackAndroid.exitApp(0);
        break;
      }
      default: next(action);
    }
  };
}
