// @flow
import {AsyncStorage} from 'react-native';

import {SELECTED_LANGUAGE_KEY} from '../constants/AsyncStorageKey';

import type {Action} from '../types/Action';
import type {Store} from '../types/Store';

export default function saveSelectedLanguageMiddleware(store: Store) {
  let {dispatch} = store;
  return (next: (action: Action) => void) => async (action: Action) => {
    if (action.type !== 'LANGUAGE_SELECTED') {
      next(action);
      return;
    }
    try {
      await AsyncStorage.setItem(SELECTED_LANGUAGE_KEY, action.language);
    } catch (error) {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: error.message,
      });
      return;
    }
    next(action);
  };
}
