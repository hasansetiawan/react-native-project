// @flow
const {describe, it} = global;

import expect from 'expect';
import navigationMiddleware from '../navigationMiddleware';
import {createDataStore} from '../../createDataStore';

import {CHALLENGE_PAGE, INITIAL_NAVBAR_INDEX} from '../../constants/NavBarPageName';

describe('navigationMiddleware', () => {
  let originalState = createDataStore().getState();
  it('should dispatch route replace when the route pushed for the first time', () => {
    let state = createDataStore().getState();
    let dispatch = expect.createSpy();
    let store = {
      getState: () => (state),
      dispatch,
    };
    let next = expect.createSpy();
    navigationMiddleware(store)(next)({
      type: 'ROUTE_PUSHED',
      route: {
        key: 'newRoute',
      },
    });
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch.calls[0].arguments).toEqual([{
      type: 'ROUTE_REPLACED',
      route: {
        key: 'newRoute',
      },
    }]);
  });

  it('should handle route pop when hardware back press', () => {
    let next = expect.createSpy();
    let state = {
      ...originalState,
      navigation: {
        ...originalState.navigation,
        detail: {
          showDetailView: true,
          routes: [{}, {}],
        },
      },
    };
    let dispatch = expect.createSpy();
    let store = {
      getState: () => (state),
      dispatch,
    };
    navigationMiddleware(store)(next)({
      type: 'HARDWARE_BACK_PRESSED',
    });
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({
      type: 'ROUTE_POPPED',
    });
  });

  it('should change the navbar to the default one when hardware back press', () => {
    let next = expect.createSpy();
    let state = {
      ...originalState,
      navigation: {
        ...originalState.navigation,
        detail: {
          showDetailView: false,
          routes: [{}],
        },
        navbar: {
          ...originalState.navigation.navbar,
          index: 0,
          selectedIndex: 30,
        },
      },
    };
    let dispatch = expect.createSpy();
    let store = {
      getState: () => (state),
      dispatch,
    };
    navigationMiddleware(store)(next)({
      type: 'HARDWARE_BACK_PRESSED',
    });
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({
      type: 'NAVBAR_CHANGED',
      route: {
        key: CHALLENGE_PAGE,
      },
      index: INITIAL_NAVBAR_INDEX,
    });
  });

  it('should show the exit dialog when hardware back press', () => {
    let next = expect.createSpy();
    let state = {
      ...originalState,
      navigation: {
        ...originalState.navigation,
        detail: {
          showDetailView: false,
          routes: [{}],
        },
        navbar: {
          ...originalState.navigation.navbar,
          index: 2,
        },
      },
    };
    let dispatch = expect.createSpy();
    let store = {
      getState: () => (state),
      dispatch,
    };
    navigationMiddleware(store)(next)({
      type: 'HARDWARE_BACK_PRESSED',
    });
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({
      type: 'EXIT_DIALOG_OPENED',
    });
  });
});
