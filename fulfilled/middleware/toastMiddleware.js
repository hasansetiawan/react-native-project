// @flow
import type {Action} from '../types/Action';
import type {Store} from '../types/Store';
import {
  SHORT_DURATION,
} from '../constants/toast';

let timeout = null;

export default function toastMiddleware(store: Store) {
  let {dispatch} = store;
  return (next: (action: Action) => void) => (action: Action) => {
    if (action.type === 'SHOW_TOAST') {
      clearTimeout(timeout);
      timeout = setTimeout(() => {
        dispatch({
          type: 'HIDE_TOAST',
        });
      }, SHORT_DURATION);
    }
    next(action);
  };
}
