// @flow
import {AsyncStorage} from 'react-native';

import {USER_ID_KEY, USER_TOKEN_KEY, SELECTED_LANGUAGE_KEY} from '../constants/AsyncStorageKey';

import type {Action} from '../types/Action';
import type {Store} from '../types/Store';

export default function checkLoginMiddleware(store: Store) {
  let {dispatch} = store;
  return (next: (action: Action) => void) => async (action: Action) => {
    if (action.type !== 'APP_INIT') {
      next(action);
      return;
    }
    let userID;
    let userToken;
    let selectedLanguage;
    try {
      userID = await AsyncStorage.getItem(USER_ID_KEY);
      userToken = await AsyncStorage.getItem(USER_TOKEN_KEY);
      selectedLanguage = await AsyncStorage.getItem(SELECTED_LANGUAGE_KEY);
    } catch (error) {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: error.message,
      });
      return;
    }
    dispatch({
      type: 'CHECK_LOGIN_FINISHED',
      isLogin: (userID && userToken) ? true : false,
    });
    if (userToken != null && userID != null) {
      dispatch({
        type: 'USER_ID_TOKEN_RECEIVED',
        userToken,
        userID,
      });

      dispatch({
        type: 'SYNC_USER_REQUESTED',
        userID,
        token: userToken,
      });

      dispatch({
        type: 'WEEKLY_CHECKIN_REQUESTED',
        token: userToken,
        todaysDate: new Date(),
      });

    }
    if (selectedLanguage != null) {
      dispatch({
        type: 'LANGUAGE_SELECTED',
        language: selectedLanguage,
      });
    }
    return;
  };
}
