// @flow
const {describe, it} = global;

import React from 'react';
import expect from 'expect';
import {shallow} from 'enzyme';
import {Provider} from 'react-redux';
import App from '../App';
import LocalizedApp from '../LocalizedApp';

describe('App', () => {
  it('should render a View', () => {
    let wrapper = shallow(<App />);
    expect(wrapper.length).toBe(1);
    expect(wrapper.type()).toBe(Provider);
    expect(wrapper.find(LocalizedApp).length).toBe(1);
  });
});
