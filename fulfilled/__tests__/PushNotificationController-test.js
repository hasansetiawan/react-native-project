const {describe, it} = global;

import expect from 'expect';
import {mapDispatchToProps} from '../PushNotificationController';

describe('PushNotificationController', () => {
  it('should call onNotificationClicked with the right args', () => {
    let dispatch = expect.createSpy();
    let coach = {id: '123', nickname: 'odi'};
    let {onNotificationClicked} = mapDispatchToProps(dispatch);
    onNotificationClicked('token', 'Notification::ChatPost', 'postID', coach);
    expect(dispatch).toHaveBeenCalled();
    expect(dispatch).toHaveBeenCalledWith({type: 'FETCH_CHAT_LIST_REQUESTED', token: 'token', page: 1, recipient: coach.id, isRefreshingList: true});
  });
});
