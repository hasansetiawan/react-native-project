// @flow
import React, {Component} from 'react';
import {
  KeyboardAvoidingView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
  Platform,
} from 'react-native';
import autobind from 'class-autobind';
import MessageScrollView, {Message} from '../views/MessageScrollView';

type MessageItem = {
  id: string;
  index: number;
  text: string;
};

type State = {
  messages: Array<MessageItem>;
};

type Props = {};

export default class Example extends Component {
  state: State = {
    messages: [],
  };
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    setTimeout(() => {
      this._initMessages(20);
      // setInterval(() => this._addBottom(), 10000);
    }, 1200);
  }

  render() {
    let ViewComponent = (Platform.OS === 'ios') ? KeyboardAvoidingView : View;
    return (
      <ViewComponent style={styles.container} behavior="padding">
        <View style={styles.statusBar} />
        <MessageScrollView style={styles.messageList}>
          {this._renderMessages()}
        </MessageScrollView>
        <View style={styles.controlBar}>
          <Button onPress={this._addTop}>Add Top</Button>
          <Button onPress={this._addBottom}>Add Bottom</Button>
          <TextInput style={styles.textInput} placeholder="Focus to show Keyboard" />
        </View>
      </ViewComponent>
    );
  }

  _renderMessages() {
    let {messages} = this.state;
    return messages.map((message) => (
      <Message style={styles.message} id={message.id} key={message.id}>
        <Text>{message.text}</Text>
      </Message>
    ));
  }

  _addTop() {
    let newMessages = this.state.messages.slice();
    let firstMessage = newMessages.length ? newMessages[0] : null;
    let index = firstMessage ? firstMessage.index - 1 : 0;
    newMessages.unshift(this._generateMessage(index));
    this.setState({messages: newMessages});
  }

  _addBottom() {
    let newMessages = this.state.messages.slice();
    let lastMessage = newMessages.length ? newMessages[newMessages.length - 1] : null;
    let index = lastMessage ? lastMessage.index + 1 : 0;
    newMessages.push(this._generateMessage(index));
    this.setState({messages: newMessages});
  }

  _generateMessage(index) {
    let id = Math.floor(Math.random() * Math.pow(2, 53)).toString(36);
    return {
      id: id,
      index: index,
      text: `This is example message ${index.toString()}`,
    };
  }

  _initMessages(count: number) {
    let newMessages = [];
    for (let i = 0; i < count; i++) {
      newMessages.push(this._generateMessage(i));
    }
    this.setState({messages: newMessages});
  }
}

let styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  statusBar: {
    height: 20,
  },
  messageList: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#000',
    margin: 10,
  },
  message: {
    padding: 20,
  },
  controlBar: {
    flexDirection: 'row',
    marginHorizontal: 10,
    marginBottom: 10,
  },
  textInput: {
    flex: 1,
    borderWidth: 1,
    borderColor: '#999',
    height: 32,
    padding: 5,
    fontSize: 14,
  },
});


function Button(props: Object) {
  let {children, style, ...otherProps} = props;
  return (
    <TouchableOpacity {...otherProps} style={[buttonStyles.root, style]}>
      <View><Text style={buttonStyles.text}>{children}</Text></View>
    </TouchableOpacity>
  );
}

let buttonStyles = StyleSheet.create({
  root: {
    height: 32,
    padding: 5,
    borderWidth: 1,
    borderColor: '#999',
    marginRight: 10,
    justifyContent: 'center',
  },
  text: {
    fontSize: 14,
  },
});
