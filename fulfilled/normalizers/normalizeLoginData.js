// @flow
import {normalize, Schema} from 'normalizr';

export default function normalizeLoginData(userJSON: Object) {
  let userSchema = new Schema('userList');
  let coachSchema = new Schema('coachList');

  userSchema.define({
    coach: coachSchema,
  });

  return normalize(userJSON, userSchema);
}
