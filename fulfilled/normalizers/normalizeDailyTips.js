// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export function normalizeDailyTips(dailyTipsJSON: Object) {
  let dailyTipsSchema = new Schema('dailyTipsList');
  let readingMaterialSchema = new Schema('readingMaterialList');

  dailyTipsSchema.define({
    reading_material: readingMaterialSchema,
  });

  return normalize(dailyTipsJSON, arrayOf(dailyTipsSchema));
}

export function normalizeDailyTip(dailyTipsJSON: Object) {
  let dailyTipsSchema = new Schema('dailyTipsList');
  let readingMaterialSchema = new Schema('readingMaterialList');

  dailyTipsSchema.define({
    reading_material: readingMaterialSchema,
  });

  return normalize(dailyTipsJSON, dailyTipsSchema);
}
