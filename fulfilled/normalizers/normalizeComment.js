// @flow
import {normalize, Schema} from 'normalizr';

export default function normalizeComment(commentJSON: Object) {
  let userSchema = new Schema('userList');
  let commentSchema = new Schema('commentList');

  commentSchema.define({
    user: userSchema,
  });

  return normalize(commentJSON, commentSchema);
}
