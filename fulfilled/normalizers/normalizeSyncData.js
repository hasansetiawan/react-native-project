// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export default function normalizeSyncData(userJSON: Object) {
  let userSchema = new Schema('userList');
  let coachSchema = new Schema('coachList');
  let currentHabitRecordSchema = new Schema('currentHabitRecordList');
  let habitSchema = new Schema('habitList');
  let programSchema = new Schema('programList');
  let programRecordSchema = new Schema('programRecordList');

  currentHabitRecordSchema.define({
    habit: habitSchema,
  });
  programRecordSchema.define({
    program: programSchema,
    current_habit_record: currentHabitRecordSchema,
  });

  userSchema.define({
    coach: coachSchema,
    program_records: arrayOf(programRecordSchema),
  });

  let userProfile = normalize(userJSON, userSchema);
  return userProfile;
}
