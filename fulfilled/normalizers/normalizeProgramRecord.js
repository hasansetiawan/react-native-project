// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export default function normalizeProgramRecord(programRecordJSON: Object) {
  let currentHabitRecordSchema = new Schema('currentHabitRecordList');
  let habitSchema = new Schema('habitList');
  let programSchema = new Schema('programList');
  let programRecordSchema = new Schema('programRecordList');

  currentHabitRecordSchema.define({
    habit: habitSchema,
  });
  programRecordSchema.define({
    program: programSchema,
    current_habit_record: currentHabitRecordSchema,
  });

  let programRecord = normalize(programRecordJSON, arrayOf(programRecordSchema));
  return programRecord;
}
