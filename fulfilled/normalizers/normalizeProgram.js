// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export default function normalizeProgram(programsJSON: Object) {
  let program = new Schema('programList');
  let habit = new Schema('habitList', {idAttribute: 'text'});
  let habitActivity = new Schema('habitActivityList', {idAttribute: 'text'});
  habit.define({
    activity_texts: arrayOf(habitActivity),
  });
  program.define({
    habits: arrayOf(habit),
  });

  return normalize(programsJSON, arrayOf(program));
}
