// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export function normalizePostList(postsJSON: Object) {
  let post = new Schema('postList');
  let user = new Schema('userList');
  let habit = new Schema('habitList');
  let comment = new Schema('commentList');

  comment.define({
    user: user,
  });
  post.define({
    user: user,
    habit: habit,
    likers: arrayOf(user),
    comments: arrayOf(comment),
  });

  return normalize(postsJSON, arrayOf(post));
}

export function normalizePost(postsJSON: Object) {
  let post = new Schema('postList');
  let user = new Schema('userList');
  let habit = new Schema('habitList');
  let comment = new Schema('commentList');

  comment.define({
    user: user,
  });
  post.define({
    user: user,
    habit: habit,
    likers: arrayOf(user),
    comments: arrayOf(comment),
  });

  return normalize(postsJSON, post);
}
