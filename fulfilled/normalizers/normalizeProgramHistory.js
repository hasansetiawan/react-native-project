// @flow
import {normalize, Schema, arrayOf} from 'normalizr';

export default function normalizeProgramHistory(postsJSON: Object) {
  let programHistory = new Schema('programHistoryList');
  let program = new Schema('programList');

  programHistory.define({
    program: program,
  });

  return normalize(postsJSON, arrayOf(programHistory));
}
