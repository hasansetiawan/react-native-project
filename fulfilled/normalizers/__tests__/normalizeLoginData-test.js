let {describe, it} = global;

import expect from 'expect';

import normalizeLoginData from '../normalizeLoginData';

describe('normalizeLoginData', () => {
  it('should normalize user json', () => {
    let userJSON = {
      id: 'userID',
      name: 'audy',
      coach: {
        id: 'coachID',
        name: 'Zhen',
      },
    };
    let normalized = normalizeLoginData(userJSON);
    expect(normalized).toEqual({
      result: 'userID',
      entities: {
        userList: {
          userID: {
            id: 'userID',
            name: 'audy',
            coach: 'coachID',
          },
        },
        coachList: {
          coachID: {
            id: 'coachID',
            name: 'Zhen',
          },
        },
      },
    });
  });
});
