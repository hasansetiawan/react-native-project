let {describe, it} = global;

import expect from 'expect';

import normalizeProgram from '../normalizeProgram';

describe('normalizeProgram', () => {
  it('should normalize user program json', () => {
    let userProgramJSON = [
      {
        title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
        advancement_type: 1,
        category: 1,
        category_description: 'nutrition',
        intro_content: '',
        icon: '&#xE42E;',
        icon_color: 'color_coach_post',
        habit_count: 1,
        total_habit_days: 7,
        lock: false,
        is_foundation: false,
        is_premium: false,
        id: '57033f225a15c98a2b00000c',
        display: 1,
        program_goal: '',
        status: 1,
        total_users: 7447,
        habits: [
          {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            activity_texts: [
              {
                text: '20 menit: Mengatur makan kini begitu mudah!',
                rendered_title: '20 menit: Mengatur makan kini begitu mudah!',
                rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Selamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.</p><p>Mampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal. Hal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.</p><p><strong>Tips pertama</strong>: Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut. Kenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.</p><p>Ketika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi. Kamu pasti bisa!</p><p><a href=\'http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled\'>Baca lebih lanjut mengenai pendekatan Fulfilled</a></p>`,
              },
              {
                text: 'Dari rasa turun ke hati',
                rendered_title: 'Dari rasa turun ke hati',
                rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP2_200.png' width='180px' /></div><p>Gunakan beberapa saat untuk benar-benar merasakan makananmu hari ini.</p><p>Selama 20 menit makan hari ini, gunakan beberapa saat untuk menutup mata. Suap/gigit makanan dan kunyah. Biarkan mata tetap tertutup! Nikmati semua rasa, tekstur, dan aroma dari setiap serpihan makanan dalam mulutmu. Manis? Asin? Lembut? Kenyal? Setelah beberapa saat, kamu bisa membuka matamu… Dengan demikian, kamu bisa lebih menikmati setiap suapan makananmu.</p><p>Jangan lupa untuk mengambil foto makananmu!</p>`,
              },
            ],
          },
        ],
      },
    ];
    let normalized = normalizeProgram(userProgramJSON);
    expect(normalized).toEqual({
      result: ['57033f225a15c98a2b00000c'],
      entities: {
        programList: {
          '57033f225a15c98a2b00000c': {
            title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
            advancement_type: 1,
            category: 1,
            category_description: 'nutrition',
            intro_content: '',
            icon: '&#xE42E;',
            icon_color: 'color_coach_post',
            habit_count: 1,
            total_habit_days: 7,
            lock: false,
            is_foundation: false,
            is_premium: false,
            id: '57033f225a15c98a2b00000c',
            display: 1,
            program_goal: '',
            status: 1,
            total_users: 7447,
            habits: ['Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini'],
          },
        },
        habitList: {
          'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini': {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            activity_texts: ['20 menit: Mengatur makan kini begitu mudah!', 'Dari rasa turun ke hati'],
          },
        },
        habitActivityList: {
          '20 menit: Mengatur makan kini begitu mudah!': {
            text: '20 menit: Mengatur makan kini begitu mudah!',
            rendered_title: '20 menit: Mengatur makan kini begitu mudah!',
            rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Selamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.</p><p>Mampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal. Hal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.</p><p><strong>Tips pertama</strong>: Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut. Kenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.</p><p>Ketika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi. Kamu pasti bisa!</p><p><a href=\'http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled\'>Baca lebih lanjut mengenai pendekatan Fulfilled</a></p>`,
          },
          'Dari rasa turun ke hati': {
            text: 'Dari rasa turun ke hati',
            rendered_title: 'Dari rasa turun ke hati',
            rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP2_200.png' width='180px' /></div><p>Gunakan beberapa saat untuk benar-benar merasakan makananmu hari ini.</p><p>Selama 20 menit makan hari ini, gunakan beberapa saat untuk menutup mata. Suap/gigit makanan dan kunyah. Biarkan mata tetap tertutup! Nikmati semua rasa, tekstur, dan aroma dari setiap serpihan makanan dalam mulutmu. Manis? Asin? Lembut? Kenyal? Setelah beberapa saat, kamu bisa membuka matamu… Dengan demikian, kamu bisa lebih menikmati setiap suapan makananmu.</p><p>Jangan lupa untuk mengambil foto makananmu!</p>`,
          },
        },
      },
    });
  });
});
