let {describe, it} = global;

import expect from 'expect';

import normalizeProgramRecord from '../normalizeProgramRecord';

describe('normalize Program Record', () => {
  it('should normalize Program Record JSON', () => {
    let programRecordJSON = [
      {
        program: {
          title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
          program_goal: '',
          advancement_type_description: 'check_based',
          category_description: 'nutrition',
          icon: '&#xE42E;',
          icon_color: 'color_coach_post',
          is_foundation: false,
          id: '57033f225a15c98a2b00000c',
        },
        current_habit_record: {
          days_completed: 0,
          habit: {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            completion_target: 7,
            id: '570340125a15c968dc000003',
            uncompleted_post_id: null,
            days_completed: null,
          },
          id: '58326b46bb9982993a00001a',
          days_active: 22.9966565662815,
        },
        id: '58326b46bb9982993a000019',
        days_on_program: null,
        days_completed: 0,
        total_program_time: 7,
      },
    ];

    let result = normalizeProgramRecord(programRecordJSON);

    expect(result).toEqual({
      entities: {
        programRecordList: {
          '58326b46bb9982993a000019': {
            id: '58326b46bb9982993a000019',
            days_on_program: null,
            days_completed: 0,
            total_program_time: 7,
            program: '57033f225a15c98a2b00000c',
            current_habit_record: '58326b46bb9982993a00001a',
          },
        },
        programList: {
          '57033f225a15c98a2b00000c': {
            title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
            program_goal: '',
            advancement_type_description: 'check_based',
            category_description: 'nutrition',
            icon: '&#xE42E;',
            icon_color: 'color_coach_post',
            is_foundation: false,
            id: '57033f225a15c98a2b00000c',
          },
        },
        currentHabitRecordList: {
          '58326b46bb9982993a00001a': {
            days_completed: 0,
            habit: '570340125a15c968dc000003',
            id: '58326b46bb9982993a00001a',
            days_active: 22.9966565662815,
          },
        },
        habitList: {
          '570340125a15c968dc000003': {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            completion_target: 7,
            id: '570340125a15c968dc000003',
            uncompleted_post_id: null,
            days_completed: null,
          },
        },
      },
      result: ['58326b46bb9982993a000019'],
    });
  });
});
