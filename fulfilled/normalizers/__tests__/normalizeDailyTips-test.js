let {describe, it} = global;

import expect from 'expect';

import {normalizeDailyTips, normalizeDailyTip} from '../normalizeDailyTips';

describe('normalizeDailyTips', () => {
  it('should normalize daily tips json', () => {
    let dailyTipsJSON = [
      {
        text: '0: You won’t feel deprived, promise!',
        completion_target: 1,
        completion_threshold: null,
        type: 'Lesson',
        text_present_tense: null,
        completed: true,
        reading_material: {
          markdown: '<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nWelcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.\n\nBeing in control of your eating will get you 80% of the way to your weight goal.\nIt’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself. \n\n**Here’s the first tip:** Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. \nWhy? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat. \n\nWhen you’re done, check off the box in the app. We know you can do it! \n\n[Read more about Fulfilled’s approach](http://blog.getfulfilled.co/why-i-started-fulfilled/)',
          title: 'You won’t feel deprived, promise!',
          id: '56d94ead2a48ce1f07000006',
          _id: '56d94ead2a48ce1f07000006',
          html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Welcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.</p><p>Being in control of your eating will get you 80% of the way to your weight goal. It’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself.</p><p><strong>Here’s the first tip:</strong> Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. Why? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat.</p><p>When you’re done, check off the box in the app. We know you can do it!</p><p><a href=\'http://blog.getfulfilled.co/why-i-started-fulfilled/\'>Read more about Fulfilled’s approach</a></p>`,
        },
        questions: [],
        id: '56d94ead2a48ce1f07000007',
      },
    ];
    let normalized = normalizeDailyTips(dailyTipsJSON);
    expect(normalized).toEqual({
      result: ['56d94ead2a48ce1f07000007'],
      entities: {
        dailyTipsList: {
          '56d94ead2a48ce1f07000007': {
            text: '0: You won’t feel deprived, promise!',
            completion_target: 1,
            completion_threshold: null,
            type: 'Lesson',
            text_present_tense: null,
            completed: true,
            reading_material: '56d94ead2a48ce1f07000006',
            questions: [],
            id: '56d94ead2a48ce1f07000007',
          },
        },
        readingMaterialList: {
          '56d94ead2a48ce1f07000006': {
            markdown: '<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nWelcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.\n\nBeing in control of your eating will get you 80% of the way to your weight goal.\nIt’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself. \n\n**Here’s the first tip:** Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. \nWhy? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat. \n\nWhen you’re done, check off the box in the app. We know you can do it! \n\n[Read more about Fulfilled’s approach](http://blog.getfulfilled.co/why-i-started-fulfilled/)',
            title: 'You won’t feel deprived, promise!',
            id: '56d94ead2a48ce1f07000006',
            _id: '56d94ead2a48ce1f07000006',
            html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Welcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.</p><p>Being in control of your eating will get you 80% of the way to your weight goal. It’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself.</p><p><strong>Here’s the first tip:</strong> Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. Why? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat.</p><p>When you’re done, check off the box in the app. We know you can do it!</p><p><a href=\'http://blog.getfulfilled.co/why-i-started-fulfilled/\'>Read more about Fulfilled’s approach</a></p>`,
          },
        },
      },
    });
  });

  it('should normalize one daily tip json', () => {
    let dailyTipsJSON = {
      text: '0: You won’t feel deprived, promise!',
      completion_target: 1,
      completion_threshold: null,
      type: 'Lesson',
      text_present_tense: null,
      completed: true,
      reading_material: {
        markdown: '<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nWelcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.\n\nBeing in control of your eating will get you 80% of the way to your weight goal.\nIt’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself. \n\n**Here’s the first tip:** Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. \nWhy? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat. \n\nWhen you’re done, check off the box in the app. We know you can do it! \n\n[Read more about Fulfilled’s approach](http://blog.getfulfilled.co/why-i-started-fulfilled/)',
        title: 'You won’t feel deprived, promise!',
        id: '56d94ead2a48ce1f07000006',
        _id: '56d94ead2a48ce1f07000006',
        html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Welcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.</p><p>Being in control of your eating will get you 80% of the way to your weight goal. It’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself.</p><p><strong>Here’s the first tip:</strong> Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. Why? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat.</p><p>When you’re done, check off the box in the app. We know you can do it!</p><p><a href=\'http://blog.getfulfilled.co/why-i-started-fulfilled/\'>Read more about Fulfilled’s approach</a></p>`,
      },
      questions: [],
      id: '56d94ead2a48ce1f07000007',
    };
    let normalized = normalizeDailyTip(dailyTipsJSON);
    expect(normalized).toEqual({
      result: '56d94ead2a48ce1f07000007',
      entities: {
        dailyTipsList: {
          '56d94ead2a48ce1f07000007': {
            text: '0: You won’t feel deprived, promise!',
            completion_target: 1,
            completion_threshold: null,
            type: 'Lesson',
            text_present_tense: null,
            completed: true,
            reading_material: '56d94ead2a48ce1f07000006',
            questions: [],
            id: '56d94ead2a48ce1f07000007',
          },
        },
        readingMaterialList: {
          '56d94ead2a48ce1f07000006': {
            markdown: '<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nWelcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.\n\nBeing in control of your eating will get you 80% of the way to your weight goal.\nIt’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself. \n\n**Here’s the first tip:** Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. \nWhy? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat. \n\nWhen you’re done, check off the box in the app. We know you can do it! \n\n[Read more about Fulfilled’s approach](http://blog.getfulfilled.co/why-i-started-fulfilled/)',
            title: 'You won’t feel deprived, promise!',
            id: '56d94ead2a48ce1f07000006',
            _id: '56d94ead2a48ce1f07000006',
            html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Welcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.</p><p>Being in control of your eating will get you 80% of the way to your weight goal. It’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself.</p><p><strong>Here’s the first tip:</strong> Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. Why? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat.</p><p>When you’re done, check off the box in the app. We know you can do it!</p><p><a href=\'http://blog.getfulfilled.co/why-i-started-fulfilled/\'>Read more about Fulfilled’s approach</a></p>`,
          },
        },
      },
    });
  });
});
