let {describe, it} = global;

import expect from 'expect';

import normalizeComment from '../normalizeComment';

describe('normalizeComment', () => {
  it('should normalize comment json', () => {
    let commentJSON = {
      text: 'newComment',
      user: {
        nickname: 'sstur2',
        name: 'Simon',
        id: '57dd3d125def6cbf4d000014',
      },
      id: '581b0290c1b282f687000005',
    };
    let normalized = normalizeComment(commentJSON);
    expect(normalized).toEqual({
      result: '581b0290c1b282f687000005',
      entities: {
        commentList: {
          '581b0290c1b282f687000005': {
            text: 'newComment',
            user: '57dd3d125def6cbf4d000014',
            id: '581b0290c1b282f687000005',
          },
        },
        userList: {
          '57dd3d125def6cbf4d000014': {
            nickname: 'sstur2',
            name: 'Simon',
            id: '57dd3d125def6cbf4d000014',
          },
        },
      },
    });
  });
});
