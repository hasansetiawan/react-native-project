let {describe, it} = global;

import expect from 'expect';

import normalizeSyncData from '../normalizeSyncData';

describe('normalizeSyncData', () => {
  it('should normalize user json', () => {
    let userJSON = {
      id: 'userID',
      name: 'audy',
      coach: {
        id: 'coachID',
        name: 'Zhen',
      },
      program_records: [
        {
          program: {
            title: 'Master of Mindful Eating Challenge',
            programGoal: '',
            id: '56c6062e6e7d6b98f4000006',
          },
          current_habit_record: {
            daysCompleted: 6,
            habit: {
              text: 'Mindful Eating - Ate Slowly Today',
              completion_target: 7,
              type: 'Daily',
              id: '56c607736e7d6b98f4000008',
              uncompletedPostId: null,
              daysCompleted: null,
            },
            id: '57dd3d1804dc60758900016b',
            daysActive: 41.68752670373846,
          },
          id: '57dd3d1804dc60758900016a',
        },
      ],
    };
    let normalized = normalizeSyncData(userJSON);
    expect(normalized).toEqual({
      result: 'userID',
      entities: {
        userList: {
          userID: {
            id: 'userID',
            name: 'audy',
            coach: 'coachID',
            program_records: ['57dd3d1804dc60758900016a'],
          },
        },
        coachList: {
          coachID: {
            id: 'coachID',
            name: 'Zhen',
          },
        },
        programRecordList: {
          '57dd3d1804dc60758900016a': {
            id: '57dd3d1804dc60758900016a',
            program: '56c6062e6e7d6b98f4000006',
            current_habit_record: '57dd3d1804dc60758900016b',
          },
        },
        programList: {
          '56c6062e6e7d6b98f4000006': {
            id: '56c6062e6e7d6b98f4000006',
            title: 'Master of Mindful Eating Challenge',
            programGoal: '',
          },
        },
        currentHabitRecordList: {
          '57dd3d1804dc60758900016b': {
            daysCompleted: 6,
            habit: '56c607736e7d6b98f4000008',
            id: '57dd3d1804dc60758900016b',
            daysActive: 41.68752670373846,
          },
        },
        habitList: {
          '56c607736e7d6b98f4000008': {
            text: 'Mindful Eating - Ate Slowly Today',
            completion_target: 7,
            type: 'Daily',
            id: '56c607736e7d6b98f4000008',
            uncompletedPostId: null,
            daysCompleted: null,
          },
        },
      },
    });
  });
});
