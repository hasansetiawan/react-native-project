let {describe, it} = global;

import expect from 'expect';

import normalizeProgramHistory from '../normalizeProgramHistory';

describe('normalizeProgramHistory', () => {
  it('should normalize user program history json', () => {
    let userProgramHistoryJSON = [
      {
        program: {
          title: 'Tantangan Olahraga Dasar',
          program_goal: 'Tantangan Olahraga Dasar',
          advancement_type_description: 'check_based',
          category_description: 'workout',
          icon: '',
          icon_color: '',
          is_foundation: false,
          id: '57c5585f7485bd364100000c',
        },
        current_habit_record: null,
        id: '57c597ff7485bd16fa00001d',
        days_on_program: null,
        days_completed: 0,
        total_program_time: 14,
      },
      {
        program: {
          title: 'Tantangan Olahraga Advance',
          program_goal: 'Tantangan Olahraga Advance',
          advancement_type_description: 'check_based',
          category_description: 'workout',
          icon: '',
          icon_color: '',
          is_foundation: false,
          id: '57c5585f7485bd364100001c',
        },
        current_habit_record: null,
        id: '57c597ff7485bd16fa00002d',
        days_on_program: null,
        days_completed: 0,
        total_program_time: 14,
      },
    ];
    let normalized = normalizeProgramHistory(userProgramHistoryJSON);
    expect(normalized).toEqual({
      result: ['57c597ff7485bd16fa00001d', '57c597ff7485bd16fa00002d'],
      entities: {
        programHistoryList: {
          '57c597ff7485bd16fa00001d': {
            program: '57c5585f7485bd364100000c',
            current_habit_record: null,
            id: '57c597ff7485bd16fa00001d',
            days_on_program: null,
            days_completed: 0,
            total_program_time: 14,
          },
          '57c597ff7485bd16fa00002d': {
            program: '57c5585f7485bd364100001c',
            current_habit_record: null,
            id: '57c597ff7485bd16fa00002d',
            days_on_program: null,
            days_completed: 0,
            total_program_time: 14,
          },
        },
        programList: {
          '57c5585f7485bd364100000c': {
            title: 'Tantangan Olahraga Dasar',
            program_goal: 'Tantangan Olahraga Dasar',
            advancement_type_description: 'check_based',
            category_description: 'workout',
            icon: '',
            icon_color: '',
            is_foundation: false,
            id: '57c5585f7485bd364100000c',
          },
          '57c5585f7485bd364100001c': {
            title: 'Tantangan Olahraga Advance',
            program_goal: 'Tantangan Olahraga Advance',
            advancement_type_description: 'check_based',
            category_description: 'workout',
            icon: '',
            icon_color: '',
            is_foundation: false,
            id: '57c5585f7485bd364100001c',
          },
        },
      },
    });
  });
});
