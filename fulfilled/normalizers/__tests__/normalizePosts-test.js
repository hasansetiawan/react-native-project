let {describe, it} = global;

import expect from 'expect';
import {normalizePostList} from '../normalizePosts';

import {nestedPostData} from '../../fixtures/Posts';

describe('normalize Posts', () => {
  it('should normalize list of post', () => {
    let postsJSON = nestedPostData;

    expect(normalizePostList(postsJSON)).toEqual({
      result: ['56958b2f87ad72f2e2000009'],
      entities: {
        postList: {
          '56958b2f87ad72f2e2000009': {
            createdAt: '2016-01-12T23:24:31Z',
            imageWidth: 500,
            imageHeight: 667,
            type: 'Meal',
            user: '51f5d3945653eb4f55000003',
            likers: ['56921044a85a722400000009'],
            comments: ['56958bae87ad72f2e200000c', '56960e86c6b0dc57ae000002', '5696fa86c6b0dcd636000009'],
            id: '56958b2f87ad72f2e2000009',
            displayText: 'Breakfast: 2 slices of bread with unsweetened almond butter. Love the nuttiness of almond butter!',
            privacy: 'community',
            posterNickname: 'zhen',
            image: {
              standardResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/low_resolution_file.jpg',
              lowResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/low_resolution_file.jpg',
              thumbnailResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/thumbnail_file.jpg',
            },
            isLiked: false,
            rating: null,
            category: 'none',
            nutritionistNickname: null,
          },
        },
        userList: {
          '51f5d3945653eb4f55000003': {
            nickname: 'zhen',
            firstName: 'Zhen Hoe',
            name: 'Zhen Hoe',
            timeZone: 'Singapore',
            isMentor: false,
            isCoach: true,
            isNutritionist: false,
            isDietitian: false,
            ageInDays: 387,
            id: '51f5d3945653eb4f55000003',
            coachIntroductionId: '527729b37a4c9960c3000028',
          },
          '56921044a85a722400000009': {
            nickname: 'cj',
            firstName: 'Catherine',
            name: 'Catherine',
            timeZone: 'Singapore',
            isMentor: null,
            isCoach: false,
            isNutritionist: false,
            isDietitian: false,
            ageInDays: 192,
            id: '56921044a85a722400000009',
          },
          '568b79fb0283519ecb000005': {
            nickname: 'bcontessa',
            firstName: 'Bcontessa',
            name: 'Bcontessa',
            timeZone: 'Singapore',
            isMentor: null,
            isCoach: false,
            isNutritionist: false,
            isDietitian: false,
            ageInDays: 263,
            id: '568b79fb0283519ecb000005',
          },
        },
        commentList: {
          '56958bae87ad72f2e200000c': {
            text: 'Wow where do you buy almond butter?',
            user: '568b79fb0283519ecb000005',
            id: '56958bae87ad72f2e200000c',
          },
          '56960e86c6b0dc57ae000002': {
            text: 'Where to get such almond butter ? 👍🏻 ',
            user: '56921044a85a722400000009',
            id: '56960e86c6b0dc57ae000002',
          },
          '5696fa86c6b0dcd636000009': {
            text: 'You can find it on redmart. Nuts about butter. Try it. Really good and healthy. ',
            user: '51f5d3945653eb4f55000003',
            id: '5696fa86c6b0dcd636000009',
          },
        },
      },
    });
  });
});
