export default {
  headerTextColor: '#FFFFFF',
  headerFontSize: 14,
  headerFontWeight: 'bold',
  headerAlignment: 'left',
  headerBackgroudColor: '#800053',
  headerPaddingTop: 5,
  headerIconColor: 'white',
};
