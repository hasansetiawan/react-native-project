// @flow

export const COMPANY_NAME = 'GetFulfilled';
export const SUPPORT_EMAIL = 'support@getfulfilled.co';
export const WEBSITE = 'http://id.getfulfilled.co/';
