// @flow

export const CONTAINER_COLOR = '#fafafa';
export const THEME_COLOR = '#800053';
export const DEFAULT_ICON_COLOR = '#616161';
export const GREY_HORIZONTAL_LINE = '#E0E0E0';

export const FITNESS_BUTTON_COLOR = '#009688';
export const FOOD_BUTTON_COLOR = '#FF9800';
export const DRINK_BUTTON_COLOR = '#03A9F4';
export const SCALE_BUTTON_COLOR = '#6A1B9A';
export const THINKING_BUTTON_COLOR = '#FBC02D';

export function getColor(color?: string) {
  switch (color) {
    case 'blue': {
      return '#4A90E2';
    }
    case 'green': {
      return '#61A11A';
    }
    case 'orange': {
      return '#F09459';
    }
    case 'theme': {
      return THEME_COLOR;
    }
    case 'premium': {
      return '#F9D600';
    }
    case 'transparent': {
      return '#00000000';
    }
    default: {
      return '#989898';
    }
  }
}
