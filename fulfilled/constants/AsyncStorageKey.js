// @flow

export const BASE_KEY = '@FulfilledStore';

export const USER_ID_KEY = `${BASE_KEY}:userID`;
export const USER_TOKEN_KEY = `${BASE_KEY}:userToken`;
export const SELECTED_LANGUAGE_KEY = `${BASE_KEY}:selectedLanguage`;
