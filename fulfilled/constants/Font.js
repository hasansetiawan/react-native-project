// @flow

export const FONT_APP_TITLE = 'Lobster 1.4';
export const FONT_REGULAR = 'Montserrat-Regular';
export const FONT_MEDIUM = 'Montserrat-Medium';
export const FONT_SEMIBOLD = 'Montserrat-SemiBold';
export const FONT_BOLD = 'Montserrat-Bold';
export const FONT_LIGHT = 'Montserrat-Light';
export const FONT_THIN = 'Montserrat-Thin';
