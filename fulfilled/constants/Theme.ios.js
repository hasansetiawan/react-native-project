export default {
  headerTextColor: '#FFFFFF',
  headerFontSize: 14,
  headerFontWeight: 'bold',
  headerAlignment: 'center',
  headerBackgroudColor: '#800053',
  headerPaddingTop: 23,
  headerIconColor: 'white',
};
