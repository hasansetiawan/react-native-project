const hitSlop = {
  top: 15,
  bottom: 15,
  left: 15,
  right: 15,
};

export default hitSlop;
