// @flow

import type {NewPostFormData} from '../types/Post';

const DEFAULT_FORM_DATA: {[key: string]: NewPostFormData} = {
  ActivityLog: {
    category: 'ActivityLog',
    content: '',
    hour: '',
    minute: '',
    comment: '',
  },
  Meal: {
    category: 'Meal',
    content: '',
    selectedTime: '',
  },
  Drink: {
    category: 'Drink',
    content: '',
    selectedAmount: '',
  },
  Measurement: {
    category: 'Measurement',
    weight: '',
    waistline: '',
    hip: '',
    comment: '',
  },
  Note: {
    category: 'Note',
    content: '',
  },
};

export default DEFAULT_FORM_DATA;
