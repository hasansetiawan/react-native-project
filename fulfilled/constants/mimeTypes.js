// @flow

const MIME_TYPES = {
  jpeg: 'image/jpeg',
  jpg: 'image/jpeg',
  png: 'image/png',
  mp4: 'video/mpeg',
  '3gp': 'video/3gpp',
};

export default MIME_TYPES;
