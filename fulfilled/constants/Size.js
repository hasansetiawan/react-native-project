// @flow

export const ELEVATION = 2;
export const SMALLEST_SCREEN = 320;
export const BIGGEST_SCREEN = 480;
export const SMALLEST_FONT = 12;
export const BIGGEST_FONT = 18;
