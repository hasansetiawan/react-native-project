// @flow

export const COMMUNITY_PAGE = 'pageNames/community';
export const CHAT_PAGE = 'pageNames/chat';
export const CHALLENGE_PAGE = 'pageNames/challenge';
export const PROFILE_PAGE = 'pageNames/profile';
export const NOTIFICATION_PAGE = 'pageNames/notifications';

export const INITIAL_NAVBAR_INDEX = 2;
export const CHAT_NAVBAR_INDEX = 1;
