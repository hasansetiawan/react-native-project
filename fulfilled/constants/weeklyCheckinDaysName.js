// @flow

import {weekday} from '../helpers/getDaysName';

const DEFAULT_WEEKLY_CHECKIN_DAY = [weekday[0], weekday[6]]; // Sunday and Saturday

export default DEFAULT_WEEKLY_CHECKIN_DAY;
