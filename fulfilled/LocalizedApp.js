// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  StatusBar,
  NavigationExperimental,
  TouchableOpacity,
} from 'react-native';
import {
  GoogleAnalyticsTracker,
} from 'react-native-google-analytics-bridge';
import {
  Drawer,
  ExitDialog,
  Toast,
  PopupModal,
  LocalizedText,
} from './core-ui';

import {connect} from 'react-redux';

import NavBarContainer from './containers/NavBarContainer';
import LanguageProvider from './localization/LanguageProvider';
import languages from './localization/languages';
import Header from './core-ui/Header';
import DetailView from './views/DetailView';
import CommunityView from './views/CommunityView';
import ProfileView from './views/ProfileView';
import ChatView from './views/chat/ChatView';
import NotificationView from './views/NotificationView';
import ChallengeView from './views/ChallengeView';
import LoginView from './views/LoginView';
import OrientationContentView from './views/OrientationContentView';
import SplashScreenView from './views/SplashScreenView';
import ProgramView from './views/ProgramView';
import WeeklyCheckinFormView from './views/WeeklyCheckinFormView';

import PushNotificationController from './PushNotificationController';

import ImageViewModal from './views/ImageViewModal';

import styles from './LocalizedApp-style';

import {
  NOTIFICATION_PAGE,
  CHALLENGE_PAGE,
  COMMUNITY_PAGE,
  PROFILE_PAGE,
  CHAT_PAGE,
} from './constants/NavBarPageName';

import type {LoginStatus} from './types/LoginStatus';
import type {RootState} from './types/RootState';
import type {Dispatch} from './types/Dispatch';
import type {PopupState} from './types/Popup';
import type {User} from './types/User';
import type {Coach} from './types/Coach';
import type {KeyboardState} from './types/KeyboardState';

const {
 CardStack: NavigationCardStack,
} = NavigationExperimental;

import {trackerID} from './constants/googleAnalyticsTrackerID';
const tracker = new GoogleAnalyticsTracker(trackerID);
const MINIMUM_USER_DAYS_OLD = 7;

class LocalizedApp extends Component {
  props: {
    showWeeklyCheckinDialog: boolean;
    selectedLanguage: string;
    navbarState: Object;
    detailNavState: Object;
    isDrawerOpen: boolean;
    loginStatus: LoginStatus;
    showOrientation: boolean;
    popupModal: PopupState;
    user: User;
    keyboard: KeyboardState;
    coach: Coach;
    isChatBeingRefreshed: boolean;
    setNavigator: (navigator: Object) => void;
    openDrawer: () => void;
    closeDrawer: () => void;
    popDetailNav: () => void;
    onPopupModalClose: () => void;
    onProgramListMenuPressed: (token: string) => void;
    onWeeklyCheckinPress: () => void;
    onRefreshChatPagePress: (token: string, page: number, coach: Coach) => void;
  };

  constructor() {
    super(...arguments);
    autobind(this);
    tracker.trackScreenView('Main');
  }

  render() {
    let {
      showWeeklyCheckinDialog,
      onWeeklyCheckinPress,
      loginStatus,
      isDrawerOpen,
      closeDrawer,
      openDrawer,
      selectedLanguage,
      navbarState,
      detailNavState,
      popDetailNav,
      popupModal,
      onPopupModalClose,
      user,
      onProgramListMenuPressed,
      keyboard,
      coach,
      onRefreshChatPagePress,
      isChatBeingRefreshed,
    } = this.props;
    let pageName = navbarState.routes[0].key;
    let content;
    let {isChecking, isLogin, isSyncUserFinish} = loginStatus;
    if (isChecking === false && isLogin === false) {
      content = (
        <View style={styles.container}>
          <StatusBar
            backgroundColor="#5A073C"
            barStyle="light-content"
          />
          <LoginView />
          <DetailView withHeader={false} detailNavState={detailNavState} onClose={popDetailNav} />
          <ExitDialog />
        </View>
      );
    } else if (isChecking === false && isSyncUserFinish === true && isLogin === true) {
      let {showOrientation} = this.props;
      if (showOrientation === true) {
        content = (
          <OrientationContentView />
        );
      } else {
        let rightButtonIcon = '';
        let rightButtonAction = () => {};
        if (navbarState.routes[0].key === CHALLENGE_PAGE) {
          rightButtonIcon = 'web';
          rightButtonAction = () => onProgramListMenuPressed(user.token);
        } else if (navbarState.routes[0].key === CHAT_PAGE) {
          rightButtonIcon = 'refresh';
          if (!isChatBeingRefreshed) {
            rightButtonAction = () => onRefreshChatPagePress(user.token, 1, coach);
          } else {
            rightButtonAction = () => {};
          }
        }

        content = (
          <View style={styles.container}>
            <Drawer isDrawerOpen={isDrawerOpen} onDrawerClose={closeDrawer}>
              <StatusBar
                backgroundColor="#5A073C"
                barStyle="light-content"
              />
              <Header
                pageName={pageName}
                leftButtonIcon="menu"
                leftButtonAction={() => openDrawer()}
                rightButtonIcon={rightButtonIcon}
                rightButtonAction={rightButtonAction}
              />
              {
                (showWeeklyCheckinDialog && pageName === CHALLENGE_PAGE && user.ageInDays > MINIMUM_USER_DAYS_OLD) ?
                  <TouchableOpacity onPress={onWeeklyCheckinPress}>
                    <View style={styles.weeklyCheckinDialog}>
                      <View style={styles.weeklyCheckinDialogTextContainer}>
                        <LocalizedText
                          style={styles.weeklyCheckinDialogText}
                          from="weeklyCheckinDialogBox"
                        />
                      </View>
                    </View>
                  </TouchableOpacity> : null
              }

              <NavigationCardStack
                direction="horizontal"
                navigationState={navbarState}
                renderScene={this._renderScene}
                enableGestures={true}
                renderHeader={null}
                style={{}}
              />
              {(keyboard.isKeyboardShown === true && pageName === COMMUNITY_PAGE) ? null : <NavBarContainer />}
            </Drawer>

            <DetailView detailNavState={detailNavState} onClose={popDetailNav} />
            <ExitDialog />
          </View>
        );
      }
    } else {
      content = (
        <View style={styles.container}>
          <SplashScreenView />
        </View>
      );
    }

    return (
      <LanguageProvider selectedLanguage={selectedLanguage} languages={languages}>
        <View style={{flex: 1}}>
          <PushNotificationController />
          {content}
          {(popupModal.isShowing === true) ? <PopupModal localizedText={popupModal.content.slice(-1)} onClose={onPopupModalClose} visible={popupModal.isShowing} /> : null}
          <ImageViewModal />
          <Toast />
        </View>
      </LanguageProvider>
    );
  }

  _renderScene({scene}) {
    switch (scene.route.key) {
      case NOTIFICATION_PAGE: {
        return <NotificationView />;
      }
      case CHAT_PAGE: {
        return <ChatView />;
      }
      case CHALLENGE_PAGE: {
        return <ChallengeView />;
      }
      case COMMUNITY_PAGE: {
        return <CommunityView />;
      }
      case PROFILE_PAGE: {
        return <ProfileView />;
      }
    }
  }
}

let mapStateToProps = (state: RootState) => ({
  user: state.user,
  coach: state.coach,
  selectedLanguage: state.selectedLanguage,
  navbarState: state.navigation.navbar,
  detailNavState: state.navigation.detail,
  isDrawerOpen: state.isDrawerOpen,
  loginStatus: state.loginStatus,
  showOrientation: (state.coach.orientationContentIDs != null) ? true : false,
  popupModal: state.popupModal,
  keyboard: state.keyboard,
  showWeeklyCheckinDialog: state.weeklyCheckin.showWeeklyCheckinDialog,
  isChatBeingRefreshed: state.loadingPage.chatPage,
});

let mapDispatchToProps = (dispatch: Dispatch) => ({
  onPopupModalClose: () => dispatch({type: 'HIDE_POPUP'}),
  openDrawer: () => dispatch({type: 'DRAWER_OPENED'}),
  closeDrawer: () => dispatch({type: 'DRAWER_CLOSED'}),
  popDetailNav: () => dispatch({type: 'ROUTE_POPPED'}),
  onProgramListMenuPressed: (token: string) => {
    dispatch({
      type: 'LOADING_STARTED',
      loadingPages: 'programListPage',
    });
    dispatch({
      type: 'FETCH_PROGRAM_LIST_REQUESTED',
      token,
    });
    dispatch({
      type: 'ROUTE_PUSHED',
      route: {
        key: 'programList',
        Component: ProgramView,
        title: 'programList',
      },
    });
  },
  onWeeklyCheckinPress: () => {
    dispatch({
      type: 'ROUTE_PUSHED',
      route: {
        key: 'weeklyCheckin',
        Component: WeeklyCheckinFormView,
        title: 'weeklyReport',
      },
    });
  },
  onRefreshChatPagePress: (token: string, page: number, coach: Coach) => {
    dispatch({
      type: 'LOADING_STARTED',
      loadingPages: 'chatPage',
    });
    dispatch({
      type: 'FETCH_CHAT_LIST_REQUESTED',
      token,
      page,
      recipient: coach.id,
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(LocalizedApp);
