// @flow
import type {Lesson} from './Lesson';

export type ChallengePack = {
  title: string;
  lessons: Array<Lesson>;
  price: number;
};
