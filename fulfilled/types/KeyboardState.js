// @flow

export type KeyboardState = {
  isKeyboardShown: boolean;
  keyboardSpace: number;
};
