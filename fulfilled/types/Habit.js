//@flow

import type {ProgramCategoryDescription} from './Program';

export type Habit = {
  id: string;
  text: string;
  completionTarget: number;
  completionThreshold: number;
  type: string;
  textPresentTense?: string;
  completed: boolean;
  uncompletedPostID?: string;
  daysCompleted: number;
  daysOnHabit: number;
  habitCompletionMessageID: string;
  reminderText: string;
  programCategory: ProgramCategoryDescription;
  completedToday: boolean | string;
  category: string;
  contentCheckoffID: string;
  activityParentRecordID: string;
  activityTodoID: string;
  activityTodoContentID: string;
  activityTodoText: string;
  userHabitRecordID: string;
};

export type HabitReducer = {
  currentHabit: Habit;
  progressPercentage: number;
  isCooldown: boolean;
  showCompleteContent: boolean;
};
