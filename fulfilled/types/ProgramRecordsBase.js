//@flow
export type ProgramRecordsBase = {
  text: string;
  days_completed: number;
};
