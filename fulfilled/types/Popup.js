// @flow

export type PopupState = {
  isShowing: boolean;
  content: Array<string>;
};
