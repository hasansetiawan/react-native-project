// @flow
export type PushNotification = {
  deviceToken: string;
  parseInstallationID: string;
};
