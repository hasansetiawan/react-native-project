// @flow

export type Notification = {
  id: string;
  createdAt: string;
  isNew: boolean;
  type: NotificationType;
  text: string;
  postID: string;
  questionID?: string;
  contentID?: string;
  userHabitRecordID?: string;
  programRecordID?: string;
};
export type NotificationType = 'coach_post' | 'like_post' | 'comment_post';

export type NotificationReducer = {
  notificationList: Map<string, Notification>;
  notificationDetailID: ?string;
  page: number;
  isLoadingMore: boolean;
};
