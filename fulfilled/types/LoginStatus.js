// @flow
export type LoginStatus = {
  isChecking: boolean;
  isLogin: boolean;
  isSyncUserFinish: boolean;
};
