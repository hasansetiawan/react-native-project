// @flow

export type Drawer = {
  openDrawer: () => void;
  closeDrawer: () => void;
};
