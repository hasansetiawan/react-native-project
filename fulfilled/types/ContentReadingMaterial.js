// @flow

export type ContentReadingMaterial = {
  id: string;
  renderedBody: string;
  renderedTitle: string;
};
