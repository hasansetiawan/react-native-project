// @flow

export type HeaderProps = {
  pageName: string;
  rightButtonIcon?: string;
  rightButtonAction?: Function;
  leftButtonIcon?: string;
  leftButtonAction?: Function;
};

export type Route = {
  key: string;
  title?: string;
};

export type Navigation = {
  navbar: {
    index: number;
    key: string;
    selectedIndex: number;
    routes: Array<Route>;
  };
  detail: {
    showDetailView: boolean;
    index: number;
    key: string;
    currentDetailHeader: ?string;
    routes: Array<Route>;
  };
  showExitDialog: boolean;
};
