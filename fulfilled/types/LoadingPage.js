// @flow

export type LoadingPage = {
  challengePage: boolean;
  notificationPage: boolean;
  chatPage: boolean;
  communityFeedPage: boolean;
  postHistoryPage: boolean;
  programHistoryPage: boolean;
  programListPage: boolean;
  createPostPage: boolean;
  loginPage: boolean;
  signUpPage: boolean;
  resetPasswordPage: boolean;
  communityFeedRefresh: boolean;
  notificationDetailPage: boolean;
  completeHabitActivity: boolean;
  coachProfile: boolean;
  changeProfilePicturePage: boolean;
  userPostsHistoryRefresh: boolean;
  selectedProgramPage: boolean;
  registerSubscription: boolean;
  weeklyCheckinForm: boolean;
  notificationRefresh: boolean;
};

export type LoadingPageKey = 'challengePage' | 'notificationPage' |
                             'chatPage' | 'communityFeedPage' |
                             'postHistoryPage' | 'programHistoryPage' |
                             'programListPage' | 'createPostPage' |
                             'loginPage' | 'signUpPage' | 'resetPasswordPage' |
                             'notificationDetailPage' | 'communityFeedRefresh' |
                             'completeHabitActivity' | 'coachProfile' | 'changeProfilePicturePage' |
                             'userPostsHistoryRefresh' | 'selectedProgramPage' | 'registerSubscription' |
                             'weeklyCheckinForm' | 'notificationRefresh';
