//@flow

// also called Lessons

import type {Question} from './Question';

export type HabitActivity = {
  id: string;
  text: string;
  completionTarget: number;
  completionThreshold: number;
  type: string;
  completed: boolean;
  readingMaterial: string;
  category: string;
  completedToday: boolean;
  displayType: string;
  questions: Array<Question>;
  daysCompleted: 1 | 0 | null;
};

export type HabitActivityAction = {
  type: 'HABIT_ACTIVITIES_RECEIVED';
};

export type ReadingMaterial = {
  markdown: string;
  id: string;
  html: string;
  title: string;
};

export type HabitActivityReducer = {
  habitActivityList: Map<string, HabitActivity>; // also called dailyTips
  totalActivityCompleted: number;
  currentActivityID: string;
  isCooldown: boolean;
  progressPercentage: number;
};
