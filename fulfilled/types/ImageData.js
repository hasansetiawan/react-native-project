// @flow
export type ImageData = {
  uri: string;
  name: string;
  type: string;
};
