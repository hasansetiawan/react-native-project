//@flow

export type User = {
  id: string;
  name: string; // I'm not really sure about this
  nickname: string;
  firstName: string;
  lastName?: string;
  profilePicture?: ProfilePicture;
  paymentInformation?: any;
  phone?: string;
  email: string;
  lastActive: string;
  ageInDays: number;
  hasCoach: boolean;
  hasPaid: boolean;
  orientationCompleted: boolean; //on app start, app will check if this flag. If true, show orientation flow, else go straight to dashboard
  timeZone: string;
  stripeLastCharged?: any;
  parseInstallationId?: string; //For notifications
  isMentor: boolean;
  isCoach: boolean;
  isNutritionist: boolean;
  isDietitian: boolean;
  categoryDescription: string;
  platform: string;
  language: string;
  userMeasurement?: any;
  newNotificationsCount: number;
  newFeedCount: number;
  chatsCount: number;
  pastWeekMealsCount: number;
  pastWeekActivitiesCount: number;
  token: string;
  standaloneHabitRecords?: any;
  programRecords: Array<any>;
  coach: string;
  habitCheckoffCount: number;
  longestStreak: number;
  hasCompletedFoundation?: any;
  completedProgramIds: Array<string>;
  isPremiumMember: boolean;
  subscribed: boolean;
  premiumMembershipExpiry: string;
  wordCountLeft: number;
};

export type ProfilePicture = {
  standardResolutionUrl: string;
  lowResolutionUrl: string;
  thumbnailResolutionUrl?: string;
};

export type CommunityMember = {
  id: string;
  nickname: string;
  isMentor: boolean;
  isCoach: boolean;
  isNutritionist: boolean;
  isDietitian: boolean;
  profilePicture?: ProfilePicture;
};
