//@flow
export type UserHabitRecordsBase = {
  text: string;
  days_completed: number;
};
