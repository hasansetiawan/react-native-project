//@flow

export type ToastType = {
  toastMessage: ?string;
};
