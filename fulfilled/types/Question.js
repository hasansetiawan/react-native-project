//@flow
export type Question = {
  id: string;
  text: string;
  slug: string;
  answerPublic: string;
  placeholder: string;
  createdAt: string;
  responseFormat: any;
  html: string;
};
