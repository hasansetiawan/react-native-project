// @flow
import type {Privacy} from './Privacy';

export type PostType = 'Drink' | 'Meal' | 'ActivityLog' | 'Measurement' | 'Note';

export type PostImage = {
  standardResolution: string;
  lowResolution: string;
  thumbnailResolution: string;
};

export type Post = {
  id: string;
  createdAt: string;
  imageWidth: null;
  imageHeight: null;
  type: PostType;
  user: string;
  habitId?: string;
  likers: Array<string>; // id of Likers
  comments: Array<string>;
  displayText: string;
  privacy: Privacy;
  image: ?PostImage;
  posterNickname: string;
  isLiked: boolean;
};

export type ActivityFormData = {
  category: 'ActivityLog';
  content: string;
  hour: string;
  minute: string;
  comment: string;
};

export type MealFormData = {
  category: 'Meal';
  content: string;
  selectedTime: string;
};

export type DrinkFormData = {
  category: 'Drink';
  content: string;
  selectedAmount: string;
};

export type MeasurementFormData = {
  category: 'Measurement';
  weight: string;
  waistline: string;
  hip: string;
  comment: string;
};

export type NoteFormData = {
  category: 'Note';
  content: string;
};

export type NewPostFormData = ActivityFormData | MealFormData | DrinkFormData | MeasurementFormData | NoteFormData;
