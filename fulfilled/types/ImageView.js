// @flow
export type ImageViewState = {
  uri: string;
  showModal: boolean;
};
