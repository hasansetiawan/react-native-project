// @flow
export type PaymentDetail = {
  productId: string;
  orderId: string;
  purchaseToken: string;
  purchaseTime: string;
  purchaseState: string;
  receiptSignature: string;
  receiptData: string;
  developerPayload: string;
};

export type PurchaseState = 'PurchasedSuccessfully' | 'Canceled' | 'Refunded' | 'SubscriptionExpired';
