// @flow
export type Comment = {
  id: string;
  text: string;
  user: string;
};
