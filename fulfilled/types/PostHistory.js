// @flow
import type {Post} from './Post';
export type PostHistoryReducer = {
  postList: Map<string, Post>;
  userID: string;
  page: number;
  lastPostID: string;
  isLoadingMore: boolean;
};
