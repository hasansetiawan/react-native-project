// @flow

export type WeeklyCheckinStatus = {
  success: boolean;
  meals: number;
  activities: number;
  habits: number;
  lessons: number;
  total: number;
  latestCheckinDate: string;
  latestCheckinID: string;
};

export type WeeklyCheckinState = {
  showWeeklyCheckinDialog: boolean;
  weeklyCheckinStatus?: WeeklyCheckinStatus;
};
