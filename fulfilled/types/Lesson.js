// @flow
export type Lesson = {
  number: number;
  title: string;
  description: string; //etc...
};
