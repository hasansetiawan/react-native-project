// @flow

export type DateTimeFormat = 'DATE_TIME_SHORT' | 'DATE_TIME_LONG' | 'DATE_SHORT' | 'DATE_LONG';
