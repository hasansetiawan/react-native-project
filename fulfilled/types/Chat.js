// @flow

import type {Privacy} from './Privacy';

export type Chat = {
  id: string;
  createdAt: string;
  text: string;
  displayText: string;
  privacy: Privacy;
  posterNickname: string;
  recipientNickname: string;
  userIsPoster: boolean;
  createdAtInSeconds: number;
};

export type ChatReducer = {
  chatList: Map<string, Chat>;
  page: number;
  inputChat: string;
  showWordCountLeftStatus: boolean;
};
