// @flow
import type {Route} from './Navigator';
import type {Drawer} from './Drawer';
import type {Comment} from './Comment';
import type {NewPostFormData, PostType} from './Post';
import type {Privacy} from './Privacy';
import type {User, CommunityMember} from './User';
import type {Coach} from './Coach';
import type {Chat} from './Chat';
import type {Program} from './Program';
import type {Habit} from './Habit';
import type {HabitActivity, ReadingMaterial} from './HabitActivity';
import type {ImageData} from './ImageData';
import type {Post} from './Post';
import type {ContentReadingMaterial} from './ContentReadingMaterial';
import type {LoadingPageKey} from './LoadingPage';
import type {WeeklyCheckinStatus} from './WeeklyCheckin';

export type Action = {
  type: 'LANGUAGE_SELECTED';
  language: string;
} | {
  type: 'USER_RECEIVED';
  user: User;
} | {
  type: 'CHAT_RECEIVED';
} | {
  type: 'NEW_CHAT_ADDED';
} | {
  type: 'NEW_CHAT_RECEIVED';
} | {
  type: 'COMMENT_RECEIVED';
  comments: Array<Comment>;
} | {
  type: 'COMMENT_ADDED';
  postID: string;
  comment: Comment;
} | {
  type: 'DRAWER_RECEIVED';
  drawer: ?Drawer;
} | {
  type: 'DRAWER_OPENED';
} | {
  type: 'DRAWER_CLOSED';
} | {
  type: 'ROUTE_PUSHED';
  route: Route;
} | {
  type: 'ROUTE_POPPED';
} | {
  type: 'NAVBAR_CHANGED';
  route: Route;
  index: number;
} | {
  type: 'PROGRAM_RECEIVED';
} | {
  type: 'PROGRAM_ACTIVATED';
} | {
  type: 'PROGRAM_DEACTIVATED';
} | {
  type: 'CHAT_ADDED';
  id: string;
  createdAt: string;
  senderNickname: string;
  recipientNickname: string;
} | {
  type: 'LIKE_ADDED';
  postID: string;
  userID: string;
  token: string;
} | {
  type: 'LIKE_REMOVED';
  postID: string;
  userID: string;
} | {
  type: 'POST_CREATED';
  createdAt: string;
  id: string;
  privacy: Privacy;
  user: User;
  imageData: ?ImageData;
} | {
  type: 'NOTIFICATION_COUNT_REMOVED';
  pageName: string;
} | {
  type: 'USER_SUBSCRIBED';
} | {
  type: 'USER_UNSUBSCRIBED';
} | {
  type: 'FORMDATA_CHANGED';
  newFormData: NewPostFormData;
} | {
  type: 'FORM_OPENED';
  category: PostType;
} | {
  type: 'EXIT_DIALOG_HIDE';
} | {
  type: 'EXIT_DIALOG_OPENED';
} | {
  type: 'EXIT_APP';
} | {
  type: 'DETAIL_ROUTE_RESET';
} | {
  type: 'HARDWARE_BACK_PRESSED';
} | {
  type: 'APP_STATE_CHANGED';
  newAppState: string;
} | {
  type: 'ROUTE_REPLACED';
  route: Route;
} | {
  type: 'COACH_RECEIVED';
  coach: Coach;
} | {
  type: 'CHAT_LIST_RECEIVED';
  chatList: {[key: string]: Chat};
  page?: number;
} | {
  type: 'USER_PROGRAM_HISTORY_RECEIVED';
  programHistoryList: Array<Program>;
} | {
  type: 'FETCH_POSTS_REQUESTED';
  token: string;
  page: number;
  lastPostID?: string;
  postType?: string;
} | {
  type: 'COMMUNITY_MEMBER_RECEIVED';
  memberList: Array<CommunityMember>;
} | {
  type: 'CURRENT_HABIT_RECEIVED';
  habit: Habit;
} | {
  type: 'USER_POST_HISTORY_RECEIVED';
  postList: Array<Post>;
  userID: string;
  page: number;
  lastPostID?: string;
} | {
  type: 'USER_POST_HISTORY_REQUESTED';
  token: string;
  page: number;
  userID: string;
  lastPostID?: string;
  postType?: string;
} | {
  type: 'USER_POST_HISTORY_FETCH_START';
  isLoadingMore: boolean;
} | {
  type: 'USER_POST_HISTORY_FETCH_END';
  isLoadingMore: boolean;
} | {
  type: 'ADD_COMMENT_SUCCESS';
  optimisticID: string;
  comment: Comment;
} | {
  type: 'HABIT_ACTIVITY_LIST_RECEIVED';
  habitActivityList: Array<HabitActivity>;
} | {
  type: 'READING_MATERIAL_LIST_RECEIVED';
  readingMaterialList: Array<ReadingMaterial>;
} | {
  type: 'SEND_POST_SUCCESS';
  optimisticID: string;
  post: Post;
} | {
  type: 'CHECK_LOGIN_FINISHED';
  isLogin: boolean;
} | {
  type: 'USER_ID_TOKEN_RECEIVED';
  userToken: string;
  userID: string;
} | {
  type: 'STATUS_COOLDOWN_RECEIVED';
  isCoolDown: boolean;
} | {
  type: 'SYNC_USER_REQUESTED';
  userID: string;
  token: string;
} | {
  type: 'SHOW_TOAST';
  toastMessage: string;
} | {
  type: 'HIDE_TOAST';
} | {
  type: 'LOGIN';
  nickname: string;
  password: string;
} | {
  type: 'ORIENTATION_CONTENT_RECEIVED';
  orientationContent: ContentReadingMaterial;
} | {
  type: 'USER_PROGRAM_HISTORY_REQUESTED';
  token: string;
} | {
  type: 'LOADING_STARTED';
  loadingPages: Array<LoadingPageKey> | LoadingPageKey;
} | {
  type: 'COMPLETE_ORIENTATION_REQUESTED';
  token: string;
  userID: string;
} | {
  type: 'COMPLETE_ORIENTATION_SUCCESS';
} | {
  type: 'CHANGE_PROFILE_PICTURE_REQUESTED';
  id: string;
  imageData: ImageData;
  token: string;
  language: string;
} | {
  type: 'DEVICE_TOKEN_RECEIVED';
  deviceToken: string;
} | {
  type: 'PARSE_INSTALLATION_ID_RECEIVED';
  parseInstallationID: string;
} | {
  type: 'POSTS_RECEIVED';
  postList: Array<Post>;
  page?: number;
} | {
  type: 'INTERNET_STATUS_CHANGED';
  isConnected: boolean;
} | {
  type: 'SUBMIT_ORIENTATION_ANSWER_REQUESTED';
  token: string;
  userID: string;
  formData: string;
} | {
  type: 'FETCH_MORE_USER_POST_HISTORY_START';
} | {
  type: 'FETCH_MORE_USER_POST_HISTORY_END';
} | {
  type: 'FETCH_NOTIFICATION_DETAIL_REQUESTED';
  token: string;
  postID: string;
} | {
  type: 'NOTIF_DETAIL_CHANGED';
  notificationDetailID: string;
} | {
  type: 'FETCH_CHAT_LIST_REQUESTED';
  token: string;
  page: number;
  recipient: string;
} | {
  type: 'SHOW_TOAST';
  toastMessage: string;
} | {
  type: 'INPUT_CHAT_CHANGED';
  input: string;
} | {
  type: 'CLOSE_IMAGE_VIEW_MODAL';
} | {
  type: 'IMAGE_URI_RECEIVED';
  uri: string;
} | {
  type: 'FETCH_NOTIF_LIST_REQUESTED';
  token: string;
  page: number;
  isRefreshingList?: boolean;
} | {
  type: 'NEW_NOTIF_RECEIVED';
} | {
  type: 'SUBSCRIBE_STATUS_REQUESTED';
  token: string;
} | {
  type: 'REGISTER_SUBSCRIPTION_REQUESTED';
  token: string;
  productID: string;
  resyncUser: boolean;
  platform: 'ios' | 'android';
  subscriptionToken?: string;
  purchaseReceipts?: Array<string>;
} | {
  type: 'RESET';
} | {
  type: 'SUBSCRIPTION_CANCELATION_REQUESTED';
  token: string;
  subscriptionToken: string;
} | {
  type: 'LODGE_SUBSCRIPTION_SUPPORT_REQUESTED';
  token: string;
  platform: 'ios' | 'android';
  subscriptionToken?: string;
  purchaseReceipts?: Array<string>;
  diagnosticData: string;
} | {
  type: 'SHOW_POPUP';
  content: string;
} | {
  type: 'HIDE_POPUP';
} | {
  type: 'ASSIGN_PROGRAM_REQUESTED';
  token: string;
  programID: string;
} | {
  type: 'KEYBOARD_DID_SHOW';
  keyboardSpace: number;
} | {
  type: 'KEYBOARD_DID_HIDE';
} | {
  type: 'PRODUCT_ID_LIST_RECEIVED';
  productIDList: Array<string>;
} | {
  type: 'FETCH_PROGRAM_LIST_REQUESTED';
  token: string;
} | {
  type: 'CHAT_WORD_COUNT_UPDATED';
  wordCountLeft: number;
} | {
  type: 'HIDE_WORD_COUNT_STATUS';
} | {
  type: 'SHOW_WORD_COUNT_STATUS';
} | {
  type: 'SHOW_WEEKLY_CHECKIN_DIALOG';
} | {
  type: 'HIDE_WEEKLY_CHECKIN_DIALOG';
} | {
  type: 'WEEKLY_CHECKIN_REQUESTED';
  token: string;
  todaysDate: Date;
} | {
  type: 'WEEKLY_CHECKIN_STATUS_RECEIVED';
  weeklyCheckinStatus: WeeklyCheckinStatus;
} | {
  type: 'POST_WEEKLY_CHECKIN_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  checkinReview: string;
  checkinPlan: string;
  checkinWeight: string;
  checkinWaist: string;
} | {
  type: 'FETCH_MORE_NOTIFS_STARTED';
} | {
  type: 'FETCH_MORE_NOTIFS_FINISHED';
} | {
  type: 'NEW_CHAT_NOTIF_RECEIVED';
};
