//@flow

export type HabitCheckoff = {
  index: number;
  checkoff_reminder: any;
  habit_id: string;
};
