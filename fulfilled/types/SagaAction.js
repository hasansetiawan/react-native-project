// @flow

import type {PostType} from './Post';
import type {Privacy} from './Privacy';
import type {ImageData} from './ImageData';
import type {LoadingPageKey} from './LoadingPage';

export type UserPostHistoryAction = {
  type: 'USER_POST_HISTORY_REQUESTED';
  token: string;
  page: number;
  userID: string;
  lastPostID?: string;
  postType?: string;
  refresh?: boolean;
};

export type UserProgramHistoryAction = {
  type: 'USER_PROGRAM_HISTORY_REQUESTED';
  token: string;
};

export type SyncUserAction = {
  type: 'SYNC_USER_REQUESTED';
  userID: string;
  token: string;
  todaysDate: Date;
};

export type ChatListAction = {
  type: 'FETCH_CHAT_LIST_REQUESTED';
  token: string;
  page: number;
  recipient: string;
  isRefreshingList?: boolean;
};

export type FetchPostsAction = {
  type: 'FETCH_POSTS_REQUESTED';
  token: string;
  page: number;
  lastPostID?: string;
  postType?: PostType;
  refresh?: boolean;
};

export type HabitDailyTipsAction = {
  type: 'FETCH_HABIT_REQUESTED';
  token: string;
  programRecordID: string;
};

export type SendChatAction = {
  type: 'SEND_CHAT_REQUESTED';
  id: string;
  token: string;
  text: string;
  recipientNickname: string;
};

export type LikePostAction = {
  type: 'LIKE_ADDED';
  token: string;
  postID: string;
};

export type CommentPostAction = {
  type: 'SEND_COMMENT_REQUESTED';
  id: string;
  token: string;
  text: string;
  postID: string;
};

export type NotificationListAction = {
  type: 'FETCH_NOTIF_LIST_REQUESTED';
  token: string;
  page: number;
  isRefreshingList?: boolean;
};

export type SignUpAction = {
  type: 'SIGN_UP_REQUESTED';
  fullName: string;
  nickname: string;
  email: string;
  password: string;
  parseID: string;
};

export type CreateNotePostAction = {
  type: 'SEND_NOTE_POST_REQUESTED';
  id: string;
  token: string;
  text: string;
  privacy: Privacy;
  imageData: ?ImageData;
};

export type ActivityPostAction = {
  type: 'SEND_ACTIVITY_POST_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  activityDescription: string;
  activityDurationHours: string;
  activityDurationMinutes: string;
  activityComment: string;
  imageData: ?ImageData;
};

export type MealPostAction = {
  type: 'SEND_MEAL_POST_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  mealDescription: string;
  mealPeriod: string;
  imageData: ?ImageData;
};

export type MeasurementPostAction = {
  type: 'SEND_MEASUREMENT_POST_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  text: string;
  weight: string;
  waist: string;
  hip: string;
  imageData: ?ImageData;
};

export type DrinkPostAction = {
  type: 'SEND_DRINK_POST_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  drinkDescription: string;
  drinkSize: string;
  imageData: ?ImageData;
};

export type FinishHabitActivityAction = {
  type: 'FINISH_HABIT_ACTIVITY_REQUESTED';
  userID: string;
  token: string;
  habitActivityID: string;
  formData?: string;
};

export type PostImageAction = {
  type: 'POST_IMAGE_SUBMITTED';
  token: string;
  postID: string;
  imageData: ImageData;
};

export type LoginAction = {
  type: 'LOGIN';
  nickname: string;
  password: string;
  parseInstallationID: string;
  todaysDate: Date;
};

export type ProgramListAction = {
  type: 'FETCH_PROGRAM_LIST_REQUESTED';
  token: string;
};

export type FetchContentAction = {
  type: 'FETCH_CONTENT_REQUESTED';
  contentID: string;
  token: string;
  showContent?: boolean;
  loadingPage?: LoadingPageKey;
};

export type CompleteOrientationAction = {
  type: 'COMPLETE_ORIENTATION_REQUESTED';
  token: string;
  userID: string;
};

export type SubmitProfilePictureAction = {
  type: 'CHANGE_PROFILE_PICTURE_REQUESTED';
  id: string;
  imageData: ImageData;
  token: string;
  name: string;
  language: string;
};

export type FetchProfilePictureAction = {
  type: 'FETCH_PROFILE_PICTURE_REQUESTED';
  token: string;
  userID: string;
};

export type ResetUserPasswordAction = {
  type: 'RESET_PASSWORD_REQUESTED';
  nickname: string;
};

export type SubmitOrientationAnswerAction = {
  token: string;
  userID: string;
  formData: string;
};
export type NotificationDetailAction = {
  type: 'FETCH_NOTIFICATION_DETAIL_REQUESTED';
  token: string;
  postID: string;
};

export type FinishHabitAction = {
  type: 'FINISH_HABIT_REQUESTED';
  token: string;
  habitID: string;
  userHabitRecordID: string;
};

export type CheckSubscribeStatusAction = {
  type: 'SUBSCRIBE_STATUS_REQUESTED';
  token: string;
};

export type RegisterSubscriptionAction = {
  type: 'REGISTER_SUBSCRIPTION_REQUESTED';
  token: string;
  platform: 'ios' | 'android';
  subscriptionToken?: string;
  subscriptionReceipt?: string;
  subscriptionSignature?: string;
  purchaseReceipts?: Array<string>;
};

export type LodgeSubscriptionSupportAction = {
  type: 'LODGE_SUBSCRIPTION_SUPPORT_REQUESTED';
  token: string;
  platform: 'ios' | 'android';
  subscriptionToken?: string;
  subscriptionReceipt?: string;
  subscriptionSignature?: string;
  purchaseReceipts?: Array<string>;
  diagnosticData: string;
};

export type AssignProgramAction = {
  type: 'ASSIGN_PROGRAM_REQUESTED';
  token: string;
  programID: string;
};

export type FetchProgramRecordAction = {
  type: 'PROGRAM_RECORD_REQUESTED';
  token: string;
};

export type GetProductIDAction = {
  type: 'GET_PRODUCT_ID_LIST_REQUESTED';
  platform: 'ios' | 'android';
};

export type GetWeeklyCheckinAction = {
  type: 'WEEKLY_CHECKIN_REQUESTED';
  token: string;
  todaysDate: Date;
};

export type PostWeeklyCheckinAction = {
  type: 'POST_WEEKLY_CHECKIN_REQUESTED';
  id: string;
  token: string;
  privacy: Privacy;
  checkinReview: string;
  checkinPlan: string;
  checkinWeight: string;
  checkinWaist: string;
};
