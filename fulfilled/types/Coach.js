// @flow

import type {ProfilePicture} from './User';

export type Coach = {
  id: string;
  coachIntroductionID: string;
  nickname: string;
  firstName: string;
  name: string;
  timeZone: string;
  isMentor: boolean;
  isCoach: boolean;
  isNutritionist: boolean;
  isDietitian: boolean;
  ageInDays: number;
  orientationContentIDs?: Array<string>;
  profilePicture?: ProfilePicture;
};

export type CoachTitle = 'mentor' | 'nutrititonist' | 'coach' | 'dietitian';
