//@flow

export type Program = {
  title: string;
  advancementType: number;
  category: number;
  categoryDescription: string;
  introContent: string;
  icon: string;
  iconColor: string;
  habitCount: number;
  totalHabitDays: number;
  lock: boolean;
  isFoundation: boolean; //if this is true, app needs to check that this program is done before other programs are unlocked
  isPremium: boolean; //add premium tag if true
  id: string;
  display: number;
  programGoal: string;
  status: number;
  totalUsers: number;
  habits: Array<string>;
  image: ProgramIcon;
};

export type ProgramHabit = {
  text: string;
  activityTexts: Array<string>;
};

export type ProgramHabitActivity = {
  text: string;
  renderedTitle: string;
  renderedContent: string;
};

export type ProgramState = {
  programList: Map<string, Program>;
  selectedProgramID: string;
  currentProgram: {
    title: string;
    id: string;
  };
  habitList: Map<string, ProgramHabit>;
  programRecordID: string;
};

export type ProgramIcon = {
  standardResolution: string;
  lowResolution: string;
  thumbnailResolution?: string;
  svg: string;
};


export type ProgramCategoryDescription = 'nutrition' | 'workout';
