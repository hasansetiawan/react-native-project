// @flow

export type Privacy = 'community' | 'guide' | 'private';
//community = public, guide = only coach can see, private = only poster can see
