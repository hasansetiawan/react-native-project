// @flow
import {call, put} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import normalizeLoginData from '../normalizers/normalizeLoginData';
import formatToCamelCase from '../helpers/formatToCamelCase';

import type {LoginAction} from '../types/SagaAction';

import UserAPI from '../API/UserAPI';
let postLogin = UserAPI.postLogin;

export function* watchLoginSaga(): Generator<any, any, any> {
  yield takeLatest('LOGIN', loginAuthentication);
}

export function* loginAuthentication(action: LoginAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let userAuth = {
    nickname: action.nickname,
    password: action.password,
    parseInstallationID: action.parseInstallationID,
  };
  try {
    result = yield call(postLogin, userAuth);
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      normalizedResult = yield call(normalizeLoginData, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      yield put({type: 'RESET'});
      let {userList, coachList} = camelCasedResult.entities;
      let user = userList[camelCasedResult.result];
      let coach = coachList[user.coach];
      yield put({type: 'USER_RECEIVED', user});
      yield put({type: 'COACH_RECEIVED', coach});
      yield put({
        type: 'SYNC_USER_REQUESTED',
        userID: user.id,
        token: user.token,
      });
      yield put({
        type: 'WEEKLY_CHECKIN_REQUESTED',
        token: user.token,
        todaysDate: action.todaysDate,
      });
      yield put({
        type: 'SAVE_LOGIN_REQUESTED',
        userID: user.id,
        token: user.token,
      });
      yield put({
        type: 'DRAWER_CLOSED',
      });
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'loginPage',
  });
}
