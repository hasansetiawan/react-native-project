const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {fetchChatList, sendChat} from '../chatSaga';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/ChatAPI';

describe('chatSaga test', () => {
  it('should fetch chat list based on token, page, and recipient', () => {
    let nestedObject = [
      {
        created_at: '2016-10-27T08:42:01Z',
        text: 'test',
        id: '5811bdd9c1b282354f000012',
        display_text: 'test',
        privacy: 'private',
        poster_nickname: 'zhen',
        recipient_nickname: 'sstur2',
        user_is_poster: false,
        created_at_in_seconds: 1477557721,
      },
    ];
    let data = {token: 'userToken', page: 1, recipient: 'sstur2'};
    let formated = formatToCamelCase(nestedObject);

    let generator = fetchChatList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchChatList, 'userToken', 1, 'sstur2'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(formatToCamelCase, nestedObject));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'CHAT_LIST_RECEIVED', chatList: formated, page: data.page}));
  });
  it('should throw put SHOW_TOAST when fetch chat list throws', () => {
    let nestedObject = [
      {
        created_at: '2016-10-27T08:42:01Z',
        text: 'test',
        id: '5811bdd9c1b282354f000012',
        display_text: 'test',
        privacy: 'private',
        poster_nickname: 'zhen',
        recipient_nickname: 'sstur2',
        user_is_poster: false,
        created_at_in_seconds: 1477557721,
      },
    ];
    let data = {token: 'userToken', page: 1, recipient: 'sstur2'};
    let error = {
      message: 'something',
    };
    let generator = fetchChatList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchChatList, 'userToken', 1, 'sstur2'));
    expect(generator.throw(error).value)
      .toEqual(put({type: 'SHOW_TOAST', toastMessage: 'something'}));
  });

  it('should send new chatbased on token, text, and recipient', () => {
    let data = {id: '123', token: 'userToken', text: 'hai', recipientNickname: 'sstur2'};
    let generator = sendChat(data);

    let nestedObject = {
      created_at: '2016-10-27T08:42:01Z',
      text: 'test',
      id: '5811bdd9c1b282354f000012',
      display_text: 'test',
      privacy: 'private',
      poster_nickname: 'zhen',
      recipient_nickname: 'sstur2',
      user_is_poster: false,
      created_at_in_seconds: 1477557721,
      word_count_left: 200,
    };

    let formated = formatToCamelCase(nestedObject);

    expect(generator.next(nestedObject).value)
      .toEqual(call(API.submitChat, 'userToken', 'hai', 'sstur2'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(formatToCamelCase, nestedObject));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'ADD_CHAT_SUCCESS', optimisticID: data.id, newChat: formated}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'CHAT_WORD_COUNT_UPDATED', wordCountLeft: nestedObject.word_count_left}));
  });

  // TODO: remove this when already know why recipient id sometimes null
  it('should not call API when recipient is null', () => {
    let nestedObject = [
      {
        created_at: '2016-10-27T08:42:01Z',
        text: 'test',
        id: '5811bdd9c1b282354f000012',
        display_text: 'test',
        privacy: 'private',
        poster_nickname: 'zhen',
        recipient_nickname: 'sstur2',
        user_is_poster: false,
        created_at_in_seconds: 1477557721,
      },
    ];
    let data = {token: 'userToken', page: 1};
    let generator = fetchChatList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(put({type: 'SHOW_TOAST', toastMessage: 'recipientIDNotFound'}));
  });
});
