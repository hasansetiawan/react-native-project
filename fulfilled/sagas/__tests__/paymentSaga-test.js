const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {
  checkSubscribeStatus,
  registerSubscription,
  lodgeSubscriptionSupport,
  getProductID,
} from '../paymentSaga';

import API from '../../API/PaymentAPI';

describe('paymentSaga test', () => {
  it('should check user subscription status', () => {
    let returnObject = {
      success: true,
      subscribed: true,
      expiry: '2012-04-23T18:25:43.511Z',
    };
    let data = {token: 'userToken'};

    let generator = checkSubscribeStatus(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.checkSubscription, 'userToken'));
    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SUBSCRIBE_STATUS_RECEIVED', subscribed: returnObject.subscribed, expiry: returnObject.expiry}));
  });
  it('should register new subscription on ios', () => {
    let returnObject = {
      success: true,
      subscribed: true,
      expiry: '2012-04-23T18:25:43.511Z',
    };
    let data = {
      token: 'userToken',
      resyncUser: true,
      platform: 'ios',
      purchaseReceipts: 'purchaseReceipts',
    };
    let generator = registerSubscription(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.subscribeIOS, 'userToken', 'purchaseReceipts'));

    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SUBSCRIBE_STATUS_RECEIVED', subscribed: returnObject.subscribed, expiry: returnObject.expiry}));
    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'ROUTE_POPPED'}));
    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SHOW_POPUP', content: 'subscriptionSuccess'}));
  });

  it('should register new subscription on android', () => {
    let returnObject = {
      success: true,
      subscribed: true,
      expiry: '2012-04-23T18:25:43.511Z',
    };
    let data = {
      token: 'userToken',
      resyncUser: true,
      platform: 'android',
      purchaseReceipts: 'purchaseReceipts',
      subscriptionToken: 'subscriptionToken',
      subscriptionReceipt: 'subscriptionReceipt',
      subscriptionSignature: 'subscriptionSignature',
    };
    let generator = registerSubscription(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.subscribeAndroid, 'userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature'));

    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SUBSCRIBE_STATUS_RECEIVED', subscribed: returnObject.subscribed, expiry: returnObject.expiry}));

    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'ROUTE_POPPED'}));

    expect(generator.next(returnObject).value)
      .toEqual(put({
        type: 'SHOW_POPUP',
        content: 'subscriptionSuccess',
      }
    ));
  });

  it('should throw error when platform neither ios nor android', () => {
    let returnObject = {
      success: false,
      subscribed: false,
      expiry: '',
    };
    let data = {
      token: 'userToken',
      resyncUser: true,
      platform: 'anything else',
      purchaseReceipts: 'purchaseReceipts',
      subscriptionToken: 'subscriptionToken',
    };
    let generator = registerSubscription(data);

    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SHOW_TOAST', toastMessage: 'platform not found'}));
  });

  it('should request lodge subscription', () => {
    let returnObject = {
      success: true,
      subscribed: true,
      expiry: '2012-04-23T18:25:43.511Z',
    };
    let data = {
      token: 'userToken',
      platform: 'android',
      purchaseReceipts: 'purchaseReceipts',
      subscriptionToken: 'subscriptionToken',
      subscriptionReceipt: 'subscriptionReceipt',
      subscriptionSignature: 'subscriptionSignature',
      diagnosticData: 'diagnosticData',
    };
    let generator = lodgeSubscriptionSupport(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.lodgeSubscriptionSupportAndroid, 'userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature', 'diagnosticData'));

    data = {
      token: 'userToken',
      platform: 'ios',
      purchaseReceipts: 'purchaseReceipts',
      subscriptionToken: 'subscriptionToken',
      diagnosticData: 'diagnosticData',
    };
    generator = lodgeSubscriptionSupport(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.lodgeSubscriptionSupportIOS, 'userToken', 'purchaseReceipts', 'diagnosticData'));

  });

  it('should request product id list', () => {
    let returnObject = {
      subscriptions: [
        'standard_monthly_rate_at_2016_11',
      ],
    };
    let data = {
      platform: 'android',
    };
    let generator = getProductID(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.getProductIDAndroid));

    data = {
      token: 'userToken',
      platform: 'ios',
      purchaseReceipts: 'purchaseReceipts',
      subscriptionToken: 'subscriptionToken',
      diagnosticData: 'diagnosticData',
    };
    generator = getProductID(data);
    expect(generator.next(returnObject).value)
      .toEqual(call(API.getProductIDIOS));

  });
});
