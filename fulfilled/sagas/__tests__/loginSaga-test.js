const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {loginAuthentication} from '../loginSaga';
import normalizeLoginData from '../../normalizers/normalizeLoginData';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import UserAPI from '../../API/UserAPI';
let postLogin = UserAPI.postLogin;

describe('loginSaga test', () => {
  let nestedObject = {
    id: '1',
    name: 'something',
    nick_name: 'hey',
    coach: {
      id: '-1',
      name: 'prawiti',
    },
  };
  let normalized = normalizeLoginData(nestedObject);
  let formated = formatToCamelCase(normalized);
  it('should postLogin based on user authentication', () => {
    let today = new Date();
    let generator = loginAuthentication({nickname: 'nickname', password: '123', parseInstallationID: 'parseInstallationID', todaysDate: today});
    expect(generator.next(nestedObject).value)
      .toEqual(call(postLogin, {nickname: 'nickname', password: '123', parseInstallationID: 'parseInstallationID'}));
    expect(generator.next(nestedObject).value)
      .toEqual(call(normalizeLoginData, nestedObject));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));

    let {userList, coachList} = formated.entities;
    let user = userList[formated.result];
    expect(generator.next(formated).value)
      .toEqual(put({type: 'RESET'}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'USER_RECEIVED', user}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COACH_RECEIVED', coach: coachList[user.coach]}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SYNC_USER_REQUESTED', userID: user.id, token: user.token}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_REQUESTED', token: user.token, todaysDate: today}));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'SAVE_LOGIN_REQUESTED', userID: user.id, token: user.token}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'DRAWER_CLOSED'}));
  });
  it('should throw put LOGIN_FAILED when postLogin throws', () => {
    let generator = loginAuthentication({nickname: '123', password: '123', parseInstallationID: 'parseInstallationID'});
    expect(generator.next().value)
      .toEqual(call(postLogin, {nickname: '123', password: '123', parseInstallationID: 'parseInstallationID'}));
    let errorMessage = {
      message: 'something',
    };
    expect(generator.throw(errorMessage).value)
      .toEqual(put({type: 'SHOW_TOAST', toastMessage: 'something'}));
  });
});
