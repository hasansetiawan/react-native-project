const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {
  fetchProgramList,
  assignProgram,
  fetchProgramRecord,
} from '../programSaga';
import formatToCamelCase from '../../helpers/formatToCamelCase';
import normalizeProgram from '../../normalizers/normalizeProgram';
import normalizeProgramRecord from '../../normalizers/normalizeProgramRecord';

import API from '../../API/ProgramAPI';

describe('programSaga test', () => {
  it('should fetch program list based on token', () => {
    let nestedObject = [
      {
        title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
        advancement_type: 1,
        category: 1,
        category_description: 'nutrition',
        intro_content: '',
        icon: '&#xE42E;',
        icon_color: 'color_coach_post',
        habit_count: 1,
        total_habit_days: 7,
        lock: false,
        is_foundation: false,
        is_premium: false,
        id: '57033f225a15c98a2b00000c',
        display: 1,
        program_goal: '',
        status: 1,
        total_users: 7447,
        habits: [
          {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            activity_texts: [
              {
                text: '20 menit: Mengatur makan kini begitu mudah!',
                rendered_title: '20 menit: Mengatur makan kini begitu mudah!',
                rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Selamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.</p><p>Mampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal. Hal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.</p><p><strong>Tips pertama</strong>: Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut. Kenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.</p><p>Ketika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi. Kamu pasti bisa!</p><p><a href=\'http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled\'>Baca lebih lanjut mengenai pendekatan Fulfilled</a></p>`,
              },
              {
                text: 'Dari rasa turun ke hati',
                rendered_title: 'Dari rasa turun ke hati',
                rendered_content: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP2_200.png' width='180px' /></div><p>Gunakan beberapa saat untuk benar-benar merasakan makananmu hari ini.</p><p>Selama 20 menit makan hari ini, gunakan beberapa saat untuk menutup mata. Suap/gigit makanan dan kunyah. Biarkan mata tetap tertutup! Nikmati semua rasa, tekstur, dan aroma dari setiap serpihan makanan dalam mulutmu. Manis? Asin? Lembut? Kenyal? Setelah beberapa saat, kamu bisa membuka matamu… Dengan demikian, kamu bisa lebih menikmati setiap suapan makananmu.</p><p>Jangan lupa untuk mengambil foto makananmu!</p>`,
              },
            ],
          },
        ],
      },
    ];
    let data = {token: 'userToken'};
    let normalize = normalizeProgram(nestedObject);
    let formated = formatToCamelCase(normalize);

    let generator = fetchProgramList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchProgramList, 'userToken'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(normalizeProgram, nestedObject));
    expect(generator.next(normalize).value)
      .toEqual(call(formatToCamelCase, normalize));

    let {programList, habitList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'PROGRAM_LIST_RECEIVED', programList: Object.values(programList), habitList: Object.values(habitList)}));
  });
  it('should throw put FETCH_PROGRAM_LIST_FAILED when fetch program list throws', () => {
    let nestedObject = [
      {
        title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
        advancement_type: 1,
        category: 1,
        category_description: 'nutrition',
        intro_content: '',
        icon: '&#xE42E;',
        icon_color: 'color_coach_post',
        habit_count: 1,
        total_habit_days: 7,
        lock: false,
        is_foundation: false,
        is_premium: false,
        id: '57033f225a15c98a2b00000c',
        display: 1,
        program_goal: '',
        status: 1,
        total_users: 7293,
      },
    ];
    let data = {token: 'userToken'};
    let generator = fetchProgramList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchProgramList, 'userToken'));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'FETCH_PROGRAM_LIST_FAILED', error: 'something'}));
  });
  it('should assign program to user based on token', () => {
    let nestedObject = {success: true};
    let data = {token: 'userToken', programID: 'programID'};

    let generator = assignProgram(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.assignProgram, 'userToken', 'programID'));
    expect(generator.next(nestedObject).value)
      .toEqual(put({type: 'PROGRAM_RECORD_REQUESTED', token: data.token}));
    expect(generator.next(nestedObject).value)
      .toEqual(put({type: 'LOADING_STARTED', loadingPages: 'challengePage'}));
    expect(generator.next(nestedObject).value)
      .toEqual(put({type: 'SHOW_POPUP', content: 'assignProgramSuccess'}));
  });
  it('should show pop up when assign program failed', () => {
    let nestedObject = {success: false, message: 'errorMessage'};
    let data = {token: 'userToken', programID: 'programID'};

    let generator = assignProgram(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.assignProgram, 'userToken', 'programID'));
    expect(generator.next(nestedObject).value)
      .toEqual(put({type: 'SHOW_POPUP', content: 'errorMessage'}));
  });
  it('should fetch program record based on token', () => {
    let nestedObject = [
      {
        program: {
          title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
          program_goal: '',
          advancement_type_description: 'check_based',
          category_description: 'nutrition',
          icon: '&#xE42E;',
          icon_color: 'color_coach_post',
          is_foundation: false,
          id: '57033f225a15c98a2b00000c',
        },
        current_habit_record: {
          days_completed: 0,
          habit: {
            text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
            completion_target: 7,
            id: '570340125a15c968dc000003',
            uncompleted_post_id: null,
            days_completed: null,
          },
          id: '58326b46bb9982993a00001a',
          days_active: 22.9966565662815,
        },
        id: '58326b46bb9982993a000019',
        days_on_program: null,
        days_completed: 0,
        total_program_time: 7,
      },
    ];
    let data = {token: 'userToken'};
    let normalizedResult = normalizeProgramRecord(nestedObject);
    let formated = formatToCamelCase(normalizedResult);

    let generator = fetchProgramRecord(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchProgramRecord, 'userToken'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(normalizeProgramRecord, nestedObject));
    expect(generator.next(normalizedResult).value)
      .toEqual(call(formatToCamelCase, normalizedResult));

    let {
      programList,
      programRecordList,
      habitList,
      currentHabitRecordList,
    } = formated.entities;

    let programRecord = programRecordList[formated.result[0]];
    let currentProgram = programList[programRecord.program];
    let currentHabit = habitList[currentHabitRecordList[programRecord.currentHabitRecord].habit];

    expect(generator.next(formated).value)
      .toEqual(put({type: 'FETCH_HABIT_REQUESTED', token: data.token, programRecordID: formated.result[0]}));
    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'PROGRAM_RECORD_RECEIVED',
        programRecordID: formated.result[0],
        currentProgram: {
          title: currentProgram.title,
          id: currentProgram.id,
        },
      }
    ));
    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: currentHabit,
      }
    ));
  });
});
