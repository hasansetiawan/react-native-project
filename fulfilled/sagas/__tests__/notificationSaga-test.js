const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {fetchNotificationList} from '../notificationSaga';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/NotificationAPI';

describe('notificationSaga test', () => {
  let nestedObject = [
    {
      created_at: '2016-10-20T09:27:10Z',
      id: '58088deeebde51849c001c4f',
      _id: '58088deeebde51849c001c4f',
      created_at_local_time: '2016-10-20T17:27:10+08:00',
      is_new: false,
      type: 'coach_post',
      text: `Coach Tiwi posted a message: 'Dear Fulfiller,\n\nFulfilled telah meningkatkan server aplikasi kami agar dapat melayani Fulfiller ...'`,
      post_id: '57f1d7ffebde51e9ae0000c5',
      question_id: null,
      content_id: null,
      user_habit_record_id: null,
      program_record_id: null,
    },
  ];
  let data = {token: 'userToken', page: 1};
  let formated = formatToCamelCase(nestedObject);

  it('should fetch notification list based on userToken', () => {
    let generator = fetchNotificationList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchNotifications, 'userToken', 1));
    expect(generator.next(nestedObject).value)
      .toEqual(call(formatToCamelCase, nestedObject));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'NOTIF_LIST_RECEIVED', notifList: Object.values(formated), page: 1}));
  });
  it('should not update notification page number in notification reducer when refresh', () => {
    data = {token: 'userToken', page: 1, isRefreshingList: true};
    let generator = fetchNotificationList(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchNotifications, 'userToken', 1));
    expect(generator.next(nestedObject).value)
      .toEqual(call(formatToCamelCase, nestedObject));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'NOTIF_LIST_RECEIVED', notifList: Object.values(formated)}));
  });
  it('should throw put FETCH_NOTIF_LIST_FAILED when fetch throws', () => {
    let generator = fetchNotificationList(data);
    expect(generator.next().value)
      .toEqual(call(API.fetchNotifications, 'userToken', 1));
  });
});
