const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {
  fetchPostList,
  likePost,
  commentPost,
  createNotePost,
  createActivityPost,
  createMealPost,
  createMeasurementPost,
  createDrinkPost,
  submitPostImage,
} from '../postSaga';
import {nestedPostData} from '../../fixtures/Posts';
import {normalizePostList, normalizePost} from '../../normalizers/normalizePosts';
import normalizeComment from '../../normalizers/normalizeComment';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/PostAPI';

describe('postSaga', () => {
  it('should fetchPostList based on action given', () => {
    let action = {
      type: 'FETCH_POSTS_REQUESTED',
      token: 'userToken',
      page: 1,
    };
    let normalized = normalizePostList(nestedPostData);
    let formated = formatToCamelCase(normalized);

    let generator = fetchPostList(action);
    expect(generator.next(nestedPostData).value)
      .toEqual(call(API.fetchPostList, 'userToken', 1, null, null));
    expect(generator.next(nestedPostData).value)
      .toEqual(call(normalizePostList, nestedPostData));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList, userList, commentList} = formated.entities;

    for (let user of Object.values(userList)) {
      expect(generator.next(formated).value)
        .toEqual(put({type: 'FETCH_PROFILE_PICTURE_REQUESTED', token: 'userToken', userID: user.id}));
    }
    expect(generator.next(formated).value)
      .toEqual(put({type: 'POSTS_RECEIVED', postList: Object.values(postList), page: action.page}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMMENT_LIST_RECEIVED', commentList: Object.values(commentList)}));
  });
  it('should throw put FETCH_POSTS_FAILED when fetchPostList throws', () => {
    let action = {
      type: 'FETCH_POSTS_REQUESTED',
      token: 'userToken',
      page: 1,
    };
    let generator = fetchPostList(action);
    expect(generator.next().value)
      .toEqual(call(API.fetchPostList, 'userToken', 1, null, null));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'FETCH_POSTS_FAILED', error: 'something'}));
  });

  it('should likePost based on action given', () => {
    let action = {
      type: 'LIKE_ADDED',
      token: 'userToken',
      postID: 'postID',
    };
    let post = nestedPostData[0];
    let generator = likePost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(post).value)
      .toEqual(call(API.likePost, 'userToken', 'postID'));
    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'POSTS_RECEIVED', postList: Object.values(postList)}));
  });
  it('should throw put LIKE_POST_FAILED when likePost throws', () => {
    let action = {
      type: 'LIKE_ADDED',
      token: 'userToken',
      postID: 'postID',
    };
    let post = nestedPostData[0];
    let generator = likePost(action);
    expect(generator.next(post).value)
      .toEqual(call(API.likePost, 'userToken', 'postID'));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'LIKE_POST_FAILED', error: 'something'}));
  });

  it('should comment post based on action given', () => {
    let action = {
      type: 'SEND_COMMENT_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      postID: 'postID',
      text: 'newComment',
    };
    let post = nestedPostData[0];
    let generator = commentPost(action);
    let normalized = normalizeComment(post);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(post).value)
      .toEqual(call(API.commentPost, 'userToken', 'newComment', 'postID'));

    expect(generator.next(post).value)
      .toEqual(call(normalizeComment, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {commentList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'ADD_COMMENT_SUCCESS', optimisticID: 'tempID', comment: commentList[formated.result]}));
  });

  it('should create note post based on action given', () => {
    let action = {
      type: 'SEND_NOTE_POST_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      privacy: 'community',
      text: 'newContent',
    };
    let post = nestedPostData[0];
    let generator = createNotePost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(post).value)
      .toEqual(call(API.submitNotePost, 'userToken', 'newContent', 'community'));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));
  });

  it('should create activity log post based on action given', () => {
    let action = {
      type: 'SEND_ACTIVITY_POST_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      activityDescription: 'activity desc',
      activityDurationHours: '10',
      activityDurationMinutes: '20',
      privacy: 'community',
      activityComment: 'comment',
    };
    let post = nestedPostData[0];
    let generator = createActivityPost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(post).value)
      .toEqual(call(API.submitActivityPost, 'userToken', 'community', 'activity desc', '10', '20', 'comment'));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));
  });

  it('should create meal post based on action given', () => {
    let action = {
      type: 'SEND_MEAL_POST_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      mealDescription: 'meal desc',
      mealPeriod: 'Morning Snack',
      privacy: 'community',
    };
    let post = nestedPostData[0];
    let generator = createMealPost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(post).value)
      .toEqual(call(API.submitMealPost, 'userToken', 'community', 'meal desc', 'Morning Snack'));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));
  });

  it('should create measurement post based on action given', () => {
    let action = {
      type: 'SEND_MEASUREMENT_POST_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      text: 'text',
      privacy: 'community',
      weight: '10',
      waist: '20',
      hip: '30',
    };
    let post = nestedPostData[0];
    let generator = createMeasurementPost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);
    expect(generator.next(post).value)
      .toEqual(call(API.submitMeasurementPost, 'userToken', 'text', 'community', '10', '20', '30'));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));
  });

  it('should create drink post based on action given', () => {
    let action = {
      type: 'SEND_DRINK_POST_REQUESTED',
      id: 'tempID',
      token: 'userToken',
      privacy: 'community',
      drinkDescription: 'text',
      drinkSize: '1 litre',
    };
    let post = nestedPostData[0];
    let generator = createDrinkPost(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);
    expect(generator.next(post).value)
      .toEqual(call(API.submitDrinkPost, 'userToken', 'community', 'text', '1 litre'));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));
  });

  it('should submit post image based on action given', () => {
    let action = {
      type: 'POST_IMAGE_SUBMITTED',
      token: 'userToken',
      postID: '123',
      imageData: {
        uri: 'imageDataUri',
      },
    };
    let post = nestedPostData[0];
    let generator = submitPostImage(action);
    let normalized = normalizePost(post);
    let formated = formatToCamelCase(normalized);
    expect(generator.next(post).value)
      .toEqual(call(API.submitImage, 'userToken', '123', {uri: 'imageDataUri'}));

    expect(generator.next(post).value)
      .toEqual(call(normalizePost, post));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {postList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: '123', post: postList[formated.result]}));
  });
});
