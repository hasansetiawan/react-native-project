const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {fetchHabitDailyTipsList, completeHabitActivity, completeHabit} from '../habitSaga';
import {normalizeDailyTips, normalizeDailyTip} from '../../normalizers/normalizeDailyTips';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/HabitAPI';

describe('habitSaga', () => {
  let nestedObject = [
    {
      id: '56c607736e7d6b98f4000008',
      text: 'Mindful Eating - Ate Slowly Today',
      completed: false,
      user_habit_record_id: '57dd3d1804dc60758900016b',
      reminder_text: 'Your journey to transformation awaits. Get started with Fulfilled.\nDeciding to change is the hard part. Let Fulfilled do the rest.\nThe best version of you is waiting. Unleash it with Fulfilled.\nWe know you’ve been thinking about this for a while. Just start.\nDon’t hesitate. We’re here to support and encourage you.\nYou may have tried before and failed. This time, we’ve got your back.\nLove yourself. Get started with Fulfilled.\nBelief that we can change fuels the Fulfilled community. Join.\nGive us a chance to earn your trust. Get started with Fulfilled.\nTransformation takes time. We’re not going anywhere. Join us.\nBe a little better today than yesterday. Start Fulfilled.\nRefocus from shame and guilt to transformation and fulfilment.\nSupport & encouragement equals success at Fulfilled. Come join us.',
      program_category: 'nutrition',
      completed_today: false,
      has_instructions: false,
      category: 'core',
      habitCompletionMessageID: '123',
      strategy: 'I am a MASTER OF MINDFUL EATING',
      activity_category: 'lesson',
      activity_parent_record_id: '57dd3d1804dc60758900016b',
      activity_todo_id: '56efc790eb15d2e0b6000008',
      activity_todo_content_id: '56efc790eb15d2e0b6000007',
      activity_todo_text: 'Goodies for you!',
      activity_id: null,
      activity_prompt_text: null,
      content_checkoff_id: '56cd6cdf25bd17902700000c',
      habit_ending_completion_content_id: '56cd6dc325bd17ad41000009',
      display_type: 'show_all_lessons',
    },
    {
      text: '0: You won’t feel deprived, promise!',
      completion_target: 1,
      completion_threshold: null,
      type: 'Lesson',
      text_present_tense: null,
      completed: true,
      reading_material: {
        markdown: `<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nWelcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.\n\nBeing in control of your eating will get you 80% of the way to your weight goal.\nIt’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself. \n\n**Here’s the first tip:** Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. \nWhy? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat. \n\nWhen you’re done, check off the box in the app. We know you can do it! \n\n[Read more about Fulfilled’s approach](http://blog.getfulfilled.co/why-i-started-fulfilled/)`,
        title: 'You won’t feel deprived, promise!',
        id: '56d94ead2a48ce1f07000006',
        html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Welcome to this first foundation challenge. By the end of 7 days, you’ll have tried many ways to help you control your diet without feeling deprived, and to make it a habit.</p><p>Being in control of your eating will get you 80% of the way to your weight goal. It’s also the key to keeping energy levels high, staying in good health and most importantly: knowing yourself.</p><p><strong>Here’s the first tip:</strong> Before a meal, set a 20-minute timer. Finish your food only when/after the time’s up. Why? It gives your brain time to tell your tummy that you’re full. You will naturally eat less, and you definitely won’t overeat.</p><p>When you’re done, check off the box in the app. We know you can do it!</p><p><a href=\'http://blog.getfulfilled.co/why-i-started-fulfilled/\'>Read more about Fulfilled’s approach</a></p>`,
      },
      questions: [],
      id: '56d94ead2a48ce1f07000007',
    },
  ];
  let action = {
    type: 'FETCH_HABIT_REQUESTED',
    token: 'userToken',
    programRecordID: 'programRecordID',
  };
  let normalizedDailyTips = normalizeDailyTips([nestedObject[1]]);
  let formatedDailyTips = formatToCamelCase(normalizedDailyTips);
  let formatedHabit = formatToCamelCase([nestedObject[0]]);
  it('should fetchHabitDailyTipsList based on action given', () => {
    let generator = fetchHabitDailyTipsList(action);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchHabitDailyTipsList, 'userToken', 'programRecordID'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(normalizeDailyTips, [nestedObject[1]]));
    expect(generator.next(normalizedDailyTips).value)
      .toEqual(call(formatToCamelCase, normalizedDailyTips));
    expect(generator.next(formatedDailyTips).value)
      .toEqual(call(formatToCamelCase, [nestedObject[0]]));


    let {dailyTipsList, readingMaterialList} = formatedDailyTips.entities;

    expect(generator.next(formatedHabit).value)
      .toEqual(put({
        type: 'FETCH_CONTENT_REQUESTED',
        token: action.token,
        contentID: Object.values(formatedHabit)[0].contentCheckoffID,
      }
    ));

    expect(generator.next(formatedHabit).value)
      .toEqual(put({
        type: 'FETCH_CONTENT_REQUESTED',
        token: action.token,
        contentID: Object.values(formatedHabit)[0].habitCompletionMessageID,
      }
    ));

    expect(generator.next(formatedHabit).value)
      .toEqual(put({
        type: 'READING_MATERIAL_LIST_RECEIVED',
        readingMaterialList: Object.values(readingMaterialList),
      }
    ));
    expect(generator.next(formatedHabit).value)
      .toEqual(put({
        type: 'HABIT_ACTIVITY_LIST_RECEIVED',
        habitActivityList: Object.values(dailyTipsList),
      }
    ));
    expect(generator.next(formatedHabit).value)
      .toEqual(put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: Object.values(formatedHabit)[0],
      }
    ));
  });
  it('should throw put FETCH_HABIT_FAILED when fetchPostList throws', () => {
    let generator = fetchHabitDailyTipsList(action);
    expect(generator.next().value)
      .toEqual(call(API.fetchHabitDailyTipsList, 'userToken', 'programRecordID'));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'FETCH_HABIT_FAILED', error: 'something'}));
  });

  it('should complete habit activity', () => {
    let result = {
      text: '20 menit: Mengatur makan kini begitu mudah!',
      completion_target: 1,
      completion_threshold: null,
      _type: 'Lesson',
      text_present_tense: null,
      reading_material: {
        markdown: `<div style=\'text-align: center; padding-bottom: 12px\'>\n<img src=\'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png\' width=\'120px\'></div>\n\nSelamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.\n\nMampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal.\nHal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.\n\n**Tips pertama**: Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut.\nKenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.\n\nKetika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi.\nKamu pasti bisa!\n\n[Baca lebih lanjut mengenai pendekatan Fulfilled](http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled)`,
        title: '20 menit: Mengatur makan kini begitu mudah!',
        id: '57063b6becd3647f32000006',
        _id: '57063b6becd3647f32000006',
        html: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png' width='120px' /></div><p>Selamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.</p><p>Mampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal. Hal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.</p><p><strong>Tips pertama</strong>: Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut. Kenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.</p><p>Ketika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi. Kamu pasti bisa!</p><p><a href=\'http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled\'>Baca lebih lanjut mengenai pendekatan Fulfilled</a></p>`,
      },
      questions: [],
      id: '57063b6becd3647f32000007',
      user_habit_record_id: null,
      days_completed: 1,
      days_on_habit: null,
      _id: '57063b6becd3647f32000007',
      uncompleted_post_id: null,
      habit_completion_message_id: null,
      reminder_text: '',
      program_category: null,
      completed_today: false,
      has_instructions: false,
      category: null,
      strategy: null,
      activity_category: null,
      activity_parent_record_id: null,
      activity_todo_id: null,
      activity_todo_content_id: null,
      activity_todo_text: null,
      activity_id: null,
      activity_prompt_text: null,
      content_checkoff_id: null,
      habit_ending_completion_content_id: null,
      display_type: 'regular',
    };
    action = {
      type: 'FINISH_HABIT_ACTIVITY_REQUESTED',
      token: 'userToken',
      userID: 'userID',
      habitActivityID: nestedObject[0].id,
      formData: null,
    };
    let generator = completeHabitActivity(action);
    let normalized = normalizeDailyTip(result);
    let formatted = formatToCamelCase(normalized);
    expect(generator.next(action).value)
      .toEqual(call(API.completeDailyTips, 'userToken', 'userID', '56c607736e7d6b98f4000008', null));
    expect(generator.next(result).value)
      .toEqual(call(normalizeDailyTip, result));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));

    let {dailyTipsList, readingMaterialList} = formatted.entities;
    expect(generator.next(formatted).value)
      .toEqual(put({
        type: 'HABIT_ACTIVITY_LIST_RECEIVED',
        habitActivityList: Object.values(dailyTipsList),
      }
    ));

    expect(generator.next(formatted).value)
      .toEqual(put({
        type: 'READING_MATERIAL_LIST_RECEIVED',
        readingMaterialList: Object.values(readingMaterialList),
      }
    ));

    action = {
      type: 'FINISH_HABIT_ACTIVITY_REQUESTED',
      token: 'userToken',
      userID: 'userID',
      habitActivityID: nestedObject[0].id,
      formData: 'formData',
    };
    generator = completeHabitActivity(action);
    formatted = formatToCamelCase(nestedObject[0]);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.completeDailyTips, 'userToken', 'userID', '56c607736e7d6b98f4000008', 'formData'));
  });

  it('should complete habit', () => {
    let result = [
      {
        text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
        completion_target: 7,
        completion_threshold: 7,
        _type: 'Daily',
        text_present_tense: '',
        completed: false,
        reading_material: null,
        id: '570340125a15c968dc000003',
        user_habit_record_id: '58371579bb998211ed000027',
        days_completed: 1,
        days_on_habit: null,
        _id: '570340125a15c968dc000003',
        uncompleted_post_id: null,
        habit_completion_message_id: '57350430e24e50c9be00001d',
        reminder_text: 'Transformasikan dirimu bersama Fulfilled.\nMemutuskan untuk berubah terkadang memang sulit. Biarkan Fulfilled menyelesaikannya.\nTemukan versi terbaik dirimu bersama Fulfilled.\nKamu pasti sudah lama menginginkan hidup yang sehat. Mari kita mulai.\nJangan ragu. Kami di sini untuk membantumu.\nMungkin kamu pernah mencoba dan gagal. Maka sekarang, kami ada untuk membantumu.\nSayangi dirimu. Mulailah bersama Fulfilled.\nPercayalah bahwa kita bisa berubah bersama komunitas Fulfilled. Mari bergabung.\nBerikan kami kesempatan untuk mendapatkan kepercayaanmu. Mulailah bersama Fulfilled.\nTransformasi memang membutuhkan waktu. Kami di sini. Bergabunglah.\nJadilah sedikit lebih baik hari ini dibanding kemarin. Mulailah bersama Fulfilled.\nSingkirkan rasa malu dan rasa bersalah, transformasikan dirimu.\nDukungan dan dorongan sama dengan keberhasilan di Fulfilled. Mari bergabung bersama kami.',
        program_category: 'nutrition',
        completed_today: 'done',
        has_instructions: false,
        category: 'core',
        strategy: '',
        activity_category: 'lesson',
        activity_parent_record_id: '58371579bb998211ed000027',
        activity_todo_id: '57063d9becd364de8e000004',
        activity_todo_content_id: '57063d9becd364de8e000003',
        activity_todo_text: 'Dari rasa turun ke hati',
        activity_id: null,
        activity_prompt_text: null,
        content_checkoff_id: '57064af1ecd3642a47000005',
        habit_ending_completion_content_id: null,
        display_type: 'show_all_lessons',
      },
    ];
    action = {
      type: 'FINISH_HABIT_REQUESTED',
      token: 'userToken',
      userHabitRecordID: 'userHabitRecordID',
      habitID: 'habitID',
    };
    let generator = completeHabit(action);
    let formatted = formatToCamelCase(result);
    expect(generator.next(result).value)
      .toEqual(call(API.completeHabit, 'userToken', 'userHabitRecordID', 'habitID'));
    expect(generator.next(result).value)
      .toEqual(call(formatToCamelCase, result));
    expect(generator.next(formatted).value)
      .toEqual(put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: Object.values(formatted)[0],
      }
    ));
  });
});
