const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import getLanguages from '../../helpers/getLanguages';

import {
  syncUser,
  fetchUserProgramHistory,
  fetchUserPostHistory,
  signUp,
  fetchUserProfilePicture,
  submitProfilePicture,
  resetUserPassword,
  getLatestWeeklyCheckin,
  postWeeklyCheckin,
} from '../userSaga';
import {nestedPostData} from '../../fixtures/Posts';
import normalizeSyncData from '../../normalizers/normalizeSyncData';
import normalizeLoginData from '../../normalizers/normalizeLoginData';
import normalizeProgramHistory from '../../normalizers/normalizeProgramHistory';
import {normalizePostList, normalizePost} from '../../normalizers/normalizePosts';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/UserAPI';
import PostAPI from '../../API/PostAPI';

describe('userSaga test', () => {
  let nestedObject = {
    id: '1',
    name: 'something',
    nickname: 'hey',
    isMentor: false,
    isCoach: false,
    isDietitian: false,
    isNutritionist: false,
    coach: {
      id: '132',
      name: 'prawiti',
    },
    program_records: [
      {
        program: {
          title: 'Master of Mindful Eating Challenge',
          programGoal: '',
          id: '56c6062e6e7d6b98f4000006',
        },
        current_habit_record: {
          daysCompleted: 6,
          habit: {
            text: 'Mindful Eating - Ate Slowly Today',
            completion_target: 7,
            type: 'Daily',
            id: '56c607736e7d6b98f4000008',
            uncompletedPostId: null,
            daysCompleted: null,
          },
          id: '57dd3d1804dc60758900016b',
          daysActive: 41.68752670373846,
        },
        id: '57dd3d1804dc60758900016a',
      },
    ],
  };
  let data = {userID: '123', token: 'userToken'};
  let normalized = normalizeSyncData(nestedObject);
  let formated = formatToCamelCase(normalized);

  it('should sync user based on userID and userToken', () => {
    let generator = syncUser(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.syncUser, '123', 'userToken'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(normalizeSyncData, nestedObject));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));

    let {userList, coachList} = formated.entities;
    let user = userList[formated.result];
    let coach = coachList[user.coach];

    expect(generator.next(formated).value)
      .toEqual(
        put({
          type: 'LOADING_STARTED',
          loadingPages: ['challengePage', 'communityFeedPage', 'chatPage', 'notificationPage'],
        },
      ));
    expect(generator.next(formated).value)
      .toEqual(
        put({
          type: 'FETCH_HABIT_REQUESTED',
          token: 'userToken',
          programRecordID: '57dd3d1804dc60758900016a',
        },
    ));
    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'PROGRAM_RECORD_RECEIVED',
        programRecordID: '57dd3d1804dc60758900016a',
        currentProgram: {
          title: 'Master of Mindful Eating Challenge',
          id: '56c6062e6e7d6b98f4000006',
        },
      },
    ));
    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: {
          text: 'Mindful Eating - Ate Slowly Today',
          completionTarget: 7,
          type: 'Daily',
          id: '56c607736e7d6b98f4000008',
          uncompletedPostId: null,
          daysCompleted: null,
        },
      },
    ));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'FETCH_PROFILE_PICTURE_REQUESTED', token: 'userToken', userID: user.id}));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'FETCH_PROFILE_PICTURE_REQUESTED', token: 'userToken', userID: coach.id}));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'FETCH_CHAT_LIST_REQUESTED', token: data.token, page: 1, recipient: coach.id}));

    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'FETCH_POSTS_REQUESTED',
        token: data.token,
        page: 1,
      }
    ));
    expect(generator.next(formated).value)
      .toEqual(put({
        type: 'FETCH_NOTIF_LIST_REQUESTED',
        token: data.token,
        page: 1,
      }
    ));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'LOADING_FINISHED', loadingPages: 'signUpPage'}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'ROUTE_POPPED'}));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'USER_RECEIVED', user}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: [
        {
          id: user.id,
          nickname: user.nickname,
          isMentor: user.isMentor,
          isDietitian: user.isDietitian,
          isNutritionist: user.isNutritionist,
          isCoach: user.isCoach,
        },
      ]}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COACH_RECEIVED', coach}));

  });
  it('should throw put SYNC_FAILED when sync throws', () => {
    let generator = syncUser(data);
    let error = {
      message: 'something',
    };
    expect(generator.next().value)
      .toEqual(call(API.syncUser, '123', 'userToken'));
    expect(generator.throw(error).value)
      .toEqual(put({type: 'SHOW_TOAST', toastMessage: 'something'}));
  });

  it('should fetch user program history based on userToken', () => {
    let programHistory = [
      {
        program: {
          title: 'Tantangan Olahraga Dasar',
          program_goal: 'Tantangan Olahraga Dasar',
          advancement_type_description: 'check_based',
          category_description: 'workout',
          icon: '',
          icon_color: '',
          is_foundation: false,
          id: '57c5585f7485bd364100000c',
        },
        current_habit_record: null,
        id: '57c597ff7485bd16fa00001d',
        days_on_program: null,
        days_completed: 0,
        total_program_time: 14,
      },
    ];
    let generator = fetchUserProgramHistory({token: '123'});
    expect(generator.next(programHistory).value)
      .toEqual(call(API.fetchUserProgramHistory, '123'));
    expect(generator.next(programHistory).value)
      .toEqual(call(normalizeProgramHistory, programHistory));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    let {programList} = formated.entities;
    expect(generator.next(formated).value)
      .toEqual(put({type: 'USER_PROGRAM_HISTORY_RECEIVED', programHistoryList: Object.values(programList)}));
  });

  it('should throw put FETCH_USER_PROGRAM_HISTORY_FAILED when fetching user program history throws', () => {
    let generator = fetchUserProgramHistory({token: '123'});
    expect(generator.next().value)
      .toEqual(call(API.fetchUserProgramHistory, '123'));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'FETCH_USER_PROGRAM_HISTORY_FAILED', error: 'something'}));
  });

  it('should fetch user post history based on userToken, page, and userID', () => {
    let generator = fetchUserPostHistory({token: '123', page: 1, userID: 'audy', lastPostID: 'lastPostID'});
    expect(generator.next(nestedPostData).value)
      .toEqual(call(API.fetchUserPostHistory, '123', 1, 'audy', 'lastPostID'));

    expect(generator.next(nestedPostData).value)
      .toEqual(call(normalizePostList, nestedPostData));

    normalized = normalizePostList(nestedPostData);
    formated = formatToCamelCase(normalized);

    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));

    let {postList, commentList, userList} = formated.entities;

    expect(generator.next(formated).value)
      .toEqual(put({type: 'USER_POST_HISTORY_RECEIVED', postList: Object.values(postList), userID: 'audy', page: 1}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMMENT_LIST_RECEIVED', commentList: Object.values(commentList)}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)}));
  });

  it('should sign up new user based on args given', () => {
    let data = {
      fullName: 'Audy',
      nickname: 'Odi',
      email: 'odi@gmail.com',
      password: 'odiodi',
      parseID: '123',
    };
    let generator = signUp(data);
    let newUser = {
      id: '1',
      name: 'Audy',
      nick_name: 'Odi',
      token: 'userToken',
      parseInstallationID: '123',
      coach: {
        id: '-1',
        name: 'prawiti',
      },
    };

    let normalized = normalizeLoginData(newUser);
    let formated = formatToCamelCase(normalized);

    expect(generator.next(data).value)
      .toEqual(call(API.signUp, data.fullName, data.nickname, data.email, data.password, data.parseID));

    expect(generator.next(newUser).value)
      .toEqual(call(normalizeLoginData, newUser));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));

    let {userList, coachList} = formated.entities;
    let user = userList[formated.result];
    let coach = coachList[user.coach];

    expect(generator.next(formated).value)
      .toEqual(put({type: 'SYNC_USER_REQUESTED', userID: user.id, token: user.token}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SAVE_LOGIN_REQUESTED', userID: user.id, token: user.token}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'USER_RECEIVED', user}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COACH_RECEIVED', coach}));
  });

  it('should fetch profile picture', () => {
    let data = {
      token: 'userToken',
      userID: 'userID',
    };
    let generator = fetchUserProfilePicture(data);
    let imageData = {
      lowResolutionUrl: 'lowResolutionUrl',
      standardResolutionUrl: 'standardResolutionUrl',
    };

    let formated = formatToCamelCase(normalized);

    expect(generator.next(data).value)
      .toEqual(call(API.fetchUserProfilePicture, data.userID, data.token));

    expect(generator.next(imageData).value)
      .toEqual(call(formatToCamelCase, imageData));

    expect(generator.next(formated).value)
      .toEqual(put({type: 'PROFILE_PICTURE_RECEIVED', profilePicture: formated, userID: data.userID}));
  });

  it('should submit profile picture', () => {
    let data = {
      imageData: {
        uri: 'imageDataUri',
      },
      token: 'userToken',
      name: 'odi',
      language: 'en',
      id: 'userID',
    };
    let resultSubmitProfilePicture = {
      id: '123',
    };
    let resultSubmitImage = {
      image: {
        lowResolution: 'lowResolution',
        standardResolution: 'standardResolution',
      },
    };
    let generator = submitProfilePicture(data);
    let formated = formatToCamelCase(resultSubmitImage);
    let message = getLanguages()[data.language]['changedProfilePicture'];
    expect(generator.next(data).value)
      .toEqual(call(API.submitProfilePicture, data.token, `odi ${message}`, 'community'));

    expect(generator.next(resultSubmitProfilePicture).value)
      .toEqual(call(PostAPI.submitImage, data.token, resultSubmitProfilePicture.id, data.imageData));

    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, resultSubmitImage));

    expect(generator.next(resultSubmitImage).value)
      .toEqual(put({type: 'PROFILE_PICTURE_UPDATED', profilePicture: resultSubmitImage.image, id: data.id}));
  });

  it('should reset user password', () => {
    let data = {
      nickname: 'odi',
    };
    let generator = resetUserPassword(data);
    expect(generator.next(data).value)
      .toEqual(call(API.resetPassword, data.nickname));

    expect(generator.next(data).value)
      .toEqual(put({type: 'SHOW_RESET_PASSWORD_CONFIRMATION'}));
  });

  it('should fetch getLatestWeeklyCheckin', () => {
    let todaysDate = new Date('January 25, 2017 11:13:00'); // Wednesday
    let data = {
      token: 'userToken',
      todaysDate,
    };
    let resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: '2017-01-22T15:31:20Z', // Friday
      latest_checkin_id: '58877346e555c070fd000003',
    };
    let generator = getLatestWeeklyCheckin(data);
    let formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));
  });

  it('should not show weekly dialog if today is not in the default weekly checkin day', () => {
    let todaysDate = new Date('January 25, 2017 11:13:00'); // Wednesday
    let data = {
      token: 'userToken',
      todaysDate,
    };
    let resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: '2017-01-22T15:31:20Z', // Friday
      latest_checkin_id: '58877346e555c070fd000003',
    };
    let generator = getLatestWeeklyCheckin(data);
    let formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));

    expect(generator.next(formated).value)
      .toEqual(null);
  });

  it('should show weekly checkin dialog if today is in default weekly checkin days', () => {
    let todaysDate = new Date('January 29, 2017 11:13:00'); // Sunday
    let data = {
      token: 'userToken',
      todaysDate,
    };
    let resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: '2017-01-22T15:31:20Z', // Friday
      latest_checkin_id: '58877346e555c070fd000003',
    };
    let generator = getLatestWeeklyCheckin(data);
    let formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SHOW_WEEKLY_CHECKIN_DIALOG'}));

    todaysDate = new Date('January 28, 2017 11:13:00'); // Saturday
    data = {
      token: 'userToken',
      todaysDate,
    };
    resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: null,
      latest_checkin_id: null,
    };
    generator = getLatestWeeklyCheckin(data);
    formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SHOW_WEEKLY_CHECKIN_DIALOG'}));
  });

  it('should not show weekly checkin dialog if already check-in in the same week even though todays date is in weekly checkin day', () => {
    let todaysDate = new Date('January 29, 2017 11:13:00'); // Sunday
    let data = {
      token: 'userToken',
      todaysDate,
    };
    let resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: 'January 28, 2017 07:13:00', // Saturday
      latest_checkin_id: '58877346e555c070fd000003',
    };
    let generator = getLatestWeeklyCheckin(data);
    let formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));
    expect(generator.next(formated).value)
      .toEqual(null);

    todaysDate = new Date('January 30, 2017 11:13:00'); // Monday
    data = {
      token: 'userToken',
      todaysDate,
    };
    resultJSON = {
      success: true,
      meals: 0,
      activities: 0,
      habits: 0,
      lessons: 0,
      total: 0,
      latest_checkin_date: 'January 29, 2017 07:13:00', // Sunday
      latest_checkin_id: '58877346e555c070fd000003',
    };
    generator = getLatestWeeklyCheckin(data);
    formated = formatToCamelCase(resultJSON);
    expect(generator.next(data).value)
      .toEqual(call(API.getLatestWeeklyCheckin, data.token));
    expect(generator.next(formated).value)
      .toEqual(call(formatToCamelCase, formated));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'WEEKLY_CHECKIN_STATUS_RECEIVED', weeklyCheckinStatus: {...formated}}));
    expect(generator.next(formated).value)
      .toEqual(null);
  });
  it('should fetch postWeeklyCheckin with correct args', () => {
    let data = {
      token: 'userToken',
      id: 'tempID',
      privacy: 'community',
      checkinReview: 'checkinReview',
      checkinPlan: 'checkinPlan',
      checkinWeight: '',
      checkinWaist: '',
    };
    let resultJSON = {
      ...nestedPostData[0],
    };
    let generator = postWeeklyCheckin(data);
    let normalized = normalizePost(resultJSON);
    let formated = formatToCamelCase(normalized);
    let {postList} = formated.entities;

    expect(generator.next(data).value)
      .toEqual(call(API.postWeeklyCheckin, data.token, data.privacy, data.checkinReview, data.checkinPlan, data.checkinWeight, data.checkinWaist));
    expect(generator.next(resultJSON).value)
      .toEqual(call(normalizePost, resultJSON));
    expect(generator.next(normalized).value)
      .toEqual(call(formatToCamelCase, normalized));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'SEND_POST_SUCCESS', optimisticID: 'tempID', post: postList[formated.result]}));


  });

});
