const {describe, it} = global;
import expect from 'expect';
import {call, put} from 'redux-saga/effects';

import {fetchOrientationContent, completeOrientation, submitOrientationAnswer} from '../orientationContentSaga';
import formatToCamelCase from '../../helpers/formatToCamelCase';

import API from '../../API/ContentAPI';

describe('orientationContentSaga test', () => {
  it('should fetch orientation content based on token and content id', () => {
    let nestedObject = {
      id: '570776fa05dd48efa3000005',
      rendered_body: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/orientation/FA_Fulfilled+Orientation+Page-01.png' /></div><p>Selamat bergabung di dalam keluarga besar Fulfilled!</p><p>Kami berkomitmen untuk membantumu menjadi versi terbaik dari dirimu.</p><p>Siapkah kamu?</p>`,
      rendered_title: 'Selamat Datang! ',
    };
    let data = {token: 'userToken', contentID: '570776fa05dd48efa3000005'};
    let formated = formatToCamelCase(nestedObject);

    let generator = fetchOrientationContent(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchContent, 'userToken', '570776fa05dd48efa3000005'));
    expect(generator.next(nestedObject).value)
      .toEqual(call(formatToCamelCase, nestedObject));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'ORIENTATION_CONTENT_RECEIVED', orientationContent: formated}));
  });
  it('should throw put FETCH_ORIENTATION_CONTENT_FAILED when fetch orientation content throws', () => {
    let nestedObject = {
      id: '570776fa05dd48efa3000005',
      rendered_body: `<div style='text-align: center; padding-bottom: 12px'><img src='https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/orientation/FA_Fulfilled+Orientation+Page-01.png' /></div><p>Selamat bergabung di dalam keluarga besar Fulfilled!</p><p>Kami berkomitmen untuk membantumu menjadi versi terbaik dari dirimu.</p><p>Siapkah kamu?</p>`,
      rendered_title: 'Selamat Datang! ',
    };
    let data = {token: 'userToken', contentID: '570776fa05dd48efa3000005'};
    let generator = fetchOrientationContent(data);
    expect(generator.next(nestedObject).value)
      .toEqual(call(API.fetchContent, 'userToken', '570776fa05dd48efa3000005'));
    expect(generator.throw('something').value)
      .toEqual(put({type: 'FETCH_ORIENTATION_CONTENT_FAILED', error: 'something'}));
  });

  it('should complete orientation based on token', () => {
    let nestedObject = {
      success: true,
    };
    let data = {token: 'userToken'};
    let formated = formatToCamelCase(nestedObject);

    let generator = completeOrientation(data);
    expect(generator.next(data).value)
      .toEqual(call(API.completeOrientation, 'userToken'));
    expect(generator.next(formated).value)
      .toEqual(put({type: 'COMPLETE_ORIENTATION_SUCCESS'}));
  });

  it('should submit orientation answer', () => {
    let returnObject = {
      success: true,
    };
    let data = {token: 'userToken', formData: 'formData', userID: 'userID'};
    let generator = submitOrientationAnswer(data);
    expect(generator.next(data).value)
      .toEqual(call(API.submitOrientationAnswer, 'userToken', 'userID', 'formData'));
    expect(generator.next(returnObject).value)
      .toEqual(put({type: 'SUBMIT_ORIENTATION_ANSWER_SUCCESS'}));
  });

});
