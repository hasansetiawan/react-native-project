// @flow

import {put, call} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';
import {normalizePostList, normalizePost} from '../normalizers/normalizePosts';
import normalizeComment from '../normalizers/normalizeComment';
import formatToCamelCase from '../helpers/formatToCamelCase';

import API from '../API/PostAPI';

import type {
  FetchPostsAction,
  LikePostAction,
  CommentPostAction,
  CreateNotePostAction,
  ActivityPostAction,
  MealPostAction,
  MeasurementPostAction,
  DrinkPostAction,
  PostImageAction,
} from '../types/SagaAction';

export function* fetchPostList(action: FetchPostsAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.fetchPostList, action.token, action.page, (action.lastPostID) ? action.lastPostID : null, (action.postType) ? action.postType : null);
    if (result != null) {
      normalizedResult = yield call(normalizePostList, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList, userList, commentList} = camelCasedResult.entities;

      for (let id of Object.keys(userList)) {
        let member = userList[id];
        yield put({type: 'FETCH_PROFILE_PICTURE_REQUESTED', token: action.token, userID: member.id});
      }

      if (action.refresh === true) {
        yield put({type: 'POSTS_RECEIVED', postList: Object.values(postList)});
      } else {
        yield put({type: 'POSTS_RECEIVED', postList: Object.values(postList), page: action.page});
      }
      yield put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)});
      yield put({type: 'COMMENT_LIST_RECEIVED', commentList: Object.values(commentList)});
    }
  } catch (err) {
    yield put({type: 'FETCH_POSTS_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: ['communityFeedPage', 'communityFeedRefresh'],
  });
}

export function* likePost(action: LikePostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.likePost, action.token, action.postID);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList, userList, commentList} = camelCasedResult.entities;
      yield put({type: 'POSTS_RECEIVED', postList: Object.values(postList)});
      if (userList != null) {
        yield put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)});
      }
      if (commentList != null) {
        yield put({type: 'COMMENTS_RECEIVED', commentList: Object.values(commentList)});
      }
    }
  } catch (err) {
    yield put({type: 'LIKE_POST_FAILED', error: err});
  }
}

export function* commentPost(action: CommentPostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.commentPost, action.token, action.text, action.postID);
    if (result != null) {
      normalizedResult = yield call(normalizeComment, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {commentList} = camelCasedResult.entities;
      yield put({type: 'ADD_COMMENT_SUCCESS', optimisticID, comment: commentList[camelCasedResult.result]});
    }
  } catch (err) {
    yield put({type: 'SEND_COMMENT_FAILED', error: err});
  }
}

export function* createNotePost(action: CreateNotePostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitNotePost, action.token, action.text, action.privacy);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      if (action.imageData != null) {
        yield put({type: 'POST_IMAGE_SUBMITTED', token: action.token, postID: post.id, imageData: action.imageData});
      }
    }
  } catch (err) {
    yield put({type: 'SEND_POST_FAILED', error: err});
  }
}

export function* createActivityPost(action: ActivityPostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitActivityPost, action.token, action.privacy, action.activityDescription, action.activityDurationHours, action.activityDurationMinutes, action.activityComment);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      if (action.imageData != null) {
        yield put({type: 'POST_IMAGE_SUBMITTED', token: action.token, postID: post.id, imageData: action.imageData});
      }
    }
  } catch (err) {
    yield put({type: 'SEND_POST_FAILED', error: err});
  }
}

export function* createMealPost(action: MealPostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitMealPost, action.token, action.privacy, action.mealDescription, action.mealPeriod);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      if (action.imageData != null) {
        yield put({type: 'POST_IMAGE_SUBMITTED', token: action.token, postID: post.id, imageData: action.imageData});
      }
    }
  } catch (err) {
    yield put({type: 'SEND_POST_FAILED', error: err});
  }
}

export function* createMeasurementPost(action: MeasurementPostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitMeasurementPost, action.token, action.text, action.privacy, action.weight, action.waist, action.hip);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      if (action.imageData != null) {
        yield put({type: 'POST_IMAGE_SUBMITTED', token: action.token, postID: post.id, imageData: action.imageData});
      }
    }
  } catch (err) {
    yield put({type: 'SEND_POST_FAILED', error: err});
  }
}

export function* createDrinkPost(action: DrinkPostAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitDrinkPost, action.token, action.privacy, action.drinkDescription, action.drinkSize);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      if (action.imageData != null) {
        yield put({type: 'POST_IMAGE_SUBMITTED', token: action.token, postID: post.id, imageData: action.imageData});
      }
    }
  } catch (err) {
    yield put({type: 'UPLOAD_IMAGE_FAILED', error: err});
  }
}

export function* submitPostImage(action: PostImageAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.submitImage, action.token, action.postID, action.imageData);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      yield put({type: 'SEND_POST_SUCCESS', optimisticID: action.postID, post: postList[camelCasedResult.result]});
    }
  } catch (err) {
    yield put({type: 'SEND_POST_FAILED', error: err});
  }
}


export function* watchPostSaga(): Generator<any, any, any> {
  yield takeLatest('FETCH_POSTS_REQUESTED', fetchPostList);
  yield takeEvery('LIKE_ADDED', likePost);
  yield takeEvery('SEND_COMMENT_REQUESTED', commentPost);
  yield takeEvery('SEND_NOTE_POST_REQUESTED', createNotePost);
  yield takeEvery('SEND_ACTIVITY_POST_REQUESTED', createActivityPost);
  yield takeEvery('SEND_MEAL_POST_REQUESTED', createMealPost);
  yield takeEvery('SEND_MEASUREMENT_POST_REQUESTED', createMeasurementPost);
  yield takeEvery('SEND_DRINK_POST_REQUESTED', createDrinkPost);
  yield takeEvery('POST_IMAGE_SUBMITTED', submitPostImage);
}
