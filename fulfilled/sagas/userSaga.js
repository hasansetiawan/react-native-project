// @flow

import {Platform} from 'react-native';
import {put, call} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';
import normalizeSyncData from '../normalizers/normalizeSyncData';
import normalizeLoginData from '../normalizers/normalizeLoginData';
import normalizeProgramHistory from '../normalizers/normalizeProgramHistory';
import {normalizePostList, normalizePost} from '../normalizers/normalizePosts';

import formatToCamelCase from '../helpers/formatToCamelCase';

import getLanguages from '../helpers/getLanguages';
import isWeekend from '../helpers/isWeekend';
import isAlreadyWeeklyCheckedIn from '../helpers/isAlreadyWeeklyCheckedIn';

import API from '../API/UserAPI';
import PostAPI from '../API/PostAPI';

import type {
  SyncUserAction,
  UserProgramHistoryAction,
  UserPostHistoryAction,
  SignUpAction,
  SubmitProfilePictureAction,
  FetchProfilePictureAction,
  ResetUserPasswordAction,
  GetWeeklyCheckinAction,
  PostWeeklyCheckinAction,
} from '../types/SagaAction';

const languages = getLanguages();

export function* syncUser(action: SyncUserAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.syncUser, action.userID, action.token);
    if (result != null) {
      normalizedResult = yield call(normalizeSyncData, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {
        userList,
        coachList,
        programList,
        programRecordList,
        habitList,
        currentHabitRecordList,
      } = camelCasedResult.entities;
      let user = userList[camelCasedResult.result];
      let coach = coachList[user.coach];

      if (user.programRecords.length < 1 && user.completedProgramIds.length < 1) {
        // if the new user hasn't assigned to a program, re-fetch again
        yield put({
          type: 'SYNC_USER_REQUESTED',
          token: action.token,
          userID: action.userID,
        });
        return;
      }

      yield put({
        type: 'LOADING_STARTED',
        loadingPages: ['challengePage', 'communityFeedPage', 'chatPage', 'notificationPage'],
      });

      // if the user hasn't finish orientation
      if (user.orientationCompleted === false) {
        let {orientationContentIds} = coach;
        coach['orientationContentIDs'] = orientationContentIds;
        delete coach['orientationContentIds'];

        for (let id of orientationContentIds) {
          yield put({type: 'FETCH_ORIENTATION_CONTENT_REQUESTED', token: action.token, contentID: id});
        }
      }

      // if the user already finish a program and haven't assign to another program yet, the program records become empty array
      if (user.programRecords.length > 0) {
        let programRecord = programRecordList[user.programRecords[0]];
        let currentProgram = programList[programRecord.program];
        let currentHabit = habitList[currentHabitRecordList[programRecord.currentHabitRecord].habit];
        yield put({type: 'FETCH_HABIT_REQUESTED', token: action.token, programRecordID: user.programRecords[0]});

        yield put({
          type: 'PROGRAM_RECORD_RECEIVED',
          programRecordID: user.programRecords[0],
          currentProgram: {
            title: currentProgram.title,
            id: currentProgram.id,
          },
        });

        yield put({
          type: 'CURRENT_HABIT_RECEIVED',
          habit: currentHabit,
        });
      } else {
        yield put({
          type: 'LOADING_FINISHED',
          loadingPages: 'challengePage',
        });
      }
      yield put({
        type: 'FETCH_PROFILE_PICTURE_REQUESTED',
        token: action.token,
        userID: user.id,
      });

      yield put({
        type: 'FETCH_PROFILE_PICTURE_REQUESTED',
        token: action.token,
        userID: coach.id,
      });

      yield put({type: 'FETCH_CHAT_LIST_REQUESTED', token: action.token, page: 1, recipient: coach.id});

      yield put({
        type: 'FETCH_POSTS_REQUESTED',
        token: action.token,
        page: 1,
      });
      yield put({
        type: 'FETCH_NOTIF_LIST_REQUESTED',
        token: action.token,
        page: 1,
      });

      let newMemberList = [
        {
          id: user.id,
          nickname: user.nickname,
          isMentor: user.isMentor,
          isCoach: user.isCoach,
          isDietitian: user.isDietitian,
          isNutritionist: user.isNutritionist,
        },
      ];
      yield put({
        type: 'LOADING_FINISHED',
        loadingPages: 'signUpPage',
      });
      yield put({
        type: 'ROUTE_POPPED',
      });
      yield put({type: 'USER_RECEIVED', user});
      yield put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: newMemberList});
      yield put({type: 'COACH_RECEIVED', coach});
      yield put({
        type: 'LOADING_STARTED',
        loadingPage: 'coachProfile',
      });
      yield put({
        type: 'FETCH_CONTENT_REQUESTED',
        token: action.token,
        contentID: coach.coachIntroductionID,
      });
      yield put({
        type: 'GET_PRODUCT_ID_LIST_REQUESTED',
        platform: Platform.OS,
      });
      yield put({
        type: 'SYNC_USER_FINISHED',
      });
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}

export function* fetchUserProgramHistory(action: UserProgramHistoryAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.fetchUserProgramHistory, action.token);
    if (result != null) {
      normalizedResult = yield call(normalizeProgramHistory, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {programList} = camelCasedResult.entities;
      yield put({type: 'USER_PROGRAM_HISTORY_RECEIVED', programHistoryList: Object.values(programList)});
    }
  } catch (err) {
    yield put({type: 'FETCH_USER_PROGRAM_HISTORY_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'programHistoryPage',
  });
}

export function* fetchUserPostHistory(action: UserPostHistoryAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.fetchUserPostHistory, action.token, action.page, action.userID, action.lastPostID);
    if (result != null) {
      normalizedResult = yield call(normalizePostList, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList, commentList, userList} = camelCasedResult.entities;
      if (action.refresh === true) {
        yield put({type: 'USER_POST_HISTORY_RECEIVED', postList: Object.values(postList), userID: action.userID});
      } else {
        yield put({type: 'USER_POST_HISTORY_RECEIVED', postList: Object.values(postList), userID: action.userID, page: action.page});
      }
      yield put({type: 'COMMENT_LIST_RECEIVED', commentList: Object.values(commentList)});
      yield put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)});
    }
  } catch (err) {
    yield put({type: 'FETCH_USER_PROGRAM_HISTORY_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'postHistoryPage',
  });
  yield put({
    type: 'FETCH_MORE_USER_POST_HISTORY_END',
  });
}


export function* signUp(action: SignUpAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.signUp, action.fullName, action.nickname, action.email, action.password, action.parseID);
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      normalizedResult = yield call(normalizeLoginData, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {userList, coachList} = camelCasedResult.entities;
      let user = userList[camelCasedResult.result];
      let coach = coachList[user.coach];
      if (user != null) {
        yield put({
          type: 'SYNC_USER_REQUESTED',
          userID: user.id,
          token: user.token,
        });
        yield put({
          type: 'SAVE_LOGIN_REQUESTED',
          userID: user.id,
          token: user.token,
        });
        yield put({type: 'USER_RECEIVED', user});
        yield put({type: 'COACH_RECEIVED', coach});
        yield put({
          type: 'DRAWER_CLOSED',
        });
      }
    }
  } catch (err) {
    yield put({type: 'LOADING_FINISHED', loadingPages: 'signUpPage'});
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}

export function* submitProfilePicture(action: SubmitProfilePictureAction): any {
  let result;
  let camelCasedResult;
  let message = languages[action.language]['changedProfilePicture'];
  try {
    result = yield call(API.submitProfilePicture, action.token, `${action.name} ${message}`, 'community');
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      let addImageResult = yield call(PostAPI.submitImage, action.token, result.id, action.imageData);
      if (addImageResult != null) {
        camelCasedResult = yield call(formatToCamelCase, addImageResult);
      }
      if (camelCasedResult != null) {
        yield put({
          type: 'PROFILE_PICTURE_UPDATED',
          profilePicture: camelCasedResult.image,
          id: action.id,
        });
        yield put({
          type: 'ROUTE_POPPED',
        });
      }
    }
  } catch (err) {
    yield put({type: 'CHANGE_PROFILE_PICTURE_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'changeProfilePicturePage',
  });
}

export function* fetchUserProfilePicture(action: FetchProfilePictureAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.fetchUserProfilePicture, action.userID, action.token);
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      camelCasedResult = yield call(formatToCamelCase, result);
      if (camelCasedResult != null) {
        yield put({
          type: 'PROFILE_PICTURE_RECEIVED',
          profilePicture: camelCasedResult,
          userID: action.userID,
        });
      }
    }
  } catch (err) {
    yield put({type: 'FETCH_PROFILE_PICTURE_FAILED', error: err});
  }
}

export function* resetUserPassword(action: ResetUserPasswordAction): any {
  let result;
  try {
    result = yield call(API.resetPassword, action.nickname);
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      if (result.success === 'false') {
        throw new Error('no nickname or email found');
      }
      yield put({
        type: 'SHOW_RESET_PASSWORD_CONFIRMATION',
      });
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'resetPasswordPage',
  });
}

export function* getLatestWeeklyCheckin(action: GetWeeklyCheckinAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.getLatestWeeklyCheckin, action.token);
    if (result != null) {
      if (result.errors != null) {
        throw new Error(result.errors[0]);
      }
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      yield put({
        type: 'WEEKLY_CHECKIN_STATUS_RECEIVED',
        weeklyCheckinStatus: {...camelCasedResult},
      });
      let {todaysDate} = action;
      if (camelCasedResult.latestCheckinDate == null && isWeekend(todaysDate)) {
        yield put({
          type: 'SHOW_WEEKLY_CHECKIN_DIALOG',
        });
      } else {
        let latestCheckinDate = new Date(camelCasedResult.latestCheckinDate);
        if (isWeekend(todaysDate) && !isAlreadyWeeklyCheckedIn(todaysDate, latestCheckinDate)) {
          yield put({
            type: 'SHOW_WEEKLY_CHECKIN_DIALOG',
          });
        }
      }
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}

export function* postWeeklyCheckin(action: PostWeeklyCheckinAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.postWeeklyCheckin, action.token, action.privacy, action.checkinReview, action.checkinPlan, action.checkinWeight, action.checkinWaist);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList} = camelCasedResult.entities;
      let post = postList[camelCasedResult.result];
      yield put({type: 'SEND_POST_SUCCESS', optimisticID, post});
      yield put({type: 'SHOW_POPUP', content: 'weeklyCheckinSuccess'});
      yield put({type: 'HIDE_WEEKLY_CHECKIN_DIALOG'});
      yield put({type: 'ROUTE_POPPED'});
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'weeklyCheckinForm',
  });
}



export function* watchUserSaga(): Generator<any, any, any> {
  yield takeEvery('SYNC_USER_REQUESTED', syncUser);
  yield takeLatest('USER_PROGRAM_HISTORY_REQUESTED', fetchUserProgramHistory);
  yield takeLatest('USER_POST_HISTORY_REQUESTED', fetchUserPostHistory);
  yield takeLatest('SIGN_UP_REQUESTED', signUp);
  yield takeLatest('CHANGE_PROFILE_PICTURE_REQUESTED', submitProfilePicture);
  yield takeEvery('FETCH_PROFILE_PICTURE_REQUESTED', fetchUserProfilePicture);
  yield takeLatest('RESET_PASSWORD_REQUESTED', resetUserPassword);
  yield takeLatest('WEEKLY_CHECKIN_REQUESTED', getLatestWeeklyCheckin);
  yield takeLatest('POST_WEEKLY_CHECKIN_REQUESTED', postWeeklyCheckin);
}
