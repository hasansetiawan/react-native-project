// @flow
import {put, call} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';

import API from '../API/PaymentAPI';

import type {
  CheckSubscribeStatusAction,
  RegisterSubscriptionAction,
  LodgeSubscriptionSupportAction,
  GetProductIDAction,
} from '../types/SagaAction';

export function* checkSubscribeStatus(action: CheckSubscribeStatusAction): any {
  let result;
  try {
    result = yield call(API.checkSubscription, action.token);
    if (result != null) {
      yield put({type: 'SUBSCRIBE_STATUS_RECEIVED', subscribed: result.subscribed, expiry: result.expiry});
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'subscribe',
  });
}

export function* registerSubscription(action: RegisterSubscriptionAction): any {
  let result;
  try {
    if (action.platform !== 'ios' && action.platform !== 'android') {
      throw new Error('platform not found');
    }
    if (action.platform === 'ios' && action.purchaseReceipts != null) {
      result = yield call(API.subscribeIOS, action.token, action.purchaseReceipts);
    } else if (
      action.platform === 'android' &&
      action.subscriptionToken != null &&
      action.subscriptionReceipt != null &&
      action.subscriptionSignature != null
    ) {
      result = yield call(API.subscribeAndroid, action.token, action.subscriptionToken, action.subscriptionReceipt, action.subscriptionSignature);
    }

    if (result != null) {
      if (result.success === true) {
        yield put({type: 'SUBSCRIBE_STATUS_RECEIVED', subscribed: result.subscribed, expiry: result.expiry});
        yield put({
          type: 'ROUTE_POPPED',
        });
        yield put({
          type: 'SHOW_POPUP',
          content: 'subscriptionSuccess',
        });
      } else {
        yield put({
          type: 'SHOW_POPUP',
          content: 'subscriptionFailed',
        });
      }
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'registerSubscription',
  });
}

export function* lodgeSubscriptionSupport(action: LodgeSubscriptionSupportAction): any {
  let result;
  try {
    if (action.platform !== 'ios' && action.platform !== 'android') {
      throw new Error('platform not found');
    }
    if (action.platform === 'ios' && action.purchaseReceipts != null) {
      result = yield call(API.lodgeSubscriptionSupportIOS, action.token, action.purchaseReceipts, action.diagnosticData);
    } else if (
      action.platform === 'android' &&
      action.subscriptionToken != null &&
      action.subscriptionReceipt != null &&
      action.subscriptionSignature != null
    ) {
      result = yield call(API.lodgeSubscriptionSupportAndroid, action.token, action.subscriptionToken, action.subscriptionReceipt, action.subscriptionSignature, action.diagnosticData);
    }
    if (result != null) {
      if (result.success === true) {
        yield put({type: 'SHOW_TOAST', toastMessage: `success, case id : ${result.case_id}`});
      }
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}

export function* getProductID(action: GetProductIDAction): any {
  let result;
  try {
    let {platform} = action;
    if (platform === 'android') {
      result = yield call(API.getProductIDAndroid);
    } else if (platform === 'ios') {
      result = yield call(API.getProductIDIOS);
    } else {
      throw new Error('no platform found');
    }
    if (result != null) {
      yield put({type: 'PRODUCT_ID_LIST_RECEIVED', productIDList: result.subscriptions});
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}

export function* watchPaymentSaga(): Generator<any, any, any> {
  yield takeLatest('SUBSCRIBE_STATUS_REQUESTED', checkSubscribeStatus);
  yield takeLatest('REGISTER_SUBSCRIPTION_REQUESTED', registerSubscription);
  yield takeEvery('LODGE_SUBSCRIPTION_SUPPORT_REQUESTED', lodgeSubscriptionSupport);
  yield takeLatest('GET_PRODUCT_ID_LIST_REQUESTED', getProductID);
}
