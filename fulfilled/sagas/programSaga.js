// @flow
import {call, put} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import formatToCamelCase from '../helpers/formatToCamelCase';
import normalizeProgram from '../normalizers/normalizeProgram';
import normalizeProgramRecord from '../normalizers/normalizeProgramRecord';

import type {
  ProgramListAction,
  AssignProgramAction,
  FetchProgramRecordAction,
} from '../types/SagaAction';

import API from '../API/ProgramAPI';

export function* fetchProgramList(action: ProgramListAction): any {
  let result;
  let camelCasedResult;
  let normalizeResult;
  try {
    result = yield call(API.fetchProgramList, action.token);
    if (result != null) {
      normalizeResult = yield call(normalizeProgram, result);
    }
    if (normalizeResult != null) {
      camelCasedResult = yield call(formatToCamelCase, normalizeResult);
    }
    if (camelCasedResult != null) {
      let {programList, habitList} = camelCasedResult.entities;
      yield put({type: 'PROGRAM_LIST_RECEIVED', programList: Object.values(programList), habitList: Object.values(habitList)});
    }
  } catch (err) {
    yield put({type: 'FETCH_PROGRAM_LIST_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'programListPage',
  });
}

export function* assignProgram(action: AssignProgramAction): any {
  let result;
  try {
    result = yield call(API.assignProgram, action.token, action.programID);
    if (result != null) {
      let {success} = result;
      if (success === true) {
        yield put({type: 'PROGRAM_RECORD_REQUESTED', token: action.token});
        yield put({type: 'LOADING_STARTED', loadingPages: 'challengePage'});
        yield put({type: 'SHOW_POPUP', content: 'assignProgramSuccess'});
      } else {
        yield put({type: 'SHOW_POPUP', content: result.message});
      }
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'selectedProgramPage',
  });
}

export function* fetchProgramRecord(action: FetchProgramRecordAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.fetchProgramRecord, action.token);
    if (result != null) {
      normalizedResult = yield call(normalizeProgramRecord, result);
    }
    if (normalizedResult != null) {
      camelCasedResult = yield call(formatToCamelCase, normalizedResult);
    }
    if (camelCasedResult != null) {
      let {
        programList,
        programRecordList,
        habitList,
        currentHabitRecordList,
      } = camelCasedResult.entities;

      let programRecord = programRecordList[camelCasedResult.result[0]];
      let currentProgram = programList[programRecord.program];
      let currentHabit = habitList[currentHabitRecordList[programRecord.currentHabitRecord].habit];
      yield put({type: 'FETCH_HABIT_REQUESTED', token: action.token, programRecordID: camelCasedResult.result[0]});

      yield put({
        type: 'PROGRAM_RECORD_RECEIVED',
        programRecordID: camelCasedResult.result[0],
        currentProgram: {
          title: currentProgram.title,
          id: currentProgram.id,
        },
      });

      yield put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: currentHabit,
      });
    }
  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
}


export function* watchProgramSaga(): Generator<any, any, any> {
  yield takeLatest('FETCH_PROGRAM_LIST_REQUESTED', fetchProgramList);
  yield takeLatest('ASSIGN_PROGRAM_REQUESTED', assignProgram);
  yield takeLatest('PROGRAM_RECORD_REQUESTED', fetchProgramRecord);
}
