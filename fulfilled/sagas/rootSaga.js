// @flow

import {fork} from 'redux-saga/effects';
import {watchLoginSaga} from './loginSaga';
import {watchUserSaga} from './userSaga';
import {watchChatSaga} from './chatSaga';
import {watchPostSaga} from './postSaga';
import {watchHabitSaga} from './habitSaga';
import {watchNotifSaga} from './notificationSaga';
import {watchProgramSaga} from './programSaga';
import {watchOrientationContentSaga} from './orientationContentSaga';
import {watchContentSaga} from './contentSaga';
import {watchPaymentSaga} from './paymentSaga';

function* rootSaga(): Generator<any, any, any> {
  yield fork(watchLoginSaga);
  yield fork(watchUserSaga);
  yield fork(watchChatSaga);
  yield fork(watchPostSaga);
  yield fork(watchHabitSaga);
  yield fork(watchNotifSaga);
  yield fork(watchProgramSaga);
  yield fork(watchOrientationContentSaga);
  yield fork(watchContentSaga);
  yield fork(watchPaymentSaga);
}

export default rootSaga;
