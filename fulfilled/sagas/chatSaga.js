// @flow

import {put, call} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import formatToCamelCase from '../helpers/formatToCamelCase';

import API from '../API/ChatAPI';

import type {ChatListAction, SendChatAction} from '../types/SagaAction';

export function* fetchChatList(action: ChatListAction): any {
  let result;
  let camelCasedResult;
  try {
    // TODO: temporary solution. Find why sometimes recipient is null
    if (action.recipient) {
      result = yield call(API.fetchChatList, action.token, action.page, action.recipient);
      if (result != null) {
        camelCasedResult = yield call(formatToCamelCase, result);
      }
      if (camelCasedResult != null) {
        if (action.isRefreshingList) {
          yield put({type: 'CHAT_LIST_RECEIVED', chatList: camelCasedResult});
        } else {
          yield put({type: 'CHAT_LIST_RECEIVED', chatList: camelCasedResult, page: action.page});
        }
      }
    } else {
      throw new Error('recipientIDNotFound');
    }

  } catch (err) {
    yield put({type: 'SHOW_TOAST', toastMessage: err.message});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'chatPage',
  });
}

export function* sendChat(action: SendChatAction): any {
  let result;
  let camelCasedResult;
  let optimisticID = action.id;
  try {
    result = yield call(API.submitChat, action.token, action.text, action.recipientNickname);
    if (result != null) {
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      yield put({type: 'ADD_CHAT_SUCCESS', optimisticID, newChat: camelCasedResult});
      yield put({type: 'CHAT_WORD_COUNT_UPDATED', wordCountLeft: camelCasedResult.wordCountLeft});
      yield put({type: 'SHOW_WORD_COUNT_STATUS'});
    }
  } catch (err) {
    yield put({type: 'SEND_CHAT_FAILED', error: err});
  }
}

export function* watchChatSaga(): Generator<any, any, any> {
  yield takeLatest('FETCH_CHAT_LIST_REQUESTED', fetchChatList);
  yield takeLatest('SEND_CHAT_REQUESTED', sendChat);
}
