// @flow

import {put, call} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';
import formatToCamelCase from '../helpers/formatToCamelCase';
import {normalizeDailyTips, normalizeDailyTip} from '../normalizers/normalizeDailyTips';
import API from '../API/HabitAPI';

import type {
  HabitDailyTipsAction,
  FinishHabitActivityAction,
  FinishHabitAction,
} from '../types/SagaAction';

export function* fetchHabitDailyTipsList(action: HabitDailyTipsAction): any {
  let result;
  let dailyTipsResult;
  let habitResult;

  let camelCasedDailyTipsResult;
  let camelCasedHabitResult;

  let normalizedDailyTipsResult;
  try {
    result = yield call(API.fetchHabitDailyTipsList, action.token, action.programRecordID);
    if (result != null) {
      if (Array.isArray(result)) {
        dailyTipsResult = result.slice(1);
        habitResult = result.slice(0, 1);
        if (habitResult.length && habitResult[0].activity_todo_id == null) {
          yield put({type: 'FETCH_HABIT_REQUESTED', token: action.token, programRecordID: action.programRecordID});
          return;
        }

        normalizedDailyTipsResult = yield call(normalizeDailyTips, dailyTipsResult);
        if (normalizedDailyTipsResult != null) {
          camelCasedDailyTipsResult = yield call(formatToCamelCase, normalizedDailyTipsResult);
        }

        camelCasedHabitResult = yield call(formatToCamelCase, habitResult);
      }
    }
    if (camelCasedDailyTipsResult != null && camelCasedHabitResult != null) {
      let currentHabit = Object.values(camelCasedHabitResult)[0];
      if (currentHabit.contentCheckoffID) {
        yield put({
          type: 'FETCH_CONTENT_REQUESTED',
          token: action.token,
          contentID: currentHabit.contentCheckoffID,
        });
      }

      if (currentHabit.habitCompletionMessageID) {
        yield put({
          type: 'FETCH_CONTENT_REQUESTED',
          token: action.token,
          contentID: currentHabit.habitCompletionMessageID,
        });
      }

      let {dailyTipsList, readingMaterialList} = camelCasedDailyTipsResult.entities;
      yield put({
        type: 'READING_MATERIAL_LIST_RECEIVED',
        readingMaterialList: Object.values(readingMaterialList),
      });
      yield put({
        type: 'HABIT_ACTIVITY_LIST_RECEIVED',
        habitActivityList: Object.values(dailyTipsList),
      });
      yield put({
        type: 'CURRENT_HABIT_RECEIVED',
        habit: currentHabit,
      });
    }
  } catch (err) {
    yield put({type: 'FETCH_HABIT_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: ['challengePage', 'completeHabitActivity'],
  });
}

export function* completeHabitActivity(action: FinishHabitActivityAction): any {
  let result;
  let camelCasedResult;
  let normalizedResult;
  try {
    result = yield call(API.completeDailyTips, action.token, action.userID, action.habitActivityID, action.formData);
    if (result != null) {
      normalizedResult = yield call(normalizeDailyTip, result);
    }
    if (normalizedResult != null) {
      camelCasedResult = yield call(formatToCamelCase, normalizedResult);
    }
    if (camelCasedResult != null) {
      let {dailyTipsList, readingMaterialList} = camelCasedResult.entities;
      yield put({
        type: 'HABIT_ACTIVITY_LIST_RECEIVED',
        habitActivityList: Object.values(dailyTipsList),
      });
      yield put({
        type: 'READING_MATERIAL_LIST_RECEIVED',
        readingMaterialList: Object.values(readingMaterialList),
      });
    }
  } catch (err) {
    yield put({type: 'FINISH_HABIT_ACTIVITY_FAILED', error: err});
  }
}

export function* completeHabit(action: FinishHabitAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.completeHabit, action.token, action.userHabitRecordID, action.habitID);
    if (result != null) {
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      let currentHabit = Object.values(camelCasedResult)[0];
      yield put({type: 'CURRENT_HABIT_RECEIVED', habit: currentHabit});

      if (currentHabit.contentCheckoffID != null) {
        yield put({
          type: 'FETCH_CONTENT_REQUESTED',
          token: action.token,
          contentID: currentHabit.contentCheckoffID,
          showContent: true,
        });
      }

      if (currentHabit.habitCompletionMessageID != null) {
        yield put({
          type: 'FETCH_CONTENT_REQUESTED',
          token: action.token,
          contentID: currentHabit.habitCompletionMessageID,
        });
      }



    }
  } catch (err) {
    yield put({type: 'FINISH_HABIT_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'completeHabitActivity',
  });
}

export function* watchHabitSaga(): Generator<any, any, any> {
  yield takeEvery('FETCH_HABIT_REQUESTED', fetchHabitDailyTipsList);
  yield takeLatest('FINISH_HABIT_ACTIVITY_REQUESTED', completeHabitActivity);
  yield takeLatest('FINISH_HABIT_REQUESTED', completeHabit);
}
