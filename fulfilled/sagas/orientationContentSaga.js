// @flow
import {call, put} from 'redux-saga/effects';
import {takeEvery, takeLatest} from 'redux-saga';
import formatToCamelCase from '../helpers/formatToCamelCase';

import type {
  FetchContentAction,
  CompleteOrientationAction,
  SubmitOrientationAnswerAction,
} from '../types/SagaAction';

import API from '../API/ContentAPI';

export function* fetchOrientationContent(action: FetchContentAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.fetchContent, action.token, action.contentID);
    if (result != null) {
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      yield put({type: 'ORIENTATION_CONTENT_RECEIVED', orientationContent: camelCasedResult});
    }
  } catch (err) {
    yield put({type: 'FETCH_ORIENTATION_CONTENT_FAILED', error: err});
  }
}

export function* completeOrientation(action: CompleteOrientationAction): any {
  let result;
  try {
    result = yield call(API.completeOrientation, action.token);
    if (result != null) {
      if (result.success === true) {
        yield put({type: 'COMPLETE_ORIENTATION_SUCCESS'});
        yield put({
          type: 'LOADING_STARTED',
          loadingPages: ['challengePage', 'communityFeedPage', 'chatPage', 'notificationPage'],
        });
        yield put({
          type: 'SYNC_USER_REQUESTED',
          token: action.token,
          userID: action.userID,
        });
      } else {
        throw new Error('failed to complete orientation');
      }
    } else {
      throw new Error('failed to complete orientation');
    }
  } catch (err) {
    yield put({type: 'COMPLETE_ORIENTATION_FAILED', error: err});
  }
}

export function* submitOrientationAnswer(action: SubmitOrientationAnswerAction): any {
  let result;
  try {
    result = yield call(API.submitOrientationAnswer, action.token, action.userID, action.formData);
    if (result != null) {
      if (result.success === true) {
        yield put({type: 'SUBMIT_ORIENTATION_ANSWER_SUCCESS'});
      } else {
        throw new Error('failed to submit orientation answer');
      }
    } else {
      throw new Error('failed to submit orientation answer');
    }
  } catch (err) {
    yield put({type: 'SUBMIT_ORIENTATION_ANSWER_FAILED', error: err});
  }
}


export function* watchOrientationContentSaga(): Generator<any, any, any> {
  yield takeEvery('FETCH_ORIENTATION_CONTENT_REQUESTED', fetchOrientationContent);
  yield takeLatest('COMPLETE_ORIENTATION_REQUESTED', completeOrientation);
  yield takeLatest('SUBMIT_ORIENTATION_ANSWER_REQUESTED', submitOrientationAnswer);
}
