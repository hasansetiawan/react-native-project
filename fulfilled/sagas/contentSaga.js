// @flow
import {call, put} from 'redux-saga/effects';
import {takeEvery} from 'redux-saga';
import formatToCamelCase from '../helpers/formatToCamelCase';

import type {
  FetchContentAction,
} from '../types/SagaAction';

import API from '../API/ContentAPI';

export function* fetchContent(action: FetchContentAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.fetchContent, action.token, action.contentID);
    if (result != null) {
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      yield put({type: 'CONTENT_RECEIVED', content: camelCasedResult});
      if (action.showContent === true) {
        yield put({type: 'SHOW_COMPLETE_CONTENT'});
      }
    }
  } catch (err) {
    yield put({type: 'FETCH_CONTENT_FAILED', error: err});
  }
  if (action.loadingPage) {
    yield put({type: 'LOADING_FINISHED', loadingPages: [action.loadingPage]});
  }
}

export function* watchContentSaga(): Generator<any, any, any> {
  yield takeEvery('FETCH_CONTENT_REQUESTED', fetchContent);
}
