// @flow

import {put, call} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import {normalizePost} from '../normalizers/normalizePosts';
import formatToCamelCase from '../helpers/formatToCamelCase';

import API from '../API/NotificationAPI';

import type {NotificationListAction, NotificationDetailAction} from '../types/SagaAction';

export function* fetchNotificationList(action: NotificationListAction): any {
  let result;
  let camelCasedResult;
  try {
    result = yield call(API.fetchNotifications, action.token, action.page);
    if (result != null) {
      camelCasedResult = yield call(formatToCamelCase, result);
    }
    if (camelCasedResult != null) {
      if (action.isRefreshingList) {
        yield put({
          type: 'NOTIF_LIST_RECEIVED',
          notifList: Object.values(camelCasedResult),
        });
      } else {
        yield put({
          type: 'NOTIF_LIST_RECEIVED',
          notifList: Object.values(camelCasedResult),
          page: action.page,
        });
      }
    }
  } catch (err) {
    yield put({type: 'FETCH_NOTIF_LIST_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'notificationPage',
  });
  yield put({
    type: 'FETCH_MORE_NOTIFS_FINISHED',
  });

  if (action.isRefreshingList) {
    yield put({
      type: 'LOADING_FINISHED',
      loadingPages: 'notificationRefresh',
    });
  }
}

export function* fetchNotificationDetail(action: NotificationDetailAction): any {
  let result;
  let normalizedResult;
  let camelCasedResult;
  try {
    result = yield call(API.fetchNotificationDetail, action.token, action.postID);
    if (result != null) {
      normalizedResult = yield call(normalizePost, result);
      if (normalizedResult != null) {
        camelCasedResult = yield call(formatToCamelCase, normalizedResult);
      }
    }
    if (camelCasedResult != null) {
      let {postList, userList, commentList} = camelCasedResult.entities;
      yield put({type: 'NOTIF_DETAIL_CHANGED', notificationDetailID: action.postID});
      yield put({type: 'POSTS_RECEIVED', postList: Object.values(postList)});
      yield put({type: 'COMMUNITY_MEMBERS_RECEIVED', memberList: Object.values(userList)});
      if (commentList != null) {
        yield put({type: 'COMMENT_LIST_RECEIVED', commentList: Object.values(commentList)});
      }
    }
  } catch (err) {
    yield put({type: 'FETCH_NOTIFICATION_DETAIL_FAILED', error: err});
  }
  yield put({
    type: 'LOADING_FINISHED',
    loadingPages: 'notificationDetailPage',
  });
}

export function* watchNotifSaga(): Generator<any, any, any> {
  yield takeLatest('FETCH_NOTIF_LIST_REQUESTED', fetchNotificationList);
  yield takeLatest('FETCH_NOTIFICATION_DETAIL_REQUESTED', fetchNotificationDetail);
}
