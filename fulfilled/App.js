// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {Provider} from 'react-redux';
import {BackAndroid, AppState, NetInfo, Keyboard} from 'react-native';
import {createDataStore} from './createDataStore';
import ActionSheetProvider from './helpers/ActionSheetProvider';
import LocalizedApp from './LocalizedApp';

let store = createDataStore();
export default class App extends Component {
  constructor() {
    super(...arguments);
    autobind(this);
    store.dispatch({
      type: 'APP_INIT',
    });
  }
  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', this._onBackButtonPress);
    AppState.addEventListener('change', this._appStateChanged);
    NetInfo.isConnected.addEventListener(
        'change',
        this._handleConnectionInfoChange
    );
    Keyboard.addListener('keyboardDidShow', this._onKeyboardWillShow);
    Keyboard.addListener('keyboardDidHide', this._onKeyboardDidHide);
    // there is a bug in BackAndroid that doesn't run the app state changed when the app is close and re-open again, so we did it manually
    store.dispatch({
      type: 'APP_STATE_CHANGED',
      newAppState: 'active',
    });
  }
  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this._onBackButtonPress);
    AppState.removeEventListener('change', this._appStateChanged);
    NetInfo.isConnected.removeEventListener(
        'change',
        this._handleConnectionInfoChange
    );
    Keyboard.removeListener('keyboardDidShow', this._onKeyboardWillShow);
    Keyboard.removeListener('keyboardDidHide', this._onKeyboardDidHide);
  }
  render() {
    return (
      <Provider store={store}>
        <ActionSheetProvider>
          <LocalizedApp />
        </ActionSheetProvider>
      </Provider>
    );
  }
  _onBackButtonPress() {
    store.dispatch({type: 'HARDWARE_BACK_PRESSED'});
    return true;
  }
  _appStateChanged(appState: string) {
    store.dispatch({type: 'APP_STATE_CHANGED', newAppState: appState});
  }
  _handleConnectionInfoChange(isConnected: boolean) {
    if (isConnected === false) {
      store.dispatch({type: 'SHOW_POPUP', content: 'noInternet'});
    }
  }
  _onKeyboardWillShow(e: {endCoordinates: {height: number}}) {
    store.dispatch({type: 'KEYBOARD_DID_SHOW', keyboardSpace: e.endCoordinates.height});

  }
  _onKeyboardDidHide() {
    store.dispatch({type: 'KEYBOARD_DID_HIDE'});
  }
}
