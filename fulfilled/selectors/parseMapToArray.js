// @flow

import {createSelector} from 'reselect';

function getMapObject<T>(map: Map<string, T>): Map<string, T> {
  return map;
}

function parseMapToArray<T>(map: Map<string, T>): Array<T> {
  return Array.from(map.values());
}

export default createSelector(
  [getMapObject],
  parseMapToArray,
);
