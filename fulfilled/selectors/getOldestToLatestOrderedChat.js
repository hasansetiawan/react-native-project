// @flow

import {createSelector} from 'reselect';

import type {Chat} from '../types/Chat';

const getChatList = (chatList: Map<string, Chat>) => chatList;

export const getOldestToLatestOrderedChat = createSelector(
  [getChatList],
  (chatList: Map<string, Chat>) => {
    let orderedChatList = Array.from(chatList.values()).sort((a, b) => {
      return new Date(a.createdAt) - new Date(b.createdAt);
    });
    return orderedChatList;
  }
);
