let {describe, it} = global;

import expect from 'expect';

import {getContentFromHTML} from '../getContentFromHTML';
import markdown from '../../fixtures/Markdown';

describe('getContentFromHTML', () => {
  it(`should get the overview's first image`, () => {
    let markdownParsed = {
      ...markdown,
      entityMap: {
        0: {
          type: 'IMAGE',
          data: {
            src: 'http://picture.jpg',
          },
        },
        1: {
          type: 'IMAGE',
          data: {
            src: 'http://gambar.jpg',
          },
        },
      },
    };
    let result = getContentFromHTML(markdownParsed).overview;
    expect(result.imageSrc).toBe('http://picture.jpg');
  });

  it(`should parse the overview's text only`, () => {
    let markdownParsed = {
      ...markdown,
      blocks: [
        {
          type: 'unstyled',
          entityNodes: [
            {
              entity: '0',
              styleNodes: [],
            },
          ],
        },
        {
          type: 'unstyled',
          entityNodes: [
            {
              entity: null,
              styleNodes: [
                {
                  text: 'welcome',
                  styles: null,
                },
              ],
            },
          ],
        },
        {
          type: 'unstyled',
          entityNodes: [
            {
              entity: '1',
              styleNodes: [],
            },
          ],
        },
        {
          type: 'unstyled',
          entityNodes: [
            {
              entity: null,
              styleNodes: [
                {
                  text: 'home',
                  styles: null,
                },
              ],
            },
          ],
        },
      ],
    };

    let result = getContentFromHTML(markdownParsed).overview;
    expect(result.text).toBe('welcome home');
  });
});
