let {describe, it} = global;

import expect from 'expect';
import {getLatestOrderedPosts} from '../getLatestOrderedPosts';
describe('getOrderedPosts', () => {
  it('should give the right order by sorting the latest', () => {
    let postList = new Map();
    postList.set('123', {
      id: '123',
      createdAt: 'Thu Oct 13 2016 13:41:34 GMT+0700 (WIB)',
    });
    postList.set('321', {
      id: '321',
      createdAt: 'Thu Oct 13 2016 13:51:34 GMT+0700 (WIB)',
    });
    let state = {
      post: {
        postList,
      },
    };

    let orderedList = getLatestOrderedPosts(state.post.postList);
    expect(orderedList[0].id).toBe('321');
    expect(orderedList[1].id).toBe('123');


    postList = new Map();
    postList.set('123', {
      id: '123',
      createdAt: 'Thu Oct 13 2016 13:51:34 GMT+0700 (WIB)',
    });
    postList.set('321', {
      id: '321',
      createdAt: 'Thu Oct 13 2016 13:31:34 GMT+0700 (WIB)',
    });
    state = {
      post: {
        postList,
      },
    };

    orderedList = getLatestOrderedPosts(state.post.postList);
    expect(orderedList[0].id).toBe('123');
    expect(orderedList[1].id).toBe('321');
  });
});
