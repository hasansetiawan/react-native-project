let {describe, it} = global;

import expect from 'expect';
import {getOldestToLatestOrderedChat} from '../getOldestToLatestOrderedChat';
describe('getOldestToLatestOrderedChat', () => {
  it('should give the oldest to latest order chats', () => {
    let postList = new Map();
    postList.set('123', {
      id: '123',
      createdAt: 'Thu Oct 13 2016 13:41:34 GMT+0700 (WIB)',
    });
    postList.set('321', {
      id: '321',
      createdAt: 'Thu Oct 13 2016 13:51:34 GMT+0700 (WIB)',
    });
    let state = {
      post: {
        postList,
      },
    };

    let orderedList = getOldestToLatestOrderedChat(state.post.postList);
    expect(orderedList[0].id).toBe('123');
    expect(orderedList[1].id).toBe('321');


    postList = new Map();
    postList.set('123', {
      id: '123',
      createdAt: 'Thu Oct 13 2016 13:51:34 GMT+0700 (WIB)',
    });
    postList.set('321', {
      id: '321',
      createdAt: 'Thu Oct 13 2016 13:31:34 GMT+0700 (WIB)',
    });
    state = {
      post: {
        postList,
      },
    };

    orderedList = getOldestToLatestOrderedChat(state.post.postList);
    expect(orderedList[0].id).toBe('321');
    expect(orderedList[1].id).toBe('123');
  });
});
