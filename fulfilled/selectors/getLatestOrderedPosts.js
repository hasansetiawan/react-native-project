// @flow

import {createSelector} from 'reselect';

import type {Post} from '../types/Post';

const getPostList = (postList: Map<string, Post>) => postList;

export const getLatestOrderedPosts = createSelector(
  [getPostList],
  (postList: Map<string, Post>) => {
    let orderedPostList = Array.from(postList.values()).sort((a, b) => {
      return new Date(b.createdAt) - new Date(a.createdAt);
    });
    return orderedPostList;
  }
);
