// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

type Authentication = {
  nickname: string;
  password: string;
  parseInstallationID: string;
};

const UserAPI = {
  postLogin(userAuth: Authentication) {
    let urlEncoded = formatUrlEncoded({
      nickname: userAuth.nickname,
      password: userAuth.password,
      parse_installation_id: userAuth.parseInstallationID,
    });
    let options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/users/login.json`, options);
  },

  syncUser(userID: string, userToken: string) {
    let urlEncoded = formatUrlEncoded({
      user_id: userID,
      token: userToken,
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/users/sync_user.json`, options);
  },

  fetchUserProfilePicture(userID: string, userToken: string) {
    let options = {
      method: 'GET',
    };
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    return fetchJSON(`/users/${userID}/profile_picture.json?${urlEncoded}`, options);
  },

  fetchUserCurrentProgram(userToken: string) {
    let options = {
      method: 'GET',
    };
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    return fetchJSON(`/app/program.json?${urlEncoded}`, options);
  },

  fetchUserCurrentProgramHabits(userToken: string, programRecordID: string) {
    let options = {
      method: 'GET',
    };
    let urlEncoded = formatUrlEncoded({
      token: userToken,
      program_record_id: programRecordID,
    });
    return fetchJSON(`/app/habit.json?${urlEncoded}`, options);
  },

  signUp(
    firstName: string,
    nickName: string,
    email: string,
    password: string,
    parseID: string,
    platform: string = 'id',
    source: string = 'appstore',
  ) {
    let params = {
      first_name: firstName,
      nickname: nickName,
      email,
      password,
      platform,
      source,
    };
    if (parseID != null) {
      params = {
        ...params,
        parse_installation_id: parseID,
      };
    }
    let urlEncoded = formatUrlEncoded(params);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/users/signup.json`, options);
  },

  async fetchUserProgramHistory(userToken: string) {
    let options = {
      method: 'GET',
    };
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    let data = await fetchJSON(`/app/program_history.json?${urlEncoded}`, options);
    return data;
  },

  async fetchUserPostHistory(userToken: string, page: number, userID: string, lastPostID: ?string, postType: ?string) {
    let input = {
      token: userToken,
      page,
      user_id: userID,
    };

    if (lastPostID != null) {
      input = {
        ...input,
        last_post_id: lastPostID,
      };
    }
    if (postType != null) {
      input = {
        ...input,
        post_type: postType,
      };
    }
    let url = `/posts/filtered_index.json?${formatUrlEncoded(input)}`;
    let data = await fetchJSON(url);
    return Array.isArray(data) ? data : [];
  },
  submitProfilePicture(userToken: string, text: string, privacy: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        text,
        privacy,
      }),
    };
    return fetchJSON(`/profile_pictures.json`, options);
  },

  resetPassword(nickname: string) {
    let params = {
      nickname,
    };
    let urlEncoded = formatUrlEncoded(params);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/password_resets.json`, options);
  },
  async getLatestWeeklyCheckin(userToken: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
      }),
    };
    return fetchJSON(`/users/past_week_activity_counts.json`, options);
  },

  async postWeeklyCheckin(
    userToken: string,
    privacy: string,
    checkinReview: string,
    checkinPlan: string,
    checkinWeight: ?string,
    checkinWaist: ?string,
  ) {
    let bodyForm = {
      token: userToken,
      privacy,
      checkin_review: checkinReview,
      checkin_plan: checkinPlan,
    };
    if (checkinWeight) {
      bodyForm = {
        ...bodyForm,
        checkin_weight: checkinWeight,
      };
    }
    if (checkinWaist) {
      bodyForm = {
        ...bodyForm,
        checkin_waist: checkinWaist,
      };
    }
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        ...bodyForm,
      }),
    };
    return fetchJSON(`/checkins.json`, options);
  },

};

export default UserAPI;
