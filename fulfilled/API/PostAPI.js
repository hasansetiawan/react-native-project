// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';
import type {PostType} from '../types/Post';
import type {ImageData} from '../types/ImageData';

type VideoData = ImageData;

const PostAPI = {
  async fetchPostList(userToken: string, page: number, lastPostID: ?string = null, postType: ?PostType = null) {
    let input = {
      token: userToken,
      page,
    };

    if (lastPostID != null) {
      input = {
        ...input,
        last_post_id: lastPostID,
      };
    }
    if (postType != null) {
      input = {
        ...input,
        post_type: postType,
      };
    }
    let url = `/feed/community.json?${formatUrlEncoded(input)}`;
    let data = await fetchJSON(url);
    return Array.isArray(data) ? data : [];
  },
  submitMealPost(
    userToken: string,
    privacy: string,
    mealDescription: string,
    mealPeriod: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        privacy,
        meal_description: mealDescription,
        meal_period: mealPeriod,
      }),
    };
    return fetchJSON(`/meals.json`, options);
  },

  submitDrinkPost(
    userToken: string,
    privacy: string,
    drinkDescription: string,
    drinkSize: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        privacy,
        drink_description: drinkDescription,
        drink_size: drinkSize,
      }),
    };
    return fetchJSON(`/drinks.json`, options);
  },

  submitActivityPost(
    userToken: string,
    privacy: string,
    activityDescription: string,
    activityDurationHours: string,
    activityDurationMinutes: string,
    activityComment: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        privacy,
        activity_description: activityDescription,
        activity_duration_hours: activityDurationHours,
        activity_duration_minutes: activityDurationMinutes,
        text: activityComment,
      }),
    };
    return fetchJSON(`/activity_logs.json`, options);
  },

  submitNotePost(userToken: string, text: string, privacy: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        text,
        privacy,
      }),
    };
    return fetchJSON(`/notes.json`, options);
  },

  submitMeasurementPost(
    userToken: string,
    text: string,
    privacy: string,
    weight: string,
    waist: string,
    hip: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        text,
        privacy,
        weight,
        waist,
        hip,
      }),
    };
    return fetchJSON(`/measurements.json`, options);
  },

  submitMeasureMentAchievementsPost(userToken: string, text: string, privacy: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        text,
        privacy,
      }),
    };
    return fetchJSON(`/measurement_achievements.json`, options);
  },

  submitCheckinPost(
    userToken: string,
    privacy: string,
    checkInReview: string,
    checkInPlan: string,
    checkInWeight: string,
    checkInWaist: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        privacy,
        checkin_review: checkInReview,
        checkin_plan: checkInPlan,
        checkin_weight: checkInWeight,
        checkin_waist: checkInWaist,
      }),
    };
    return fetchJSON(`/checkins.json`, options);
  },

  submitMilestonePost(
    userToken: string,
    userID: string,
    milestoneWeight: string,
    milestoneMonth: string,
    milestoneYear: string,
  ) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        user_id: userID,
        milestone_target_weight: milestoneWeight,
        milestone_target_date: milestoneYear + '-' + milestoneMonth + '-1', // yyyy-mm-1
      }),
    };
    return fetchJSON(`/users/create_milestone.json`, options);
  },

  //NOTE: the submit post workflow with images / videos is to call the submit{Post} API
  //      and then call this submitImage or submitVideo API.
  submitImage(userToken: string, postID: string, image: ImageData) {
    let formData = new FormData();
    formData.append('0', image);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    };
    return fetchJSON(`/posts/${postID}/add_image.json?token=${userToken}`, options);
  },

  submitVideo(userToken: string, postID: string, video: VideoData) {
    let formData = new FormData();
    formData.append('0', video);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'multipart/form-data',
      },
      body: formData,
    };
    return fetchJSON(`/posts/${postID}/add_video.json?token=${userToken}`, options);
  },

  //NOTE: check if already liked before calling this API.
  likePost(userToken: string, postID: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({token: userToken}),
    };
    return fetchJSON(`/posts/${postID}/like.json`, options);
  },

  commentPost(userToken: string, text: string, postID: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({token: userToken, text}),
    };
    return fetchJSON(`/posts/${postID}/comments.json`, options);
  },
};

export default PostAPI;
