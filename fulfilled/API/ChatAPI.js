// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

export let chatListUrl = `/app/chat.json`;

const ChatAPI = {
  async fetchChatList(userToken: string, page: number, recipientID: string): Promise<Array<Object>> {
    let input = {
      token: userToken,
      page,
      recipient_id: recipientID,
    };
    let url = `${chatListUrl}?${formatUrlEncoded(input)}`;
    let data = await fetchJSON(url);
    return Array.isArray(data) ? data : [];
  },

  submitChat(userToken: string, text: string, recipientNickname: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        text,
        //NOTE: recipient_id is supposed to be recipient_nickname. We should ask Zhen/Yus
        //      to fix this naming mismatch.
        recipient_id: recipientNickname,
      }),
    };
    return fetchJSON(`/app/chat.json`, options);
  },
};

export default ChatAPI;
