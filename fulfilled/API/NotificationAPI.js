// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

const NotificationAPI = {
  fetchNotifications(userToken: string, page: number = 1, simple: boolean = true) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
      page,
      simple,
    });
    return fetchJSON(`/notifications.json?${urlEncoded}`);
  },
  fetchNotificationDetail(userToken: string, postID: string) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    return fetchJSON(`/posts/${postID}.json?${urlEncoded}`);
  },
};

export default NotificationAPI;
