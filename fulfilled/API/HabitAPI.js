// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

const HabitAPI = {
  fetchHabitDailyTipsList(userToken: string, programRecordID: string) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
      program_record_id: programRecordID,
    });
    return fetchJSON(`/app/habit?${urlEncoded}`);
  },
  fetchDailyTipsContent(userToken: string, contentID: string) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    let options = {
      method: 'GET',
    };
    return fetchJSON(`/contents/${contentID}/preview.json?${urlEncoded}`, options);
  },

  completeDailyTips(userToken: string, userID: string, habitID: string, formData?: string) {
    let params = {
      token: userToken,
      user_id: userID,
      completion: 'done',
    };
    if (formData != null) {
      params = {
        ...params,
        form_data: formData,
      };
    }
    let urlEncoded = formatUrlEncoded(params);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/habits/${habitID}/complete.json`, options);
  },

  completeHabit(userToken: string, userHabitRecordID: string, habitID: string) {
    let params = {
      token: userToken,
      user_habit_record_id: userHabitRecordID,
      complete_habit: true,
    };
    let urlEncoded = formatUrlEncoded(params);
    let options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/app/habit/${habitID}.json`, options);
  },
};

export default HabitAPI;
