// @flow

import fetchJSON from '../helpers/fetchJSON';

import formatUrlEncoded from '../helpers/formatUrlEncoded';

export let programListUrl = `/app/program/available_programs.json`;
export let programHistoryUrl = `/app/program_history.json`;
export let programRecordUrl = `/app/program.json`;

const ProgramAPI = {
  async fetchProgramList(userToken: string): Promise<Array<Object>> {
    let url = `${programListUrl}?token=${userToken}&habits=true`;
    let options = {
      method: 'GET',
    };
    let data = await fetchJSON(url, options);
    return Array.isArray(data) ? data : [];
  },

  async fetchProgramHistory(userToken: string) {
    let url = `${programHistoryUrl}?${formatUrlEncoded({token: userToken})}`;
    let options = {
      method: 'GET',
    };
    let data = await fetchJSON(url, options);
    return Array.isArray(data) ? data : [];
  },

  async assignProgram(userToken: string, programID: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        program_id: programID,
      }),
    };
    return fetchJSON(`/app/program/assign_program.json`, options);
  },

  async fetchProgramRecord(userToken: string) {
    let url = `${programRecordUrl}?${formatUrlEncoded({token: userToken})}`;
    let options = {
      method: 'GET',
    };
    let data = await fetchJSON(url, options);
    return Array.isArray(data) ? data : [];
  },
};

export default ProgramAPI;
