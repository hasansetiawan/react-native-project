// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

const PaymentAPI = {
  getProductIDAndroid() {
    let url = `/payments/subscription/googleplay/products.json`;
    return fetchJSON(url);
  },
  getProductIDIOS() {
    let url = `/payments/subscription/appstore/products.json`;
    return fetchJSON(url);
  },
  subscribeAndroid(userToken: string, subscriptionToken: string, subscriptionReceipt: string, subscriptionSignature: string) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        subscription_token: subscriptionToken,
        subscription_receipt: subscriptionReceipt,
        subscription_signature: subscriptionSignature,
      }),
    };
    let url = '/payments/subscription/googleplay/register.json';
    return fetchJSON(url, options);
  },

  subscribeIOS(userToken: string, purchaseReceipts: Array<string>) {
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        purchase_receipts: JSON.stringify(purchaseReceipts),
      }),
    };
    let url = '/payments/subscription/appstore/register.json';
    return fetchJSON(url, options);
  },

  checkSubscription(userToken: string) {
    let input = {
      token: userToken,
    };
    let url = `/payments/subscription/user_check.json?${formatUrlEncoded(input)}`;
    return fetchJSON(url);
  },

  subscriptionHistory(userToken: string) {
    let input = {
      token: userToken,
    };
    let url = `/payments/subscription/user_subscription_history.json?${formatUrlEncoded(input)}`;
    return fetchJSON(url);
  },

  lodgeSubscriptionSupportAndroid(userToken: string, subscriptionToken: string, subscriptionReceipt: string, subscriptionSignature: string, diagnosticData: ?string = null) {
    let optionalData = {};
    if (diagnosticData) {
      optionalData = {
        diagnostic_data: diagnosticData,
      };
    }
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        subscription_token: subscriptionToken,
        subscription_receipt: subscriptionReceipt,
        subscription_signature: subscriptionSignature,
        ...optionalData,
      }),
    };
    let url = '/payments/subscription/googleplay/support_request.json';
    return fetchJSON(url, options);
  },

  lodgeSubscriptionSupportIOS(userToken: string, purchaseReceipts: Array<string>, diagnosticData: ?string = null) {
    let optionalData = {};
    if (diagnosticData) {
      optionalData = {
        diagnostic_data: diagnosticData,
      };
    }
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: userToken,
        purchase_receipts: JSON.stringify(purchaseReceipts),
        ...optionalData,
      }),
    };
    let url = '/payments/subscription/appstore/support_request.json';
    return fetchJSON(url, options);
  },
};

export default PaymentAPI;
