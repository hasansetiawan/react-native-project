const {describe, it} = global;

import expect from 'expect';
import ContentAPI from '../ContentAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('ContentAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });
  it('should call fetchOrientation with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ContentAPI.fetchContent('userToken', 'contentID');
    let urlEncoded = formatUrlEncoded({token: 'userToken', content_id: 'contentID'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/contents/contentID/preview.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toInclude({method: 'GET'});
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call submitOrientationAnswer with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ContentAPI.submitOrientationAnswer('userToken', 'userID', `{formData: {foo: 'bar'}}`);
    let urlEncoded = formatUrlEncoded({
      token: 'userToken',
      user_id: 'userID',
      form_data: `{formData: {foo: 'bar'}}`,
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/orientation.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call completeOrientation with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ContentAPI.completeOrientation('userToken');
    let urlEncoded = formatUrlEncoded({
      token: 'userToken',
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/complete_orientation.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
});
