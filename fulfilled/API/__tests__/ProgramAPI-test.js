const {describe, it} = global;

import expect from 'expect';
import ProgramAPI, {programListUrl, programHistoryUrl, programRecordUrl} from '../ProgramAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';

import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('ProgramAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });
  it('should call fetchProgramList with the correct args', async() => {
    let mockData = [];
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ProgramAPI.fetchProgramList('randomToken');
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}${programListUrl}?token=randomToken&habits=true`);
    expect(mockFetch.calls[0].arguments[1]).toInclude(
      {
        method: 'GET',
      }
    );
    let result = await promise;
    expect(result).toEqual(mockData);
  });

  it('should call fetchProgramHistory with the correct args', async() => {
    let mockData = [];
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ProgramAPI.fetchProgramHistory('randomToken');
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}${programHistoryUrl}?token=randomToken`);
    expect(mockFetch.calls[0].arguments[1]).toInclude(
      {
        method: 'GET',
      }
    );
    let result = await promise;
    expect(result).toEqual(mockData);
  });

  it('should call assignProgram with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ProgramAPI.assignProgram('userToken', 'programID');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({token: 'userToken', program_id: 'programID'}),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/program/assign_program.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call fetchProgramRecord with the correct args', async() => {
    let mockData = [];
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ProgramAPI.fetchProgramRecord('userToken');
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}${programRecordUrl}?token=userToken`);
    expect(mockFetch.calls[0].arguments[1]).toInclude(
      {
        method: 'GET',
      }
    );
    let result = await promise;
    expect(result).toEqual(mockData);
  });
});
