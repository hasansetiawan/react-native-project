const {describe, it} = global;

import expect from 'expect';
import UserAPI from '../UserAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('UserAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });
  it('should call postLogin with the correct args', async() => {
    let mockData = 'asd';
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.postLogin({nickname: 'asd', password: 'asd123FF', parseInstallationID: 'parseInstallationID'});
    let urlEncoded = formatUrlEncoded({nickname: 'asd', password: 'asd123FF', parse_installation_id: 'parseInstallationID'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/login.json`);
    expect(mockFetch.calls[0].arguments[1]).toInclude(
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/x-www-form-urlencoded',
        },
        body: urlEncoded,
      }
    );
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
  });

  it('should call syncUser with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.syncUser('userID', 'userToken');
    let urlEncoded = formatUrlEncoded({user_id: 'userID', token: 'userToken'});
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/sync_user.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call fetchUserProfilePicture with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.fetchUserProfilePicture('userID', 'userToken');
    let urlEncoded = formatUrlEncoded({token: 'userToken'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/userID/profile_picture.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toInclude({method: 'GET'});
    let result = await promise;
    expect(await result.json()).toEqual({});
  });

  it('should call fetchUserCurrentProgram with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.fetchUserCurrentProgram('userToken');
    let urlEncoded = formatUrlEncoded({token: 'userToken'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/program.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toInclude({method: 'GET'});
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
  it('should call fetchUserCurrentProgramHabits with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.fetchUserCurrentProgramHabits('userToken', 'programRecordID');
    let urlEncoded = formatUrlEncoded({token: 'userToken', program_record_id: 'programRecordID'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/habit.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toInclude({method: 'GET'});
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
  it('should call signUp with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.signUp('firstName', 'nickName', 'email', 'password', '123');
    let urlEncoded = formatUrlEncoded({
      first_name: 'firstName',
      nickname: 'nickName',
      email: 'email',
      password: 'password',
      platform: 'id',
      source: 'appstore',
      parse_installation_id: '123',
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/signup.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call fetchUserProgramHistory with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.fetchUserProgramHistory('userToken');
    let urlEncoded = formatUrlEncoded({token: 'userToken'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/program_history.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toInclude({method: 'GET'});
    let result = await promise;
    expect(await result.json()).toEqual({});
  });

  it('should call fetchPostHistory with the correct args', async() => {
    let mockData = [];
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.fetchUserPostHistory('randomToken', 1, 'userID');
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/posts/filtered_index.json?token=randomToken&page=1&user_id=userID`);
    let result = await promise;
    expect(result).toEqual(mockData);
    promise = UserAPI.fetchUserPostHistory('randomToken', 1, 'userID', 'lastPostID', 'meal');
    expect(mockFetch.calls[1].arguments[0]).toEqual(`${SERVER_API}/posts/filtered_index.json?token=randomToken&page=1&user_id=userID&last_post_id=lastPostID&post_type=meal`);
    restoreFetch();
  });

  it('should call submitProfilePicture with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.submitProfilePicture('userToken', 'text', 'privacy');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        text: 'text',
        privacy: 'privacy',
      }),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/profile_pictures.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call resetPassword with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.resetPassword('nickname');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        nickname: 'nickname',
      }),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/password_resets.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call getLatestWeeklyCheckin with correct args', async () => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.getLatestWeeklyCheckin('userToken');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
      }),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/users/past_week_activity_counts.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call postWeeklyCheckin with all form fields', async () => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.postWeeklyCheckin(
      'userToken',
      'privacy',
      'checkinReview',
      'checkinPlan',
      'checkinWeight',
      'checkinWaist',
    );
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        privacy: 'privacy',
        checkin_review: 'checkinReview',
        checkin_plan: 'checkinPlan',
        checkin_weight: 'checkinWeight',
        checkin_waist: 'checkinWaist',
      }),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/checkins.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call postWeeklyCheckin without optional form fields correctly', async () => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = UserAPI.postWeeklyCheckin(
      'userToken',
      'privacy',
      'checkinReview',
      'checkinPlan',
    );
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        privacy: 'privacy',
        checkin_review: 'checkinReview',
        checkin_plan: 'checkinPlan',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/checkins.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});


    promise = UserAPI.postWeeklyCheckin(
      'userToken',
      'privacy',
      'checkinReview',
      'checkinPlan',
      'checkinWeight',
    );
    options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        privacy: 'privacy',
        checkin_review: 'checkinReview',
        checkin_plan: 'checkinPlan',
        checkin_weight: 'checkinWeight',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(2);
    expect(mockFetch.calls[1].arguments[0]).toEqual(`${SERVER_API}/checkins.json`);
    expect(mockFetch.calls[1].arguments[1]).toEqual(options);
    result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});


    promise = UserAPI.postWeeklyCheckin(
      'userToken',
      'privacy',
      'checkinReview',
      'checkinPlan',
      null,
      'checkinWaist',
    );
    options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        privacy: 'privacy',
        checkin_review: 'checkinReview',
        checkin_plan: 'checkinPlan',
        checkin_waist: 'checkinWaist',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(3);
    expect(mockFetch.calls[2].arguments[0]).toEqual(`${SERVER_API}/checkins.json`);
    expect(mockFetch.calls[2].arguments[1]).toEqual(options);
    result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});

    restoreFetch();
  });
});
