const {describe, it} = global;

import expect from 'expect';
import HabitAPI from '../HabitAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('HabitAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });

  it('should call fetchHabitDailyTipsList with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = HabitAPI.fetchHabitDailyTipsList('userToken', 'programRecordID');
    let urlEncoded = formatUrlEncoded({token: 'userToken', program_record_id: 'programRecordID'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/habit?${urlEncoded}`);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
  it('should call fetchDailyTipsContent with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = HabitAPI.fetchDailyTipsContent('userToken', 'contentID');
    let urlEncoded = formatUrlEncoded({token: 'userToken'});
    let options = {
      method: 'GET',
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/contents/contentID/preview.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call completeDailyTips with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = HabitAPI.completeDailyTips('userToken', 'userID', 'habitID');
    let urlEncoded = formatUrlEncoded({token: 'userToken', user_id: 'userID', completion: 'done'});
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/habits/habitID/complete.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });

  it('should call completeHabit with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = HabitAPI.completeHabit('userToken', 'userHabitRecordID', 'habitID');
    let urlEncoded = formatUrlEncoded({token: 'userToken', user_habit_record_id: 'userHabitRecordID', complete_habit: true});
    let options = {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/habit/habitID.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
});
