const {describe, it} = global;

import expect from 'expect';
import PaymentAPI from '../PaymentAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('PaymentAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });

  it('should call subscribeAndroid with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.subscribeAndroid('userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        subscription_token: 'subscriptionToken',
        subscription_receipt: 'subscriptionReceipt',
        subscription_signature: 'subscriptionSignature',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/googleplay/register.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);

    let newOptions = {
      ...options,
      body: formatUrlEncoded({
        token: 'userToken',
        subscription_token: 'subscriptionToken',
        subscription_receipt: 'subscriptionReceipt',
        subscription_signature: 'subscriptionSignature',
      }),
    };
    promise = PaymentAPI.subscribeAndroid('userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature');
    expect(mockFetch.calls[1].arguments[1]).toEqual(newOptions);
    restoreFetch();
  });

  it('should call subscribeIOS with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.subscribeIOS('userToken', ['purchaseReceipt']);
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        purchase_receipts: JSON.stringify(['purchaseReceipt']),
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/appstore/register.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

  it('should call checkSubscription with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.checkSubscription('userToken');
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/user_check.json?token=userToken`);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

  it('should call subscriptionHistory with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.subscriptionHistory('userToken');
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/user_subscription_history.json?token=userToken`);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

  it('should call lodgeSubscriptionReportAndroid with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.lodgeSubscriptionSupportAndroid('userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature', 'diagnosticData');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        subscription_token: 'subscriptionToken',
        subscription_receipt: 'subscriptionReceipt',
        subscription_signature: 'subscriptionSignature',
        diagnostic_data: 'diagnosticData',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/googleplay/support_request.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);

    let newOptions = {
      ...options,
      body: formatUrlEncoded({
        token: 'userToken',
        subscription_token: 'subscriptionToken',
        subscription_receipt: 'subscriptionReceipt',
        subscription_signature: 'subscriptionSignature',
      }),
    };
    promise = PaymentAPI.lodgeSubscriptionSupportAndroid('userToken', 'subscriptionToken', 'subscriptionReceipt', 'subscriptionSignature');
    expect(mockFetch.calls[1].arguments[1]).toEqual(newOptions);
    restoreFetch();
  });

  it('should call lodgeSubscriptionReportIOS with the correct args', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.lodgeSubscriptionSupportIOS('userToken', ['purchaseReceipts'], 'diagnosticData');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({
        token: 'userToken',
        purchase_receipts: JSON.stringify(['purchaseReceipts']),
        diagnostic_data: 'diagnosticData',
      }),
    };
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/appstore/support_request.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

  it('should call get product id android', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.getProductIDAndroid();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/googleplay/products.json`);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

  it('should call get product id ios', async() => {
    let mockData = {foo: 'bar'};
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = PaymentAPI.getProductIDIOS();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/payments/subscription/appstore/products.json`);
    let result = await promise;
    expect(await result.json()).toEqual(mockData);
    restoreFetch();
  });

});
