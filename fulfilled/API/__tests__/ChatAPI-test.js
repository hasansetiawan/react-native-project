const {describe, it} = global;

import expect from 'expect';
import ChatAPI, {chatListUrl} from '../ChatAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('ChatAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });
  it('should call fetch with the correct args', async() => {
    let mockData = [];
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse(mockData))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ChatAPI.fetchChatList('randomToken', 1, 'userID');
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}${chatListUrl}?token=randomToken&page=1&recipient_id=userID`);
    expect(mockFetch.calls[0].arguments[1]).toEqual({});
    let result = await promise;
    expect(result).toEqual(mockData);
  });

  it('should call submitChat with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = ChatAPI.submitChat('userToken', 'text', 'recipientNickname');
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: formatUrlEncoded({token: 'userToken', text: 'text', recipient_id: 'recipientNickname'}),
    };
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/app/chat.json`);
    expect(mockFetch.calls[0].arguments[1]).toEqual(options);
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
});
