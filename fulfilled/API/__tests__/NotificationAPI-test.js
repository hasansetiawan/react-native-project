const {describe, it} = global;

import expect from 'expect';
import NotificationAPI from '../NotificationAPI';
import {setFetch} from '../../helpers/fetchJSON';

import formatUrlEncoded from '../../helpers/formatUrlEncoded';
import SERVER_API from '../../constants/defaultServerAPIUrl';

describe('NotificationAPI', () => {
  let createResponse = (data) => ({
    json: () => Promise.resolve(data),
  });
  it('should call fetchNotifications with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = NotificationAPI.fetchNotifications('userToken');
    let urlEncoded = formatUrlEncoded({token: 'userToken', page: 1, simple: true});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/notifications.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toEqual({});
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
  it('should call fetchNotificationDetail with the correct args', async() => {
    let mockFetch = expect.createSpy().andReturn(
      Promise.resolve(createResponse({foo: 'bar'}))
    );
    let restoreFetch = setFetch(mockFetch);
    let promise = NotificationAPI.fetchNotificationDetail('userToken', 'postID');
    let urlEncoded = formatUrlEncoded({token: 'userToken'});
    restoreFetch();
    expect(promise).toBeA(Promise);
    expect(mockFetch.calls.length).toBe(1);
    expect(mockFetch.calls[0].arguments[0]).toEqual(`${SERVER_API}/posts/postID.json?${urlEncoded}`);
    expect(mockFetch.calls[0].arguments[1]).toEqual({});
    let result = await promise;
    expect(await result.json()).toEqual({foo: 'bar'});
  });
});
