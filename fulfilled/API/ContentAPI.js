// @flow

import fetchJSON from '../helpers/fetchJSON';
import formatUrlEncoded from '../helpers/formatUrlEncoded';

const ContentAPI = {
  fetchContent(userToken: string, contentID: string) {
    let options = {
      method: 'GET',
    };
    let urlEncoded = formatUrlEncoded({
      token: userToken,
      content_id: contentID,
    });
    return fetchJSON(`/contents/${contentID}/preview.json?${urlEncoded}`, options);
  },

  submitOrientationAnswer(userToken: string, userID: string, formData: string) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
      user_id: userID,
      form_data: formData,
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/app/orientation.json`, options);
  },

  completeOrientation(userToken: string) {
    let urlEncoded = formatUrlEncoded({
      token: userToken,
    });
    let options = {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
      },
      body: urlEncoded,
    };
    return fetchJSON(`/users/complete_orientation.json`, options);
  },
};

export default ContentAPI;
