// @flow
import {connect} from 'react-redux';
import NavBar from '../core-ui/NavBar';

import type {RootState} from '../types/RootState';
const mapStateToProps = (state: RootState) => {
  return {
    selectedIndex: state.navigation.navbar.selectedIndex,
    user: state.user,
  };
};

const mapDispatchToProps = (dispatch) => ({
  onNavigate: (pageName: string, index: number) => {
    dispatch({
      type: 'NAVBAR_CHANGED',
      route: {key: pageName},
      index,
    });
    dispatch({
      type: 'NOTIFICATION_COUNT_REMOVED',
      pageName,
    });
  },
});

export default connect(mapStateToProps, mapDispatchToProps)(NavBar);
