// @flow

import {connect} from 'react-redux';
import NotificationView from '../views/NotificationView';
import CommunityPostDetail from '../views/CommunityPostDetail';

const testData = {
  post: {
    id: '57e653aa92d4a518b9000049',
    createdAt: '2016-09-24T10:21:30Z',
    imageWidth: null,
    imageHeight: null,
    type: 'HabitEntry', //Post type
    user: '51f5d3945653eb4f55000003',
    habitId: '56c607736e7d6b98f4000008',
    likers: ['56921044a85a722400000009', '57de6abeafd3db91a6000021', '57d608f4ef23f38772000057', '51f5d3945653eb4f55000003', '56921044a85a722400000015', '56921044a85a722400000011'],
    comments: [],
    displayText: 'Mindful Eating - Ate Slowly Today for the 3rd time',
    privacy: 'community',
    posterNickname: 'bec',
    isLiked: false,
  },
  isCommenting: new Set(),
  likes: new Set(),
  comments: new Map(),
};

function mapDispatchToProps(dispatch) {
  return {
    onNotificationClicked: (id: string) => { // only for demo
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'NOTIFICATION/' + id,
          Component: CommunityPostDetail,
          props: testData,
          title: 'postMenu/activity',
        },
      });
    },
  };
}


export default connect(null, mapDispatchToProps)(NotificationView);
