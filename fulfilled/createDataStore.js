// @flow

import {createStore, combineReducers, applyMiddleware} from 'redux';
import navigationReducer from './reducers/navigationReducer';
import userReducer from './reducers/userReducer';
import programReducer from './reducers/programReducer';
import chatReducer from './reducers/chatReducer';
import commentReducer from './reducers/commentReducer';
import postReducer from './reducers/postReducer';
import habitReducer from './reducers/habitReducer';
import habitActivityReducer from './reducers/habitActivityReducer';
import languageReducer from './reducers/languageReducer';
import coachReducer from './reducers/coachReducer';
import communityMemberReducer from './reducers/communityMemberReducer';
import notificationReducer from './reducers/notificationReducer';
import userProgramHistoryReducer from './reducers/userProgramHistoryReducer';
import userPostsHistoryReducer from './reducers/userPostsHistoryReducer';
import drawerReducer from './reducers/drawerReducer';
import readingMaterialReducer from './reducers/readingMaterialReducer';
import loginStatusReducer from './reducers/loginStatusReducer';
import orientationContentReducer from './reducers/orientationContentReducer';
import loadingPageReducer from './reducers/loadingPageReducer';
import pushNotificationReducer from './reducers/pushNotificationReducer';
import toastReducer from './reducers/toastReducer';
import appStateReducer from './reducers/appStateReducer';
import contentReducer from './reducers/contentReducer';
import resetPasswordReducer from './reducers/resetPasswordReducer';
import imageViewReducer from './reducers/imageViewReducer';
import profilePictureReducer from './reducers/profilePictureReducer';
import popupModalReducer from './reducers/popupModalReducer';
import keyboardReducer from './reducers/keyboardReducer';
import productReducer from './reducers/productReducer';
import weeklyCheckinReducer from './reducers/weeklyCheckinReducer';

import navigationMiddleware from './middleware/navigationMiddleware';
import checkLoginMiddleware from './middleware/checkLoginMiddleware';
import saveLoginMiddleware from './middleware/saveLoginMiddleware';
import logoutMiddleware from './middleware/logoutMiddleware';
import toastMiddleware from './middleware/toastMiddleware';
import saveSelectedLanguageMiddleware from './middleware/saveSelectedLanguageMiddleware';

import createMiddleware from 'redux-saga';

let sagaMiddleware = createMiddleware();
import rootSaga from './sagas/rootSaga';


let app = combineReducers({
  navigation: navigationReducer,
  user: userReducer,
  coach: coachReducer,
  programs: programReducer,
  chat: chatReducer,
  comments: commentReducer,
  post: postReducer,
  habit: habitReducer,
  readingMaterial: readingMaterialReducer,
  habitActivity: habitActivityReducer,
  selectedLanguage: languageReducer,
  communityMembers: communityMemberReducer,
  notification: notificationReducer,
  userProgramHistory: userProgramHistoryReducer,
  userPostsHistory: userPostsHistoryReducer,
  isDrawerOpen: drawerReducer,
  loginStatus: loginStatusReducer,
  orientationContent: orientationContentReducer,
  loadingPage: loadingPageReducer,
  pushNotification: pushNotificationReducer,
  toast: toastReducer,
  appState: appStateReducer,
  content: contentReducer,
  showResetPasswordConfirmation: resetPasswordReducer,
  imageView: imageViewReducer,
  profilePictureList: profilePictureReducer,
  popupModal: popupModalReducer,
  keyboard: keyboardReducer,
  productIDList: productReducer,
  weeklyCheckin: weeklyCheckinReducer,
});

export let createDataStore = () => {
  let store = createStore(
    app,
    applyMiddleware(
      navigationMiddleware,
      checkLoginMiddleware,
      saveLoginMiddleware,
      saveSelectedLanguageMiddleware,
      logoutMiddleware,
      sagaMiddleware,
      toastMiddleware,
    ),
  );
  sagaMiddleware.run(rootSaga);
  return store;
};
