// @flow
import {
  StyleSheet,
} from 'react-native';
import {CONTAINER_COLOR} from './constants/color';
import {
  FONT_MEDIUM,
} from './constants/Font';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: CONTAINER_COLOR,
    zIndex: 0,
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  weeklyCheckinDialog: {
    backgroundColor: '#FF9800',
    flexWrap: 'wrap',
  },
  weeklyCheckinDialogTextContainer: {
    flexWrap: 'wrap',
    alignItems: 'center',
    paddingHorizontal: 5,
    paddingVertical: 10,
  },
  weeklyCheckinDialogText: {
    fontFamily: FONT_MEDIUM,
    fontSize: 13,
    textAlign: 'center',
    color: 'white',
  },
});

export default styles;
