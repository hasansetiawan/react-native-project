// @flow

let initialPost = new Map();

initialPost.set('57e653aa92d4a518b9000049', {
  id: '57e653aa92d4a518b9000049',
  createdAt: '2016-10-21T04:37:14.097Z',
  imageWidth: null,
  imageHeight: null,
  type: 'ActivityLog', //Post type
  user: '51f5d3945653eb4f55000003',
  habitId: '56c607736e7d6b98f4000008',
  likers: ['56921044a85a722400000009', '57de6abeafd3db91a6000021', '57d608f4ef23f38772000057', '51f5d3945653eb4f55000003', '56921044a85a722400000015', '56921044a85a722400000011'],
  comments: [],
  displayText: 'Mindful Eating - Ate Slowly Today for the 3rd time',
  privacy: 'community',
  posterNickname: 'bec',
  isLiked: false,
});
initialPost.set('57e62cf292d4a5b51100002b', {
  id: '57e62cf292d4a5b51100002b',
  createdAt: '2016-09-24T07:36:18Z',
  imageWidth: null,
  imageHeight: null,
  type: 'ActivityLog', //Post type
  user: '57d608f4ef23f38772000057',
  likers: ['56921044a85a722400000009'],
  comments: ['57818d58749961f3c7000002'],
  displayText: 'Lunch: Thai food - mango salad , fried kang kong , i strip of garlic pork, minced basil meat',
  privacy: 'community',
  posterNickname: 'cj',
  image: {
    standardResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/57e62c7d92d4a518b9000034/low_resolution_file.jpg',
    lowResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/57e62c7d92d4a518b9000034/low_resolution_file.jpg',
    thumbnailResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/57e62c7d92d4a518b9000034/thumbnail_file.jpg',
  },
  isLiked: false,
});


export let nestedPostData = [{
  createdAt: '2016-01-12T23:24:31Z',
  imageWidth: 500,
  imageHeight: 667,
  type: 'Meal',
  user: {
    nickname: 'zhen',
    firstName: 'Zhen Hoe',
    name: 'Zhen Hoe',
    timeZone: 'Singapore',
    isMentor: false,
    isCoach: true,
    isNutritionist: false,
    isDietitian: false,
    ageInDays: 387,
    id: '51f5d3945653eb4f55000003',
    coachIntroductionId: '527729b37a4c9960c3000028',
  },
  likers: [
    {
      nickname: 'cj',
      firstName: 'Catherine',
      name: 'Catherine',
      timeZone: 'Singapore',
      isMentor: null,
      isCoach: false,
      isNutritionist: false,
      isDietitian: false,
      ageInDays: 192,
      id: '56921044a85a722400000009',
    },
  ],
  comments: [
    {
      text: 'Wow where do you buy almond butter?',
      user: {
        nickname: 'bcontessa',
        firstName: 'Bcontessa',
        name: 'Bcontessa',
        timeZone: 'Singapore',
        isMentor: null,
        isCoach: false,
        isNutritionist: false,
        isDietitian: false,
        ageInDays: 263,
        id: '568b79fb0283519ecb000005',
      },
      id: '56958bae87ad72f2e200000c',
    },
    {
      text: 'Where to get such almond butter ? 👍🏻 ',
      user: {
        nickname: 'cj',
        firstName: 'Catherine',
        name: 'Catherine',
        timeZone: 'Singapore',
        isMentor: null,
        isCoach: false,
        isNutritionist: false,
        isDietitian: false,
        ageInDays: 192,
        id: '56921044a85a722400000009',
      },
      id: '56960e86c6b0dc57ae000002',
    },
    {
      text: 'You can find it on redmart. Nuts about butter. Try it. Really good and healthy. ',
      user: {
        nickname: 'zhen',
        firstName: 'Zhen Hoe',
        name: 'Zhen Hoe',
        timeZone: 'Singapore',
        isMentor: false,
        isCoach: true,
        isNutritionist: false,
        isDietitian: false,
        ageInDays: 387,
        id: '51f5d3945653eb4f55000003',
        coachIntroductionId: '527729b37a4c9960c3000028',
      },
      id: '5696fa86c6b0dcd636000009',
    },
  ],
  id: '56958b2f87ad72f2e2000009',
  displayText: 'Breakfast: 2 slices of bread with unsweetened almond butter. Love the nuttiness of almond butter!',
  privacy: 'community',
  posterNickname: 'zhen',
  image: {
    standardResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/low_resolution_file.jpg',
    lowResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/low_resolution_file.jpg',
    thumbnailResolution: 'https://s3.amazonaws.com/dmusesz/uploads/post/image/56958b2f87ad72f2e2000009/thumbnail_file.jpg',
  },
  isLiked: false,
  rating: null,
  category: 'none',
  nutritionistNickname: null,
}];

export default initialPost;
