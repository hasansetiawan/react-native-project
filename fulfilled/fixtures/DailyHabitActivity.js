// @flow

import type {HabitActivity} from '../types/HabitActivity';
let dailyTips: HabitActivity = {
  id: '57063b6becd3647f32000007',
  text: '0: 0: 20 menit: Mengatur makan kini begitu mudah!',
  completionTarget: 1,
  completionThreshold: 1,
  type: 'Lesson',
  completed: false,
  readingMaterial: '1234929323',
  displayType: 'regular',
  completedToday: false,
  category: 'meal',
  daysCompleted: 0,
  questions: [],
};

export default dailyTips;
