// @flow

let initialQuestions = new Map();
initialQuestions.set('', {
  text: 'Beritahukan kami rasa apa yang kamu SUKA kemarin?',
  slug: 'beritahukan-kami-rasa-apa-yang-kamu-suka-kemarin',
  answerPublic: true,
  createdAt: '2016-04-07T11:02:26Z',
  responseFormat: 'opentext',
  html: '',
  id: '57063e42ecd364019c000004',
});

export default initialQuestions;
