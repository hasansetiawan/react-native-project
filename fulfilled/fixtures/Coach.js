// @flow

import type {Coach} from '../types/Coach';

export let coach: Coach = {
  nickname: 'zhen',
  firstName: 'Zhen',
  name: 'Zhen',
  timeZone: 'Singapore',
  isMentor: false,
  isCoach: true,
  isNutritionist: false,
  isDietitian: false,
  ageInDays: 388,
  id: '51f5d3945653eb4f55000003',
  coachIntroductionID: '527729b37a4c9960c3000028',
};
