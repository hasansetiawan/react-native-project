// @flow

let initialHabit = {
  id: '570340125a15c968dc000003',
  text: 'Keep your body hydrated. For each day, drink at least eight glasses of water',
  completionTarget: 7,
  completionThreshold: 7,
  type: 'Daily',
  textPresentTense: '',
  completed: false,
  readingMaterial: null,
  userHabitRecordId: '57303a4c6376f7f37c000012',
  daysCompleted: 6,
  daysOnHabit: null,
  uncompletedPostId: null,
  habitCompletionMessageId: '57350430e24e50c9be00001d',
  reminderText: 'Transformasikan dirimu bersama Fulfilled.\nMemutuskan untuk berubah terkadang memang sulit. Biarkan Fulfilled menyelesaikannya.\nTemukan versi terbaik dirimu bersama Fulfilled.\nKamu pasti sudah lama menginginkan hidup yang sehat. Mari kita mulai.\nJangan ragu. Kami di sini untuk membantumu.\nMungkin kamu pernah mencoba dan gagal. Maka sekarang, kami ada untuk membantumu.\nSayangi dirimu. Mulailah bersama Fulfilled.\nPercayalah bahwa kita bisa berubah bersama komunitas Fulfilled. Mari bergabung.\nBerikan kami kesempatan untuk mendapatkan kepercayaanmu. Mulailah bersama Fulfilled.\nTransformasi memang membutuhkan waktu. Kami di sini. Bergabunglah.\nJadilah sedikit lebih baik hari ini dibanding kemarin. Mulailah bersama Fulfilled.\nSingkirkan rasa malu dan rasa bersalah, transformasikan dirimu.\nDukungan dan dorongan sama dengan keberhasilan di Fulfilled. Mari bergabung bersama kami.',
};

export default initialHabit;
