// @flow

let communityMembers = new Map();
communityMembers.set('57de6abeafd3db91a6000021', {
  id: '57de6abeafd3db91a6000021',
  nickname: 'bec',
  isMentor: null,
  isCoach: false,
  isNutritionist: false,
  isDietitian: false,
});
communityMembers.set('57d608f4ef23f38772000057', {
  id: '57d608f4ef23f38772000057',
  nickname: 'fena',
  isMentor: null,
  isCoach: false,
  isNutritionist: false,
  isDietitian: false,
});
communityMembers.set('51f5d3945653eb4f55000003', {
  id: '51f5d3945653eb4f55000003',
  nickname: 'zhen',
  isMentor: false,
  isCoach: true,
  isNutritionist: false,
  isDietitian: false,
});
communityMembers.set('56921044a85a722400000009', {
  id: '56921044a85a722400000009',
  nickname: 'cj',
  isMentor: true,
  isCoach: false,
  isNutritionist: false,
  isDietitian: false,
});
communityMembers.set('56921044a85a722400000011', {
  id: '56921044a85a722400000011',
  nickname: 'Manda this is a verrrrryyyyyyyyy long nickname just for testing flexWrap',
  isMentor: null,
  isCoach: false,
  isNutritionist: false,
  isDietitian: false,
});
communityMembers.set('56921044a85a722400000015', {
  id: '56921044a85a722400000015',
  nickname: 'Kris',
  isMentor: null,
  isCoach: false,
  isNutritionist: false,
  isDietitian: false,
});

export default communityMembers;
