// @flow

let initialHabitActivities = new Map();
initialHabitActivities.set('', {
  id: '57063f35ecd364019c000006',
  text: '0: 0: 20 menit: Mengatur makan kini begitu mudah!',
  completionTarget: 1,
  completionThreshold: null,
  type: 'Lesson',
  textPresentTense: null,
  completed: false,
  readingMaterial: {
    markdown: '',
  },
  userHabitRecordId: null,
  daysCompleted: 1,
  daysOnHabit: null,
  uncompletedPostId: null,
  habitCompletionMessageId: null,
  reminderText: '',
  programCategory: null,
  completedToday: false,
  hasInstructions: false,
  category: null,
  displayType: 'regular',
  questions: ['57063e42ecd364019c000004'],
});

export default initialHabitActivities;
