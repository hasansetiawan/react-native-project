// @flow

import type {Program} from '../types/Program';
let initialPrograms: Map<string, Program> = new Map();

initialPrograms.set('182741', {
  title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
  advancementType: 1,
  category: 1,
  categoryDescription: 'nutrition',
  introContent: '',
  icon: '&#xE42E;',
  iconColor: 'color_coach_post',
  habitCount: 1,
  totalHabitDays: 7,
  lock: false,
  isFoundation: false, //if this is true, app needs to check that this program is done before other programs are unlocked
  isPremium: false, //add premium tag if true
  id: '57033f225a15c98a2b00000c',
  display: 1,
  programGoal: '',
  status: 1,
  totalUsers: 6044,
  habits: ['Aku makan hingga 80% kenyang hari ini.'],
  image: {
    lowResolution: '',
    standardResolution: '',
    thumbnailResolution: '',
    svg: '',
  },
});

export {initialPrograms};
