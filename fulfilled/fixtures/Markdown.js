let markdown = {
  entityMap: {
    '0': { // eslint-disable-line
      type: 'IMAGE',
      data: {
        src: 'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP2.png',
      },
    },
    '1': { // eslint-disable-line
      type: 'IMAGE',
      data: {
        src: 'https://s3-ap-southeast-1.amazonaws.com/fulfilled/program_images/TIP1.png',
      },
    },
    '2': { // eslint-disable-line
      type: 'LINK',
      data: {
        url: 'http://www.id-getfulfilled.co/home/2016/5/9/kenapa-saya-mendirikan-fulfilled',
      },
    },
  },
  blocks: [
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: '0',
          styleNodes: [],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: null,
          styleNodes: [
            {
              text: 'Selamat datang di tantangan pertama kami. Selama 7 hari ke depan, kamu akan mendapatkan berbagai macam tips untuk membantumu mengatur asupan makan dengan begitu mudah, dan membuat hal ini menjadi sebuah kebiasaan yang menyenangkan.',
              styles: null,
            },
          ],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: '1',
          styleNodes: [],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: null,
          styleNodes: [
            {
              text: 'Mampu mengatur makan berarti akan dengan mudah mencapai 80% berat badan ideal. Hal ini juga merupakan kunci untuk menjaga tubuh tetap benergi, tetap sehat, dan yang paling penting: mengenali tubuhmu lebih dekat.',
              styles: null,
            },
          ],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: null,
          styleNodes: [
            {
              text: 'Tips pertama',
              styles: [
                'BOLD', 'ITALIC',
              ],
            },
            {
              text: ': Gunakan waktu 20 menit untuk makan. Nikmati makananmu selama waktu tersebut. Kenapa? Karena otak kita membutuhkan waktu ±20 menit untuk mengirimkan sinyal kenyang kepada perut. Dengan begitu, kamu akan secara alami mengonsumsi makanan lebih sedikit, dan tentu tidak akan makan berlebihan.',
              styles: null,
            },
          ],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: null,
          styleNodes: [
            {
              text: 'Ketika kamu berhasil menggunakan waktu selama 20 menit di tiap kali makan hari ini, centang kotak di dalam aplikasi. Kamu pasti bisa!',
              styles: null,
            },
          ],
        },
      ],
    },
    {
      type: 'unstyled',
      entityNodes: [
        {
          entity: '2',
          styleNodes: [
            {
              text: 'Baca lebih lanjut mengenai pendekatan Fulfilled',
              styles: null,
            },
          ],
        },
      ],
    },
  ],
};

export default markdown;
