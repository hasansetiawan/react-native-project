// @flow

let initialChats = new Map();

initialChats.set('5811bdd9c1b282354f000012', {
  id: '5811bdd9c1b282354f000012',
  createdAt: '2016-10-27T08:42:01Z',
  text: 'Selain mengatur asupan makan, olahraga juga dibutuhkan untuk menjaga tubuh tetap sehat dan ideal. Tantangan olahraga dasar adalah jawabannya! Di akhir hari ke-14 dalam tantangan ini, tingkat aktivitas fisikmu akan meningkat, semakin banyak kalori yang dibakar, berarti kamu semakin dekat pula dengan tubuh sehat, bugar, dan ideal!',
  displayText: 'test',
  privacy: 'private',
  posterNickname: 'zhen',
  recipientNickname: 'sstur2',
  userIsPoster: false,
  createdAtInSeconds: 1477557721,
});

initialChats.set('5811bdd9c1b282354f000013', {
  id: '5811bdd9c1b282354f000013',
  createdAt: '2016-10-27T08:45:01Z',
  text: 'test',
  displayText: 'test',
  privacy: 'private',
  posterNickname: 'sstur2',
  recipientNickname: 'zhen',
  userIsPoster: true,
  createdAtInSeconds: 1477557721,
});
export {initialChats};
