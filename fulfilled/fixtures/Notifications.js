// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {Notification} from '../types/Notification';

let initialNotifications: Array<Notification> = [

  {
    createdAt: '2016-10-20T09:27:10Z',
    id: '58088deeebde51849c001c4f',
    isNew: false,
    type: 'coach_post',
    text: `Coach Tiwi posted a message: 'Dear Fulfiller,\n\nFulfilled telah meningkatkan server aplikasi kami agar dapat melayani Fulfiller ...`,
    postID: '57f1d7ffebde51e9ae0000c5',
  },
  {
    createdAt: '2016-10-18T15:10:46Z',
    id: '58063b76ebde510b0e001db5',
    isNew: false,
    type: 'like_post',
    text: `Adam4380 menyukai postingan 'satu gelas susu: 330 ml'`,
    postID: '58045a9cebde5162e1000b8e',
  },
  {
    createdAt: '2016-10-17T16:07:49Z',
    id: '5804f7552a6491a019007347',
    isNew: false,
    type: 'comment_post',
    text: `Liliksuryani mengomentari postingan 'Lunch: Nasi goreng'`,
    postID: '58045e0cebde5171d5000b96',
  },
];

export default arrayToMap(null, initialNotifications);
