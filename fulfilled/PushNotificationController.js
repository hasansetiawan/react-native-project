// @flow
import {Component} from 'react';
import {Platform} from 'react-native';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import PushNotification from 'react-native-push-notification';
import Parse from 'parse/react-native';
import {
  APP_ID,
  GCM_SENDER_ID,
  PARSE_URL,
  APP_IDENTIFIER,
  JAVASCRIPT_KEY,
  MASTER_KEY,
} from './constants/PushNotificationKeys';

import NotificationDetailView from './views/NotificationDetailView';

import getNotifCategory from './helpers/getNotifCategory';
import {
  CHAT_PAGE,
  CHAT_NAVBAR_INDEX,
  NOTIFICATION_PAGE,
} from './constants/NavBarPageName';

import type {RootState} from './types/RootState';
import type {User} from './types/User';
import type {Coach} from './types/Coach';
import type {Dispatch} from './types/Dispatch';

type Props = {
  user: User;
  coach: Coach;
  appState: string;
  navbar: string;
  onDeviceTokenReceived: (deviceToken: string) => void;
  onParseInstallationIDReceived: (parseInstallationID: string) => void;
  onNotificationClicked: (token: string, type: string, postID: string) => void;
  changeAppState: (newAppState: string) => void;
  onNotifReceived: (token: string, category: string, coach: Coach, navbar: string) => void;
  showToast: (toastMessage: string) => void;
};

export class PushNotificationController extends Component {
  props: Props
  state: {appState: string}

  constructor() {
    super(...arguments);
    autobind(this);
    // in opensource parse server, I need to force it to use master key in order to get all client access granted
    Parse._initialize(APP_ID, JAVASCRIPT_KEY, MASTER_KEY);
    Parse.CoreManager.set('USE_MASTER_KEY', true);
    Parse.serverURL = PARSE_URL;
    this.state = {appState: this.props.appState};
  }

  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (newProps.appState !== oldProps.appState) {
      this.setState({appState: newProps.appState});
    }
  }

  componentDidMount() {
    let notifDataMap: Map<string, Object> = new Map();
    PushNotification.configure({
      // (optional) Called when Token is generated (iOS and Android)
      onRegister: (tokenObject) => {
        let {token, os} = tokenObject;
        this.props.onDeviceTokenReceived(token);
        Parse._getInstallationId()
        .then((id) => {
          this.props.onParseInstallationIDReceived(id);
          let Installation = Parse.Object.extend('_Installation');
          let query = new Parse.Query(Installation);
          query.equalTo('installationId', id);
          query.find()
            .then((installations) => {
              let installation;
              if (installations.length === 0) {
                // No previous installation object, create new one.
                installation = new Installation();
              } else {
                // Found previous one, update.
                installation = installations[0];
              }
              installation.set('channels', ['']);
              installation.set('deviceToken', token);
              installation.set('deviceType', os);
              installation.set('installationId', id);
              installation.set('appIdentifier', APP_IDENTIFIER);
              installation.set('localeIdentifier', 'en-US');
              installation.set('appName', 'Fulfilled Indonesia');
              installation.set('timeZone', 'Indonesia');
              installation.set('parseVersion', '1.13.1');
              if (os === 'android') {
                installation.set('GCMSenderId', GCM_SENDER_ID);
                installation.set('pushType', 'gcm');
              } else if (os === 'ios') {
                installation.set('pushType', 'apns');
              }
              return installation.save();
            })
            .catch((error) => {
              this.props.showToast(error.message);
              return;
            });
        })
        .catch((error) => {
          this.props.showToast(error.message);
          return;
        });

      },

      // (required) Called when a remote or local notification is opened or received
      onNotification: (notification) => {
        let {onNotifReceived, changeAppState, user, coach, navbar} = this.props;
        if (notification.data != null && notification.data.notif_category != null) {
          notifDataMap.set(notification.id, notification.data);
          if (notification.userInteraction === false) {
            onNotifReceived(user.token, notification.data.notif_category, coach, navbar);
          }
        }
        if (this.state.appState === 'active' && notification.userInteraction === false && Platform.OS === 'android') {
          PushNotification.localNotification({
            id: notification.id,
            title: 'Fulfilled', // (optional, for iOS this is only used in apple watch, the title will be the app name on other iOS devices)
            message: notification.message, // (required)
            playSound: true, // (optional) default: true
            soundName: 'default', // (optional) Sound to play when the notification is shown. Value of 'default' plays the default sound. It can be set to a custom sound such as 'android.resource://com.xyz/raw/my_sound'. It will look for the 'my_sound' audio file in 'res/raw' directory and play it. default: 'default' (default sound is played)
            vibrate: true, // (optional) default: true
            vibration: 300, // vibration length in milliseconds, ignored if vibrate=false, default: 1000
          });
        }

        if (notification.userInteraction === true) {
          changeAppState('active');
          let data = notifDataMap.get(notification.id);
          if (data != null) {
            let {coach, onNotificationClicked} = this.props;
            onNotificationClicked(user.token, data.notif_category, data.post_id, coach);
            notifDataMap.delete(notification.id);
          }
        }
      },

      // ANDROID ONLY: GCM Sender ID (optional - not required for local notifications, but is need to receive remote push notifications)
      senderID: GCM_SENDER_ID,

      // IOS ONLY (optional): default: all - Permissions to register.
      // permissions: {
      //   alert: true,
      //   badge: true,
      //   sound: true,
      // },

      // Should the initial notification be popped automatically
      // default: true
      popInitialNotification: true,

      /**
        * (optional) default: true
        * - Specified if permissions (ios) and token (android and ios) will requested or not,
        * - if not, you must call PushNotificationsHandler.requestPermissions() later
        */
      requestPermissions: true,
    });
  }

  render() {
    return null;
  }
}

export function mapStateToProps(state: RootState) {
  return {
    coach: state.coach,
    user: state.user,
    appState: state.appState,
    navbar: state.navigation.navbar.routes[0].key,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    showToast: (toastMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage,
      });
    },
    changeAppState: (newAppState: string) => {
      dispatch({
        type: 'APP_STATE_CHANGED',
        newAppState,
      });
    },
    onNotifReceived: (token: string, category: string, coach: Coach, navbar: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'notificationPage',
      });
      dispatch({
        type: 'FETCH_NOTIF_LIST_REQUESTED',
        token,
        page: 1,
        isRefreshingList: true,
      });

      let type = getNotifCategory(category);
      if (type === 'ChatPost') {
        dispatch({
          type: 'LOADING_STARTED',
          loadingPages: 'chatPage',
        });
        dispatch({
          type: 'FETCH_CHAT_LIST_REQUESTED',
          token: token,
          page: 1,
          recipient: coach.id,
          isRefreshingList: true,
        });
        if (navbar !== CHAT_PAGE) {
          dispatch({
            type: 'NEW_CHAT_NOTIF_RECEIVED',
          });
        }
      } else {
        if (navbar !== NOTIFICATION_PAGE) {
          dispatch({
            type: 'NEW_NOTIF_RECEIVED',
          });
        }
      }
    },
    onDeviceTokenReceived: (deviceToken: string) => {
      dispatch({
        type: 'DEVICE_TOKEN_RECEIVED',
        deviceToken,
      });
    },
    onParseInstallationIDReceived: (parseInstallationID: string) => {
      dispatch({
        type: 'PARSE_INSTALLATION_ID_RECEIVED',
        parseInstallationID,
      });
    },
    onNotificationClicked: (token: string, category: string, postID: string, coach: Coach) => {
      if (category) {
        let type = getNotifCategory(category);

        if (type === 'ChatPost') {
          dispatch({
            type: 'LOADING_STARTED',
            loadingPages: 'chatPage',
          });
          dispatch({
            type: 'FETCH_CHAT_LIST_REQUESTED',
            token: token,
            page: 1,
            recipient: coach.id,
            isRefreshingList: true,
          });
          dispatch({
            type: 'NAVBAR_CHANGED',
            route: {key: CHAT_PAGE},
            index: CHAT_NAVBAR_INDEX,
          });
        } else {
          dispatch({
            type: 'FETCH_NOTIFICATION_DETAIL_REQUESTED',
            token,
            postID,
          });
          dispatch({
            type: 'LOADING_STARTED',
            loadingPages: 'notificationDetailPage',
          });
          dispatch({
            type: 'ROUTE_PUSHED',
            route: {
              key: 'notificationDetail',
              title: '',
              Component: NotificationDetailView,
            },
          });
        }
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PushNotificationController);
