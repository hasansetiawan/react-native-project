// @flow
import React, {Component, PropTypes} from 'react';

import type Locale from '../localization/Locale';

type GetMessage = (text: string) => string;

type Props = {};

type Context = {
  locale: Locale;
};

function connectLocale(ComposedComponent: Function) {
  let displayName: string = ComposedComponent.displayName || ComposedComponent.name || 'Anonymous';

  class LocalizedComponent extends Component {
    // Essentially, we require all props that ComposedComponent requires,
    // except `getMessage`
    props: Props;
    context: Context;
    _subscription: {unsubscribe: () => void};
    _getMessage: GetMessage;

    static displayName = `Localized(${displayName})`;

    static contextTypes = {
      locale: PropTypes.object,
    };

    constructor(props: Props, context: Context) {
      super(props, context);
      this._subscription = context.locale.subscribe(
        () => this.forceUpdate()
      );
      this._getMessage = (fromString) => {
        return this.context.locale.getMessage(fromString);
      };
    }

    componentWillUnmount() {
      this._subscription.unsubscribe();
    }

    render() {
      let otherProps = this.props;
      return <ComposedComponent {...otherProps} getMessage={this._getMessage} />;
    }
  }

  return LocalizedComponent;
}

export default connectLocale;
