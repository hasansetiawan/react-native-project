let {describe, it} = global;

import expect from 'expect';

import en from '../en';
import id from '../id';

describe('localization language', () => {
  it('should have the same key, both en and id', () => {
    let idKeys = Object.keys(id);
    let enKeys = Object.keys(en);
    expect(enKeys.length).toBe(idKeys.length);
    for (let key of enKeys) {
      expect(idKeys.indexOf(key) > -1).toBe(true);
    }
    for (let key of idKeys) {
      expect(enKeys.indexOf(key) > -1).toBe(true);
    }
  });
});
