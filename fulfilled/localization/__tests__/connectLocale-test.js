// @flow
const {describe, it} = global;

import React, {Component} from 'react';
import expect from 'expect';
import connectLocale from '../connectLocale';

type Props = {
  fromString: string;
  getMessage: (fromString: string) => string;
};

class Hello extends Component<void, Props, void> {
  props: Props;

  render() {
    let {fromString, getMessage} = this.props;
    return <div>{getMessage(fromString)}</div>;
  }
}

function HelloPure(props: Props) {
  let {fromString, getMessage} = props;
  return <div>{getMessage(fromString)}</div>;
}

const LocalizedHello = connectLocale(Hello);
const LocalizedHelloPure = connectLocale(HelloPure);

describe('connectLocale', () => {

  it('should work with class components', () => {
    let foo = <LocalizedHello fromString="hi" />;
    expect(foo).toExist();
  });

  it('should work with pure functional components', () => {
    let foo = <LocalizedHelloPure fromString="hi" />;
    expect(foo).toExist();
  });

});
