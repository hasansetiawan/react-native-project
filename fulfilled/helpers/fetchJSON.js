// @flow
/* global Headers */

import SERVER_API from '../constants/defaultServerAPIUrl';
import getVersion from './getVersion';

type Fetch = typeof global.fetch;
let customFetch: ?Fetch;

export function setFetch(newFetch: Fetch) {
  let previousFetch = customFetch;
  customFetch = newFetch;
  let restorePrevious = () => {
    customFetch = previousFetch;
  };
  return restorePrevious;
}

export default async function fetchJSON(url: string, options: Object = {}) {
  let fetch = customFetch || global.fetch;
  let {version, buildTime} = getVersion();
  if (customFetch == null) {
    let {headers} = options;
    let newHeaders = {
      ...headers,
      'X-Fulfilled-App-Version': `${version} ${buildTime}`,
    };
    let newOptions = {
      ...options,
      headers: newHeaders,
    };
    let response = await fetch(`${SERVER_API}${url}`, {
      ...newOptions,
    });
    let contentTypeRaw = response.headers.get('Content-Type') || '';
    let contentType = contentTypeRaw.split(';')[0].toLowerCase().trim();
    let body;
    if (contentType === 'application/json') {
      body = await response.json();
    } else {
      let text = await response.text();
      body = {__BODY__: text};
    }
    let errorMessage;
    if (!response.ok) {
      errorMessage = `Unexpected response status: ${response.status}`;
    } else if (contentType !== 'application/json') {
      errorMessage = `Unexpected response type: ${contentType}`;
    }
    if (errorMessage != null) {
      let error: Object = new Error(errorMessage);
      Object.assign(error, {
        status: response.status,
        headers: toObject(response.headers),
        body,
      });
      throw error;
    }
    return body;
  } else {
    let response = await fetch(`${SERVER_API}${url}`, {...options});
    return response;
  }

}

function toObject(headers: Headers) {
  let result: {[key: string]: mixed} = {};
  if (headers.forEach) {
    headers.forEach((value, key) => {
      result[key] = value;
    });
  } else {
    for (let [key, value] of headers.entries()) {
      result[key] = value;
    }
  }
  return result;
}
