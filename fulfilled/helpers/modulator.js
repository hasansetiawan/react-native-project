//@flow
import type {Range} from '../types/Range';

export default function modulate(input: number, inputRange: Range, resultRange: Range) {
  return (
    (
      (input - inputRange.start) *
      (resultRange.start - resultRange.end) /
      (inputRange.start - inputRange.end)
    ) +
    resultRange.start
  );
}
