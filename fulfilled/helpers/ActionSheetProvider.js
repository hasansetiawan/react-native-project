// @flow
import React, {Component, PropTypes} from 'react';
import ActionSheet from '@exponent/react-native-action-sheet';

type Props = {
  children?: ReactNode;
};

class ActionSheetProvider extends Component {
  props: Props;
  _actionSheetRef: Object;

  static childContextTypes = {
    getActionSheet: PropTypes.func,
  };

  getChildContext() {
    return {
      getActionSheet: () => this._actionSheetRef,
    };
  }

  render() {
    let {children} = this.props;
    return (
      <ActionSheet ref={(instance) => (this._actionSheetRef = instance)}>
        {children}
      </ActionSheet>
    );
  }
}

export default ActionSheetProvider;
