// @flow

export function validateLogin(nickname?: string, password?: string) {
  if (nickname && password) {
    return null;
  }
  return 'emptyFields';
}

export function validateRegister(fullName?: string, nickname?: string, password?: string, email?: string) {
  if (fullName && nickname && password && email) {
    let emailRegex = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    if (email) {
      if (!emailRegex.test(email)) {
        return 'emailNotValid';
      }
    }
    return null;
  }
  return 'emptyFields';
}

export function validateActivityPost(content?: string, hour?: string, minute?: string) {
  if (content && minute && hour) {
    if (isNaN(hour)) {
      return 'hourNotAValidNumber';
    }
    if (isNaN(minute)) {
      return 'minuteNotAValidNumber';
    }
    return null;
  }
  return 'emptyFields';
}

export function validateNotePost(content?: string) {
  if (content == null) {
    return 'emptyFields';
  }
  return null;
}


export function validateMealPost(content?: string, time?: string) {
  if (content && time) {
    return null;
  }
  return 'emptyFields';
}


export function validateDrinkPost(content?: string, amount?: string) {
  if (content && amount) {
    return null;
  }
  return 'emptyFields';
}


export function validateMeasurementPost(weight?: string, hip?: string, waist?: string) {
  if (weight && hip && waist) {
    if (isNaN(weight)) {
      return 'weightNotAValidNumber';
    }
    if (isNaN(hip)) {
      return 'hipNotAValidNumber';
    }
    if (isNaN(waist)) {
      return 'waistNotAValidNumber';
    }
    return null;
  }
  return 'emptyFields';
}

export function validateResetPassword(nickname?: string) {
  if (!nickname) {
    return 'emptyFields';
  }
  return null;
}

export function validateQuestionAnswered(answer?: string) {
  if (!answer) {
    return 'answerRequired';
  }
  return null;
}

export function validateWeeklyCheckin(checkinReview: string, checkinPlan: string, checkinWeight: ?string, checkinWaist: ?string) {
  if (!checkinReview) {
    return 'checkinReviewAnswerRequired';
  }
  if (!checkinPlan) {
    return 'checkinPlanAnswerRequired';
  }
  if (checkinWeight) {
    if (isNaN(checkinWeight)) {
      return 'weightNotAValidNumber';
    }
  }
  if (checkinWaist) {
    if (isNaN(checkinWaist)) {
      return 'waistNotAValidNumber';
    }
  }
  return null;
}
