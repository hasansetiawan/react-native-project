// @flow

import id from '../localization/languages/id';
import en from '../localization/languages/en';

export default function getLanguages() {
  return {
    id,
    en,
  };
}
