// @flow

export default function formatUrlEncoded(input: {[key: string]: any}) {
  return Object.keys(input).map((key) => {
    let pureValue;
    let pureKey = encodeURIComponent(key);
    if (Array.isArray(input[key])) {
      return input[key].map((value) => {
        pureValue = encodeURIComponent(value);
        return pureKey + '[]=' + pureValue;
      }).join('&');
    } else {
      pureValue = encodeURIComponent(input[key]);
      return pureKey + '=' + pureValue;
    }
  }).join('&');
}
