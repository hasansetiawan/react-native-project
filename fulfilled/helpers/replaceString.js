// @flow

export default function replaceString(initialString: string, regex: RegExp, replacedString: string) {
  return initialString.replace(regex, replacedString);
}
