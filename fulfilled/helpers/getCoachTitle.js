// @flow

type Member = {
  isCoach: boolean;
  isNutritionist: boolean;
  isDietitian: boolean;
  isMentor: boolean;
};

export default function getCoachTitle(coach: Member) {
  if (coach.isCoach === true) {
    return 'coach';
  } else if (coach.isNutritionist === true) {
    return 'nutrititonist';
  } else if (coach.isDietitian === true) {
    return 'dietitian';
  } else if (coach.isMentor === true) {
    return 'mentor';
  } else {
    return null;
  }
}
