// @flow

import getDaysName from './getDaysName';

import DEFAULT_WEEKLY_CHECKIN_DAY from '../constants/weeklyCheckinDaysName';

export default function isWeekend(date: Date) {
  return DEFAULT_WEEKLY_CHECKIN_DAY.includes(getDaysName(date.getDay()));
}
