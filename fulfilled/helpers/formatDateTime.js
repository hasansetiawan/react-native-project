// @flow
import type Locale from '../localization/Locale';

export const MONTH_NAMES = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
// const DAY_NAMES = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];

const FORMATS = {
  DATE_TIME_SHORT: (date: Date, locale: Locale) => {
    let time = `${padZero(date.getHours())}:${padZero(date.getMinutes())}`;
    if (isToday(date)) {
      let today = locale.getMessage(`today`);
      return `${today}, ${time}`;
    }
    let month = MONTH_NAMES[date.getMonth()];
    let dateNumber = padZero(date.getDate());
    let year = date.getFullYear();
    let monthName = locale.getMessage(`monthShort/${month}`);
    return `${dateNumber} ${monthName} ${year}, ${time}`;
  },
  DATE_TIME_LONG: (date: Date, locale: Locale) => {
    let time = `${padZero(date.getHours())}:${padZero(date.getMinutes())}`;
    if (isToday(date)) {
      let today = locale.getMessage(`today`);
      return `${today}, ${time}`;
    }
    let month = MONTH_NAMES[date.getMonth()];
    let dateNumber = padZero(date.getDate());
    let year = date.getFullYear();
    let monthName = locale.getMessage(`monthFull/${month}`);
    return `${dateNumber} ${monthName} ${year}, ${time}`;
  },
  DATE_LONG: (date: Date, locale: Locale) => {
    if (isToday(date)) {
      let today = locale.getMessage(`today`);
      return today;
    }
    let month = MONTH_NAMES[date.getMonth()];
    let dateNumber = padZero(date.getDate());
    let year = date.getFullYear();
    let monthName = locale.getMessage(`monthFull/${month}`);
    return `${dateNumber} ${monthName} ${year}`;
  },
  DATE_SHORT: (date: Date, locale: Locale) => {
    if (isToday(date)) {
      let today = locale.getMessage(`today`);
      return today;
    }
    let month = MONTH_NAMES[date.getMonth()];
    let dateNumber = padZero(date.getDate());
    let year = date.getFullYear();
    let monthName = locale.getMessage(`monthShort/${month}`);
    return `${dateNumber} ${monthName} ${year}`;
  },
};

export function padZero(value: number): string {
  return (value < 10) ? `0${value}` : String(value);
}

export function isToday(date: Date, today: Date = new Date()) {
  return (today.toDateString() === date.toDateString()) ? true : false;
}

export default function formatDateTime(dateString: string, format: string, locale: Locale) {
  let date = new Date(dateString);
  let formatter = FORMATS[format];
  return formatter ? formatter(date, locale) : '';
}
