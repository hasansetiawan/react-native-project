// @flow

import getDaysName, {weekday} from './getDaysName';

export default function isAlreadyWeeklyCheckedIn(todaysDate: Date, latestCheckinDate: Date) {
  let yesterdayDateMilliseconds = new Date(todaysDate).setDate(todaysDate.getDate() - 1);
  let yesterdayDate = new Date(yesterdayDateMilliseconds);

  if (
    getDaysName(todaysDate.getDay()) === weekday[6] && // if today is Saturday
    latestCheckinDate.toDateString() === todaysDate.toDateString()
  ) {
    return true;
  } else if (
    getDaysName(todaysDate.getDay()) === weekday[0] && // if today is Sunday
    (latestCheckinDate.toDateString() === todaysDate.toDateString() || latestCheckinDate.toDateString() === yesterdayDate.toDateString())
  ) {
    return true;
  }
  return false;
}
