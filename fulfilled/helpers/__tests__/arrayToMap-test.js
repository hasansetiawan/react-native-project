let {describe, it} = global;

import expect from 'expect';
import arrayToMap from '../arrayToMap';

describe('parse Array to Map', () => {
  let array = [
    {
      id: '132',
      name: 'audy',
      nickname: 'Odi',
    },
    {
      id: '152',
      name: 'Amanda',
      nickname: 'Manda',
    },
  ];

  it('should add item from previous map', () => {
    let oldMap = new Map();
    oldMap.set('222', {id: '222', name: 'Jorgie', nickname: 'Jorg'});
    let map = arrayToMap(oldMap, array);
    expect(map.size).toBe(3);
    expect(map.has('222')).toBe(true);
    expect(map.has('132')).toBe(true);
    expect(map.has('152')).toBe(true);
  });

  it('should parse array to map with default key', () => {
    let map = arrayToMap(null, array);
    expect(map.size).toBe(2);
    expect(map.has('132')).toBe(true);
    expect(map.has('152')).toBe(true);
  });

  it('should parse array to map with custom key', () => {
    let map = arrayToMap(null, array, 'nickname');
    expect(map.size).toBe(2);
    expect(map.has('132')).toBe(false);
    expect(map.has('152')).toBe(false);

    expect(map.has('Odi')).toBe(true);
    expect(map.has('Manda')).toBe(true);
  });
});
