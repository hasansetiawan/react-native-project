let {describe, it, after} = global;

import expect from 'expect';
import fetchJSON, {setFetch} from '../fetchJSON';
import SERVER_API from '../../constants/defaultServerAPIUrl';

import Pretender from 'fetch-pretender';

import getVersion from '../getVersion';

describe('fetch JSON', () => {
  let server = new Pretender();
  let {version, buildTime} = getVersion();
  server.post(`${SERVER_API}/api/something.json`, () => {
    return [
      '200',
      {
        'Content-Type': 'application/json; charset=utf-8',
        'X-Fulfilled-App-Version': `${version} ${buildTime}`,
      },
      '{"user": "john"}',
    ];
  });
  server.post(`${SERVER_API}/api/something.html`, () => {
    return [
      '200',
      {
        'Content-Type': 'text/html; charset=utf-8',
        'X-Fulfilled-App-Version': `${version} ${buildTime}`,
      },
      '<p>Hello</p>',
    ];
  });
  after(() => {
    server.shutdown();
  });
  it('should change the fetch object', () => {
    let oldFetch = fetch;
    let mockFetch = expect.createSpy();
    let restoreFetch = setFetch(mockFetch);
    expect(oldFetch).toNotEqual(mockFetch);
    restoreFetch();
    expect(fetch).toEqual(oldFetch);
  });
  it('should call the fetch object with given url and option', async() => {
    let options = {
      method: 'GET',
    };

    let mockFetch = expect.createSpy();
    let restoreFetch = setFetch(mockFetch);

    let result = fetchJSON('/api/test', {...options});
    expect(result).toBeA(Promise);
    expect(mockFetch).toHaveBeenCalledWith(`${SERVER_API}/api/test`, {method: 'GET'});
    restoreFetch();
  });
  it('Should fetch an fetchJSON error', async() => {
    let result;
    try {
      result = await fetchJSON('/api/something.json', {method: 'POST'});
      expect(result).toEqual({user: 'john'});
    } catch (error) {
      return;
    }
  });
  it('Should throw error on unexpected content', async() => {
    let result;
    let error;
    try {
      result = await fetchJSON('/api/something.html', {method: 'POST'});
    } catch (e) {
      error = e;
    }
    expect(result).toNotExist();
    expect(error).toBeAn(Error);
    expect(error.status).toBe(200);
    expect(error.message).toBe('Unexpected response type: text/html');
  });
});
