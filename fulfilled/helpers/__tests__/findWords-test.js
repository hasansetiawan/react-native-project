// @flow
const {describe, it} = global;
import expect from 'expect';
import findWords from '../findWords';

describe('findWords', () => {
  // let example = 'Sudah menggunakan Versi Beta Fulfilled?\n\nCeritakan pengalaman kamu dengan mengisi survei dari kami di sini https://fulfilled.typeform.com/to/UTWVQ5\n\nYuk sama-sama kita tingkatkan aplikasi Fulfilled Indonesia untuk menginspirasi lebih banyak orang menjalani kebiasaan sehatnya!';

  it('should process empty string', () => {
    let fn = expect.createSpy();
    findWords('', /foo/, fn);
    expect(fn).toHaveBeenCalledWith(false, '');
  });

  it('should process string with no match', () => {
    let fn = expect.createSpy();
    findWords('Hello world!', /foo/g, fn);
    expect(fn).toHaveBeenCalledWith(false, 'Hello world!');
  });

  it('should process string with one match', () => {
    let fn = expect.createSpy();
    findWords('Hello world!', /world/g, fn);
    expect(fn.calls.length).toBe(3);
    expect(fn.calls.map((x) => x.arguments)).toEqual([
      [false, 'Hello '],
      [true, 'world'],
      [false, '!'],
    ]);
  });

  it('should process string with two matches', () => {
    let fn = expect.createSpy();
    findWords('The car drove far.', /[cf]ar/g, fn);
    expect(fn.calls.length).toBe(5);
    expect(fn.calls.map((x) => x.arguments)).toEqual([
      [false, 'The '],
      [true, 'car'],
      [false, ' drove '],
      [true, 'far'],
      [false, '.'],
    ]);
  });

});
