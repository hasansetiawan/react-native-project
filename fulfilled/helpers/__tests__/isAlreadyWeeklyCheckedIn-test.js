// @flow
const {describe, it} = global;
import expect from 'expect';
import isAlreadyWeeklyCheckedIn from '../isAlreadyWeeklyCheckedIn';

describe('isAlreadyWeeklyCheckedIn', () => {
  it('should return true if today is Saturday and latest checkin date is in the same day', () => {
    let todaysDate = new Date('2017-01-28 11:12:00');
    let latestCheckinDate = new Date('2017-01-28 07:12:00');
    let result = isAlreadyWeeklyCheckedIn(todaysDate, latestCheckinDate);
    expect(result).toBe(true);

    todaysDate = new Date('2017-01-29 11:12:00');
    latestCheckinDate = new Date('2017-01-29 07:12:00');
    result = isAlreadyWeeklyCheckedIn(todaysDate, latestCheckinDate);
    expect(result).toBe(true);
  });

  it('should return true if today is Sunday and latest checkin date is yesterday', () => {
    let todaysDate = new Date('2017-01-29 11:12:00');
    let latestCheckinDate = new Date('2017-01-28 07:12:00');
    let result = isAlreadyWeeklyCheckedIn(todaysDate, latestCheckinDate);
    expect(result).toBe(true);

    todaysDate = new Date('2017-01-29 11:12:00');
    latestCheckinDate = new Date('2017-01-22 07:12:00');
    result = isAlreadyWeeklyCheckedIn(todaysDate, latestCheckinDate);
    expect(result).toBe(false);
  });
});
