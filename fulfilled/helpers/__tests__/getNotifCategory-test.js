// @flow
const {describe, it} = global;
import expect from 'expect';
import getNotifCategory from '../getNotifCategory';

describe('getNotifCategory', () => {
  it('should the right notification category', () => {
    let category = 'Notification::CommentPost';
    let type = getNotifCategory(category);
    expect(type).toBe('CommentPost');

    category = 'Notification::ChatPost';
    type = getNotifCategory(category);
    expect(type).toBe('ChatPost');

    category = 'Notification::LikePost';
    type = getNotifCategory(category);
    expect(type).toBe('LikePost');
  });
});
