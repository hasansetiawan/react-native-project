// @flow

let {describe, it} = global;

import expect from 'expect';
import getCountdown from '../getCountdown';

describe('getCountdown', () => {
  it('should return hours minutes and seconds till midnight', () => {
    let {hours, minutes, seconds} = getCountdown(new Date(2016, 10, 21, 14, 20, 50));
    expect(seconds).toBe(10);
    expect(minutes).toBe(39);
    expect(hours).toBe(9);
  });

  it('should handle midnight', () => {
    let {hours, minutes, seconds} = getCountdown(new Date(2016, 10, 21, 0, 0, 0));
    expect(seconds).toBe(0);
    expect(minutes).toBe(0);
    expect(hours).toBe(24);
  });

  it('should handle 0 seconds / minutes / hours', () => {
    let first = getCountdown(new Date(2016, 10, 21, 14, 20, 0));
    expect(first.seconds).toBe(0);
    expect(first.minutes).toBe(40);
    expect(first.hours).toBe(9);

    let second = getCountdown(new Date(2016, 10, 21, 14, 0, 50));
    expect(second.seconds).toBe(10);
    expect(second.minutes).toBe(59);
    expect(second.hours).toBe(9);

    let third = getCountdown(new Date(2016, 10, 21, 0, 20, 50));
    expect(third.seconds).toBe(10);
    expect(third.minutes).toBe(39);
    expect(third.hours).toBe(23);

    let fourth = getCountdown(new Date(2016, 10, 21, 14, 0, 0));
    expect(fourth.seconds).toBe(0);
    expect(fourth.minutes).toBe(0);
    expect(fourth.hours).toBe(10);

    let fifth = getCountdown(new Date(2016, 10, 21, 0, 0, 50));
    expect(fifth.seconds).toBe(10);
    expect(fifth.minutes).toBe(59);
    expect(fifth.hours).toBe(23);

    let sixth = getCountdown(new Date(2016, 10, 21, 0, 20, 0));
    expect(sixth.seconds).toBe(0);
    expect(sixth.minutes).toBe(40);
    expect(sixth.hours).toBe(23);
  });
});
