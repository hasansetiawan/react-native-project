// @flow
const {describe, it} = global;
import expect from 'expect';
import replaceString from '../replaceString';

describe('replaceString', () => {
  it('should replace specific string ', () => {
    let wrongHtml = `<iframe allowfullscreen="true" frameborder="0" height="300" src="https://www.youtube.com/embed/sFXQwk63Y9E?showinfo=0&amp;rel=0" width="400" />`;
    let fixedHtml = replaceString(wrongHtml, /<iframe([^>]*)\/>/g, '<iframe$1></iframe>');
    expect(fixedHtml).toBe(`<iframe allowfullscreen="true" frameborder="0" height="300" src="https://www.youtube.com/embed/sFXQwk63Y9E?showinfo=0&amp;rel=0" width="400" ></iframe>`);
  });
});
