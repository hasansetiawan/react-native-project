// @flow

const {describe, it} = global;

import formatToCamelCase from '../formatToCamelCase';
import expect from 'expect';

describe('formatToCamelCase test', () => {
  it('should format simple object without changes', () => {
    let plainObject = {
      id: '123',
      name: 'john',
    };
    expect(formatToCamelCase(plainObject)).toEqual(plainObject);
  });
  it('should format snake case object to camelCase object', () => {
    let snakeCaseObject = {
      user_id: '123',
      user_name: 'michael',
      user_password: 'somethingsecret',
    };
    expect(formatToCamelCase(snakeCaseObject)).toEqual({
      userID: '123',
      userName: 'michael',
      userPassword: 'somethingsecret',
    });
  });
  it('should format deeply nested SnakeCase Object to CamelCase Object', () => {
    let deeplyNested = {
      user_id: '123',
      user_name: 'michael',
      user_task: {
        task_id: '123',
        task_name: 'do homework',
        task_category: {
          category_id: '124',
          category_list: ['urgent', 123],
        },
      },
    };
    expect(formatToCamelCase(deeplyNested)).toEqual({
      userID: '123',
      userName: 'michael',
      userTask: {
        taskID: '123',
        taskName: 'do homework',
        taskCategory: {
          categoryID: '124',
          categoryList: ['urgent', 123],
        },
      },
    });

    deeplyNested = {
      program: {
        title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
        program_goal: '',
        advancement_type_description: 'check_based',
        category_description: 'nutrition',
        icon: '&#xE42E;',
        icon_color: 'color_coach_post',
        is_foundation: false,
        id: '57033f225a15c98a2b00000c',
      },
      current_habit_record: {
        days_completed: 0,
        habit: {
          text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
          completion_target: 7,
          id: '570340125a15c968dc000003',
          uncompleted_post_id: null,
          days_completed: null,
        },
        id: '58326b46bb9982993a00001a',
        days_active: 22.9966565662815,
      },
      id: '58326b46bb9982993a000019',
      days_on_program: null,
      days_completed: 0,
      total_program_time: 7,
    };

    expect(formatToCamelCase(deeplyNested)).toEqual({
      program: {
        title: 'Tantangan Ahlinya Makan Penuh Kesadaran',
        programGoal: '',
        advancementTypeDescription: 'check_based',
        categoryDescription: 'nutrition',
        icon: '&#xE42E;',
        iconColor: 'color_coach_post',
        isFoundation: false,
        id: '57033f225a15c98a2b00000c',
      },
      currentHabitRecord: {
        daysCompleted: 0,
        habit: {
          text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
          completionTarget: 7,
          id: '570340125a15c968dc000003',
          uncompletedPostID: null,
          daysCompleted: null,
        },
        id: '58326b46bb9982993a00001a',
        daysActive: 22.9966565662815,
      },
      id: '58326b46bb9982993a000019',
      daysOnProgram: null,
      daysCompleted: 0,
      totalProgramTime: 7,
    });
  });
});
