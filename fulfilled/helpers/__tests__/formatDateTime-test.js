// @flow

let {describe, it} = global;

import expect from 'expect';
import formatDateTime, {isToday, MONTH_NAMES, padZero} from '../formatDateTime';
import Locale from '../../localization/Locale';
import mockLanguages from '../../localization/fixtures/mockLanguages';


describe('format date time', () => {
  let dateUTCString = '2016-10-10T17:00:00Z';
  let dateUTCStringNeedPadding = '2016-10-01T17:03:00Z';
  let dateUTC = new Date(dateUTCString);
  let dateUTCNeedPadding = new Date(dateUTCStringNeedPadding);

  it('should render DATE_TIME_LONG in specified language', () => {
    let locale = new Locale(mockLanguages, 'en');
    let monthName = locale.getMessage(`monthFull/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCString, 'DATE_TIME_LONG', locale)).toBe(`${padZero(dateUTC.getDate())} ${monthName} ${dateUTC.getFullYear()}, ${padZero(dateUTC.getHours())}:${padZero(dateUTC.getMinutes())}`);

    locale = new Locale(mockLanguages, 'id');
    monthName = locale.getMessage(`monthFull/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCStringNeedPadding, 'DATE_TIME_LONG', locale)).toBe(`${padZero(dateUTCNeedPadding.getDate())} ${monthName} ${dateUTCNeedPadding.getFullYear()}, ${padZero(dateUTCNeedPadding.getHours())}:${padZero(dateUTCNeedPadding.getMinutes())}`);
  });
  it('should render DATE_TIME_SHORT in specified language', () => {
    let locale = new Locale(mockLanguages, 'en');
    let monthName = locale.getMessage(`monthShort/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCString, 'DATE_TIME_SHORT', locale)).toBe(`${padZero(dateUTC.getDate())} ${monthName} ${dateUTC.getFullYear()}, ${padZero(dateUTC.getHours())}:${padZero(dateUTC.getMinutes())}`);

    locale = new Locale(mockLanguages, 'id');
    monthName = locale.getMessage(`monthShort/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCStringNeedPadding, 'DATE_TIME_SHORT', locale)).toBe(`${padZero(dateUTCNeedPadding.getDate())} ${monthName} ${dateUTCNeedPadding.getFullYear()}, ${padZero(dateUTCNeedPadding.getHours())}:${padZero(dateUTCNeedPadding.getMinutes())}`);
  });
  it('should render DATE_SHORT in specified language', () => {
    let locale = new Locale(mockLanguages, 'en');
    let monthName = locale.getMessage(`monthShort/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCString, 'DATE_SHORT', locale)).toBe(`${padZero(dateUTC.getDate())} ${monthName} ${dateUTC.getFullYear()}`);

    locale = new Locale(mockLanguages, 'id');
    monthName = locale.getMessage(`monthShort/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCStringNeedPadding, 'DATE_SHORT', locale)).toBe(`${padZero(dateUTCNeedPadding.getDate())} ${monthName} ${dateUTCNeedPadding.getFullYear()}`);
  });
  it('should render DATE_LONG in specified language', () => {

    let locale = new Locale(mockLanguages, 'en');
    let monthName = locale.getMessage(`monthFull/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCString, 'DATE_LONG', locale)).toBe(`${padZero(dateUTC.getDate())} ${monthName} ${dateUTC.getFullYear()}`);

    locale = new Locale(mockLanguages, 'id');
    monthName = locale.getMessage(`monthFull/${MONTH_NAMES[dateUTC.getMonth()]}`);
    expect(formatDateTime(dateUTCStringNeedPadding, 'DATE_LONG', locale)).toBe(`${padZero(dateUTCNeedPadding.getDate())} ${monthName} ${dateUTCNeedPadding.getFullYear()}`);
  });

  it('should return true if the date is today', () => {
    let today = new Date();
    let result = isToday(today);
    expect(result).toBe(true);

    let date = new Date('2016-10-20T09:27:10Z');
    result = isToday(date);
    expect(result).toBe(false);
  });
});
