// @flow

const {describe, it} = global;

import expect from 'expect';

import formatUrlEncoded from '../formatUrlEncoded';

describe('formatUrlEncoded test', () => {
  it('should change an object to formatUrlEncoded', () => {
    let obj = {
      nickname: '123',
      password: 'asd',
    };
    let obj2 = {
      nickname: '123',
    };
    let result = formatUrlEncoded(obj);
    let result2 = formatUrlEncoded(obj2);
    expect(result).toEqual(`nickname=123&password=asd`);
    expect(result2).toEqual(`nickname=123`);
  });
  it('Should remove special character on key and value', () => {
    let ob = {
      nick_name: '1=2',
    };
    let res = formatUrlEncoded(ob);
    expect(res).toEqual(`nick_name=1%3D2`);
  });
  it('Should handle array values', () => {
    let input = {
      foo: '123',
      bar: ['asd', 'qwe'],
    };
    let actual = formatUrlEncoded(input);
    let expected = 'foo=123&bar[]=asd&bar[]=qwe';
    expect(actual).toBe(expected);
  });
});
