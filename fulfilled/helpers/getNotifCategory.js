// @flow

export default function getNotifCategory(category: string) {
  let categoryTempArray = category.split(':');
  return categoryTempArray[categoryTempArray.length - 1];

}
