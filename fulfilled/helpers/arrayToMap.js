// @flow

const DEFAULT_KEY = 'id';
export default function arrayToMap<T>(prevMap: Map<string, T> | null, array: Array<T>, key: string = DEFAULT_KEY): Map<string, T> {
  let map = new Map(prevMap);
  for (let item of array) {
    let id = item != null && typeof item === 'object' ? String(item[key]) : '';
    map.set(id, item);
  }
  return map;
}
