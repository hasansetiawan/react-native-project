// @flow
type Callback = (isMatch: boolean, piece: string) => void;

export default function findWords(string: string, regex: RegExp, fn: Callback) {
  let lastIndex = 0;
  string.replace(regex, (matchPiece, matchIndex) => {
    let nonMatchPiece = string.slice(lastIndex, matchIndex);
    if (nonMatchPiece.length) {
      fn(false, nonMatchPiece);
    }
    fn(true, matchPiece);
    lastIndex = matchIndex + matchPiece.length;
    return ''; // Make Flow happy
  });
  let lastPiece = string.slice(lastIndex);
  fn(false, lastPiece);
}


// Hello world!
//       -----
