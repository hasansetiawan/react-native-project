// @flow

import pkg from '../../package.json';

export default function getVersion() {
  let {version, buildTime} = pkg;
  return {version, buildTime};
}
