import {Dimensions} from 'react-native';
import modulate from './modulator';
import {
  SMALLEST_SCREEN,
  SMALLEST_FONT,
  BIGGEST_SCREEN,
  BIGGEST_FONT,
} from '../constants/Size';

export function getScreenSize() {
  let {height, width} = Dimensions.get('window');
  return {height, width};
}

export function getFontSize() {
  let {width} = Dimensions.get('window');
  let font = {
    start: SMALLEST_FONT,
    end: BIGGEST_FONT,
  };
  let screen = {
    start: SMALLEST_SCREEN,
    end: BIGGEST_SCREEN,
  };
  let normal = modulate(width, screen, font);
  let plus = (value: number) => {
    return normal + value;
  };
  let times = (value: number) => {
    return normal * value;
  };
  return {normal, plus, times};
}
