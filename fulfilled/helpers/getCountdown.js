// @flow

export default function getCountdown(now: Date = new Date()) {
  let hours = 24;
  let minutes = 0;
  let seconds = 0;

  seconds -= now.getSeconds();
  minutes -= now.getMinutes();
  hours -= now.getHours();

  if (seconds < 0) {
    minutes -= 1;
    seconds += 60;
  }

  if (minutes < 0) {
    hours -= 1;
    minutes += 60;
  }

  return {hours, minutes, seconds};
}
