// @flow
import type {Action} from '../types/Action';

export default function appStateReducer(appState: string, action: Action) {
  if (appState == null) {
    appState = '';
  }
  switch (action.type) {
    case 'APP_STATE_CHANGED': {
      return action.newAppState;
    }
    default: {
      return appState;
    }
  }
}
