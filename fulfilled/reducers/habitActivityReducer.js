// @flow
import arrayToMap from '../helpers/arrayToMap';

import type {HabitActivityReducer} from '../types/HabitActivity';
import type {Action} from '../types/Action';

let initialState = {
  habitActivityList: new Map(),
};

export default function habitActivityReducer(habitActivities: HabitActivityReducer, action: Action) {
  if (habitActivities == null) {
    return initialState;
  }
  switch (action.type) {
    case 'HABIT_ACTIVITY_LIST_RECEIVED': {
      let {habitActivityList} = habitActivities;
      let newHabitActivities = arrayToMap(habitActivityList, action.habitActivityList);
      return {
        habitActivityList: newHabitActivities,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return habitActivities;
  }
}
