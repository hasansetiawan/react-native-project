// @flow
import type {Action} from '../types/Action';

export default function resetPasswordReducer(showResetPasswordConfirmation: boolean, action: Action) {
  if (showResetPasswordConfirmation == null) {
    return false;
  }
  switch (action.type) {
    case 'SHOW_RESET_PASSWORD_CONFIRMATION': {
      return true;
    }
    case 'CLOSE_RESET_PASSWORD_CONFIRMATION': {
      return false;
    }
    default: {
      return showResetPasswordConfirmation;
    }
  }
}
