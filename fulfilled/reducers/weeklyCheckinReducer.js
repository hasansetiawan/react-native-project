// @flow
import type {Action} from '../types/Action';
import type {WeeklyCheckinState} from '../types/WeeklyCheckin';

let initialState = {
  showWeeklyCheckinDialog: false,
};

export default function weeklyCheckinReducer(weeklyCheckinState: WeeklyCheckinState, action: Action) {
  if (weeklyCheckinState == null) {
    return initialState;
  }
  switch (action.type) {
    case 'WEEKLY_CHECKIN_STATUS_RECEIVED': {
      return {
        ...weeklyCheckinState,
        weeklyCheckinStatus: {...action.weeklyCheckinStatus},
      };
    }
    case 'SHOW_WEEKLY_CHECKIN_DIALOG': {
      return {
        ...weeklyCheckinState,
        showWeeklyCheckinDialog: true,
      };
    }
    case 'HIDE_WEEKLY_CHECKIN_DIALOG': {
      return {
        ...weeklyCheckinState,
        showWeeklyCheckinDialog: false,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: {
      return weeklyCheckinState;
    }
  }
}
