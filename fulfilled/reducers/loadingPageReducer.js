// @flow
import type {LoadingPage} from '../types/LoadingPage';
import type {Action} from '../types/Action';

let initialState = {
  challengePage: false,
  notificationPage: false,
  chatPage: false,
  communityFeedPage: false,
  postHistoryPage: false,
  programHistoryPage: false,
  programListPage: false,
  createPostPage: false,
  loginPage: false,
  signUpPage: false,
  resetPasswordPage: false,
  notificationDetailPage: false,
  communityFeedRefresh: false,
  completeHabitActivity: false,
  coachProfile: false,
  changeProfilePicturePage: false,
  userPostsHistoryRefresh: false,
  selectedProgramPage: false,
  registerSubscription: false,
  weeklyCheckinForm: false,
  notificationRefresh: false,
};

export default function loadingPageReducer(isLoading: LoadingPage, action: Action) {
  if (isLoading == null) {
    return initialState;
  }
  switch (action.type) {
    case 'LOADING_STARTED': {
      let {loadingPages} = action;
      let newIsLoading = {};
      if (Array.isArray(loadingPages)) {
        for (let page of loadingPages) {
          newIsLoading[page] = true;
        }
      } else {
        newIsLoading[loadingPages] = true;
      }
      return {
        ...isLoading,
        ...newIsLoading,
      };
    }
    case 'LOADING_FINISHED': {
      let {loadingPages} = action;
      let newIsLoading = {};
      if (Array.isArray(loadingPages)) {
        for (let page of loadingPages) {
          newIsLoading[page] = false;
        }
      } else {
        newIsLoading[loadingPages] = false;
      }
      return {
        ...isLoading,
        ...newIsLoading,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return isLoading;
  }
}
