// @flow
import type {Action} from '../types/Action';
import type {ImageViewState} from '../types/ImageView';

let initialState = {
  uri: '',
  showModal: false,
};

export default function imageViewReducer(imageView: ImageViewState, action: Action) {
  if (imageView == null) {
    return initialState;
  }
  switch (action.type) {
    case 'IMAGE_URI_RECEIVED': {
      return {
        uri: action.uri,
        showModal: true,
      };
    }
    case 'CLOSE_IMAGE_VIEW_MODAL': {
      return {
        ...imageView,
        showModal: false,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: {
      return imageView;
    }
  }
}
