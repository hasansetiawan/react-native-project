// @flow
import type {Action} from '../types/Action';
import type {LoginStatus} from '../types/LoginStatus';

export default function loginStatusReducer(loginStatus: LoginStatus, action: Action) {
  if (loginStatus == null) {
    return {
      isChecking: true,
      isLogin: null,
      isSyncUserFinish: false,
    };
  }
  switch (action.type) {
    case 'CHECK_LOGIN_FINISHED': {
      return {
        isChecking: false,
        isLogin: action.isLogin,
        isSyncUserFinish: false,
      };
    }
    case 'SYNC_USER_FINISHED': {
      return {
        isChecking: false,
        isLogin: true,
        isSyncUserFinish: true,
      };
    }
    default: {
      return loginStatus;
    }
  }
}
