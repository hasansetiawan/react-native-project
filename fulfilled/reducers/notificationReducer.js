// @flow
import arrayToMap from '../helpers/arrayToMap';

import type {NotificationReducer} from '../types/Notification';
import type {Action} from '../types/Action';

let initialState = {
  notificationList: new Map(),
  notificationDetailID: null,
  page: 1,
  isLoadingMore: false,
};

export default function notificationReducer(notification: NotificationReducer, action: Action) {
  if (notification == null) {
    return initialState;
  }
  switch (action.type) {
    case 'NOTIF_LIST_RECEIVED': {
      let newNotificationList = arrayToMap(notification.notificationList, action.notifList);
      return {
        ...notification,
        notificationList: newNotificationList,
        notificationDetailID: notification.notificationDetailID,
        page: (action.page != null) ? action.page : notification.page,
      };
    }
    case 'NOTIF_DETAIL_CHANGED': {
      return {
        ...notification,
        notificationDetailID: action.notificationDetailID,
      };
    }
    case 'FETCH_MORE_NOTIFS_STARTED': {
      return {
        ...notification,
        isLoadingMore: true,
      };
    }
    case 'FETCH_MORE_NOTIFS_FINISHED': {
      return {
        ...notification,
        isLoadingMore: false,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return notification;
  }
}
