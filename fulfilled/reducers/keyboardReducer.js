// @flow
import type {Action} from '../types/Action';
import type {KeyboardState} from '../types/KeyboardState';

let initialState = {
  isKeyboardShown: false,
  keyboardSpace: 0,
};

export default function keyboardReducer(keyboardState: KeyboardState, action: Action) {
  if (keyboardState == null) {
    return initialState;
  }
  switch (action.type) {
    case 'KEYBOARD_DID_SHOW': {
      return {
        isKeyboardShown: true,
        keyboardSpace: action.keyboardSpace,
      };
    }
    case 'KEYBOARD_DID_HIDE': {
      return initialState;
    }
    default: {
      return keyboardState;
    }
  }
}
