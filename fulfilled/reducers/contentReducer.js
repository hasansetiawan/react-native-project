// @flow

import type {Action} from '../types/Action';
import type {ContentReadingMaterial} from '../types/ContentReadingMaterial';

let initialState = new Map();

export default function contentReducer(contentList: Map<string, ContentReadingMaterial>, action: Action) {
  if (contentList == null) {
    return initialState;
  }
  switch (action.type) {
    case 'CONTENT_RECEIVED': {
      let {content} = action;
      let newContentList = new Map(contentList);
      newContentList.set(content.id, content);
      return newContentList;
    }
    case 'RESET': {
      return initialState;
    }
    default: return contentList;
  }
}
