// @flow

import type {Action} from '../types/Action';

let initialState = false;

export default function drawerReducer(isOpen: boolean, action: Action) {
  if (isOpen == null) {
    return false;
  }
  switch (action.type) {
    case 'DRAWER_CLOSED': {
      return false;
    }
    case 'DRAWER_OPENED': {
      return true;
    }
    case 'RESET': {
      return initialState;
    }
    default: return isOpen;
  }
}
