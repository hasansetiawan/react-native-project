// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {ProgramState} from '../types/Program';
import type {Action} from '../types/Action';

let initialState = {
  programList: new Map(),
  habitList: new Map(),
  currentProgram: '',
  programRecordID: '',
};

export default function programReducer(programState: ProgramState, action: Action) {
  if (programState == null) {
    return initialState;
  }
  let {programList, selectedProgramID} = programState;
  switch (action.type) {
    case 'PROGRAM_LIST_RECEIVED': {
      let {programList, habitList} = action;
      let newProgramList = arrayToMap(programState.programList, programList);
      let newHabitList = arrayToMap(programState.habitList, habitList, 'text');
      return {
        ...programState,
        programList: newProgramList,
        habitList: newHabitList,
      };
    }
    case 'PROGRAM_RECORD_RECEIVED': {
      let {programRecordID, currentProgram} = action;
      return {
        ...programState,
        programRecordID,
        currentProgram,
      };
    }
    case 'PROGRAM_ACTIVATED': {
      let newProgramList = new Map(programList);
      let selectedProgram = newProgramList.get(selectedProgramID);
      newProgramList.set(selectedProgramID, {
        ...selectedProgram,
        isActive: true,
      });
      return {
        ...programState,
        programList: newProgramList,
        selectedProgramID,
      };
    }
    case 'PROGRAM_DEACTIVATED': {
      let newProgramList = new Map(programList);
      let selectedProgram = newProgramList.get(selectedProgramID);
      newProgramList.set(selectedProgramID, {
        ...selectedProgram,
        isActive: false,
      });
      return {
        ...programState,
        programList: newProgramList,
        selectedProgramID,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return programState;
  }
}
