/* @flow */

import type {ToastType} from '../types/Toast';

let defaultState: ToastType = {
  toastMessage: null,
};

function toastReducer(state: ToastType = defaultState, action: Object): ToastType {
  switch (action.type) {
    case 'SHOW_TOAST': {
      return {
        toastMessage: action.toastMessage,
      };
    }
    case 'HIDE_TOAST': {
      return defaultState;
    }
    case 'RESET': {
      return defaultState;
    }
  }
  return state;
}

export default toastReducer;
