// @flow
import type {Action} from '../types/Action';

export default function productReducer(productIDList: Array<string>, action: Action) {
  if (productIDList == null) {
    return [];
  }
  switch (action.type) {
    case 'PRODUCT_ID_LIST_RECEIVED': {
      return action.productIDList;
    }
    default: {
      return productIDList;
    }
  }
}
