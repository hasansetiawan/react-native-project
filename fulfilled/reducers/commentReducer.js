// @flow

import type {Comment} from '../types/Comment';
import type {Action} from '../types/Action';
import arrayToMap from '../helpers/arrayToMap';

let initialState = new Map();

export default function commentReducer(comments: Map<string, Comment>, action: Action) {
  if (comments == null) {
    return initialState;
  }
  switch (action.type) {
    case 'COMMENT_LIST_RECEIVED': {
      let {commentList} = action;
      let newComments = arrayToMap(comments, commentList);
      return newComments;
    }
    case 'COMMENT_ADDED': {
      let newComments = new Map(comments);
      newComments.set(action.comment.id, action.comment);
      return newComments;
    }
    case 'ADD_COMMENT_SUCCESS': {
      let newComments = new Map(comments);
      newComments.delete(action.optimisticID);
      newComments.set(action.comment.id, action.comment);
      return newComments;
    }
    case 'RESET': {
      return initialState;
    }
    default: return comments;
  }
}
