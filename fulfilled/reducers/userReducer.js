// @flow

import type {User} from '../types/User';
import type {Action} from '../types/Action';
import {
  NOTIFICATION_PAGE,
  COMMUNITY_PAGE,
  CHAT_PAGE,
} from '../constants/NavBarPageName';

type InitialUser = {
  id: string;
  newNotificationsCount: number;
  isPremiumMember: boolean;
  chatsCount: number;
};

const MAX_NOTIF = 99;

let initialState = {
  id: '',
  newNotificationsCount: 0,
  isPremiumMember: false,
  chatsCount: 0,
};

export default function userReducer(user: User | InitialUser, action: Action) {
  if (user == null) {
    return initialState;
  }
  switch (action.type) {
    case 'USER_RECEIVED': {
      let newUser = {...user, ...action.user};
      let {newNotificationsCount, newFeedCount, chatsCount} = newUser;
      if (newNotificationsCount > MAX_NOTIF) {
        newUser.newNotificationsCount = MAX_NOTIF;
      }
      if (newFeedCount > MAX_NOTIF) {
        newUser.newFeedCount = MAX_NOTIF;
      }
      if (chatsCount > MAX_NOTIF) {
        newUser.chatsCount = MAX_NOTIF;
      }
      return newUser;
    }
    case 'NOTIFICATION_COUNT_REMOVED': {
      let newUser = {
        ...user,
      };
      switch (action.pageName) {
        case NOTIFICATION_PAGE: {
          newUser.newNotificationsCount = 0;
          return newUser;
        }
        case COMMUNITY_PAGE: {
          newUser.newFeedCount = 0;
          return newUser;
        }
        case CHAT_PAGE: {
          newUser.chatsCount = 0;
          return newUser;
        }
        default: return newUser;
      }
    }
    case 'USER_ID_TOKEN_RECEIVED': {
      return {
        ...user,
        token: action.userToken,
        id: action.userID,
      };
    }
    case 'NEW_NOTIF_RECEIVED': {
      return {
        ...user,
        newNotificationsCount: user.newNotificationsCount + 1,
      };
    }
    case 'NEW_CHAT_NOTIF_RECEIVED': {
      return {
        ...user,
        chatsCount: user.chatsCount + 1,
      };
    }
    case 'SUBSCRIBE_STATUS_RECEIVED': {
      return {
        ...user,
        isPremiumMember: action.subscribed,
        subscribed: action.subscribed,
        premiumMembershipExpiry: action.expiry,
      };
    }
    case 'PROGRAM_RECORD_RECEIVED': {
      return {
        ...user,
        programRecords: [action.programRecordID],
      };
    }
    case 'CHAT_WORD_COUNT_UPDATED': {
      return {
        ...user,
        wordCountLeft: action.wordCountLeft,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return user;
  }
}
