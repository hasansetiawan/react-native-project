// @flow
import type {Action} from '../types/Action';
import type {ProfilePicture} from '../types/User';

export default function profilePictureReducer(profilePictureList: Map<string, ProfilePicture>, action: Action) {
  if (profilePictureList == null) {
    return new Map();
  }
  switch (action.type) {
    case 'PROFILE_PICTURE_RECEIVED': {
      let {userID, profilePicture} = action;
      let newProfilePictureList = new Map(profilePictureList);
      newProfilePictureList.set(userID, profilePicture);
      return newProfilePictureList;
    }
    case 'PROFILE_PICTURE_UPDATED': {
      let newProfilePictureList = new Map(profilePictureList);
      let newProfilePicture = {
        lowResolutionUrl: action.profilePicture.lowResolution,
        standardResolutionUrl: action.profilePicture.standardResolution,
      };
      newProfilePictureList.set(action.id, newProfilePicture);
      return newProfilePictureList;
    }
    default: {
      return profilePictureList;
    }
  }
}
