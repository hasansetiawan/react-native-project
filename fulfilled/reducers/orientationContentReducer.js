// @flow

import type {Action} from '../types/Action';
import type {ContentReadingMaterial} from '../types/ContentReadingMaterial';

let initialState = new Map();

export default function orientationContentReducer(orientationContentList: Map<string, ContentReadingMaterial>, action: Action) {
  if (orientationContentList == null) {
    return initialState;
  }
  switch (action.type) {
    case 'ORIENTATION_CONTENT_RECEIVED': {
      let {orientationContent} = action;
      let newOrientationContentList = new Map(orientationContentList);
      newOrientationContentList.set(orientationContent.id, orientationContent);
      return newOrientationContentList;
    }
    case 'COMPLETE_ORIENTATION_SUCCESS': {
      return new Map();
    }
    case 'RESET': {
      return initialState;
    }
    default: return orientationContentList;
  }
}
