// @flow
import arrayToMap from '../helpers/arrayToMap';

import type {Action} from '../types/Action';
import type {CommunityMember} from '../types/User';

let initialState = new Map();
export default function communityMemberReducer(members: Map<string, CommunityMember>, action: Action) {
  if (members == null) {
    return initialState;
  }
  switch (action.type) {
    case 'COMMUNITY_MEMBERS_RECEIVED': {
      let {memberList} = action;
      let newMembers = arrayToMap(members, memberList);
      return newMembers;
    }
    case 'RESET': {
      return initialState;
    }
    default: return members;
  }
}
