// @flow

import {NavigationExperimental} from 'react-native';
import type {Navigation} from '../types/Navigator';
import type {Action} from '../types/Action';
import {
  CHALLENGE_PAGE,
  INITIAL_NAVBAR_INDEX,
} from '../constants/NavBarPageName';

const {
  StateUtils: NavigationStateUtils,
} = NavigationExperimental;

export const initialData = {
  navbar: {
    index: 0,
    selectedIndex: INITIAL_NAVBAR_INDEX,
    key: 'NavbarFullfilledApp',
    routes: [
      {key: CHALLENGE_PAGE},
    ],
  },
  detail: { // will be used later
    showDetailView: false,
    index: 0, // initial Index
    key: 'DetailFullfilledApp',
    currentDetailHeader: null,
    routes: [{key: 'initialDetailRoute'}],
  },
  showExitDialog: false,
};

export default function navigationReducer(navigation: Navigation, action: Action) {
  if (navigation == null) {
    return initialData;
  }
  switch (action.type) {
    case 'ROUTE_PUSHED': {
      let {detail} = navigation;
      return {
        ...navigation,
        detail: {
          ...NavigationStateUtils.push(detail, {...action.route}),
          currentDetailHeader: action.route.title,
        },
      };
    }
    case 'ROUTE_REPLACED': {
      let {detail} = navigation;
      let {showDetailView} = detail;
      if (showDetailView === false) {
        showDetailView = true;
      }
      return {
        ...navigation,
        detail: {
          ...NavigationStateUtils.replaceAtIndex(detail, 0, {...action.route}),
          showDetailView,
          currentDetailHeader: action.route.title,
        },
      };
    }
    case 'ROUTE_POPPED': {
      let detail;
      if (navigation.detail.routes.length === 1) {
        detail = {
          ...NavigationStateUtils.pop(navigation.detail),
          showDetailView: false,
          currentDetailHeader: null,
        };
      } else {
        let newDetail = NavigationStateUtils.pop(navigation.detail);
        let {index} = newDetail;
        detail = {
          ...newDetail,
          currentDetailHeader: newDetail.routes[index].title,
        };
      }
      return {
        ...navigation,
        detail,
      };
    }
    case 'NAVBAR_CHANGED': {
      return {
        ...navigation,
        detail: navigation.detail,
        navbar: {
          ...NavigationStateUtils.replaceAtIndex(navigation.navbar, 0, action.route),
          selectedIndex: action.index,
        },
      };
    }
    case 'EXIT_DIALOG_HIDE': {
      return {
        ...navigation,
        showExitDialog: false,
      };
    }
    case 'EXIT_DIALOG_OPENED': {
      return {
        ...navigation,
        showExitDialog: true,
      };
    }
    case 'RESET': {
      return initialData;
    }
    default: return navigation;
  }
}
