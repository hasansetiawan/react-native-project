// @flow

import type {HabitReducer} from '../types/Habit';
import type {Action} from '../types/Action';

let initialState = {
  currentHabit: {},
  progressPercentage: 0,
  isCooldown: false,
  showCompleteContent: false,
};

export default function habitReducer(habit: HabitReducer, action: Action) {
  if (habit == null) {
    return initialState;
  }
  switch (action.type) {
    case 'CURRENT_HABIT_RECEIVED': {
      let newCurrentHabit = action.habit;
      let {completedToday, daysCompleted, completionTarget} = newCurrentHabit;
      let progressPercentage = Math.round((daysCompleted / completionTarget) * 100);
      let isCooldown = (completedToday === 'done' || completedToday === true) ? true : false;
      return {
        ...habit,
        currentHabit: newCurrentHabit,
        progressPercentage,
        isCooldown,
        showCompleteContent: habit.showCompleteContent,
      };
    }
    case 'SHOW_COMPLETE_CONTENT': {
      return {
        ...habit,
        showCompleteContent: true,
      };
    }
    case 'CLOSE_COMPLETE_CONTENT': {
      return {
        ...habit,
        showCompleteContent: false,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return habit;
  }
}
