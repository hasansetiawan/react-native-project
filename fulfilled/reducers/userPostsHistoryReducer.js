// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {PostHistoryReducer} from '../types/PostHistory';
import type {Action} from '../types/Action';

let initialState = {
  postList: new Map(),
  userID: '',
  page: 1,
  lastPostID: '',
  isLoadingMore: false,
};

export default function userPostsHistoryReducer(postsHistory: PostHistoryReducer, action: Action) {
  if (postsHistory == null) {
    return initialState;
  }
  switch (action.type) {
    case 'USER_POST_HISTORY_RECEIVED': {
      let {userID} = postsHistory;
      let {postList} = action;
      let newPostList;
      let newLastPostID = postList[postList.length - 1].id;
      let newPage = (action.page) ? action.page : postsHistory.page;
      if (userID !== action.userID) {
        newPostList = arrayToMap(new Map(), action.postList);
        userID = action.userID;
      } else {
        newPostList = arrayToMap(postsHistory.postList, action.postList);
      }
      return {
        postList: newPostList,
        userID,
        page: newPage,
        lastPostID: newLastPostID,
        isLoadingMore: postsHistory.isLoadingMore,
      };
    }
    case 'FETCH_MORE_USER_POST_HISTORY_START': {
      return {
        ...postsHistory,
        isLoadingMore: true,
      };
    }
    case 'FETCH_MORE_USER_POST_HISTORY_END': {
      return {
        ...postsHistory,
        isLoadingMore: false,
      };
    }
    case 'COMMENT_ADDED': {
      let newPostList = new Map(postsHistory.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        post.comments.push(action.comment.id);
        newPostList.set(action.postID, post);
      }
      return {
        ...postsHistory,
        postList: newPostList,
      };
    }
    case 'LIKE_ADDED': {
      let newPostList = new Map(postsHistory.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        post.likers.push(action.userID);
        newPostList.set(action.postID, post);
      }
      return {
        ...postsHistory,
        postList: newPostList,
      };
    }
    case 'LIKE_REMOVED': {
      let newPostList = new Map(postsHistory.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        let {userID} = action;
        let likers = post.likers.filter((liker) => {
          return liker !== userID;
        });

        let newPost = {
          ...post,
          likers,
        };
        newPostList.set(action.postID, newPost);
      }
      return {
        ...postsHistory,
        postList: newPostList,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return postsHistory;
  }
}
