// @flow

import type {Coach} from '../types/Coach';
import type {Action} from '../types/Action';

let initialState = {};

export default function coachReducer(coach: Coach, action: Action) {
  if (coach == null) {
    return initialState;
  }
  switch (action.type) {
    case 'COACH_RECEIVED': {
      let newCoach = action.coach;
      return newCoach;
    }
    case 'COMPLETE_ORIENTATION_SUCCESS': {
      let newCoach = coach;
      if (newCoach.orientationContentIDs != null) {
        delete newCoach.orientationContentIDs;
      }
      return newCoach;
    }
    case 'RESET': {
      return initialState;
    }
    default: return coach;
  }
}
