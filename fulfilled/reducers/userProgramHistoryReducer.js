// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {Program} from '../types/Program';
import type {Action} from '../types/Action';

let initialState = new Map();

export default function userProgramHistoryReducer(programHistory: Map<string, Program>, action: Action) {
  if (programHistory == null) {
    return initialState;
  }
  switch (action.type) {
    case 'USER_PROGRAM_HISTORY_RECEIVED': {
      let {programHistoryList} = action;
      let newProgramHistory = arrayToMap(programHistory, programHistoryList);
      return newProgramHistory;
    }
    case 'FETCH_USER_PROGRAM_HISTORY_FAILED': {
      return programHistory;
    }
    case 'RESET': {
      return initialState;
    }
    default: return programHistory;
  }
}
