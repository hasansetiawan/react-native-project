/* @flow */

import type {PopupState} from '../types/Popup';
import type {Action} from '../types/Action';
let initialState: PopupState = {
  isShowing: false,
  content: [],
};

export default function popupModalReducer(state: PopupState = initialState, action: Action) {
  switch (action.type) {
    case 'SHOW_POPUP': {
      let newContent = [
        ...state.content,
        action.content,
      ];
      return {
        isShowing: true,
        content: newContent,
      };
    }
    case 'HIDE_POPUP': {
      let newContent = [
        ...state.content,
      ];

      if (newContent.length > 1) {
        newContent.splice(-1, 1);
        return {
          isShowing: true,
          content: newContent,
        };
      } else {
        return initialState;
      }
    }
    case 'RESET': {
      return initialState;
    }
  }
  return state;
}
