// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {Post, NewPostFormData} from '../types/Post';
import type {Action} from '../types/Action';

import DEFAULT_FORM_DATA from '../constants/DefaultFormData';

type Posts = {
  formData: NewPostFormData;
  postList: Map<string, Post>;
  lastPostID: string;
  page: number;
};

let initialState = {
  formData: DEFAULT_FORM_DATA,
  postList: new Map(),
  lastPostID: '',
  page: 1,
};

export default function postReducer(posts: Posts, action: Action) {
  if (posts == null) {
    return initialState;
  }
  switch (action.type) {
    case 'POSTS_RECEIVED': {
      let {formData} = posts;
      let {postList} = action;
      let newLastPostID = postList[postList.length - 1].id;
      let newPage = (action.page) ? action.page : posts.page;
      let newPostList = arrayToMap(posts.postList, postList);
      return {
        formData,
        lastPostID: newLastPostID,
        page: newPage,
        postList: newPostList,
      };
    }
    case 'FETCH_POSTS_FAILED': {
      return posts;
    }
    case 'POST_CREATED': {
      let {formData} = posts;
      let newPost = {
        id: action.id,
        createdAt: action.createdAt,
        imageWidth: null,
        imageHeight: null,
        type: formData.category,
        privacy: action.privacy,
        isLiked: false,
        user: action.user.id,
        posterNickname: action.user.nickname,
        comments: [],
        likers: [],
        image: (action.imageData != null) ? {
          standardResolution: action.imageData.uri,
          lowResolution: action.imageData.uri,
          thumbnailResolution: action.imageData.uri,
        } : null,
      };
      switch (formData.category) {
        case 'ActivityLog': {
          let content = `Activity: ${formData.content}`;
          if (Number(formData.hour) > 0 || Number(formData.minute) > 0) {
            content += ' for';
            if (Number(formData.hour) > 0) {
              content += ` ${formData.hour} hour(s)`;
            }
            if (Number(formData.minute)) {
              content += ` ${formData.minute} minute(s)`;
            }
          }
          newPost = {
            ...newPost,
            displayText: `${content} \n\n ${formData.comment}`,
          };
          break;
        }
        case 'Meal': {
          newPost = {
            ...newPost,
            displayText: `${formData.selectedTime}: ${formData.content}`,
          };
          break;
        }
        case 'Drink': {
          newPost = {
            ...newPost,
            displayText: `${formData.content}: ${formData.selectedAmount}`,
          };
          break;
        }
        case 'Note': {
          newPost = {
            ...newPost,
            displayText: `${formData.content}`,
          };
          break;
        }
        case 'Measurement': {
          newPost = {
            ...newPost,
            displayText: `${formData.comment} \n\nWeight: ${formData.weight} \nWaistline: ${formData.waistline} \nHip: ${formData.hip}`,
          };
          break;
        }
        default: newPost = null; break;
      }
      if (newPost != null) {
        let newPosts = new Map(posts.postList);
        newPosts.set(newPost.id, newPost);
        return {
          ...posts,
          postList: newPosts,
        };
      } else {
        return posts;
      }
    }
    case 'SEND_POST_SUCCESS': {
      let {optimisticID, post} = action;
      let newPostList = new Map(posts.postList);
      newPostList.delete(optimisticID);
      newPostList.set(post.id, post);

      let {lastPostID, page} = posts;
      return {
        lastPostID,
        page,
        formData: posts.formData,
        postList: newPostList,
      };
    }
    case 'FORMDATA_CHANGED': {
      let {newFormData} = action;
      return {
        ...posts,
        formData: newFormData,
      };
    }
    case 'FORM_OPENED': {
      let {category} = action;
      return {
        ...posts,
        formData: DEFAULT_FORM_DATA[category],
      };
    }
    case 'COMMENT_ADDED': {
      let newPostList = new Map(posts.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        post.comments.push(action.comment.id);
        newPostList.set(action.postID, post);
      }
      return {
        ...posts,
        postList: newPostList,
      };
    }
    case 'LIKE_ADDED': {
      let newPostList = new Map(posts.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        post.likers.push(action.userID);
        newPostList.set(action.postID, post);
      }
      return {
        ...posts,
        postList: newPostList,
      };
    }
    case 'LIKE_REMOVED': {
      let newPostList = new Map(posts.postList);
      let post = newPostList.get(action.postID);
      if (post != null) {
        let {userID} = action;
        let likers = post.likers.filter((liker) => {
          return liker !== userID;
        });

        let newPost = {
          ...post,
          likers,
        };
        newPostList.set(action.postID, newPost);
      }
      return {
        ...posts,
        postList: newPostList,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return posts;
  }
}
