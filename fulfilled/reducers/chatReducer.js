// @flow

import type {ChatReducer} from '../types/Chat';
import type {Action} from '../types/Action';

let initialState = {
  chatList: new Map(),
  page: 1,
  inputChat: '',
  showWordCountLeftStatus: true,
};

export default function chatReducer(chatState: ChatReducer, action: Action) {
  if (chatState == null) {
    return initialState;
  }
  switch (action.type) {
    case 'CHAT_LIST_RECEIVED': {
      let newChatList = new Map(chatState.chatList);
      for (let key of Object.keys(action.chatList)) {
        let chat = action.chatList[key];
        newChatList.set(chat.id, chat);
      }
      return {
        ...chatState,
        page: (action.page) ? action.page : chatState.page,
        chatList: newChatList,
      };
    }
    case 'CHAT_ADDED': {
      let newChat = {
        id: action.id,
        createdAt: action.createdAt,
        displayText: chatState.inputChat,
        text: chatState.inputChat,
        posterNickname: action.senderNickname,
        recipientNickname: action.recipientNickname,
        userIsPoster: true,
        privacy: 'private',
        createdAtInSeconds: (Date.now() / 1000),
      };
      let newChatList = new Map(chatState.chatList);
      newChatList.set(newChat.id, newChat);
      return {
        ...chatState,
        chatList: newChatList,
        inputChat: '',
      };
    }
    case 'ADD_CHAT_SUCCESS': {
      let {optimisticID, newChat} = action;
      let newChatList = new Map(chatState.chatList);
      newChatList.delete(optimisticID);
      newChatList.set(newChat.id, newChat);
      return {
        ...chatState,
        chatList: newChatList,
      };
    }
    case 'INPUT_CHAT_CHANGED': {
      return {
        ...chatState,
        inputChat: action.input,
      };
    }
    case 'HIDE_WORD_COUNT_STATUS': {
      return {
        ...chatState,
        showWordCountLeftStatus: false,
      };
    }
    case 'SHOW_WORD_COUNT_STATUS': {
      return {
        ...chatState,
        showWordCountLeftStatus: true,
      };
    }
    case 'RESET': {
      return initialState;
    }
    default: return chatState;
  }
}
