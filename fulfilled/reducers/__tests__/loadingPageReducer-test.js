let {describe, it} = global;

import expect from 'expect';
import loadingPageReducer from '../loadingPageReducer';

describe('Loading Page Reducer', () => {
  it('should change loading state to true to the selected page', () => {
    let init = loadingPageReducer(null, null);
    let action = {
      type: 'LOADING_STARTED',
      loadingPages: 'challengePage',
    };
    let loadingState = loadingPageReducer(init, action);
    expect(loadingState).toEqual({
      ...init,
      challengePage: true,
    });

    action = {
      type: 'LOADING_STARTED',
      loadingPages: ['challengePage', 'loginPage'],
    };

    loadingState = loadingPageReducer(loadingState, action);
    expect(loadingState).toEqual({
      ...init,
      challengePage: true,
      loginPage: true,
    });
  });

  it('should change loading state to false to the selected page', () => {
    let init = loadingPageReducer(null, null);
    let action = {
      type: 'LOADING_STARTED',
      loadingPages: ['signUpPage', 'chatPage', 'communityFeedPage'],
    };
    let loadingState = loadingPageReducer(init, action);

    action = {
      type: 'LOADING_FINISHED',
      loadingPages: 'signUpPage',
    };

    loadingState = loadingPageReducer(loadingState, action);
    expect(loadingState).toEqual({
      ...init,
      communityFeedPage: true,
      chatPage: true,
      signUpPage: false,
    });

    action = {
      type: 'LOADING_FINISHED',
      loadingPages: ['communityFeedPage', 'chatPage'],
    };

    loadingState = loadingPageReducer(loadingState, action);
    expect(loadingState).toEqual({
      ...init,
      communityFeedPage: false,
      chatPage: false,
      signUpPage: false,
    });
  });
});
