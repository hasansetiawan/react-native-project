let {describe, it} = global;

import expect from 'expect';
import userReducer from '../userReducer';
import {
  COMMUNITY_PAGE,
  CHAT_PAGE,
  NOTIFICATION_PAGE,
} from '../../constants/NavBarPageName';

describe('User Reducer', () => {
  let user = {
    newFeedCount: 10,
    newNotificationsCount: 5,
    chatsCount: 20,
  };
  it('should remove notification count', () => {
    let newUser = userReducer(user, {
      type: 'NOTIFICATION_COUNT_REMOVED',
      pageName: COMMUNITY_PAGE,
    });

    expect(newUser.newFeedCount).toBe(0);
    expect(newUser.newNotificationsCount).toBe(5);
    expect(newUser.chatsCount).toBe(20);


    newUser = userReducer(user, {
      type: 'NOTIFICATION_COUNT_REMOVED',
      pageName: CHAT_PAGE,
    });

    expect(newUser.newFeedCount).toBe(10);
    expect(newUser.newNotificationsCount).toBe(5);
    expect(newUser.chatsCount).toBe(0);

    newUser = userReducer(user, {
      type: 'NOTIFICATION_COUNT_REMOVED',
      pageName: NOTIFICATION_PAGE,
    });

    expect(newUser.newFeedCount).toBe(10);
    expect(newUser.newNotificationsCount).toBe(0);
    expect(newUser.chatsCount).toBe(20);
  });
});
