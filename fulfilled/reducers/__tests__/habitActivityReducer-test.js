let {describe, it} = global;

import expect from 'expect';
import habitActivityReducer from '../habitActivityReducer';

describe('habitActivityReducer', () => {
  let initialHabitActivities = [
    {
      id: '57063f35ecd364019c000006',
      text: '0: 0: 20 menit: Mengatur makan kini begitu mudah!',
      completionTarget: 1,
      completionThreshold: null,
      type: 'Lesson',
      textPresentTense: null,
      completed: false,
      readingMaterial: {
        markdown: '',
      },
      userHabitRecordId: null,
      daysCompleted: 1,
      daysOnHabit: null,
      uncompletedPostId: null,
      habitCompletionMessageId: null,
      reminderText: '',
      programCategory: null,
      completedToday: false,
      hasInstructions: false,
      category: null,
      displayType: 'regular',
      questions: ['57063e42ecd364019c000004'],
    },
    {
      id: '57063f35ecd364019c000007',
      text: 'Minum yang banyak',
      completionTarget: 1,
      completionThreshold: null,
      type: 'Lesson',
      textPresentTense: null,
      completed: false,
      readingMaterial: {
        markdown: '',
      },
      userHabitRecordId: null,
      daysCompleted: 0,
      daysOnHabit: null,
      uncompletedPostId: null,
      habitCompletionMessageId: null,
      reminderText: '',
      programCategory: null,
      completedToday: false,
      hasInstructions: false,
      category: null,
      displayType: 'regular',
      questions: ['57063e42ecd364019c000004'],
    },
  ];

  it('should render new habitActivities when received', () => {
    let init = habitActivityReducer(null);
    let habitActivities = habitActivityReducer(init, {
      type: 'HABIT_ACTIVITY_LIST_RECEIVED',
      habitActivityList: initialHabitActivities,
    });

    expect(habitActivities.habitActivityList.size).toBe(2);
  });
});
