let {describe, it} = global;

import expect from 'expect';
import habitReducer from '../habitReducer';

describe('habitReducer', () => {
  let initialHabit = {
    text: 'Makan Penuh Kesadaran - Aku makan dengan perlahan hari Ini',
    completionTarget: 7,
    completion_threshold: 7,
    text_present_tense: '',
    completed: false,
    reading_material: null,
    id: '570340125a15c968dc000003',
    user_habit_record_id: '5836637debde51a53c0000ba',
    daysCompleted: 1,
    days_on_habit: null,
    _id: '570340125a15c968dc000003',
    uncompleted_post_id: null,
    habit_completion_message_id: '57350430e24e50c9be00001d',
    reminder_text: 'Transformasikan dirimu bersama Fulfilled.\nMemutuskan untuk berubah terkadang memang sulit. Biarkan Fulfilled menyelesaikannya.\nTemukan versi terbaik dirimu bersama Fulfilled.\nKamu pasti sudah lama menginginkan hidup yang sehat. Mari kita mulai.\nJangan ragu. Kami di sini untuk membantumu.\nMungkin kamu pernah mencoba dan gagal. Maka sekarang, kami ada untuk membantumu.\nSayangi dirimu. Mulailah bersama Fulfilled.\nPercayalah bahwa kita bisa berubah bersama komunitas Fulfilled. Mari bergabung.\nBerikan kami kesempatan untuk mendapatkan kepercayaanmu. Mulailah bersama Fulfilled.\nTransformasi memang membutuhkan waktu. Kami di sini. Bergabunglah.\nJadilah sedikit lebih baik hari ini dibanding kemarin. Mulailah bersama Fulfilled.\nSingkirkan rasa malu dan rasa bersalah, transformasikan dirimu.\nDukungan dan dorongan sama dengan keberhasilan di Fulfilled. Mari bergabung bersama kami.',
    program_category: 'nutrition',
    completedToday: 'done',
    has_instructions: false,
    category: 'core',
    strategy: '',
    activity_category: 'lesson',
    activity_parent_record_id: '5836637debde51a53c0000ba',
    activity_todo_id: '57063d9becd364de8e000004',
    activity_todo_content_id: '57063d9becd364de8e000003',
    activity_todo_text: 'Dari rasa turun ke hati',
    activity_id: null,
    activity_prompt_text: null,
    content_checkoff_id: '57064af1ecd3642a47000005',
    habit_ending_completion_content_id: null,
    display_type: 'show_all_lessons',
  };

  it('should update progress bar when user finish todays daily tips', () => {
    let init = habitReducer(null);
    let habitActivities = habitReducer(init, {
      type: 'CURRENT_HABIT_RECEIVED',
      habit: initialHabit,
    });

    expect(habitActivities.progressPercentage).toBe(Math.round((1 / 7) * 100));

  });
});
