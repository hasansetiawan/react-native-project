let {describe, it} = global;

import expect from 'expect';
import commentReducer from '../commentReducer';

describe('comment reducer', () => {
  it('should render new comments when received', () => {
    let initialComments = new Map();
    initialComments.set('999', {
      id: '999',
      text: 'old comment',
      user: {
        id: '172',
        nickname: 'Ricky',
      },
    });
    let newComments = [{
      id: '123',
      text: 'test comment',
      user: {
        id: '152',
        nickname: 'Audy',
      },
    }];
    let comments = commentReducer(initialComments, {type: 'COMMENT_LIST_RECEIVED', commentList: newComments});
    expect(comments.size).toBe(2);
    expect(comments.has('123')).toBe(true);
    expect(comments.has('999')).toBe(true);
  });
  it('should render new comments when added', () => {
    let initialComments = new Map();
    initialComments.set('123', {
      id: '123',
      text: 'test comment',
      user: {
        id: '900',
        nickname: 'Audy',
      },
    });

    let newComment = {
      id: '152',
      text: 'test comment',
      user: {
        id: '311',
        nickname: 'Daniel',
      },
    };
    let comments = commentReducer(initialComments, {type: 'COMMENT_ADDED', comment: newComment});
    expect(comments.size).toBe(2);
    expect(comments.has('123')).toBe(true);
    expect(comments.has('152')).toBe(true);
  });
});
