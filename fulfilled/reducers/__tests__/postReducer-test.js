// @flow
let {describe, it} = global;

import expect from 'expect';
import postReducer from '../postReducer';
import examplePosts from '../../fixtures/Posts';

const examplePost = Array.from(examplePosts.values())[0];

import DEFAULT_FORM_DATA from '../../constants/DefaultFormData';

describe('postReducer', () => {
  it('should add comment', () => {
    let initialListPosts = new Map();
    initialListPosts.set('123', {
      ...examplePost,
      id: '123',
      comments: ['523'],
    });

    let formData = DEFAULT_FORM_DATA.ActivityLog;
    let newPosts = postReducer({formData: formData, postList: initialListPosts, lastPostID: '', page: 1}, {
      type: 'COMMENT_ADDED',
      postID: '123',
      comment: {
        id: '312',
        text: 'comment 1',
        user: '123',
      },
    });
    let post = newPosts.postList.get('123');
    expect(post).toExist();
    if (post) {
      expect(post.comments.length).toBe(2);
      expect(post.comments).toEqual(['523', '312']);
    }
  });

  it('should add like', () => {
    let initialListPosts = new Map();
    initialListPosts.set('123', {
      ...examplePost,
      id: '123',
      likers: ['user1'],
    });

    let formData = DEFAULT_FORM_DATA.ActivityLog;
    let newPosts = postReducer({formData: formData, postList: initialListPosts, lastPostID: '', page: 1}, {
      type: 'LIKE_ADDED',
      token: 'token',
      postID: '123',
      userID: 'user2',
    });
    let post = newPosts.postList.get('123');
    expect(post).toExist();
    if (post) {
      expect(post.likers.length).toBe(2);
      expect(post.likers).toEqual(['user1', 'user2']);
    }
  });

  it('should remove like', () => {
    let initialListPosts = new Map();
    initialListPosts.set('123', {
      ...examplePost,
      id: '123',
      likers: ['user1', 'user2'],
    });
    let formData = DEFAULT_FORM_DATA.ActivityLog;

    let newPosts = postReducer({formData, postList: initialListPosts, lastPostID: '', page: 1}, {
      type: 'LIKE_REMOVED',
      postID: '123',
      userID: 'user2',
    });
    let post = newPosts.postList.get('123');
    expect(post).toExist();
    if (post) {
      expect(post.likers.length).toBe(1);
      expect(post.likers).toEqual(['user1']);
    }
  });

  it('should change the structure of formData', () => {
    let initialListPosts = new Map();
    initialListPosts.set('123', {
      ...examplePost,
      id: '123',
      comments: ['523'],
    });

    let formData = DEFAULT_FORM_DATA.ActivityLog;
    let posts = postReducer({postList: initialListPosts, formData, lastPostID: '', page: 1}, {
      type: 'FORM_OPENED',
      category: 'ActivityLog',
    });

    expect(posts.formData).toEqual(DEFAULT_FORM_DATA['ActivityLog']);

    posts = postReducer({postList: initialListPosts, formData, lastPostID: '', page: 1}, {
      type: 'FORM_OPENED',
      category: 'Meal',
    });

    expect(posts.formData).toEqual(DEFAULT_FORM_DATA['Meal']);
  });

  it('should change the content of the formData', () => {
    let initialListPosts = new Map();
    initialListPosts.set('123', {
      ...examplePost,
      comments: ['523'],
    });
    let formData = DEFAULT_FORM_DATA.ActivityLog;
    let posts = postReducer({postList: initialListPosts, formData, lastPostID: '', page: 1}, {
      type: 'FORM_OPENED',
      category: 'ActivityLog',
    });

    let newPosts = postReducer({postList: initialListPosts, formData, lastPostID: '', page: 1}, {
      type: 'FORMDATA_CHANGED',
      newFormData: {
        category: 'ActivityLog',
        ...posts.formData,
        content: 'data test',
        comment: 'data comment',
      },
    });

    if (newPosts.formData.content) {
      expect(newPosts.formData.content).toBe('data test');
    }
    if (newPosts.formData.comment) {
      expect(newPosts.formData.comment).toBe('data comment');
    }
  });

  it('should replace the optimistic post with the real post', () => {
    let initialListPosts = new Map();
    initialListPosts.set('_123', {
      ...examplePost,
      id: '_123',
      comments: ['523'],
    });
    let formData = DEFAULT_FORM_DATA.ActivityLog;
    let initialPosts = {
      formData,
      postList: initialListPosts,
      lastPostID: '',
      page: 1,
    };
    let action = {
      type: 'SEND_POST_SUCCESS',
      optimisticID: '_123',
      post: {
        ...examplePost,
        id: 'newOne',
        comments: ['523'],
      },
    };

    let newPosts = postReducer(initialPosts, action);
    expect(newPosts.postList.has('_123')).toBe(false);
    expect(newPosts.postList.has('newOne')).toBe(true);
  });
});
