let {describe, it} = global;
import expect from 'expect';

import arrayToMap from '../../helpers/arrayToMap';
import notificationReducer from '../notificationReducer';

describe('Notification Reducer', () => {
  let init = notificationReducer(null, null);
  it('should change page number if not in refreshing state', () => {
    let newNotifList = [
      {
        id: '123',
      },
    ];
    let action = {
      type: 'NOTIF_LIST_RECEIVED',
      notifList: newNotifList,
      page: 2,
    };
    let notificationState = notificationReducer(init, action);
    expect(notificationState).toEqual({
      ...init,
      notificationList: arrayToMap(new Map(), newNotifList),
      page: 2,
    });
  });
  it('should not change page number if its in refreshing state', () => {
    let newNotifList = [
      {
        id: '123',
      },
    ];
    let action = {
      type: 'NOTIF_LIST_RECEIVED',
      notifList: newNotifList,
      page: 1,
      isRefreshingList: true,
    };
    let notificationState = notificationReducer(init, action);
    expect(notificationState).toEqual({
      ...init,
      notificationList: arrayToMap(new Map(), newNotifList),
      page: init.page,
    });
  });
});
