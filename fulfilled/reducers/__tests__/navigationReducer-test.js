let {describe, it} = global;

import expect from 'expect';
import {View} from 'react-native';
import navigationReducer, {initialData} from '../navigationReducer';

import {PROFILE_PAGE} from '../../constants/NavBarPageName';

describe('Navigation Reducer', () => {
  it('should give initial reducer', () => {
    let init = navigationReducer(null, null);
    expect(init).toEqual(initialData);
  });

  it('should change the navbar', () => {
    let action = {
      type: 'NAVBAR_CHANGED',
      route: {
        key: PROFILE_PAGE,
      },
      index: 4,
    };

    let newState = navigationReducer(initialData, action);
    expect(newState.navbar.routes.length).toBe(1);
    expect(newState.navbar.index).toBe(0);
    expect(newState.navbar.selectedIndex).toBe(4);
  });

  it('should push the routes', () => {
    let action = {
      type: 'ROUTE_PUSHED',
      route: {
        key: PROFILE_PAGE,
        Component: View,
        props: {
          id: '1',
        },
      },
    };

    let newState = navigationReducer(initialData, action);
    expect(newState.detail.index).toBe(1);
    expect(newState.detail.routes.length).toBe(2);
    expect(newState.detail.routes[1]).toEqual({
      key: PROFILE_PAGE,
      Component: View,
      props: {
        id: '1',
      },
    });
  });

  it('should replace the route', () => {
    let action = {
      type: 'ROUTE_REPLACED',
      route: {
        key: PROFILE_PAGE,
        Component: View,
        props: {
          id: '1',
        },
      },
    };

    let newState = navigationReducer(initialData, action);
    expect(newState.detail.index).toBe(0);
    expect(newState.detail.showDetailView).toBe(true);
    expect(newState.detail.routes.length).toBe(1);
    expect(newState.detail.routes[0]).toEqual({
      key: PROFILE_PAGE,
      Component: View,
      props: {
        id: '1',
      },
    });
  });

  it('should pop the routes', () => {
    let action = {
      type: 'ROUTE_PUSHED',
      route: {
        key: PROFILE_PAGE,
        Component: View,
        props: {
          id: '1',
        },
      },
    };
    let popAction = {
      type: 'ROUTE_POPPED',
    };

    let newState = navigationReducer(initialData, action);
    newState = navigationReducer(newState, popAction);
    expect(newState.detail.index).toBe(0);
    expect(newState.detail.showDetailView).toBe(false);
  });
});
