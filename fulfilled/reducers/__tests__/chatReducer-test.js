let {describe, it} = global;

import expect from 'expect';
import chatReducer from '../chatReducer';

describe('chat reducer', () => {
  it('should add new chat', () => {
    let initialChats = chatReducer(null);
    initialChats = chatReducer(initialChats, {
      type: 'INPUT_CHAT_CHANGED',
      input: 'new chat',
    });
    let newChat = chatReducer(initialChats, {
      type: 'CHAT_ADDED',
      senderNickname: 'user1',
      recipientNickname: 'coach',
    });

    expect(newChat.chatList.size).toBe(1);
    let arrayChats = Array.from(newChat.chatList.values());
    expect(arrayChats[0].text).toBe('new chat');
    expect(arrayChats[0].posterNickname).toBe('user1');
    expect(arrayChats[0].recipientNickname).toBe('coach');
  });
});
