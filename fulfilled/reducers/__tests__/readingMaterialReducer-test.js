let {describe, it} = global;

import expect from 'expect';
import readingMaterialReducer from '../readingMaterialReducer';

describe('readingMaterialReducer', () => {
  it('should add reading material when action READING_MATERIAL_LIST_RECEIVED dispatched', () => {
    let readingMaterialList = [
      {
        id: '123',
        html: 'first html content',
        markdown: 'first markdown content',
      },
      {
        id: '967',
        html: 'second html content',
        markdown: 'second markdown content',
      },
    ];
    let action = {
      type: 'READING_MATERIAL_LIST_RECEIVED',
      readingMaterialList,
    };

    let result = readingMaterialReducer(new Map(), action);
    expect(result.size).toBe(2);
    expect(result.get('123')).toEqual({
      id: '123',
      html: 'first html content',
      markdown: 'first markdown content',
    });
    expect(result.get('967')).toEqual({
      id: '967',
      html: 'second html content',
      markdown: 'second markdown content',
    });
  });
});
