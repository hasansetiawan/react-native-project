// @flow
import type {Action} from '../types/Action';
import {DEFAULT_LANGUAGE} from '../constants/language';

export default function languageReducer(selectedLanguage: string, action: Action) {
  if (selectedLanguage == null) {
    selectedLanguage = DEFAULT_LANGUAGE;
  }
  switch (action.type) {
    case 'LANGUAGE_SELECTED': {
      return action.language;
    }
    default: {
      return selectedLanguage;
    }
  }
}
