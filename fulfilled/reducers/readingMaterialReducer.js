// @flow

import arrayToMap from '../helpers/arrayToMap';

import type {Action} from '../types/Action';
import type {ReadingMaterial} from '../types/HabitActivity';

let initialState = new Map();

export default function readingMaterialReducer(readingMaterial: Map<string, ReadingMaterial>, action: Action) {
  if (readingMaterial == null) {
    return initialState;
  }
  switch (action.type) {
    case 'READING_MATERIAL_LIST_RECEIVED': {
      let {readingMaterialList} = action;
      let newReadingMaterial = arrayToMap(readingMaterial, readingMaterialList);
      return newReadingMaterial;
    }
    case 'RESET': {
      return initialState;
    }
    default: return readingMaterial;
  }
}
