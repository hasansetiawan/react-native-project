// @flow

import type {Action} from '../types/Action';
import type {PushNotification} from '../types/PushNotification';

export default function pushNotificationReducer(pushNotification: PushNotification, action: Action) {
  if (pushNotification == null) {
    return {
      deviceToken: '',
      parseInstallationID: '',
    };
  }
  switch (action.type) {
    case 'DEVICE_TOKEN_RECEIVED': {
      let {deviceToken} = action;
      return {
        ...pushNotification,
        deviceToken,
      };
    }
    case 'PARSE_INSTALLATION_ID_RECEIVED': {
      let {parseInstallationID} = action;
      return {
        ...pushNotification,
        parseInstallationID,
      };
    }
    default: return pushNotification;
  }
}
