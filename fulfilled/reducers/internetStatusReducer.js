// @flow
import type {Action} from '../types/Action';

export default function internetStatusReducer(isInternetConnected: boolean, action: Action) {
  if (isInternetConnected == null) {
    isInternetConnected = true;
  }
  switch (action.type) {
    case 'INTERNET_STATUS_CHANGED': {
      return action.isConnected;
    }
    default: {
      return isInternetConnected;
    }
  }
}
