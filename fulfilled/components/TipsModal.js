// @flow
import React from 'react';

import {
  View,
  Modal,
  TouchableOpacity,
  Text,
} from 'react-native';
import {
  Icon,
} from '../core-ui';

import ContentView from '../views/ContentView';
import styles from './TipsModal-style';

import type {Content} from '../types/Content';

type Props = {
  onRequestClose: () => void;
  visible: boolean;
  content: ?Content;
  title?: string;
};

function TipsModal(props: Props) {
  let {visible, onRequestClose, content, title} = props;
  return (
    <Modal
      animationType="slide"
      visible={visible}
      onRequestClose={onRequestClose}
      transparent={true}
      supportedOrientations={['portrait']}
    >
      <View style={styles.root}>
        <View style={styles.modalContainer}>
          <View style={styles.modalHeader}>
            <View style={styles.titleContainer}>
              {
                (title) ?
                  <Text style={styles.titleText}>{title}</Text> : null
              }
            </View>
            <TouchableOpacity style={styles.closeButton} onPress={onRequestClose}>
              <Icon name="close" style={styles.closeIcon} />
            </TouchableOpacity>
          </View>
          {(content) ? <ContentView content={content} /> : null}
        </View>
      </View>
    </Modal>
  );
}

export default TipsModal;
