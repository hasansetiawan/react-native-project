import {StyleSheet} from 'react-native';
import {
  FONT_BOLD,
} from '../constants/Font';
import {THEME_COLOR} from '../constants/color';

export default StyleSheet.create({
  root: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
  },
  modalContainer: {
    flex: 1,
    marginHorizontal: 15,
    marginTop: 60,
    marginBottom: 55,
    paddingVertical: 15,
    paddingHorizontal: 5,
    borderRadius: 7,
    backgroundColor: 'white',
  },
  closeButton: {
    alignSelf: 'flex-end',
    flex: 1,
    paddingBottom: 5,
  },
  closeIcon: {
    fontSize: 28,
    color: '#666',
  },
  modalHeader: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  titleContainer: {
    flex: 7,
    alignSelf: 'center',
    marginRight: 5,
  },
  titleText: {
    fontFamily: FONT_BOLD,
    color: THEME_COLOR,
    fontSize: 16,
    textAlign: 'center',
  },
});
