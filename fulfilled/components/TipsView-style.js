import {StyleSheet} from 'react-native';
import {
  FONT_BOLD,
  FONT_REGULAR,
  FONT_LIGHT,
} from '../constants/Font';
import {THEME_COLOR} from '../constants/color';
import {getScreenSize} from '../helpers/getSize';

const {height} = getScreenSize();

export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 15,
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  titleContainerText: {
    textAlign: 'center',
  },
  titleTodayTips: {
    fontFamily: FONT_BOLD,
    color: THEME_COLOR,
    fontSize: 16,
    textAlign: 'center',
    marginRight: 5,
  },
  titleText: {
    fontFamily: FONT_REGULAR,
    color: '#666',
    fontSize: 14,
    textAlign: 'center',
    flexWrap: 'wrap',
  },
  bigContentContainer: {
    alignItems: 'center',
    marginBottom: 20,
  },
  smallContentContainer: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 5,
  },
  imageContainer: {
    height: height * 0.2,
    width: height * 0.2,
  },
  textContainer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 0,
  },
  contentPreviewText: {
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
    marginBottom: 5,
  },
  readMore: {
    flexDirection: 'row',
  },
  readMoreText: {
    fontFamily: FONT_REGULAR,
    color: '#666',
  },
  readMoreIcon: {
    fontSize: 18,
    color: '#666',
  },
  noTipsText: {
    fontFamily: FONT_REGULAR,
    fontSize: 14,
    color: '#666',
    marginBottom: 5,
  },
});
