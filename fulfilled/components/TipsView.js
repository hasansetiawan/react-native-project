// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import ProgressCircleSnail from 'react-native-progress/CircleSnail';

import {
  Icon,
  LocalizedText,
  ResponsiveImage,
} from '../core-ui';
import styles from './TipsView-style';
import TipsModal from './TipsModal';
import getContentFromHTML from '../selectors/getContentFromHTML';


type Props = {
  readingMaterial: {
    title: string;
    html: string;
  };
};

export default class TipsView extends Component {
  state: {isModalOpen: boolean};
  props: Props;
  constructor() {
    super(...arguments);
    autobind(this);

    this.state = {
      isModalOpen: false,
    };
  }

  render() {
    let {readingMaterial: {title, html}} = this.props;
    let {overview, content} = getContentFromHTML(html);
    if (content == null) {
      return (
        <View style={styles.container}>
          <View style={styles.contentContainer}>
            <View style={[styles.textContainer, {paddingTop: 10}]}>
              <LocalizedText from="noTips" style={styles.noTipsText} />
            </View>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.container}>
        <View style={styles.titleContainer}>
          <Text style={styles.titleContainerText}>
            <LocalizedText from="todayTips" style={styles.titleText} />
            <Text>{' : '}</Text>
            <Text style={styles.titleTodayTips} >{title}</Text>
          </Text>
        </View>
        <TouchableOpacity onPress={this._onModalRequestOpen}>
          <View style={styles.bigContentContainer}>
            <View style={styles.smallContentContainer}>
              {
                (overview.imageSrc != null) ?
                  <View style={styles.imageContainer}>
                    <ResponsiveImage
                      source={{uri: overview.imageSrc}}
                      renderIndicator={() => <ProgressCircleSnail animated={true} color="rgba(136, 136, 136, 1)" size={50} />}
                    />
                  </View> : null
              }
            </View>
            <View style={styles.readMore}>
              <LocalizedText upperCase={true} from="readMore" style={styles.readMoreText} />
              <Icon name="play-arrow" style={styles.readMoreIcon} />
            </View>
          </View>
        </TouchableOpacity>
        <TipsModal
          content={content}
          visible={this.state.isModalOpen}
          onRequestClose={this._onModalRequestClose}
          title={title}
        />
      </View>
    );
  }

  _onModalRequestClose() {
    this.setState({isModalOpen: false});
  }
  _onModalRequestOpen() {
    this.setState({isModalOpen: true});
  }
}
