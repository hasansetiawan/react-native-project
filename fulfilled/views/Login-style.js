import {StyleSheet} from 'react-native';
import {
  FONT_APP_TITLE,
  FONT_LIGHT,
} from '../constants/Font';
import {getColor} from '../constants/color';
import {getScreenSize} from '../helpers/getSize';

export default StyleSheet.create({
  mainContainer: {
    padding: 20,
    justifyContent: 'space-between',
    height: getScreenSize().height - 40,
  },
  logoContainer: {
    alignItems: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  logoText: {
    fontFamily: FONT_APP_TITLE,
    fontSize: 36,
    color: getColor('orange'),
  },
  inputContainer: {
  },
  loginContainer: {
  },
  forgetPassword: {
    alignSelf: 'center',
    marginTop: 15,
  },
  registerContainer: {
    alignItems: 'center',
    paddingBottom: getScreenSize().height / 30,
  },
  registerText: {
    fontFamily: FONT_LIGHT,
    fontSize: 14,
    color: '#666',
  },
});
