// @flow

import React from 'react';
import {
  View,
  StyleSheet,
  WebView,
  ActivityIndicator,
} from 'react-native';

import {
  THEME_COLOR,
} from '../constants/color';


type Props = {
  uri: string;
};

var styles = StyleSheet.create({
  video: {
    height: 200,
  },
});

export default function VideoPlayer(props: Props) {
  let {uri} = props;
  return (
    <View style={styles.video}>
      <WebView
        javaScriptEnabled={true}
        source={{uri}}
        startInLoadingState={true}
        renderLoading={() => {
          return (
            <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
              <ActivityIndicator
                animating={true}
                size="large"
                color={THEME_COLOR}
              />
            </View>
          );
        }}
      />
    </View>
  );
}
