// @flow
import {StyleSheet} from 'react-native';
import {getScreenSize} from '../helpers/getSize';
import {
  FONT_BOLD,
  FONT_LIGHT,
} from '../constants/Font';
import {
  CONTAINER_COLOR,
} from '../constants/color';

let {height} = getScreenSize();
export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#EAEAEA',
  },
  profilePictureContainer: {
    backgroundColor: '#EAEAEA',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 10,
    marginBottom: 10,
  },
  profilePicture: {
    marginTop: 10,
    width: height * 0.2,
    height: height * 0.2,
    borderRadius: (height * 0.2) / 2,
  },
  coachName: {
    marginBottom: 5,
    color: '#555',
    fontFamily: FONT_BOLD,
  },
  coachTitle: {
    color: '#555',
    fontFamily: FONT_LIGHT,
  },
  contentShadow: {
    backgroundColor: CONTAINER_COLOR,
    height: height * 0.55,
    marginHorizontal: 10,
    borderRadius: 8,
    paddingTop: 10,
    shadowColor: '#444',
    shadowOffset: {
      width: 1,
      height: 2,
    },
    shadowOpacity: 0.5,
    shadowRadius: 2,
  },
});
