// @flow

import React from 'react';
import {
  View,
  Image,
} from 'react-native';
import {connect} from 'react-redux';
import {CardView, Button, LoadingIndicator} from '../core-ui';

import styles from './SelectedPhotoView-style';

import type {ImageData} from '../types/ImageData';
import type {Dispatch} from '../types/Dispatch';
import type {User} from '../types/User';
import type {RootState} from '../types/RootState';

type Props = {
  isLoading: boolean;
  user: User;
  imageData: ImageData;
  onConfirm: (imageData: ImageData, user: User) => void;
};

export function SelectedPhotoView(props: Props) {
  let {imageData, onConfirm, user, isLoading} = props;
  return (
    <CardView style={styles.root}>
      {(isLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
      <View style={styles.imageContainer}>
        <Image
          style={styles.image}
          source={imageData}
        />
      </View>
      <View style={styles.buttonContainer}>
        <Button
          text="confirmProfilePicture"
          type="primary"
          color="theme"
          onPress={() => onConfirm(imageData, user)}
        />
      </View>
    </CardView>
  );
}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    isLoading: state.loadingPage.changeProfilePicturePage,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onConfirm: (imageData: ImageData, user: User) => {
      let {id, token, nickname, language} = user;
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'changeProfilePicturePage',
      });
      dispatch({
        type: 'CHANGE_PROFILE_PICTURE_REQUESTED',
        id,
        imageData,
        token,
        name: nickname,
        language,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SelectedPhotoView);
