// @flow

import React from 'react';
import {
  View,
  Text,
  Platform,
} from 'react-native';

import {
  LocalizedText,
  CardView,
  LocalizedDate,
  Icon,
} from '../core-ui';

import styles from './SubscriptionDetailView-style';

import type {User} from '../types/User';

type Props = {
  user: User;
};

export default function SubscriptionDetailView(props: Props) {
  let {user} = props;
  let cancelationInfo = '';
  if (Platform.OS === 'android') {
    cancelationInfo = 'Google Play Store -> Menu -> My Apps -> Subscriptions';
  } else if (Platform.OS === 'ios') {
    cancelationInfo = 'Settings -> iTunes & App Store -> Apple ID -> Subscriptions';
  }
  return (
    <View>
      <CardView>
        <View style={styles.headerContainer}>
          <Icon name="star" style={styles.iconStyle} />
          <View style={styles.headerTextContainer}>
            <LocalizedText style={styles.defaultText} from="userRegisteredAsPremiumMember" />
          </View>
        </View>
        <View style={styles.expiryContent}>
          <Text style={styles.defaultText}>
            <LocalizedText from="subscriptionExpiry" />
            <Text>: {' '}</Text>
            <LocalizedDate style={styles.expityDate} date={user.premiumMembershipExpiry} format="DATE_TIME_LONG" />
            <Text>{' '}</Text>
            <LocalizedText from="and" />
            <Text>{' '}</Text>
            <LocalizedText from="autoRenewal" />
          </Text>
        </View>
        <View style={styles.cancelationInfoContainer}>
          <LocalizedText style={styles.defaultText} from="subscriptionCancelationInfo" />
          <Text style={styles.defaultText}>{cancelationInfo}</Text>
        </View>
      </CardView>
    </View>
  );
}
