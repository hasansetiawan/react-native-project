// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import {
  StyleSheet,
  View,
  NavigationExperimental,
} from 'react-native';
import Modal from 'react-native-modalbox';

import {KeyboardAvoidingView} from '../core-ui';
import {CONTAINER_COLOR} from '../constants/color';
import Header from '../core-ui/Header';

import {getScreenSize} from '../helpers/getSize';

import type {RootState} from '../types/RootState';

const {height, width} = getScreenSize();

type NavigationProps = {
  Component?: Object;
  props?: any;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    backgroundColor: CONTAINER_COLOR,
  },
  content: {
    flex: 1,
  },
  keyboardAvoidViewContainer: {
    position: 'absolute',
    bottom: 0,
    left: 0,
    top: 1,
    flex: 1,
    width: width,
    height: height,
  },
});

const {
  CardStack: NavigationCardStack,
} = NavigationExperimental;

type Props = {
  onClose: () => void;
  withHeader?: boolean;
  headerTitle: string;
  detailNavState: Object;
};

class DetailView extends Component {
  props: Props;
  constructor() {
    super(...arguments);
    autobind(this);
  }
  render() {
    let {detailNavState, headerTitle, withHeader} = this.props;
    let isOpen = detailNavState.showDetailView;
    if (!isOpen) {
      return null;
    }
    if (withHeader !== false) {
      withHeader = true;
    }
    let header = (withHeader) ?
      <Header pageName={headerTitle} leftButtonIcon={(detailNavState.index > 0) ? 'keyboard-arrow-left' : 'close'} leftButtonAction={() => this._onBackButtonPress()} /> :
        <View />;
    return (
      <KeyboardAvoidingView behavior="padding" style={styles.keyboardAvoidViewContainer}>
        <Modal
          isOpen={isOpen}
          animationDuration={350}
          swipeToClose={false}
          backdropPressToClose={false}
        >
          {header}
          <View style={styles.container}>
            <NavigationCardStack
              direction="horizontal"
              navigationState={detailNavState}
              renderScene={this._renderScene}
              onNavigateBack={this._onBackButtonPress}
              enableGestures={true}
              renderHeader={null}
              style={{}}
            />
          </View>
        </Modal>
      </KeyboardAvoidingView>
    );
  }

  _renderScene({scene}) {
    let {key, ...otherProps} = scene.route;
    return (renderComponent(key, otherProps));
  }

  _onBackButtonPress() {
    this.props.onClose();
    return true;
  }
}

function renderComponent(key: string, navProps: NavigationProps) {
  let {Component, props} = navProps;
  if (Component != null) {
    return <Component key={key} {...props} />;
  } else {
    return null;
  }
}

function mapStateToProps(state: RootState) {
  return {
    headerTitle: state.navigation.detail.currentDetailHeader,
  };
}


export default connect(mapStateToProps, null)(DetailView);
