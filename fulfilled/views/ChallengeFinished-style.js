// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    flex: 1,
    justifyContent: 'center',
  },
  headerStyle: {
    justifyContent: 'flex-start',
  },
  textContainer: {
    paddingBottom: 20,
  },
  buttonIcon: {
    fontSize: 20,
    color: 'white',
  },
});
