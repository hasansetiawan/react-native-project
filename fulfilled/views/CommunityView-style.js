import {StyleSheet} from 'react-native';
import {
  FONT_LIGHT,
} from '../constants/Font';

let menuIcon = {
  fontSize: 18,
  height: 18,
  color: 'white',
};
export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  scrollContainer: {
    flex: 1,
  },
  cardContainer: {
    paddingBottom: 10,
  },
  menuIcon,
  menuIconRotate: {
    ...menuIcon,
    transform: [{rotate: '180deg'}],
  },
  textCenter: {
    textAlign: 'center',
    paddingTop: 10,
  },
  actionButtonMenuText: {
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
  },
});
