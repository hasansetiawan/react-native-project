import {
  StyleSheet,
  Platform,
} from 'react-native';
import {
  FONT_REGULAR,
  FONT_LIGHT,
} from '../constants/Font';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  searchContainer: {
    padding: 20,
    paddingVertical: 10,
    backgroundColor: '#F8F8F8',
    borderBottomWidth: 0.5,
    borderBottomColor: '#CCC',
  },
  searchBar: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    paddingLeft: 15,
    borderRadius: 4,
    backgroundColor: '#EEE',
    borderWidth: 0.5,
    borderColor: '#CCC',
  },
  searchTextInput: {
    flex: 1,
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
    padding: 0,
  },
  iconSearch: {
    fontSize: 18,
    color: '#555',
  },
  scrollContainer: {
    marginBottom: (Platform.OS === 'ios') ? 0 : 25,
    //android has issues, native navigation menu covering the ScrollView
  },
  overView: {
    flexDirection: 'row',
    backgroundColor: '#F8F8F8',
    paddingHorizontal: 23,
    paddingVertical: 18,
  },
  overViewWriting: {
    flex: 4,
    flexDirection: 'column',
    marginRight: 20,
  },
  overViewTitle: {

  },
  overViewTitleText: {
    fontFamily: FONT_REGULAR,
    fontSize: 18,
    color: '#777',
  },
  overViewDescription: {
    marginVertical: 10,
  },
  overViewDescriptionText: {
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
  },
  overViewImageContainer: {
    flex: 2,
    justifyContent: 'center',
  },
  overViewImage: {
    width: 100,
    height: 100,
  },
  premiumAds: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  premiumAdsText: {
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
    textAlign: 'center',
    marginBottom: 10,
  },
  premiumAdsButton: {
    backgroundColor: '#4A90E2',
  },
  premiumAdsButtonText: {
    fontFamily: FONT_REGULAR,
    fontSize: 14,
    color: 'white',
    textAlign: 'center',
  },
  noProgramFound: {
    flex: 1,
    marginTop: 50,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
