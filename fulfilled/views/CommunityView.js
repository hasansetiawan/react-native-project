import React, {Component} from 'react';
import {
  View,
  RecyclerViewBackedScrollView,
  ListView,
  ActivityIndicator,
  RefreshControl,
  Platform,
} from 'react-native';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import ActionButton from 'react-native-action-button';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  CommunityCard,
  Icon,
  LocalizedText,
  LoadingIndicator,
} from '../core-ui';

import {getLatestOrderedPosts} from '../selectors/getLatestOrderedPosts';
import PostHistoryView from './PostHistoryView';
import WebView from './WebView';
import NewPostView from './NewPostForm/NewPostView';
import {
  THEME_COLOR,
  DRINK_BUTTON_COLOR,
  FOOD_BUTTON_COLOR,
  FITNESS_BUTTON_COLOR,
  SCALE_BUTTON_COLOR,
  THINKING_BUTTON_COLOR,
} from '../constants/color';
import styles from './CommunityView-style';

import type {Post} from '../types/Post';
import type {Comment} from '../types/Comment';
import type {RootState} from '../types/RootState';
import type {Coach} from '../types/Coach';
import type {User, CommunityMember, ProfilePicture} from '../types/User';
import type {Dispatch} from '../types/Dispatch';

const MenuItem = ActionButton.Item;

type Props = {
  coach: Coach;
  posts: Array<Post>;
  communityMembers: Map<string, CommunityMember>;
  comments: Map<string, Comment>;
  user: User;
  isLoading: boolean;
  lastPostID: string;
  page: number;
  isRefreshingList: boolean;
  profilePictureList: Map<string, ProfilePicture>;
  isKeyboardShown: boolean;
  onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => void;
  onSubmitComment: (postID: string, text: string, user: User) => void;
  onCreatePostPress: (category: string, coach: Coach) => void;
  onUserClick: (user: User, targettedUser: CommunityMember) => void;
  onLoadMorePost: (lastPostID: string, token: string, page: number) => void;
  onRefresh: (token: string) => void;
  onImageClick: (uri: string) => void;
  onLinkPress: (uri: string) => void;
};

export class CommunityView extends Component {
  props: Props;
  state: {dataSource: Object};
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      dataSource: this.generateDatasource(this.props.posts),
    };
  }
  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (newProps.posts !== oldProps.posts) {
      this.setState({dataSource: this.generateDatasource(newProps.posts)});
    }
  }

  generateDatasource(posts: Array<Post>) {
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return ds.cloneWithRows(posts);
  }

  render() {
    let {coach, onCreatePostPress, isLoading, isKeyboardShown} = this.props;
    if (isLoading === true) {
      return (
        <View style={styles.mainContainer}>
          <LoadingIndicator />
        </View>
      );
    } else {
      return (
        <View style={styles.mainContainer}>
          <ListView
            ref="listview"
            style={styles.scrollContainer}
            renderScrollComponent={(props: Object) => {
              if (Platform.OS === 'ios') {
                return (
                  <KeyboardAwareScrollView {...props} />
                );
              } else {
                return (
                  <RecyclerViewBackedScrollView {...props} />
                );
              }
            }}
            dataSource={this.state.dataSource}
            refreshControl={
              <RefreshControl
                refreshing={this.props.isRefreshingList}
                onRefresh={this._onRefresh}
              />
            }
            initialListSize={3}
            scrollRenderAheadDistance={50}
            renderRow={(post) => {
              return this._renderPost(post, post.id);
            }}
            renderFooter={() => <ActivityIndicator size="large" animating={true} />}
            onEndReached={this._fetchMorePosts}
          />
          {
            (isKeyboardShown === false) ?
              (
                <ActionButton
                  buttonColor={THEME_COLOR}
                  size={50}
                  spacing={1}
                  position="right"
                  offsetX={20}
                  offsetY={5}
                  bgColor="rgba(245, 245, 245, 0.75)"
                >
                  <MenuItem
                    size={40}
                    buttonColor={FITNESS_BUTTON_COLOR}
                    title={<LocalizedText from="postMenu/ActivityLog" style={styles.actionButtonMenuText} />}
                    onPress={() => onCreatePostPress('ActivityLog', coach)}
                  >
                    <Icon name="directions-bike" style={styles.menuIcon} />
                  </MenuItem>

                  <MenuItem
                    size={40}
                    buttonColor={FOOD_BUTTON_COLOR}
                    title={<LocalizedText from="postMenu/Meal" style={styles.actionButtonMenuText} />}
                    onPress={() => onCreatePostPress('Meal', coach)}
                  >
                    <Icon name="local-dining" style={styles.menuIcon} />
                  </MenuItem>

                  <MenuItem
                    size={40}
                    buttonColor={DRINK_BUTTON_COLOR}
                    title={<LocalizedText from="postMenu/Drink" style={styles.actionButtonMenuText} />}
                    onPress={() => onCreatePostPress('Drink', coach)}
                  >
                    <Icon name="local-drink" style={styles.menuIcon} />
                  </MenuItem>

                  <MenuItem
                    size={40}
                    buttonColor={SCALE_BUTTON_COLOR}
                    title={<LocalizedText from="postMenu/Measurement" style={styles.actionButtonMenuText} />}
                    onPress={() => onCreatePostPress('Measurement', coach)}
                  >
                    <Icon name="straighten" style={styles.menuIcon} />
                  </MenuItem>

                  <MenuItem
                    size={40}
                    buttonColor={THINKING_BUTTON_COLOR}
                    title={<LocalizedText from="postMenu/Note" style={styles.actionButtonMenuText} />}
                    onPress={() => onCreatePostPress('Note', coach)}
                  >
                    <Icon name="wb-incandescent" style={styles.menuIconRotate} />
                  </MenuItem>
                </ActionButton>
              ) : null
          }
        </View>
      );
    }
  }

  _renderPost(post: Post, id: string) {
    let {
      user,
      communityMembers,
      profilePictureList,
      onLikePress,
      onSubmitComment,
      onUserClick,
      onImageClick,
      onLinkPress,
    } = this.props;
    let {comments} = this.props;
    let postComments = [];
    for (let id of post.comments) {
      postComments.push(comments.get(id));
    }

    let likers = [];
    for (let id of post.likers) {
      likers.push(communityMembers.get(id));
    }
    return (
      <View key={id} style={styles.cardContainer}>
        <CommunityCard
          user={user}
          post={post}
          profilePictureList={profilePictureList}
          communityMembers={communityMembers}
          comments={postComments}
          likers={likers}
          onLikePress={onLikePress}
          onSubmitComment={onSubmitComment}
          onUserClick={onUserClick}
          onImageClick={onImageClick}
          onLinkPress={onLinkPress}
        />
      </View>
    );
  }
  _fetchMorePosts() {
    let {onLoadMorePost, lastPostID, page, user} = this.props;
    let newPage = page + 1;
    onLoadMorePost(lastPostID, user.token, newPage);
  }
  _onRefresh() {
    let {user, onRefresh} = this.props;
    onRefresh(user.token);
  }
}


function mapStateToProps(state: RootState) {
  return {
    posts: getLatestOrderedPosts(state.post.postList),
    communityMembers: new Map(state.communityMembers),
    comments: new Map(state.comments),
    user: state.user,
    isLoading: state.loadingPage.communityFeedPage,
    lastPostID: state.post.lastPostID,
    page: state.post.page,
    isRefreshingList: state.loadingPage.communityFeedRefresh,
    profilePictureList: state.profilePictureList,
    isKeyboardShown: state.keyboard.isKeyboardShown,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onImageClick: (uri: string) => {
      dispatch({
        type: 'IMAGE_URI_RECEIVED',
        uri,
      });
    },
    onCreatePostPress: (category: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: `postMenu/${category}`,
          Component: NewPostView,
          title: `postMenu/${category}`,
        },
      });
      dispatch({
        type: 'FORM_OPENED',
        category,
      });
    },
    onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => {
      if (isLiked === false) {
        dispatch({
          type: 'LIKE_ADDED',
          token: userToken,
          postID,
          userID,
        });
      }
    },
    onSubmitComment: (postID: string, text: string, user: User) => {
      let id = '_' + Math.random().toString();
      dispatch({
        type: 'COMMENT_ADDED',
        postID,
        comment: {
          id,
          text,
          user: user.id,
        },
      });
      dispatch({
        type: 'SEND_COMMENT_REQUESTED',
        postID,
        token: user.token,
        text,
      });
    },
    onUserClick: (user: User, targettedUser: CommunityMember) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'postHistoryPage',
      });
      dispatch({
        type: 'USER_POST_HISTORY_REQUESTED',
        token: user.token,
        page: 1,
        userID: targettedUser.id,
      });
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'profilePage/myPosts',
          Component: PostHistoryView,
          title: `${targettedUser.nickname}'s Posts`,
          props: {user, targettedUserID: targettedUser.id},
        },
      });
    },
    onLoadMorePost: (lastPostID: string, token: string, page: number) => {
      dispatch({
        type: 'FETCH_POSTS_REQUESTED',
        token,
        page,
        lastPostID,
      });
    },
    onRefresh: (token: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'communityFeedRefresh',
      });
      dispatch({
        type: 'FETCH_POSTS_REQUESTED',
        token,
        page: 1,
        refresh: true,
      });
    },
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CommunityView);
