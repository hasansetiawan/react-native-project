// @flow

import React from 'react';
import {
  WebView as WebViewNative,
  ActivityIndicator,
  View,
} from 'react-native';

import {
  THEME_COLOR,
} from '../constants/color';

type Props = {
  uri: string;
};

export default function WebView(props: Props) {
  let {uri} = props;
  return (
    <WebViewNative
      source={{uri}}
      style={{flex: 1}}
      startInLoadingState={true}
      renderLoading={() => {
        return (
          <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
            <ActivityIndicator
              animating={true}
              size="large"
              color={THEME_COLOR}
            />
          </View>
        );
      }}
    />
  );
}
