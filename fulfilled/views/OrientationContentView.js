// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import {
  View,
  StatusBar,
} from 'react-native';
import {Button, LocalizedTextInput} from '../core-ui';
import Header from '../core-ui/Header';
import Swiper from 'react-native-swiper';
import ContentView from './ContentView';
import getContentFromHTML from '../selectors/getContentFromHTML';
import {getScreenSize} from '../helpers/getSize';
import {validateQuestionAnswered} from '../helpers/validate';

import textInputStyle from './NewPostForm/NewPostView-style';

import type {ContentReadingMaterial as OrientationContent} from '../types/ContentReadingMaterial';
import type {User} from '../types/User';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Dispatch';

type InputObject = {
  inputKey: string;
  text: string;
};

type SlideState = {
  index: number;
};

type Props = {
  user: User;
  orientationContentList: Map<string, OrientationContent>;
  orderedID: Array<string>;
  onOrientationCompleted: (user: User, input: InputObject) => void;
  onValidateFail: (errorMessage: string) => void;
};

type State = {
  input: InputObject;
};

export class OrientationContentView extends Component {
  props: Props
  state: State
  formSlideIndex: ?number
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      input: {
        inputKey: '',
        text: '',
      },
    };
  }
  render() {
    let {
      orientationContentList,
      orderedID,
      onOrientationCompleted,
      user,
    } = this.props;
    return (
      <View>
        <StatusBar
          backgroundColor="#5A073C"
          barStyle="light-content"
        />
        <Header pageName="introduction" />

        <View style={{flex: 1}}>
          <Swiper
            ref="swiper"
            loop={false}
            height={getScreenSize().height - 60}
            showsPagination={true}
            activeDot={<ActiveDot />}
            onMomentumScrollEnd={this._onSlide}
          >
            {
              orderedID.map((id, index) => {
                let orientationContent = orientationContentList.get(id);
                if (orientationContent != null) {
                  let {content} = getContentFromHTML(orientationContent.renderedBody);
                  let textInput;
                  for (let key of Object.keys(content.entityMap)) {
                    let entity = content.entityMap[key];
                    if (entity.type === 'INPUT') {
                      this.formSlideIndex = index;
                      let inputKey = entity.data['data-question-id'];
                      let {input} = this.state;
                      let inputStyle = {
                        marginHorizontal: 20,
                        backgroundColor: '#EEE',
                        borderColor: '#9E9E9E',
                      };
                      textInput = (
                        <LocalizedTextInput
                          multiline={false}
                          style={[textInputStyle.multiline, inputStyle]}
                          placeholder={entity.data.placeholder}
                          value={(input != null) ? input.text : ''}
                          onChangeText={(text) => (
                            this.setState({
                              input: {
                                inputKey,
                                text,
                              },
                            })
                          )}
                        />
                      );
                      break;
                    }
                  }
                  return (
                    <View key={index} style={{flex: 1, paddingBottom: 50, marginTop: 20}}>
                      {
                        (id === orderedID[orderedID.length - 1]) ?
                          <ContentView
                            content={content}
                            textInput={textInput}
                            closeButton={
                              <View style={{marginHorizontal: 50, marginVertical: 10}}>
                                <Button
                                  type="primary"
                                  color="theme"
                                  text="START"
                                  onPress={() => {
                                    onOrientationCompleted(user, this.state.input);
                                  }}
                                />
                              </View>
                            }
                          />
                         :
                           <ContentView
                             content={content}
                             textInput={textInput}
                           />
                      }
                    </View>
                  );
                }
                return null;
              })
            }
          </Swiper>
        </View>
      </View>
    );
  }
  _onSlide(e: Object, slideState: SlideState) {
    if (slideState.index - 1 === this.formSlideIndex) {
      let {onValidateFail} = this.props;
      let {input} = this.state;
      let validateResult = validateQuestionAnswered(input.text);
      if (validateResult != null) {
        onValidateFail(validateResult);
        if (this.formSlideIndex != null) {
          this.refs.swiper.scrollBy(this.formSlideIndex - slideState.index, false);
        }
      } else {
      }
    }
  }
}

function ActiveDot() {
  return (
    <View
      style={{
        backgroundColor: '#5A073C',
        width: 8,
        height: 8,
        borderRadius: 4,
        marginLeft: 3,
        marginRight: 3,
        marginTop: 3,
        marginBottom: 3,
      }}
    />
  );
}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    orderedID: state.coach.orientationContentIDs,
    orientationContentList: state.orientationContent,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onOrientationCompleted: (user: User, inputObject: InputObject) => {
      dispatch({
        type: 'SUBMIT_ORIENTATION_ANSWER_REQUESTED',
        token: user.token,
        userID: user.id,
        formData: `{\"answers\":{\"${inputObject.inputKey}\":\"${inputObject.text}\"}}`,
      });
      dispatch({
        type: 'COMPLETE_ORIENTATION_REQUESTED',
        token: user.token,
        userID: user.id,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OrientationContentView);
