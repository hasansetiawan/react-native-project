import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  logoContainer: {
    flex: 1,
  },
  buttonsContainer: {
    flex: 1,
  },
  rowContainer: {
    flexDirection: 'row',
    paddingHorizontal: 15,
  },
  lineContainer: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 25,
  },
  line: {
    height: 1,
    borderColor: '#a6a6a6',
    borderWidth: 0.5,
  },
  orText: {
    alignSelf: 'center',
    color: '#6f6f6f',
  },
});
