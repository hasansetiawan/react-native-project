// @flow
import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Text,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {
  CardView,
  Icon,
  Button,
  LocalizedText,
  LocalizedTextInput,
  LoadingIndicator,
} from '../core-ui';
import styles from './WeeklyCheckinFormView-style';
import textInputStyle from '../core-ui/TextField-style';
import {
  validateWeeklyCheckin,
} from '../helpers/validate';

import type {Privacy} from '../types/Privacy';
import type {User} from '../types/User';
import type {Dispatch} from '../types/Dispatch';
import type {RootState} from '../types/RootState';
import type {WeeklyCheckinStatus} from '../types/WeeklyCheckin';

type UserInputsKey = 'checkinReview' | 'checkinPlan' | 'checkinWeight' | 'checkinWaist';

type UserInputs = {
  checkinReview: string;
  checkinPlan: string;
  checkinWeight: string;
  checkinWaist: string;
};

type Props = {
  user: User;
  isLoading: boolean;
  weeklyCheckinStatus: WeeklyCheckinStatus;
  onSubmit: (privacy: Privacy, user: User, userInputs: UserInputs) => void;
  onValidateFail: (errorMessage: string) => void;
};

type State = {
  selectedVisibility: Privacy;
  userInputs: UserInputs;
};

export class WeeklyCheckinFormView extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedVisibility: 'community',
      userInputs: {
        checkinReview: '',
        checkinPlan: '',
        checkinWeight: '',
        checkinWaist: '',
      },
    };
  }

  render() {
    let {isLoading, weeklyCheckinStatus} = this.props;
    let {selectedVisibility, userInputs} = this.state;
    let ScrollComponent = ScrollView;
    if (Platform.OS === 'ios') {
      ScrollComponent = KeyboardAwareScrollView;
    }
    return (
      <ScrollComponent>
        <View style={styles.container}>
          {(isLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <CardView>
            <View style={styles.buttonContainer}>
              <View style={styles.visibilityLockIconContainer}>
                <Icon name="lock" style={styles.visibilityLockIcon} />
              </View>
              <Button
                type="primary"
                text="community"
                style={[styles.buttonVisibility, {flex: 5}]}
                color="transparent"
                textStyle={[styles.buttonText, styles.grayText]}
                onPress={() => this._setVisibility('community')}
                leftIcon={selectedVisibility === 'community' ? 'radio-button-checked' : 'radio-button-unchecked'}
                iconStyle={[styles.visibilityIcon, styles.grayText]}
              />
              <View style={styles.buttonSpacer} />
              <Button
                type="primary"
                text="guide"
                style={[styles.buttonVisibility, {flex: 5}]}
                color="transparent"
                textStyle={[styles.buttonText, styles.grayText]}
                onPress={() => this._setVisibility('guide')}
                leftIcon={selectedVisibility === 'guide' ? 'radio-button-checked' : 'radio-button-unchecked'}
                iconStyle={[styles.visibilityIcon, styles.grayText]}
              />
            </View>
            <View>
              <View style={styles.postDetail}>
                <View style={[styles.achievementContainer, {marginBottom: 7}]}>
                  <View style={styles.achievementIconContainer}>
                    <Icon name="check-box" style={styles.achievementIcon} />
                  </View>
                  <View style={styles.achievementTextContainer}>
                    <Text style={styles.achievementText}>
                      <Text>{weeklyCheckinStatus.activities}{' '}</Text>
                      <LocalizedText
                        from="taskCompleted"
                      />
                    </Text>
                  </View>
                </View>
                <View style={styles.achievementContainer}>
                  <View style={styles.achievementIconContainer}>
                    <Icon name="description" style={styles.achievementIcon} />
                  </View>
                  <View style={styles.achievementTextContainer}>
                    <Text style={styles.achievementText}>
                      <Text>{weeklyCheckinStatus.lessons}{' '}</Text>
                      <LocalizedText
                        from="lessonsRead"
                      />
                    </Text>
                  </View>
                </View>
              </View>

              <View style={styles.postDetail}>
                <View style={styles.questionTextContainer}>
                  <LocalizedText
                    style={styles.questionText}
                    from="weeklyCheckinReviewQuestion"
                  />
                </View>
                <View style={[textInputStyle.dark, textInputStyle.compact]}>
                  <LocalizedTextInput
                    multiline={true}
                    style={[textInputStyle.default, {fontSize: 13, height: 60}]}
                    placeholder="placeholder/weeklyCheckinReview"
                    value={userInputs.checkinReview}
                    onChangeText={(text) => this._onInputChange('checkinReview', text)}
                  />
                </View>
              </View>
              <View style={styles.postDetail}>
                <View style={styles.questionTextContainer}>
                  <LocalizedText
                    style={styles.questionText}
                    from="weeklyCheckinPlanQuestion"
                  />
                </View>
                <View style={[textInputStyle.dark, textInputStyle.compact]}>
                  <LocalizedTextInput
                    multiline={true}
                    style={[textInputStyle.default, {fontSize: 13, height: 60}]}
                    placeholder="placeholder/weeklyCheckinPlan"
                    value={userInputs.checkinPlan}
                    onChangeText={(text) => this._onInputChange('checkinPlan', text)}
                  />
                </View>
              </View>
              <View style={styles.postDetail}>
                <View style={styles.questionTextContainer}>
                  <LocalizedText
                    style={styles.questionText}
                    from="weeklyCheckinMeasurementQuestion"
                  />
                </View>
                <View style={{flexDirection: 'row'}}>
                  <View style={[styles.textInputWithLabelColumn, {flex: 1, flexDirection: 'row'}]}>
                    <View style={[textInputStyle.dark, {flex: 4, justifyContent: 'flex-start', paddingVertical: 3, paddingHorizontal: 5}]}>
                      <LocalizedTextInput
                        keyboardType="numeric"
                        placeholder="weight"
                        isBlockTextInput={false}
                        style={[textInputStyle.default, {flex: 1, paddingLeft: 5, height: 25, fontSize: 13}]}
                        value={userInputs.checkinWeight}
                        onChangeText={(text: string) => this._onInputChange('checkinWeight', text)}
                      />
                    </View>
                    <Text style={[styles.durationLabel, {flex: 1}]}>kg</Text>
                  </View>
                  <View style={[styles.textInputWithLabelColumn, {flex: 1, flexDirection: 'row'}]}>
                    <View style={[textInputStyle.dark, {flex: 3, justifyContent: 'flex-start', paddingVertical: 3, paddingHorizontal: 5}]}>
                      <LocalizedTextInput
                        keyboardType="numeric"
                        placeholder="waistline"
                        isBlockTextInput={false}
                        style={[textInputStyle.default, {flex: 1, paddingLeft: 5, height: 25, fontSize: 13}]}
                        value={userInputs.checkinWaist}
                        onChangeText={(text: string) => this._onInputChange('checkinWaist', text)}
                      />
                    </View>
                    <View style={styles.durationLabelContainer}>
                      <LocalizedText style={[styles.durationLabel, {marginRight: 0}]} from="inches" />
                    </View>
                  </View>
                </View>
              </View>
            </View>
            <View style={styles.postDetail}>
              <LocalizedText from="weeklyCheckinMotivation" style={styles.weeklyCheckinMotivationText} />
            </View>
            <Button
              type="primary"
              isBlockButton={true}
              text="saveNotes"
              style={styles.iconContainer}
              color="blue"
              onPress={this._onSubmit}
              upperCase={false}
            />
          </CardView>
        </View>
      </ScrollComponent>
    );
  }
  _setVisibility(value: Privacy): void {
    this.setState({
      selectedVisibility: value,
    });
  }
  _onSubmit() {
    let {selectedVisibility, userInputs} = this.state;
    let {onSubmit, user, onValidateFail} = this.props;
    let error;
    error = validateWeeklyCheckin(userInputs.checkinReview, userInputs.checkinPlan, userInputs.checkinWeight, userInputs.checkinWaist);
    if (error == null) {
      onSubmit(selectedVisibility, user, userInputs);
    } else {
      onValidateFail(error);
    }
  }
  _onInputChange(key: UserInputsKey, value: string) {
    let {userInputs} = this.state;
    let newUserInputs = {
      ...userInputs,
      [key]: value,
    };
    this.setState({userInputs: newUserInputs});
  }
}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    isLoading: state.loadingPage.weeklyCheckinForm,
    weeklyCheckinStatus: state.weeklyCheckin.weeklyCheckinStatus,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onSubmit: (privacy: Privacy, user: User, userInputs: UserInputs) => {
      let {
        checkinReview,
        checkinPlan,
        checkinWeight,
        checkinWaist,
      } = userInputs;

      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'weeklyCheckinForm',
      });

      dispatch({
        type: 'POST_WEEKLY_CHECKIN_REQUESTED',
        id: Math.random().toString(),
        token: user.token,
        privacy,
        checkinReview,
        checkinPlan,
        checkinWeight,
        checkinWaist,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(WeeklyCheckinFormView);
