// @flow
import {StyleSheet} from 'react-native';

import {
  FONT_BOLD,
  FONT_LIGHT,
} from '../constants/Font';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  textBold: {
    fontFamily: FONT_BOLD,
  },
  textItalic: {
    fontStyle: 'italic',
    fontSize: 14,
  },
  textRegular: {
    fontFamily: FONT_LIGHT,
    fontSize: 14,
    color: '#666',
  },
  textBlock: {
    marginTop: 10,
    marginBottom: 5,
    marginHorizontal: 20,
  },
});
