import {StyleSheet} from 'react-native';
import {
  CONTAINER_COLOR,
  DEFAULT_ICON_COLOR,
} from '../constants/color';
import {
  FONT_BOLD,
  FONT_LIGHT,
} from '../constants/Font';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    flexDirection: 'column',
    backgroundColor: CONTAINER_COLOR,
  },
  card: {
    flexDirection: 'row',
    backgroundColor: 'white',
    marginBottom: 10,
    padding: 0,
  },
  avatarImage: {
    alignItems: 'center',
    paddingRight: 10,
  },
  avatarImageIcon: {
    fontSize: 30,
    color: DEFAULT_ICON_COLOR,
  },
  avatarProfilePicture: {
    width: 25,
    height: 25,
    borderRadius: 25 / 2,
  },
  contentContainer: {
    flex: 1,
  },
  notificationContent: {
    padding: 20,
    paddingVertical: 15,
  },
  notifType: {
    flex: 1,
    fontFamily: FONT_BOLD,
    fontSize: 13,
    color: DEFAULT_ICON_COLOR,
  },
  header: {
    flexDirection: 'row',
    minHeight: 30,
    alignItems: 'center',
    marginBottom: 10,
  },
  content: {
    paddingLeft: 2,
    marginBottom: 15,
  },
  contentText: {
    color: DEFAULT_ICON_COLOR,
    fontSize: 13,
    fontFamily: FONT_LIGHT,
  },
  createdTime: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
  },
  iconStyle: {
    color: DEFAULT_ICON_COLOR,
    fontSize: 13,
    paddingTop: 1,
    marginRight: 5,
  },
  iconText: {
    color: DEFAULT_ICON_COLOR,
    fontFamily: FONT_LIGHT,
    fontSize: 11,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerTouchable: {
    flexDirection: 'row',
  },
  noNotificationText: {
    flex: -1,
    flexWrap: 'wrap',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: FONT_LIGHT,
    color: '#666',
  },
});
