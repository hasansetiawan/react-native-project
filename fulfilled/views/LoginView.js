 // @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import autobind from 'class-autobind';
import {
  Button,
  TextLink,
  LocalizedTextInput,
  LoadingIndicator,
  LocalizedText,
} from '../core-ui';
import {Icon} from '../core-ui';
import RegistrationView from './RegistrationView';
import ResetPasswordView from './ResetPasswordView';
import styles from './Login-style';
import textInputStyle from '../core-ui/TextField-style';
import logo from '../images/fulfilled_logo.png';
import {getScreenSize} from '../helpers/getSize';
import {validateLogin} from '../helpers/validate';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Dispatch';
import type {KeyboardState} from '../types/KeyboardState';

type State = {
  nickname: string;
  password: string;
};

type Props = {
  keyboard: KeyboardState;
  isLoading: boolean;
  parseInstallationID: string;
  onLogin: (username: string, password: string) => void;
  onSignUp: () => void;
  onValidateFail: (errorMessage: string) => void;
  onResetPassword: () => void;
};

export class LoginView extends Component {
  state: State;
  props: Props;
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      nickname: '',
      password: '',
    };
  }

  render() {
    let {keyboard, onSignUp, isLoading, onResetPassword} = this.props;
    let marginTop = 0;
    if (keyboard.isKeyboardShown === true) {
      let {height} = getScreenSize();
      marginTop = 1 - keyboard.keyboardSpace + (height * 0.2);
    }

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.mainContainer, {marginTop}]}>
          {(isLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <View style={styles.logoContainer}>
            <Image style={styles.image} source={logo} resizeMode="contain" />
            <Text style={styles.logoText}>Fulfilled</Text>
          </View>
          <View style={styles.inputContainer}>
            <View style={textInputStyle.plainRow}>
              <Icon name="person" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                value={this.state.nickname}
                placeholder="placeholder/nickname"
                style={[textInputStyle.default, {paddingLeft: 15}]}
                autoCapitalize="none"
                autoCorrect={false}
                onChangeText={this.onUsernameChange}
              />
            </View>
            <View style={textInputStyle.plainRow}>
              <Icon name="lock" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                value={this.state.password}
                placeholder="placeholder/password"
                style={[textInputStyle.default, {paddingLeft: 15}]}
                secureTextEntry={true}
                onChangeText={this.onPasswordChange}
              />
            </View>
          </View>
          <View style={styles.loginContainer}>
            <Button color="theme" type="primary" text="login" onPress={this.onLoginPress} />
            <View style={styles.forgetPassword}>
              <Text style={styles.registerText}>
                <TextLink onPress={onResetPassword}>
                  <LocalizedText from="forgetPassword" />
                </TextLink>
              </Text>
            </View>
          </View>
          <View style={styles.registerContainer}>
            <Text style={styles.registerText}>
              <LocalizedText from="noAccount" />
              <TextLink onPress={onSignUp}>
                <LocalizedText from="signUp" />
              </TextLink>
            </Text>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  onUsernameChange(nickname: string) {
    this.setState({nickname});
  }

  onPasswordChange(password: string) {
    this.setState({password});
  }

  onLoginPress() {
    let {onLogin, onValidateFail, parseInstallationID} = this.props;
    let {nickname, password} = this.state;
    let error = validateLogin(nickname, password);
    if (error == null) {
      onLogin(nickname, password, parseInstallationID);
    } else {
      onValidateFail(error);
    }
  }
}

function mapStateToProps(state: RootState) {
  return {
    keyboard: state.keyboard,
    isLoading: state.loadingPage.loginPage,
    parseInstallationID: state.pushNotification.parseInstallationID,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onResetPassword: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'signup',
          Component: ResetPasswordView,
        },
      });
    },
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onLogin: (nickname: string, password: string, parseInstallationID: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'loginPage',
      });
      dispatch({
        type: 'LOGIN',
        nickname,
        password,
        parseInstallationID,
        todaysDate: new Date(),
      });
    },
    onSignUp: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'signup',
          Component: RegistrationView,
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(LoginView);
