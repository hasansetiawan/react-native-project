// @flow
import React, {Component} from 'react';
import {
 View,
 Text,
 Image,
} from 'react-native';
import autobind from 'class-autobind';
import {connect} from 'react-redux';

import {
  TextLink,
} from '../core-ui';
import WebView from './WebView';
import {Icon} from '../core-ui';
import styles from './AboutView-style';

import logo from '../images/fulfilled_logo.png';

import getVersion from '../helpers/getVersion';

import type {Dispatch} from '../types/Dispatch';

import {
  WEBSITE,
  SUPPORT_EMAIL,
} from '../constants/companyProfile';

type Props = {
  onLinkClicked: (uri: string) => void;
};

export class AboutView extends Component {
  props: Props
  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    let {version} = getVersion();
    let onLinkClickedHandler = () => {
      this.props.onLinkClicked(WEBSITE);
    };
    return (
      <View style={styles.mainContainer}>
        <View style={styles.logoContainer}>
          <Image style={styles.image} source={logo} resizeMode="contain" />
          <Text style={styles.logoText}>Fulfilled</Text>
        </View>
        <View style={styles.texViewtContainer}>
          <View style={styles.textContainerRow}>
            <View style={styles.textIconContainer}>
              <Icon name="email" style={styles.textIcon} />
            </View>
            <View style={styles.textContainer}>
              <Text style={styles.text}>{SUPPORT_EMAIL}</Text>
            </View>
          </View>
          <View style={styles.textContainerRow}>
            <View style={styles.textIconContainer}>
              <Icon name="public" style={styles.textIcon} />
            </View>
            <View style={styles.textContainer}>
              <TextLink onPress={onLinkClickedHandler}>
                <Text style={styles.text}>{WEBSITE}</Text>
              </TextLink>
            </View>
          </View>
        </View>
        <View style={styles.footer}>
          <Text style={styles.footerText}>version {version}</Text>
        </View>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLinkClicked: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(AboutView);
