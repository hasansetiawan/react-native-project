// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './Registration-style';
import logo from '../images/fulfilled_logo.png';
import textInputStyle from '../core-ui/TextField-style';
import {Icon} from '../core-ui';
import {
  Button,
  LocalizedTextInput,
  LocalizedText,
  TextLink,
  LoadingIndicator,
} from '../core-ui';
import {validateRegister} from '../helpers/validate';
import {getScreenSize} from '../helpers/getSize';

import type {RootState} from '../types/RootState';
import type {KeyboardState} from '../types/KeyboardState';

type State = {
  fullName: string;
  nickname: string;
  password: string;
  email: string;
  visiblePassword: boolean;
};

type Props = {
  isLoading: boolean;
  keyboard: KeyboardState;
  onRegistration: (fullName: string, nickname: string, password: string, email: string, parseInstallationID: string) => void;
  onBackToLogin: () => void;
  onValidateFail: (errorMessage: string) => void;
  parseInstallationID: string;
};

export class RegistrationView extends Component {
  state: State
  props: Props
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      fullName: '',
      nickname: '',
      password: '',
      email: '',
      visiblePassword: false,
    };
  }
  render() {
    let {keyboard, onRegistration, onBackToLogin, isLoading, onValidateFail} = this.props;
    let {visiblePassword} = this.state;

    let marginTop = 0;
    if (keyboard.isKeyboardShown === true) {
      let {height} = getScreenSize();
      marginTop = 1 - keyboard.keyboardSpace + (height * 0.2);
    }

    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={[styles.container, {marginTop}]}>
          {(isLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <View style={styles.messageContainer}>
            <Image style={styles.image} source={logo} resizeMode="contain" />
            <Text style={styles.logoText}>Fulfilled</Text>
          </View>
          <View style={styles.inputContainer}>
            <View style={textInputStyle.plainRow}>
              <Icon name="person" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                placeholder="placeholder/fullName"
                onChangeText={(fullName) => this.setState({fullName})}
                style={[textInputStyle.default, {paddingLeft: 15}]}
                value={this.state.fullName}
              />
              <View style={styles.iconBuffer} />
            </View>
            <View style={textInputStyle.plainRow}>
              <Icon name="account-box" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                placeholder="placeholder/nickname"
                onChangeText={(nickname) => this.setState({nickname})}
                style={[textInputStyle.default, {paddingLeft: 15}]}
                value={this.state.nickname}
                autoCorrect={false}
                autoCapitalize="none"
              />
              <View style={styles.iconBuffer} />
            </View>
            <View style={textInputStyle.plainRow}>
              <Icon name="lock" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                placeholder="placeholder/password"
                onChangeText={(password) => this.setState({password})}
                secureTextEntry={(!visiblePassword) ? true : false}
                style={[textInputStyle.default, {paddingLeft: 15}]}
                value={this.state.password}
                autoCapitalize="none"
              />
              <TouchableOpacity onPress={this._changeVisibility}>
                <Icon name={(visiblePassword) ? 'visibility' : 'visibility-off'} style={textInputStyle.inputIcon} />
              </TouchableOpacity>
            </View>
            <View style={textInputStyle.plainRow}>
              <Icon name="email" style={textInputStyle.inputIcon} />
              <LocalizedTextInput
                placeholder="placeholder/email"
                onChangeText={(email) => this.setState({email})}
                keyboardType="email-address"
                style={[textInputStyle.default, {paddingLeft: 15}]}
                value={this.state.email}
                autoCapitalize="none"
              />
              <View style={styles.iconBuffer} />
            </View>
          </View>
          <View>
            <View style={styles.button}>
              <Button
                color="theme"
                type="primary"
                text="createAccount"
                onPress={() => {
                  let {parseInstallationID} = this.props;
                  let {fullName, nickname, password, email} = this.state;
                  let error = validateRegister(fullName, nickname, password, email);
                  if (error) {
                    onValidateFail(error);
                  } else {
                    onRegistration(fullName, nickname, password, email, parseInstallationID);
                  }
                }}
              />
            </View>
            <View style={styles.loginContainer}>
              <Text style={styles.loginText}>
                <LocalizedText from="haveAccount" />
                <TextLink onPress={onBackToLogin}>
                  <LocalizedText from="login" />
                </TextLink>
              </Text>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }

  _changeVisibility() {
    let {visiblePassword} = this.state;
    this.setState({visiblePassword: !visiblePassword});
  }
  _scrollToInput(reactNode: any) {
    this.refs.scroll.scrollToFocusedInput(reactNode);
  }
}

function mapStateToProps(state: RootState) {
  return {
    keyboard: state.keyboard,
    isLoading: state.loadingPage.signUpPage,
    parseInstallationID: state.pushNotification.parseInstallationID,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onRegistration: (fullName: string, nickname: string, password: string, email: string, parseInstallationID: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'signUpPage',
      });
      dispatch({
        type: 'SIGN_UP_REQUESTED',
        fullName,
        nickname,
        password,
        email,
        parseID: parseInstallationID,
      });
    },
    onBackToLogin: () => {
      dispatch({
        type: 'ROUTE_POPPED',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RegistrationView);
