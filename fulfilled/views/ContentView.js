// @flow

import React from 'react';
import {
  View,
  Text,
} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import ProgressCircleSnail from 'react-native-progress/CircleSnail';

import {
  TextLink,
  ResponsiveImage,
} from '../core-ui';

import WebView from './WebView';
import VideoPlayer from './VideoPlayer';
import styles from './ContentView-style';

import type {Content, EntityNode, Entity} from '../types/Content';
import type {ContentModel} from '../types/ContentModel';

type Props = {
  onLinkPress: (uri: string) => void;
  content: Content;
  textInput: ReactNode;
  imageWrap?: (image: ReactElement<*>) => ReactElement<*>;
  closeButton?: ReactNode;
};

let EMPTY_ENTITY_MAP = {};
let EMPTY_ENTITY = {type: ''};
let EMPTY_DATA = {};

export function ContentView(props: Props) {
  let {content, onLinkPress, textInput, imageWrap, closeButton} = props;
  let {entityMap = EMPTY_ENTITY_MAP, blocks} = content;
  return (
    <KeyboardAwareScrollView style={styles.container}>
      {
        blocks.map((block, blockIndex) => {
          return (
            <View key={blockIndex}>
              {
                block.entityNodes.map((entityNode, entityNodeIndex) => {
                  return (
                    <View key={entityNodeIndex}>
                      <EntityView
                        entityNode={entityNode}
                        entityMap={entityMap}
                        onLinkPress={onLinkPress}
                        textInput={textInput}
                        imageWrap={imageWrap}
                      />
                      <View style={styles.textBlock}>
                        <Text>
                          {
                            (entityNode.entity == null) ?
                            entityNode.styleNodes.map((styleNode, index) => {
                              return (
                                <StyleNode key={index} styleNode={styleNode} />
                              );
                            })
                            : null
                          }
                        </Text>
                      </View>
                    </View>
                  );
                }
              )
            }
            </View>
          );
        })
      }
      {closeButton}
    </KeyboardAwareScrollView>
  );
}

type EntityViewProps = {
  entityNode: EntityNode;
  entityMap: {[key: string]: Entity};
  onLinkPress: (uri: string) => void;
  textInput: any;
  imageWrap?: ?(image: ReactElement<*>) => ReactElement<*>;
};

export function EntityView(props: EntityViewProps) {
  let {entityNode, entityMap, onLinkPress, imageWrap} = props;
  let entityKey = entityNode.entity;
  if (entityKey != null) {
    let entity = entityKey && entityMap[entityKey] || EMPTY_ENTITY;
    if (entity.type === 'IMAGE') {
      let image = (
        <ResponsiveImage
          renderIndicator={() => <ProgressCircleSnail animated={true} color="rgba(136, 136, 136, 1)" size={80} />}
          source={{uri: entity.data.src}}
        />
      );
      return (
        <View style={{flex: 1, justifyContent: 'center'}}>
          {imageWrap ? imageWrap(image) : image}
        </View>
      );
    } else if (entity.type === 'LINK') {
      let {styleNodes} = entityNode;
      let data = entity.data || EMPTY_DATA;
      return (
        <View style={styles.textBlock}>
          <TextLink onPress={() => onLinkPress(data.url)}>
            {styleNodes[0].text}
          </TextLink>
        </View>
      );
    } else if (entity.type === 'INPUT') {
      let {textInput} = props;
      return textInput;
    } else if (entity.type === 'IFRAME') {
      return (
        <VideoPlayer uri={entity.data.src} />
      );
    }
  }
  return null;
}

type StyleNodeProps = {
  styleNode: ContentModel;
};

export function StyleNode(props: StyleNodeProps) {
  let {styleNode} = props;
  let textStyles = [styles.textRegular];
  if (styleNode.styles != null) {
    textStyles = styleNode.styles.map((style) => {
      if (style === 'BOLD') {
        return styles.textBold;
      } else if (style === 'ITALIC') {
        return styles.textItalic;
      } else {
        return styles.textRegular;
      }
    });
  }
  return <Text style={textStyles}>{styleNode.text}</Text>;
}


function mapDispatchToProps(dispatch) {
  return {
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(ContentView);
