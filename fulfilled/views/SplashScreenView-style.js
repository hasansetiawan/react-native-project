// @flow
import {StyleSheet} from 'react-native';
import {CONTAINER_COLOR} from '../constants/color';
import {
  FONT_APP_TITLE,
  FONT_LIGHT,
} from '../constants/Font';

import {
  THEME_COLOR,
} from '../constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: CONTAINER_COLOR,
  },
  logo: {
    width: 180,
    height: 180,
  },
  loadingTextContainer: {
    marginTop: 20,
  },
  loadingText: {
    fontFamily: FONT_LIGHT,
  },
  appTitleText: {
    fontFamily: FONT_APP_TITLE,
    alignSelf: 'center',
    color: THEME_COLOR,
    fontSize: 36,
  },
});
