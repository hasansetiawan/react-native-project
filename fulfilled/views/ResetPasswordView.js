// @flow
import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';
import {connect} from 'react-redux';
import styles from './Registration-style';
import textInputStyle from '../core-ui/TextField-style';
import {
  Button,
  LocalizedTextInput,
  TextLink,
  LoadingIndicator,
  PopupModal,
} from '../core-ui';
import logo from '../images/fulfilled_logo.png';
import {validateResetPassword} from '../helpers/validate';

import type {RootState} from '../types/RootState';

type State = {
  input: string;
};

type Props = {
  isLoading: boolean;
  showResetPasswordConfirmation: boolean;
  onSubmit: (nickname: string) => void;
  onBackToLogin: () => void;
  onValidateFail: (errorMessage: string) => void;
};

export class ResetPasswordView extends Component {
  state: State
  props: Props
  constructor() {
    super(...arguments);
    this.state = {
      input: '',
    };
  }
  render() {
    let {onSubmit, onBackToLogin, isLoading, onValidateFail, showResetPasswordConfirmation} = this.props;
    return (
      <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
        <View style={styles.container}>
          {(isLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <View style={styles.logoContainer}>
            <Image style={styles.image} source={logo} resizeMode="contain" />
            <Text style={styles.logoText}>Fulfilled</Text>
          </View>
          <View style={styles.inputContainer}>
            <View style={textInputStyle.plainRowWithoutPadding}>
              <LocalizedTextInput
                placeholder="placeholder/nicknameOrEmail"
                onChangeText={(input) => this.setState({input})}
                style={[textInputStyle.default, {paddingLeft: 15}]}
                value={this.state.input}
                autoCapitalize="none"
              />
              <View style={styles.iconBuffer} />
            </View>
          </View>
          <View style={styles.button}>
            <Button
              color="theme"
              type="primary"
              text="resetPassword"
              onPress={() => {
                let {input} = this.state;
                let error = validateResetPassword(input);
                if (error) {
                  onValidateFail(error);
                } else {
                  onSubmit(input);
                }
              }}
            />
          </View>
          <View style={styles.loginContainer}>
            <Text style={styles.loginText}>
              Back to{' '}
              <TextLink onPress={onBackToLogin}>
                Login
              </TextLink>
            </Text>
          </View>
          {(showResetPasswordConfirmation === true) ? <PopupModal style={{backgroundColor: 'rgba(0, 0, 0, 0.5)'}} localizedText="resetPasswordConfirmed" onClose={onBackToLogin} /> : null}
        </View>
      </TouchableWithoutFeedback>
    );
  }
}

function mapStateToProps(state: RootState) {
  return {
    isLoading: state.loadingPage.resetPasswordPage,
    showResetPasswordConfirmation: state.showResetPasswordConfirmation,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onSubmit: (nickname: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'resetPasswordPage',
      });
      dispatch({
        type: 'RESET_PASSWORD_REQUESTED',
        nickname,
      });
    },
    onBackToLogin: () => {
      dispatch({
        type: 'CLOSE_RESET_PASSWORD_CONFIRMATION',
      });
      dispatch({
        type: 'ROUTE_POPPED',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ResetPasswordView);
