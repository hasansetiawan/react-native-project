import {StyleSheet} from 'react-native';

import {coachName, coachTitle, card, coachDetail} from '../../core-ui/shared/sharedStyles';
import {getScreenSize} from '../../helpers/getSize';
import {
  FONT_REGULAR,
  FONT_LIGHT,
} from '../../constants/Font';

let {height} = getScreenSize();

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  spacer: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center',
    padding: 25,
    paddingBottom: 30,
  },
  card: card,
  coachIconPicture: {
    color: '#444',
    fontSize: 55,
  },
  coachDetail: {
    ...coachDetail,
  },
  coachProfileButton: {
    justifyContent: 'flex-end',
  },
  coachName: coachName,
  coachTitle: coachTitle,
  conversationSpacer: {
    marginBottom: 10,
  },
  messageInput: {
    padding: 10,
    paddingHorizontal: 12,
    flexDirection: 'row',
    justifyContent: 'center',
    backgroundColor: 'white',
    alignItems: 'center',
  },
  submitButton: {
    marginLeft: 5,
    alignSelf: 'flex-end',
    justifyContent: 'center',
  },
  avatarImage: {
    width: height * 0.10,
    height: height * 0.10,
    borderRadius: (height * 0.10) / 2,
  },
  noChatWrapper: {
    flexDirection: 'row',
  },
  noChatText: {
    flex: -1,
    flexWrap: 'wrap',
    textAlign: 'center',
    fontSize: 12,
    fontFamily: FONT_LIGHT,
    color: '#666',
  },
  buttonProfile: {
    borderRadius: 4,
    padding: 5,
  },
  textButtonProfile: {
    fontFamily: FONT_REGULAR,
    fontSize: 12,
    padding: 0,
    marginHorizontal: 5,
  },
  iconButtonProfile: {
    color: 'white',
    fontSize: 12,
    marginLeft: 5,
  },
  inputChatContainer: {
    flex: 7,
    height: 30,
  },
  submitButtonContainer: {
    flex: 1,
    justifyContent: 'center',
  },
  premiumAds: {
    backgroundColor: '#DDDDDD',
    paddingHorizontal: 20,
    paddingVertical: 15,
  },
  premiumAdsText: {
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
    textAlign: 'center',
    marginBottom: 10,
  },
  premiumAdsButton: {
    backgroundColor: '#4A90E2',
  },
  modalRoot: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
    alignItems: 'center',
  },
  modalContainer: {
    borderRadius: 5,
    backgroundColor: 'white',
    minHeight: 100,
    marginHorizontal: 50,
    padding: 20,
    justifyContent: 'space-around',
  },
  modalButtonContainer: {
    minWidth: 100,
  },
  modalTextContainer: {
    flexWrap: 'wrap',
    marginBottom: 20,
  },
  modalText: {
    textAlign: 'center',
  },
  chatMessage: {
    paddingHorizontal: 10,
  },
  wordCountStatus: {
    flex: 1,
    flexDirection: 'row',
    height: 40,
  },
  closeWordCountStatusButtonContainer: {
    flex: 1,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingRight: 5,
    paddingLeft: 3,
  },
  wordCountStatusTextContainer: {
    flex: 11,
    alignItems: 'flex-start',
    justifyContent: 'center',
    paddingLeft: 10,
  },
  wordCountStatusText: {
    fontSize: 12,
    fontFamily: FONT_LIGHT,
    flexWrap: 'wrap',
  },
});
