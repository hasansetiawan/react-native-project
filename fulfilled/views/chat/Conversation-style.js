import {StyleSheet} from 'react-native';
import {
  FONT_LIGHT,
} from '../../constants/Font';

let chatItem = {
  flexDirection: 'row',
};

let baloonAndTime = {
  paddingTop: 15,
  flex: 1,
  flexDirection: 'row',
};

let baloonRectangle = {
  paddingVertical: 8,
  paddingHorizontal: 13,
  borderRadius: 6,
  shadowColor: '#444',
  shadowOffset: {
    width: 1,
    height: 2,
  },
  shadowOpacity: 0.5,
  shadowRadius: 1,
};

let content = {
  flexWrap: 'wrap',
  fontFamily: FONT_LIGHT,
  fontSize: 14,
};

export default StyleSheet.create({
  container: {
    paddingHorizontal: 15,
    paddingVertical: 10,
  },
  chatItemUser: {
    ...chatItem,
    justifyContent: 'flex-end',
  },
  chatItemCoach: {
    ...chatItem,
    justifyContent: 'flex-start',
  },
  baloonAndTimeUser: {
    ...baloonAndTime,
    justifyContent: 'flex-end',
  },
  baloonAndTimeCoach: {
    ...baloonAndTime,
    justifyContent: 'flex-start',
  },
  time: {
    fontFamily: FONT_LIGHT,
    paddingTop: 10,
    fontSize: 9.5,
    color: '#888',
  },
  baloon: {
    flex: -1,
    flexDirection: 'row',
    marginHorizontal: 10,
    marginBottom: 5,
    flexWrap: 'wrap',
  },
  baloonRectangleUser: {
    ...baloonRectangle,
    backgroundColor: '#F09459',
  },
  baloonRectangleCoach: {
    ...baloonRectangle,
    backgroundColor: 'white',
  },
  contentUser: {
    ...content,
    color: '#fff',
  },
  contentCoach: {
    ...content,
    color: '#555',
  },
  avatarContainer: {
    paddingTop: 14,
  },
  avatar: {
    fontSize: 30,
    color: '#666',
  },
});
