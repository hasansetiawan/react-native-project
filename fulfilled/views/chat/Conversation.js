// @flow
import autobind from 'class-autobind';
import React, {Component} from 'react';
import {
  Text,
  View,
} from 'react-native';
import {connect} from 'react-redux';
import {Icon, LocalizedDate, TextLink} from '../../core-ui';
import WebView from '../WebView';
import findWords from '../../helpers/findWords';
import style from './Conversation-style';
import {ELEVATION} from '../../constants/Size';
import {TEXTLINK_REGEX} from '../../constants/regularExpression';

import type {Dispatch} from '../../types/Dispatch';

type Chat = {
  content: Array<string>;
  timestamp: string;
};

type Props = {
  isIncoming: boolean;
  content: string;
  timestamp: string;
  onLinkPress: (uri: string) => void;
};

export function UserChat(props: Chat) {
  let {timestamp, content} = props;
  return (
    <View style={style.chatItemUser} key={Math.random()}>
      <View style={style.baloonAndTimeUser}>
        <LocalizedDate style={style.time} date={timestamp} format="DATE_TIME_SHORT" />
        <View style={style.baloon}>
          <View style={style.baloonRectangleUser} elevation={ELEVATION}>
            <Text style={style.contentUser}>{content}</Text>
          </View>
        </View>
      </View>
      <View style={style.avatarContainer}>
        <Icon name="account-circle" style={style.avatar} />
      </View>
    </View>
  );
}

export function OtherChat(props: Chat) {
  let {timestamp, content} = props;
  return (
    <View style={style.chatItemCoach}>
      <View style={style.avatarContainer}>
        <Icon name="account-circle" style={style.avatar} />
      </View>
      <View style={style.baloonAndTimeCoach}>
        <View style={style.baloon}>
          <View style={style.baloonRectangleCoach} elevation={ELEVATION}>
            <Text style={style.contentCoach}>{content}</Text>
          </View>
        </View>
        <LocalizedDate style={style.time} date={timestamp} format="DATE_TIME_SHORT" />
      </View>
    </View>
  );
}

class ChatMessage extends Component {
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    let {isIncoming, content, timestamp, onLinkPress} = this.props;

    let parts = [];
    findWords(content, TEXTLINK_REGEX, (isURL, word) => {
      if (isURL) {
        parts.push(
          <TextLink onPress={() => onLinkPress(word)}>
            {word}
          </TextLink>
        );
      } else {
        parts.push(word);
      }
    });
    if (isIncoming) {
      return <OtherChat content={parts} timestamp={timestamp} />;
    } else {
      return <UserChat content={parts} timestamp={timestamp} />;
    }
  }

  _renderConversation(conversation: Array<Chat>) {
    return conversation.map((chatItem, index) => {
      let {timestamp, content} = chatItem;
      if (chatItem.isIncomming) {
        return (<OtherChat timestamp={timestamp} content={content} key={index} />);
      }
      return (<UserChat timestamp={timestamp} content={content} key={index} />);
    });
  }
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(ChatMessage);
