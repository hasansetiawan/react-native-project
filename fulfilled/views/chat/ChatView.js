// @flow
import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
  RefreshControl,
  Platform,
  TouchableWithoutFeedback,
  Keyboard,
  Modal,
} from 'react-native';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import {
  CardView,
  IconButton,
  Icon,
  LoadingIndicator,
  LocalizedText,
  LocalizedTextInput,
  Button,
} from '../../core-ui';
import style from './Chat-style';
import inputTextStyle from '../../core-ui/TextField-style';
import ChatMessage from './Conversation';
import CoachIntroductionView from '../CoachIntroductionView';
import SubscriptionView from '../SubscriptionView';
import MessageScrollView, {Message} from '../MessageScrollView';
//import InvertibleScrollView from 'react-native-invertible-scroll-view';

import hitSlop from '../../constants/hitSlop';
import {getScreenSize} from '../../helpers/getSize';
import getCoachTitle from '../../helpers/getCoachTitle';
import type {CoachTitle} from '../../types/Coach';
import type {User, ProfilePicture} from '../../types/User';
import type {RootState} from '../../types/RootState';
import type {KeyboardState} from '../../types/KeyboardState';

type State = {
  refreshing: boolean;
  sendButtonPressed: boolean;
  showChatLimitationModal: boolean;
};

type Props = {
  keyboard: KeyboardState;
  showWordCountLeftStatus: boolean;
  conversation: Array<Chat>;
  user: User;
  coach: Coach;
  isLoading: boolean;
  recipientNickname: string;
  page: number;
  inputChat: string;
  onChatSubmit: (text: string, userToken: string, userNickname: string, coachNickname: string) => void;
  onLoadOldConversation: (token: string, page: number, recipient: string) => void;
  onInputChatChanged: (text: string) => void;
  onProfileCoachClicked: () => void;
  onSubscribePress: () => void;
  closeWordCountStatus: () => void;
};

type Chat = {
  id: string;
  content: string;
  timestamp: string;
  isIncoming: boolean;
};

type Coach = {
  id: string;
  nickname: string;
  name: string;
  title: CoachTitle;
  profilePicture?: ProfilePicture;
};

export class ChatView extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      refreshing: false,
      sendButtonPressed: false,
      showChatLimitationModal: false,
    };
  }

  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (oldProps.user.wordCountLeft !== newProps.user.wordCountLeft) {
      if (this.state.sendButtonPressed === true && newProps.user.wordCountLeft <= 0) {
        Keyboard.dismiss();
        this.setState({showChatLimitationModal: true});
      }
    }
  }

  render() {
    let {_onChangeText, _submitPressed} = this;
    let {closeWordCountStatus, user, conversation, coach, isLoading, inputChat, onProfileCoachClicked, keyboard} = this.props;
    let {name, title} = coach;
    if (isLoading === true) {
      return (
        <View style={style.container}>
          <LoadingIndicator />
        </View>
      );
    }
    let coachView;
    if (coach.profilePicture != null && coach.profilePicture.standardResolutionUrl != null) {
      coachView = (
        <View>
          <Image
            style={style.avatarImage}
            source={{uri: coach.profilePicture.standardResolutionUrl}}
          />
        </View>
      );
    } else {
      coachView = (
        <Icon name="account-circle" style={style.coachIconPicture} />
      );
    }
    let convo;
    if (conversation.length === 0) {
      convo = (
        <TouchableWithoutFeedback onPress={() => Keyboard.dismiss()}>
          <View style={style.spacer}>
            <View style={style.noChatWrapper}>
              <LocalizedText from="noChat" style={style.noChatText} />
            </View>
          </View>
        </TouchableWithoutFeedback>
      );
    } else {
      convo = (
        <MessageScrollView
          style={style.scrollView}
          refreshControl={
            <RefreshControl
              refreshing={this.state.refreshing}
              onRefresh={this._onRefresh}
            />
          }
        >
          {this._renderConversation()}
        </MessageScrollView>
      );
    }
    let marginBottom = 0;
    let coachProfile;
    if (keyboard.isKeyboardShown === true) {
      let {height} = getScreenSize();
      marginBottom = keyboard.keyboardSpace - (height * 0.08);
    } else {
      coachProfile = (
        <CardView>
          <View style={style.card}>
            {coachView}
            <View style={style.coachDetail}>
              <Text style={style.coachName}>{name}</Text>
              <LocalizedText from={`userTitle/${title}`} style={style.coachTitle} />
              <Button
                style={style.buttonProfile}
                onPress={onProfileCoachClicked}
                textStyle={style.textButtonProfile}
                text="seeProfile"
                type="primary"
                color="theme"
              />
            </View>
          </View>
        </CardView>
      );
    }

    let textInput = (
      <View style={[inputTextStyle.white, inputTextStyle.multiline, (Platform.OS === 'ios') ? {marginBottom} : {}]}>
        <View style={style.inputChatContainer}>
          <LocalizedTextInput
            multiline={false}
            style={[inputTextStyle.default, {paddingTop: 5, paddingBottom: 10, paddingLeft: 15}]}
            value={inputChat}
            placeholder="chat/typeMessage"
            autoCapitalize="none"
            autoCorrect={false}
            onSubmitEditing={() => {
              if (inputChat) {
                _submitPressed();
              }
            }}
            onChangeText={_onChangeText}
            returnKeyType="send"
            autogrow={true}
          />
        </View>
        <View style={style.submitButtonContainer}>
          <IconButton style={style.submitButton} iconName="send" isDisabled={inputChat === ''} onPress={_submitPressed} hitSlop={hitSlop} />
        </View>
      </View>
    );

    if (user.isPremiumMember === false && user.wordCountLeft <= 0) {
      textInput = (
        <View style={style.premiumAds}>
          <LocalizedText style={style.premiumAdsText} from="programs/subscriptionAdsChat" />
          <View style={{justifyContent: 'center'}}>
            <Button
              iconStyle={{
                fontSize: 20,
                color: 'white',
              }}
              color="blue"
              text="subscribe"
              leftIcon="mail"
              type="primary"
              style={style.premiumAdsButton}
              onPress={this._onSubscribePressed}
            />
          </View>
        </View>
      );
    }

    return (
      <View style={[style.container]}>
        {coachProfile}
        {convo}
        {
          (this.props.showWordCountLeftStatus && this.props.user.wordCountLeft > 0 && this.props.user.isPremiumMember === false) ?
            <View style={style.wordCountStatus}>
              <View style={style.wordCountStatusTextContainer}>
                <Text style={style.wordCountStatusText}>
                  <Text>{this.props.user.wordCountLeft}</Text>
                  <Text>{' '}</Text>
                  <LocalizedText from="remainingWords" />
                </Text>
              </View>
              <View style={style.closeWordCountStatusButtonContainer}>
                <IconButton style={style.closeWordCountStatusButton} iconName="close" onPress={closeWordCountStatus} hitSlop={hitSlop} />
              </View>
            </View> : null
        }
        {textInput}
        <Modal
          visible={this.state.showChatLimitationModal}
          transparent={true}
          onRequestClose={this._closeChatLimitationModal}
          animationType="fade"
        >
          <TouchableWithoutFeedback onPress={this._closeChatLimitationModal}>
            <View style={style.modalRoot}>
              <TouchableWithoutFeedback>
                <View style={style.modalContainer}>
                  <View style={style.modalTextContainer}>
                    <LocalizedText style={style.modalText} from="popUpNotification/chatLimitationMessage" />
                  </View>
                  <View style={style.modalButtonContainer}>
                    <Button
                      iconStyle={{
                        fontSize: 20,
                        color: 'white',
                      }}
                      color="blue"
                      text="subscribe"
                      leftIcon="mail"
                      type="primary"
                      style={style.premiumAdsButton}
                      onPress={this._onSubscribePressed}
                    />
                  </View>
                </View>
              </TouchableWithoutFeedback>
            </View>
          </TouchableWithoutFeedback>
        </Modal>
      </View>
    );
  }
  _renderConversation() {
    let conversation = this.props.conversation;
    return conversation.map((chatItem, index) => {
      let {isIncoming, timestamp, content} = chatItem;
      return (
        <Message id={chatItem.id} style={style.chatMessage}>
          <ChatMessage isIncoming={isIncoming} timestamp={timestamp} content={content} key={index} />
        </Message>
      );
    });
  }
  _onSubscribePressed() {
    let {onSubscribePress} = this.props;
    this.setState({showChatLimitationModal: false});
    onSubscribePress();
  }
  _closeChatLimitationModal() {
    this.setState({showChatLimitationModal: false, sendButtonPressed: false});
  }

  _fetchOldConversation() {
    let {onLoadOldConversation, coach, page, user} = this.props;
    let newPage = page + 1;
    onLoadOldConversation(user.token, newPage, coach.id);
    this.setState({refreshing: false});
  }

  _onRefresh() {
    this.setState({refreshing: true});
    this._fetchOldConversation();
  }

  _onChangeText(newValue: string) {
    this.props.onInputChatChanged(newValue);
  }

  _submitPressed = () => {
    let {onChatSubmit, inputChat} = this.props;

    if (onChatSubmit) {
      onChatSubmit(inputChat, this.props.user.token, this.props.user.nickname, this.props.coach.nickname);
      this.setState({sendButtonPressed: true});
    }
  };
}

import {getOldestToLatestOrderedChat} from '../../selectors/getOldestToLatestOrderedChat';

function mapStateToProps(state: RootState) {
  let coachProfilePicture = state.profilePictureList.get(state.coach.id);
  return {
    keyboard: state.keyboard,
    conversation: Array.from(getOldestToLatestOrderedChat(state.chat.chatList)).map((chat) => {
      return {
        id: chat.id,
        content: chat.text,
        timestamp: chat.createdAt,
        isIncoming: (state.user.nickname === chat.posterNickname) ? false : true,
      };
    }),
    coach: {
      id: state.coach.id,
      nickname: state.coach.nickname,
      name: state.coach.name,
      title: getCoachTitle(state.coach),
      profilePicture: coachProfilePicture,
    },
    user: state.user,
    isLoading: state.loadingPage.chatPage,
    page: state.chat.page,
    inputChat: state.chat.inputChat,
    showWordCountLeftStatus: state.chat.showWordCountLeftStatus,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    closeWordCountStatus: () => {
      dispatch({
        type: 'HIDE_WORD_COUNT_STATUS',
      });
    },
    onProfileCoachClicked: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'coachProfile',
          title: 'pageNames/profile',
          Component: CoachIntroductionView,
        },
      });
    },
    onInputChatChanged: (text: string) => {
      dispatch({
        type: 'INPUT_CHAT_CHANGED',
        input: text,
      });
    },
    onChatSubmit: (text: string, userToken: string, senderNickname: string, recipientNickname: string) => {
      let id = '_' + Math.random().toString();
      let createdAt = new Date().toISOString();
      dispatch({
        type: 'SEND_CHAT_REQUESTED',
        token: userToken,
        id,
        senderNickname,
        recipientNickname,
        text,
      });
      dispatch({
        type: 'CHAT_ADDED',
        id,
        createdAt,
        senderNickname,
        recipientNickname,
      });
    },
    onLoadOldConversation: (token: string, page: number, recipient: string) => {
      dispatch({
        type: 'FETCH_CHAT_LIST_REQUESTED',
        token,
        page,
        recipient,
      });
    },
    onSubscribePress: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'subscriptionView',
          Component: SubscriptionView,
          title: 'subscription',
        },
      });
    },
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ChatView);
