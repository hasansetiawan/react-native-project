// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  ListView,
  RecyclerViewBackedScrollView,
  ActivityIndicator,
  Platform,
} from 'react-native';
import {connect} from 'react-redux';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';


import WebView from './WebView';
import {CommunityCard, LoadingIndicator, LocalizedText} from '../core-ui';
import {getLatestOrderedPosts} from '../selectors/getLatestOrderedPosts';

import styles from './CommunityView-style';

import type {Post} from '../types/Post';
import type {RootState} from '../types/RootState';
import type {User, CommunityMember, ProfilePicture} from '../types/User';
import type {Coach} from '../types/Coach';

type Props = {
  coach: Coach;
  posts: Array<Post>;
  communityMembers: Map<string, CommunityMember>;
  comments: Map<string, Comment>;
  user: User;
  targettedUserID: string;
  isLoading: boolean;
  isLoadingMore: boolean;
  lastPostID: string;
  page: number;
  isRefreshingList: boolean;
  profilePictureList: Map<string, ProfilePicture>;
  onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => void;
  onSubmitComment: (postID: string, text: string, user: User) => void;
  onCreatePostPress: (category: string, coach: Coach) => void;
  onUserClick: (user: User, targettedUser: CommunityMember) => void;
  onLoadMorePost: (lastPostID: string, token: string, page: number, userID: string) => void;
  onImageClick: (uri: string) => void;
  onLinkPress: (uri: string) => void;
};

type State = {
  dataSource: Object;
  isLoadingMore: boolean;
};

export class PostHistoryView extends Component {
  props: Props
  state: State
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      dataSource: this._generateDataSource(this.props.posts),
      isLoadingMore: false,
    };
  }
  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (newProps.posts !== oldProps.posts) {
      this.setState({
        dataSource: this._generateDataSource(newProps.posts),
      });
    }
    if (newProps.isLoadingMore !== oldProps.isLoadingMore) {
      this.setState({isLoadingMore: newProps.isLoadingMore});
    }
  }

  render() {
    let {
      user,
      communityMembers,
      comments,
      onLikePress,
      onSubmitComment,
      isLoading,
      onImageClick,
      profilePictureList,
      onLinkPress,
    } = this.props;

    let {isLoadingMore} = this.state;

    if (isLoading === true) {
      return (
        <View style={{flex: 1}}>
          <LoadingIndicator />
        </View>
      );
    }
    if (this.props.posts.length === 0) {
      return (
        <Text style={styles.textCenter}>
          <LocalizedText from="noPostHistory" />
        </Text>
      );
    }
    return (
      <ListView
        style={{flex: 1}}
        dataSource={this.state.dataSource}
        initialListSize={3}
        scrollRenderAheadDistance={50}
        renderRow={(post) => {
          let postComments = [];
          for (let id of post.comments) {
            postComments.push(comments.get(id));
          }

          let likers = [];
          for (let id of post.likers) {
            likers.push(communityMembers.get(id));
          }

          return (
            <View style={styles.cardContainer}>
              <CommunityCard
                user={user}
                post={post}
                profilePictureList={profilePictureList}
                communityMembers={communityMembers}
                comments={postComments}
                likers={likers}
                onLikePress={onLikePress}
                onSubmitComment={onSubmitComment}
                onImageClick={onImageClick}
                onLinkPress={onLinkPress}
              />
            </View>
          );
        }}
        renderFooter={() => <ActivityIndicator size="large" animating={isLoadingMore} />}
        onEndReached={this._fetchMorePosts}
        renderScrollComponent={(props) => {
          if (Platform.OS === 'ios') {
            return <KeyboardAwareScrollView {...props} />;
          } else {
            return <RecyclerViewBackedScrollView {...props} />;
          }
        }}
      />
    );
  }

  _generateDataSource(posts: Array<Post>) {
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    return ds.cloneWithRows(posts);
  }

  _fetchMorePosts() {
    let {onLoadMorePost, lastPostID, page, user, targettedUserID} = this.props;
    let newPage = page + 1;
    onLoadMorePost(lastPostID, user.token, newPage, targettedUserID);
  }
}

function mapStateToProps(state: RootState) {
  return {
    posts: getLatestOrderedPosts(state.userPostsHistory.postList),
    communityMembers: new Map(state.communityMembers),
    comments: new Map(state.comments),
    user: state.user,
    isLoading: state.loadingPage.postHistoryPage,
    lastPostID: state.userPostsHistory.lastPostID,
    page: state.userPostsHistory.page,
    isLoadingMore: state.userPostsHistory.isLoadingMore,
    profilePictureList: state.profilePictureList,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onImageClick: (uri: string) => {
      dispatch({
        type: 'IMAGE_URI_RECEIVED',
        uri,
      });
    },
    onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => {
      if (isLiked === false) {
        dispatch({
          type: 'LIKE_ADDED',
          token: userToken,
          postID,
          userID,
        });
      }
    },
    onSubmitComment: (postID: string, text: string, user: User) => {
      let id = '_' + Math.random().toString();
      dispatch({
        type: 'COMMENT_ADDED',
        postID,
        comment: {
          id,
          text,
          user: user.id,
        },
      });
      dispatch({
        type: 'SEND_COMMENT_REQUESTED',
        postID,
        token: user.token,
        text,
      });
    },
    onLoadMorePost: (lastPostID: string, token: string, page: number, userID: string) => {
      dispatch({
        type: 'FETCH_MORE_USER_POST_HISTORY_START',
      });
      dispatch({
        type: 'USER_POST_HISTORY_REQUESTED',
        token,
        page,
        userID,
        lastPostID,
      });
    },
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PostHistoryView);
