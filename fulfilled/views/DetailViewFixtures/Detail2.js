// @flow

import React from 'react';
import {View, Text} from 'react-native';

function Detail1() {
  return (
    <View style={{flex: 1, backgroundColor: 'red', alignItems: 'center', justifyContent: 'center'}}>
      <Text>Detail 2</Text>
    </View>
  );
}

export default Detail1;
