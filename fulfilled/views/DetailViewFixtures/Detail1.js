// @flow

import React from 'react';
import {connect} from 'react-redux';
import {View, Text, TouchableOpacity} from 'react-native';

import Detail2 from './Detail2';

function Detail1(props: Object) {
  let {onDetail2Click} = props;
  return (
    <View style={{flex: 1, backgroundColor: 'blue', alignItems: 'center', justifyContent: 'center'}}>
      <Text>Detail1</Text>
      <TouchableOpacity onPress={() => onDetail2Click()}>
        <Text style={{color: 'white'}}>Go to detail 1</Text>
      </TouchableOpacity>
    </View>
  );
}

function mapDispatchToProps(dispatch) {
  return {
    onDetail2Click: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: '/detail',
          Component: Detail2,
        },
      });
    },
    onBack: () => {
      dispatch({
        type: 'ROUTE_POPPED',
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(Detail1);
