let {describe, it} = global;
import React from 'react';
import expect from 'expect';
import {shallow} from 'enzyme';
import {ResponsiveImage} from '../../core-ui';

import {EntityView, StyleNode} from '../ContentView';
import {
  FONT_BOLD,
} from '../../constants/Font';

describe('Content View', () => {
  let entityMap = {
    data1: {
      type: 'IMAGE',
      data: {
        src: 'source url image',
      },
    },
    data2: {
      type: 'LINK',
      data: {
        url: 'url test',
      },
    },
  };

  it('should render EntityView for Image', () => {
    let entityNode = {
      entity: 'data1',
    };

    let wrapper = shallow(<EntityView entityNode={entityNode} entityMap={entityMap} />);
    let image = wrapper.find(ResponsiveImage);

    expect(image.length).toBe(1);
    expect(image.prop('source')).toEqual({uri: 'source url image'});
    expect(wrapper.find('TextLink').length).toBe(0);

  });

  it('should render EntityView for Link', () => {
    let entityNode = {
      entity: 'data2',
      styleNodes: [{
        text: 'textUrl',
      }],
    };

    let wrapper = shallow(<EntityView entityNode={entityNode} entityMap={entityMap} />);
    let link = wrapper.find('TextLink');

    expect(link.length).toBe(1);
    expect(wrapper.find('Image').length).toBe(0);

  });

  it('should render text with style', () => {
    let styleNode = {
      text: 'textUrl',
      styles: ['BOLD'],
    };

    let wrapper = shallow(<StyleNode styleNode={styleNode} />);
    let text = wrapper.find('Text');

    expect(text.at(0).prop('style')).toEqual([{fontFamily: FONT_BOLD}]);
  });
});
