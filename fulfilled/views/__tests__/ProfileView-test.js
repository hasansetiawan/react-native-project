let {describe, it} = global;

import React from 'react';
import {Text} from 'react-native';
import {LocalizedText} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

import {AvatarView, HeaderMenuView} from '../ProfileView';

describe('Profile View', () => {
  let headerMenus = [
    {icon: 'check-box', title: 'profilePage/checkCount', value: 42},
    {icon: 'format-list-bulleted', title: 'profilePage/longestCheckStreak', value: 21},
  ];
  it('should render icon if there is no avatar image', () => {
    let container = shallow(<AvatarView username="test username" />);
    let icon = container.find('Icon');
    let image = container.find('Image');
    expect(icon.length).toBe(1);
    expect(image.length).toBe(0);
  });
  it('should render the Header Menus', () => {
    let container = shallow(<HeaderMenuView headerMenus={headerMenus} />);
    let texts = container.find('Text');
    let localTexts = container.find('LocalizedText');
    expect(texts.length).toBe(2);
    expect(localTexts.length).toBe(2);

    expect(localTexts.at(0).matchesElement(
      <LocalizedText from={headerMenus[0].title} />
    )).toBe(true);

    expect(texts.at(0).matchesElement(
      <Text>{headerMenus[0].value}</Text>
    )).toBe(true);
  });
});
