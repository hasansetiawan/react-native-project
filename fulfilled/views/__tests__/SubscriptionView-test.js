let {describe, it} = global;

import React from 'react';
import expect, {createSpy} from 'expect';
import {shallow} from 'enzyme';
import {SubscriptionCard} from '../SubscriptionView';

describe('Subscription View', () => {
  it('SubscriptionCard should render stuff properly', () => {
    let onPress = createSpy();
    let detail = {
      id: 'id',
      priceString: 'priceString',
      description: 'description',
      title: 'title',
    };
    let wrapper = shallow(<SubscriptionCard detail={detail} onPress={onPress} />);
    expect(wrapper.find('TouchableOpacity').length).toBe(1);
    expect(wrapper.find('View').length).toBe(3);
    expect(wrapper.find('Icon').length).toBe(1);
    wrapper.find('TouchableOpacity').simulate('press');
    expect(onPress).toHaveBeenCalled();
  });
});
