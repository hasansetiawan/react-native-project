/* @flow */
import {RegistrationView} from '../RegistrationView';
import React from 'react';
import {LocalizedTextInput} from '../../core-ui';

import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('RegistrationView', () => {

  const props = {
    isLoading: false,
    onRegistration: expect.createSpy(),
    onBackToLogin: expect.createSpy(),
    onValidateFail: expect.createSpy(),
    parseInstallationID: '123',
    keyboard: {isKeyboardShown: false, keyboardSpace: 0},
  };
  const wrapper = shallow(
    <RegistrationView {...props} />
  );
  it('should correctly render RegistrationView', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(LocalizedTextInput).length).toBe(4);
    expect(wrapper.find('Button').length).toBe(1);
  });
});
