// @flow
import React from 'react';
import {connect} from 'react-redux';
import {
  View,
  Image,
  Text,
} from 'react-native';

import {
  LocalizedText,
  Icon,
  LoadingIndicator,
} from '../core-ui';

import ContentView from './ContentView';
import getContentFromHTML from '../selectors/getContentFromHTML';
import getCoachTitle from '../helpers/getCoachTitle';

import styles from './CoachIntroductionView-style';

import type {Coach} from '../types/Coach';
import type {RootState} from '../types/RootState';
import type {Content} from '../types/Content';
import type {ProfilePicture} from '../types/User';

type Props = {
  isLoading: boolean;
  coach: Coach;
  content: ?Content;
  profilePicture?: ProfilePicture;
};

export function CoachIntroductionView(props: Props) {
  let {coach, content, isLoading, profilePicture} = props;
  if (isLoading) {
    return (
      <View style={{flex: 1}}>
        <LoadingIndicator />
      </View>
    );
  }
  let profilePictureContent;
  if (profilePicture && profilePicture.standardResolutionUrl) {
    profilePictureContent = (
      <Image
        style={styles.profilePicture}
        source={{uri: profilePicture.standardResolutionUrl}}
      />
    );
  } else {
    profilePictureContent = (
      <Icon name="account-circle" style={{color: '#444', fontSize: 130}} />
    );
  }

  let coachTitle = getCoachTitle(coach) || '';

  return (
    <View style={styles.root}>
      <View style={styles.profilePictureContainer}>
        {profilePictureContent}
        <Text style={styles.coachName}>{coach.firstName}</Text>
        <LocalizedText from={`userTitle/${coachTitle}`} style={styles.coachTitle} />
      </View>
      <View style={styles.contentShadow} elevation={3}>
        <ContentView content={content} />
      </View>

    </View>
  );
}

function mapStateToProps(state: RootState) {
  let content = state.content.get(state.coach.coachIntroductionID);
  if (content) {
    content = getContentFromHTML(content.renderedBody).content;
  }

  return {
    isLoading: state.loadingPage.coachProfile,
    coach: state.coach,
    profilePicture: state.profilePictureList.get(state.coach.id),
    content,
  };
}

export default connect(mapStateToProps, null)(CoachIntroductionView);
