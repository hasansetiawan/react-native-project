// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  Modal,
  StyleSheet,
  View,
  Image,
  Dimensions,
} from 'react-native';
import PhotoView from 'react-native-photo-view';

import {
  Button,
} from '../core-ui';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Dispatch';

type Props = {
  uri: string;
  showModal: boolean;
  onClose: () => void;
};

type State = {
  width: number;
  height: number;
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'black',
  },
  buttonContainer: {
    paddingTop: 15,
    backgroundColor: 'black',
  },
});

export class ImageViewModal extends Component {
  props: Props
  state: State
  constructor() {
    super(...arguments);
    this.state = {
      width: 100,
      height: 100,
    };
  }
  componentWillReceiveProps(newProps: Props) {
    if (newProps.uri) {
      Image.getSize(newProps.uri, (srcWidth, srcHeight) => {
        const maxHeight = Dimensions.get('window').height; // or something else
        const maxWidth = Dimensions.get('window').width;

        const ratio = Math.min(maxWidth / srcWidth, maxHeight / srcHeight);
        this.setState({width: srcWidth * ratio, height: srcHeight * ratio});
      }, () => {
      });
    }
  }
  render() {
    let {uri, showModal, onClose} = this.props;
    let {width, height} = this.state;
    return (
      <Modal
        animationType="slide"
        transparent={false}
        visible={showModal}
        onRequestClose={() => onClose()}
      >
        <View style={styles.buttonContainer}>
          <Button
            leftIcon="clear"
            iconStyle={{fontSize: 30, color: 'white'}}
            style={{alignSelf: 'flex-end'}}
            onPress={onClose}
          />
        </View>
        <View style={styles.container}>
          <PhotoView
            source={{uri: uri}}
            resizeMode="cover"
            loadingIndicatorSource={{animating: true, color: 'white', size: 'large'}}
            minimumZoomScale={1.0}
            maximumZoomScale={10}
            androidScaleType="fitCenter"
            style={{flex: 1, backgroundColor: 'black', width, height}}
          />
        </View>
      </Modal>
    );
  }
}

function mapStateToProps(state: RootState) {
  return {
    uri: state.imageView.uri,
    showModal: state.imageView.showModal,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onClose: () => {
      dispatch({
        type: 'CLOSE_IMAGE_VIEW_MODAL',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ImageViewModal);
