// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import {
  View,
  ScrollView,
  Image,
} from 'react-native';
import {
  ProgramCard,
  Icon,
  LocalizedText,
  LocalizedTextInput,
  LoadingIndicator,
  Button,
} from '../core-ui';
import SelectedProgramView from './SelectedProgramView';
import SubscriptionView from './SubscriptionView';
import styles from './ProgramView-style';

import type {RootState} from '../types/RootState';
import type {Program} from '../types/Program';
import type {Dispatch} from '../types/Dispatch';
import type {User} from '../types/User';
import type {KeyboardState} from '../types/KeyboardState';

type Props = {
  user: User;
  programs: Map<string, Program>;
  currentProgramID: string;
  isLoading: boolean;
  isRegisterNewSubscriptionLoading: boolean;
  progressBarPercentage: number;
  productIDList: Array<string>;
  keyboard: KeyboardState;
  onProgramSelect: (program: Program) => void;
  showToast: (toastMessage: string) => void;
  showPopup: (content: string) => void;
  onSubscribePress: () => void;
};

type State = {
  filterProgramValue: string;
};


class ProgramView extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);

    this.state = {filterProgramValue: ''};
  }

  render() {
    let {isLoading, user, isRegisterNewSubscriptionLoading, onSubscribePress} = this.props;
    if (isLoading) {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <LoadingIndicator />
        </View>
      );
    }
    return (
      <View style={styles.mainContainer}>
        <ScrollView style={styles.scrollContainer}>
          {(isRegisterNewSubscriptionLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <View style={styles.overView}>
            <View style={styles.overViewWriting}>
              <View style={styles.overViewTitle}>
                <LocalizedText style={styles.overViewTitleText} from="programsTitle" />
              </View>
              <View style={styles.overViewDescription}>
                <LocalizedText style={styles.overViewDescriptionText} from="programs/programOverview" />
              </View>
            </View>
            <View style={styles.overViewImageContainer}>
              <View style={styles.overViewImageContainer}>
                <Image
                  style={styles.overViewImage}
                  source={require('../images/program_overview.png')}
                  resizeMode="contain"
                />
              </View>
            </View>
          </View>
          {
            (user.isPremiumMember === false) ?
              <View style={styles.premiumAds}>
                <LocalizedText style={styles.premiumAdsText} from="programs/subscriptionAds" />
                <View style={{justifyContent: 'center'}}>
                  <Button
                    iconStyle={{
                      fontSize: 20,
                      color: 'white',
                    }}
                    color="blue"
                    text="subscribe"
                    leftIcon="mail"
                    type="primary"
                    style={styles.premiumAdsButton}
                    onPress={onSubscribePress}
                  />
                </View>
              </View> : null
          }
          <View style={styles.searchContainer}>
            <View style={styles.searchBar}>
              <LocalizedTextInput
                placeholder="placeholder/searchProgram"
                placeholderTextColor="#666"
                style={styles.searchTextInput}
                value={this.state.filterProgramValue}
                onChangeText={this._onChangeText}
              />
              <Icon name="search" style={styles.iconSearch} />
            </View>
          </View>
          {this._renderPrograms()}
        </ScrollView>
      </View>
    );
  }
  _onChangeText(newValue: string) {
    this.setState({filterProgramValue: newValue});
  }
  _renderPrograms() {
    let {keyboard, programs, currentProgramID, onProgramSelect, user, progressBarPercentage, onSubscribePress} = this.props;
    let {filterProgramValue} = this.state;
    let programsArray = Array.from(programs.values());
    let filteredPrograms = programsArray.filter((program) => program.title.toLowerCase().indexOf(filterProgramValue.toLowerCase()) >= 0);
    if (filteredPrograms.length === 0) {
      return (
        <View style={[styles.noProgramFound, {marginBottom: keyboard.keyboardSpace + 30}]}>
          <LocalizedText from="noProgramFound" />
        </View>
      );
    }
    return filteredPrograms.map((program, index) => {
      return (
        <ProgramCard
          user={user}
          key={index}
          isActive={(program.id === currentProgramID && progressBarPercentage < 100) ? true : false}
          program={program}
          onPress={() => onProgramSelect(program, onSubscribePress)}
          hasBeenTaken={(user.completedProgramIds.indexOf(program.id) > -1 || progressBarPercentage === 100) ? true : false}
        />
      );
    });
  }

}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    programs: state.programs.programList,
    currentProgramID: state.programs.currentProgram.id,
    isLoading: state.loadingPage.programListPage,
    isRegisterNewSubscriptionLoading: state.loadingPage.registerSubscription,
    progressBarPercentage: state.habit.progressPercentage,
    productIDList: state.productIDList,
    keyboard: state.keyboard,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    showToast: (toastMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage,
      });
    },
    onProgramSelect: (program: Program, subscriptionFunc: Function) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'selectedProgramView',
          Component: SelectedProgramView,
          title: 'programPreview',
          props: {program, onSubscribePress: () => subscriptionFunc()},
        },
      });
    },
    showPopup: (content: string) => {
      dispatch({
        type: 'SHOW_POPUP',
        content,
      });
    },
    onSubscribePress: () => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'subscriptionView',
          Component: SubscriptionView,
          title: 'subscription',
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProgramView);
