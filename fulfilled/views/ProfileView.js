// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  View,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import autobind from 'class-autobind';
import {Icon} from '../core-ui';
import {
  CardView,
  UploadImageModal,
  LocalizedText,
} from '../core-ui';

import PostHistoryView from './PostHistoryView';
import ProgramHistoryView from './ProgramHistoryView';
import SelectedPhotoView from './SelectedPhotoView';
import SubscriptionDetailView from './SubscriptionDetailView';

import styles from './Profile-style';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Dispatch';
import type {User} from '../types/User';
import type {ImageData} from '../types/ImageData';

type Profile = {
  avatarURL?: string;
  username: string;
};

type Menu = {
  icon: string;
  title: string;
  onClick: () => void;
};

type HeaderMenu = {
  title: string;
  icon: string;
  value: number;
};

type Props = {
  profile: Profile;
  user: User;
  headerMenus: Array<HeaderMenu>;
  menus: Array<Menu>;
  onUserPostHistoryClicked: () => void;
  onUserProgramHistoryClicked: () => void;
  onImagePicked: (imageData: ImageData) => void;
  onSubscriptionDetailClicked: (user: User) => void;
};
export function ProfileView(props: Props) {
  let {
    profile: {avatarURL, username},
    headerMenus,
    onUserProgramHistoryClicked,
    onUserPostHistoryClicked,
    user,
    onImagePicked,
    onSubscriptionDetailClicked,
  } = props;
  return (
    <View style={styles.container}>
      <CardView style={styles.header}>
        <AvatarView avatarURL={avatarURL} username={username} onImagePicked={onImagePicked} />
        <HeaderMenuView headerMenus={headerMenus} />
      </CardView>

      <ProfileMenuView
        user={user}
        onUserPostHistoryClicked={onUserPostHistoryClicked}
        onUserProgramHistoryClicked={onUserProgramHistoryClicked}
        onSubscriptionDetailClicked={onSubscriptionDetailClicked}
      />
    </View>
  );
}

type AvatarViewProps = {
  avatarURL?: string;
  username: string;
  onImagePicked: (imageData: ImageData) => void;
};

export class AvatarView extends Component {
  props: AvatarViewProps;
  state: {isModalOpen: boolean};

  constructor() {
    super(...arguments);
    autobind(this);

    this.state = {
      isModalOpen: false,
    };
  }

  render() {
    let {avatarURL, username, onImagePicked} = this.props;
    return (
      <View style={styles.avatar}>
        <TouchableOpacity style={styles.avatarImageContainer} onPress={this._onModalRequestOpen}>
          {
            (avatarURL != null) ? (
              <Image
                style={styles.avatarImage}
                source={{uri: avatarURL}}
              />
            ) : (
              <View style={styles.avatarImageIconContainer}>
                <Icon name="add-a-photo" style={styles.avatarImageIcon} />
              </View>
            )
          }
        </TouchableOpacity>
        <View style={styles.avatarNameContainer}>
          <Text style={styles.avatarName}>{username}</Text>
        </View>
        <UploadImageModal
          visible={this.state.isModalOpen}
          onRequestClose={this._onModalRequestClose}
          animationType="fade"
          onImagePicked={onImagePicked}
        />
      </View>
    );
  }

  _onModalRequestClose() {
    this.setState({isModalOpen: false});
  }
  _onModalRequestOpen() {
    this.setState({isModalOpen: true});
  }
}


type HeaderMenuViewProps = {
  headerMenus: Array<HeaderMenu>;
};

export function HeaderMenuView(props: HeaderMenuViewProps) {
  let {headerMenus} = props;
  return (
    <View style={styles.headerMenu}>
      {
        headerMenus.map((menu, index) => {
          return (
            <View key={index} style={styles.headerMenuItem}>
              <View style={styles.headerMenuTitle}>
                <Icon name={menu.icon} style={styles.headerMenuIcon} />
                <LocalizedText style={styles.headerMenuText} from={menu.title} />
              </View>
              <Text style={styles.checklistNumber}>{(menu.value != null) ? menu.value : 0}</Text>
            </View>
          );
        })
      }
    </View>
  );
}


type ProfileMenuViewProps = {
  user: User;
  onUserPostHistoryClicked: (user: User) => void;
  onUserProgramHistoryClicked: (user: User) => void;
  onSubscriptionDetailClicked: (user: User) => void;
};

export function ProfileMenuView(props: ProfileMenuViewProps) {
  let {onUserPostHistoryClicked, onUserProgramHistoryClicked, user, onSubscriptionDetailClicked} = props;
  let menus = [
    {icon: 'speaker-notes', title: 'profilePage/myPosts', onClick: () => onUserPostHistoryClicked(user)},
    {icon: 'check-box', title: 'profilePage/challengesDone', onClick: () => onUserProgramHistoryClicked(user)},
  ];

  if (user.isPremiumMember === true) {
    menus = [
      ...menus,
      {icon: 'star', title: 'profilePage/subscriptionDetail', onClick: () => onSubscriptionDetailClicked(user)},
    ];
  }

  return (
    <View style={styles.menu}>
      {
        menus.map((menu, index) => {
          return (
            <View key={index} style={styles.touchableMenuContainer} elevation={3}>
              <TouchableOpacity onPress={menu.onClick}>
                <CardView style={styles.menuItem}>
                  <Icon name={menu.icon} style={styles.menuItemIcon} />
                  <LocalizedText from={menu.title} style={styles.menuItemText} />
                  <Icon name="keyboard-arrow-right" style={styles.menuItemDetailIcon} />
                </CardView>
              </TouchableOpacity>
            </View>
          );
        })
      }
    </View>
  );
}


function mapStateToProps(state: RootState) {
  let profilePicture = state.profilePictureList.get(state.user.id);
  return {
    profile: {
      username: state.user.name,
      avatarURL: (profilePicture) ? profilePicture.standardResolutionUrl : null,
    },
    user: state.user,
    headerMenus: [
      {icon: 'check-box', title: 'profilePage/checkCount', value: state.user.habitCheckoffCount},
      {icon: 'library-books', title: 'profilePage/longestCheckStreak', value: state.user.longestStreak},
    ],
    profilePicture: state.profilePictureList.get(state.user.id),
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onImagePicked: (imageData: ImageData) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'profilePage/myPosts',
          Component: SelectedPhotoView,
          title: 'settingPhoto',
          props: {imageData},
        },
      });
    },
    onUserPostHistoryClicked: (user: User) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'postHistoryPage',
      });
      dispatch({
        type: 'USER_POST_HISTORY_REQUESTED',
        token: user.token,
        page: 1,
        userID: user.id,
      });
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'profilePage/myPosts',
          Component: PostHistoryView,
          title: 'profilePage/myPosts',
          props: {targettedUserID: user.id},
        },
      });
    },
    onUserProgramHistoryClicked: (user: User) => {
      dispatch({
        type: 'USER_PROGRAM_HISTORY_REQUESTED',
        token: user.token,
      });
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'profilePage/challengesDone',
          Component: ProgramHistoryView,
          title: 'profilePage/challengesDone',
        },
      });
    },
    onSubscriptionDetailClicked: (user: User) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'profilePage/subscriptionDetail',
          Component: SubscriptionDetailView,
          title: 'profilePage/subscriptionDetail',
          props: {user},
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProfileView);
