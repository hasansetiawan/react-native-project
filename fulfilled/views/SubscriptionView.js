// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import {
  View,
  ScrollView,
  TouchableOpacity,
  Platform,
  Text,
  NativeModules,
} from 'react-native';
import InAppBilling from 'react-native-billing';
import {
  Icon,
  LocalizedText,
  LoadingIndicator,
} from '../core-ui';
import styles from './SubscriptionView-style';

import type {RootState} from '../types/RootState';
import type {ProfilePicture, User} from '../types/User';
import type {Dispatch} from '../types/Dispatch';
import type {PaymentDetail} from '../types/PaymentDetail';

type ProductDetail = {
  id: string;
  priceString: string;
  description: string;
  title: string;
};

type ProductDetailAndroid = {
  productId: string;
  title: string;
  description: string;
  isSubscription: boolean;
  currency: string;
  priceValue: number;
  priceText: string;
};

type ProductDetailIOS = {
  identifier: string;
  price: number;
  currencySymbol: string;
  currencyCode: string;
  priceString: string;
  downloadable: boolean;
  description: string;
  title: string;
};

type Props = {
  user: User;
  onSubscribeAndroid: (user: User, paymentDetail: PaymentDetail) => void;
  onSubscribeIOS: (user: User, receipts: Array<string>) => void;
  coachProfilePicture?: ProfilePicture;
  productIDList: Array<string>;
  showToast: (toastMessage: string) => void;
};

type State = {
  selectedProductID?: string;
  isFetchingProductDetail: boolean;
  isConfirmationDialogShown: boolean;
  productDetailList: Array<ProductDetail>;
};

const {InAppUtils} = NativeModules;

export class SubscriptionView extends Component {
  props: Props
  state: State
  constructor() {
    super(...arguments);
    autobind(this);

    this.state = {
      isFetchingProductDetail: false,
      productDetailList: [],
      isConfirmationDialogShown: false,
    };

    if (this.props.productIDList.length > 0) {
      if (Platform.OS === 'android') {
        InAppBilling.close()
        .then(() => InAppBilling.open())
        .then(() => InAppBilling.getSubscriptionDetailsArray([...this.props.productIDList]))
        .then((details: Array<ProductDetailAndroid>) => {
          let newProductDetailList = details.map((detail: ProductDetailAndroid) => {
            return {
              id: detail.productId,
              priceString: detail.priceText,
              description: detail.description,
              title: detail.title.split('(')[0],
            };
          });
          this.setState({
            isFetchingProductDetail: false,
            productDetailList: newProductDetailList,
          });
          return InAppBilling.close();
        })
        .catch((err) => {
          this.props.showToast(err.message);
          this.setState({
            isFetchingProductDetail: false,
          });
        });
      } else if (Platform.OS === 'ios') {
        InAppUtils.loadProducts(this.props.productIDList, (error, products) => {
          if (error) {
            this.setState({
              isFetchingProductDetail: false,
            });
            return this.props.showToast(error.message);
          }
          let newProductDetailList = products.map((product: ProductDetailIOS) => {
            return {
              id: product.identifier,
              priceString: product.priceString,
              description: product.description,
              title: product.title,
            };
          });
          this.setState({
            isFetchingProductDetail: false,
            productDetailList: newProductDetailList,
          });
        });
      }
    }
  }
  render() {
    let {productDetailList, isFetchingProductDetail} = this.state;
    return (
      <View style={styles.container}>
        <ScrollView>
          {
            (isFetchingProductDetail === true) ?
              <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
                <LoadingIndicator />
              </View> : null
          }
          {
            productDetailList.map((productDetail) => {
              return (
                <SubscriptionCard
                  detail={productDetail}
                  onPress={() => this._onSubscribePressed(productDetail.id)}
                />
              );
            })
          }
          <View style={styles.footer}>
            <LocalizedText from="subscriptionContent/whatsIncluded" style={styles.footerTitle} />
            <View style={styles.footerRow}>
              <Icon name="account-circle" style={styles.footerIcon} />
              <View style={styles.textContainer}>
                <LocalizedText from="subscriptionContent/timelySupportAndFeedback" style={styles.text} />
              </View>
            </View>
            <View style={styles.footerRow}>
              <Icon name="restaurant-menu" style={styles.footerIcon} />
              <View style={styles.textContainer}>
                <LocalizedText from="subscriptionContent/learnHowToEatBetter" style={styles.text} />
              </View>
            </View>
            <View style={styles.footerRow}>
              <Icon name="directions-run" style={styles.footerIcon} />
              <View style={styles.textContainer}>
                <LocalizedText from="subscriptionContent/formHealthyHabits" style={styles.text} />
              </View>
            </View>
            <View style={styles.footerRow}>
              <Icon name="trending-up" style={styles.footerIcon} />
              <View style={styles.textContainer}>
                <LocalizedText from="subscriptionContent/stayConsistent" style={styles.text} />
              </View>
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  _closeConfirmationDialog() {
    this.setState({isConfirmationDialogShown: false});
  }
  _openConfirmationDialog() {
    // let {showPopup} = this.props;
    // showPopup('premiumCommingSoon');

    this.setState({isConfirmationDialogShown: true});
  }
  _onSubscribePressed(productID: string) {
    let {user, showToast, onSubscribeIOS, onSubscribeAndroid} = this.props;
    if (Platform.OS === 'android') {
      InAppBilling.close()
      .then(() => InAppBilling.open())
      .then(() => InAppBilling.subscribe(productID))
      .then((details) => {
        onSubscribeAndroid(user, details);
        return InAppBilling.close();
      })
      .catch((err) => {
        showToast(err.message);
      });
    } else {
      InAppUtils.loadProducts(this.props.productIDList, (error) => {
        if (error) {
          return this.props.showToast(error.message);
        }
        InAppUtils.purchaseProduct(productID, (error) => {
          if (error) {
            return showToast(error.message);
          }
          InAppUtils.receiptData((error, receiptData) => {
            if (error) {
              return showToast(error.message);
            } else {
              return onSubscribeIOS(user, [receiptData]);
            }
          });
        });
      });
    }
  }
}

type SubscriptionCardProps = {
  detail: ProductDetail;
  onPress: () => void;
};

export function SubscriptionCard(props: SubscriptionCardProps) {
  let {detail, onPress} = props;
  return (
    <TouchableOpacity style={styles.subscriptionCard} onPress={onPress}>
      <View style={styles.cardRow}>
        <View style={styles.cardColumn}>
          <Text style={styles.titleText}>{`${detail.title} ${detail.priceString}`}</Text>
          <Text style={styles.text}>{detail.description}</Text>
        </View>
      </View>
      <View style={styles.buttonRow}>
        <Icon name="play-circle-filled" style={styles.cardRightIcon} />
      </View>
    </TouchableOpacity>
  );
}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    productIDList: state.productIDList,
    coachProfilePicture: state.profilePictureList.get(state.coach.id),
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    showToast(toastMessage: string) {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage,
      });
    },
    onSubscribeIOS: (user: User, receipts: Array<string>) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'registerSubscription',
      });
      dispatch({
        type: 'REGISTER_SUBSCRIPTION_REQUESTED',
        token: user.token,
        platform: Platform.OS,
        purchaseReceipts: receipts,
      });
    },
    onSubscribeAndroid: (user: User, paymentDetail: PaymentDetail) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'registerSubscription',
      });
      dispatch({
        type: 'REGISTER_SUBSCRIPTION_REQUESTED',
        token: user.token,
        platform: Platform.OS,
        subscriptionToken: paymentDetail.purchaseToken,
        subscriptionReceipt: paymentDetail.receiptData,
        subscriptionSignature: paymentDetail.receiptSignature,
      });
    },
    showPopup: (content: string) => {
      dispatch({
        type: 'SHOW_POPUP',
        content,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SubscriptionView);
