// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
} from 'react-native';
import {
  Button,
  Icon,
  HeaderCard,
  LocalizedText,
} from '../core-ui';
import styles from './SelectedProgramView-style';
import type {Program} from '../types/Program';
import type {User} from '../types/User';

type Props = {
  program: Program;
  user: User;
  onStartPress: () => void;
  onSubscribePress: () => void;
};


export default class SelectedProgramCard extends Component {
  props: Props
  constructor() {
    super(...arguments);
    autobind(this);
  }
  render() {
    let {program, onStartPress, user, onSubscribePress} = this.props;
    let {title, introContent, totalUsers, totalHabitDays} = program;
    let headerCardColor = 'default';

    let premiumInfo;
    let button = (
      <Button
        leftIcon="play-arrow"
        iconStyle={styles.headerButtonIcon}
        color="green"
        text="start"
        type="primary"
        onPress={onStartPress}
      />
    );

    if (program.isPremium === true) {
      if (user.isPremiumMember === true) {
        premiumInfo = (
          <View style={styles.programInfo}>
            <View style={styles.programInfoUpper}>
              <View style={styles.programInfoIconContainer}>
                <Icon name="star" style={[styles.programInfoIcon, styles.premiumInfoIcon]} />
              </View>
              <View style={styles.programInfoTextContainer}>
                <LocalizedText from="premiumInfo" style={styles.programInfoName} />
              </View>
            </View>
          </View>
        );
      } else {
        premiumInfo = (
          <View style={styles.programInfo}>
            <View style={styles.programInfoUpper}>
              <View style={styles.programInfoIconContainer}>
                <Icon name="lock" style={[styles.programInfoIcon, styles.premiumInfoIcon]} />
              </View>
              <View style={styles.programInfoTextContainer}>
                <LocalizedText from="premiumInfo" style={styles.programInfoName} />
              </View>
            </View>
          </View>
        );
        button = (
          <Button
            iconStyle={styles.headerButtonIcon}
            color="blue"
            text="subscribe"
            leftIcon="mail"
            type="primary"
            onPress={onSubscribePress}
          />
        );
      }
    }

    let programInfo = (
      <View style={styles.programInfoContainer}>
        {(introContent) ?
          <View style={[styles.programInfo, {marginBottom: 20}]}>
            <View style={styles.programInfoUpper}>
              <View style={styles.programInfoTextContainer}>
                <Text style={styles.programInfoName}>{introContent}</Text>
              </View>
            </View>
          </View> : null
        }
        <View style={styles.programInfo}>
          <View style={styles.programInfoUpper}>
            <View style={styles.programInfoIconContainer}>
              <Icon name="web" style={styles.programInfoIcon} />
            </View>
            <View style={styles.programInfoTextContainer}>
              <Text style={styles.programInfoName}>
                <LocalizedText from="consistOf" />
                {' ' + totalHabitDays + ' '}
                <LocalizedText from="sessions" />
              </Text>
            </View>
          </View>
        </View>
        <View style={styles.programInfo}>
          <View style={styles.programInfoUpper}>
            <View style={styles.programInfoIconContainer}>
              <Icon name="supervisor-account" style={styles.programInfoIcon} />
            </View>
            <View style={styles.programInfoTextContainer}>
              <Text style={styles.programInfoName}>
                {totalUsers + ' '}
                <LocalizedText from="users" />
                {' '}
                <LocalizedText from="joinThisProgram" />
              </Text>
            </View>
          </View>
        </View>
        {premiumInfo}
      </View>
    );

    return (
      <HeaderCard
        title={title}
        color={headerCardColor}
        style={styles.selectedProgramCard}
        contentStyle={{paddingTop: 8}}
      >
        {programInfo}
        <View style={styles.headerContentRowButton}>
          {button}
        </View>
      </HeaderCard>
    );
  }
}
