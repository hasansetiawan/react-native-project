// @flow
import {StyleSheet} from 'react-native';
import {
  FONT_APP_TITLE,
  FONT_LIGHT,
  FONT_BOLD,
} from '../constants/Font';
import {getColor} from '../constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    padding: 20,
    backgroundColor: 'white',
  },
  messageContainer: {
    alignItems: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  logoText: {
    fontFamily: FONT_APP_TITLE,
    fontSize: 36,
    color: getColor('orange'),
  },
  inputContainer: {
    marginVertical: 40,
  },
  field: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  iconBuffer: {
    width: 20,
    height: 20,
  },
  button: {
    marginBottom: 30,
  },
  loginContainer: {
    alignItems: 'center',
    paddingBottom: 30,
  },
  loginText: {
    fontFamily: FONT_LIGHT,
    fontSize: 14,
    color: '#666',
  },
  text: {
    fontFamily: FONT_BOLD,
    fontSize: 18,
    color: '#777',
  },
  logoContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
