// @flow
import {StyleSheet} from 'react-native';
import {getScreenSize} from '../helpers/getSize';
import {
  FONT_MEDIUM,
  FONT_LIGHT,
  FONT_BOLD,
} from '../constants/Font';
let {height} = getScreenSize();
export default StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  cardHeader: {
    alignItems: 'flex-start',
    backgroundColor: '#aaa',
    paddingVertical: 5,
    paddingLeft: 10,
  },
  cardContent: {
    paddingHorizontal: 10,
    paddingTop: 5,
    paddingBottom: 10,
  },
  cardRow: {
    flex: 5,
    flexDirection: 'row',
    paddingLeft: 10,
  },
  buttonRow: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowContent: {
    flex: 1,
    flexDirection: 'row',
  },
  textContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  text: {
    color: '#777',
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    fontSize: 12,
  },
  titleText: {
    color: '#777',
    flexWrap: 'wrap',
    fontFamily: FONT_BOLD,
    fontSize: 16,
  },
  cardColumn: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
  },
  cardLeftIcon: {
    color: '#777',
    paddingRight: 10,
  },
  cardRightIcon: {
    alignSelf: 'center',
    color: '#777',
    fontSize: 35,
    paddingLeft: 10,
  },
  footer: {
    paddingVertical: 10,
  },
  footerTitle: {
    fontFamily: FONT_MEDIUM,
  },
  footerRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingLeft: 5,
    paddingTop: 5,
    marginBottom: 8,
  },
  footerIcon: {
    color: '#777',
    fontSize: 48,
    paddingRight: 10,
    justifyContent: 'center',
    alignSelf: 'center',
  },
  profilePicture: {
    width: height * 0.08,
    height: height * 0.08,
    borderRadius: (height * 0.08) / 2,
    marginRight: 10,
  },
  buttonContainer: {
    flexDirection: 'row',
    justifyContent: 'flex-end',
    paddingVertical: 10,
  },
  button: {
    marginLeft: 10,
  },
  iconStyle: {
    fontSize: 20,
    color: 'white',
    marginRight: 10,
  },
  subscriptionCard: {
    flexDirection: 'row',
    backgroundColor: 'white',
    minHeight: 90,
    borderRadius: 5,
    borderColor: '#9E9E9E',
    borderWidth: 1,
    marginVertical: 10,
    padding: 10,
  },
  cardTitle: {
    flexDirection: 'column',
    alignItems: 'flex-end',
  },
});
