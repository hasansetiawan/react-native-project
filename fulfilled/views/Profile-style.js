import {StyleSheet} from 'react-native';
import {
  CONTAINER_COLOR,
  DEFAULT_ICON_COLOR,
  THEME_COLOR,
} from '../constants/color';
import {
  FONT_BOLD,
  FONT_LIGHT,
  FONT_REGULAR,
} from '../constants/Font';
import {getScreenSize} from '../helpers/getSize';

let {height} = getScreenSize();

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: CONTAINER_COLOR,
  },
  header: {
    backgroundColor: 'white',
    padding: 15,
    marginBottom: 10,
  },

  avatar: {
    alignItems: 'center',
  },
  avatarImageContainer: {
  },
  avatarImage: {
    width: height * 0.10,
    height: height * 0.10,
    borderRadius: (height * 0.10) / 2,
  },
  avatarImageIconContainer: {
    borderRadius: (height * 0.20) / 2,
    borderWidth: 1,
    borderColor: DEFAULT_ICON_COLOR,
    width: height * 0.10,
    height: height * 0.10,
    justifyContent: 'center',
  },
  avatarImageIcon: {
    fontSize: height * 0.06,
    alignSelf: 'center',
    color: DEFAULT_ICON_COLOR,
  },
  avatarNameContainer: {
    justifyContent: 'center',
    marginTop: 8,
    marginBottom: 15,
  },

  avatarName: {
    fontFamily: FONT_BOLD,
    fontSize: 15,
    color: DEFAULT_ICON_COLOR,
  },

  headerMenu: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'flex-end',
    marginVertical: 10,
  },
  headerMenuIcon: {
    fontSize: 20,
    color: THEME_COLOR,
    marginRight: 5,
  },
  headerMenuText: {
    flexWrap: 'wrap',
    fontFamily: FONT_REGULAR,
    fontSize: 12,
    color: '#666',
  },

  headerMenuItem: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },

  headerMenuTitle: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 3,
  },
  menuTitleText: {
    fontFamily: FONT_REGULAR,
    fontSize: 15,
    color: DEFAULT_ICON_COLOR,
  },
  checklistNumber: {
    fontSize: 30,
    fontFamily: FONT_LIGHT,
    marginBottom: 1,
    color: '#555',
  },
  menu: {
  },

  touchableMenuContainer: {
    height: 60,
  },

  menuItem: {
    flexDirection: 'row',
    backgroundColor: 'white',
    padding: 15,
    alignItems: 'center',
  },
  menuItemIcon: {
    fontSize: 20,
    color: 'orange',
    marginLeft: 15,
    marginRight: 20,
  },
  menuItemText: {
    flex: 1,
    fontFamily: FONT_LIGHT,
    color: DEFAULT_ICON_COLOR,
  },
  menuItemDetailIcon: {
    fontSize: 20,
    color: DEFAULT_ICON_COLOR,
  },
});
