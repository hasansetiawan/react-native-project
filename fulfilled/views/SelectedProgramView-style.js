// @flow
import {StyleSheet} from 'react-native';
import {
  FONT_LIGHT,
  FONT_MEDIUM,
  FONT_BOLD,
  FONT_REGULAR,
} from '../constants/Font';

import {getColor, THEME_COLOR} from '../constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F5F5F5',
    paddingHorizontal: 20,
    justifyContent: 'center',
  },
  selectedProgramViewContainer: {
    paddingHorizontal: 0,
  },
  selectedProgramCard: {
    marginTop: 10,
  },
  headerContentRow: {
    flexDirection: 'row',
    marginBottom: 25,
    justifyContent: 'center',
  },
  headerContentIcon: {
    fontSize: 20,
    color: '#888',
    paddingRight: 10,
  },
  headerContentText: {
    fontFamily: FONT_MEDIUM,
    fontSize: 14,
    color: '#666',
    flex: 1,
    flexWrap: 'wrap',
    textAlign: 'center',
  },
  programInfoContainer: {
    flexDirection: 'column',
    justifyContent: 'space-around',
    marginBottom: 15,
  },
  programInfo: {
    alignItems: 'center',
    marginBottom: 5,
  },
  programInfoUpper: {
    flexDirection: 'row',
    alignItems: 'flex-start',
  },
  programInfoIcon: {
    fontSize: 20,
    color: '#888',
    marginRight: 5,
  },
  programInfoCount: {
    fontFamily: FONT_LIGHT,
    fontSize: 16,
    color: '#888',
  },
  programInfoName: {
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#888',
  },
  progressText: {
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#888',
  },
  progressBar: {
    flexDirection: 'row',
    marginTop: 3,
    alignItems: 'center',
  },
  progressPercentageText: {
    fontFamily: FONT_LIGHT,
    fontSize: 12,
    color: '#666',
  },
  headerContentRowButton: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  habitNotDoneText: {
    flex: 1,
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    textAlign: 'right',
    fontSize: 12,
    color: '#888',
    paddingRight: 15,
  },
  habitDoneText: {
    flex: 1,
    flexWrap: 'wrap',
    fontFamily: FONT_LIGHT,
    textAlign: 'center',
    fontSize: 12,
    color: '#888',
  },
  headerButtonIcon: {
    color: '#FFF',
    fontSize: 25,
  },
  field: {
    borderBottomColor: 'grey',
    borderBottomWidth: 1,
    marginBottom: 10,
  },
  button: {
    marginTop: 10,
  },
  questionContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
  },
  icon: {
    color: getColor('theme'),
    fontSize: 30,
  },
  finishText: {
    textAlign: 'center',
    fontFamily: FONT_LIGHT,
  },
  titleContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 15,
  },
  titleContainerText: {
    textAlign: 'center',
  },
  titleTodayTips: {
    fontFamily: FONT_BOLD,
    color: THEME_COLOR,
    fontSize: 16,
    textAlign: 'center',
    marginRight: 5,
  },
  titleText: {
    fontFamily: FONT_REGULAR,
    color: '#666',
    fontSize: 14,
    textAlign: 'center',
    flexWrap: 'wrap',
  },
  premiumInfoIcon: {
    color: '#F9D600',
  },
  programInfoIconContainer: {
    flex: 1,
    justifyContent: 'flex-start',
  },
  programInfoTextContainer: {
    flex: 6,
    marginTop: 3,
    justifyContent: 'flex-start',
  },
  lessonViewContainer: {
    marginBottom: 20,
  },
});
