// @flow
import React from 'react';
import {ScrollView, Text, View} from 'react-native';
import {connect} from 'react-redux';

import {CardView, LoadingIndicator, LocalizedText} from '../core-ui';
import {FONT_LIGHT} from '../constants/Font';
import {getScreenSize} from '../helpers/getSize';

import type {Program} from '../types/Program';
import type {RootState} from '../types/RootState';

type Props = {
  programs: Map<string, Program>;
  isLoading: boolean;
};
export function ProgramHistoryView(props: Props) {
  let {programs, isLoading} = props;
  let content;
  let noProgramHistoryTextStyle = {
    alignSelf: 'center',
    flex: -1,
    flexWrap: 'wrap',
    textAlign: 'center',
    fontSize: 14,
    fontFamily: FONT_LIGHT,
    color: '#666',
  };
  if (programs.size === 0) {
    content = (
      <View style={{height: getScreenSize().height * 0.8, justifyContent: 'center'}}>
        <LocalizedText from="noProgramHistory" style={noProgramHistoryTextStyle} />
      </View>
    );
  } else {
    content = Array.from(programs.values()).map((program, index) => {
      return (
        <CardView key={index}>
          <Text>{program.title}</Text>
        </CardView>
      );
    });
  }
  if (isLoading === true) {
    return (
      <View style={{flex: 1}}>
        <LoadingIndicator />
      </View>
    );
  }
  return (
    <ScrollView>
      {content}
    </ScrollView>
  );
}

function mapStateToProps(state: RootState) {
  return {
    programs: state.userProgramHistory,
    isLoading: state.loadingPage.programHistoryPage,
  };
}

export default connect(mapStateToProps, null)(ProgramHistoryView);
