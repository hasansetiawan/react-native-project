// @flow
import {StyleSheet} from 'react-native';
// import {CONTAINER_COLOR} from '../constants/color';
import {
  FONT_LIGHT,
  FONT_BOLD,
} from '../constants/Font';

export default StyleSheet.create({
  headerContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  iconStyle: {
    fontSize: 80,
    color: '#F9D600',
  },
  headerTextContainer: {
    flexWrap: 'wrap',
  },
  defaultText: {
    textAlign: 'center',
    fontFamily: FONT_LIGHT,
  },
  expiryContent: {
    flexWrap: 'wrap',
    marginTop: 20,
  },
  expityDate: {
    fontFamily: FONT_BOLD,
  },
  cancelationInfoContainer: {
    marginTop: 20,
    flexWrap: 'wrap',
  },
});
