import React, {Component} from 'react';
import {
  View,
  ScrollView,
  Platform,
} from 'react-native';
import autobind from 'class-autobind';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import {connect} from 'react-redux';
import {
  CommunityCard,
  LoadingIndicator,
} from '../core-ui';

import WebView from './WebView';
import styles from './CommunityView-style';

import type {Post} from '../types/Post';
import type {Comment} from '../types/Comment';
import type {RootState} from '../types/RootState';
import type {User, ProfilePicture, CommunityMember} from '../types/User';
import type {Dispatch} from '../types/Dispatch';
import type {KeyboardState} from '../types/KeyboardState';

type Props = {
  post: Post;
  user: User;
  keyboard: KeyboardState;
  communityMembers: Map<string, CommunityMember>;
  comments: Map<string, Comment>;
  isLoading: boolean;
  profilePictureList: Map<string, ProfilePicture>;
  onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => void;
  onSubmitComment: (postID: string, text: string, user: User) => void;
  onImageClick: (uri: string) => void;
  onLinkPress: (uri: string) => void;
};

type State = {
  contentHeight: number;
};

export class NotificationDetailView extends Component {
  props: Props;
  state: State;
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {contentHeight: 0};
  }

  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (this.refs.scroll != null) {
      if (oldProps.keyboard.isKeyboardShown !== newProps.keyboard.isKeyboardShown) {
        this._scrollToXY(0, this.state.contentHeight);
      }
    }
  }

  render() {
    let {
      onImageClick,
      profilePictureList,
      post,
      comments,
      isLoading,
      user,
      communityMembers,
      onLikePress,
      onSubmitComment,
      onLinkPress,
    } = this.props;
    if (isLoading === true) {
      return (
        <View style={styles.mainContainer}>
          <LoadingIndicator />
        </View>
      );
    } else {
      let postComments = [];
      for (let id of post.comments) {
        postComments.push(comments.get(id));
      }

      let likers = [];
      for (let id of post.likers) {
        likers.push(communityMembers.get(id));
      }
      let ScrollComponent = ScrollView;
      if (Platform.OS === 'ios') {
        ScrollComponent = KeyboardAwareScrollView;
      }
      return (
        <View style={{flex: 1}}>
          <ScrollComponent
            ref="scroll"
            style={{marginBottom: 30}}
          >
            <CommunityCard
              user={user}
              post={post}
              onLayout={(ev) => {
                this.setState({contentHeight: ev.nativeEvent.layout.height});
              }}
              profilePictureList={profilePictureList}
              openCommentSection={true}
              communityMembers={communityMembers}
              comments={postComments}
              likers={likers}
              onLikePress={onLikePress}
              onSubmitComment={onSubmitComment}
              onImageClick={onImageClick}
              onLinkPress={onLinkPress}
            />
          </ScrollComponent>
        </View>
      );
    }
  }

  _scrollToXY(x: number, y: number) {
    this.refs.scroll.scrollTo({x, y, animated: true});
  }
}


function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    keyboard: state.keyboard,
    post: state.post.postList.get(state.notification.notificationDetailID),
    communityMembers: new Map(state.communityMembers),
    comments: new Map(state.comments),
    isLoading: state.loadingPage.notificationDetailPage,
    profilePictureList: state.profilePictureList,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onImageClick: (uri: string) => {
      dispatch({
        type: 'IMAGE_URI_RECEIVED',
        uri,
      });
    },
    onLikePress: (postID: string, userToken: string, userID: string, isLiked: boolean) => {
      if (isLiked === false) {
        dispatch({
          type: 'LIKE_ADDED',
          token: userToken,
          postID,
          userID,
        });
      }
    },
    onSubmitComment: (postID: string, text: string, user: User) => {
      let id = '_' + Math.random().toString();
      dispatch({
        type: 'COMMENT_ADDED',
        postID,
        comment: {
          id,
          text,
          user: user.id,
        },
      });
      dispatch({
        type: 'SEND_COMMENT_REQUESTED',
        postID,
        token: user.token,
        text,
      });
    },
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationDetailView);
