// @flow

import React from 'react';
import {View} from 'react-native';
import {LocalizedTextInput, LocalizedText} from '../../core-ui';
import styles from './NewPostView-style';
import textInputStyle from '../../core-ui/TextField-style';
import type {ActivityFormData} from '../../types/Post';

type Props = {
  formData: ActivityFormData;
  onChange: (newFormData: ActivityFormData) => void;
};

export default function ActivityForm(props: Props) {
  let {
    formData,
    onChange,
  } = props;

  let onChangeHandler = (key: string, value: string) => {
    let newFormData = {
      ...formData,
      [key]: value,
    };
    onChange(newFormData);
  };

  return (
    <View>
      <View style={styles.postDetail}>
        <View style={[textInputStyle.dark, textInputStyle.compact]}>
          <LocalizedTextInput
            multiline={true}
            numberOfLines={4}
            style={[textInputStyle.default, {fontSize: 13, height: 60}]}
            placeholder="placeholder/activityLogFormContent"
            value={formData.content}
            onChangeText={(text) => onChangeHandler('content', text)}
          />
        </View>
      </View>
      <View style={styles.postDetail}>
        <View style={styles.duration}>
          <View style={[textInputStyle.dark, {paddingVertical: 3, paddingHorizontal: 5}]}>
            <LocalizedTextInput
              isBlockTextInput={false}
              placeholder="0"
              style={[textInputStyle.default, {paddingLeft: 5, height: 20, width: 25, fontSize: 13}]}
              selectTextOnFocus={true}
              value={formData.hour}
              keyboardType="numeric"
              onChangeText={(text) => onChangeHandler('hour', text)}
            />
          </View>
          <LocalizedText from="hour" style={styles.durationLabel} />
          <View style={[textInputStyle.dark, {paddingVertical: 3, paddingHorizontal: 5}]}>
            <LocalizedTextInput
              isBlockTextInput={false}
              placeholder="0"
              style={[textInputStyle.default, {paddingLeft: 5, height: 20, width: 25, fontSize: 13}]}
              selectTextOnFocus={true}
              value={formData.minute}
              keyboardType="numeric"
              onChangeText={(text) => onChangeHandler('minute', text)}
            />
          </View>
          <LocalizedText from="minute" style={styles.durationLabel} />
        </View>
      </View>
      <View style={styles.postDetail}>
        <View style={[textInputStyle.dark, textInputStyle.compact]}>
          <LocalizedTextInput
            multiline={true}
            style={[textInputStyle.default, {fontSize: 13, height: 60}]}
            placeholder="placeholder/activityLogFormComment"
            value={formData.comment}
            onChangeText={(text) => onChangeHandler('comment', text)}
          />
        </View>
      </View>
    </View>
  );
}
