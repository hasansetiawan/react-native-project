let {describe, it} = global;

import React from 'react';
import expect from 'expect';
import {shallow} from 'enzyme';
import DrinkForm from '../DrinkForm';

describe('Drink Form', () => {
  let formData = {
    content: '',
    selectedAmount: '',
  };
  let onChange = expect.createSpy();
  let user = {
    language: 'id',
  };
  let wrapper = shallow(<DrinkForm user={user} formData={formData} onChange={onChange} />);
  it('should correctly render drink form', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find('LocalizedTextInput').length).toBe(1);
  });

  it('should correctly change the content of formData according to its key', () => {
    let textInputs = wrapper.find('LocalizedTextInput');
    textInputs.at(0).simulate('changeText', 'data');
    expect(onChange.calls.length).toBe(1);
    expect(onChange.calls[0].arguments[0].content).toBe('data');

    let options = wrapper.find('Option');
    options.at(0).simulate('select');
    expect(onChange.calls.length).toBe(2);
    expect(onChange.calls[1].arguments[0].selectedAmount).toBe('330 ml');


  });
});
