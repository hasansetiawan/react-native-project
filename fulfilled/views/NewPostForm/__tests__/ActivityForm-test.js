let {describe, it} = global;

import React from 'react';
import expect from 'expect';
import {shallow} from 'enzyme';
import ActivityForm from '../ActivityForm';

describe('Activity Log Form', () => {
  let formData = {
    content: '',
    hour: '',
    minute: '',
    comment: '',
  };
  let onChange = expect.createSpy();
  let wrapper = shallow(<ActivityForm formData={formData} onChange={onChange} />);
  it('should correctly render activity log form', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find('LocalizedTextInput').length).toBe(4);
  });

  it('should correctly change the content of formData according to its key', () => {
    let textInputs = wrapper.find('LocalizedTextInput');
    textInputs.at(0).simulate('changeText', 'data');
    expect(onChange.calls.length).toBe(1);
    expect(onChange.calls[0].arguments[0].content).toBe('data');
  });
});
