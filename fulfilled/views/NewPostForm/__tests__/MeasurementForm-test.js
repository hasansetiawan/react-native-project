let {describe, it} = global;

import React from 'react';
import expect from 'expect';
import {shallow} from 'enzyme';
import MeasurementForm from '../MeasurementForm';

describe('Measurement Form', () => {
  let formData = {
    comment: '',
    weight: '',
    hip: '',
    waistline: '',
  };
  let onChange = expect.createSpy();
  let wrapper = shallow(<MeasurementForm formData={formData} onChange={onChange} />);
  it('should correctly render Measurement form', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find('LocalizedTextInput').length).toBe(4);
  });

  it('should correctly change the content of formData according to its key', () => {
    let textInputs = wrapper.find('LocalizedTextInput');
    textInputs.at(0).simulate('changeText', 'data');
    expect(onChange.calls.length).toBe(1);
    expect(onChange.calls[0].arguments[0].weight).toBe('data');
  });
});
