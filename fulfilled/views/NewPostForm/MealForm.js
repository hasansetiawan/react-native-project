// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View} from 'react-native';
import {
  LocalizedTextInput,
  Button,
} from '../../core-ui';
import ActionSheet, {Option} from '../../core-ui/LocalizedActionSheet';

import getLanguages from '../../helpers/getLanguages';

import styles from './NewPostView-style';
import textInputStyle from '../../core-ui/TextField-style';

import type {MealFormData} from '../../types/Post';
import type {User} from '../../types/User';

type Props = {
  user: User;
  formData: MealFormData;
  onChange: (newFormData: MealFormData) => void;
};

const languages = getLanguages();

export default class MealForm extends Component {
  props: Props;
  state: {isTimeSheetOpen: boolean};
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isTimeSheetOpen: false,
    };
  }
  render() {
    let {formData, onChange, user} = this.props;
    let onChangeHandler = (key: string, value: string) => {
      let newFormData = {
        ...formData,
        [key]: value,
      };
      onChange(newFormData);
    };
    return (
      <View>
        <View style={styles.postDetail}>
          <View style={[textInputStyle.dark, textInputStyle.compact]}>
            <LocalizedTextInput
              multiline={true}
              style={[textInputStyle.default, {fontSize: 13, height: 80}]}
              placeholder="placeholder/mealFormContent"
              value={formData.content}
              onChangeText={(text) => onChangeHandler('content', text)}
            />
          </View>
        </View>
        <View style={styles.postDetail}>
          <Button
            type="primary"
            isBlockButton={true}
            text={formData.selectedTime ? formData.selectedTime : 'selectedTime'}
            onPress={this._showTimeSheet}
            iconStyle={styles.dropdownIcon}
            rightIcon="expand-more"
            upperCase={false}
          />
        </View>
        <ActionSheet isOpen={this.state.isTimeSheetOpen} onClose={this._showTimeSheet}>
          <Option from="Anytime" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Anytime'])} />
          <Option from="Breakfast" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Breakfast'])} />
          <Option from="Morning Snack" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Morning Snack'])} />
          <Option from="Lunch" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Lunch'])} />
          <Option from="Afternoon Snack" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Afternoon Snack'])} />
          <Option from="Dinner" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['Dinner'])} />
          <Option from="After Dinner" type="NORMAL" onSelect={() => onChangeHandler('selectedTime', languages[user.language]['After Dinner'])} />
          <Option from="cancel" type="CANCEL" />
        </ActionSheet>
      </View>
    );
  }
  _showTimeSheet() {
    let {isTimeSheetOpen} = this.state;
    this.setState({isTimeSheetOpen: !isTimeSheetOpen});
  }
}
