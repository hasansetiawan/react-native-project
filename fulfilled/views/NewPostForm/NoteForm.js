// @flow

import React from 'react';
import {View} from 'react-native';
import {
  LocalizedTextInput,
} from '../../core-ui';
import styles from './NewPostView-style';
import textInputStyle from '../../core-ui/TextField-style';
import type {NoteFormData} from '../../types/Post';

type Props = {
  formData: NoteFormData;
  onChange: (newFormData: NoteFormData) => void;
};

export default function NoteForm(props: Props) {
  let {
    formData,
    onChange,
  } = props;

  let onChangeHandler = (key: string, value: string) => {
    let newFormData = {
      ...formData,
      [key]: value,
    };
    onChange(newFormData);
  };

  return (
    <View>
      <View style={styles.postDetail}>
        <View style={[textInputStyle.dark, textInputStyle.compact]}>
          <LocalizedTextInput
            multiline={true}
            style={[textInputStyle.default, {fontSize: 13, height: 60}]}
            placeholder="placeholder/noteFormContent"
            value={formData.content}
            onChangeText={(text: string) => onChangeHandler('content', text)}
          />
        </View>
      </View>
    </View>
  );
}
