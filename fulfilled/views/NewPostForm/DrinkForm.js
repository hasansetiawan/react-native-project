// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View} from 'react-native';
import {
  LocalizedTextInput,
  Button,
} from '../../core-ui';
import ActionSheet, {Option} from '../../core-ui/LocalizedActionSheet';

import getLanguages from '../../helpers/getLanguages';

import styles from './NewPostView-style';
import textInputStyle from '../../core-ui/TextField-style';

import type {DrinkFormData} from '../../types/Post';
import type {User} from '../../types/User';

type Props = {
  user: User;
  formData: DrinkFormData;
  onChange: (newFormData: DrinkFormData) => void;
};

const languages = getLanguages();

export default class DrinkForm extends Component {
  props: Props;
  state: {isAmountSheetOpen: boolean};
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isAmountSheetOpen: false,
    };
  }
  render() {
    let {formData, onChange, user} = this.props;
    let onChangeHandler = (key: string, value: string) => {
      let newFormData = {
        ...formData,
        [key]: value,
      };
      onChange(newFormData);
    };
    return (
      <View>
        <View style={styles.postDetail}>
          <View style={[textInputStyle.dark, textInputStyle.compact]}>
            <LocalizedTextInput
              multiline={true}
              style={[textInputStyle.default, {fontSize: 13, height: 80}]}
              placeholder="placeholder/drinkFormContent"
              value={formData.content}
              onChangeText={(text) => onChangeHandler('content', text)}
            />
          </View>
        </View>
        <View style={styles.postDetail}>
          <Button
            type="primary"
            isBlockButton={true}
            text={(formData.selectedAmount) ? (formData.selectedAmount) : 'selectedAmount'}
            onPress={this._showAmountSheet}
            iconStyle={styles.dropdownIcon}
            rightIcon="expand-more"
            upperCase={false}
          />
        </View>
        <ActionSheet isOpen={this.state.isAmountSheetOpen} onClose={this._showAmountSheet}>
          <Option from="drinkAmount/330 ml" onSelect={() => onChangeHandler('selectedAmount', languages[user.language]['drinkAmount']['330 ml'])} />
          <Option from="drinkAmount/500 ml" onSelect={() => onChangeHandler('selectedAmount', languages[user.language]['drinkAmount']['500 ml'])} />
          <Option from="drinkAmount/1 litre" onSelect={() => onChangeHandler('selectedAmount', languages[user.language]['drinkAmount']['1 litre'])} />
          <Option from="drinkAmount/1.5 litres" onSelect={() => onChangeHandler('selectedAmount', languages[user.language]['drinkAmount']['1.5 litres'])} />
          <Option from="drinkAmount/2 litres" onSelect={() => onChangeHandler('selectedAmount', languages[user.language]['drinkAmount']['2 litres'])} />
          <Option from="cancel" type="CANCEL" />
        </ActionSheet>
      </View>
    );
  }
  _showAmountSheet() {
    let {isAmountSheetOpen} = this.state;
    this.setState({isAmountSheetOpen: !isAmountSheetOpen});
  }
}
