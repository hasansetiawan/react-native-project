// @flow

import React from 'react';
import {View, Text} from 'react-native';
import {LocalizedTextInput, LocalizedText} from '../../core-ui';
import styles from './NewPostView-style';
import textInputStyle from '../../core-ui/TextField-style';
import {getScreenSize} from '../../helpers/getSize';
import type {MeasurementFormData} from '../../types/Post';

type Props = {
  formData: MeasurementFormData;
  onChange: (newFormData: MeasurementFormData) => void;
};

export default function ActivityForm(props: Props) {
  let {
    formData,
    onChange,
  } = props;

  let onChangeHandler = (key: string, value: string) => {
    let newFormData = {
      ...formData,
      [key]: value,
    };
    onChange(newFormData);
  };
  let measurements = getScreenSize().width * 0.35;
  return (
    <View>
      <View style={[styles.postDetail, {marginBottom: 15}]}>
        <View style={styles.textInputWithLabelColumn}>
          <View style={[textInputStyle.dark, {paddingVertical: 3, paddingHorizontal: 5}]}>
            <LocalizedTextInput
              keyboardType="numeric"
              placeholder="weight"
              isBlockTextInput={false}
              style={[textInputStyle.default, {paddingLeft: 5, height: 25, width: measurements, fontSize: 13}]}
              value={formData.weight}
              onChangeText={(text: string) => onChangeHandler('weight', text)}
            />
          </View>
          <Text style={styles.durationLabel}>kg</Text>
        </View>
        <View style={styles.textInputWithLabelColumn}>
          <View style={[textInputStyle.dark, {paddingVertical: 3, paddingHorizontal: 5}]}>
            <LocalizedTextInput
              placeholder="waistline"
              keyboardType="numeric"
              isBlockTextInput={false}
              style={[textInputStyle.default, {paddingLeft: 5, height: 25, width: measurements, fontSize: 13}]}
              value={formData.waistline}
              onChangeText={(text: string) => onChangeHandler('waistline', text)}
            />
          </View>
          <LocalizedText style={styles.durationLabel} from="inches" />
        </View>
        <View style={[styles.textInputWithLabelColumn, {marginBottom: 5}]}>
          <View style={[textInputStyle.dark, {paddingVertical: 3, paddingHorizontal: 5}]}>
            <LocalizedTextInput
              placeholder="hip"
              keyboardType="numeric"
              isBlockTextInput={false}
              style={[textInputStyle.default, {paddingLeft: 5, height: 25, width: measurements, fontSize: 13}]}
              value={formData.hip}
              onChangeText={(text: string) => onChangeHandler('hip', text)}
            />
          </View>
          <LocalizedText style={styles.durationLabel} from="inches" />
        </View>
      </View>
      <View style={styles.postDetail}>
        <View style={[textInputStyle.dark, textInputStyle.compact]}>
          <LocalizedTextInput
            multiline={true}
            style={[textInputStyle.default, {fontSize: 13, height: 60}]}
            placeholder="placeholder/measurementFormComment"
            value={formData.comment}
            onChangeText={(text: string) => onChangeHandler('comment', text)}
          />
        </View>
      </View>
    </View>
  );
}
