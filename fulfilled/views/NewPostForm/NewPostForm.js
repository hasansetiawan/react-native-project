//@flow

import React from 'react';

import ActivityForm from './ActivityForm';
import MealForm from './MealForm';
import DrinkForm from './DrinkForm';
import MeasurementForm from './MeasurementForm';
import NoteForm from './NoteForm';

import type {NewPostFormData} from '../../types/Post';
import type {User} from '../../types/User';

type Props = {
  user: User;
  formData: NewPostFormData;
  onChange: (newFormData: NewPostFormData) => void;
  showTimeSheet?: () => void;
  selectedTime?: string;
  showAmountSheet?: () => void;
  selectedAmount?: string;
};

export default function NewPostForm(props: Props) {
  let {formData, onChange, user} = props;
  switch (formData.category) {
    case 'ActivityLog': {
      return <ActivityForm user={user} formData={formData} onChange={onChange} />;
    }
    case 'Meal': {
      return <MealForm user={user} formData={formData} onChange={onChange} />;
    }
    case 'Drink': {
      return <DrinkForm user={user} formData={formData} onChange={onChange} />;
    }
    case 'Measurement': {
      return <MeasurementForm user={user} formData={formData} onChange={onChange} />;
    }
    case 'Note': {
      return <NoteForm user={user} formData={formData} onChange={onChange} />;
    }
    default: return null;
  }
}
