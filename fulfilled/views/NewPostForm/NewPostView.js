// @flow
import React, {Component} from 'react';
import {
  View,
  Image,
  ScrollView,
} from 'react-native';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import {
  CardView,
  UploadImageModal,
  Icon,
  Button,
} from '../../core-ui';
import styles from './NewPostView-style';
import NewPostForm from './NewPostForm';
import {
  validateActivityPost,
  validateMealPost,
  validateDrinkPost,
  validateMeasurementPost,
  validateNotePost,
} from '../../helpers/validate';

import type {NewPostFormData} from '../../types/Post';
import type {Privacy} from '../../types/Privacy';
import type {Coach} from '../../types/Coach';
import type {User} from '../../types/User';
import type {ImageData} from '../../types/ImageData';


type Props = {
  user: User;
  coach: Coach;
  formData: NewPostFormData;
  onSubmit: (privacy: Privacy, user: User, formData: NewPostFormData) => void;
  onChange: (newFormData: NewPostFormData) => void;
  onValidateFail: (errorMessage: string) => void;
};

type State = {
  selectedVisibility: Privacy;
  showUploadModal: boolean;
  showPrivacyOption: boolean;
  imageData: ?ImageData;
};

export class NewPostView extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedVisibility: 'community',
      showUploadModal: false,
      showPrivacyOption: false,
      imageData: null,
    };
  }

  render() {
    let {formData, onChange, user} = this.props;
    let {selectedVisibility, imageData} = this.state;
    return (
      <ScrollView>
        <View style={styles.container}>
          <CardView>
            <View style={styles.buttonContainer}>
              <View style={styles.visibilityLockIconContainer}>
                <Icon name="lock" style={styles.visibilityLockIcon} />
              </View>
              <Button
                type="primary"
                text="community"
                style={[styles.buttonVisibility, {flex: 5}]}
                color="transparent"
                textStyle={[styles.buttonText, styles.grayText]}
                onPress={() => this._setVisibility('community')}
                leftIcon={selectedVisibility === 'community' ? 'radio-button-checked' : 'radio-button-unchecked'}
                iconStyle={[styles.visibilityIcon, styles.grayText]}
              />
              <View style={styles.buttonSpacer} />
              <Button
                type="primary"
                text="guide"
                style={[styles.buttonVisibility, {flex: 5}]}
                color="transparent"
                textStyle={[styles.buttonText, styles.grayText]}
                onPress={() => this._setVisibility('guide')}
                leftIcon={selectedVisibility === 'guide' ? 'radio-button-checked' : 'radio-button-unchecked'}
                iconStyle={[styles.visibilityIcon, styles.grayText]}
              />
            </View>
            <NewPostForm
              user={user}
              formData={formData}
              onChange={onChange}
            />
            <View style={styles.postDetail}>
              <View style={{flexDirection: 'row'}}>
                <Button
                  type="primary"
                  isBlockButton={true}
                  text=""
                  style={[styles.iconContainer, styles.block]}
                  color="grey"
                  leftIcon="add-a-photo"
                  iconStyle={[styles.icon, {alignSelf: 'center'}]}
                  onPress={this._onModalRequestOpen}
                  upperCase={false}
                />
              </View>
              {
                (imageData != null) ?
                  <View style={styles.imageInputContainer}>
                    <Image
                      style={styles.imageInput}
                      source={imageData}
                    />
                  </View> :
                  null
              }
            </View>
            <UploadImageModal
              visible={this.state.showUploadModal}
              onRequestClose={this._onModalRequestClose}
              animationType="fade"
              onImagePicked={this._onImagePicked}
            />
            <Button
              type="primary"
              isBlockButton={true}
              text="createPost"
              style={styles.iconContainer}
              color="blue"
              onPress={this._onSubmit}
              upperCase={false}
            />
          </CardView>
        </View>
      </ScrollView>
    );
  }
  _onModalRequestClose() {
    this.setState({showUploadModal: false});
  }
  _onModalRequestOpen() {
    this.setState({showUploadModal: true});
  }
  _setVisibility(value: Privacy): void {
    this.setState({
      selectedVisibility: value,
    });
  }
  _onImagePicked(imageData: ?ImageData) {
    this.setState({imageData});
  }

  _onSubmit() {
    let {selectedVisibility, imageData} = this.state;
    let {onSubmit, user, formData, onValidateFail} = this.props;
    let error;
    switch (formData.category) {
      case 'Note': {
        error = validateNotePost(formData.content);
        break;
      }
      case 'ActivityLog': {
        error = validateActivityPost(formData.content, formData.hour, formData.minute);
        break;
      }
      case 'Meal': {
        error = validateMealPost(formData.content, formData.selectedTime);
        break;
      }
      case 'Measurement': {
        error = validateMeasurementPost(formData.weight, formData.hip, formData.waistline);
        break;
      }
      case 'Drink': {
        error = validateDrinkPost(formData.content, formData.selectedAmount);
        break;
      }
    }
    if (error == null) {
      onSubmit(selectedVisibility, user, formData, imageData);
    } else {
      onValidateFail(error);
    }
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
    coach: state.coach,
    formData: state.post.formData,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onSubmit: (privacy: Privacy, user: User, formData: NewPostFormData, imageData: ?ImageData) => {
      let id = '_' + Math.random().toString();
      let date = Date.now().toString();
      dispatch({
        type: 'ROUTE_POPPED',
      });
      dispatch({
        type: 'POST_CREATED',
        createdAt: date,
        id,
        privacy,
        user,
        imageData,
      });
      switch (formData.category) {
        case 'Note': {
          dispatch({
            type: 'SEND_NOTE_POST_REQUESTED',
            id,
            text: formData.content,
            token: user.token,
            privacy,
            imageData,
          });
          break;
        }
        case 'ActivityLog': {
          dispatch({
            type: 'SEND_ACTIVITY_POST_REQUESTED',
            id,
            token: user.token,
            privacy,
            activityDescription: formData.content,
            activityDurationHours: formData.hour,
            activityDurationMinutes: formData.minute,
            activityComment: formData.comment,
            imageData,
          });
          break;
        }
        case 'Meal': {
          dispatch({
            type: 'SEND_MEAL_POST_REQUESTED',
            id,
            token: user.token,
            privacy,
            mealDescription: formData.content,
            mealPeriod: formData.selectedTime,
            imageData,
          });
          break;
        }
        case 'Measurement': {
          dispatch({
            type: 'SEND_MEASUREMENT_POST_REQUESTED',
            id,
            token: user.token,
            privacy,
            text: formData.comment,
            waist: formData.waistline,
            hip: formData.hip,
            weight: formData.weight,
            imageData,
          });
          break;
        }
        case 'Drink': {
          dispatch({
            type: 'SEND_DRINK_POST_REQUESTED',
            id,
            token: user.token,
            privacy,
            drinkDescription: formData.content,
            drinkSize: formData.selectedAmount,
            imageData,
          });
          break;
        }
      }
    },
    onChange: (newFormData: NewPostFormData) => {
      dispatch({
        type: 'FORMDATA_CHANGED',
        newFormData,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPostView);
