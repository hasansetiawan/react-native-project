// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  Text,
  View,
  TouchableOpacity,
  ListView,
  RecyclerViewBackedScrollView,
  ActivityIndicator,
  RefreshControl,
} from 'react-native';
import {connect} from 'react-redux';
import {
  Icon,
  CardView,
  LocalizedText,
  LocalizedDate,
  LoadingIndicator,
} from '../core-ui';

import {getLatestOrderedPosts} from '../selectors/getLatestOrderedPosts';

import NotificationDetailView from './NotificationDetailView';

import styles from './Notification-style';

import {
  CHAT_PAGE,
  CHAT_NAVBAR_INDEX,
} from '../constants/NavBarPageName';


import type {Notification, NotificationType} from '../types/Notification';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Dispatch';
import type {User} from '../types/User';
import type {Coach} from '../types/Coach';

type Props = {
  user: User;
  coach: Coach;
  notifications: Array<Notification>;
  isLoading: boolean;
  page: number;
  isLoadingMore: boolean;
  isRefreshingList: boolean;
  onNotificationClicked: (token: string, type: string, postID: string, coach: Coach) => void;
  onFetchMoreNotifications: (token: string, page: number) => void;
  onRefreshNotification: (token: string) => void;
};

class NotificationView extends Component {
  props: Props
  state: {dataSource: Object}
  constructor() {
    super(...arguments);
    autobind(this);
    let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
    let dataSource = ds.cloneWithRows(this.props.notifications);
    this.state = {dataSource};
  }
  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    if (newProps.notifications !== oldProps.notifications) {
      let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
      let dataSource = ds.cloneWithRows(newProps.notifications);
      this.setState({dataSource});
    }
  }
  render() {
    let {coach, isLoading, isLoadingMore, notifications, onNotificationClicked, user} = this.props;
    if (isLoading === true) {
      return (
        <View style={{flex: 1}}>
          <LoadingIndicator />
        </View>
      );
    }
    if (notifications.length > 0) {
      return (
        <ListView
          style={styles.mainContainer}
          dataSource={this.state.dataSource}
          initialListSize={3}
          scrollRenderAheadDistance={50}
          renderRow={(notif) => {
            return (
              <CardView style={styles.card}>
                <NotificationContent type={notif.type} content={notif.text} timestamp={notif.createdAt} onNotificationClicked={() => onNotificationClicked(user.token, notif.type, notif.postID, coach)} />
              </CardView>
            );
          }}
          refreshControl={
            <RefreshControl
              refreshing={this.props.isRefreshingList}
              onRefresh={this._onRefresh}
            />
          }
          renderScrollComponent={(props) => <RecyclerViewBackedScrollView {...props} />}
          renderFooter={() => {
            if (isLoadingMore) {
              return (
                <ActivityIndicator size="large" animating={true} />
              );
            } else {
              return null;
            }
          }}
          onEndReached={this._fetchMoreNotif}
        />
      );
    } else {
      return (
        <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
          <LocalizedText from="noNotification" style={styles.noNotificationText} />
        </View>
      );
    }
  }

  _fetchMoreNotif() {
    let {onFetchMoreNotifications, user, page} = this.props;
    onFetchMoreNotifications(user.token, page + 1);
  }

  _onRefresh() {
    let {onRefreshNotification, user} = this.props;
    onRefreshNotification(user.token);
  }
}

function NotificationAvatar(props: {type: NotificationType}) {
  let {type} = props;

  switch (type) {
    case 'coach_post': {
      return (
        <View style={styles.avatarImage}>
          <Icon name="account-circle" style={styles.avatarImageIcon} />
        </View>
      );
    }
    case 'like_post': {
      return (
        <View style={styles.avatarImage}>
          <Icon name="thumb-up" style={styles.avatarImageIcon} />
        </View>
      );
    }
    case 'comment_post': {
      return (
        <View style={styles.avatarImage}>
          <Icon name="comment" style={styles.avatarImageIcon} />
        </View>
      );
    }
    default: {
      return (
        <View style={styles.avatarImage}>
          <Icon name="account-circle" style={styles.avatarImageIcon} />
        </View>
      );
    }
  }
}

type NotificationContentProps = {
  type: NotificationType;
  timestamp: string;
  content: string;
  onNotificationClicked: () => void;
};

export function NotificationContent(props: NotificationContentProps) {
  let {type, timestamp, content, onNotificationClicked} = props;
  return (
    <TouchableOpacity onPress={onNotificationClicked} style={styles.contentContainer}>
      <View style={styles.notificationContent}>
        <View style={styles.header}>
          <NotificationAvatar type={type} />
          <LocalizedText from={`notifType/${type}`} style={styles.notifType} />
        </View>
        <View style={styles.content}>
          <Text style={styles.contentText}>{content}</Text>
        </View>
        <View style={styles.createdTime}>
          <Icon name="access-time" style={styles.iconStyle} />
          <LocalizedDate style={styles.iconText} date={timestamp} format="DATE_TIME_SHORT" />
        </View>
      </View>
    </TouchableOpacity>
  );
}

function mapStateToProps(state: RootState) {
  return {
    user: state.user,
    coach: state.coach,
    notifications: getLatestOrderedPosts(state.notification.notificationList),
    isLoading: state.loadingPage.notificationPage,
    isLoadingMore: state.notification.isLoadingMore,
    page: state.notification.page,
    isRefreshingList: state.loadingPage.notificationRefresh,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onRefreshNotification: (token: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'notificationRefresh',
      });
      dispatch({
        type: 'FETCH_NOTIF_LIST_REQUESTED',
        token,
        page: 1,
        isRefreshingList: true,
      });
    },
    onFetchMoreNotifications: (token: string, page: number) => {
      dispatch({
        type: 'FETCH_MORE_NOTIFS_STARTED',
      });
      dispatch({
        type: 'FETCH_NOTIF_LIST_REQUESTED',
        token,
        page,
      });
    },
    onNotificationClicked: (token: string, type: string, postID: string, coach: Coach) => {
      if (type === 'chat_post') {
        dispatch({
          type: 'LOADING_STARTED',
          loadingPages: 'chatPage',
        });
        dispatch({
          type: 'FETCH_CHAT_LIST_REQUESTED',
          token: token,
          page: 1,
          recipient: coach.id,
          isRefreshingList: true,
        });
        dispatch({
          type: 'NAVBAR_CHANGED',
          route: {key: CHAT_PAGE},
          index: CHAT_NAVBAR_INDEX,
        });
      } else {
        dispatch({
          type: 'FETCH_NOTIFICATION_DETAIL_REQUESTED',
          token,
          postID,
        });
        dispatch({
          type: 'LOADING_STARTED',
          loadingPages: 'notificationDetailPage',
        });
        dispatch({
          type: 'ROUTE_PUSHED',
          route: {
            key: 'notificationDetail',
            title: '',
            Component: NotificationDetailView,
          },
        });
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationView);
