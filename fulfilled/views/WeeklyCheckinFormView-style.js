// @flow

import {StyleSheet} from 'react-native';
import {coachName, coachTitle, coachDetail, card} from '../core-ui/shared/sharedStyles';
import {
  FONT_REGULAR,
  FONT_BOLD,
  FONT_LIGHT,
  FONT_MEDIUM,
} from '../constants/Font';
import {
  CONTAINER_COLOR,
} from '../constants/color';

import {getScreenSize} from '../helpers/getSize';

const {height} = getScreenSize();


let borderInput = {
  borderWidth: 1,
  borderColor: '#ebe8e9',
};
export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: CONTAINER_COLOR,
    marginBottom: 20,
  },
  user: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
  },
  userPic: {
    color: '#444',
    fontSize: 40,
    marginRight: 10,
  },
  userName: {
    fontFamily: FONT_BOLD,
    fontSize: 16,
    color: '#666',
  },
  postDetail: {
    marginBottom: 20,
  },
  postDetailName: {
    paddingLeft: 2,
    fontFamily: FONT_BOLD,
    fontSize: 14,
    color: '#777',
    marginBottom: 2,
  },
  multiline: {
    ...borderInput,
    borderRadius: 4,
    height: 60,
    fontFamily: FONT_LIGHT,
    color: '#555',
    fontSize: 14,
    paddingHorizontal: 10,
    paddingVertical: 5,
  },
  coachName: {
    ...coachName,
    fontWeight: 'bold',
  },
  coachTitle: coachTitle,
  coachDetail: coachDetail,
  card: card,
  duration: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  textInputWithLabelRow: {
    flexDirection: 'row',
  },
  textInputWithLabelColumn: {
    flexDirection: 'row',
    marginBottom: 10,
  },
  durationLabel: {
    alignSelf: 'center',
    marginLeft: 10,
    marginRight: 15,
    fontFamily: FONT_LIGHT,
    fontSize: 13,
    color: '#666',
  },
  spacer: {
    flex: 1,
  },
  singleLine: {
    ...borderInput,
    height: 30,
    width: 30,
    padding: 5,
    borderRadius: 4,
    fontFamily: FONT_REGULAR,
    fontSize: 14,
    color: '#666',
  },
  singleLineSize: {
    ...borderInput,
    height: 30,
    width: 80,
    padding: 5,
    borderRadius: 4,
    fontFamily: FONT_REGULAR,
    fontSize: 14,
    color: '#666',
  },
  block: {
    flex: 1,
  },
  iconContainer: {
    backgroundColor: '#ebe8e9',
    padding: 10,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 4,
  },
  icon: {
    color: 'white',
    fontSize: 20,
  },
  visibilityLockIcon: {
    color: '#989898',
    fontSize: 20,
  },
  visibilityLockIconContainer: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    flexDirection: 'row',
    marginBottom: 8,
  },
  buttonSpacer: {
    width: 10,
  },
  buttonVisibility: {
    flex: 1,
  },
  buttonText: {
    fontSize: 11,
  },
  saveButtonContainer: {
    backgroundColor: '#1191d9',
    flex: 1,
    width: 40,
    flexDirection: 'row',
    alignItems: 'center',
  },
  saveButton: {
    marginLeft: 25,
    color: '#fff',
  },
  settingButton: {
    backgroundColor: '#97999c',
    height: 40,
    flex: 2,
    flexDirection: 'row',
    alignItems: 'center',
  },
  visibilityIcon: {
    fontSize: 16,
    color: '#fff',
  },
  grayText: {
    color: '#898989',
  },
  dropdownIcon: {
    fontSize: 22,
    color: '#fff',
    alignSelf: 'center',
  },
  options: {
    color: '#fff',
  },
  optionsContainer: {
    marginTop: 12,
    backgroundColor: '#800053',
    flex: 1,
    height: 40,
    alignItems: 'center',
  },
  optionsValue: {
    marginTop: 10,
    color: '#fff',
  },
  imageInputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    margin: 10,
  },
  imageInput: {
    width: 300,
    height: 300,
  },
  avatarImage: {
    width: height * 0.072,
    height: height * 0.072,
    borderRadius: (height * 0.072) / 2,
    marginRight: 10,
  },
  questionText: {
    fontFamily: FONT_MEDIUM,
    fontSize: 13,
  },
  questionTextContainer: {
    flexWrap: 'wrap',
    marginBottom: 5,
  },
  durationLabelContainer: {
    alignItems: 'flex-end',
    justifyContent: 'center',
  },
  weeklyCheckinMotivationText: {
    fontFamily: FONT_LIGHT,
    fontSize: 13,
    fontStyle: 'italic',
    textAlign: 'center',
  },
  achievementContainer: {
    flexDirection: 'row',
  },
  achievementIconContainer: {
    paddingHorizontal: 3,
  },
  achievementIcon: {
    color: '#989898',
    fontSize: 20,
  },
  achievementText: {
    fontFamily: FONT_MEDIUM,
    fontSize: 13,
  },
  achievementTextContainer: {
    justifyContent: 'center',
    alignItems: 'center',
  },
});
