import {StyleSheet} from 'react-native';
import {
  FONT_APP_TITLE,
  FONT_LIGHT,
} from '../constants/Font';
import {getColor} from '../constants/color';
import {getScreenSize} from '../helpers/getSize';

export default StyleSheet.create({
  mainContainer: {
    padding: 20,
    justifyContent: 'space-between',
    height: getScreenSize().height - 40,
  },
  logoContainer: {
    alignItems: 'center',
  },
  image: {
    width: 150,
    height: 150,
  },
  logoText: {
    fontFamily: FONT_APP_TITLE,
    fontSize: 36,
    color: getColor('orange'),
  },
  texViewtContainer: {
    flex: 1,
    justifyContent: 'flex-start',
    alignItems: 'center',
    paddingTop: 10,
  },
  textContainerRow: {
    flex: 1,
    flexDirection: 'row',
  },
  textIconContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  textIcon: {
    fontSize: 20,
    color: getColor('theme'),
    paddingHorizontal: 10,
  },
  textContainer: {
    flex: 2,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  text: {
    fontFamily: FONT_LIGHT,
  },
  footer: {
    flex: 3,
    justifyContent: 'center',
    alignItems: 'center',
  },
  footerRow: {
    flexDirection: 'row',
    backgroundColor: 'red',
  },
  footerText: {
    fontFamily: FONT_LIGHT,
    color: getColor('grey'),
  },
});
