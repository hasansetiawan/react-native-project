// @flow
import React from 'react';
import {
  Modal,
  View,
} from 'react-native';
import {
  HeaderCard,
  LocalizedText,
  Button,
} from '../core-ui';
import styles from './ChallengeFinished-style';

type Props = {
  isVisible: boolean;
  onClose: () => void;
};

export default function ChallengeFinished(props: Props) {
  return (
    <Modal
      visible={props.isVisible}
      animationType="slide"
      transparent={true}
      onRequestClose={props.onClose}
    >
      <View style={styles.container}>
        <HeaderCard title="First challenge finished!" headerStyle={styles.headerStyle} color="green">
          <View style={styles.textContainer}>
            <LocalizedText from="programs/firstChallengeFinished" />
          </View>
          <Button
            type="primary"
            color="blue"
            leftIcon="web"
            iconStyle={styles.buttonIcon}
            text="programList"
            upperCase={true}
            onPres={() => {/* TODO: show program list */}}
          />
        </HeaderCard>
      </View>
    </Modal>
  );
}
