// @flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import autobind from 'class-autobind';
import {
  View,
  ScrollView,
} from 'react-native';
import {
  ChallengeCard,
  ConfirmationDialog,
  LoadingIndicator,
} from '../core-ui';
import SelectedProgramCard from './SelectedProgramCard';
import styles from './SelectedProgramView-style';
import type {Program, ProgramHabit} from '../types/Program';
import type {RootState} from '../types/RootState';
import type {User} from '../types/User';
import type {Dispatch} from '../types/Dispatch';

type Props = {
  program: Program;
  user: User;
  habitList: Map<string, ProgramHabit>;
  onJoiningProgramLoading: boolean;
  progressBarPercentage: number;
  onSubscribePress: () => void;
  showPopup: (content: string) => void;
  onConfirmStartProgram: (user: User, program: Program) => void;
};

type State = {
  isConfirmationDialogShown: boolean;
};


export class SelectedProgramView extends Component {
  props: Props
  state: State
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isConfirmationDialogShown: false,
    };
  }
  render() {
    let {program, user, habitList, onJoiningProgramLoading, onSubscribePress} = this.props;
    let habit = habitList.get(program.habits[0]) || {activityTexts: []};

    return (
      <View style={[styles.container, styles.selectedProgramViewContainer]}>
        <ScrollView>
          {(onJoiningProgramLoading) ? <LoadingIndicator style={{backgroundColor: '#FFF7'}} withModal /> : null}
          <View style={[styles.selectedProgramCard, {marginHorizontal: 20}]}>
            <SelectedProgramCard
              program={program}
              user={user}
              onStartPress={this._onStartPress}
              onSubscribePress={onSubscribePress}
            />
          </View>
          <View style={{flex: 1}}>

            <LessonsView habits={habit.activityTexts} />

            <ConfirmationDialog
              isOpen={this.state.isConfirmationDialogShown}
              onClose={this._closeConfirmationDialog}
              onConfirm={() => {
                this._closeConfirmationDialog();
                this.props.onConfirmStartProgram(user, program);
              }}
              content="joinProgramConfirmation"
              confirmIcon="check"
              confirmText="yes"
              cancelText="cancel"
            />
          </View>
        </ScrollView>
      </View>
    );
  }

  _onStartPress() {
    let {user, progressBarPercentage, showPopup, program} = this.props;
    if (program.isPremium === true && user.isPremiumMember === false) {
      showPopup('programPremium');
    } else if (progressBarPercentage === 100 ||
    (user.programRecords.length < 1 && user.completedProgramIds.length > 0)) {
      this._showConfirmationDialog();
    } else {
      showPopup('programNotDone');
    }
  }

  _showConfirmationDialog() {
    this.setState({isConfirmationDialogShown: true});
  }
  _closeConfirmationDialog() {
    this.setState({isConfirmationDialogShown: false});
  }
}

type LessonsViewProps = {
  habits: Array<string>;
};
function LessonsView(props: LessonsViewProps) {
  let {habits} = props;
  if (habits == null || habits.length < 1) {
    return null;
  }
  return (
    <View style={styles.lessonViewContainer}>
      {
        habits.map((habit, index) => {
          let type = '';
          if (index === 0) {
            type = 'TOP';
          } else if (index === habits.length - 1) {
            type = 'BOTTOM';
          } else {
            type = 'MIDDLE';
          }
          return (
            <ChallengeCard
              key={index}
              type={type}
              isLocked={false}
              day={index + 1}
              lesson={habit}
              onPress={() => {/* NOTE: it does nothing */}}
            />
          );
        })
      }
    </View>
  );
}

function mapStateToProps(state: RootState) {
  let {user, habit, loadingPage} = state;
  return {
    onJoiningProgramLoading: loadingPage.selectedProgramPage,
    progressBarPercentage: habit.progressPercentage,
    user,
    habitList: state.programs.habitList,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    showPopup: (content: string) => {
      dispatch({
        type: 'SHOW_POPUP',
        content,
      });
    },
    onConfirmStartProgram: (user: User, program: Program) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'selectedProgramPage',
      });
      dispatch({
        type: 'ASSIGN_PROGRAM_REQUESTED',
        token: user.token,
        programID: program.id,
      });
    },
  };
}


export default connect(mapStateToProps, mapDispatchToProps)(SelectedProgramView);
