// @flow

import React from 'react';
import {View, Text, Image} from 'react-native';
import Spinner from 'react-native-spinkit';

import styles from './SplashScreenView-style';
import {getColor} from '../constants/color';

export default function SplashScreenView() {
  return (
    <View style={styles.container}>
      <Image
        style={styles.logo}
        resizeMode="cover"
        source={require('../images/fulfilled_logo.png')}
      />
      <View>
        <Text style={styles.appTitleText}>Fulfilled</Text>
      </View>
      <View style={styles.loadingTextContainer}>
        <Text style={styles.loadingText}>mulai kebiasaan sehatmu dari sekarang</Text>
      </View>
      <Spinner
        isVisible={true}
        color={getColor('theme')}
        size={50}
        type="ThreeBounce"
      />
    </View>
  );
}
