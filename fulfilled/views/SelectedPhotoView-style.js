// @flow
import {StyleSheet} from 'react-native';
import {getScreenSize} from '../helpers/getSize';

let {height} = getScreenSize();
const IMAGE_CONTAINER_SIZE_RATIO = 0.45;
const IMAGE_SIZE_RATIO = 0.44;


export default StyleSheet.create({
  root: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  imageContainer: {
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: (height * IMAGE_CONTAINER_SIZE_RATIO) / 2,
    width: height * IMAGE_CONTAINER_SIZE_RATIO,
    height: height * IMAGE_CONTAINER_SIZE_RATIO,
  },
  image: {
    paddingRight: 10,
    width: height * IMAGE_SIZE_RATIO,
    height: height * IMAGE_SIZE_RATIO,
    borderRadius: (height * IMAGE_SIZE_RATIO) / 2,
  },
  buttonContainer: {
    paddingTop: 10,
  },
});
