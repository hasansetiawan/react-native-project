import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  challengeContainer: {
    flex: 1,
  },
  summaryContainer: {
    flex: 3,
    paddingBottom: 20,
  },
  scrollContainer: {
    flex: 5,
  },
});
