// @flow
import React, {Component, Children} from 'react';
import {View, ScrollView} from 'react-native';
import autobind from 'class-autobind';

type Props = {
  children?: ReactNode;
};
type Layout = {
  scrollViewHeight: number;
  contentHeight: number;
};

const SCROLL_TOLERANCE = 20;
const THROTTLE_MS = 16;

export default class MessageScrollView extends Component {
  props: Props;
  _isInit: boolean = true;
  _timeout: ?number;
  _scrollView: ?Object;
  _scrollOffsetY: number = 0;
  _isAtTop: boolean = true;
  _isAtBottom: boolean = true;
  _layout: Layout = {
    scrollViewHeight: 0,
    contentHeight: 0,
  };
  _previousLayout: Layout = {
    scrollViewHeight: 0,
    contentHeight: 0,
  };
  _childList: Array<string>;
  _previousChildList: Array<string>;

  constructor() {
    super(...arguments);
    autobind(this);
    let childList = getChildList(this.props);
    this._childList = childList;
    this._previousChildList = childList;
  }

  componentWillReceiveProps(newProps: Props) {
    this._childList = getChildList(newProps);
  }

  render() {
    let {children, ...otherProps} = this.props;
    return (
      <ScrollView
        {...otherProps}
        ref={(ref) => (this._scrollView = ref)}
        onContentSizeChange={(width, height) => {
          let {scrollViewHeight} = this._layout;
          this._layout = {
            contentHeight: height,
            scrollViewHeight,
          };
          this._onContentChange();
        }}
        onLayout={(event) => {
          let {height} = event.nativeEvent.layout;
          let {contentHeight} = this._layout;
          this._layout = {
            contentHeight,
            scrollViewHeight: height,
          };
        }}
        onScroll={(event) => {
          let scrollOffsetY = event.nativeEvent.contentOffset.y;
          this._scrollOffsetY = scrollOffsetY;
          this._isAtTop = (scrollOffsetY < SCROLL_TOLERANCE);
          this._isAtBottom = isAtBottom(this._layout, scrollOffsetY);
        }}
        scrollEventThrottle={THROTTLE_MS}
      >
        {this._getMessages(children)}
      </ScrollView>
    );
  }

  _getMessages(children) {
    let filtered = [];
    Children.forEach(children, (child) => {
      if (child.type === Message) {
        filtered.push(child);
      }
    });
    return filtered;
  }

  _onContentChange() {
    clearTimeout(this._timeout);
    this._timeout = setTimeout(this._contentChanged, THROTTLE_MS);
  }

  _contentChanged() {
    if (this._isInit) {
      // The first time always scroll to the bottom.
      this._scrollToBottom();
    } else if (this._isAtBottom) {
      this._scrollToBottom();
    // } else if (this._isAtTop) {
    //   this._scrollToTop();
    } else {
      let oldContentHeight = this._previousLayout.contentHeight;
      let newContentHeight = this._layout.contentHeight;
      let heightIncrease = newContentHeight - oldContentHeight;
      if (this._hasFirstMessageChanged()) {
        this._scrollTo(this._scrollOffsetY + heightIncrease);
      }
    }
    this._isInit = false;
    this._previousChildList = this._childList;
    this._previousLayout = this._layout;
  }

  _scrollToTop() {
    this._scrollTo(0);
    this._isAtTop = true;
    this._isAtBottom = false;
  }

  _scrollToBottom() {
    let maxScrollY = getMaxScrollY(this._layout);
    this._scrollTo(maxScrollY);
    this._isAtTop = false;
    this._isAtBottom = true;
  }

  _scrollTo(y: number) {
    let scrollView = this._scrollView;
    if (scrollView) {
      scrollView.scrollTo({x: 0, y: y, animated: true});
      this._scrollOffsetY = y;
    }
  }

  _hasFirstMessageChanged() {
    let oldFirstChildID = this._previousChildList[0];
    let newFirstChildID = this._childList[0];
    return (oldFirstChildID !== newFirstChildID);
  }

}

function getChildList(props: Props) {
  let childList: Array<string> = [];
  Children.forEach(props.children, (child) => {
    if (child.type === Message) {
      childList.push(child.props.id);
    }
  });
  return childList;
}

function isAtBottom(layout: Layout, scrollOffsetY: number) {
  let maxScrollY = getMaxScrollY(layout);
  let isScrolledPastBottom = scrollOffsetY > maxScrollY;
  let distanceFromBottom = maxScrollY - scrollOffsetY;
  return (isScrolledPastBottom || (distanceFromBottom < SCROLL_TOLERANCE));
}

function getMaxScrollY(layout: Layout) {
  let {scrollViewHeight, contentHeight} = layout;
  return (contentHeight > scrollViewHeight) ? contentHeight - scrollViewHeight : 0;
}


export class Message extends Component {
  props: {id: string};

  render() {
    let {id, ...otherProps} = this.props; // eslint-disable-line no-unused-vars
    return <View {...otherProps} />;
  }
}
