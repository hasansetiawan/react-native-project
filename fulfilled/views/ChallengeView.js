// @flow
import React, {Component} from 'react';
import {
  View,
  Text,
  Modal,
  TouchableWithoutFeedback,
  ActivityIndicator,
  ScrollView,
} from 'react-native';
import autobind from 'class-autobind';
import {connect} from 'react-redux';
import {
  LocalizedText,
  CountdownTimer,
  HeaderCard,
  Button,
  LoadingIndicator,
  QuestionCard,
  CardView,
  // Icon,
} from '../core-ui';

import ProgressBar from 'react-native-progress/Bar';
import {getScreenSize} from '../helpers/getSize';
import {getColor, GREY_HORIZONTAL_LINE} from '../constants/color';

import ProgramView from './ProgramView';
import TipsView from '../components/TipsView';
import TipsModal from '../components/TipsModal';
import styles from './SelectedProgramView-style';

import getContentFromHTML from '../selectors/getContentFromHTML';

import type {RootState} from '../types/RootState';
import type {Program} from '../types/Program';
import type {Habit} from '../types/Habit';
import type {HabitActivity} from '../types/HabitActivity';
import type {User} from '../types/User';
import type {Question} from '../types/Question';
import type {Content} from '../types/Content';

type ReadingMaterial = {
  title: string;
  html: string;
};

type Props = {
  user: User;
  program: Program;
  currentHabit: Habit;
  isCooldown: boolean;
  progressBarPercentage: number;
  currentHabitActivity: HabitActivity;
  readingMaterial: ReadingMaterial;
  isLoading: boolean;
  isCompletingHabitActivity: boolean;
  question: ?Question;
  contentCheckOff: ?Content;
  contentHabitComplete: ?Content;
  showCompleteContent: boolean;
  onCloseCompleteContent: () => void;
  onDoneThisHabitClick: (currentHabit: Habit, activity: HabitActivity, user: User) => void;
  onRemainingTimeEnd: () => void;
  onValidateFail: (errorMessage: string) => void;
  onProgramListClicked: (token: string) => void;
};

type State = {
  isQuestionShown: boolean;
};

const {width} = getScreenSize();

export class ChallengeView extends Component {
  props: Props;
  state: State;
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isQuestionShown: false,
    };
  }

  render() {
    let {
      user,
      program,
      currentHabit,
      isCooldown,
      progressBarPercentage,
      readingMaterial,
      onRemainingTimeEnd,
      isLoading,
      isCompletingHabitActivity,
      contentCheckOff,
      contentHabitComplete,
      showCompleteContent,
      onCloseCompleteContent,
      onProgramListClicked,
      question,
      onValidateFail,
    } = this.props;

    if (isLoading === true) {
      return (
        <View style={styles.container}>
          <LoadingIndicator />
        </View>
      );
    }

    if (
      progressBarPercentage === 100 ||
      (user.programRecords.length < 1 && user.completedProgramIds.length > 0)
    ) {
      return (
        <View style={styles.container}>
          <CardView style={{justifyContent: 'center'}}>
            <Text style={styles.finishText}>
              <LocalizedText from="habitFinished" />
            </Text>
            <View style={{alignSelf: 'center'}}>
              {/* <Icon name="supervisor-account" style={styles.icon} /> */}
              <Button style={{marginTop: 10}} leftIcon="web" iconStyle={{fontSize: 20, color: 'white'}} color="theme" text="programList" type="primary" onPress={() => onProgramListClicked(user.token)} />
            </View>
          </CardView>
          <TipsModal
            visible={showCompleteContent}
            onRequestClose={() => onCloseCompleteContent()}
            content={(progressBarPercentage === 100) ? contentHabitComplete : contentCheckOff}
            title={readingMaterial.title}
          />
        </View>
      );
    }

    if (!program.title) {
      return null;
    }

    let content;
    let headerBottomContent;
    if (!isCooldown) {
      content = (<TipsView readingMaterial={readingMaterial} />);
      headerBottomContent = (
        <View style={styles.headerContentRowButton}>
          {/* <LocalizedText from="habitNotDoneYet" style={styles.habitNotDoneText} /> */}
          <Button
            disabled={isCompletingHabitActivity}
            iconStyle={styles.headerButtonIcon}
            isBlockButton={true}
            color="blue"
            text="habitActivityDone"
            type="primary"
            style={styles.headerButton}
            onPress={this._onDoneThisHabitHandler}
          />
          {
            (isCompletingHabitActivity) ?
              <ActivityIndicator animating={isCompletingHabitActivity} /> : null
          }
        </View>
      );
    } else {
      content = (
        <View style={{flex: 1}}>
          <View style={styles.titleContainer}>
            <Text style={styles.titleContainerText}>
              <LocalizedText from="tomorrowTips" style={styles.titleText} />
              <Text>{' : '}</Text>
              <Text style={styles.titleTodayTips}>{readingMaterial.title}</Text>
            </Text>
          </View>
          <CountdownTimer onRemainingTimeEnd={onRemainingTimeEnd} />
        </View>
      );
      headerBottomContent = (
        <View style={styles.headerContentRowButton}>
          <LocalizedText from="habitDone" style={styles.habitDoneText} />
        </View>
      );
    }

    return (
      <View style={styles.container}>
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.selectedProgramCard} />
          <HeaderCard title={program.title} color="green">
            <View style={styles.headerContentRow}>
              <Text style={styles.headerContentText}>{currentHabit.text}</Text>
            </View>
            <View style={{marginBottom: 15}}>
              {/* <LocalizedText from="progress" style={styles.progressText} /> */}
              <View style={styles.progressBar}>
                <View style={{flex: 1}}>
                  <ProgressBar
                    progress={progressBarPercentage / 100}
                    width={width - 140}
                    height={6}
                    borderWidth={0}
                    borderRadius={20}
                    color={(progressBarPercentage > 0) ? getColor('green') : GREY_HORIZONTAL_LINE}
                    unfilledColor="#DDD"
                  />
                </View>
                <Text style={styles.progressPercentageText}>{currentHabit.daysCompleted + ' / ' + currentHabit.completionTarget}</Text>
                <Modal
                  animationType="fade"
                  visible={this.state.isQuestionShown}
                  transparent={true}
                  onRequestClose={this._closeQuestionModal}
                >
                  <TouchableWithoutFeedback onPress={this._closeQuestionModal}>
                    <View style={styles.questionContainer}>
                      <TouchableWithoutFeedback>
                        <View style={{marginHorizontal: 20}}>
                          <QuestionCard question={question} onSubmit={this._onQuestionSubmit} onValidateFail={onValidateFail} />
                        </View>
                      </TouchableWithoutFeedback>
                    </View>
                  </TouchableWithoutFeedback>
                </Modal>
                <TipsModal
                  visible={showCompleteContent}
                  onRequestClose={() => onCloseCompleteContent()}
                  content={(progressBarPercentage === 100) ? contentHabitComplete : contentCheckOff}
                />
              </View>
            </View>
            {
              headerBottomContent
            }
          </HeaderCard>
          {content}
        </ScrollView>
      </View>
    );
  }
  _onDoneThisHabitHandler() {
    let {currentHabit, currentHabitActivity, user, onDoneThisHabitClick, question} = this.props;
    if (question != null) {
      this.setState({isQuestionShown: true});
    } else {
      if (onDoneThisHabitClick) {
        onDoneThisHabitClick(currentHabit, currentHabitActivity, user);
      }
    }
  }
  _onQuestionSubmit(formData: string) {
    let {currentHabit, currentHabitActivity, user, onDoneThisHabitClick} = this.props;
    if (onDoneThisHabitClick) {
      this.setState({isQuestionShown: false});
      onDoneThisHabitClick(currentHabit, currentHabitActivity, user, formData);
    }
  }
  _closeQuestionModal() {
    this.setState({isQuestionShown: false});
  }
}

function mapStateToProps(state: RootState) {
  let {habit, habitActivity, readingMaterial, user} = state;
  let title = '';
  let html;
  let question;
  let currentHabitActivity;
  let contentCheckOff;
  let contentHabitComplete;
  if (user.programRecords.length > 0) {
    currentHabitActivity = habitActivity.habitActivityList.get(habit.currentHabit.activityTodoID);
    if (currentHabitActivity) {
      question = currentHabitActivity.questions[0];
      let material = readingMaterial.get(currentHabitActivity.readingMaterial);
      if (material) {
        title = material.title;
        html = material.html;
      }
    }
    contentCheckOff = state.content.get(habit.currentHabit.contentCheckoffID);
    if (contentCheckOff) {
      contentCheckOff = getContentFromHTML(contentCheckOff.renderedBody).content;
    }

    contentHabitComplete = state.content.get(habit.currentHabit.habitCompletionMessageID);
    if (contentHabitComplete) {
      contentHabitComplete = getContentFromHTML(contentHabitComplete.renderedBody).content;
    }
  }
  return {
    program: state.programs.currentProgram,
    currentHabit: habit.currentHabit,
    progressBarPercentage: habit.progressPercentage,
    user: state.user,
    isCooldown: habit.isCooldown,
    readingMaterial: {title, html},
    isLoading: state.loadingPage.challengePage,
    isCompletingHabitActivity: state.loadingPage.completeHabitActivity,
    showCompleteContent: habit.showCompleteContent,
    currentHabitActivity,
    question,
    contentCheckOff,
    contentHabitComplete,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onValidateFail: (errorMessage: string) => {
      dispatch({
        type: 'SHOW_TOAST',
        toastMessage: errorMessage,
      });
    },
    onDoneThisHabitClick: (currentHabit: Habit, currentHabitActivity: HabitActivity, user: User, formData?: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'completeHabitActivity',
      });
      dispatch({
        type: 'FINISH_HABIT_REQUESTED',
        token: user.token,
        userHabitRecordID: currentHabit.userHabitRecordID,
        habitID: currentHabit.id,
      });
      dispatch({
        type: 'FINISH_HABIT_ACTIVITY_REQUESTED',
        userID: user.id,
        token: user.token,
        habitActivityID: currentHabitActivity.id,
        formData,
      });
    },
    onRemainingTimeEnd: (user: User) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'challengePage',
      });
      dispatch({
        type: 'FETCH_HABIT_REQUESTED',
        token: user.token,
        programRecordID: user.programRecords[0],
      });
    },
    onCloseCompleteContent: () => {
      dispatch({
        type: 'CLOSE_COMPLETE_CONTENT',
      });
    },
    onProgramListClicked: (token: string) => {
      dispatch({
        type: 'LOADING_STARTED',
        loadingPages: 'programListPage',
      });
      dispatch({
        type: 'FETCH_PROGRAM_LIST_REQUESTED',
        token,
      });
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'programList',
          Component: ProgramView,
          title: 'programList',
        },
      });
    },
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(ChallengeView);
