// @flow
import React, {Component} from 'react';
import {View, Platform, AppState, ToastAndroid} from 'react-native';
import {Provider} from 'react-redux';
import codePush from 'react-native-code-push'; // eslint-disable-line

import createAppStore from './store/configureStore';
import RootNavigation from './navigation/RootNavigation';
import AndroidButtons from './core-ui/AndroidButtons';

import onCodePushSyncStatusChange from './helpers/onCodePushSyncStatusChange';

import type {DownloadProgress} from './types/CodePushState';

let store = createAppStore();

export default class App extends Component {
  componentDidMount() {
    AppState.addEventListener('change', this._appStateChanged);
  }
  componentWillUnmount() {
    AppState.removeEventListener('change', this._appStateChanged);
  }
  render() {
    return (
      <Provider store={store}>
        <View style={{flex: 1}}>
          <RootNavigation />
          {Platform.OS === 'ios' ?
            <AndroidButtons />
            : null
          }
        </View>
      </Provider>
    );
  }
  _appStateChanged(newAppState: string) {
    let onSyncStatusChange = (SyncStatus: number) => { // eslint-disable-line
      onCodePushSyncStatusChange(SyncStatus, store);
    };
    let onDownloadProgress = (downloadProgress: DownloadProgress) => { // eslint-disable-line
      if (downloadProgress) {
        store.dispatch({
          type: 'DOWNLOAD_PROGRESS_UPDATED',
          downloadProgress: downloadProgress.receivedBytes,
          totalDownloadSize: downloadProgress.totalBytes,
        });
      }
    };
    let onError = (error) => { // eslint-disable-line
      store.dispatch({
        type: 'UPDATE_FINISHED',
      });
      ToastAndroid.show(error.message, ToastAndroid.SHORT);
    };

    if (newAppState === 'active') {
      // if (Platform.OS === 'ios') {
      //   codePush.sync({
      //     installMode: codePush.InstallMode.ON_NEXT_RESUME,
      //   });
      // } else {
      //   codePush.sync({
      //     updateDialog: {
      //       title: 'Update Tersedia',
      //       mandatoryUpdateMessage: 'Terdapat update untuk aplikasi. Tekan tombol Lanjutkan untuk melakukan update.',
      //       optionalUpdateMessage: 'Tersedia update untuk aplikasi. Apakah Anda ingin melakukan update sekarang?',
      //       mandatoryContinueButtonLabel: 'Lanjutkan',
      //       optionalInstallButtonLabel: 'Ya',
      //       optionalIgnoreButtonLabel: 'Nanti',
      //     },
      //     installMode: codePush.InstallMode.IMMEDIATE,
      //   }, onSyncStatusChange, onDownloadProgress, onError);
      // }
    }
  }
}
