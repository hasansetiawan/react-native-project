//@flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
    marginHorizontal: 15,
  },
  zeroPadding: {
    padding: 0,
  },
});
