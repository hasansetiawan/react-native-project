//@flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View, ScrollView, Text} from 'react-native';
import {TextField, Button, Dropdown, TitleBar} from '../core-ui';
import styles from './NewAnalysis-style';
import type {Option} from '../core-ui/Dropdown';
import type {AnalyticForm} from '../types/Analytic';
import type {CostDetail} from '../types/Cost';

const propsDropdownPanen = {
  defaultValue: '5',
  options: [
    {value: 0, label: 'Pilih Presentase'},
    {value: 0.10, label: '10%'},
    {value: 0.15, label: '15%'},
    {value: 0.20, label: '20%'},
    {value: 0.25, label: '25%'},
    {value: 0.30, label: '30%'},
  ],
  animationType: 'none',
  label: 'Penyusutan',
};

type State = {
  presentaseOption: number;
  formData: AnalyticForm;
};

type Props = {
  onEditAnalysisSubmit: () => void;
  onPressCancel: () => void;
  plants: Array<Option>;
  plantCost: CostDetail;
};

export default class AnalysisEdit extends Component {
  state: State;
  props: Props;
  constructor() {
    super(...arguments);
    autobind(this);
    let {plantCost} = this.props;
    this.state = {
      presentaseOption: plantCost.depreciation / plantCost.firstHarvest,
      formData: {
        title: plantCost.title || '',
        plantID: plantCost.plantID || '',
        farmingProductionCost: plantCost.farmingProductionCost || 0,
        othersExpense: plantCost.seedsCost || 0,
        fieldRentCost: plantCost.fertilizerCost || 0,
        labourCost: plantCost.labourCost || 0,
        totalExpense: plantCost.totalExpense || 0,
        firstHarvest: plantCost.firstHarvest || 0,
        endHarvest: plantCost.endHarvest || 0,
        depreciation: plantCost.depreciation || 0,
        price: plantCost.price || 0,
        grossIncome: plantCost.grossIncome || 0,
        netIncome: plantCost.netIncome || 0,
      },
    };
  }
  render() {
    let {
      title, farmingProductionCost, fieldRentCost,
      othersExpense, labourCost,
      plantID, totalExpense, firstHarvest, endHarvest,
      depreciation, price, grossIncome, netIncome,
    } = this.state.formData;
    let {onEditAnalysisSubmit, plants, onPressCancel} = this.props;
    const propsPlantsDropdown = {
      options: plants,
      animationType: 'none',
      label: 'Jenis Tanaman',
    };
    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="Ubah Analisis Biaya" />
        <ScrollView>
          <View style={styles.formContainer}>
            <View style={styles.field}>
              <TextField label="Judul Analisis" placeholder="Judul Analisis" value={title} onChangeText={this._onJudulChange} />
              <Dropdown {...propsPlantsDropdown}
                  onSelect={this._onSelect}
                  selectedValue={plantID}
              />
              <TextField keyboardType="numeric" onBlur={this._onBlurIncome} itemLabelPre="Rp." label="Biaya Saprotan (Benih, Pupuk, dan Pestisida)" placeholder="Biaya Saprotan" value={farmingProductionCost.toString()} onChangeText={this._onSaprotanChange} />
              <TextField keyboardType="numeric" onBlur={this._onBlurIncome} itemLabelPre="Rp." label="Biaya Sewa Lahan" placeholder="Biaya Sewa Lahan" value={fieldRentCost.toString()} onChangeText={this._onFieldRentCost} />
              <TextField keyboardType="numeric" onBlur={this._onBlurIncome} itemLabelPre="Rp." label="Biaya Sarana Tambahan Lain" placeholder="Biaya Sarana Tambahan Lain" value={othersExpense.toString()} onChangeText={this._onOthersExpense} />
              <TextField keyboardType="numeric" onBlur={this._onBlurLabourCost} itemLabelPre="Rp." label="Biaya Tenaga Kerja" placeholder="Biaya Tenaga Kerja" value={labourCost.toString()} onChangeText={this._onLabourCost} />
            </View>
            <View style={styles.total}>
              <Text style={styles.textTotalLeft}>Total Biaya Pengeluaran</Text>
              <Text style={styles.textTotalRight}>Rp. {totalExpense.toString()}</Text>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.rowItem}>
                <TextField keyboardType="numeric" itemLabelEnd="Kg" label="Hasil Panen Awal" placeholder="Hasil Panen Awal" value={firstHarvest.toString()} onBlur={this._onBlurIncome} onChangeText={this._onFirstHarvest} />
              </View>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.rowItem}>
                <Dropdown {...propsDropdownPanen}
                  onSelect={this._onSelectPanenOption}
                  selectedValue={this.state.presentaseOption}
                />
              </View>
              <View style={{flex: 0.1}} />
              <View style={styles.rowRight}>
                <View style={{height: 13}} />
                <View style={styles.disabled}><Text>{depreciation.toString()} Kg</Text></View>
              </View>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.rowItem}>
                <Text style={{fontWeight: 'bold'}}>Hasil Panen Akhir</Text>
                <View style={styles.disabled}><Text>{endHarvest.toString()} Kg</Text></View>
              </View>
            </View>
            <View style={styles.rowContainer}>
              <View style={styles.rowItem}>
                <TextField onBlur={this._onBlurIncome} keyboardType="numeric" value={price.toString()} onChangeText={this._onPrice} label="Harga Hasil Panen" placeholder="Harga Hasil Panen" itemLabelPre="Rp." itemLabelEnd="/Kg" />
              </View>
            </View>
            <View style={styles.total}>
              <Text style={styles.textTotalLeft}>Total Penerimaan Hasil Panen</Text>
              <Text style={styles.textTotalRight}>Rp. {grossIncome.toString()}</Text>
            </View>
            <View style={styles.totalPendapatan}>
              <Text style={styles.textTotalLeft}>Total Pendapatan</Text>
              <Text style={styles.textTotalRight}>Rp. {netIncome.toString()}</Text>
            </View>
            <View style={styles.fieldButton}>
              <Button text="Cancel" onPress={() => onPressCancel()} inverted />
              <Button text="Submit" onPress={() => onEditAnalysisSubmit(this.state.formData)} />
            </View>
          </View>
        </ScrollView>
      </View>
    );
  }
  _onJudulChange(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        title: value,
      },
    });
  }
  _onPrice(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        price: Number(value),
        grossIncome: this._onCalculateGrossIncome(Number(value)),
        netIncome: this._onCalculateNetIncome(this._onCalculateGrossIncome(Number(value))),
      },
    });
  }
  _onCalculateExpense() {
    let formData = this.state.formData;
    let totalExpense = formData.farmingProductionCost + formData.fieldRentCost + formData.othersExpense + formData.labourCost;
    return totalExpense;
  }
  _onCalculateGrossIncome(price: number, endHarvest?: number) {
    let formData = this.state.formData;
    let harvest = endHarvest ? endHarvest : formData.endHarvest;
    return price * harvest;
  }
  _onCalculateNetIncome(grossIncome: number) {
    let formData = this.state.formData;
    return grossIncome - formData.totalExpense;
  }
  _onBlurLabourCost() {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        totalExpense: this._onCalculateExpense(),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onBlurIncome() {
    let formData = this.state.formData;
    let depreciation = this._depreciationCount(formData.firstHarvest, this.state.presentaseOption);
    this.setState({
      formData: {
        ...formData,
        depreciation: depreciation,
        endHarvest: this._onCalculateHarvest(depreciation),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onSaprotanChange(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        farmingProductionCost: Number(value),
        totalExpense: this._onCalculateExpense(),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onFieldRentCost(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        fieldRentCost: Number(value),
        totalExpense: this._onCalculateExpense(),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onOthersExpense(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        othersExpense: Number(value),
        totalExpense: this._onCalculateExpense(),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onLabourCost(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        labourCost: Number(value),
        totalExpense: this._onCalculateExpense(),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onFirstHarvest(value: string): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        firstHarvest: Number(value),
        endHarvest: this._onCalculateHarvest(Number(value), this.state.presentaseOption),
        grossIncome: this._onCalculateGrossIncome(formData.price),
        netIncome: this._onCalculateNetIncome(formData.grossIncome),
      },
    });
  }
  _onSelect(value: string | number): void {
    let formData = this.state.formData;
    this.setState({
      formData: {
        ...formData,
        plantID: value,
      },
    });
  }
  _onCalculateHarvest(depreciation: number) {
    let formData = this.state.formData;
    let endHarvest = formData.firstHarvest - depreciation;
    return endHarvest;
  }
  _depreciationCount(firstHarvest: number, precent: number) {
    return precent * firstHarvest;
  }
  _onSelectPanenOption(value: string | number): void {
    let formData = this.state.formData;
    let depreciation = this._depreciationCount(formData.firstHarvest, Number(value));
    let grossIncome = this._onCalculateGrossIncome(formData.price, this._onCalculateHarvest(depreciation));
    this.setState({
      presentaseOption: Number(value),
      formData: {
        ...formData,
        depreciation: depreciation,
        endHarvest: this._onCalculateHarvest(depreciation),
        grossIncome: grossIncome,
        netIncome: this._onCalculateNetIncome(grossIncome),
      },
    });
  }
}
