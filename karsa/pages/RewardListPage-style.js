import {StyleSheet, Dimensions} from 'react-native';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  root: {
    flex: 1,
  },
  reward: {
    width: 50,
    height: 50,
    backgroundColor: 'green',
  },
  rewardDetailWrapper: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  rewardDetail: {
    width: 200,
    height: 400,
  },
  headerHeight: {
    height: 150,
  },
  productWrapper: {
    flex: 1,
    flexDirection: 'row',
    marginBottom: 80,
    width: width,
    height: 100,
  },
  productItemWrapper: {
    marginHorizontal: 2.5,
    width: 0.3 * width,
    height: 175,
  },
  loading: {
    marginVertical: 20,
  },
  listContainer: {
    flex: 1,
    marginLeft: 16.5,
    marginBottom: 20,
  },
  tagText: {
    marginBottom: 10,
    fontSize: 18,
  },
});
