// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  contentWrapper: {
    padding: 20,
  },
  rowWrapper: {
    flexDirection: 'row',
  },
  centerWrapper: {
    alignItems: 'center',
  },
});
