// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: '#f4f4f4',
  },
  imageWrapper: {
    flex: 2,
    flexDirection: 'row',
    alignItems: 'stretch',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
  },
  textWrapper: {
    flex: 1,
    justifyContent: 'space-around',
    alignItems: 'center',
    paddingHorizontal: 15,
  },
  title: {
    fontSize: 20,
  },
  description: {
    textAlign: 'center',
  },
  button: {
    alignSelf: 'stretch',
  },
});
