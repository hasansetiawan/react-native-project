// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
  Alert,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  Dropdown,
  TextField,
} from '../core-ui';
import autobind from 'class-autobind';
import styles from './ForumPage-style';
import mapObjectToDropdownOptions from '../helpers/mapObjectToDropdownOptions';
import UserThreadsContainer from '../containers/UserThreadsContainer';
import OtherThreadsContainer from '../containers/OtherThreadsContainer';
import ExpertForumContainer from '../containers/ExpertForumContainer';

import type {Value, Option} from '../core-ui/Dropdown';
import type {Forum, ThreadCategory} from '../types/Forum';

type Props = {
  forumCategory: Map<number, Forum>;
  threadFilterCategory: Map<number, ThreadCategory>;
  selectedForumIdx?: string;
  activeGroup?: 'joined' | 'not-joined' | 'invite';
  fetchForumCategories: () => void;
  fetchThreadFilterCategories: (slug: Value) => void;
  fetchFilteredThreads: (forumCategory: Value, threadCategory: Value, sortBy: Value) => void;
  fetchSearchedThreads: (forumCategory: Value, query: string) => void;
  fetchSearchedGroups: (groupCategory: 'joined' | 'not-joined' | 'invite', query: string) => void;
  onAddNewForum?: () => void;
};
type State = {
  isFilterEnable: boolean;
  isSearching: boolean;
  searchText: string;
  selectedForumIdx: Value;
  filterCategoryIdx: Value;
  filterSortIdx: Value;
  forumOptions: Array<Option>;
  filterCategoryOptions: Array<Option>;
  page: number;
  isSearchResult: boolean;
  isFilterResult: boolean;
  activeGroup: 'joined' | 'not-joined' | 'invite' | null;
};

const filterSortOptions = [
  {value: 'latest', label: 'Terbaru'},
  {value: 'popular', label: 'Terpopuler'},
];

const hitSlop = {top: 5, bottom: 5, right: 5, left: 5};

export default class ForumPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    let {selectedForumIdx, activeGroup} = this.props;
    this.state = {
      isFilterEnable: false,
      isSearching: false,
      searchText: '',
      selectedForumIdx: selectedForumIdx ? selectedForumIdx : 'petani',
      filterCategoryIdx: 0,
      filterSortIdx: 0,
      forumOptions: [
        {value: 'mind', label: 'Forum Saya'},
      ],
      filterCategoryOptions: [
        {value: 'all', label: 'Semua'},
      ],
      page: 1,
      isSearchResult: false,
      isFilterResult: false,
      activeGroup: activeGroup ? activeGroup : null,
    };
  }

  componentWillMount() {
    this.props.fetchForumCategories();
    this.props.fetchThreadFilterCategories('petani');
  }

  componentWillReceiveProps(newProps: Props) {
    let {forumCategory, threadFilterCategory} = newProps;
    if (forumCategory && forumCategory.size > 0) {
      let forumOptions = [
        {value: 'mind', label: 'Forum Saya'}, // NOTE: the word "mind" is from the server, it should've been "mine"
        ...mapObjectToDropdownOptions(forumCategory, {value: 'slug', label: 'description'}),
      ];
      this.setState({forumOptions});
    }
    if (threadFilterCategory && threadFilterCategory.size > 0) {
      let filterCategoryOptions = [
        {value: 'all', label: 'Semua'},
        ...mapObjectToDropdownOptions(threadFilterCategory, {value: 'id', label: 'categoryName'}),
      ];
      this.setState({filterCategoryOptions});
    }
  }

  render() {
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Forum Diskusi">
          <IconButton icon="add" onPress={this.props.onAddNewForum} />
          <IconButton icon="search" onPress={this._onSearchPressed} />
        </TitleBar>
        {this._renderHeader()}
        {this._renderFilterForm()}
        {this._renderSearchForm()}
        {this._renderForum()}
      </View>
    );
  }

  _renderHeader() {
    let {isFilterEnable, forumOptions, selectedForumIdx} = this.state;
    if (isFilterEnable) {
      return (
        <View style={styles.headerRow}>
          <View style={[styles.rowWrapper, styles.verticalMargin]}>
            <View style={styles.flex} />
            <TouchableOpacity onPress={() => this._onFilterSubmit()} hitSlop={hitSlop}>
              <Text style={[styles.headerText, styles.rightMargin]}>SELESAI</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={() => this._onFilterPressed(false)} hitSlop={hitSlop}>
              <Text style={styles.headerText}>BATAL</Text>
            </TouchableOpacity>
          </View>
        </View>
      );
    }
    return (
      <View style={styles.headerRow}>
        <Dropdown
          options={forumOptions}
          onSelect={this._onForumSelected}
          selectedValue={selectedForumIdx}
          buttonStyle={styles.headerDropdown}
          textStyle={styles.headerDropdownText}
        />
        <TouchableOpacity onPress={() => this._onFilterPressed(true)} hitSlop={hitSlop}>
          <Text style={styles.headerText}>FILTER</Text>
        </TouchableOpacity>
      </View>
    );
  }

  _renderFilterForm() {
    let {isFilterEnable, filterCategoryOptions, filterCategoryIdx, filterSortIdx} = this.state;
    if (isFilterEnable) {
      return (
        <View style={[styles.filterFormWrapper, styles.topMargin]}>
          <View style={styles.filterForm}>
            <Dropdown
              placeholder="Pilih Kategori"
              options={filterCategoryOptions}
              onSelect={this._onFilterCategorySelected}
              selectedValue={filterCategoryIdx}
            />
          </View>
          <View style={styles.flex} />
          <View style={styles.filterForm}>
            <Dropdown
              placeholder="Urutkan"
              options={filterSortOptions}
              onSelect={this._onFilterSortSelected}
              selectedValue={filterSortIdx}
            />
          </View>
        </View>
      );
    }
    return null;
  }

  _renderSearchForm() {
    let {isSearching, searchText} = this.state;
    if (isSearching) {
      return (
        <View style={[styles.negativeTopMargin, styles.horizontalPadding]}>
          <TextField
            placeholder="Cari"
            iconName="search"
            value={searchText}
            onIconPress={() => this._onSearchSubmit()}
            onChangeText={this._onSearchTextChanged}
          />
        </View>
      );
    }
    return null;
  }

  _renderForum() {
    let {selectedForumIdx, isFilterResult, isSearchResult} = this.state;
    switch (selectedForumIdx) {
      case 'mind': {
        return (
          <UserThreadsContainer
            isForumResult={isSearchResult || isFilterResult}
            fetchMoreResult={this._onFetchMoreThreads}
          />
        );
      }
      case 'pakar': {
        return (
          <ExpertForumContainer
            setActiveGroupType={(activeGroup) => this.setState({activeGroup})}
            isForumResult={isSearchResult}
          />
        );
      }
      default: {
        return (
          <OtherThreadsContainer
            forum={selectedForumIdx}
            isForumResult={isSearchResult || isFilterResult}
            fetchMoreResult={this._onFetchMoreThreads}
          />
        );
      }
    }
  }

  _onFilterCategorySelected(selected: Value) {
    this.setState({filterCategoryIdx: selected});
  }

  _onFilterSortSelected(selected: Value) {
    this.setState({filterSortIdx: selected});
  }

  _onForumSelected(selected: Value) {
    this.props.fetchThreadFilterCategories(selected);
    let {activeGroup} = this.state;
    this.setState({
      selectedForumIdx: selected,
      isSearchResult: false,
      isFilterResult: false,
      activeGroup: selected !== 'pakar' ? null : activeGroup,
    });
  }

  _onFetchMoreThreads(threadsCount: number) {
    let {isFilterResult, isSearchResult, page} = this.state;
    if (!(threadsCount < (page - 1) * 7)) {
      if (isFilterResult) {
        this._onFilterSubmit(page);
      }
      if (isSearchResult) {
        this._onSearchSubmit(page);
      }
    }
  }

  _onFilterSubmit(page: number = 1) {
    let {fetchFilteredThreads} = this.props;
    let {selectedForumIdx, filterCategoryIdx, filterSortIdx} = this.state;
    if (filterCategoryIdx === 0 || filterSortIdx === 0) {
      Alert.alert('Filter Forum', 'Mohon terlebih dahulu pilih Kategori dan Urutkan');
    } else {
      fetchFilteredThreads(selectedForumIdx, filterCategoryIdx, filterSortIdx, page);
      this.setState({page: page + 1, isFilterResult: true});
    }
  }

  _onSearchSubmit(page: number = 1) {
    let {selectedForumIdx, searchText} = this.state;
    let {activeGroup} = this.state;
    if (activeGroup) {
      this.props.fetchSearchedGroups(activeGroup, searchText); // TODO: confirm if paging is used
    } else {
      this.props.fetchSearchedThreads(selectedForumIdx, searchText, page);
    }
    this.setState({page: page + 1, isSearchResult: true});
  }

  _onFilterPressed(isSetting: boolean) {
    this.setState({isFilterEnable: isSetting, isSearching: false});
  }

  _onSearchPressed() {
    this.setState({isSearching: !this.state.isSearching, isFilterEnable: false});
  }

  _onSearchTextChanged(text: string) {
    this.setState({searchText: text});
  }
}
