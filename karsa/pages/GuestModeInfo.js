// @flow
import React from 'react';
import {View, Text} from 'react-native';
import Button from '../core-ui/Button';

import styles from './GuestModeInfo-style';

type Props = {
  onButtonPress: () => void;
};

export default function GuestModeInfo(props: Props) {
  return (
    <View style={styles.root}>
      <Text>Silahkan lakukan registrasi / login jika</Text>
      <Text>ingin menggunakan fitur ini</Text>
      <Button text="MASUK" onPress={props.onButtonPress} />
    </View>
  );
}
