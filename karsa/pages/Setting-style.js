//@flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
  contentWrapper: {
    paddingHorizontal: 15,
  },
  profilePictureWrapper: {
    alignSelf: 'center',
    paddingVertical: 20,
  },
  buttonWrapper: {
    flexDirection: 'row',
    padding: 15,
  },
});
