import {StyleSheet, Dimensions} from 'react-native';
import {IMAGE_SLIDE_RATIO} from '../constants/size';

let {width} = Dimensions.get('window');

export default StyleSheet.create({
  iconWrapper: {
    flexDirection: 'row',
  },
  iconText: {
    color: 'white',
    fontSize: 16,
    marginHorizontal: 10,
  },
  image: {
    width: width,
    height: width * IMAGE_SLIDE_RATIO,
  },
  descriptionWrapper: {
    flexDirection: 'row',
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  description: {
    color: 'black',
  },
  titleWrapper: {
    padding: 10,
  },
  titleText: {
    color: 'black',
    fontSize: 18,
    fontWeight: 'bold',
  },
  contentWrapper: {
    paddingVertical: 5,
    paddingHorizontal: 7,
  },
  contentText: {
    fontSize: 16,
  },
  icon: {
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  lable: {
    fontSize: 18,
    color: 'black',
  },
});
