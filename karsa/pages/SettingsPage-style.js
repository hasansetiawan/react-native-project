// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
  },
  rowWrapper: {
    alignItems: 'center',
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    flexDirection: 'row',
    justifyContent: 'space-between',
    padding: 20,
  },
  text: {
    color: '#999',
  },
  textBold: {
    fontWeight: 'bold',
  },
  icon: {
    fontSize: 24,
  },
  accordionMenu: {
    borderTopWidth: 0,
    borderBottomWidth: 1,
    borderColor: '#ccc',
    flexDirection: 'row',
    paddingHorizontal: 20,
  },
  accordionChild: {
    backgroundColor: '#888',
    width: 10,
    marginRight: 10,
  },
});
