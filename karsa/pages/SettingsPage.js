// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {
  TitleBar,
  Icon,
  Accordion,
} from '../core-ui';
import styles from './SettingsPage-style';
import TutorialModalContainer from '../containers/TutorialModalContainer';

type State = {
  isTutorialCollapsed: boolean;
};

type Props = {
  guestMode: boolean;
  onChangePasswordPress: () => void;
  onAboutPress: () => void;
  onContactPress: () => void;
  onTermsPress: () => void;
  onLogoutPress: () => void;
  onTutorialPress: () => void;
  navigateToProfile: () => void;
};

export default class SettingsPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isTutorialCollapsed: true,
    };
  }

  render() {
    let {guestMode, onChangePasswordPress, onAboutPress, onContactPress, onTermsPress, navigateToProfile, onLogoutPress, onTutorialPress} = this.props;
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Pengaturan" />
        <ScrollView ref="settingScrollRef">
          {guestMode ? null : (
            <SettingMenu type="primary" label="PROFIL" onPress={navigateToProfile} />
          )}
          {guestMode ? null : (
            <SettingMenu type="primary" label="UBAH PASSWORD" onPress={onChangePasswordPress} />
          )}
          <Accordion
            style={styles.rowWrapper}
            textStyle={styles.text}
            title="Tutorial"
            isCollapsed={this.state.isTutorialCollapsed}
            onPress={this._onTutorialAccordionPress}
          >
            <SettingMenu type="accordionMenu" label="Tanaman" onPress={onTutorialPress} />
            <SettingMenu type="accordionMenu" label="Sidebar" onPress={onTutorialPress} />
            <SettingMenu type="accordionMenu" label="Sharing" onPress={onTutorialPress} />
          </Accordion>
          <SettingMenu type="secondary" label="Tentang Kami" onPress={onAboutPress} />
          <SettingMenu type="secondary" label="Hubungi Kami" onPress={onContactPress} />
          <SettingMenu type="secondary" label="Syarat & Ketentuan" onPress={onTermsPress} />
          <SettingMenu type="secondary" label="Kebijakan Privasi" onPress={onTermsPress} />
          <SettingMenu type="secondary" label="Keluar" onPress={onLogoutPress} />
        </ScrollView>
        <TutorialModalContainer />
      </View>
    );
  }

  _onTutorialAccordionPress() {
    this.setState({
      isTutorialCollapsed: !this.state.isTutorialCollapsed,
    });
  }
}

type SettingsMenuProps = {
  type: 'primary' | 'secondary' | 'accordionMenu';
  label: string | ReactNode; // NOTE: may accept <LocalizedText />
  onPress: () => void;
};

export function SettingMenu(props: SettingsMenuProps) {
  let {type, label, onPress} = props;
  let content;
  switch (type) {
    case 'primary':
      content = (
        <View style={styles.rowWrapper}>
          <Text style={styles.textBold}>{label}</Text>
          <Icon name="chevron-right" style={styles.icon} />
        </View>
      );
      break;
    case 'secondary':
      content = (
        <View style={styles.rowWrapper}>
          <Text style={styles.text}>{label}</Text>
        </View>
      );
      break;
    case 'accordionMenu':
      content = (
        <View style={styles.accordionMenu}>
          <View style={styles.accordionChild} />
          <Text style={[styles.text, {marginVertical: 10}]}>{label}</Text>
        </View>
      );
      break;
    default:
      content = (
        <View style={styles.rowWrapper}>
          <Text style={styles.text}>{label}</Text>
        </View>
      );
  }

  return (
    <TouchableOpacity onPress={onPress}>
      {content}
    </TouchableOpacity>
  );
}
