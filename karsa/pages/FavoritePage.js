// @flow
import React from 'react';
import {StyleSheet, View} from 'react-native';

import TitleBar from '../core-ui/TitleBar';
import FavoriteListContainer from '../containers/FavoriteListContainer';

let styles = StyleSheet.create({
  root: {
    flex: 1,
  },
});

export default function FavoritePage() {
  return (
    <View style={styles.root}>
      <TitleBar iconButtonName="menu" title="Favorit" />
      <FavoriteListContainer />
    </View>
  );
}
