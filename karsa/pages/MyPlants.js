// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  ListView,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  GradientCard,
  TextField,
  LoadingIndicator,
  ProductHighlight,
} from '../core-ui';
import FavoriteButton from '../core-ui/FavoriteButton';
import styles from './MyPlants-style';
import {ALTERNATE_GREY} from '../constants/color';
import listViewDataSource from '../helpers/listViewDataSource';
import {groupItems} from '../core-ui/GridView'; //TODO CHange this to helpers

import type {Plant} from '../types/Plant';

import karsa from '../images/icon_my_plant.png';

type State = {
  searchKeyword: string;
  isSearchShown: boolean;
  currentPage: number;
};

type Props = {
  plants: Array<Plant>;
  onPress: (plantID: number) => void;
  onSearch: (keyword: string) => void;
  fetchMoreData: (page: number) => void;
  isFetchingMoreData: boolean;
};

export default class MyPlants extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      searchKeyword: '',
      isSearchShown: false,
      currentPage: 2,
    };
  }
  _generateDS(items: Array<Plant>) {
    let ds = listViewDataSource();
    return ds.cloneWithRows(groupItems(items, 2));
  }
  _renderItem(plants: Array<Plant>) {
    let plantRow = plants.map((plant) => {
      return (
        <GradientCard
          key={plant.id}
          type="plant"
          photo={plant.photoThumb == null ? karsa : plant.photoThumb}
          name={plant.plantName}
          plantCategory={plant.plantCategory}
          plantVariant={plant.plantVariant}
          plantAge={plant.plantAge}
          onPress={() => this.props.onPress(plant.id)}
          customImageStyle={{backgroundColor: ALTERNATE_GREY}}
          highlight={plant.type === 'example' ? <ProductHighlight icon="info-outline" text="Contoh" /> : null}
        />
      );
    });
    if (plantRow.length % 2 !== 0) {
      plantRow.push(<View key={-1} style={{flex: 1}} />);
    }
    return (
      <View style={{flexDirection: 'row'}}>
        {plantRow}
      </View>
    );
  }
  _fetchMoreData() {
    let {plants, fetchMoreData} = this.props;
    let {currentPage} = this.state;
    if (!(plants.length < (currentPage - 1) * 12 / 2)) {
      fetchMoreData(this.state.currentPage);
      let temp = this.state.currentPage;
      this.setState({
        currentPage: ++temp,
      });
    }
  }
  componentWillReceiveProps(newProps: Props) {
    let {plants} = newProps;
    if (plants.length <= 12) {
      this.setState({
        currentPage: 2,
      });
    }
  }
  render() {
    let {plants, onSearch, isFetchingMoreData} = this.props;
    let onSearchHandler = () => {
      onSearch(this.state.searchKeyword);
    };
    let content = (
      <ListView
        dataSource={this._generateDS(plants)}
        renderRow={(item) => this._renderItem(item)}
        onEndReachedThreshold={100}
        enableEmptySections={true}
        onEndReached={() => this._fetchMoreData()}
        renderFooter={() => isFetchingMoreData ? <LoadingIndicator /> : null}
        showsVerticalScrollIndicator={false}
      />
    );
    if (plants.length === 0) {
      if (isFetchingMoreData) {
        content =
          <View style={styles.centerFlex}>
            <LoadingIndicator />
          </View>;
      } else {
        content = (
          <View style={styles.noSearchResult}>
            <Text style={styles.noSearchResultText}>Tanaman tidak ditemukan</Text>
          </View>
        );
      }
    }
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Panduan Mingguan">
          <IconButton icon="search" onPress={() => this.setState({isSearchShown: true})} />
          <FavoriteButton
            id="weeklyGuidance"
            description="Panduan Mingguan"
            actions={[
              {type: 'PUSH_ROUTE', key: 'weeklyGuidance'},
            ]}
          />
        </TitleBar>
        {
          this.state.isSearchShown ?
            <View style={styles.searchBar}>
              <TextField
                label="Pencarian"
                iconName="search"
                placeholder="Cari tanaman"
                onChangeText={(text) => this.setState({searchKeyword: text})}
                onIconPress={onSearchHandler}
              />
            </View> :
            null
        }
        <View style={styles.body}>
          {content}
        </View>
      </View>
    );
  }

}
