//@flow

import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

const marginField = {
  marginLeft: 15,
  marginRight: 15,
};
const headerText = {
  fontWeight: 'bold',
  height: 20,
  color: '#3c3d3b',
};

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  mainContainer: {
    flex: 1,
    paddingTop: 6,
  },
  formContainer: {
    flex: 0.4,
    ...marginField,
  },
  buttonContainer: {
    flex: 0.1,
    flexDirection: 'row',
  },
  loginButton: {
    flex: 0.2,
    backgroundColor: '#19c80b',
    alignItems: 'center',
    marginBottom: 20,
  },
  registerButton: {
    flex: 1,
    backgroundColor: '#19c80b',
    alignItems: 'center',
    marginBottom: 20,
    marginTop: 20,
  },
  greenText: {
    color: PRIMARY_GREEN,
  },
  field: {
    paddingHorizontal: 10,
  },
  headerText: {
    fontSize: 18,
    ...headerText,
  },
  headerTextCabang: {
    fontSize: 16,
    ...headerText,
  },
  addressText: {
    marginHorizontal: 7,
    fontSize: 14,
    flex: 1,
    flexWrap: 'wrap',
    color: '#6c6d6b',
  },
  textWithIcon: {
    flexDirection: 'row',
    alignItems: 'flex-start',
    marginTop: 5,
  },
  icon: {
    fontSize: 18,
    marginRight: 5,
  },
  contact: {
    marginLeft: 7,
    fontSize: 14,
    color: '#6c6d6b',
  },
  divider: {
    height: 20,
    marginBottom: 10,
    marginTop: 30,
    flexDirection: 'row',
  },
  hr: {
    borderBottomWidth: 1,
    borderBottomColor: '#cacfcc',
    flex: 0.4,
  },
});
