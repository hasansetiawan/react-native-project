//@flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {ScrollView, Alert} from 'react-native';
import {TextField, Button, LoadingIndicator} from '../core-ui';
import styles from './ContactEmail-style';
import type {EmailMessage} from '../types/Office';
type ContactProps = {
  initialName?: string;
  initialEmail?: string;
  isLoading?: boolean;
  onSubmit: (emailMessage: EmailMessage) => void;
};

export default class ContactEmail extends Component {
  state: EmailMessage;
  props: ContactProps;
  constructor() {
    super(...arguments);
    autobind(this);
    let {initialName, initialEmail} = this.props;
    this.state = {
      name: initialName ? initialName : '',
      email: initialEmail ? initialEmail : '',
      phone: '',
      message: '',
    };
  }
  render() {
    let {isLoading} = this.props;
    let {name, email, phone, message} = this.state;
    let button = isLoading ? (
      <Button inverted>
        <LoadingIndicator style={styles.zeroPadding} small />
      </Button>
    ) : (
      <Button text="Kirim" onPress={this._onSubmit} />
    );
    return (
      <ScrollView style={styles.mainContainer} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps>
        <TextField label="Nama" autoCapitalize="words" placeholder="Masukan Nama Anda" value={name} onChangeText={this._onNameChange} />
        <TextField label="Email" keyboardType="email-address" autoCapitalize="none" placeholder="Masukan Email Anda" value={email} onChangeText={this._onEmailChange} />
        <TextField label="Nomor Telepon" keyboardType="phone-pad" placeholder="Masukan No HP Anda" value={phone} onChangeText={this._onPhoneChange} />
        <TextField label="Pesan" autoCapitalize="sentences" placeholder="Masukan Pesan Anda" multiline={true} value={message} onChangeText={this._onMessageChange} />
        {button}
      </ScrollView>
    );
  }
  _onNameChange(value: string): void {
    this.setState({
      name: value,
    });
  }
  _onEmailChange(value: string): void {
    this.setState({
      email: value,
    });
  }
  _onPhoneChange(value: string): void {
    this.setState({
      phone: value,
    });
  }
  _onMessageChange(value: string): void {
    this.setState({
      message: value,
    });
  }
  _onSubmit() {
    let {name, email, phone, message} = this.state;
    let isMessageValid =
      name !== '' &&
      email !== '' &&
      phone !== '' &&
      message !== '';
    isMessageValid ? this._alertMessage('valid') : this._alertMessage('invalid');
    this.props.onSubmit({...this.state});
  }
  _alertMessage(status: 'valid' | 'invalid') {
    switch (status) {
      case 'valid': {
        Alert.alert(
          'Info',
          'Email berhasil dikirim!',
          [
            {text: 'OK'},
          ],
        );
        break;
      }
      case 'invalid': {
        Alert.alert(
          'Info',
          'Mohon masukkan input data dengan benar',
          [
            {text: 'OK'},
          ],
        );
        break;
      }
    }
  }
}
