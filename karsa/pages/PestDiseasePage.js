// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  Image,
  TouchableOpacity,
  ListView,
  ActivityIndicator,
} from 'react-native';
import {
  TitleBar,
  StatefulTabGroup,
  Tab,
  Dropdown,
  LoadingIndicator,
  TextField,
} from '../core-ui';
import autobind from 'class-autobind';
import styles from './PestDiseasePage-style';
import listViewDataSource from '../helpers/listViewDataSource';

import type {Value} from '../core-ui/Dropdown';
import type {PestDisease, PestDiseaseCommodity} from '../types/PestDisease';

type Props = {
  commodities: Array<PestDiseaseCommodity>;
  pests: Array<PestDisease>;
  diseases: Array<PestDisease>;
  fetchMorePestData: (currentPage: number, filterCommodity?: number | string) => void;
  isFetchingMorePestData: boolean;
  isFetchingMoreDiseaseData: boolean;
  fetchMoreDiseaseData: (currentPage: number, filterCommodity?: number | string) => void;
  selectedPestCommodityChanged: () => void;
  selectedDiseaseCommodityChanged: () => void;
  isPestDiseaseLoading: boolean;
  navigateToPestDetail: (pestID: number) => void;
  navigateToDiseaseDetail: (diseaseID: number) => void;
  searchPest: (query: string) => void;
  searchDisease: (query: string) => void;
};
type State = {
  selectedPestCommodity: Value;
  selectedDiseaseCommodity: Value;
  pestPage: number;
  diseasePage: number; // range page: 1-8
  activeTab: 'pest' | 'disease';
  searchTextPest: string;
  searchTextDisease: string;
  isSearchRequested: boolean;
};

export default class PestDiseasePage extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedPestCommodity: -1,
      selectedDiseaseCommodity: -1,
      diseasesDS: this._generateDS(this.props.diseases),
      diseasePage: 2, // page number 1 data will be send on initial load
      pestPage: 2,    // page number 1 data will be send on initial load
      activeTab: 'pest',
      searchTextPest: '',
      searchTextDisease: '',
      isSearchRequested: false,
    };
  }

  _generateDS(items: Array<PestDisease>) {
    let ds = listViewDataSource();
    return ds.cloneWithRows(items);
  }

  render() {
    let {pests, diseases, isPestDiseaseLoading} = this.props;
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Hama & Penyakit" />
        {
          isPestDiseaseLoading ? (
            <LoadingIndicator />
          ) : (
            <StatefulTabGroup styling="primary" onTabSelect={this._onTabSelected}>
              <Tab title="HAMA">
                {this._renderTab(pests, 'pest')}
              </Tab>
              <Tab title="PENYAKIT">
                {this._renderTab(diseases, 'disease')}
              </Tab>
            </StatefulTabGroup>
          )
        }
      </View>
    );
  }

  _fetchMoreData(tabName: 'pest' | 'disease') {
    if (tabName === 'disease') {
      let {diseasePage} = this.state;
      let filterCommodity = this.state.selectedDiseaseCommodity;
      this.props.fetchMoreDiseaseData(diseasePage, filterCommodity);
      this.setState({
        diseasePage: diseasePage += 1,
      });
    } else if (tabName === 'pest') {
      let {pestPage} = this.state;
      let filterCommodity = this.state.selectedPestCommodity;
      this.props.fetchMorePestData(pestPage, filterCommodity);
      this.setState({
        pestPage: pestPage += 1,
      });
    }
  }
  _renderTab(collection: Array<PestDisease>, tabName: 'pest' | 'disease') {
    let {selectedPestCommodity, selectedDiseaseCommodity, searchTextPest, searchTextDisease, isSearchRequested} = this.state;
    let {commodities} = this.props;
    let options = commodities.map(({commodity, id}) => ({value: id, label: commodity}));
    options.push({value: -1, label: 'Semua'});
    return (
      <View style={styles.tabWrapper}>
        <View style={styles.tabContentWrapper}>
          {
            tabName === 'pest' ? (
              <View style={styles.searchContainer}>
                <View style={styles.searchTextContainer}>
                  <TextField
                    iconName="search"
                    placeholder="Cari Hama"
                    value={searchTextPest}
                    onChangeText={this._onSearchTextChanged}
                    onIconPress={this._onSearchPressed}
                  />
                </View>
              </View>
            ) : (
              <View style={styles.searchContainer}>
                <View style={styles.searchTextContainer}>
                  <TextField
                    placeholder="Cari Penyakit"
                    value={searchTextDisease}
                    iconName="search"
                    onIconPress={this._onSearchPressed}
                    onChangeText={this._onSearchTextChanged}
                  />
                </View>
              </View>
            )
          }
          <Dropdown
            options={options}
            label="Pilih Komoditas"
            onSelect={
              tabName === 'pest' ?
                this._onPestCommoditySelected :
                this._onDiseaseCommoditySelected
            }
            selectedValue={tabName === 'pest' ? selectedPestCommodity : selectedDiseaseCommodity}
          />
          <ListView
            showsVerticalScrollIndicator={false}
            style={{flex: 1}}
            dataSource={this._generateDS(collection)}
            renderRow={(item, sectionID, rowID) => {
              return this._renderItem(item, rowID, tabName);
            }}
            enableEmptySections={true}
            onEndReachedThreshold={100}
            onEndReached={
              () => {
                if (!isSearchRequested) {
                  if (collection.length > 3) {
                    this._fetchMoreData(tabName);
                  }
                }
              }
            }
            renderFooter={() => {
              if (tabName === 'pest' && this.props.isFetchingMorePestData && this.state.pestPage < 9) {
                return (
                  <View style={styles.spinnerWrapper}>
                    <ActivityIndicator
                      animating={true}
                      color="#00c853"
                    />
                  </View>
                );
              } else if (tabName === 'pest' && !this.props.isFetchingMorePestData && this.props.pests.length === 0) {
                return (
                  <Text>Data hama tidak ada</Text>
                );
              } else if (tabName === 'disease' && this.props.isFetchingMoreDiseaseData && this.state.diseasePage < 9) {
                return (
                  <View style={styles.spinnerWrapper}>
                    <ActivityIndicator
                      animating={true}
                      color="#00c853"
                    />
                  </View>
                );
              } else if (tabName === 'disease' && !this.props.isFetchingMoreDiseaseData && this.props.diseases.length === 0) {
                return (
                  <Text>Data penyakit tidak ada</Text>
                );
              } else {
                return null;
              }
            }}
          />
        </View>
      </View>
    );
  }

  _renderItem(item: PestDisease, index: number, type: 'pest' | 'disease') {
    let {photo} = item;
    return (
      <TouchableOpacity
        key={index}
        onPress={() => {
          type === 'pest' ?
            this.props.navigateToPestDetail(item.id) :
            this.props.navigateToDiseaseDetail(item.id);
        }}
      >
        <View style={styles.listItemWrapper}>
          <Image style={styles.thumbnail} source={photo} resizeMode="cover" />
          <View style={styles.columnWrapper}>
            <View style={styles.itemTitleWrapper}>
              <Text style={styles.itemTitle}>{item.name}</Text>
            </View>
            <Text ellipsizeMode="tail" numberOfLines={4}>{item.description}</Text>
          </View>
        </View>
      </TouchableOpacity>
    );
  }
  _onPestCommoditySelected(index: Value) {
    if (index !== this.state.selectedPestCommodity) {
      this.props.selectedPestCommodityChanged();
      this.props.fetchMorePestData(1, index);
      this.setState({
        selectedPestCommodity: index,
        pestPage: 2,
        searchTextPest: '',
        isSearchRequested: false,
      });
    }
  }
  _onDiseaseCommoditySelected(index: Value) {
    if (index !== this.state.selectedDiseaseCommodity) {
      this.props.selectedDiseaseCommodityChanged();
      this.props.fetchMoreDiseaseData(1, index);
      this.setState({
        selectedDiseaseCommodity: index,
        diseasePage: 2,
        searchTextDisease: '',
        isSearchRequested: false,
      });
    }
  }

  _onTabSelected(tabIndex: number) {
    this.setState({
      activeTab: tabIndex === 0 ? 'pest' : 'disease',
    });
  }

  _onSearchTextChanged(text: string) {
    let {activeTab} = this.state;
    if (activeTab === 'pest') {
      this.setState({
        searchTextPest: text,
      });
    } else {
      this.setState({
        searchTextDisease: text,
      });
    }
  }

  _onSearchPressed() {
    let {activeTab, searchTextPest, searchTextDisease} = this.state;
    let {searchPest, searchDisease} = this.props;
    if (activeTab === 'pest') {
      searchPest(searchTextPest);
      this.setState({selectedPestCommodity: -1, isSearchRequested: true});
    } else {
      searchDisease(searchTextDisease);
      this.setState({selectedDiseaseCommodity: -1, isSearchRequested: true});
    }
  }
}
