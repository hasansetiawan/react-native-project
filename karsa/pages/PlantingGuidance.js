//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  Image,
} from 'react-native';
import {
  FlatButton,
  TileButtonWrapper,
  TitleBar,
  LoadingIndicator,
} from '../core-ui';
import {
  MyPlants, Cantaloupe, Chili, Corn, Cucumber, Eggplant, Onion, Peas,
  Potato, SweetPotato, Tomato, Watermelon,
} from '../core-image';
import styles from './HomePage-style';
import type {PlantingGuide} from '../types/PlantingGuide';

type Props = {
  guestMode: boolean;
  isPlantingGuidesLoading: boolean;
  plantingGuides: Array<PlantingGuide>;
  navigateTo: () => () => void;
  onFavorite: () => void;
  navigateToPlantingGuidanceDetail: (plantDetailNames: Array<string>) => void;
};

export default class PlantingGuidance extends Component {
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    let {isPlantingGuidesLoading} = this.props;
    let {_navigateDetail} = this;
    let content = isPlantingGuidesLoading ?
      (
        <View style={styles.menus}>
          <LoadingIndicator />
        </View>
      ) : (
        <View style={styles.menus}>
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.lightCyan]} onPress={() => _navigateDetail(1)}>
              {this._renderButton(MyPlants, 'Padi')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.lightGreen]} onPress={() => _navigateDetail(2)}>
              {this._renderButton(Chili, 'Cabai')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.green]} onPress={() => _navigateDetail(3)}>
              {this._renderButton(Peas, 'Kacang-Kacangan')}
            </FlatButton>
          </TileButtonWrapper>
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.green]} onPress={() => _navigateDetail(4)}>
              {this._renderButton(SweetPotato, 'Ubi')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={() => _navigateDetail(5)}>
              {this._renderButton(Corn, 'Jagung')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={() => _navigateDetail(6)}>
              {this._renderButton(Potato, 'Kentang')}
            </FlatButton>
          </TileButtonWrapper>
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.lightCyan]} onPress={() => _navigateDetail(7)}>
              {this._renderButton(Eggplant, 'Terung')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.lightGreen]} onPress={() => _navigateDetail(8)}>
              {this._renderButton(Cucumber, 'Timun')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.green]} onPress={() => _navigateDetail(9)}>
              {this._renderButton(Onion, 'Bawang-Bawangan')}
            </FlatButton>
          </TileButtonWrapper>
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.green]} onPress={() => _navigateDetail(10)}>
              {this._renderButton(Tomato, 'Tomat')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={() => _navigateDetail(11)}>
              {this._renderButton(Cantaloupe, 'Melon')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={() => _navigateDetail(12)}>
              {this._renderButton(Watermelon, 'Semangka')}
            </FlatButton>
          </TileButtonWrapper>
        </View>
    );
    return (
      <View style={{flex: 1}}>
        <TitleBar title="Panduan Penanaman" />
        {content}
      </View>
    );
  }

  _navigateDetail(plantTypeID: number) {
    let {plantingGuides, navigateToPlantingGuidanceDetail} = this.props;

    // Filtering the guides by selected plant type
    let filteredGuidesByType = plantingGuides.filter((guide) => {
      return guide.plantTypeID === plantTypeID;
    });

    // Setting planting guides detail list from filteredGuidesByType
    let plantDetailNames = [];
    filteredGuidesByType.map((guide) => {
      plantDetailNames.push(guide.plantDetailName);
    });
    navigateToPlantingGuidanceDetail(plantDetailNames);
  }

  _renderButton(image, text) {
    return (
      <View style={styles.button}>
        <Image source={image} style={styles.image} resizeMode="contain" />
        <Text style={styles.buttonText}>{text}</Text>
      </View>
    );
  }
}
