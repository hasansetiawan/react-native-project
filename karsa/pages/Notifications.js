//@flow
import React, {Component} from 'react';
import {View, Text, ListView, TouchableOpacity} from 'react-native';
import {TitleBar, ThumbnailCard, LoadingIndicator} from '../core-ui';
import autobind from 'class-autobind';
import GuestModeInfoContainer from '../containers/GuestModeInfoContainer';
import styles from './Notifications-style';
import listViewDataSource from '../helpers/listViewDataSource';

import defaultAvatar from '../images/default-avatar.png';

import type {Notification} from '../types/Notifications';

type Props = {
  guestMode: boolean;
  notifications: Map<number, Notification>;
  isNotificationLoading: boolean;
  fetchNotifications: () => void;
  onPress?: () => void;
};

type State = {
  notificationDataSource: ListView.DataSource;
  page: number;
};

export default class NotificationPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      notificationDataSource: listViewDataSource().cloneWithRows([]),
      page: 2,
    };
  }

  componentWillMount() {
    this.props.fetchNotifications(1);
  }

  componentWillReceiveProps(newProps: Props) {
    if (newProps.notifications) {
      let ds = listViewDataSource();
      let notificationArray = Array.from(newProps.notifications.values());
      this.setState({notificationDataSource: ds.cloneWithRows(notificationArray)});
    }
  }

  render() {
    let {isNotificationLoading, guestMode, onPress} = this.props;
    let {notificationDataSource} = this.state;
    if (guestMode) {
      return <GuestModeInfoContainer />;
    }
    let content = (
      <ListView
        showsVerticalScrollIndicator={false}
        dataSource={notificationDataSource}
        onEndReached={this._fetchMoreNotifications}
        onEndReachedThreshold={10}
        enableEmptySections={true}
        renderRow={(item) => <NotifItem key={item.id} onPress={onPress} {...item} />}
        renderFooter={this._renderFooter}
      />
    );
    if (isNotificationLoading && notificationDataSource.getRowCount() === 0) {
      content = (
        <LoadingIndicator />
      );
    }
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Pemberitahuan" />
        {content}
      </View>
    );
  }

  _renderFooter() {
    let {isNotificationLoading} = this.props;
    let {notificationDataSource} = this.state;
    if (isNotificationLoading && notificationDataSource.getRowCount() > 0) {
      return (
        <LoadingIndicator />
      );
    } else {
      return null;
    }
  }

  _fetchMoreNotifications() {
    let {fetchNotifications} = this.props;
    let {notificationDataSource, page} = this.state;
    if (!(notificationDataSource.getRowCount() < (page - 1) * 12)) {
      fetchNotifications(page);
      this.setState({page: page + 1});
    }
  }
}

type ItemProps = $Subtype<Notification>;

function NotifItem(props: ItemProps) {
  let {message, type, date, userDetail, onPress} = props;
  let userAvatar;
  let firstName;
  if (userDetail) {
    let {avatar, avatarFb} = userDetail;
    firstName = userDetail.firstName;
    userAvatar = avatar ? {uri: avatar} :
      avatarFb ? {uri: avatarFb} : defaultAvatar;
  }
  let actions = [];
  switch (type) {
    case 'like comment':
    case 'like thread':
    case 'comment': {
      let {threadDetail} = props;
      let threadID = threadDetail ? threadDetail.id : 0;
      actions.push({type: 'THREAD_SELECTED', threadID});
      actions.push({type: 'FETCH_THREAD_DETAIL_REQUESTED', threadID});
      actions.push({type: 'FETCH_COMMENTS_REQUESTED', threadID, page: 1});
      actions.push({type: 'PUSH_ROUTE', key: 'threadDetail'});
      break;
    }
    case 'review': {
      let {productDetail} = props;
      let productID = productDetail ? productDetail.id : 0;
      actions.push({type: 'FETCH_PRODUCT_DETAIL_REQUESTED', productID});
      actions.push({type: 'FETCH_PRODUCT_REVIEW_REQUESTED', productID});
      actions.push({type: 'PUSH_ROUTE', key: 'productDetail'});
      break;
    }
    case 'accept-invite':
    case 'accept-request': {
      let {groupDetail} = props;
      let groupID = groupDetail ? groupDetail.id : 0;
      actions.push({type: 'GROUP_SELECTED', groupID});
      actions.push({type: 'FETCH_GROUP_DETAIL_REQUESTED', groupID});
      actions.push({type: 'FETCH_GROUP_MEMBERS_REQUESTED', groupID});
      actions.push({type: 'FETCH_GROUP_THREADS_REQUESTED', groupID});
      actions.push({type: 'PUSH_ROUTE', key: 'groupDetail'});
      break;
    }
    case 'invite':
    case 'request': {
      actions.push({type: 'FETCH_USER_GROUP_LIST_REQUESTED'});
      actions.push({type: 'FETCH_GROUP_LIST_REQUESTED'});
      actions.push({type: 'FETCH_GROUP_INVITATION_LIST_REQUESTED'});
      actions.push({type: 'PUSH_ROUTE', key: 'thread', props: {selectedForumIdx: 'pakar', activeGroup: 'invite'}});
      break;
    }
    // TODO: handle this type of notification
    // case 'broadcast_news': {
    //   let {newsDetail} = props;
    //   break;
    // }
    default: {
      actions.push({type: 'PUSH_ROUTE', key: 'comingSoon'});
      break;
    }
  }
  let onCardPress = () => {
    onPress(actions);
  };
  return (
    <TouchableOpacity>
      <ThumbnailCard image={userAvatar} shape="round" onPress={onCardPress}>
        <View style={styles.listWrapper}>
          <View style={styles.itemWrapper}>
            <Text style={styles.userText}>{firstName}</Text>
            <Text>{date}</Text>
          </View>
          <View>
            <Text style={styles.content}>{message}</Text>
          </View>
        </View>
      </ThumbnailCard>
    </TouchableOpacity>
  );
}
