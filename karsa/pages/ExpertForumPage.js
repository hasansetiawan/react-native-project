// @flow

import React, {Component} from 'react';

import autobind from 'class-autobind';
import {
  View,
  Text,
  ListView,
} from 'react-native';

import {
  StatefulTabGroup,
  Tab,
  LoadingIndicator,
} from '../core-ui';

import ThreadGroupCard from '../core-ui/ThreadGroupCard';
import listViewDataSource from '../helpers/listViewDataSource';

import styles from './ExpertForumPage-style';

type GroupThread = {
  id: number;
  groupMemberCount: number;
  groupTopicCount: number;
  groupName: string;
  groupDescription: string;
  groupImageThumbnail: ImageSource;
  groupCategory: 'joined' | 'not-joined' | 'invite';
  statusRequest: boolean;
};

type Props = {
  groupThreads: Array<GroupThread>;
  isUserGroupLoading: boolean;
  isOtherGroupLoading: boolean;
  isInvitedGroupLoading: boolean;
  isProcessGroupLoading: boolean;
  fetchUserGroupList: () => void;
  fetchGroupList: () => void;
  fetchGroupInvitationList: () => void;
  setActiveGroupType: (groupType: string) => void;
  onGroupPress: (id: number) => void;
  onJoinPress: (id: number) => void;
  onConfirmPress: (id: number) => void;
};

type State = {
  processGroupID: number;
  userGroupsDataSource: ListView.DataSource;
  groupsDataSource: ListView.DataSource;
  groupInvitationsDataSource: ListView.DataSource;
};

export default class ExpertForumPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    let ds = listViewDataSource();
    this.state = {
      userGroupsDataSource: ds.cloneWithRows([]),
      groupsDataSource: ds.cloneWithRows([]),
      groupInvitationsDataSource: ds.cloneWithRows([]),
      processGroupID: -1,
    };
    autobind(this);
  }

  componentWillMount() {
    let {fetchUserGroupList, fetchGroupList, fetchGroupInvitationList, setActiveGroupType} = this.props;
    fetchUserGroupList();
    fetchGroupList();
    fetchGroupInvitationList();
    setActiveGroupType('joined');
  }

  componentWillReceiveProps(newProps: Props) {
    let {groupThreads} = newProps;
    if (groupThreads.length > 0) {
      let ds = listViewDataSource();
      let myGroup = groupThreads.filter((group) => group.groupCategory === 'joined');
      let otherGroup = groupThreads.filter((group) => group.groupCategory === 'not-joined');
      let invitedGroup = groupThreads.filter((group) => group.groupCategory === 'invite');
      this.setState({
        userGroupsDataSource: ds.cloneWithRows(myGroup),
        groupsDataSource: ds.cloneWithRows(otherGroup),
        groupInvitationsDataSource: ds.cloneWithRows(invitedGroup),
      });
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <StatefulTabGroup styling="secondary" onTabSelect={this._onTabSelected} scrollable>
          <Tab title={`KELOMPOK TANI SAYA`} titleSize={12}>
            <View style={[styles.container, styles.tabContainer]}>
              {this._renderMyGroups()}
            </View>
          </Tab>
          <Tab title={`DAFTAR KELOMPOK TANI`} titleSize={12}>
            <View style={[styles.container, styles.tabContainer]}>
              {this._renderOtherGroups()}
            </View>
          </Tab>
          <Tab title={`UNDANGAN KEANGGOTAN`} titleSize={12}>
            <View style={[styles.container, styles.tabContainer]}>
              {this._renderGroupInvitations()}
            </View>
          </Tab>
        </StatefulTabGroup>
      </View>
    );
  }

  _onTabSelected(index: number) {
    let {setActiveGroupType} = this.props;
    switch (index) {
      case 0: return setActiveGroupType('joined');
      case 1: return setActiveGroupType('not-joined');
      case 2: return setActiveGroupType('invite');
    }
  }

  _onJoinPress(groupID: number) {
    this.setState({processGroupID: groupID});
    this.props.onJoinPress(groupID);
  }

  _onConfirmPress(groupID: number) {
    this.setState({processGroupID: groupID});
    this.props.onConfirmPress(groupID);
  }

  _renderMyGroups() {
    let {isUserGroupLoading, onGroupPress} = this.props;
    let {userGroupsDataSource} = this.state;
    if (isUserGroupLoading) {
      return (
        <View style={[styles.container, styles.center]}>
          <LoadingIndicator />
        </View>
      );
    } else if (userGroupsDataSource.getRowCount() > 0) {
      return (
        <ListView
          contentContainerStyle={styles.tabContainer}
          dataSource={userGroupsDataSource}
          showsVerticalScrollIndicator={false}
          renderRow={(group) => (
            <View style={styles.cardContainer}>
              <ThreadGroupCard
                groupHeader={group}
                isButtonDisabled
                buttonTitle={group.statusRequest ? 'SUDAH BERGABUNG' : 'MENUNGGU KONFIRMASI'}
                onGroupPress={() => onGroupPress(group.id)}
              />
            </View>
          )}
        />
      );
    } else {
      return (
        <View style={[styles.container, styles.center]}>
          <Text style={styles.empty}>Tidak ada kelompok tani.</Text>
        </View>
      );
    }
  }

  _renderOtherGroups() {
    let {isOtherGroupLoading, isProcessGroupLoading, onGroupPress} = this.props;
    let {groupsDataSource, processGroupID} = this.state;
    if (isOtherGroupLoading) {
      return (
        <View style={[styles.container, styles.center]}>
          <LoadingIndicator />
        </View>
      );
    } else if (groupsDataSource.getRowCount() > 0) {
      return (
        <ListView
          contentContainerStyle={styles.tabContainer}
          dataSource={groupsDataSource}
          showsVerticalScrollIndicator={false}
          renderRow={(group) => (
            <View style={styles.cardContainer}>
              <ThreadGroupCard
                groupHeader={group}
                buttonLoading={isProcessGroupLoading && processGroupID === group.id}
                onButtonPress={() => this._onJoinPress(group.id)}
                onGroupPress={() => onGroupPress(group.id)}
              />
            </View>
           )}
        />
      );
    } else {
      return (
        <View style={[styles.container, styles.center]}>
          <Text style={styles.empty}>Tidak ada kelompok tani.</Text>
        </View>
      );
    }
  }

  _renderGroupInvitations() {
    let {isInvitedGroupLoading, isProcessGroupLoading, onGroupPress} = this.props;
    let {groupInvitationsDataSource, processGroupID} = this.state;
    if (isInvitedGroupLoading) {
      return (
        <View style={[styles.container, styles.center]}>
          <LoadingIndicator />
        </View>
      );
    } else if (groupInvitationsDataSource.getRowCount() > 0) {
      return (
        <ListView
          contentContainerStyle={styles.tabContainer}
          dataSource={groupInvitationsDataSource}
          showsVerticalScrollIndicator={false}
          renderRow={(group) => (
            <View style={styles.cardContainer}>
              <ThreadGroupCard
                groupHeader={group}
                buttonTitle="KONFIRMASI"
                buttonLoading={isProcessGroupLoading && processGroupID === group.id}
                onGroupPress={() => onGroupPress(group.id)}
                onButtonPress={() => this._onConfirmPress(group.id)}
              />
            </View>
          )}
        />
      );
    } else {
      return (
        <View style={[styles.container, styles.center]}>
          <Text style={styles.empty}>Tidak ada kelompok tani.</Text>
        </View>
      );
    }
  }
}
