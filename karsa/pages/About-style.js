//@flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
  },
  title: {
    flex: 0,
    padding: 10,
  },
  titleText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  article: {
    flex: 1,
    padding: 10,
  },
  articleText: {
    fontSize: 14,
  },
  photoWrap: {
    height: 120,
    margin: 8,
  },
});
