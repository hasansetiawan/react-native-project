//@flow

import {
  StyleSheet,
  Platform,
} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    justifyContent: 'space-between',
    backgroundColor: 'white',
    padding: 15,
    paddingBottom: (Platform.OS === 'ios') ? 15 : 50,
  },
  formContainer: {
  },
  field: {
  },
  forgot: {
    marginBottom: 5,
  },
  labelForgot: {
    color: '#19c80b',
  },
  divider: {
    marginVertical: 5,
    flexDirection: 'row',
    alignItems: 'center',
  },
  hr: {
    borderBottomWidth: 1,
    borderBottomColor: '#cacfcc',
    flex: 1,
  },
  textDivider: {
    textAlign: 'center',
    marginHorizontal: 10,
  },
  centeredButton: {
    textAlign: 'center',
  },
  photoContainer: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  photoWrap: {
    height: 100,
    width: 100,
  },
  error: {
    color: '#b53006',
  },
});
