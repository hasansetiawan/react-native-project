// @flow

import React from 'react';
import styles from './CultivationPage-style';

import {View} from 'react-native';
import {TileButton, TitleBar} from '../core-ui';
import {LIGHTER_GREEN, DARK_GREEN} from '../constants/color';

import {MyPlants, Seed} from '../core-image';

type Props = {
  navigateToWeeklyGuidance: () => void;
  navigateToPlantingGuidance: () => void;
  fetchPlantingGuides: () => void;
};

export default function CultivationPage(props: Props) {
  let desc = [
    'Berisi informasi mingguan untuk tanaman anda.',
    'Informasi mengenai penanaman dan perawatan berbagai jenis tanaman.',
  ];
  let {navigateToWeeklyGuidance, navigateToPlantingGuidance, fetchPlantingGuides} = props;
  let plantingGuidanceHandler = () => {
    fetchPlantingGuides();
    navigateToPlantingGuidance();
  };
  return (
    <View style={styles.root}>
      <TitleBar title="Teknis Budidaya" iconButtonName="menu" />
      <View style={styles.wrapper}>
        <View style={styles.flex}>
          <TileButton
            isHorizontal={true}
            onPress={navigateToWeeklyGuidance}
            tileName="Panduan Mingguan"
            tileDescription={desc[0]}
            image={MyPlants}
            colorStyle={{backgroundColor: LIGHTER_GREEN, borderColor: '#008D39'}}
          />
        </View>
        <View style={styles.flex}>
          <TileButton
            isHorizontal={true}
            onPress={plantingGuidanceHandler}
            tileName="Panduan Penanaman"
            tileDescription={desc[1]}
            image={Seed}
            colorStyle={{backgroundColor: DARK_GREEN, borderColor: '#2B7800'}}
          />
        </View>
      </View>
    </View>
  );
}
