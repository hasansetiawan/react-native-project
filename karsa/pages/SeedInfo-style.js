// @flow

import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

let rowWrapper = {
  flexDirection: 'row',
};

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  rowWrapper,
  headerRow: {
    ...rowWrapper,
    alignItems: 'center',
    backgroundColor: PRIMARY_GREEN,
    justifyContent: 'space-between',
    paddingHorizontal: 20,
  },
  headerText: {
    color: '#fff',
    fontSize: 15,
    marginBottom: 10,
  },
  filterFormWrapper: {
    ...rowWrapper,
    paddingHorizontal: 20,
  },
  filterForm: {
    flex: 1,
  },
  headerDropdown: {
    borderBottomWidth: 0,
  },
  headerDropdownText: {
    color: '#fff',
    flex: 0,
  },
  listWrapper: {
    flex: 1,
  },
  bannerImage: {
    height: 100,
  },
});
