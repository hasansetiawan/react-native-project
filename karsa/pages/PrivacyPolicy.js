//@flow
import React from 'react';
import {View, ScrollView} from 'react-native';
import styles from './About-style';
import {TitleBar} from '../core-ui';
import {ContentView} from './ContentView';
import getContentFromHTML from '../selectors/getContentFromHTML';

type Props = {
  desc: string;
};

export default function PrivacyPolicy(props: Props) {
  let {desc} = props;
  let {content} = getContentFromHTML(desc);
  return (
    <View style={styles.container}>
      <TitleBar iconButtonName="menu" title="Syarat dan Ketentuan" />
      <ScrollView>
        <View style={styles.article}>
          <ContentView content={content} />
        </View>
      </ScrollView>
    </View>
  );
}
