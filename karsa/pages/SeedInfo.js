// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  Icon,
  Dropdown,
  GridView,
} from '../core-ui';

import autobind from 'class-autobind';
import styles from './SeedInfo-style';

type Product = {
  picture: number;
  name: string;
  priceRange: string;
  sellerName: string;
  sellerLocation: string;
  onPress: () => void;
};

type GridProps = {
  items: Array<Product>;
  itemsPerRow: number;
  renderItem: () => ReactNode;
  style: StyleSet;
};

type Props = {
  gridProps: GridProps;
  karsaLogo: number;
};

import type {Value} from '../core-ui/Dropdown';
type State = {
  selectedComodityIdx: Value;
  isSettingFilter: boolean;
  filterSortIdx: Value;
};

export default class MyPage extends Component {
  state: State;
  props: Props;
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedComodityIdx: 0,
      isSettingFilter: false,
      filterSortIdx: 0,
    };
  }

  render() {
    let {selectedComodityIdx, isSettingFilter, filterSortIdx} = this.state;
    let {gridProps} = this.props;
    let comodities = ['Semua Komoditas', 'Cabe', 'Jagung', 'Tomat', 'Jeruk', 'Wortel'];
    let comodityOptions = comodities.map((comodity, index) => ({value: index, label: comodity}));
    let filterSortOptions = [
      {value: 0, label: 'Terbaru'},
      {value: 1, label: 'Terpopuler'},
    ];
    let filter;
    let filterForm;
    if (isSettingFilter) {
      filter = (
        <View style={styles.rowWrapper}>
          <TouchableOpacity onPress={this._onFilterSubmit}>
            <Icon name="check" fontSize={20} color="white" />
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this._onFilterPressed(false)}>
            <Icon name="clear" fontSize={20} color="white" />
          </TouchableOpacity>
        </View>
      );
      filterForm = (
        <View style={styles.filterFormWrapper}>
          <View style={styles.filterForm}>
            <Dropdown
              options={filterSortOptions}
              onSelect={this._onFilterSortSelected}
              selectedValue={filterSortIdx}
            />
          </View>
        </View>
      );
    } else {
      filter = (
        <TouchableOpacity onPress={() => this._onFilterPressed(true)}>
          <Text style={styles.headerText}>FILTER</Text>
        </TouchableOpacity>
      );
    }
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Bibit">
          <IconButton icon="search" />
        </TitleBar>
        <View style={styles.headerRow}>
          <View>
            <Dropdown
              options={comodityOptions}
              onSelect={this._onForumSelected}
              selectedValue={selectedComodityIdx}
              buttonStyle={styles.headerDropdown}
              textStyle={styles.headerDropdownText}
            />
          </View>
          {filter}
        </View>
        {filterForm}
        <GridView
          {...gridProps}
        />
      </View>
    );
  }

  _onFilterSortSelected(selected: Value) {
    this.setState({
      filterSortIdx: selected,
    });
  }

  _onForumSelected(selected: Value) {
    this.setState({
      selectedComodityIdx: selected,
    });
  }

  _onFilterSubmit() {
    //TODO: filter according to settings
  }

  _onFilterPressed(isSetting: boolean) {
    this.setState({
      isSettingFilter: isSetting,
    });
  }
}
