// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
  },
  infoWrapper: {
    flex: 1,
    padding: 20,
  },
  headerWrapper: {
    paddingBottom: 10,
  },
  headerText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  bullet: {
    backgroundColor: '#555',
    borderRadius: 3,
    height: 6,
    width: 6,
    marginRight: 5,
    marginTop: 6.5,
  },
  rowWrapper: {
    alignItems: 'flex-start',
    flexDirection: 'row',
  },
  textWrapper: {
    flex: 1,
    flexWrap: 'wrap',
    padding: 0,
    fontSize: 14,
  },
  accordionSpacer: {
    height: 10,
  },
  products: {
    width: 180,
  },
});
