import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  scrollWrapper: {
    padding: 10,
  },
  topMargin: {
    marginTop: 10,
  },
  bottomMostMargin: {
    marginBottom: 20,
  },
  topPadding: {
    paddingTop: 10,
  },
  bottomPadding: {
    paddingBottom: 10,
  },
  redFont: {
    color: '#cc3333',
  },
  bold: {
    fontWeight: 'bold',
  },
  photo: {
    margin: 10,
  },
  multilineTextInput: {
    flex: 1,
    textAlignVertical: 'top',
  },
  overlay: {
    padding: 3,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  cancel: {
    color: PRIMARY_GREEN,
    fontSize: 22,
  },
});
