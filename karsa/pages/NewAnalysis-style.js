//@flow

import {StyleSheet} from 'react-native';

const marginField = {
  marginLeft: 15,
  marginRight: 15,
};

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  formContainer: {
    ...marginField,
  },
  field: {
    height: 390,
  },
  fieldButton: {
    marginBottom: 10,
    flexDirection: 'row',
  },
  total: {
    height: 40,
    backgroundColor: '#e7ece7',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
    marginBottom: 10,
  },
  totalPendapatan: {
    height: 40,
    backgroundColor: '#babfba',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    padding: 3,
    marginBottom: 10,
  },
  rowContainer: {
    flexDirection: 'row',
    flex: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  textTotalLeft: {
    fontSize: 14,
    color: '#000',
    textAlign: 'left',
    flex: 1,
  },
  textTotalRight: {
    fontSize: 14,
    color: '#000',
    textAlign: 'right',
    flex: 1,
  },
  rowItem: {
    flex: 1,
  },
  rowRight: {
    flex: 0.5,
    height: 60,
  },
  disabled: {
    backgroundColor: '#d7d8d9',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
  },
});
