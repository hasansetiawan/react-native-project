//@flow
import React from 'react';
import styles from './NewThreadPage-style';
import ThreadFormContainer from '../containers/ThreadFormContainer';

import {View} from 'react-native';
import {TitleBar} from '../core-ui';

import type {ImageData} from '../types/ImageData';
import type {Value} from '../core-ui/Dropdown';

type Props = {
  onCreateThread: (categoryID: Value, subCategoryID: Value, title: string, description: string, photo: ?ImageData) => void;
};

export default function NewThreadPage(props: Props) {
  let {onCreateThread} = props;
  return (
    <View style={styles.flex}>
      <TitleBar iconButtonName="menu" title="Buat Topik Baru" />
      <ThreadFormContainer onSubmitPressed={onCreateThread} />
    </View>
  );
}
