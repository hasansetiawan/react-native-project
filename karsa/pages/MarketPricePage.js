// @flow

import React from 'react';
import {
  Tab,
  StatefulTabGroup,
  TitleBar,
  IconButton,
  Table,
  Label,
  Dropdown,
} from '../core-ui.js';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import formatNumber from '../helpers/formatNumber';

import {
  View,
  Text,
  ScrollView,
} from 'react-native';

import styles from './MarketPricePage-style';
import type {NationalPriceInfo, ProvincePriceInfo, CommodityPriceInfo} from '../types/Price';
import Chart from '../core-ui/chart';

type Props = {
  isMarketPriceLoading: boolean;
  isProvincePriceLoading: boolean;
  isCommodityPriceLoading: boolean;
  nationalPriceInfo: NationalPriceInfo;
  provincePriceInfo: ProvincePriceInfo;
  commodityPriceInfo: CommodityPriceInfo;
  currentDate: string;
  provinces: Array<string>;
  activeProvince: string;
  onProvinceSelect: (value: string | number) => void;
  activeCommodity: string;
  commodities: Array<string>;
  onCommoditySelect: (value: string | number) => void;
};

export default function MarketPricePage(props: Props) {
  let {isMarketPriceLoading} = props;
  let {isProvincePriceLoading, isCommodityPriceLoading} = props;
  let {nationalPriceInfo, currentDate} = props;
  let {provincePriceInfo, provinces, activeProvince} = props;
  let {commodityPriceInfo, commodities, activeCommodity} = props;
  let {onProvinceSelect, onCommoditySelect} = props;

  let provinceDropdownProps = {
    options: provinces.map((province) => ({
      value: province,
      label: province,
    })),
    label: 'Pilih Kota/Provinsi',
    defaultValue: '',
    onSelect: onProvinceSelect,
  };

  let commodityDropdownProps = {
    options: commodities.map((commodity) => ({
      value: commodity,
      label: commodity,
    })),
    label: 'Pilih Komoditas',
    defaultValue: '',
    onSelect: onCommoditySelect,
  };
  commodityDropdownProps.options.push({value: 0, label: 'Pilih Komoditas'});

  let commodityData = commodityPriceInfo.priceList.map((data) => ([
    data.date,
    data.price,
  ]));

  let maxPrice = commodityPriceInfo.priceList ? Math.max(...commodityPriceInfo.priceList.map((item) => item.price)) : 0;
  let minPrice = commodityPriceInfo.priceList ? Math.min(...commodityPriceInfo.priceList.map((item) => item.price)) : 0;
  let roundedMaxPrice = Math.round(maxPrice / 100) * 100;
  let roundedMinPrice = Math.round(minPrice / 100) * 100;
  let leapPrice = maxPrice === minPrice ? 100 : (roundedMaxPrice - roundedMinPrice) / 5;
  let startPriceIndex = maxPrice === minPrice ? minPrice + 100 : minPrice + leapPrice;

  if (isMarketPriceLoading) {
    return (
      <View style={styles.root}>
        <TitleBar title="Harga Pasar" iconButtonName="menu" >
          <IconButton icon="search" />
        </TitleBar>
        <LoadingIndicator />
      </View>
    );
  }

  let renderProvincePrice = () => {
    if (isProvincePriceLoading) {
      return <LoadingIndicator />;
    }

    if (provincePriceInfo.provinceName !== activeProvince) {
      return <Text style={styles.notFound}>Price List Not Found</Text>;
    }

    return (
      <Table dataSource={provincePriceInfo.priceList}>
        <Label title="#" isIndex />
        <Label title="Komoditas" />
        <Label title={'Harga Sebelum'} />
        <Label title={'Harga ' + provincePriceInfo.currDate} />
      </Table>
    );
  };

  let renderCommodityPrice = () => {
    if (isCommodityPriceLoading) {
      return <LoadingIndicator />;
    }

    if (commodityPriceInfo.commodityName !== activeCommodity) {
      return <Text style={styles.notFound}>Price Chart Not Found</Text>;
    }
    return (
      <ScrollView horizontal={true}>
        {commodityPriceInfo.priceList.length !== 0 ?
          <Chart
            fillColor="#55FF5555"
            style={styles.chart}
            data={commodityData}
          /> : null
        }
      </ScrollView>
    );
  };

  return (
    <View style={styles.root}>
      <TitleBar title="Harga Pasar" iconButtonName="menu" >
        <IconButton icon="search" />
      </TitleBar>
      <StatefulTabGroup styling="primary" scrollable>
        <Tab title="HARGA NASIONAL">
          <Text style={styles.header}>Harga Komoditas Nasional, {currentDate}</Text>
          <Text style={[styles.subHeader, styles.bottomMargin]}>Sumber Dari Kementerian Perdagangan Republik Indonesia</Text>
          <Table dataSource={nationalPriceInfo.priceList}>
            <Label title="#" isIndex />
            <Label title="Komoditas" />
            <Label title={'Harga Sebelum'} />
            <Label title={'Harga ' + nationalPriceInfo.currDate} />
          </Table>
        </Tab>
        <Tab title="HARGA PROVINSI">
          <Text style={styles.header}>Harga Komoditas {activeProvince}, {currentDate}</Text>
          <Text style={styles.subHeader}>Sumber Dari Kementerian Perdagangan Republik Indonesia</Text>
          <View style={styles.dropdownProvince}>
            <Dropdown
              {...provinceDropdownProps}
              selectedValue={activeProvince}
            />
          </View>
          {renderProvincePrice()}
        </Tab>
        <Tab title="PERKEMBANGAN HARGA">
          <View style={styles.flex}>
            <View style={styles.dropdownProvince}>
              <Dropdown
                {...commodityDropdownProps}
                selectedValue={activeCommodity ? activeCommodity : 0}
              />
            </View>
            <View style={styles.wrapper}>
              <ScrollView>
                <View style={styles.wrapperChild}>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider}>
                      <Text style={styles.wrapperLabel}>
                        {formatNumber((startPriceIndex + (4 * leapPrice)) + '', {prefix: ' ', separator: '.'})}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider}>
                      <Text style={styles.wrapperLabel}>
                        {formatNumber((startPriceIndex + (3 * leapPrice)) + '', {prefix: ' ', separator: '.'})}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider}>
                      <Text style={styles.wrapperLabel}>
                        {formatNumber((startPriceIndex + (2 * leapPrice)) + '', {prefix: ' ', separator: '.'})}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider}>
                      <Text style={styles.wrapperLabel}>
                        {formatNumber((startPriceIndex + (1 * leapPrice)) + '', {prefix: ' ', separator: '.'})}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider}>
                      <Text style={styles.wrapperLabel}>
                        {formatNumber(startPriceIndex + '', {prefix: ' ', separator: '.'})}
                      </Text>
                    </View>
                  </View>
                  <View style={styles.wrapperContainer}>
                    <View style={styles.wrapperDivider} />
                  </View>
                  <View style={styles.graphRenderer}>
                    {renderCommodityPrice()}
                  </View>
                </View>
              </ScrollView>
            </View>
          </View>
        </Tab>
      </StatefulTabGroup>
    </View>
  );
}
