// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  ListView,
} from 'react-native';

import {
  LoadingIndicator,
  TitleBar,
  TextField,
  ProductDetail,
  IconButton,
} from '../core-ui';
import ProductFilterDropdown from '../components/ProductFilterDropdownContainer';
import styles from './ProductListPage-style';
import listViewDataSource from '../helpers/listViewDataSource';
import formatPriceRange from '../helpers/formatPriceRange';
import type {Value} from '../core-ui/Dropdown';
import type {Product} from '../types/Product';

import {groupItems} from '../core-ui/GridView'; //TODO CHange this to helpers
type FetchOptions = {
  activeList: 'search' | 'filter' | 'normal';
  searchKey?: string;
  filterKey?: string | number;
};
type Props = {
  products: Array<Product>;
  fetchMoreData: (pageNumber: number, options: FetchOptions) => void;
  productCategory: string;
  isFetchingMoreData: boolean;
  onDropdownSelect: (value: Value, page: number) => void;
  onSearchPressed: (searchKey: string, page: number) => void;
  onProductPress: (id: number) => void;
};
type State = {
  searchKey: string;
  selectedCommodity: number | string;
  currentPage: number;
  activeList: 'normal' | 'search' | 'filter';
};

export default class ProductListPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      searchKey: '',
      selectedCommodity: -1,
      currentPage: 2,
      activeList: 'normal',
    };
  }
  _onSearchTextChanged(text: string) {
    this.setState({
      searchKey: text,
    });
  }
  _onDropdownSelect(value: Value) {
    let {onDropdownSelect} = this.props;
    onDropdownSelect(value, 1); // value is ID of sub category
    let active = value === -1 ? 'normal' : 'filter';
    this.setState({
      selectedCommodity: value,
      currentPage: 2,
      searchKey: '',
      activeList: active,
    });
  }
  _generateDS(items: Array<Product>) {
    let ds = listViewDataSource();
    return ds.cloneWithRows(groupItems(items, 2));
  }
  _onSearchPressed() {
    let {onSearchPressed} = this.props;
    let {searchKey} = this.state;
    onSearchPressed(searchKey, 1);
    this.setState({
      currentPage: 2,
      selectedCommodity: -1,
      activeList: 'search',
    });
  }
  _renderItem(products: Array<Product>, onPress: (id: number) => void) {
    let productRow = products.map((product, index) => {
      let {
        name, priceMin, priceMax,
        photo, producerName, producerCity, id,
      } = product;
      let productDetailProps = {
        name,
        priceRange: formatPriceRange(priceMin, priceMax),
        photo,
        producerName,
        producerCity,
        type: 'product',
        onPress: () => onPress(id),
        key: index,
      };
      return (
        <ProductDetail
          {...productDetailProps}
        />
      );
    });
    if (productRow.length % 2 !== 0) {
      productRow.push(<View key={productRow.length} style={{flex: 1}} />);
    }
    return (
      <View style={{flexDirection: 'row'}}>
        {productRow}
      </View>
    );
  }
  _fetchMoreData() {
    let {products, fetchMoreData} = this.props;
    let {currentPage} = this.state;
    let options = {
      activeList: this.state.activeList,
      filterKey: this.state.selectedCommodity,
      searchKey: this.state.searchKey,
    };
    if (!(products.length < (currentPage - 1) * 12 / 2)) {
      fetchMoreData(this.state.currentPage, options); // Dispatch request allproductinfo
      let temp = this.state.currentPage;
      this.setState({
        currentPage: ++temp,
      });
    }
  }
  render() {
    let {searchKey, selectedCommodity} = this.state;
    let {products, isFetchingMoreData, productCategory, onProductPress} = this.props;
    return (
      <View style={styles.container}>
        <TitleBar iconButtonName="menu" title="Info Sarana Tani Lihat Semua" />
        <View style={styles.utilityWrapper}>
          <View style={styles.searchContainer}>
            <View style={styles.searchTextContainer}>
              <TextField placeholder="Cari Berdasarkan Kata Kunci" value={searchKey} onChangeText={this._onSearchTextChanged} />
            </View>
            <IconButton icon="search" style={styles.searchButton} onPress={this._onSearchPressed} />
          </View>
          <ProductFilterDropdown
            label="Pilih Komoditas"
            onSelect={(selectedValue) => this._onDropdownSelect(selectedValue)}
            selectedValue={selectedCommodity}
          />
          <Text style={styles.headerText}>Semua produk {productCategory}</Text>
        </View>
        {products != null ?
          <ListView
            style={{flex: 1}}
            dataSource={this._generateDS(products)}
            renderRow={(item) => this._renderItem(item, onProductPress)}
            enableEmptySections={true}
            onEndReachedThreshold={100}
            onEndReached={() => this._fetchMoreData()}
            renderFooter={() => {
              if (isFetchingMoreData) {
                return <LoadingIndicator />;
              } else if (!isFetchingMoreData && products.length === 0) {
                return <Text style={styles.noProductText}>Data Produk Tidak Ada</Text>;
              }
            }}
          /> : <Text>Produk Tidak Ditemukan</Text>
        }

      </View>
    );
  }
}
