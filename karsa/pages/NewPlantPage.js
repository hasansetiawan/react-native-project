//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  ScrollView,
  Text,
  DatePickerAndroid,
  ToastAndroid,
} from 'react-native';
import {
  TitleBar,
  TextField,
  Dropdown,
  Button,
} from '../core-ui';
import styles from './NewPlantPage-style';
import NewPlantPage2 from './NewPlantPage2';
import dateStringToLocale from '../helpers/dateStringToLocale';
import type {Value, Option} from '../core-ui/Dropdown';
import type {Coordinates, GPSAddress, Location} from '../types/Location';
import type {GalleryData} from '../types/Plant';

type State = {
  isFirstPage: boolean;
  plantType: Value;
  plantCategory: Value;
  plantVariant: Value;
  plantingMethod: Value;
  plantingMethodLabel: string;
  fieldAltitude: Value;
  plantingPhase: Value;
  plantingArea: number;
  plantingRecommendation: string;
  date: Date;
  soilPH: Value;
  selectedRadio: string;
  currentPosition: Coordinates;
  location: Location;
  information: string;
};

type Props = {
  dropdownOptions: {
    plantTypesOptions: Array<Option>;
    plantCategoriesOptions: Array<Option>;
    plantVarietiesOptions: Array<Option>;
    plantingMethodsOptions: Array<Option>;
    fieldAltitudeOptions: Array<Option>;
    plantingPhaseOptions: Array<Option>;
    soilPHOptions: Array<Option>;
  };
  plantingRecommendation: string;
  gpsAddress: GPSAddress;
  newPhoto: GalleryData;
  userEmail: string;

  dropdownOnSelect: {
    onPlantTypeSelect: () => void;
    onPlantCategorySelect: () => void;
  };
  onDismissPage: () => void;
  fetchPlantingRecommendation: (
    plantType: Value,
    plantCategory: Value,
    plantVariant: Value,
    fieldAltitude: Value,
  ) => void;
  fetchGPSAddress: (coordinates: Coordinates) => void;
  navigateTo: () => () => void;
  fetchProvinceList: () => void;
  onPictureSelect: () => void;
  submitNewPlant: () => void;

};

export default class NewPlantPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isFirstPage: true,
      plantType: 0,
      plantCategory: 0,
      plantVariant: 0,
      plantingMethod: 0,
      plantingMethodLabel: '',
      fieldAltitude: '',
      plantingPhase: 0,
      plantingArea: 0,
      plantingRecommendation: '',
      date: new Date(),
      soilPH: '',
      selectedRadio: '',
      currentPosition: {longitude: null, latitude: null},
      location: {},
      information: '',
    };
  }

  componentWillReceiveProps(newProps: Props) {
    let oldProps = this.props;
    let {plantingRecommendation} = newProps;
    if (oldProps.plantingRecommendation !== plantingRecommendation) {
      this.setState({plantingRecommendation});
    }
  }

  render() {
    let { // THIS IS STATES
      isFirstPage,
      plantType,
      plantCategory,
      plantVariant,
      plantingMethod,
      plantingMethodLabel,
      fieldAltitude,
      plantingPhase,
      plantingRecommendation,
      date,
      soilPH,
      selectedRadio,
      currentPosition,
    } = this.state;
    let { // THIS IS PROPS
      dropdownOptions,
      gpsAddress,
      onPictureSelect,
      newPhoto,
      onDismissPage,
    } = this.props;
    let { // THIS IS DROPDOWN OPTIONS
      fieldAltitudeOptions,
      plantingPhaseOptions,
      plantTypesOptions,
      plantCategoriesOptions,
      plantVarietiesOptions,
      plantingMethodsOptions,
      soilPHOptions,
    } = dropdownOptions;
    let { // THIS IS THIS
      _onPlantTypeSelect,
      _onPlantCategorySelect,
      _onPlantingMethodSelect,
      _onPlantVariantSelect,
      _onFieldAltitudeSelect,
      _switchPage,
      _onGPSLocationSelect,
      _getLocationDropdownData,
      _onDropdownLocationSelect,
      _onPlantingAreaChange,
      _onInformationChange,
      _submitNewPlant,
    } = this;

    let emptyPlantTypes = 'Data tipe tanaman belum tersedia';
    let emptyPlantCategories = 'Data kategori tanaman belum tersedia';
    let emptyPlantVarieties = 'Data varietas tanaman belum tersedia';
    let emptyPlantingMethods = 'Data metode penanaman belum tersedia';

    let datePickerHandler = () => {
      this._showDatePicker({date: Date.now(), mode: 'calendar'});
    };


    let onNextPress = () => {
      _switchPage(2, plantingMethodLabel);
    };

    let body = (isFirstPage) ?
      (
        <ScrollView style={styles.firstContent}>
          <View style={styles.body}>
            <Dropdown
              label="Jenis Tanaman"              // TODO: use localization
              placeholder="Pilih jenis tanaman"
              options={plantTypesOptions}
              emptyOptionsText={emptyPlantTypes}
              animationType="none"
              onSelect={_onPlantTypeSelect}
              selectedValue={plantType}
            />
            <Dropdown
              label="Kategori Tanaman"
              placeholder="Pilih kategori tanaman"
              options={plantCategoriesOptions}
              emptyOptionsText={emptyPlantCategories}
              animationType="none"
              onSelect={_onPlantCategorySelect}
              selectedValue={plantCategory}
            />
            <Dropdown
              label="Metode Penanaman"
              placeholder="Pilih metode"
              options={plantingMethodsOptions}
              emptyOptionsText={emptyPlantingMethods}
              animationType="none"
              onSelect={_onPlantingMethodSelect}
              selectedValue={plantingMethod}
            />
            <Dropdown
              label="Varietas Tanaman"
              placeholder="Pilih varietas tanaman"
              options={plantVarietiesOptions}
              emptyOptionsText={emptyPlantVarieties}
              animationType="none"
              onSelect={_onPlantVariantSelect}
              selectedValue={plantVariant}
            />
            <Dropdown
              label="Ketinggian Dataran Lahan"
              placeholder="Pilih ketinggian dataran lahan"
              options={fieldAltitudeOptions}
              animationType="none"
              onSelect={_onFieldAltitudeSelect}
              selectedValue={fieldAltitude}
            />
          </View>
          <View style={[styles.recommendation, plantingRecommendation === 'Tidak Direkomendasikan' ? {backgroundColor: 'red'} : null]}>
            <Text>Rekomendasi Tanam: </Text>
            <Text style={styles.recommendationText}>
              {plantingRecommendation}
            </Text>
          </View>
          <View style={styles.body}>
            <Dropdown
              label="Fase Tanam"
              placeholder="Pilih fase tanam"
              options={plantingPhaseOptions}
              animationType="none"
              onSelect={(plantingPhase) => this.setState({plantingPhase})}
              selectedValue={plantingPhase}
            />
            {
              plantingMethodLabel === 'Polybag' ?
              null :
              (
                <View style={styles.fieldArea}>
                  <View style={styles.fieldAreaInput}>
                    <TextField
                      label="Luas Lahan"
                      placeholder="Masukkan luas lahan"
                      onChangeText={_onPlantingAreaChange}
                      keyboardType="numeric"
                    />
                  </View>
                  <View style={styles.fieldAreaUnit}>
                    <Text style={styles.fieldAreaMeter}>m</Text>
                    <Text style={styles.fieldAreaExponent}>2</Text>
                  </View>
                </View>
              )
            }
            <View style={styles.dateContainer}>
              <Text style={styles.inputLabel}>Tanggal Tanam</Text>
              <View style={styles.dateField}>
                <Text style={styles.dateText}>{dateStringToLocale(date)}</Text>
                <Button
                  text="Pilih Tanggal"
                  onPress={datePickerHandler}
                  style={styles.datePickerButton}
                />
              </View>
            </View>
          </View>
          <View style={styles.buttons}>
            <Button
              text="BATAL"
              onPress={onDismissPage}
              inverted
            />
            <Button
              text="LANJUTKAN"
              onPress={onNextPress}
            />
          </View>
        </ScrollView>
      ) : (
        <NewPlantPage2
          soilPH={soilPH}
          soilPHOptions={soilPHOptions}
          onSoilPHSelect={(soilPH) => this.setState({soilPH})}
          selectedRadio={selectedRadio}
          onGPSSelect={_onGPSLocationSelect}
          onManualInputSelect={_onDropdownLocationSelect}
          switchPage={_switchPage}
          currentPosition={currentPosition}
          gpsAddress={gpsAddress}
          getLocationDropdownData={_getLocationDropdownData}
          newPhoto={newPhoto}
          onPictureSelect={onPictureSelect}
          onInformationChange={_onInformationChange}
          submitNewPlant={_submitNewPlant}
        />
      );

    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="Tambah Tanaman Baru" />
        {body}
      </View>
    );
  }

  async _showDatePicker(options) {
    try {
      const {action, year, month, day} = await DatePickerAndroid.open(options);
      if (action === DatePickerAndroid.dismissedAction) {
      } else {
        let selectedDate = new Date(year, month, day);
        this.setState({
          date: selectedDate,
        });
      }
    } catch (error) {
      throw new Error(error);
    }
  }

  _onPlantTypeSelect(plantType) {
    this.setState({plantType: Number(plantType), plantCategory: 0, plantVariant: 0, plantingMethod: 0, plantingRecommendation: ''});
    this.props.dropdownOnSelect.onPlantTypeSelect(plantType);
  }

  _onPlantCategorySelect(plantCategory) {
    this.setState({plantCategory: Number(plantCategory), plantVariant: 0, plantingMethod: 0, plantingRecommendation: ''});
    let {plantType} = this.state;
    this.props.dropdownOnSelect.onPlantCategorySelect(plantType, plantCategory);
  }

  _onPlantingMethodSelect(plantingMethod) {
    this.setState({plantingMethod});
    let {plantingMethodsOptions} = this.props.dropdownOptions;
    let selectedPlantingMethodLabel = plantingMethodsOptions ?
    plantingMethodsOptions.filter((option) => {
      return option.value === plantingMethod;
    })[0].label :
    '';
    this.setState({plantingMethodLabel: selectedPlantingMethodLabel});
  }

  _onPlantVariantSelect(plantVariant) {
    this.setState({plantVariant: Number(plantVariant), plantingRecommendation: ''});
    let {plantType, plantCategory, fieldAltitude} = this.state;
    this._fetchPlantingRecommendation(plantType, plantCategory, plantVariant, fieldAltitude);
  }

  _onFieldAltitudeSelect(fieldAltitude) {
    this.setState({fieldAltitude, plantingRecommendation: ''});
    let {plantType, plantCategory, plantVariant} = this.state;
    this._fetchPlantingRecommendation(plantType, plantCategory, plantVariant, fieldAltitude);
  }

  _onPlantingAreaChange(input: number) {
    this.setState({plantingArea: Number(input)});
  }

  _fetchPlantingRecommendation(plantType, plantCategory, plantVariant, fieldAltitude) {
    if (fieldAltitude && plantVariant) {
      this.props.fetchPlantingRecommendation(plantType, plantCategory, plantVariant, fieldAltitude);
    }
  }

  _onGPSLocationSelect() {
    let {fetchGPSAddress} = this.props;
    this.setState({selectedRadio: 'gps'});

    navigator.geolocation.getCurrentPosition(
      (position) => {
        let currentPosition = {
          longitude: position.coords.longitude,
          latitude: position.coords.latitude,
        };
        this.setState({currentPosition});
        fetchGPSAddress(currentPosition);
      },
      () => {
        ToastAndroid.show('Gagal Mendapatkan Lokasi GPS. Silakan Coba sesaat lagi.', ToastAndroid.SHORT);
      },
      {enableHighAccuracy: false, timeout: 5000, maximumAge: 10000},
    );
  }

  _onDropdownLocationSelect() {
    this.setState({selectedRadio: 'manual'});
  }

  _switchPage(pageNumber: number) {
    let {plantType, plantCategory, plantingMethod, plantVariant} = this.state;
    let {fieldAltitude, plantingPhase, plantingArea} = this.state;
    let {plantingMethodsOptions} = this.props.dropdownOptions;
    let selectedPlantingMethodLabel = plantingMethodsOptions ?
    plantingMethodsOptions.filter((option) => {
      return option.value === plantingMethod;
    })[0].label :
    '';

    if (pageNumber === 1) {
      this.setState({isFirstPage: true});
    } else if (selectedPlantingMethodLabel === 'Polybag') {
      if (plantType && plantCategory && plantingMethod && plantVariant && fieldAltitude && plantingPhase) {
        this.setState({isFirstPage: false});
        this.props.fetchProvinceList();
      } else {
        ToastAndroid.show('Mohon lengkapi data sebelum melanjutkan ke halaman selanjutnya', ToastAndroid.SHORT);
      }
    } else {
      if (plantType && plantCategory && plantingMethod && plantVariant && fieldAltitude && plantingPhase && plantingArea) {
        this.setState({isFirstPage: false});
        this.props.fetchProvinceList();
      } else {
        ToastAndroid.show('Mohon lengkapi data sebelum melanjutkan ke halaman selanjutnya', ToastAndroid.SHORT);
      }
    }
  }

  _getLocationDropdownData(location: Location) {
    this.setState({location});
  }

  _onInformationChange(input: string) {
    this.setState({information: input});
  }

  _submitNewPlant() {
    let {date, plantType, plantCategory, plantVariant, soilPH, plantingRecommendation, plantingArea} = this.state;
    let {information, plantingMethod, plantingPhase, fieldAltitude, selectedRadio, location} = this.state;
    let {userEmail, newPhoto, gpsAddress} = this.props;
    if (soilPH && (gpsAddress.lat || location.province)) {
      let formattedDate = date.getFullYear() + '-' + date.getMonth() + '-' + date.getDate();
      let plant = {
        email: userEmail,
        plant_id: plantType,
        plant_cat_id: plantCategory,
        plant_var_id: plantVariant,
        plant_ph: soilPH,
        plant_recomend: plantingRecommendation,
        plant_date: formattedDate,
        plant_area: plantingArea,
        plant_unit: 'm2',
        plant_desc: information,
        plant_system: plantingMethod,
        plant_fase: plantingPhase,
        plant_height: fieldAltitude,
        photo: newPhoto.tempImage,
      };

      let plantData = (selectedRadio === 'gps') ? {
        ...plant,
        plant_long: gpsAddress.long,
        plant_lat: gpsAddress.lat,
        plant_address_gps: gpsAddress.fullAddress,
        province: 0,
        city: 0,
        subdistrict: 0,
        village: 0,
      } : {
        ...plant,
        province: location.province ? location.province : 0,
        city: location.city ? location.city : 0,
        subdistrict: location.subdistrict ? location.subdistrict : 0,
        village: location.village ? location.village : 0,
      };
      this.props.submitNewPlant(plantData);
    } else {
      ToastAndroid.show('Mohon lengkapi data sebelum menyimpan tanaman baru', ToastAndroid.SHORT);
    }
  }
}
