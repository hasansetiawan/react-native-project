// @flow

import React, {Component} from 'react';
import {
  View,
  ListView,
} from 'react-native';
import autobind from 'class-autobind';

import {
  ProductDetail,
  TitleBar,
  LoadingIndicator,
} from '../core-ui';
import styles from './StoreProductsPage-style';
import {groupItems} from '../core-ui/GridView';
import listViewDataSource from '../helpers/listViewDataSource';
import formatPriceRange from '../helpers/formatPriceRange';
import noImage from '../images/no_image.png';

import type {Product} from '../types/Product';

type Props = {
  products: Array<Product>;
  fetchMoreData: (page: number) => void;
  isFetchingMoreData: boolean;
  onProductPress: (id: number) => void;
  isLoading: boolean;
};
type State = {
  currentPage: number;
};

export default class StoreProductList extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      currentPage: 2,
    };
  }
  _generateDS(items: Array<Product>) {
    let ds = listViewDataSource();
    return ds.cloneWithRows(groupItems(items, 2));
  }
  _renderItem(products: Array<Product>, onPress: (id: number) => void) {
    let productRow = products.map((product, index) => {
      let {
        name, priceMin, priceMax,
        photo, producerName, producerCity, id,
      } = product;
      let tempPhoto;
      if (typeof photo === 'object') {
        tempPhoto = photo;
      } else {
        tempPhoto = photo && typeof photo === 'string' ? {uri: photo} : noImage;
      }
      let productDetailProps = {
        name,
        priceRange: formatPriceRange(priceMin, priceMax),
        photo: tempPhoto,
        producerName,
        producerCity,
        type: 'product',
        onPress: () => onPress(id),
        key: index,
      };
      return (
        <ProductDetail
          {...productDetailProps}
        />
      );
    });
    if (productRow.length % 2 !== 0) {
      productRow.push(<View key={productRow.length} style={{flex: 1}} />);
    }
    return (
      <View style={{flexDirection: 'row'}}>
        {productRow}
      </View>
    );
  }
  _fetchMoreData() {
    let {currentPage} = this.state;
    let {products} = this.props;
    if (!(products.length < (currentPage - 1) * 3 / 2)) {
      this.props.fetchMoreData(currentPage); // Store ID get from state + token
      let temp = currentPage++;
      this.setState({
        currentPage: temp,
      });
    }
  }
  render() {
    let {products, onProductPress, isFetchingMoreData, isLoading} = this.props;
    return (
      <View style={styles.container}>
        <TitleBar title="Produk Toko Pertanian Terdekat" />
        {isLoading ? <LoadingIndicator /> : (
          <ListView
            style={{flex: 1}}
            dataSource={this._generateDS(products)}
            renderRow={(item) => this._renderItem(item, onProductPress)}
            enableEmptySections={true}
            onEndReachedThreshold={100}
            onEndReached={() => this._fetchMoreData()}
            renderFooter={() => isFetchingMoreData ? <LoadingIndicator /> : null}
          />
        )}
      </View>
    );
  }
}
