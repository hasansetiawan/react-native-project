//@flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View, Text, Linking, ToastAndroid, TouchableOpacity} from 'react-native';
import {Icon, LoadingIndicator} from '../core-ui';
import styles from './ContactAddress-style';
import type {Value} from '../core-ui/Dropdown';

export type Office = {
  name: string;
  address: string;
  city: string;
  phone: string;
  email: string;
};

type Props = {
  mainOffice: Office;
  isLoading?: boolean;
};

type State = {
  selectedValue: Value;
};

export default class ContactAddress extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedValue: 1,
    };
  }

  render() {
    let {mainOffice, isLoading} = this.props;
    if (isLoading) {
      return (
        <View style={[styles.flex, styles.center]}>
          <LoadingIndicator />
        </View>
      );
    }

    let mainOfficePhone = mainOffice.phone.indexOf('tidak tersedia') === -1 ? (
      <TouchableOpacity onPress={() => this._openLink(`tel:${mainOffice.phone.split('ext')[0]}`)}>
        <View style={styles.textWithIcon}>
          <Icon name="phone" style={styles.icon} />
          <Text style={[styles.contact, styles.greenText]}>{mainOffice.phone}</Text>
        </View>
      </TouchableOpacity>
    ) : (
      <View style={styles.textWithIcon}>
        <Icon name="phone" style={styles.icon} />
        <Text style={styles.contact}>{mainOffice.phone}</Text>
      </View>
    );
    let mainOfficeEmail = mainOffice.email.indexOf('tidak tersedia') === -1 ? (
      <TouchableOpacity onPress={() => this._openLink(`mailto:${mainOffice.email}`)}>
        <View style={styles.textWithIcon}>
          <Icon name="email" style={styles.icon} />
          <Text style={[styles.contact, styles.greenText]}>{mainOffice.email}</Text>
        </View>
      </TouchableOpacity>
    ) : (
      <View style={styles.textWithIcon}>
        <Icon name="email" style={styles.icon} />
        <Text style={styles.contact}>{mainOffice.email}</Text>
      </View>
    );

    return (
      <View style={styles.mainContainer}>
        <View style={styles.formContainer}>
          <View style={styles.field}>
            <Text style={styles.headerText}>{mainOffice.name}</Text>
            <View style={styles.textWithIcon}>
              <Icon name="location-on" style={[styles.icon, {paddingTop: 3}]} />
              <Text style={styles.addressText}>{mainOffice.address}</Text>
            </View>
            {mainOfficePhone}
            {mainOfficeEmail}
          </View>
          <View style={styles.divider}>
            <View style={styles.hr}></View>
          </View>
        </View>
      </View>
    );
  }

  _openLink(url) {
    Linking.canOpenURL(url).then((supported) => {
      if (!supported) {
        ToastAndroid.show(`tidak bisa membuka ${url}`, ToastAndroid.SHORT);
      } else {
        return Linking.openURL(url);
      }
    });
  }

  _onSelect(item: Value): void {
    this.setState({
      selectedValue: item,
    });
  }
}
