import {StyleSheet, Dimensions} from 'react-native';
import {ALTERNATE_GREY} from '../constants/color';

const {width, height} = Dimensions.get('window');

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  flexRow: {
    flexDirection: 'row',
  },
  overlay: {
    backgroundColor: 'rgba(0, 0, 0, 0.6)',
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  dialogWidth: {
    width: 0.8 * width,
  },
  dialogHeight: {
    height: 0.8 * height,
  },
  dialogSmallerHeight: {
    height: 0.6 * height,
  },
  footer: {
    backgroundColor: ALTERNATE_GREY,
    padding: 5,
    elevation: 100,
  },
  buttonFix: {
    height: 40,
    minHeight: null,
    maxHeight: null,
  },
  zeroPadding: {
    padding: 0,
  },
  padding: {
    padding: 20,
  },
  smallPadding: {
    padding: 5,
  },
  smallVerticalPadding: {
    paddingVertical: 5,
  },
  verticalPadding: {
    paddingVertical: 10,
  },
  bottomMargin: {
    marginBottom: 10,
  },
  header: {
    fontSize: 20,
  },
  slightlyBiggerFont: {
    fontSize: 14,
  },
  biggerFont: {
    fontSize: 17,
  },
  greenFont: {
    color: 'green',
  },
  bold: {
    fontWeight: 'bold',
  },
  photo: {
    height: 150,
  },
  photoWidth: {
    width: (0.8 * width) - 40,
  },
  photoThumbnail: {
    width: 60,
    height: 60,
    marginRight: 5,
  },
});
