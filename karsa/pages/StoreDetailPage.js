// @flow

import React, {Component} from 'react';
import {
  View,
  Image,
  Text,
  ScrollView,
  ListView,
  Linking,
  ToastAndroid,
  TouchableOpacity,
} from 'react-native';
import autobind from 'class-autobind';

import styles from './StoreDetailPage-style';
import {
  TitleBar,
  StatefulTabGroup,
  Tab,
  Icon,
  Button,
  LoadingIndicator,
  ProductDetail,
  IconButton,
} from '../core-ui';
import formatPriceRange from '../helpers/formatPriceRange';
import listViewDataSource from '../helpers/listViewDataSource';
import {groupItems} from '../core-ui/GridView'; //TODO CHange this to helpers
import noImage from '../images/no_image.png';

import type {Toko} from '../types/NearbyStore';
import type {Product} from '../types/Product';

type Props = {
  isLoading: boolean;
  store: Toko;
  products: Array<Product>;
  navigateToStoreProducts: () => void;
  onProductPress: (id: number) => void;
};
type State = {
  activePage: number;
};
function NoProductText() {
  return (
    <View style={{margin: 20, alignItems: 'center', justifyContent: 'center'}}>
      <Text style={{fontWeight: '700', fontSize: 14}}>Toko Ini Tidak Memiliki Produk</Text>
    </View>
  );
}
export default class StoreDetailPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      activePage: 3,
    };
  }
  _linkOpen(url) {
    Linking.canOpenURL(url).then((supported) => {
      if (!supported) {
        ToastAndroid.show(`tidak bisa membuka ${url}`, ToastAndroid.SHORT);
      } else {
        return Linking.openURL(url);
      }
    });
  }
  _generateDS(items: Array<Product>) {
    let ds = listViewDataSource();
    return ds.cloneWithRows(groupItems(items, 2));
  }
  _renderItem(products: Array<Product>, onPress: (id: number) => void) {
    let productRow = products.map((product) => {
      let {
        name, priceMin, priceMax,
        photo, producerName, producerCity, id,
      } = product;
      let tempPhoto;
      if (typeof photo === 'object') {
        tempPhoto = photo;
      } else {
        tempPhoto = photo && typeof photo === 'string' ? {uri: photo} : noImage;
      }
      let productDetailProps = {
        name,
        priceRange: formatPriceRange(priceMin, priceMax),
        photo: tempPhoto,
        producerName,
        producerCity,
        type: 'product',
        onPress: () => onPress(id),
        key: id,
      };
      return (
        <ProductDetail
          {...productDetailProps}
        />
      );
    });
    if (productRow.length % 2 !== 0) {
      productRow.push(<View key={productRow.length} style={{flex: 1}} />);
    }
    return (
      <View style={{flexDirection: 'row'}}>
        {productRow}
      </View>
    );
  }
  render() {
    let {store, products, isLoading, onProductPress} = this.props;
    let {
      storeProfilePicture, storeProfileBackground, name, ownerName, address,
      phone, email, coordinate,
    } = store;
    let zoom = 19;
    let mapsURL = `https://www.google.com/maps/@${coordinate.latitude},${coordinate.longitude},${zoom}z`;
    let showStoreOnMapsHandler = () => {
      Linking.canOpenURL(mapsURL).then((supported) => {
        if (supported) {
          Linking.openURL(mapsURL);
        } else {
          ToastAndroid.show(`Toko tidak ditemukan di dalam peta`, ToastAndroid.SHORT);
        }
      });
    };
    return (
      <View style={styles.container}>
        <TitleBar iconButtonName="menu" title="Toko Pertanian Terdekat" />
        {isLoading ? <LoadingIndicator /> :
            (
              <ScrollView style={{flex: 1}}>
                <Image style={styles.backgroundImage} source={storeProfileBackground ? {uri: storeProfileBackground} : noImage} resizeMode="cover">
                  <View style={styles.overlay}>
                    <Image style={styles.circleImage} source={storeProfilePicture ? {uri: storeProfilePicture} : noImage} />
                    <View style={styles.storeHeader}>
                      <Text style={styles.storeName}>{name}</Text>
                      <View style={styles.storeOwnerWrapper}>
                        <Text style={styles.storeOwner}>Dimiliki oleh </Text>
                        <Text style={styles.storeOwnerName}>{ownerName ? ownerName : ''}</Text>
                      </View>
                    </View>
                  </View>
                </Image>
                <StatefulTabGroup styling="secondary">
                  <Tab title="INFO">
                    <View style={{backgroundColor: 'white', flex: 1}}>
                      <View style={styles.addressSegment}>
                        <Text style={styles.addressHeader}>Alamat</Text>
                        <Text style={styles.addressText}>{address}</Text>
                        <View style={styles.locatorContainer}>
                          <IconButton
                            icon="location-on"
                            style={styles.locatorIcon}
                            containerStyle={styles.locatorIconBox}
                            onPress={showStoreOnMapsHandler}
                            text="Temukan di peta"
                          />
                        </View>
                      </View>
                      <View style={styles.addressSegment}>
                        <Text style={styles.addressHeader}>Kontak</Text>
                        <View style={styles.rowWrapper}>
                          <TouchableOpacity onPress={() => this._linkOpen(`tel:${phone}`)}>
                            <View style={styles.contactField}>
                              <Icon style={styles.contactIcon} name="phone" />
                              <Text style={styles.contactText}>{phone}</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                        <View style={styles.rowWrapper}>
                          <TouchableOpacity onPress={() => this._linkOpen(`mailto:${email}`)}>
                            <View style={styles.contactField}>
                              <Icon style={styles.contactIcon} name="email" />
                              <Text style={styles.contactText}>{email}</Text>
                            </View>
                          </TouchableOpacity>
                        </View>
                      </View>
                    </View>
                  </Tab>
                  <Tab title="PRODUK">
                    <View style={{flex: 1, backgroundColor: 'white'}}>
                      {products.length !== 0 ? (
                        <View style={{flex: 1}}>
                          <ListView
                            style={{flex: 1}}
                            dataSource={this._generateDS(products)}
                            renderRow={(item) => this._renderItem(item, onProductPress)}
                            enableEmptySections={true}
                          />
                          <Button text="LIHAT SEMUA PRODUK" onPress={this.props.navigateToStoreProducts} />
                        </View>
                      ) : <NoProductText />}
                    </View>
                  </Tab>
                </StatefulTabGroup>
              </ScrollView>
          )}
      </View>
    );
  }
}
