//@flow

import React from 'react';
import {View, ScrollView, Text, Linking, Alert} from 'react-native';
import type {CostDetail} from '../types/Cost';
import {IconButton, TitleBar} from '../core-ui';
import styles from './AnalysisDetail-style';
import formatNumber from '../helpers/formatNumber';

type Props = {
  plantCost: CostDetail;
  navigateToEditAnalysis: () => any;
  deleteAnalysis: () => any;
  selectedAnalysis: number;
};

export default function AnalysisDetail(props: Props) {
  let options = {
    prefix: 'Rp. ',
    separator: '.',
  };
  let {navigateToEditAnalysis, selectedAnalysis, deleteAnalysis} = props;
  let {title, farmingProductionCost, seedsCost, linkPdf} = props.plantCost;
  let {fertilizerCost, labourCost, firstHarvest, endHarvest} = props.plantCost;
  let {depreciation, depreciationPercent, price, grossIncome, netIncome} = props.plantCost;
  let {createdAt} = props.plantCost;
  let expenseCost = fertilizerCost + farmingProductionCost + seedsCost + labourCost;
  let expense = formatNumber(expenseCost.toString(), options);
  let handleOnPress = (id: number) => {
    Alert.alert('Analisis Biaya', 'Hapus data ini?', [
      {text: 'Ya', onPress: () => deleteAnalysis(id)},
      {text: 'Tidak'},
    ]);
  };
  let openReport = (url: string) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Alert.alert(
            'Gagal',
            'Maaf file yang Anda cari tidak ditemukan.',
          );
      }
    });
  };
  return (
    <View style={styles.mainContainer}>
      <TitleBar iconButtonName="menu" title="Analisis Usaha dan Biaya" />
      <View style={styles.rowTitle}>
        <View style={styles.textWrapper}>
          <View>
            <Text style={styles.textTitleLeft}>{title}</Text>
            <Text style={styles.textTitleRight}>{createdAt}</Text>
          </View>
          <View style={styles.row}>
            <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="edit" onPress={() => navigateToEditAnalysis(selectedAnalysis)} />
            <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="file-download" onPress={() => openReport(linkPdf)} />
            <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="delete" onPress={() => handleOnPress(selectedAnalysis)} />
          </View>
        </View>
      </View>
      <ScrollView>
        <View>
          <View style={styles.totalGrey}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Biaya Saprotan</Text>
              <Text style={styles.textTotalRight}>
                {formatNumber(farmingProductionCost.toString(), options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalWhite}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Biaya Benih</Text>
              <Text style={styles.textTotalRight}>
                {formatNumber(seedsCost.toString(), options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalGrey}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Biaya Pupuk</Text>
              <Text style={styles.textTotalRight}>
                {formatNumber(fertilizerCost.toString(), options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalWhite}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Biaya Tenaga Kerja</Text>
              <Text style={styles.textTotalRight}>
                {formatNumber(labourCost.toString(), options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalBlack}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Total Biaya Pengeluaran</Text>
              <Text style={styles.textTotalRightBlack}>{expense}</Text>
            </View>
          </View>
          <View style={styles.rowSpacer} />
          <View style={styles.rowContainerWhite}>
            <View style={styles.textWrapperRow}>
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>Hasil Panen Awal</Text>
              </View>
              <View style={{flex: 0.1}} />
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>Penyusutan</Text>
              </View>
            </View>
          </View>
          <View style={styles.rowContainerWhite}>
            <View style={styles.textWrapperRow}>
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>{title}</Text>
                <Text style={styles.textTotalLeft}>{firstHarvest}</Text>
              </View>
              <View style={{flex: 0.1}} />
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>({depreciationPercent}) {depreciation}</Text>
              </View>
            </View>
          </View>
          <View style={styles.rowContainerGrey}>
            <View style={styles.textWrapperRow}>
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>Hasil Panen Akhir</Text>
              </View>
              <View style={{flex: 0.1}} />
            </View>
          </View>
          <View style={styles.rowContainerGrey}>
            <View style={styles.textWrapperRow}>
              <View style={styles.rowItem}>
                <Text style={styles.textTotalLeft}>{endHarvest}</Text>
              </View>
              <View style={{flex: 0.1}} />
            </View>
          </View>
          <View style={styles.totalWhite}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Harga Hasil Panen</Text>
              <Text style={styles.textTotalRight}>
                {formatNumber(price.toString(), options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalBlack}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeft}>Total Penerimaan Hasil Panen</Text>
              <Text style={styles.textTotalRightBlack}>
                {formatNumber(grossIncome, options)}
              </Text>
            </View>
          </View>
          <View style={styles.totalPendapatan}>
            <View style={styles.textWrapper}>
              <Text style={styles.textTotalLeftWhite}>Total Penerimaan</Text>
              <Text style={styles.textTotalRightWhite}>
                {formatNumber(netIncome, options)}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </View>
  );
}
