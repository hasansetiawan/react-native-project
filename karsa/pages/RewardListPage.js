// @flow
import React, {Component} from 'react';
import {
  Text,
  View,
  ScrollView,
} from 'react-native';

import TitleBar from '../core-ui/TitleBar';
import GamificationHeaderContainer from '../containers/gamification/GamificationHeaderContainer';
import RewardDetailContainer from '../containers/gamification/RewardDetailContainer';
import RewardBuyInfo from '../core-ui/gamification/RewardBuyInfo';
import type {Reward} from '../types/Reward';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import RewardItem from '../core-ui/gamification/RewardItem';
import styles from './RewardListPage-style';

type State = {
  rewards: Array<Reward>;
  selectedRewardId: number;
  showRewardDetail: boolean;
  showRewardBuyInfo: boolean;
  isBuySuccess: boolean;
  isRewardListLoading: boolean;
};

type Props = {
  rewards: Array<Reward>;
  isRewardListLoading: boolean;
};

export default class RewardListPage extends Component {
  props: Props;
  state: State = {
    isLoading: false,
    rewards: [],
    selectedRewardId: 0,
    showRewardDetail: false,
    showRewardBuyInfo: false,
    isBuySuccess: false,
    isRewardListLoading: false,
  };
  _getSelectedReward() {
    const {selectedRewardId, rewards} = this.state;
    const result = rewards.filter((reward) => reward.id === selectedRewardId);
    return result && result[0];
  }
  _handleBuyPress() {
    this.setState({
      showRewardDetail: false,
      showRewardBuyInfo: true,
    });
  }
  _renderItems(items: Array<Reward>, divIndex: number) {
    let newItems = [];
    let onPress = (id) => {
      this.setState({selectedRewardId: id, showRewardDetail: true});
    };
    for (let y = divIndex * 3; y < divIndex * 3 + 3; y += 1) {
      let item = items[y];
      if (item) {
        let {
          name,
          thumbnail,
          price,
          coin,
          id,
        } = item;
        newItems.push(
          <View style={styles.productItemWrapper} key={y}>
            <RewardItem
              name={name}
              thumbnail={thumbnail}
              price={price}
              coin={coin}
              onPress={() => onPress(id)}
            />
          </View>
        );
      }
    }
    return newItems.map((item) => item);
  }

  render() {
    // let {rewards} = this.state;
    let {isRewardListLoading, rewards} = this.props;
    let itemToRender = [];
    if (isRewardListLoading) {
      itemToRender.push(<LoadingIndicator style={styles.loading} key={1} />);
    } else {
      let numberOfDivItems = Math.ceil(rewards.length / 3);
      for (let i = 0; i < numberOfDivItems; i += 1) {
        itemToRender.push(
          <View style={styles.productWrapper} key={i}>
            {this._renderItems(rewards, i)}
          </View>
        );
      }
    }
    return (
      <View style={styles.root}>
        <TitleBar iconButtonName="menu" title="Daftar Hadiah" />
        <View style={styles.headerHeight}>
          <GamificationHeaderContainer />
        </View>
        <View style={styles.listContainer}>
          <Text style={styles.tagText}>Tukarkan koin anda, dengan hadiah yang tersedia</Text>
          <ScrollView style={styles.root}>
            {itemToRender.map((item) => item)}
          </ScrollView>
        </View>
        <RewardDetailContainer
          visible={this.state.showRewardDetail}
          rewardId={this.state.selectedRewardId}
          onClose={() => this.setState({showRewardDetail: false})}
          onBuyReward={() => this.setState({
            showRewardDetail: false,
            showRewardBuyInfo: true,
          })}
        />
        <RewardBuyInfo
          visible={this.state.showRewardBuyInfo}
          reward={this.state.selectedRewardId}
          isSuccess={this.state.isBuySuccess}
          onClose={() => this.setState({showRewardBuyInfo: false})}
        />
      </View>
    );
  }
}
