// @flow

import {StyleSheet, Dimensions} from 'react-native';
let {width} = Dimensions.get('window');

import {PRIMARY_GREEN, ALTERNATE_GREY} from '../constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  backgroundImage: {
    height: 300,
    width,
  },
  overlay: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.3)',
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleImage: {
    height: 120,
    width: 120,
    borderRadius: 60,
  },
  storeHeader: {
    marginTop: 14,
    alignItems: 'center',
  },
  storeName: {
    fontSize: 20,
    color: 'white',
    fontWeight: '700',
  },
  storeOwner: {
    fontSize: 16,
    color: 'white',
  },
  storeOwnerName: {
    color: 'white',
    fontSize: 16,
    fontWeight: '600',
  },
  storeOwnerWrapper: {
    flexDirection: 'row',
    marginTop: 10,
  },
  addressSegment: {
    alignItems: 'flex-start',
    padding: 15,
    flexWrap: 'wrap',
    marginBottom: 10,
    backgroundColor: ALTERNATE_GREY,
  },
  addressHeader: {
    fontSize: 16,
    fontWeight: '600',
    marginBottom: 5,
  },
  addressText: {
    paddingLeft: 5,
    fontSize: 14,
    marginBottom: 5,
  },
  rowWrapper: {
    paddingLeft: 5,
    flexDirection: 'row',
  },
  contactField: {
    flexDirection: 'row',
    alignItems: 'center',
    height: 40,
  },
  contactText: {
    marginLeft: 5,
    fontSize: 14,
    color: PRIMARY_GREEN,
  },
  contactIcon: {
    fontSize: 16,
  },
  locatorContainer: {
    paddingLeft: 5,
  },
  locatorIconBox: {
    backgroundColor: PRIMARY_GREEN,
    borderRadius: 4,
    padding: 3,
    paddingRight: 5,
    alignSelf: 'center',
  },
  locatorIcon: {
    fontSize: 20,
  },
});
