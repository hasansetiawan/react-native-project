// @flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Modal,
  Alert,
  TouchableWithoutFeedback,
} from 'react-native';
import {TextField, Button} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import GuestModeInfoContainer from '../containers/GuestModeInfoContainer';

import styles from '../core-ui/ForgotPasswordModal-style';

type State = {
  email: string;
};

type Props = {
  guestMode: boolean;
  onRequestClose: () => void;
  onSubmitPress: () => void;
  visible: boolean;
  isLoading: boolean;
  isSuccess: boolean;
  onAlertClick: () => void;
};

export default class ForgotPasswordModal extends Component {
  state: State;
  props: Props;

  _onEmailChange(text: string) {
    this.setState({
      email: text,
    });
  }
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      email: '',
    };
  }

  render() {
    let {guestMode, onRequestClose, onSubmitPress, isLoading, isSuccess, onAlertClick} = this.props;
    let {email} = this.state;

    if (guestMode) {
      return <GuestModeInfoContainer />;
    }

    if (isLoading) {
      return <LoadingIndicator />;
    } else {
      if (isSuccess) {
        Alert.alert(
          'Forgot Password',
          'Reset password berhasil dikirim ke email!',
          [
            {text: 'OK', onPress: () => onAlertClick()},
          ],
        );
      }
    }

    return (
      <Modal
        animationType="fade"
        visible={true}
        onRequestClose={onRequestClose}
        transparent={true}
        supportedOrientations={['portrait']}
      >
        <TouchableWithoutFeedback onPress={onRequestClose}>
          <View style={styles.root}>
            <View style={styles.modalContainer}>
              <View style={styles.fieldContainer}>
                <TextField
                  placeholder="Masukan Alamat Email Anda"
                  label="Kami akan mengirim email untuk memperbarui kata sandi Anda"
                  value={email}
                  onChangeText={this._onEmailChange}
                />
                <Button
                  text="KIRIM"
                  onPress={() => onSubmitPress(email)}
                  style={styles.submit}
                />
              </View>
            </View>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
}
