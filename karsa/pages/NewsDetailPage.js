//@flow
import React from 'react';
import styles from './NewsDetailPage-style';

import {View, Text, ScrollView, ToastAndroid, Share} from 'react-native';
import {TitleBar, IconButton, ResponsiveImage} from '../core-ui';

import type {NewsItem} from '../types/News';

type Props = {
  news: NewsItem;
};

export default function NewsDetailPage(props: Props) {
  if (props.news) {
    let {title, createdAt, author, writer, foto, urlImageRss, desc, url, slug} = props.news;
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <TitleBar iconButtonName="menu" title="Berita">
          <IconButton
            icon="share"
            onPress={() => {
              shareNews({
                contentType: 'link',
                contentUrl: url || `http://www.ingkarsa.com/dashboard/berita/${slug}`,
                contentDescription: title,
              });
            }}
          />
        </TitleBar>
        {
          foto || urlImageRss ? (
            <ResponsiveImage source={foto || urlImageRss} />
          ) : null
        }
        <View style={styles.contentWrapper}>
          <View style={styles.descriptionWrapper}>
            <Text style={styles.description}>{createdAt}, oleh: {author}, penulis: {writer}</Text>
          </View>
          <View style={styles.titleWrapper}>
            <Text style={styles.titleText}>{title}</Text>
          </View>
          <View style={styles.contentWrapper}>
            <Text style={styles.contentText}>{desc}</Text>
          </View>
        </View>
      </ScrollView>
    );
  } else {
    return (
      <View>
        <TitleBar iconButtonName="menu" title="Berita" />
        <View style={styles.contentWrapper}>
          <Text style={styles.titleText}>
            Berita yang ingin Anda lihat tidak tersedia, atau sudah dinonaktifkan.
          </Text>
        </View>
      </View>
    );
  }
}

function shareNews(shareContent) {
  let {contentUrl, contentDescription} = shareContent;
  Share.share({
    message: `Baca berita "${contentDescription}" selengkapnya di ${contentUrl}`,
  })
  .catch(() => ToastAndroid.show(`Gagal membagikan berita`, ToastAndroid.SHORT));
}
