//@flow
import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import styles from './About-style';
import {TitleBar} from '../core-ui';

type Props = {
  image: ImageSource;
  title: string;
  description: string;
};

export default function About(props: Props) {
  let {image, title, description} = props;
  return (
    <View style={styles.container}>
      <TitleBar iconButtonName="menu" title="Tentang Kami" />
      <ScrollView>
        {image.uri ?
          <Image
            source={image}
            style={styles.photoWrap}
            resizeMode="contain"
          />
          : null
        }
        <View style={styles.title}><Text style={styles.titleText}>{title}</Text></View>
        <View style={styles.article}>
          <Text style={styles.articleText}>
            {description}
          </Text>
        </View>
      </ScrollView>
    </View>
  );
}
