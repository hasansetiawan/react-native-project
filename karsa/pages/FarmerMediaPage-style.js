// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    height: 400,
    margin: 5,
  },
  flex: {
    flex: 1,
  },
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
});
