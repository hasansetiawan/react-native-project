//@flow
import React from 'react';
import CostAnalysisPage from '../CostAnalysisPage';
import {View} from 'react-native';
import ItemList, {ItemHeaderWithLink, ItemContent} from '../../core-ui/ItemList';
import TextCard from '../../core-ui/TextCard';
import {IconButton, TitleBar} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

let test = {
  plantID: 0,
  userID: 0,
  id: 0,
  seedsCost: 100,
  title: '',
  farmingProductionCost: 0,
  seedCost: 0,
  fertilizerCost: 0,
  labourCost: 0,
  totalExpense: 0,
  firstHarvest: 0,
  endHarvest: '',
  price: 0,
  grossIncome: '',
  netIncome: '',
  createdAt: '',
  depreciation: 0,
  depreciationPercent: '',
  linkPdf: 'link',
};

const {describe, it} = global;

describe('CostAnalysisPage', () => {

  const props = {
    costsAnalysis: [
      test,
    ],
    navigateBack: () => {},
    navigateToNewAnalysis: () => {},
    navigateToAnalysisDetail: () => {},
    navigateToEditAnalysis: () => {},
    deleteAnalysis: () => {},
    onMenuPress: () => {},
  };
  const wrapper = shallow(
    <CostAnalysisPage {...props} />
  );
  it('should correctly render CostAnalysisPage', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(1);
    expect(wrapper.find(ItemList).length).toBe(1);
    expect(wrapper.find(ItemHeaderWithLink).length).toBe(1);
    expect(wrapper.find(ItemContent).length).toBe(1);
    expect(wrapper.find(IconButton).length).toBe(4);
    expect(wrapper.find(TitleBar).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
    expect(wrapper.find(TextCard).length).toBe(2);
  });
});
