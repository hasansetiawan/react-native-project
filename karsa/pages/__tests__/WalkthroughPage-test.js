// @flow
let {describe, it} = global;

import expect, {createSpy} from 'expect';
import {shallow} from 'enzyme';
import React from 'react';
import Swiper from 'react-native-swiper';
import WalkthroughPage from '../WalkthroughPage';

let walktroughSlideImage = require('../images/walktrough_slide.png');

describe('Weather View', () => {
  let slides = [
    {
      image: walktroughSlideImage,
      title: 'Title One',
      description: 'Description One',
    },
    {
      image: walktroughSlideImage,
      title: 'Title Two',
      description: 'Description Two',
    },
    {
      image: walktroughSlideImage,
      title: 'Title Three',
      description: 'Description Three',
    },
  ];

  let spy = createSpy();

  let wrapper = shallow(
    <WalkthroughPage
      slides={slides}
      onSkip={spy}
    />
  );

  it('should render a Swiper', () => {
    expect(wrapper.find(Swiper).length).toBe(1);
  });

});
