//@flow
import LoginPage from '../LoginPage';
import React from 'react';
import {View, Text} from 'react-native';
import {TextField, Button} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('LoginPage', () => {

  const props = {
    isLoading: false,
    onLoginSubmit: () => null,
    onFacebookLoginButtonPress: () => {},
    onGuestLoginButtonPress: () => {},
  };
  const wrapper = shallow(
    <LoginPage {...props} />
  );
  it('should correctly render LoginPage', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(8);
    expect(wrapper.find(TextField).length).toBe(2);
    expect(wrapper.find(Text).length).toBe(2);
    expect(wrapper.find(Button).length).toBe(3);
  });
});
