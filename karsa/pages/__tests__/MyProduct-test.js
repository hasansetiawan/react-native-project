// @flow
let {describe, it} = global;

import expect from 'expect';
import {shallow} from 'enzyme';
import MyProducts from '../MyProducts';
import React from 'react';
import {View} from 'react-native';
import {Dropdown, TitleBar, GridView} from '../../core-ui';
import image from '../images/karsa-01.png';
const product = {
  id: 123,
  name: 'Abamektin 200',
  highlight: false,
  description: 'Pestisida super ampuh berbahan organik tanpa pengawet',
  photo: image,
  priceMin: 50000,
  priceMax: 100000,
  producerID: 392,
  producerName: 'PT Agrotech',
  producerAddress: 'Jl Maju Jaya no. 123, Jawa Barat',
  producerProvince: 'Jawa Barat',
  producerCity: 'Bandung',
  gallery: [],
  rate: '4.0',
  reviewCount: 2,
  new: false,
  popular: false,
};

describe('MyProducts', () => {
  it('should render MyProducts properly', () => {
    let wrapper = shallow(<MyProducts items={[product]} onMenuPress={() => {}} />);
    expect(wrapper.find(View).length).toBe(2);
    expect(wrapper.find(GridView).length).toBe(1);
    expect(wrapper.find(Dropdown).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
