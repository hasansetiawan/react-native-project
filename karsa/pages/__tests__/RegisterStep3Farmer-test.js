//@flow
import RegisterStep3Farmer from '../register/RegisterStep3Farmer';
import React from 'react';
import {View, Text} from 'react-native';
import {TitleBar, ContentStepper, IconButton} from '../../core-ui';
import PlantInfoForm from '../register/PlantInfoForm';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('RegisterStep3Farmer', () => {
  const wrapper = shallow(
    <RegisterStep3Farmer onMenuPress={() => {}} />
  );
  it('should correctly render RegisterStep3Farmer', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(2);
    expect(wrapper.find(Text).length).toBe(1);
    expect(wrapper.find(PlantInfoForm).length).toBe(1);
    expect(wrapper.find(ContentStepper).length).toBe(1);
    expect(wrapper.find(IconButton).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
