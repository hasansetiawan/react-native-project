//@flow
import NewAnalysis from '../NewAnalysis';
import React from 'react';
import {View, Text} from 'react-native';
import {
  TextField,
  Button,
  Dropdown,
  TitleBar,
} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

const props = {
  onNewAnalysisSubmit: () => {},
  onPressCancel: () => {},
  plants: [],
  onMenuPress: () => {},
};

describe('NewAnalysis', () => {
  const wrapper = shallow(
    <NewAnalysis {...props} />
  );
  it('should correctly render NewAnalysis', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(20);
    expect(wrapper.find(Text).length).toBe(9);
    expect(wrapper.find(TextField).length).toBe(7);
    expect(wrapper.find(Button).length).toBe(2);
    expect(wrapper.find(Dropdown).length).toBe(2);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
