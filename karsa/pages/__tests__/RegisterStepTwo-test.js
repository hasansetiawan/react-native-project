//@flow
import RegisterStepTwo from '../register/RegisterStepTwo';
import React from 'react';
import {View, ScrollView} from 'react-native';
import {TextField, Button, ProfilePicture, TitleBar} from '../../core-ui';
import GenderDropdown from '../register/GenderDropdown';
import ProvinceDropdown from '../register/ProvinceDropdown';
import CityDropdown from '../register/CityDropdown';
import DistrictDropdown from '../register/DistrictDropdown';
import VillageDropdown from '../register/VillageDropdown';
import NormalCheckbox from '../../core-ui/Checkbox';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('RegisterStepTwo', () => {
  const wrapper = shallow(
    <RegisterStepTwo onMenuPress={() => {}} />
  );
  it('should correctly render RegisterStepTwo', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(16);
    expect(wrapper.find(ScrollView).length).toBe(1);
    expect(wrapper.find(TextField).length).toBe(6);
    expect(wrapper.find(Button).length).toBe(1);
    expect(wrapper.find(ProfilePicture).length).toBe(1);
    expect(wrapper.find(NormalCheckbox).length).toBe(1);
    expect(wrapper.find(GenderDropdown).length).toBe(1);
    expect(wrapper.find(ProvinceDropdown).length).toBe(1);
    expect(wrapper.find(CityDropdown).length).toBe(1);
    expect(wrapper.find(DistrictDropdown).length).toBe(1);
    expect(wrapper.find(VillageDropdown).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
