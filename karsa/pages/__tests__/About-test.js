//@flow
import About from '../About';
import React from 'react';
import {View, Text, Image} from 'react-native';
import {TitleBar} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

import reactImage from '../images/karsa-01.png';

const {describe, it} = global;
let title = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit';
let description = `Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et
dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut
aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
cillum dolore eu fugiat nulla pariatur.`;

describe('About', () => {
  const wrapper = shallow(
    <About
      image={reactImage}
      title={title}
      description={description}
      onMenuPress={() => {}}
    />
  );
  it('should correctly render About', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(3);
    expect(wrapper.find(Text).length).toBe(2);
    expect(wrapper.find(TitleBar).length).toBe(1);
    expect(wrapper.find(Image).length).toBe(1);
  });
});
