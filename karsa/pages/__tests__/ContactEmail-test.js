//@flow
import ContactEmail from '../ContactEmail';
import React from 'react';
import {ScrollView} from 'react-native';
import {TextField, Button} from '../../core-ui';
import expect, {createSpy} from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('ContactEmail', () => {
  const spy = createSpy();
  const wrapper = shallow(
    <ContactEmail onSubmit={spy} />
  );
  it('should correctly render ContactEmail', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(ScrollView).length).toBe(1);
    expect(wrapper.find(TextField).length).toBe(4);
    expect(wrapper.find(Button).length).toBe(1);
  });
});
