// @flow

const {describe, it} = global;

import {shallow} from 'enzyme';
import React from 'react';
import {View} from 'react-native';
import expect from 'expect';

import ProfilePage from '../ProfilePage';
import {ProfileHeader, ProfileHeaderText, TitleBar, IconButton, StatefulTabGroup} from '../../core-ui';
import image from '../../images/karsa-01.png';

describe('ProfilePage Test', () => {
  let user = {
    name: 'Claire Sutanto',
    role: 'Farmer',
    phoneNumber: '087871326666',
    email: 'claire@somewhere.com',
    address: 'Jl. Danau Singkarak, Blok AD 8 no 20, Tangerang, Banten',
    avatar: image,
    avatarFb: image,
  };
  let wrapper = shallow(
    <ProfilePage user={user} onEditProfilePress={() => {}} onMenuPress={() => {}} />
  );
  it('Should render the right views', () => {
    expect(wrapper.find(View).length).toBe(1);
  });
  it('Should render Tabs component', () => {
    expect(wrapper.find(StatefulTabGroup).length).toBe(1);
  });
  it('Should render TitleBar Component', () => {
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
  it('Should render IconButton Component', () => {
    expect(wrapper.find(IconButton).length).toBe(1);
  });
  it('Should render ProfileHeader Component', () => {
    expect(wrapper.find(ProfileHeader).length).toBe(1);
  });
  it('Should render ProfileHeaderText Component', () => {
    expect(wrapper.find(ProfileHeaderText).length).toBe(1);
  });
});
