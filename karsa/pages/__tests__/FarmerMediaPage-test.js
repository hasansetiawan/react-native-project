const {describe, it} = global;

import React from 'react';
import {shallow} from 'enzyme';
import expect from 'expect';
import {View} from 'react-native';
import {TileButton, TileButtonWrapper, TitleBar} from '../../core-ui';

import FarmerMediaPage from '../../pages/FarmerMediaPage';

describe('Farmer Media Page Test', () => {
  let storeSpy = expect.createSpy();
  let infoSpy = expect.createSpy();
  let pestSpy = expect.createSpy();
  let wrapper = shallow(
    <FarmerMediaPage
      onStoreTilePress={storeSpy}
      onInfoTilePress={infoSpy}
      onPestTilePress={pestSpy}
    />
  );
  it('Should render the correct number of View', () => {
    expect(wrapper.find(View).length).toBe(5);
  });
  it('Should render the correct number of TileButton', () => {
    expect(wrapper.find(TileButton).length).toBe(3);
  });
  // it('Should simulate click on each tile', () => {
  //   wrapper.find(TileButton).at(0).simulate('press');
  //   expect(storeSpy.calls.length).toBe(1);
  //   wrapper.find(TileButton).at(1).simulate('press');
  //   expect(infoSpy.calls.length).toBe(1);
  //   wrapper.find(TileButton).at(2).simulate('press');
  //   expect(pestSpy.calls.length).toBe(1);
  // });
  it('Should render the correct number of TileButtonWrapper', () => {
    expect(wrapper.find(TileButtonWrapper).length).toBe(0);
  });
  it('Should render the correct number of TitleBar', () => {
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
