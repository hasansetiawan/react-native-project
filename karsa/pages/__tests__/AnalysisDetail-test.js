//@flow
import React from 'react';
import AnalysisDetail from '../AnalysisDetail';
import {View, ScrollView, Text} from 'react-native';
import {TitleBar, IconButton} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('AnalysisDetail', () => {
  const plantCost = {
    id: 12323,
    userID: 12344,
    title: 'Perhitungan Percobaan 1',
    farmingProductionCost: 1000000,
    seedsCost: 1000000,
    fertilizerCost: 1000000,
    labourCost: 1000000,
    firstHarvest: 1000,
    endHarvest: '900 kg',
    depreciation: 100,
    depreciationPercent: '10%',
    price: 40000,
    grossIncome: '36000000',
    netIncome: '32000000',
    createdAt: '22/10/2015',
    plantName: 'melon',
    plantID: 1232,
    totalExpense: 4000040,
    linkPdf: 'link',
  };
  const props = {
    plantCost,
    navigateToEditAnalysis: () => {},
    deleteAnalysis: () => {},
    selectedAnalysis: 1,
    onMenuPress: () => {},
  };
  const wrapper = shallow(
    <AnalysisDetail {...props} />
  );
  it('should correctly render AnalysisDetail', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(41);
    expect(wrapper.find(Text).length).toBe(25);
    expect(wrapper.find(ScrollView).length).toBe(1);
    expect(wrapper.find(IconButton).length).toBe(3);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
