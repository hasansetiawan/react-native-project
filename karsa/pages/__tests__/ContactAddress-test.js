//@flow
import ContactAddress from '../ContactAddress';
import React from 'react';
import {Text, View} from 'react-native';
import {Icon} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('ContactAddress', () => {

  const props = {
    mainOffice: {
      type: 'main',
      name: 'Kantor Utama',
      address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
      city: 'Jakarta',
      phone: '(021) 2083633',
      email: 'info@qarsa.com',
    },
  };
  const wrapper = shallow(
    <ContactAddress {...props} />
  );
  it('should correctly render ContactAddress', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(8);
    expect(wrapper.find(Text).length).toBe(4);
    expect(wrapper.find(Icon).length).toBe(3);
  });
});
