// @flow
let {describe, it} = global;

import expect, {createSpy} from 'expect';
import {shallow} from 'enzyme';
import React from 'react';
import {View} from 'react-native';
import Dropdown from '../../core-ui/Dropdown';
import WeatherPage from '../WeatherPage';

describe('Weather Page', () => {
  let cityOptions = [
    {value: 1, label: 'BANDUNG'},
    {value: 2, label: 'JAKARTA'},
    {value: 3, label: 'SURABAYA'},
  ];

  let todayWeather = {
    time: '2015-01-03T17:00:00.000Z',
    tempRange: '31-33',
    description: 'Cerah Berawan',
    humidity: '47%',
    windSpeed: '10 km/h',
    windDirection: 'Utara Barat Laut',
  };

  let nextWeathers = [
    {time: '2015-01-04T17:00:00.000Z', tempRange: '29-30', description: 'Cerah Berawan'},
    {time: '2015-01-05T17:00:00.000Z', tempRange: '27-28', description: 'Hujan'},
    {time: '2015-01-06T17:00:00.000Z', tempRange: '28-32', description: 'Mendung'},
    {time: '2015-01-07T17:00:00.000Z', tempRange: '26-31', description: 'Berawan'},
    {time: '2015-01-08T17:00:00.000Z', tempRange: '28-33', description: 'Hujan Lebat'},
  ];
  let selectSpy = createSpy();

  let wrapper = shallow(
    <WeatherPage
      cityOptions={cityOptions}
      todayWeather={todayWeather}
      nextWeathers={nextWeathers}
      selectedValue={0}
      onCityOptionsSelect={selectSpy}
      onMenuPress={() => {}}
    />
  );

  it('should render a weather view', () => {
    expect(wrapper.find(View).length).toBe(35);
    expect(wrapper.find(Dropdown).length).toBe(1);
  });

});
