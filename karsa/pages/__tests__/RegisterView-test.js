//@flow
import RegisterPage from '../RegisterPage';
import React from 'react';
import {View} from 'react-native';
import {TextField, Button} from '../../core-ui';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('RegisterPage', () => {

  const props = {};
  const wrapper = shallow(
    <RegisterPage {...props} />
  );
  it('should correctly render RegisterPage', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(5);
    expect(wrapper.find(TextField).length).toBe(4);
    expect(wrapper.find(Button).length).toBe(1);
  });
});
