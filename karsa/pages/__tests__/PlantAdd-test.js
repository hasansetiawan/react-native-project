//@flow
import PlantAdd from '../PlantAdd';
import React from 'react';
import {View} from 'react-native';
import {TitleBar} from '../../core-ui';
import PlantInfoForm from '../register/PlantInfoForm';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('PlantAdd', () => {
  const wrapper = shallow(
    <PlantAdd onMenuPress={() => {}} />
  );
  it('should correctly render PlantAdd', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(2);
    expect(wrapper.find(PlantInfoForm).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
