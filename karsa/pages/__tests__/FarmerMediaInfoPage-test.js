// @flow

const {describe, it} = global;

import React from 'react';
import {View} from 'react-native';
import {shallow} from 'enzyme';
import expect from 'expect';

import FarmerMediaInfoPage from '../FarmerMediaInfoPage';
import {TileButton, TileButtonWrapper, TitleBar} from '../../core-ui';

describe('Farmer Media Info Page Test', () => {
  let wrapper = shallow(
    <FarmerMediaInfoPage
      navigateTo={() => {}}
      onMenuPress={() => {}}
    />
  );
  it('Should render the right number of View Component', () => {
    expect(wrapper.find(View).length).toBe(3);
  });
  it('Should render the right number TileButton', () => {
    expect(wrapper.find(TileButton).length).toBe(4);
  });
  it('Should render the right number TileButtonWrapper', () => {
    expect(wrapper.find(TileButtonWrapper).length).toBe(2);
  });
  it('Should render the right number TitleBar', () => {
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
