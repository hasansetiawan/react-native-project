//@flow
import SeedInfo from '../SeedInfo';
import React from 'react';
import {
  View,
  Text,
  TouchableOpacity,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  Dropdown,
  GridView,
} from '../../core-ui';
import karsaLogo from '../images/karsa_logo.png';
import image from '../images/karsa-01.png';


const product = {
  picture: image,
  name: 'Abamektin 200',
  priceRange: 'Rp 750.000 - 800.000',
  sellerName: 'PT Prima Agrotech',
  sellerLocation: 'Jember',
  onPress: () => {},
};

import expect, {createSpy} from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

const gridProps = {
  items: [product, product, product, product, product, product],
  itemsPerRow: 2,
  renderItem: createSpy(),
  style: {margin: 5},
};
const seedProps = {
  gridProps: gridProps,
  karsaLogo: karsaLogo,
  onMenuPress: () => {},
};

describe('SeedInfo', () => {
  const wrapper = shallow(
    <SeedInfo {...seedProps} />
  );
  it('should correctly render SeedInfo', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(3);
    expect(wrapper.find(Text).length).toBe(1);
    expect(wrapper.find(TouchableOpacity).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
    expect(wrapper.find(IconButton).length).toBe(1);
    expect(wrapper.find(Dropdown).length).toBe(1);
    expect(wrapper.find(GridView).length).toBe(1);
  });
});
