//@flow
import PlantInfo from '../PlantInfo';
import React from 'react';
import {View} from 'react-native';
import {TitleBar} from '../../core-ui';
import PlantInfoForm from '../register/PlantInfoForm';
import expect from 'expect';
import {shallow} from 'enzyme';

const {describe, it} = global;

describe('PlantInfo', () => {
  const wrapper = shallow(
    <PlantInfo onMenuPress={() => {}} />
  );
  it('should correctly render PlantInfo', () => {
    expect(wrapper.length).toBe(1);
    expect(wrapper.find(View).length).toBe(2);
    expect(wrapper.find(PlantInfoForm).length).toBe(1);
    expect(wrapper.find(TitleBar).length).toBe(1);
  });
});
