// @flow

// const {describe, it} = global;
//
// import React from 'react';
// import {
//   View,
//   ScrollView,
//
// } from 'react-native';
// import {shallow} from 'enzyme';
// import expect from 'expect';
//
// import {TitleBar, IconButton} from '../../core-ui';
// import NearbyStorePage, {StoreInformation} from '../NearbyStorePage';
//
// import logoImage from '../images/karsa_logo.png';

// describe('Nearby Store Page', () => {
//   let storeList = [{
//     storeName: 'Toko Makmur Jaya',
//     streetAddress: 'Jl. Teuku Umar No. 12',
//     storeAddress: 'Kab. Bandung Barat, Jawa Barat',
//     storeImage: logoImage,
//     phoneNumber: '021878713212',
//   }];
//   let wrapper = shallow(
//     <NearbyStorePage storeList={storeList} />
//   );
//   it('Should Render the right number of View', () => {
//     expect(wrapper.find(View).length).toBe(2);
//   });
//   it('Should Render the right number of ScrollView', () => {
//     expect(wrapper.find(ScrollView).length).toBe(1);
//   });
//   it('Should Render the right number of TitleBar', () => {
//     expect(wrapper.find(TitleBar).length).toBe(1);
//   });
//   it('Should Render the right number of IconButton', () => {
//     expect(wrapper.find(IconButton).length).toBe(1);
//   });
//   it('Should Render the right number of StoreInformation', () => {
//     expect(wrapper.find(StoreInformation).length).toBe(1);
//   });
// });
