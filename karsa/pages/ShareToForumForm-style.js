// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  outer: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
    justifyContent: 'center',
  },
  inner: {
    flex: 1,
    marginHorizontal: 30,
    backgroundColor: '#fff',
  },
  dialog: {
    flex: 1,
  },
  content: {
    padding: 10,
  },
  attachedData: {
    backgroundColor: '#ededed',
  },
  data: {
    flex: 1,
    padding: 3,
    borderBottomWidth: 1,
    borderColor: '#d1d1d1',
  },
  label: {
    color: '#b1b1b1',
  },
  text: {
  },
});
