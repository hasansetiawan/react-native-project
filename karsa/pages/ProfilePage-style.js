// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  root: {
    flex: 1,
  },
  textStyle: {
    padding: 12,
  },
  textStyleDark: {
    padding: 12,
    backgroundColor: '#EDEDED',
  },
});
