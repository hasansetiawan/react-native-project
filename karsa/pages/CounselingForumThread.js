//@flow

import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import {IconButton, ThreadCard, TitleBar} from '../core-ui';
import styles from './CounselingForumThread-style';

import image from '../images/default-avatar.png';

let onCardPress = () => {};
let onLikePress = () => {};
let onCommentPress = () => {};

let mockProps = {
  thread: {
    id: 1,
    userID: 1,
    firstName: 'Bagus Andrianto',
    premium: false,
    userLikes: [],
    photo: null,
    photoShare: null,
    description: '',
    date: '2 hari yang lalu',
    avatar: image,
    avatarFacebook: null,
    createdAt: '8 Oktober 2016, 15:30',
    title: 'Cara menanam tanaman dengan cepat dan efektif.',
    picture: image,
    categoryName: 'Bla',
    slug: '',
    subCategoryName: 'Foo',
    latestComment: {
      id: 1,
      forumID: 1,
      userID: 1,
      firstName: 'Ismadi Surya',
      post: 'asdf',
      date: '22 Oktober 2016',
      createdAt: '',
      updatedAt: '',
      premium: false,
    },
    likeCount: 0,
    commentCount: 0,
    totalView: 0,
  },
  onCardPress,
  onLikePress,
  onCommentPress,
  isLiked: true,
};

export default class CounselingForumThread extends Component {
  render() {
    return (
      <View style={styles.container}>
        <TitleBar title="Forum Diskusi" iconButtonName="menu">
          <IconButton icon="search" />
        </TitleBar>
        <ScrollView style={styles.content}>
          <ThreadCard {...mockProps} />
        </ScrollView>
      </View>
    );
  }
}
