//@flow
import React, {Component} from 'react';
import {
  View,
  WebView,
  ToastAndroid,
  Share,
} from 'react-native';
import {TitleBar, IconButton, LoadingIndicator} from '../core-ui';
import styles from './NewsWebView-style';
import autobind from 'class-autobind';

type Props = {
  newsURL: string;
  title: string;
};

export default class NewsWebView extends Component {
  props: Props;
  _shareMessage: Function;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    return (
      <View style={styles.flex}>
        <TitleBar iconButtonName="menu" title="Berita">
          <IconButton icon="share" onPress={this._shareMessage} />
        </TitleBar>
        <WebView
          renderLoading={() => <LoadingIndicator />}
          startInLoadingState={true}
          source={{uri: this.props.newsURL}}
        />
      </View>
    );
  }

  _shareMessage() {
    let {newsURL, title} = this.props;
    Share.share({
      message: `Baca berita "${title}" selengkapnya di ${newsURL}`,
    })
    .catch(() => ToastAndroid.show(`Gagal membagikan berita`, ToastAndroid.SHORT));
  }
}
