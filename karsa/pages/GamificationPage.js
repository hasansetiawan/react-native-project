// @flow
import React from 'react';
import {ScrollView, View} from 'react-native';

import StatefulTabGroup from '../core-ui/StatefulTabGroup';
import {Tab} from '../core-ui/TabGroup';

import GamificationPageTitleBarContainer from '../containers/gamification/GamificationPageTitleBarContainer';
import GamificationHeaderContainer from '../containers/gamification/GamificationHeaderContainer';

import MyActivitiesTabContent from '../core-ui/gamification/MyActivitiesTabContent';
import BonusTabContentContainer from '../containers/gamification/BonusTabContentContainer';
import MyRewardListTabContent from '../core-ui/gamification/MyRewardListTabContent';

export default function GamificationPage() {
  return (
    <View style={{flex: 1, backgroundColor: '#fff'}}>
      <GamificationPageTitleBarContainer />
      <ScrollView>
        <View style={{height: 150}}>
          <GamificationHeaderContainer />
        </View>
        <StatefulTabGroup styling="secondary">
          <Tab title="KEGIATAN">
            <MyActivitiesTabContent />
          </Tab>
          <Tab title="BONUS">
            <BonusTabContentContainer />
          </Tab>
          <Tab title="HADIAHKU">
            <MyRewardListTabContent />
          </Tab>
        </StatefulTabGroup>
      </ScrollView>
    </View>
  );
}
