// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import {
  TitleBar,
  ThumbnailCard,
} from '../core-ui';

import styles from './NearbyStorePage-style';
import logoImage from '../images/karsa_logo.png';
import type {Toko} from '../types/NearbyStore';
import type {GPSAddress} from '../types/Location';
import noImage from '../images/no_image.png';

type Props = {
  nearbyStores: Map<number, Toko>;
  gpsAddress: GPSAddress;
  onPress: (id: number) => void;
};

export default class NearbyStorePage extends Component {
  props: Props;

  render() {
    let {nearbyStores, gpsAddress, onPress} = this.props;
    let storeList;
    if (nearbyStores.size > 0) {
      storeList = (
        <ScrollView>
          {Array.from(nearbyStores.values()).map((value, key) => <StoreInformation key={key} onPress={onPress} {...value} />)}
        </ScrollView>
      );
    } else {
      storeList = (
        <View style={styles.notFoundWrapper}>
          <Text style={styles.notFoundText}>
            Tidak Ditemukan Toko Pertanian Di Sekitar Anda
          </Text>
        </View>
      );
    }
    let currentAddress = gpsAddress ? `${gpsAddress.city}, ${gpsAddress.province}` : '';
    return (
      <View style={styles.root}>
        <TitleBar title="Toko Pertanian Terdekat" logoImage={logoImage} />
        <View style={styles.locationWrapper}>
          <Text style={styles.locationText}>{currentAddress}</Text>
        </View>
        {storeList}
      </View>
    );
  }
}

type StoreProps = {
  id: number;
  userID: number;
  storeProfilePicture: string;
  storeProfileBackground: string;
  name: string;
  address: string;
  location: Location;
  phone: string;
  email: string;
  ownerName: string;
  createdAt: Date;
  updatedAt: Date;
  coordinate: Coordinates;
  distance: number;
  onPress: (id: number) => void;
};

function StoreInformation(props: StoreProps) {
  let {name, address, storeProfilePicture, onPress, id} = props;
  return (
    <ThumbnailCard
      image={storeProfilePicture ? {uri: storeProfilePicture} : noImage}
      shape="round"
      style={{elevation: 0}}
      thumbnailStyle={{width: 50, height: 50}}
      onPress={() => onPress(id)}
    >
      <View style={styles.row}>
        <View style={styles.storeInfoWrapper}>
          <Text style={styles.storeName}>{name}</Text>
          <Text style={styles.addressText} numberOfLines={2}>{address}</Text>
        </View>
      </View>
    </ThumbnailCard>
  );
}
