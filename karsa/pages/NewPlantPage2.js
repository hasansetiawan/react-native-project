//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  ScrollView,
  Image,
  Alert,
} from 'react-native';
import {
  TextField,
  Dropdown,
  Button,
  RadioGroup,
  RadioItem,
} from '../core-ui';
import LocationDropdown from '../components/locationDropdown/LocationDropdownContainer';
import styles from './NewPlantPage-style';
import {getScreenSize} from '../helpers/getSize';
import Permissions from 'react-native-permissions';
import type {Value, Option} from '../core-ui/Dropdown';
import type {Coordinates, GPSAddress, Location} from '../types/Location';
import type {GalleryData} from '../types/Plant';

type State = {
  locationPermission: string;
};

type Props = {
  soilPH: Value;
  soilPHOptions: Array<Option>;
  selectedRadio: string;
  currentPosition: Coordinates;
  gpsAddress: GPSAddress;
  newPhoto: GalleryData;

  onSoilPHSelect: () => void;
  onGPSSelect: () => void;
  onManualInputSelect: () => void;
  switchPage: (pageNumber: number) => void;
  getLocationDropdownData: (location: Location) => void;
  onPictureSelect: () => void;
  onInformationChange: (input: string) => void;
  submitNewPlant: () => void;
};

export default class NewPlantPage2 extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      locationPermission: 'undetermined',
    };
  }

  componentDidMount() {
    Permissions.getPermissionStatus('location')
      .then((response) => {
        //response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        this.setState({locationPermission: response});
      });
  }

  render() {
    let {
      soilPH,
      soilPHOptions,
      selectedRadio,
      onSoilPHSelect,
      onManualInputSelect,
      switchPage,
      currentPosition,
      gpsAddress,
      getLocationDropdownData,
      newPhoto,
      onPictureSelect,
      onInformationChange,
      submitNewPlant,
    } = this.props;

    let onBackPress = () => {
      switchPage(1);
    };
    let photo = newPhoto.photo ? (
      <View style={styles.photo}>
        <Image source={{uri: newPhoto.photo}} style={{height: getScreenSize().width - 70}} resizeMode="cover" />
      </View>
    ) : <View />;

    return (
      <ScrollView style={styles.secondContent}>
        <View style={styles.body}>
          <Dropdown
            label="Tingkat pH Tanah"
            placeholder="Pilih pH tanah"
            options={soilPHOptions}
            animationType="none"
            onSelect={onSoilPHSelect}
            selectedValue={soilPH}
          />
          <View style={styles.location}>
            <Text style={styles.inputLabel}>Lokasi Penanaman</Text>
            <View style={styles.inputItem}>
              <RadioGroup>
                <RadioItem onSelect={this._onGPSSelect} isSelected={selectedRadio === 'gps'} label="Gunakan GPS">
                  {
                    (selectedRadio === 'gps') ? (
                      <View style={styles.locationTextContainer}>
                        <Text style={styles.locationText}>longitude : {currentPosition.longitude}</Text>
                        <Text style={styles.locationText}>latitude : {currentPosition.latitude}</Text>
                        <Text style={styles.locationText}>{gpsAddress.fullAddress}</Text>
                      </View>
                    ) : null
                  }
                </RadioItem>
                <RadioItem onSelect={onManualInputSelect} isSelected={selectedRadio === 'manual'} label="Input secara manual">
                  {
                    (selectedRadio === 'manual') ? (
                      <View style={styles.locationDropdown}>
                        <LocationDropdown
                          getSelectedValues={getLocationDropdownData}
                        />
                      </View>
                    ) : null
                  }
                </RadioItem>
              </RadioGroup>
            </View>
          </View>
          <TextField
            label="Keterangan"
            placeholder="Masukkan keterangan (optional)"
            onChangeText={onInformationChange}
          />
          <View style={styles.photoContainer}>
            <Text style={styles.inputLabel}>Foto Tanaman</Text>
            <View>
              {photo}
              <Button
                text="Pilih Foto"
                onPress={() => onPictureSelect()}
              />
            </View>
          </View>
        </View>
        <View style={styles.buttons}>
          <Button
            text="KEMBALI"
            onPress={onBackPress}
            inverted
          />
          <Button
            text="SIMPAN"
            onPress={submitNewPlant}
          />
        </View>
      </ScrollView>
    );
  }

  _onGPSSelect() {
    let {locationPermission} = this.state;
    let {onGPSSelect} = this.props;
    if (locationPermission !== 'authorized') {
      this._requestPermission(locationPermission);
    } else {
      onGPSSelect();
    }
  }

  _requestPermission(locationPermission) {
    try {
      Alert.alert(
        'Akses GPS',
        'Karsa memerlukan izin untuk mengakses GPS',
        [
          {text: 'Tolak'},
          locationPermission === 'undetermined' ?
            {text: 'Izinkan', onPress: this._authorizePermission}
            : {text: 'Buka Settings', onPress: Permissions.openSettings},
        ]
      );
    } catch (e) {
      Alert.alert('Error', 'Gagal mengakses GPS. Mohon periksa pengaturan Anda.');
    }
  }

  _authorizePermission() {
    Permissions.requestPermission('location')
      .then((res) => {
        if (res !== 'authorized') {
          Alert.alert(
            'Otorisasi Gagal',
            'Terjadi masalah dalam mendapatkan izin. Anda dapat memberikan izin secara manual di dalam menu Settings > Permissions.',
            [
              {text: 'Batal', style: 'cancel'},
              {text: 'Buka Settings', onPress: Permissions.openSettings},
            ]
          );
        } else {
          this.setState({locationPermission: res});
        }
      }).catch(() => Alert.alert('Error', 'Gagal mengakses GPS. Mohon periksa pengaturan Anda.'));
  }

}
