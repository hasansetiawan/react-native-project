// @flow

import {StyleSheet} from 'react-native';

let rowWrapper = {
  flexDirection: 'row',
};

let columnWrapper = {
  flex: 1,
  flexDirection: 'column',
};

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  headerImage: {
    height: 100,
  },
  tabWrapper: {
    flex: 1,
  },
  tabContentWrapper: {
    flex: 1,
    paddingHorizontal: 20,
  },
  rowWrapper,
  columnWrapper,
  thumbnail: {
    width: 100,
    height: 100,
    marginRight: 10,
  },
  listItemWrapper: {
    ...rowWrapper,
    borderColor: '#555',
    borderBottomWidth: 0.5,
    paddingVertical: 10,
  },
  icon: {
    color: '#555',
    fontSize: 16,
  },
  itemTitleWrapper: {
    ...rowWrapper,
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingBottom: 5,
  },
  itemTitle: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  itemDescription: {
    fontSize: 14,
  },
  indicatorWrapper: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  spinnerWrapper: {
    margin: 8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchContainer: {
    flexDirection: 'row',
  },
  searchTextContainer: {
    flex: 1,
  },
  submitSearch: {
    color: 'black',
  },
});
