// @flow

import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  headerRow: {
    flex: 1,
    height: 80,
    backgroundColor: PRIMARY_GREEN,
  },
  tabContainer: {
    padding: 8,
  },
  cardContainer: {
    marginVertical: 8,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  empty: {
    alignSelf: 'center',
    textAlign: 'center',
  },
});
