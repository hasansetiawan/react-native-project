//@flow
import React from 'react';
import {View, Linking, Alert} from 'react-native';
import ItemList, {ItemHeaderWithLink, ItemContent} from '../core-ui/ItemList';
import TextCard from '../core-ui/TextCard';
import {IconButton, TitleBar} from '../core-ui';
import formatNumber from '../helpers/formatNumber';
import type {CostDetail} from '../types/Cost';

type Props = {
  costsAnalysis: Array<CostDetail>;
  navigateBack: () => void;
  navigateToNewAnalysis: () => void;
  navigateToAnalysisDetail: () => void;
  navigateToEditAnalysis: () => void;
  deleteAnalysis: () => void;
};

let options = {
  prefix: 'Rp. ',
  separator: '.',
};

export default function costAnalysisPage(props: Props) {
  let {costsAnalysis, navigateBack, navigateToNewAnalysis, deleteAnalysis, navigateToAnalysisDetail, navigateToEditAnalysis} = props;
  let handleOnPress = (id: number) => {
    Alert.alert('Analisis Biaya', 'Hapus data ini?', [
      {text: 'Ya', onPress: () => deleteAnalysis(id)},
      {text: 'Tidak'},
    ]);
  };
  let openReport = (url: string) => {
    Linking.canOpenURL(url).then((supported) => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Alert.alert(
            'Gagal',
            'Maaf file yang Anda cari tidak ditemukan.',
          );
      }
    });
  };
  let costsAnalysisRender = [];
  costsAnalysis.forEach((costData, idx) => costsAnalysisRender.push(
    <ItemList key={idx}>
      <ItemHeaderWithLink title={costData.title} onPress={() => navigateToAnalysisDetail(costData.id)}>
        <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="edit" onPress={() => navigateToEditAnalysis(costData.id)} />
        <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="file-download" onPress={() => openReport(costData.linkPdf)} />
        <IconButton style={{color: '#6E6E6E', fontSize: 22}} icon="delete" onPress={() => handleOnPress(costData.id)} />
      </ItemHeaderWithLink>
      <ItemContent>
        <TextCard
          title="Total Pengeluaran"
          textItems={[formatNumber(costData.totalExpense.toString(), options)]}
        />
        <TextCard
          title="Total Pendapatan"
          textItems={[formatNumber(costData.netIncome, options)]}
        />
      </ItemContent>
    </ItemList>
  ));
  return (
    <View style={{flex: 1, backgroundColor: 'white'}}>
      <TitleBar back={navigateBack} iconButtonName="menu" title="Analisis Biaya Dan Usaha">
        <IconButton icon="add" onPress={navigateToNewAnalysis} />
      </TitleBar>
      {costsAnalysisRender}
    </View>
  );
}
