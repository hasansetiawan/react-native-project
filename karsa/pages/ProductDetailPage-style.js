// @flow

import {StyleSheet, Dimensions} from 'react-native';
import {PRIMARY_GREEN, PRIMARY_GREY} from '../constants/color';

let {width} = Dimensions.get('window');

export default StyleSheet.create({
  smallPhoto: {
    width: 60,
    height: 60,
    marginRight: 8,
  },
  galleryWrapper: {
    flexDirection: 'column',
  },
  mainPhoto: {
    width: 220,
    height: 220,
    alignSelf: 'center',
    marginBottom: 10,
  },
  productHeaderWrapper: {
    flex: 1,
    padding: 10,
  },
  productName: {
    fontSize: 16,
    fontWeight: '500',
    marginTop: 4,
    marginBottom: 6,
  },
  producerAddress: {
    fontSize: 14,
    marginTop: 6,
  },
  priceTag: {
    fontSize: 18,
    marginTop: 12,
    fontWeight: '500',
    color: PRIMARY_GREEN,
  },
  descWrapper: {
    padding: 10,
    marginBottom: 4,
  },
  descHeader: {
    color: PRIMARY_GREY,
    fontSize: 12,
    fontWeight: '500',
    marginBottom: 6,
  },
  desc: {
    fontSize: 12,
  },
  dataSegment: {
    borderBottomWidth: 2,
    borderColor: PRIMARY_GREY,
    margin: 8,
    flexDirection: 'row',
  },
  reviewHeader: {
    flexDirection: 'row',
    paddingHorizontal: 6,
    justifyContent: 'space-between',
    height: 40,
    alignItems: 'center',
  },
  ratingWrapper: {
    alignItems: 'center',
  },
  reviewCount: {
    fontSize: 14,
    marginRight: 8,
  },
  reviewDetailText: {
    flexWrap: 'wrap',
    fontSize: 12,
  },
  reviewDetailHeader: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  reviewerName: {
    fontWeight: '500',
    color: PRIMARY_GREEN,
  },
  reviewFooter: {
    flex: 1,
    padding: 4,
    justifyContent: 'center',
  },
  reviewFooterText: {
    margin: 20,
    fontWeight: '500',
    color: PRIMARY_GREEN,
    alignSelf: 'center',
    fontSize: 14,
  },
  modalWrapper: {
    flex: 1,
    backgroundColor: 'rgba(0, 0, 0, 0.7)',
    justifyContent: 'center',
  },
  modalContent: {
    width: width * 0.8,
    alignSelf: 'center',
    backgroundColor: 'white',
  },
  modalHeader: {
    height: 40,
    backgroundColor: PRIMARY_GREEN,
    borderBottomWidth: 2,
    borderColor: 'white',
  },
  modalTitle: {
    color: 'white',
    fontWeight: '700',
    alignSelf: 'center',
    fontSize: 14,
    margin: 8,
  },
  modalContentDetail: {
    backgroundColor: 'white',
  },
  modalReviewStarWrapper: {
    alignItems: 'center',
    padding: 6,
  },
  plantList: {
    backgroundColor: 'transparent',
    flexDirection: 'row',
    padding: 4,
  },
  plantPhoto: {
    width: 60,
    height: 60,
    alignSelf: 'center',
    borderRadius: 60 / 2,
    margin: 4,
  },
  plantContent: {
    padding: 4,
    justifyContent: 'center',
    flex: 1,
  },
  plantName: {
    fontSize: 14,
    color: PRIMARY_GREEN,
  },
  plantText: {
    fontSize: 12,
    flexWrap: 'wrap',
    marginTop: 2,
  },
});
