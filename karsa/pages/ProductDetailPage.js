// @flow

import React, {Component} from 'react';
import styles from './ProductDetailPage-style';
import autobind from 'class-autobind';

import {
  View,
  Text,
  Image,
  ScrollView,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Modal,
  TextInput,
  ToastAndroid,
  Share,
} from 'react-native';


import {
  TitleBar,
  SwipeableThumbnail,
  Button,
  StatefulTabGroup,
  Tab,
  TextCard,
  Stars,
  Comment,
  LoadingIndicator,
  SelectableList,
  IconButton,
} from '../core-ui';
import formatPriceRange from '../helpers/formatPriceRange';
import slugify from '../helpers/slugify';
import NearestStoreModal from '../components/NearestStoreModal';

import type {
  ProductDetail,
  Pesticide,
  Seed,
  Fertilizer,
  Review,
  Equipment,
} from '../types/Product';
import type {PlantHeader} from '../types/Plant';

type Props = {
  product: ProductDetail;
  onUseProductPress: (useProduct: {productID: number; plantList: Array<any>}) => any;
  isLoadMoreReviewLoading: boolean;
  onLoadMoreReview: (page: number) => any;
  onReviewSubmit: (reviewObj: {rate: number; review: string}) => any;
  plants: Array<PlantHeader>;
  isProductDetailLoading: boolean;
};
type State = {
  activePhoto: ImageSource;
  isPlantModalShown: boolean;
  activeReviewStars: number;
  isShowReviewModal: boolean;
  reviewText: string;
  isShowNearestStoreModal: boolean;
};
type BasicInfo = {
  producerName: string;
  producerAddress: string;
};

type ReviewProps = {
  reviews: Array<Review>;
  isLoading: boolean;
  onReviewSubmit: (reviewObj: {rate: number; review: string}) => any;
  onLoadMore: (page: number) => any;
};
type ReviewState = {
  activeReviewPage: number;
};

export function PesticideProductDetail(props: Pesticide & BasicInfo) {
  let {
    formulations, color, purity, toxicType, licenseNumber, type,
    howToUse, packing, dose, procedure, commodity, pest, materials,
    producerName, producerAddress,
  } = props;
  return (
    <View style={{flex: 1}}>
      <View style={styles.dataSegment}>
        <TextCard
          title="Bentuk Formulasi"
          style={{flex: 1}}
          textItems={[formulations]}
        />
        <TextCard
          title="No. Ijin"
          style={{flex: 1}}
          textItems={[licenseNumber]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Warna"
          style={{flex: 1}}
          textItems={[color]}
        />
        <TextCard
          title="Konsentrasi"
          style={{flex: 1}}
          textItems={[purity]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Bahan Aktif"
          style={{flex: 1}}
          textItems={[materials]}
        />
        <TextCard
          title="Klasifikasi"
          style={{flex: 1}}
          textItems={[type]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Cara Kerja Racun"
          style={{flex: 1}}
          textItems={[toxicType]}
        />
        <TextCard
          title="Kemasan"
          style={{flex: 1}}
          textItems={[packing]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Komoditas"
          style={{flex: 1}}
          textItems={[commodity]}
        />
        <TextCard
          title="Hama/Penyakit Sasaran"
          style={{flex: 1}}
          textItems={[pest]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Cara Aplikasi"
          style={{flex: 1}}
          textItems={[howToUse]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          isBold={true}
          title="Produsen"
          style={{flex: 1}}
          textItems={[producerName]}
        />
        <TextCard
          title="Alamat Produsen"
          style={{flex: 1}}
          textItems={[producerAddress]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Cara Pemakaian"
          style={{flex: 1}}
          textItems={[procedure]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Dosis Pemakaian"
          style={{flex: 1}}
          textItems={[dose]}
        />
      </View>
    </View>
  );
}
export function FertilizerProductDetail(props: Fertilizer & BasicInfo) {
  let {
    producerAddress, producerName, composition, purity, classification,
    color, licenseNumber, group, excellence, howToUse, dose, procedure,
    netto, warning,
  } = props;
  return (
    <View style={{flex: 1}}>
      <View style={styles.dataSegment}>
        <TextCard
          title="Komposisi"
          style={{flex: 1}}
          textItems={[composition]}
        />
        <TextCard
          title="No. Ijin"
          style={{flex: 1}}
          textItems={[licenseNumber]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Konsentrasi"
          style={{flex: 1}}
          textItems={[purity]}
        />
        <TextCard
          title="Jenis Pupuk Berdasarkan Kandungan"
          style={{flex: 1}}
          textItems={[group]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Klasifikasi"
          style={{flex: 1}}
          textItems={[classification]}
        />
        <TextCard
          title="Warna"
          style={{flex: 1}}
          textItems={[color]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Keunggulan"
          style={{flex: 1}}
          textItems={[excellence]}
        />
        <TextCard
          title="Cara Aplikasi"
          style={{flex: 1}}
          textItems={[howToUse]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Berat Bersih (/Kemasan)"
          style={{flex: 1}}
          textItems={[netto]}
        />
        <TextCard
          title="Peringatan"
          style={{flex: 1}}
          textItems={[warning]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Produsen"
          style={{flex: 1}}
          textItems={[producerName]}
          isBold={true}
        />
        <TextCard
          title="Alamat Produsen"
          style={{flex: 1}}
          textItems={[producerAddress]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Cara Pemakaian"
          style={{flex: 1}}
          textItems={[procedure]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Dosis Pemakaian"
          style={{flex: 1}}
          textItems={[dose]}
        />
      </View>
    </View>
  );
}
export function SeedProductDetail(props: Seed & BasicInfo) {
  let {
    variety, recomendation, sprout, purity, licenseNumber, age,
    weight, durability, potency, netto, plantType, producerName, producerAddress,
  } = props;
  return (
    <View style={{flex: 1}}>
      <View style={styles.dataSegment}>
        <TextCard
          title="Jenis Tanaman"
          style={{flex: 1}}
          textItems={[plantType]}
        />
        <TextCard
          title="Varietas Tanaman"
          style={{flex: 1}}
          textItems={[variety]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="No. Ijin"
          style={{flex: 1}}
          textItems={[licenseNumber]}
        />
        <TextCard
          title="Rekomendasi Dataran"
          style={{flex: 1}}
          textItems={[recomendation]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Daya Kecambah(%)"
          style={{flex: 1}}
          textItems={[sprout.toString()]}
        />
        <TextCard
          title="Informasi Lainnya"
          style={{flex: 1}}
          textItems={[purity]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Umur Panen(HST)"
          style={{flex: 1}}
          textItems={[age.toString()]}
        />
        <TextCard
          title="Hasil Bobot Perbuah(g)"
          style={{flex: 1}}
          textItems={[weight.toString()]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Ketahanan Terhadap Hama dan Penyakit"
          style={{flex: 1}}
          textItems={[durability]}
        />
        <TextCard
          title="Potensi Hasil Panen (Ton/Ha)"
          style={{flex: 1}}
          textItems={[potency]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Isi Bersih (/Kemasan)"
          style={{flex: 1}}
          textItems={[netto]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Produsen"
          style={{flex: 1}}
          textItems={[producerName]}
          isBold={true}
        />
        <TextCard
          title="Alamat Produsen"
          style={{flex: 1}}
          textItems={[producerAddress]}
        />
      </View>
    </View>
  );
}
export class ReviewProduct extends Component {
  props: ReviewProps;
  state: ReviewState;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      activeReviewPage: 1,
    };
  }
  _onLoadMoreReview() {
    let number = this.state.activeReviewPage;
    number++;
    this.props.onLoadMore(number);
    this.setState({
      activeReviewPage: number,
    });
  }

  render() {
    let {reviews, isLoading} = this.props;
    let reviewList = [];
    if (reviews.length > 0) {
      reviewList = reviews.map((item, id) => {
        let {avatar, avatarFB, firstName, rate, review} = item;
        return (
          <Comment key={id} image={avatar ? avatar : avatarFB ? avatarFB : null}>
            <View style={styles.reviewDetailHeader}>
              <Text style={styles.reviewerName}>{firstName}</Text>
              <Stars
                rate={Number.parseFloat(rate)}
                rateMax={5}
                isActive={false}
                size={20}
              />
            </View>
            <Text style={styles.reviewDetailText}>{review}</Text>
          </Comment>
        );
      });
      return (
        <View style={styles.reviewFooter}>
          {isLoading ? <LoadingIndicator /> : reviewList}
          <TouchableOpacity onPress={this._onLoadMoreReview}>
            <Text style={styles.reviewFooterText}>Lihat Ulasan Lebih Banyak</Text>
          </TouchableOpacity>
        </View>
      );
    } else {
      return (
        <View style={styles.reviewFooter}>
          <Text style={styles.reviewFooterText}>Tidak ada ulasan saat ini</Text>
        </View>
      );
    }
  }
}
export function PlantList(props: PlantHeader) {
  let {
    plantName, photo, status, plantCategory, plantVariant, provinceName,
    cityName, subdistrictName, villageName,
  } = props;
  if (status === 'not use') {
    let plantDetail = plantCategory + ', ' + plantVariant;
    let plantAddress = provinceName + ', ' + cityName + ', ' + subdistrictName + ', ' + villageName;
    return (
      <View style={styles.plantList}>
        <Image style={styles.plantPhoto} resizeMode="cover" source={photo} />
        <View style={styles.plantContent}>
          <Text style={styles.plantName}>{plantName}</Text>
          <Text style={styles.plantText}>{plantDetail}</Text>
          <Text style={styles.plantText}>{plantAddress}</Text>
        </View>
      </View>
    );
  }
  return null;
}
export function ToolsProductDetail(props: Equipment & BasicInfo) {
  let {
    sniISO, type, classification, category, dimension, howToUse,
    weight, materials, usefulness, warning, producerAddress, producerName,
  } = props;
  return (
    <View style={{flex: 1}}>
      <View style={styles.dataSegment}>
        <TextCard
          title="Jenis Produk"
          style={{flex: 1}}
          textItems={[category]}
        />
        <TextCard
          title="Tipe Produk"
          style={{flex: 1}}
          textItems={[type]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Dimensi"
          style={{flex: 1}}
          textItems={[dimension]}
        />
        <TextCard
          title="Berat"
          style={{flex: 1}}
          textItems={[weight]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Klasifikasi"
          style={{flex: 1}}
          textItems={[classification]}
        />
        <TextCard
          title="SNI dan ISO"
          style={{flex: 1}}
          textItems={[sniISO]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Kegunaan"
          style={{flex: 1}}
          textItems={[usefulness]}
        />
        <TextCard
          title="Peringatan"
          style={{flex: 1}}
          textItems={[warning]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Material Penyusun"
          style={{flex: 1}}
          textItems={[materials]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Produsen"
          style={{flex: 1}}
          textItems={[producerName]}
        />
        <TextCard
          title="Alamat Produsen"
          style={{flex: 1}}
          textItems={[producerAddress]}
        />
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Cara Pemakaian"
          style={{flex: 1}}
          textItems={[howToUse]}
        />
      </View>
    </View>
  );
}
export default class ProductDetailPage extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isPlantModalShown: false,
      activePhoto: [],
      isShowReviewModal: false,
      activeReviewStars: 0,
      reviewText: '',
      isShowNearestStoreModal: false,
    };
  }
  componentWillReceiveProps(props: Props) {
    let {photo} = props.product;
    if (photo != null) {
      this.setState({
        activePhoto: photo,
      });
    }
  }
  _onShareClick() {
    let detailURL = this.props.product.productCategory;
    let textURL = slugify(this.props.product.name);
    let url = `http://ingkarsa.com/dashboard/info-sarana-tani/${detailURL}/${textURL}`;
    Share.share({
      message: `Lihat detail dari produk ${detailURL} di ${url}`,
    })
    .catch(() => ToastAndroid.show(`Gagal membagikan data ${detailURL}`));
  }
  _onThumbnailPhotoPress(imageData?: ImageSource) {
    if (imageData != null) {
      this.setState({
        activePhoto: imageData,
      });
    }
  }
  _onNearestStorePress() {
    this.setState({
      isShowNearestStoreModal: !this.state.isShowNearestStoreModal,
    });
  }
  _onReviewSubmit() {
    let review = {
      rate: this.state.activeReviewStars,
      review: this.state.reviewText,
    };
    this.props.onReviewSubmit(review);
    this.setState({
      reviewText: '',
      isShowReviewModal: false,
      activeReviewStars: 0,
    });
  }
  _reviewModalPage() {
    return (
      <Modal
        animationType="fade"
        visible={this.state.isShowReviewModal}
        transparent={true}
        onRequestClose={() => {}}
      >
        <TouchableWithoutFeedback
          onPress={() => this.setState({isShowReviewModal: false})}
          style={{flex: 1}}
        >
          <View style={styles.modalWrapper}>
            <TouchableWithoutFeedback>
              <View style={styles.modalContent}>
                <View style={styles.modalHeader}>
                  <Text style={styles.modalTitle}>TAMBAH ULASAN</Text>
                </View>
                <View style={styles.modalContentDetail}>
                  <TextInput
                    style={{height: 80, margin: 4, padding: 2, backgroundColor: 'white'}}
                    multiline
                    onChangeText={(text) => this.setState({reviewText: text})}
                  />
                  <View style={styles.modalReviewStarWrapper}>
                    <Stars
                      rate={this.state.activeReviewStars}
                      rateMax={5}
                      isActive={true}
                      size={50}
                      onStarPress={(rating) => this.setState({activeReviewStars: rating})}
                      isHalfStarEnabled={false}
                    />
                  </View>
                  <Button
                    text="KIRIM"
                    style={{maxHeight: 40, minHeight: 0, height: 40}}
                    onPress={this._onReviewSubmit}
                  />
                </View>
              </View>
            </TouchableWithoutFeedback>
          </View>
        </TouchableWithoutFeedback>
      </Modal>
    );
  }
  render() {
    let {
      product,
      onUseProductPress,
      onReviewSubmit,
      isLoadMoreReviewLoading,
      onLoadMoreReview,
      plants,
      isProductDetailLoading,
    } = this.props;
    if (isProductDetailLoading || product == null) {
      return (
        <View style={{flex: 1, backgroundColor: 'white'}}>
          <TitleBar title="Sarana Tani" iconButtonName="menu" />
          <LoadingIndicator />
        </View>
      );
    }

    let {priceMin, priceMax, description, name, id, producerName, producerCity, reviews} = product;
    let {productCategory, pesticide, seed, fertilizer, equipment, producerAddress, rate, reviewCount} = product;
    let producerPlacement = (producerCity ? producerCity : '');
    let detailItem = null;

    if (productCategory === 'pestisida') {
      detailItem =
        <PesticideProductDetail
          {...pesticide}
          producerName={producerName}
          producerAddress={producerAddress ? producerAddress : ''}
        />;
    } else if (productCategory === 'pupuk') {
      detailItem =
        <FertilizerProductDetail
          {...fertilizer}
          producerName={producerName}
          producerAddress={producerAddress ? producerAddress : ''}
        />;
    } else if (productCategory === 'bibit') {
      detailItem =
        <SeedProductDetail
          {...seed}
          producerName={producerName}
          producerAddress={producerAddress ? producerAddress : ''}
        />;
    } else if (productCategory === 'alat-alat-pertanian') {
      detailItem =
        <ToolsProductDetail
          {...equipment}
          producerName={producerName}
          producerAddress={producerAddress ? producerAddress : ''}
        />;
    }
    let rating = Number.parseFloat(rate);
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <TitleBar title="Sarana Tani" iconButtonName="menu">
          <IconButton icon="share" onPress={this._onShareClick} />
        </TitleBar>
        <NearestStoreModal isShowing={this.state.isShowNearestStoreModal} onClose={this._onNearestStorePress} />
        <ScrollView showsVerticalScrollIndicator={false}>
          <View style={styles.galleryWrapper}>
            <Image
              resizeMode="contain"
              source={this.state.activePhoto}
              style={styles.mainPhoto}
            />
            <SwipeableThumbnail>
              {product.gallery.map((imageData, index) =>
                <Image
                  style={styles.smallPhoto}
                  source={imageData.photoThumb}
                  key={index}
                  onPress={() => this._onThumbnailPhotoPress(imageData.photo)}
                />
              )}
            </SwipeableThumbnail>
          </View>
          <View style={styles.productHeaderWrapper}>
            <Text style={styles.productName}>{name}</Text>
            <Text style={styles.producerName}>{producerName + ', ' + producerPlacement}</Text>
            <Text style={styles.priceTag}>{formatPriceRange(priceMin, priceMax)}</Text>
          </View>
          <View style={styles.reviewHeader}>
            <Stars size={30} rate={rating} rateMax={5} isActive={false} isHalfStarEnabled />
            <Text style={styles.reviewCount}>{reviewCount.toString() + ' Ulasan'}</Text>
          </View>
          <Button
            text="TOKO TERDEKAT"
            onPress={() => {
              this.setState({
                isShowNearestStoreModal: !this.state.isShowNearestStoreModal,
              });
            }}
          />
          <Button
            text="PAKAI PRODUK INI"
            onPress={() => {
              this.setState({
                isPlantModalShown: true,
              });
            }}
          />
          <Button text="TULIS ULASAN" onPress={() => this.setState({isShowReviewModal: true})} />
          {this._reviewModalPage()}
          {this.state.isPlantModalShown ?
            <SelectableList
              title="Daftar Tanamanku"
              renderRow={(plant) => <PlantList {...plant} />}
              data={plants}
              onListConfirm={(listItem) => {
                onUseProductPress({
                  productID: id,
                  plantList: listItem ? listItem : [],
                });
                this.setState({
                  isPlantModalShown: false,
                });
              }}
            /> : null
          }

          <View style={styles.descWrapper}>
            <Text style={styles.descHeader}>Description</Text>
            <Text style={styles.desc}>{description}</Text>
          </View>
          <View style={styles.tabWrapper}>
            <StatefulTabGroup styling="secondary">
              <Tab title="DETAIL">
                {detailItem}
              </Tab>
              <Tab title="ULASAN">
                <View style={{flex: 1, backgroundColor: 'white'}}>
                  <ReviewProduct
                    reviews={reviews}
                    isLoading={isLoadMoreReviewLoading}
                    onReviewSubmit={onReviewSubmit}
                    onLoadMore={onLoadMoreReview}
                  />
                </View>
              </Tab>
            </StatefulTabGroup>
          </View>
        </ScrollView>
      </View>
    );
  }
}
