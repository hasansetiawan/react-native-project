// @flow
import React from 'react';
import {ScrollView, View, Text} from 'react-native';
import formatPriceRange from '../helpers/formatPriceRange';
import CardSwiper, {CardProduct} from '../core-ui/CardSwiper';
import TitleBar from '../core-ui/TitleBar';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import ProductDetail from '../core-ui/ProductDetail';
import styles from './ProductPage-style';
import Button from '../core-ui/Button';

import type {Product} from '../types/Product';

type Props = {
  productsHighlight: Array<Product>;
  productsLatest: Array<Product>;
  productsPopular: Array<Product>;
  navigateToProductList: () => void;
  onPress: () => void;
};

export default function ProductPage(props: Props) {
  let {productsHighlight, productsLatest, productsPopular, onPress, navigateToProductList} = props;
  return (
    <View style={styles.flexOne}>
      <TitleBar iconButtonName="menu" title="Info Sarana Tani" />
      <ScrollView style={styles.flexOne}>
        <Section products={productsHighlight} isHighlight onPress={onPress} />
        <Section title="Produk Terbaru" products={productsLatest} onPress={onPress} />
        <Section title="Produk Terpopuler" products={productsPopular} onPress={onPress} />
        <View style={styles.footer}>
          <View style={styles.viewAllButton} />
        </View>
        <View style={styles.productListWrapper}>
          <Button
            text="LIHAT SEMUA PRODUK"
            onPress={navigateToProductList}
          />
        </View>
      </ScrollView>
    </View>
  );
}

type SectionProps = {
  isHighlight?: boolean;
  products: Array<Product>;
  title?: string;
  onPress: () => void;
};
export function EmptyProduct(props: {type: string}) {
  let {type} = props;
  return (
    <View style={styles.emptyProductWrapper}>
      <Text style={styles.noProductFoundText}>Tidak ada Produk {type}</Text>
    </View>
  );
}
export function Section(props: SectionProps) {
  let {isHighlight, products, title, onPress} = props;
  let highlightProducts = products.length !== 0 && isHighlight ?
    (<CardSwiper>
      {products.map(({name, photo, priceMax, priceMin, producerName, producerCity, id}, i) => (
        <CardProduct
          key={i}
          productName={name}
          priceMax={priceMax}
          priceMin={priceMin}
          producerPhoto={photo}
          producerName={producerName}
          city={producerCity}
          onPress={() => onPress(id)}
        />
      ))}
    </CardSwiper>) : <EmptyProduct type="Highlight" />;
  if (isHighlight) {
    return (
      <View style={styles.highlightWrapper}>
        <View style={styles.boxWrapper}>
          <View style={styles.productHighlightWrapper}>
            {highlightProducts}
          </View>
        </View>
      </View>
    );
  } else {
    return (
      <View style={styles.sectionWrapper}>
        <View style={styles.headerTextWrapper}>
          <Text style={styles.headerText}>{title}</Text>
        </View>
        {products.length === 0 ?
          <LoadingIndicator style={styles.loading} /> :
            <ScrollView style={styles.productWrapper} horizontal>
              {renderProducts(products, onPress)}
            </ScrollView>
        }
      </View>
    );
  }
}

export function renderProducts(products: Array<Product>, onPress: () => void) {
  return products.map((product, i) => {
    let {
      name,
      priceMin,
      priceMax,
      photo,
      producerName,
      producerCity,
      id,
    } = product;

    return (
      <View style={styles.productItemWrapper} key={i}>
        <ProductDetail
          name={name}
          priceRange={formatPriceRange(priceMin, priceMax)}
          photo={photo}
          producerName={producerName}
          producerCity={producerCity} type="product"
          onPress={() => onPress(id)}
        />
      </View>
    );
  });
}
