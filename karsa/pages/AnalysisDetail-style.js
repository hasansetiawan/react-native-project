//@flow

import {StyleSheet} from 'react-native';

const marginField = {
  marginLeft: 15,
  marginRight: 15,
};

const row = {
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
  padding: 3,
};

export default StyleSheet.create({
  row: {
    flexDirection: 'row',
  },
  mainContainer: {
    flex: 1,
  },
  field: {
    height: 400,
  },
  totalBlack: {
    backgroundColor: '#939693',
  },
  totalGrey: {
    backgroundColor: '#e7ece7',
  },
  totalWhite: {
    backgroundColor: '#fff',
  },
  rowTitle: {
    backgroundColor: '#fff',
    borderBottomColor: '#e7ece7',
    borderBottomWidth: 1,
    marginBottom: 2,
  },
  totalPendapatan: {
    backgroundColor: '#585b58',
    marginTop: 10,
    marginBottom: 10,
  },
  textWrapper: {
    ...marginField,
    ...row,
    padding: 7,
    justifyContent: 'space-between',
  },
  rowContainerWhite: {
    flex: 0,
  },
  rowContainerGrey: {
    backgroundColor: '#e7ece7',
    flex: 0,
  },
  textWrapperRow: {
    ...marginField,
    ...row,
  },
  textTotalLeft: {
    fontSize: 14,
    color: '#000',
    textAlign: 'left',
    flex: 1,
  },
  textTotalLeftWhite: {
    fontSize: 14,
    color: '#fff',
    textAlign: 'left',
    flex: 1,
  },
  textTotalRight: {
    fontSize: 14,
    color: '#000',
    textAlign: 'right',
    flex: 1,
  },
  textTotalRightBlack: {
    fontSize: 14,
    color: '#000',
    textAlign: 'right',
    fontWeight: 'bold',
    flex: 1,
  },
  textTotalRightWhite: {
    fontSize: 16,
    color: '#fff',
    textAlign: 'right',
    fontWeight: 'bold',
    flex: 1,
  },
  textTitle: {
    flex: 1,
  },
  textTitleLeft: {
    fontSize: 14,
    color: '#000',
    fontWeight: 'bold',
  },
  textTitleRight: {
    fontSize: 12,
    color: '#7a7d7a',
  },
  rowItem: {
    flex: 1,
  },
  rowSpacer: {
    height: 20,
  },
});
