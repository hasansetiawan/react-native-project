// @flow
import {StyleSheet} from 'react-native';
import {SECONDARY_GREY} from '../constants/color';

export default StyleSheet.create({
  contentWrapper: {
    paddingLeft: 5,
  },
  flex: {
    flex: 1,
  },
  backdrop: {
    backgroundColor: SECONDARY_GREY,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    top: 0,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    color: 'black',
    fontWeight: 'bold',
  },
  additionalInfo: {
    flexDirection: 'column',
    marginTop: 5,
  },
  endAlignedSelf: {
    alignSelf: 'flex-end',
  },
  authorWrapper: {
    flexDirection: 'row',
  },
  flexRow: {
    flexDirection: 'row',
  },
  author: {
    color: 'green',
  },
  blackFont: {
    color: 'black',
  },
  icon: {
    marginLeft: 10,
    marginRight: 10,
    flexDirection: 'row',
  },
  headerSlide: {
    marginLeft: 3,
    marginBottom: 3,
  },
  wrapper: {
    borderRadius: 5,
    marginBottom: 5,
  },
  button: {
    backgroundColor: '#eeeeee',
    padding: 10,
  },
});
