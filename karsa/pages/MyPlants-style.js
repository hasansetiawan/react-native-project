// @flow

import {StyleSheet} from 'react-native';
import {DARK_GREY} from '../constants/color';

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  grid: {
    margin: 5,
  },
  body: {
    paddingHorizontal: 5,
    paddingVertical: 3,
    flex: 1,
  },
  centerFlex: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchBar: {
    marginHorizontal: 15,
  },
  noSearchResult: {
    paddingTop: 25,
    padding: 15,
    justifyContent: 'center',
    alignItems: 'center',
  },
  noSearchResultText: {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '700',
    color: DARK_GREY,
  },
});
