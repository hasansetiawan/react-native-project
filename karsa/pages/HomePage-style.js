import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  lightCyan: {
    backgroundColor: '#33BBA0',
    borderColor: '#039B80',
  },
  lighterGreen: {
    backgroundColor: 'rgb(44, 189, 105)',
    borderColor: 'rgb(14, 159, 75)',
  },
  lightGreen: {
    backgroundColor: 'rgb(120, 196, 68)',
    borderColor: 'rgb(90, 166, 38)',
  },
  green: {
    backgroundColor: 'rgb(31, 184, 53)',
    borderColor: 'rgb(1, 154, 23)',
  },
  darkGreen: {
    backgroundColor: 'rgb(91, 168, 47)',
    borderColor: 'rgb(61, 138, 17)',
  },
  darkerGreen: {
    backgroundColor: 'rgb(30, 151, 92)',
    borderColor: 'rgb(0, 121, 62)',
  },
  body: {
    flex: 1,
  },
  spacer: {
    height: 5,
  },
  menus: {
    flex: 3,
    paddingVertical: 3,
    paddingHorizontal: 4,
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    paddingHorizontal: 3,
  },
  tile: {
    flex: 1,
    margin: 5,
  },
  button: {
    alignItems: 'center',
    padding: 5,
  },
  image: {
    height: 64,
    width: 64,
    backgroundColor: 'transparent',
  },
  buttonText: {
    marginTop: 5,
    color: 'white',
    fontSize: 14,
    fontWeight: '700',
    marginBottom: 8,
    textAlign: 'center',
  },
  headerSlide: {
    marginLeft: 3,
    marginBottom: 5,
  },
});
