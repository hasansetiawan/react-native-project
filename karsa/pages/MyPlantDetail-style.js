// @flow

import {StyleSheet, Dimensions} from 'react-native';
import {PRIMARY_GREEN, PRIMARY_GREY, GREY} from '../constants/color';
const {width} = Dimensions.get('window');

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
  lastPhoto: {
    flexDirection: 'row',
  },
  lastPhotoImage: {
    width: width + 3,
    height: 220,
  },
  gradient: {
    backgroundColor: 'transparent',
    justifyContent: 'flex-end',
    padding: 20,
    bottom: 0,
    left: 0,
    top: 0,
    right: 0,
    position: 'absolute',
  },
  lastPhotoDate: {
    color: 'white',
    fontSize: 14,
    fontWeight: '700',
  },
  dataSegment: {
    borderBottomWidth: 2,
    borderColor: PRIMARY_GREY,
    padding: 10,
  },
  photoImage: {
    width: 60,
    height: 60,
    marginRight: 8,
  },
  greyText: {
    color: GREY,
    fontSize: 12,
    marginBottom: 10,
  },
  plantHeader: {
    marginTop: 10,
  },
  defaultText: {
    fontSize: 14,
    marginVertical: 6,
  },
  headerText: {
    marginTop: 12,
    marginBottom: 6,
    fontSize: 14,
    fontWeight: 'bold',
  },
  subHeaderText: {
    marginTop: 5,
    fontSize: 12,
    fontWeight: 'bold',
  },
  addPhoto: {
    fontSize: 40,
    color: PRIMARY_GREEN,
  },
  addPhotoWrapper: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 60,
    height: 60,
    backgroundColor: PRIMARY_GREY,
  },
  horizontalTextWrapper: {
    flexDirection: 'row',
  },
  detailDataWrapper: {
    flexDirection: 'row',
    borderBottomWidth: 1.3,
    borderColor: PRIMARY_GREY,
    marginVertical: 6,
  },
  detailDataWrapperNoBorder: {
    flexDirection: 'row',
    marginVertical: 6,
  },
  disclaimer: {
    backgroundColor: '#e66888',
    padding: 5,
  },
  disclaimerText: {
    fontStyle: 'italic',
  },
  emptyAnalysis: {
    fontStyle: 'italic',
    fontSize: 14,
  },
  emptyAnalysisContainer: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  noProductText: {
    marginVertical: 20,
    fontWeight: '500',
    alignSelf: 'center',
    fontSize: 12,
  },
  rootProduct: {
    flex: 1,
  },
});
