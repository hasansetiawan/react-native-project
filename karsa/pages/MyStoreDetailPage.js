// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  ProfileHeader,
  ProfileHeaderText,
  StatefulTabGroup,
  Tab,
  Icon,
  Button,
  Dropdown,
  GridView,
  ProductDetail,
  ProductHighlight,
} from '../core-ui';
import autobind from 'class-autobind';
import styles from './MyStoreDetailPage-style';

import type {Value} from '../core-ui/Dropdown';
type Product = {
  picture: ImageSource;
  name: string;
  priceRange: string; // TODO: should change later?
  sellerName: string;
  sellerLocation: string;
  category: string;
  commodity: string;
  highlight: boolean;
};
type Store = {
  name: string;
  address: string;
  phone: string;
  email?: string;
  owner: string;
  productCount: number;
  picture: ImageSource;
  products: Array<Product>;
};
type Props = {
  store: Store;
};

export default function MyStoreDetailPage(props: Props) {
  // TODO: use react-native-parallax-scroll-view
  let {name, owner, productCount, picture, products, ...contacts} = props.store;
  return (
    <View style={styles.mainWrapper}>
      <TitleBar iconButtonName="menu" onIconButtonPress={() => this.props.onMenuPress()} title="Profil Toko">
        <IconButton icon="settings" />
        <IconButton icon="delete" />
      </TitleBar>
      <ScrollView>
        <ProfileHeader image={picture} headerType="large">
          <ProfileHeaderText
            profileTitle={name}
            profileType="store"
            productTotal={productCount}
            personInCharge={owner}
          />
        </ProfileHeader>
        <StatefulTabGroup styling="secondary">
          <Tab title="INFO">
            <StoreInfo {...contacts} />
          </Tab>
          <Tab title="PRODUK">
            <StoreProducts products={products} />
          </Tab>
        </StatefulTabGroup>
      </ScrollView>
    </View>
  );
}

type StoreInfoProps = {
  address: string;
  phone: string;
  email?: string;
};

export function StoreInfo(props: StoreInfoProps) {
  let {address, phone, email} = props;
  let emailRow = email
    ? (
      <View style={styles.rowWrapper}>
        <Icon name="email" style={styles.icon} />
        <Text>{email}</Text>
      </View>
    ) : null;
  return (
    <View>
      <View style={styles.addressInfoWrapper}>
        <Text style={styles.infoTitle}>Alamat</Text>
        <Text style={styles.addressText}>{address}</Text>
      </View>
      <View style={styles.contactInfoWrapper}>
        <Text style={styles.infoTitle}>Kontak</Text>
        <View style={styles.rowWrapper}>
          <Icon name="phone" style={styles.icon} />
          <Text>{phone}</Text>
        </View>
        {emailRow}
      </View>
    </View>
  );
}

type StoreProductsState = {
  selectedCategoryIdx: Value;
  selectedCommodityIdx: Value;
};
type StoreProductsProps = {
  products: Array<Product>;
};

export class StoreProducts extends Component {
  state: StoreProductsState;
  props: StoreProductsProps;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedCategoryIdx: 0,
      selectedCommodityIdx: 0,
    };
  }

  render() {
    let {selectedCategoryIdx, selectedCommodityIdx} = this.state;
    let {products} = this.props;
    let categories = new Set();
    let commodities = new Set();
    products.forEach((product) => {
      categories.add(product.category);
      commodities.add(product.commodity);
    });
    let categoryOptions = [{value: 0, label: 'Semua'}];
    let commodityOptions = [{value: 0, label: 'Semua'}];
    categories.forEach((category) => {
      categoryOptions.push({value: categoryOptions.length, label: category});
    });
    commodities.forEach((commodity) => {
      commodityOptions.push({value: commodityOptions.length, label: commodity});
    });
    return (
      <View>
        <Button
          text="Tambah Produk Jualan"
          onPress={() => {/* TODO: use modal or route to new page */}}
        />
        <View style={styles.filterWrapper}>
          <Text style={styles.filterContent}>FILTER</Text>
          <View style={[styles.filterContent, styles.filterDropdown]}>
            <Dropdown
              label="Kategori Produk"
              selectedValue={selectedCategoryIdx}
              options={categoryOptions}
              onSelect={this._onCategorySelected}
            />
          </View>
          <View style={[styles.filterContent, styles.filterDropdown]}>
            <Dropdown
            label="Komoditas"
            selectedValue={selectedCommodityIdx}
            options={commodityOptions}
            onSelect={this._onCommoditySelected}
            />
          </View>
        </View>
        <GridView
          items={products}
          itemsPerRow={2}
          renderItem={(product, idx) => {
            let highlightBadge = product.highlight
              ? <ProductHighlight icon="bookmark" text="Highlight" />
              : null;
            return (
              <ProductDetail {...product} key={idx} highlight={highlightBadge} />
            );
          }}
        />
      </View>
    );
  }

  _onCategorySelected(category: Value) {
    this.setState({selectedCategoryIdx: category});
  }

  _onCommoditySelected(commodity: Value) {
    this.setState({selectedCommodityIdx: commodity});
  }
}
