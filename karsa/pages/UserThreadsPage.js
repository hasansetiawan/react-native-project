// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  ListView,
} from 'react-native';
import {
  ThreadCard,
  LikeListModal,
} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import autobind from 'class-autobind';
import styles from './ForumPage-style';
import listViewDataSource from '../helpers/listViewDataSource';

import type {Thread} from '../types/Forum';
import type {User, UserLike} from '../types/User';

type Props = {
  threads: Map<number, Thread>;
  currentUser: User;
  isThreadLoading: boolean;
  isLikeThreadLoading: boolean;
  isForumResult: boolean;
  fetchMoreResult: () => void;
  fetchUserThreads: () => void;
  likeThread: (id: number) => void;
  onThreadSelected: (id: number) => void;
};
type State = {
  dataSource: ListView.DataSource;
  likeListVisible: boolean;
  likes: ?Array<UserLike>;
  likingThreadID: number;
  page: number;
};

export default class UserThreadsPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    const ds = listViewDataSource();
    this.state = {
      dataSource: ds.cloneWithRows([]),
      likeListVisible: false,
      likes: null,
      likingThreadID: -1,
      page: 2,
    };
  }

  componentWillMount() {
    this.props.fetchUserThreads(1);
  }

  componentWillReceiveProps(newProps: Props) {
    let {threads} = newProps;
    const ds = listViewDataSource();
    if (threads.size <= 7) {
      this.setState({page: 2});
    }
    let threadArray = Array.from(newProps.threads.values());
    let dataSource = ds.cloneWithRows(threadArray);
    this.setState({dataSource});

    let {likeListVisible, likingThreadID} = this.state;
    if (likeListVisible) {
      let thread = threads.get(likingThreadID);
      if (thread) {
        if (thread.userLikes.length === 0) {
          this.setState({likeListVisible: false});
        } else {
          this.setState({likes: thread.userLikes});
        }
      }
    }
  }

  render() {
    let {dataSource, likes, likeListVisible, likingThreadID} = this.state;
    let {threads, onThreadSelected, currentUser, isLikeThreadLoading, isThreadLoading} = this.props;
    if (threads.size === 0) {
      if (isThreadLoading) {
        return (<LoadingIndicator />);
      }
      return (
        <View style={[styles.mainWrapper, styles.center]}>
          <Text style={{textAlign: 'center'}}>Topik tidak ditemukan.</Text>
        </View>
      );
    }
    return (
      <View style={[styles.mainWrapper, styles.contentPadding]}>
        <LikeListModal
          onClose={this._onCloseLikeList}
          userLikes={likes}
          visible={!isLikeThreadLoading && likeListVisible}
        />
        <ListView
          showsVerticalScrollIndicator={false}
          style={styles.listWrapper}
          enableEmptySections={true}
          dataSource={dataSource}
          onEndReached={this._fetchMoreThreads}
          onEndReachedThreshold={10}
          renderRow={(thread) => (
            <ThreadCard
              thread={thread}
              isLikeThreadLoading={isLikeThreadLoading && likingThreadID === thread.id}
              onCardPress={() => onThreadSelected(thread.id)}
              onCommentPress={() => this._onCommentPress(thread.id)}
              onLikePress={() => this._onLikePress(thread.id)}
              isLiked={thread.userLikes.filter((item) => (item.userID === currentUser.id)).length > 0}
            />)}
            renderFooter={this._renderFooter}
        />
      </View>
    );
  }

  _renderFooter() {
    let {threads, isThreadLoading} = this.props;
    if (isThreadLoading && threads.size > 0) {
      return <LoadingIndicator />;
    }
    return null;
  }

  _fetchMoreThreads() {
    let {threads, fetchUserThreads, isForumResult, fetchMoreResult} = this.props;
    if (isForumResult) {
      fetchMoreResult(threads.size);
    } else {
      let {page} = this.state;
      if (!(threads.size < (page - 1) * 7)) {
        fetchUserThreads(page);
        this.setState({page: page + 1});
      }
    }
  }

  _onLikePress(threadID: number) {
    let {likeThread} = this.props;
    likeThread(threadID);
    this.setState({likingThreadID: threadID, likeListVisible: true});
  }

  _onCommentPress(threadID: number) {
    let {onThreadSelected} = this.props;
    onThreadSelected(threadID, {enterByPressingComment: true});
  }

  _onCloseLikeList() {
    this.setState({likeListVisible: false});
  }
}
