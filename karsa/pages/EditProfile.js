//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View, ScrollView} from 'react-native';
import {TextField, TitleBar, ProfilePicture, Button} from '../core-ui';
import AddressDropdown from './register/AddressDropdown';
import GenderDropdown from './register/GenderDropdown';
import DummyFace from '../images/default-avatar.png';
import styles from './Setting-style';

export default class Setting extends Component {
  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    return (
      <ScrollView style={styles.root}>
        <TitleBar iconButtonName="menu" title="Pengaturan" />
        <View style={styles.contentWrapper}>
          <View style={styles.profilePictureWrapper}>
            <ProfilePicture source={DummyFace} />
          </View>
          <TextField label="Nama Lengkap" placeholder="Masukkan nama lengkap anda" />
          <GenderDropdown />
          <AddressDropdown />
          <TextField label="Nomor HP" placeholder="Masukkan nomor telepon seluler Anda" />
        </View>
        <View style={styles.buttonWrapper}>
          <Button text="Batal" inverted />
          <Button text="Simpan" />
        </View>
      </ScrollView>
    );
  }

  _onSelect(selectedItem) {
    return (item: string | number) => {
      this.setState({
        [selectedItem]: item,
      });
    };
  }
}
