//@flow

import React, {Component} from 'react';
import {StatefulTabGroup, Tab} from '../core-ui';
import {View, Image} from 'react-native';
import LoginPage from './LoginPage';
import RegisterPage from './RegisterPage';
import imageHeader from '../images/header.jpg';
import styles from './LoginPage-style';

export default class LoginRegister extends Component {
  render() {
    let onLoginSubmit = () => null;
    return (
      <View style={styles.mainContainer}>
        <View style={styles.header}>
          <Image source={imageHeader} style={styles.imageHeader} />
        </View>
        <View style={styles.body}>
          <StatefulTabGroup styling="primary">
            <Tab title="MASUK">
              <LoginPage
                isLoading={false}
                onLoginSubmit={onLoginSubmit}
                onFacebookLoginButtonPress={() => {}}
                onGuestLoginButtonPress={() => {}}
              />
            </Tab>
            <Tab title="DAFTAR">
              <RegisterPage />
            </Tab>
          </StatefulTabGroup>
        </View>
      </View>
    );
  }
}
