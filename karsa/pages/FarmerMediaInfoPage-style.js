// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
  tileContainer: {
    flex: 1,
    padding: 4,
  },
});
