import {StyleSheet} from 'react-native';
import {
  PRIMARY_GREEN,
} from '../constants/color';

export default StyleSheet.create({
  highlightWrapper: {
    paddingVertical: 20,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: 'rgb(195, 188, 148)',
  },
  boxWrapper: {
    flex: 1,
    paddingHorizontal: 20,
  },
  productHighlightText: {
    fontSize: 16,
    fontWeight: 'bold',
  },
  productHighlightWrapper: {
    flexDirection: 'row',
  },
  productHighlight: {
    flex: 1,
    height: 150,
  },
  productWrapper: {
    flexDirection: 'row',
    paddingVertical: 10,
    paddingRight: 15,
  },
  productItemWrapper: {
    marginHorizontal: 15,
  },
  headerTextWrapper: {
    flexDirection: 'row',
  },
  headerText: {
    backgroundColor: PRIMARY_GREEN,
    padding: 10,
    borderBottomRightRadius: 15,
    color: 'white',
  },
  flexOne: {
    flex: 1,
  },
  loading: {
    marginVertical: 20,
  },
  emptyProductWrapper: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: 'transparent',
  },
  noProductFoundText: {
    fontSize: 14,
    fontWeight: '700',
  },
  productListWrapper: {
    paddingHorizontal: 15,
    paddingBottom: 10,
  },
});
