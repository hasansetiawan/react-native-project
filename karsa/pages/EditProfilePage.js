// @flow
import React from 'react';
import {View, ScrollView} from 'react-native';
import {ProfilePicture, TitleBar, TextField, Button} from '../core-ui';
import GenderDropdown from '../pages/register/GenderDropdown';
import ProvinceDropdown from '../pages/register/ProvinceDropdown';
import CityDropdown from '../pages/register/CityDropdown';
import styles from './EditProfilePage-style';

type Options = {
  value: string;
  label: string;
};

type TempProfile = {
  avatar: string;
  avatarFb: string;
  name: string;
  gender: string;
  noHp: string;
  allProvinceData: Array<Options>;
  allCitiesData: Array<Options>;
  province: number;
  fullAddress: string;
  city: number;
};

type Props = {
  tempProfile: TempProfile;
  onPicturePress: () => void;
  onSave: () => void;
  onCancel: () => void;
  onSelectGender: () => void;
  onSelect: () => () => void;
  onPhoneInput: () => void;
  onAddressChange: () => void;
  onNameChange: () => void;
};

export default function EditProfilePage(props: Props) {
  let {
    tempProfile,
    onPicturePress,
    onPhoneInput,
    onSelectGender,
    onSave,
    onCancel,
    onAddressChange,
    onNameChange,
    onSelect,
  } = props;
  let {
    avatar,
    avatarFb,
    name,
    province,
    city,
    allProvinceData,
    allCitiesData,
    gender,
    noHp,
    fullAddress,
  } = tempProfile;

  return (
    <View style={styles.mainWrapper}>
      <TitleBar iconButtonName="menu" title="Pengaturan" />
      <ScrollView>
        <View style={styles.contentWrapper}>
          <View style={styles.centerWrapper}>
            <ProfilePicture source={{uri: avatar ? avatar : avatarFb}} onPress={onPicturePress} />
          </View>
          <TextField value={name} label="Nama Lengkap" onChangeText={onNameChange} />
          <GenderDropdown selectedValue={gender} onSelect={onSelectGender} />
          <ProvinceDropdown onSelect={onSelect('province')} allProvinceData={allProvinceData} province={province} />
          <CityDropdown onSelect={onSelect('city')} allCitiesData={allCitiesData} city={city} />
          <TextField label="Alamat Lengkap" value={fullAddress} onChangeText={onAddressChange} />
          <TextField value={noHp} label="Nomor HP" onChangeText={onPhoneInput} />
          <View style={styles.rowWrapper}>
            <Button text="Batal" onPress={onCancel} inverted />
            <Button text="Simpan" onPress={onSave} />
          </View>
        </View>
      </ScrollView>
    </View>
  );
}
