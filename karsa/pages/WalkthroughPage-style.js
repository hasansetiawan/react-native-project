// @flow
import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

let dotStyle = {
  width: 8,
  height: 8,
  borderWidth: 1,
  borderRadius: 8,
  marginHorizontal: 2,
};

let dot = {
  backgroundColor: '#ccc',
  borderColor: '#ccc',
  ...dotStyle,
};

let activeDot = {
  backgroundColor: PRIMARY_GREEN,
  borderColor: PRIMARY_GREEN,
  ...dotStyle,
};

export default StyleSheet.create({
  walktroughView: {
    flex: 1,
    padding: 5,
  },
  swiperWrapper: {
    flex: 4,
    alignItems: 'center',
    paddingTop: 20,
  },
  buttons: {
    flex: 1,
  },
  slide: {
    flex: 1,
    alignItems: 'center',
  },
  slideContent: {
    flex: 1,
    backgroundColor: '#fff',
    borderWidth: 1,
    borderColor: '#ccc',
    borderRadius: 3,
    alignItems: 'center',
  },
  slideImageWrapper: {
    flex: 1,
  },
  slideImage: {
    flex: 1,
    width: null,
    height: null,
  },
  slideTitle: {
    fontWeight: 'bold',
    marginTop: 5,
    paddingHorizontal: 20,
  },
  slideDescription: {
    marginTop: 5,
    paddingHorizontal: 20,
  },
  dot,
  activeDot,
});
