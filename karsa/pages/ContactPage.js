// @flow
import React, {Component} from 'react';
import {View} from 'react-native';

import TitleBar from '../core-ui/TitleBar';
import {StatefulTabGroup, Tab} from '../core-ui';
import ContactAddress from './ContactAddress';
import ContactEmail from './ContactEmail';

import styles from './ContactPage-style';

import type {Office} from './ContactAddress';
import type {EmailMessage} from '../types/Office';

type ContactPageProps = {
  mainOffice: Office;
  branchOffices: Array<Office>;
  isFetchContactLoading: boolean;
  isSubmitEmailLoading: boolean;
  getContactData: () => void;
  onEmailSubmit: (emailmessage: EmailMessage) => void;
};

export default class ContactPage extends Component {
  props: ContactPageProps;

  componentWillMount() {
    this.props.getContactData();
  }

  render() {
    let {mainOffice, isFetchContactLoading, isSubmitEmailLoading, onEmailSubmit} = this.props;
    return (
      <View style={styles.contactView}>
        <TitleBar title="Hubungi Kami" iconButtonName="menu" />
        <StatefulTabGroup styling="primary">
          <Tab title="ALAMAT KANTOR">
            <ContactAddress
              mainOffice={mainOffice}
              isLoading={isFetchContactLoading}
            />
          </Tab>
          <Tab title="KIRIM EMAIL">
            <ContactEmail
              onSubmit={onEmailSubmit}
              isLoading={isSubmitEmailLoading}
            />
          </Tab>
        </StatefulTabGroup>
      </View>
    );
  }
}
