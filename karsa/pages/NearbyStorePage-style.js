// @flow

import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN, DARK_GREY} from '../constants/color';

export default StyleSheet.create({
  locationWrapper: {
    flexDirection: 'row',
    height: 48,
    backgroundColor: PRIMARY_GREEN,
    alignItems: 'center',
    justifyContent: 'center',
  },
  locationText: {
    color: 'white',
    fontWeight: 'bold',
    fontSize: 16,
  },
  root: {
    flex: 1,
  },
  row: {
    flexDirection: 'row',
  },
  storeInfoWrapper: {
    flex: 1,
  },
  storeName: {
    fontWeight: '600',
    fontSize: 16,
  },
  defaultText: {
    fontSize: 12,
    marginVertical: 2,
  },
  addressText: {
    flexWrap: 'wrap',
    marginVertical: 2,
  },
  phoneNumberWrapper: {
    marginVertical: 2,
    flexDirection: 'row',
  },
  phoneNumber: {
    fontSize: 12,
  },
  phoneIcon: {
    fontSize: 16,
    color: DARK_GREY,
    marginRight: 5,
  },
  notFoundWrapper: {
    flex: 1,
    padding: 14,
    alignItems: 'center',
    justifyContent: 'center',
  },
  notFoundText: {
    fontSize: 14,
    fontStyle: 'italic',
    fontWeight: '700',
    color: DARK_GREY,
  },
  locatorContainer: {
    justifyContent: 'center',
  },
});
