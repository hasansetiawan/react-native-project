import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  iconWrapper: {
    flexDirection: 'row',
  },
  iconText: {
    color: 'white',
    fontSize: 16,
    marginHorizontal: 10,
  },
});
