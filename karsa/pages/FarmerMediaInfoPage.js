// @flow

import React from 'react';
import {View} from 'react-native';

import {
  TitleBar,
  TileButtonWrapper,
  TileButton,
} from '../core-ui';
import {
  GREEN,
  LIGHT_CYAN,
  LIGHT_GREEN,
  DARKER_GREEN,
} from '../constants/color';

import path from '../constants/route';
import styles from './FarmerMediaInfoPage-style';

import seedImage from '../images/info_seed.png';
import fertilizerImage from '../images/info_fertilizer.png';
import pesticideImage from '../images/info_pesticide.png';
import toolsImage from '../images/info_farmer_tools.png';

type Props = {
  navigateTo: (routeName: string) => void;
};

export default function FarmerMediaInfoPage(props: Props) {
  let {navigateTo} = props;
  return (
    <View style={styles.root}>
      <TitleBar iconButtonName="menu" title="Info Sarana Tani" />
      <View style={styles.tileContainer}>
        <TileButtonWrapper>
          <TileButton
            onPress={() => navigateTo(path.seedProducts)}
            tileName="Benih"
            image={seedImage}
            imageSize={{width: 80, height: 80}}
            colorStyle={{backgroundColor: GREEN, borderColor: '#008805'}}
          />
          <TileButton
            onPress={() => navigateTo(path.fertilizerProducts)}
            tileName="Pupuk"
            image={fertilizerImage}
            colorStyle={{backgroundColor: LIGHT_CYAN, borderColor: '#038B70'}}
          />
        </TileButtonWrapper>
        <TileButtonWrapper>
          <TileButton
            onPress={() => navigateTo(path.pesticideProducts)}
            tileName="Pestisida"
            image={pesticideImage}
            colorStyle={{backgroundColor: LIGHT_GREEN, borderColor: '#489414'}}
          />
          <TileButton
            onPress={() => navigateTo(path.toolsProduct)}
            tileName="Alat-alat Pertanian"
            image={toolsImage}
            colorStyle={{backgroundColor: DARKER_GREEN, borderColor: '#00672C'}}
          />
        </TileButtonWrapper>
        <View style={styles.flex} />
      </View>
    </View>
  );
}
