//@flow
import React from 'react';
import styles from './NewThreadPage-style';
import ThreadFormContainer from '../containers/ThreadFormContainer';

import {View} from 'react-native';
import {TitleBar} from '../core-ui';

import type {Thread} from '../types/Forum';
import type {ImageData} from '../types/ImageData';
import type {Value} from '../core-ui/Dropdown';

type Props = {
  thread: Thread;
  onEditThread: (categoryID: Value, subCategoryID: Value, title: string, description: string, photo: ?ImageData, threadID: number) => void;
};

export default function EditThreadPage(props: Props) {
  let {thread, onEditThread} = props;
  return (
    <View style={styles.flex}>
      <TitleBar iconButtonName="menu" title="Ubah Topik" />
      <ThreadFormContainer thread={thread} onSubmitPressed={onEditThread} />
    </View>
  );
}
