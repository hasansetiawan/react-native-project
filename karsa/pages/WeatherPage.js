// @flow
import React from 'react';
import {View, Text, Image, ScrollView} from 'react-native';
import {Icon, TitleBar} from '../core-ui';
import moment from 'moment';
import 'moment/locale/id'; // for loading indonenesian locale

import Dropdown from '../core-ui/Dropdown';
import weatherIcon from '../images/icon_weather.png';
import mountainImage from '../images/mountain.jpg';
import type {Value} from '../core-ui/Dropdown';
import styles from './WeatherPage-style';

type CityOption = {
  value: string | number;
  label: string;
  description?: string;
};

type Weather = {
  time: string;
  tempRange: string;
  description: string;
  humidity?: string;
  windSpeed?: string;
  windDirection?: string;
};

type Props = {
  cityOptions: Array<CityOption>;
  todayWeather: Weather;
  nextWeathers: Array<Weather>;
  selectedValue: Value;
  navigator?: Object;
  onCityOptionsSelect: (item: Value) => void;

};

export default function WeatherPage(props: Props) {
  let {cityOptions, todayWeather, nextWeathers} = props;
  let {selectedValue, onCityOptionsSelect} = props;
  let todayDate = moment(todayWeather.time).locale('id').format('dddd, D MMMM YYYY');

  return (
    <View style={styles.wrapper}>
      <TitleBar title="Cuaca" iconButtonName="menu" />
      <View style={styles.dropdown}>
        <Dropdown
          selectedValue={selectedValue}
          options={cityOptions}
          animationType="none"
          textStyle={{color: '#fff'}}
          buttonStyle={{borderBottomColor: '#1ec659'}}
          onSelect={onCityOptionsSelect}
        />
      </View>
      <View style={styles.image}>
      </View>
      <View style={styles.currentWeather}>
        <Image
          source={mountainImage}
          // trick to make local image strech filling parent component
          style={{flex: 1, width: null, height: null}}
          resizeMode="cover"
        >
          <View style={styles.blur}>
            <Text style={styles.currentWeatherDate}>
              {todayDate}
            </Text>
            <View style={styles.currentWeatherMain}>
              <Image
                source={weatherIcon}
                style={{width: 80, height: 80}}
              />
              <View style={styles.currentWeatherContent}>
                <View style={styles.currentWeatherTemperature}>
                  <Text style={styles.currentWeatherTemperatureRange}>
                    {todayWeather.tempRange}
                  </Text>
                  <Text style={styles.currentDegree}>o</Text>
                  <Text style={styles.currentCelcius}>C</Text>
                </View>
                <Text style={styles.description}>{todayWeather.description}</Text>
              </View>
            </View>
            <View style={styles.currentWeatherFooter}>
              <View>
                <Text style={styles.currentWeatherFooterTitle}>Kelembaban</Text>
                <Text style={styles.currentWeatherFooterValue}>
                  {todayWeather.humidity}
                </Text>
              </View>
              <View style={styles.verticalSeparator} />
              <View>
                <Text style={styles.currentWeatherFooterTitle}>Kecepatan Angin</Text>
                <Text style={styles.currentWeatherFooterValue}>
                  {todayWeather.windSpeed}
                </Text>
              </View>
              <View style={styles.verticalSeparator} />
              <View>
                <Text style={styles.currentWeatherFooterTitle}>Arah Angin</Text>
                <Text style={styles.currentWeatherFooterValue}>
                  {todayWeather.windDirection}
                </Text>
              </View>
            </View>
          </View>
        </Image>
      </View>
      <View style={styles.weatherList}>
        <ScrollView>
          {nextWeathers.map((weather, index) => {
            let m = moment(weather.time).locale('id');
            let day = m.format('dddd');
            let date = m.format('DD MMM YYYY');
            let rowStyle = styles.weatherListItem;
            if ((index % 2) === 1) {
              rowStyle = [styles.weatherListItem, styles.altRow];
            }
            return (
              <View key={index} style={rowStyle}>
                <View style={styles.weatherListItemTime}>
                  <Text style={styles.weatherListItemDay}>{day}</Text>
                  <Text style={styles.weatherListItemDate}>{date}</Text>
                </View>
                <View style={styles.weatherListItemTemperature}>
                  <Icon name="wb-sunny" style={styles.weatherIcon} />
                  <Text style={styles.temperatureRange}>{weather.tempRange}</Text>
                  <Text style={styles.degree}>o</Text>
                  <Text style={styles.celcius}>C</Text>
                </View>
                <View style={styles.weatherListItemDescription}>
                  <Text style={styles.weatherListItemDescriptionText}>
                    {weather.description}
                  </Text>
                </View>
              </View>
            );
          })}
        </ScrollView>
      </View>
    </View>
  );
}
