// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import styles from './GroupDetailPage-style';
import listViewDataSource from '../helpers/listViewDataSource';

import {
  View,
  ListView,
  Image,
  Text,
} from 'react-native';
import {
  TitleBar,
  StatefulTabGroup,
  Tab,
  ThreadCard,
  LikeListModal,
  Thumbnail,
  LoadingIndicator,
} from '../core-ui';

import type {User, UserLike} from '../types/User';
import type {Thread, Group, GroupMember} from '../types/Forum';

type State = {
  selectedTab: 'member' | 'thread';
  memberDataSource: ListView.DataSource;
  threadDataSource: ListView.DataSource;
  memberPage: number;
  threadPage: number;
  likingThreadID: number;
  likeListVisible: boolean;
  likes: ?Array<UserLike>;
};

type Props = {
  currentUser: User;
  group: Group;
  isLikeThreadLoading: boolean;
  isFetchingGroupMembersLoading: boolean;
  isFetchingGroupThreadsLoading: boolean;
  fetchGroupMembers: (groupID: number, page: number) => void;
  fetchGroupThreads: (groupID: number, page: number) => void;
  likeThread: (threadID: number) => void;
  onThreadSelected: (threadID: number, props?: {enterByPressingComment: boolean}) => void;
};

export default class GroupDetailPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedTab: 'member',
      memberDataSource: listViewDataSource().cloneWithRows([]),
      threadDataSource: listViewDataSource().cloneWithRows([]),
      memberPage: 2,
      threadPage: 2,
      likingThreadID: -1,
      likeListVisible: false,
      likes: null,
    };
  }

  componentWillReceiveProps(newProps: Props) {
    let {memberDataSource, threadDataSource} = this.state;
    let {group} = newProps;
    if (group && group.members) {
      if (group.members.length > memberDataSource.getRowCount()) {
        this.setState({memberDataSource: listViewDataSource().cloneWithRows(group.members)});
      }
    }
    if (group && group.threads) {
      if (group.threads.length > threadDataSource.getRowCount()) {
        this.setState({threadDataSource: listViewDataSource().cloneWithRows(group.threads)});
      }
    }
  }

  render() {
    let {group, isLikeThreadLoading, isFetchingGroupMembersLoading, isFetchingGroupThreadsLoading} = this.props;
    let {selectedTab, memberDataSource, threadDataSource, likes, likeListVisible} = this.state;
    let emptyMemberData = (isFetchingGroupMembersLoading && memberDataSource.getRowCount() === 0);
    let emptyThreadData = (isFetchingGroupThreadsLoading && threadDataSource.getRowCount() === 0);
    if (!group || emptyMemberData || emptyThreadData) {
      return (
        <View style={[styles.flex, styles.center]}>
          <LoadingIndicator />
        </View>
      );
    }
    return (
      <View style={styles.flex}>
        <TitleBar title="Detail Kelompok Tani" />
        <LikeListModal
          onClose={this._onCloseLikeList}
          userLikes={likes}
          visible={!isLikeThreadLoading && likeListVisible}
        />
        <ListView
          style={[styles.flex, styles.whiteBackground]}
          contentContainerStyle={styles.bottomPadding}
          dataSource={selectedTab === 'member' ? memberDataSource : threadDataSource}
          renderHeader={this._renderHeader}
          renderRow={this._renderRow}
          renderFooter={this._renderFooter}
          onEndReached={this._onEndReached}
          onEndReachedThreshold={10}
          enableEmptySections
        />
      </View>
    );
  }

  _renderHeader() {
    let {group} = this.props;
    return (
      <View>
        <Image resizeMode="cover" resizeMethod="resize" source={group.groupImage} style={styles.groupImage} />
        <View style={[styles.absolute, styles.backdrop]} />
        <View style={[styles.absolute, styles.allPadding]}>
          <View style={styles.center}>
            <Thumbnail image={group.avatar || group.avatarFacebook} shape="round" />
            <Text style={[styles.white, styles.bold, styles.textCenter]}>{group.groupName}</Text>
          </View>
          <View>
            <View style={styles.allPadding}>
              <View style={styles.row}>
                <View style={[styles.flex, styles.center]}>
                  <Text style={[styles.white, styles.biggerText, styles.bold]}>{group.groupMemberCount}</Text>
                  <Text style={styles.white}>Anggota</Text>
                </View>
                <View style={styles.verticalSeparator}></View>
                <View style={[styles.flex, styles.center]}>
                  <Text style={[styles.white, styles.biggerText, styles.bold]}>{group.groupTopicCount}</Text>
                  <Text style={styles.white}>Topik</Text>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={[styles.greyBackground, styles.allPadding]}>
          <Text style={styles.bigText}>Tentang</Text>
          <Text style={styles.smallTopPadding}>{group.groupDescription}</Text>
        </View>
        <View>
          <StatefulTabGroup styling="secondary" onTabSelect={this._onTabSelected}>
            <Tab title="ANGGOTA" />
            <Tab title="TOPIK" />
          </StatefulTabGroup>
        </View>
      </View>
    );
  }

  _renderRow(item) {
    let {selectedTab} = this.state;
    if (selectedTab === 'member') {
      let groupMember: GroupMember = item;
      return (
        <View style={[styles.row, styles.horizontalPadding, styles.topPadding]}>
          <Thumbnail image={groupMember.avatar || groupMember.avatarFacebook} shape="round" />
          <View style={[styles.flex, styles.centerJustified, styles.horizontalPadding]}>
            <Text style={styles.bold}>{groupMember.firstName}</Text>
          </View>
        </View>
      );
    } else if (selectedTab === 'thread') {
      let thread: Thread = item;
      let {currentUser, isLikeThreadLoading, onThreadSelected} = this.props;
      let {likingThreadID} = this.state;
      return (
        <View style={[styles.horizontalPadding, styles.topPadding]}>
          <ThreadCard
            thread={thread}
            isLikeThreadLoading={isLikeThreadLoading && likingThreadID === thread.id}
            onCardPress={() => onThreadSelected(thread.id)}
            onCommentPress={() => this._onCommentPress(thread.id)}
            onLikePress={() => this._onLikePress(thread.id)}
            isLiked={thread.userLikes.filter((item) => (item.userID === currentUser.id)).length > 0}
          />
        </View>
      );
    }
  }

  _renderFooter() {
    let {selectedTab, memberDataSource, threadDataSource} = this.state;
    let {isFetchingGroupMembersLoading, isFetchingGroupThreadsLoading} = this.props;
    if (selectedTab === 'member') {
      if (isFetchingGroupMembersLoading) {
        return (
          <View style={[styles.allPadding, styles.center]}>
            <LoadingIndicator />
          </View>
        );
      } else if (memberDataSource.getRowCount() === 0) {
        return (
          <View style={[styles.allPadding, styles.center]}>
            <Text>Tidak ada anggota.</Text>
          </View>
        );
      }
    } else {
      if (isFetchingGroupThreadsLoading) {
        return (
          <View style={[styles.allPadding, styles.center]}>
            <LoadingIndicator />
          </View>
        );
      } else if (threadDataSource.getRowCount() === 0) {
        return (
          <View style={[styles.allPadding, styles.center]}>
            <Text>Tidak ada topik.</Text>
          </View>
        );
      }
    }
  }

  _onLikePress(threadID: number) {
    let {likeThread} = this.props;
    likeThread(threadID);
    this.setState({likingThreadID: threadID, likeListVisible: true});
  }

  _onCommentPress(threadID: number) {
    let {onThreadSelected} = this.props;
    onThreadSelected(threadID, {enterByPressingComment: true});
  }

  _onCloseLikeList() {
    this.setState({likeListVisible: false});
  }

  _onEndReached() {
    let {selectedTab, memberPage, threadPage, memberDataSource, threadDataSource} = this.state;
    let {group, fetchGroupMembers, fetchGroupThreads} = this.props;
    if (selectedTab === 'member') {
      if (memberDataSource.getRowCount() < group.groupMemberCount) {
        fetchGroupMembers(group.id, memberPage);
        this.setState({memberPage: memberPage + 1});
      }
    } else {
      if (threadDataSource.getRowCount() < group.groupTopicCount) {
        fetchGroupThreads(group.id, threadPage);
        this.setState({threadPage: threadPage + 1});
      }
    }
  }

  _onTabSelected(selectedIdx: number) {
    this.setState({selectedTab: selectedIdx === 0 ? 'member' : 'thread'});
  }
}
