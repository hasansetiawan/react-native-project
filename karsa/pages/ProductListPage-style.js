// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  utilityWrapper: {
    padding: 4,
    marginBottom: 8,
  },
  productRow: {
    flex: 1,
    flexDirection: 'row',
    padding: 10,
  },
  noProductText: {
    fontSize: 14,
    alignSelf: 'center',
    fontWeight: '700',
  },
  headerText: {
    fontWeight: '500',
    marginLeft: 10,
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  searchTextContainer: {
    flex: 1,
  },
  searchButton: {
    color: 'black',
  },
});
