import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN, PRIMARY_GREY, ALTERNATE_GREY, GREY} from '../constants/color';

export default StyleSheet.create({
  mainWrapper: {
    backgroundColor: 'white',
  },
  flex: {
    flex: 1,
  },
  zeroFlex: {
    flex: 0,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  textBold: {
    fontWeight: 'bold',
  },
  textItalic: {
    fontStyle: 'italic',
  },
  rowWrapper: {
    alignItems: 'center',
    flexDirection: 'row',
  },
  columnWrapper: {
    flexDirection: 'column',
  },
  line: {
    borderColor: PRIMARY_GREY,
    borderWidth: 0.5,
    height: 1,
  },
  padding: {
    padding: 7,
  },
  zeroPadding: {
    padding: 0,
  },
  greyBackground: {
    backgroundColor: ALTERNATE_GREY,
  },
  marginTop: {
    marginTop: 7,
  },
  marginRight: {
    marginRight: 7,
  },
  verticalMargin: {
    marginVertical: 7,
  },
  green: {
    color: PRIMARY_GREEN,
  },
  commentsWrapper: {
    paddingBottom: 10,
  },
  commentHeaderText: {
    padding: 7,
  },
  commentInputWrapper: {
    backgroundColor: ALTERNATE_GREY,
    elevation: 7,
    padding: 5,
  },
  commentButton: {
    marginTop: 10,
    maxHeight: 30,
    minHeight: 30,
    width: 70,
  },
  disabled: {
    backgroundColor: GREY,
    borderColor: GREY,
  },
  avatarWrapper: {
    alignSelf: 'flex-start',
  },
  avatar: {
    borderRadius: 25,
    height: 50,
    width: 50,
  },
  avatarIcon: {
    fontSize: 50,
  },
  paddingWithoutBottom: {
    paddingHorizontal: 10,
    paddingTop: 10,
  },
  noComment: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  commentPostDate: {
  },
  smallLoader: {
    flex: 0,
    padding: -1,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.75)',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
