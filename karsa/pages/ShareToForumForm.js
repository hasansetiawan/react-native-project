// @flow
import React, {Component} from 'react';
import {View, Text, Modal, ScrollView, ToastAndroid} from 'react-native';
import {
  Dialog,
  Dropdown,
  TextField,
  ResponsiveImage,
} from '../core-ui';
import autobind from 'class-autobind';
import styles from './ShareToForumForm-style';

import type {Option} from '../core-ui/Dropdown';
import type {Plant} from '../types/Plant';

export type FormInput = {
  threadCategory: number;
  threadTitle: string;
  threadContent: string;
};

type Props = {
  isVisible: boolean;
  forumCategoryOptions: Array<Option>;
  forumSubCategoryOptions: Array<Option>;
  isSubmitThreadLoading: boolean;
  plant: Plant;
  onCategorySelected: (categoryID: number) => void;
  onClose: () => void;
  onSubmit: (plantID: number, categoryID: number, subCategoryID: number, title: string, description: string) => void;
};

type State = {
  threadCategory: number;
  threadSubCategory: number;
  threadTitle: string;
  threadContent: string;
  submitted: boolean;
};

export default class ShareToForumForm extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      threadCategory: 0,
      threadSubCategory: 0,
      threadTitle: '',
      threadContent: '',
      submitted: false,
    };
  }

  componentWillReceiveProps(newProps: Props) {
    let {submitted} = this.state;
    let {isVisible, isSubmitThreadLoading} = newProps;
    if (isVisible && submitted && !isSubmitThreadLoading) {
      this.props.onClose();
    }
  }

  _handleSubmit() {
    let {threadCategory, threadSubCategory, threadTitle, threadContent} = this.state;
    if (threadCategory === 0 || threadSubCategory === 0 || threadTitle === '' || threadContent === '') {
      return ToastAndroid.show('Mohon lengkapi data terlebih dahulu', ToastAndroid.SHORT);
    }
    let {id} = this.props.plant;
    this.props.onSubmit(id, threadCategory, threadSubCategory, threadTitle, threadContent);
    this.setState({submitted: true});
  }

  render() {
    let {isVisible, forumCategoryOptions, forumSubCategoryOptions, isSubmitThreadLoading, plant, onClose} = this.props;
    let plantLocation;
    if (plant.plantAddressGps && !plant.plantAddressGps.startsWith('Tidak')) {
      plantLocation = plant.plantAddressGps;
    } else if (plant.villageName && plant.subdistrictName && plant.cityName && plant.provinceName) {
      plantLocation = `${plant.villageName}, ${plant.subdistrictName}, ${plant.cityName}, ${plant.provinceName}`;
    }
    return (
      <Modal
        animationType="slide"
        transparent={true}
        visible={isVisible}
        onRequestClose={onClose}
      >
        <View style={styles.outer}>
          <View style={styles.inner}>
            <Dialog
              title="Bagikan ke Forum"
              buttonText="Bagikan"
              buttonLoading={isSubmitThreadLoading}
              onClose={onClose}
              onSubmit={this._handleSubmit}
              flexContent
            >
              <ScrollView keyboardShouldPersistTaps>
                <View style={[styles.dialog, styles.content]}>
                  <Dropdown
                    label="Kategori Forum"
                    options={forumCategoryOptions}
                    selectedValue={this.state.threadCategory}
                    onSelect={(value) => {
                      this.setState({threadCategory: Number(value), threadSubCategory: 0});
                      this.props.onCategorySelected(Number(value));
                    }}
                  />
                  <Dropdown
                    label="Subkategori Forum"
                    options={forumSubCategoryOptions}
                    selectedValue={this.state.threadSubCategory}
                    onSelect={(value) => this.setState({threadSubCategory: Number(value)})}
                  />
                  <TextField
                    label="Judul Topik"
                    placeholder="Masukkan judul untuk topik Anda"
                    value={this.state.threadTitle}
                    onChangeText={(text) => this.setState({threadTitle: text})}
                  />
                  <View style={styles.attachedData}>
                    <Text>Lampiran Data Tanaman</Text>
                    {
                      plant.photo && typeof plant.photo === 'object' && !Array.isArray(plant.photo) ? (
                        <ResponsiveImage source={plant.photo} />
                      ) : null
                    }
                    <View style={{flexDirection: 'row'}}>
                      <View style={styles.data}>
                        <Text style={styles.label}>Tanaman</Text>
                        <Text style={styles.text}>{plant.plantName}, {plant.plantCategory}, {plant.plantVariant}</Text>
                      </View>
                      <View style={styles.data}>
                        <Text style={styles.label}>Tanggal Tanam</Text>
                        <Text style={styles.text}>{plant.date} (usia {plant.plantAge})</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={styles.data}>
                        <Text style={styles.label}>Metode Tanam</Text>
                        <Text style={styles.text}>{plant.plantingMethod}</Text>
                      </View>
                      <View style={styles.data}>
                        <Text style={styles.label}>Ketinggian Tanah</Text>
                        <Text style={styles.text}>{plant.landHeight}</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={styles.data}>
                        <Text style={styles.label}>Lokasi Penanaman</Text>
                        <Text style={styles.text}>{plantLocation}</Text>
                      </View>
                    </View>
                    <View style={{flexDirection: 'row'}}>
                      <View style={styles.data}>
                        <Text style={styles.label}>Keterangan</Text>
                        <Text style={styles.text}>
                          {plant.description}
                        </Text>
                      </View>
                    </View>
                  </View>
                  <TextField
                    label="Isi Topik"
                    placeholder="Masukkan isi topik Anda"
                    value={this.state.threadContent}
                    onChangeText={(text) => this.setState({threadContent: text})}
                  />
                </View>
              </ScrollView>
            </Dialog>
          </View>
        </View>
      </Modal>
    );
  }
}
