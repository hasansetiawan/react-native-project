// @flow
import React, {Component} from 'react';
import {
  View,
  Image,
} from 'react-native';
import {
  StatefulTabGroup,
  Tab,
} from '../core-ui';
import styles from './InitialPage-style';
import autobind from 'class-autobind';
import LoginPageContainer from '../containers/LoginPageContainer';
import RegisterPageContainer from '../containers/RegisterPageContainer';
import karsaImage from '../images/header.jpg';


type State = {

};
type Props = {

};

export default class LoginPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    return (
      <View style={styles.mainContainer}>
        <Image source={karsaImage} style={styles.image} resizeMode="stretch" />
        <StatefulTabGroup styling="primary">
          <Tab title="LOGIN">
            <LoginPageContainer />
          </Tab>
          <Tab title="DAFTAR">
            <RegisterPageContainer />
          </Tab>
        </StatefulTabGroup>
      </View>
    );
  }
}
