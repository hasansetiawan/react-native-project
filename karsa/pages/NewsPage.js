//@flow
import React, {Component} from 'react';
import styles from './NewsPage-style';
import datePrettify from '../helpers/datePrettify';
import autobind from 'class-autobind';

import {View, Text, ListView, ToastAndroid, Share} from 'react-native';
import {ImageSwiper, ImageSlide, ImageSlideOverlay, LoadingIndicator, TitleBar, ThumbnailCard, IconButton} from '../core-ui';

let {DataSource} = ListView;

import type {News, NewsItem} from '../types/News';

type Props = {
  news: Array<NewsItem>;
  newsHighlight: Array<NewsItem>;
  showSwiper?: boolean;
  showSticky?: boolean;
  onNewsSelect: () => void;
  onHighlightSelect: () => void;
  fetchMoreNews: () => void;
};

type State = {
  refreshing: boolean;
  page: number;
  ds: DataSource;
};

export default class NewsPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      ds: new DataSource({rowHasChanged: (r1, r2) => r1 !== r2}),
      refreshing: false,
      page: 1,
    };
  }

  render() {
    let {news, newsHighlight, onNewsSelect, onHighlightSelect} = this.props;
    let {refreshing} = this.state;
    if (news.length === 0) {
      return <LoadingIndicator />;
    }
    return (
      <View style={{flex: 1, backgroundColor: 'white'}}>
        <TitleBar title="Berita" />
        <ListView
            showsVerticalScrollIndicator={false}
            enableEmptySections={true}
            onEndReachedThreshold={10}
            onEndReached={this._onRefresh}
            renderHeader={() => (
              <View style={styles.headerSlide}>
                <Swiper newsSwiper={newsHighlight} onHighlightSelect={onHighlightSelect} />
              </View>
            )}
            renderFooter={() => {
              if (refreshing) {
                return <LoadingIndicator />;
              }
            }}
            dataSource={this.state.ds.cloneWithRows(news)}
            renderRow={(news) => (
              <NewsItemComponent
                key={news.id} news={news} onPress={() => onNewsSelect(news.id)}
                onSharePress={() => this._shareNews({
                  contentType: 'link',
                  contentUrl: news.url ? news.url : `http://www.ingkarsa.com/dashboard/berita/${news.slug}`,
                  contentDescription: news.title,
                })}
              />
            )}
        />
      </View>
    );
  }

  componentWillReceiveProps() {
    this.setState({refreshing: false});
  }

  _onRefresh() {
    let {page} = this.state;
    let {fetchMoreNews} = this.props;
    fetchMoreNews(page);
    this.setState({
      page: page + 1,
      refreshing: true,
    });
  }

  _shareNews(shareContent) {
    let {contentUrl, contentDescription} = shareContent;
    Share.share({
      message: `Baca berita "${contentDescription}" selengkapnya di ${contentUrl}`,
    })
    .catch(() => ToastAndroid.show(`Gagal membagikan berita`, ToastAndroid.SHORT));
  }
}

type NewsHighlightProps = {
  newsSwiper: Array<NewsItem>;
  onHighlightSelect: () => void;
};

class Swiper extends Component {
  props: NewsHighlightProps;

  render() {
    let {newsSwiper, onHighlightSelect} = this.props;
    return (
      <View>
        <LoadingIndicator style={[styles.backdrop, styles.center]} />
        <ImageSwiper>
          {
            newsSwiper.map((news) => {
              let {createdAt, author, writer, title, foto, urlImageRss, id, url, slug} = news;
              let shareContent = {
                contentType: 'link',
                contentUrl: url || `http://www.ingkarsa.com/dashboard/berita/${slug}`,
                contentDescription: title,
              };
              let date = datePrettify(createdAt);
              return (
                <ImageSlide key={id} picture={foto || urlImageRss} onPress={() => onHighlightSelect(id)}>
                  <ImageSlideOverlay date={date} source={writer || author} title={title} shareContent={shareContent} />
                </ImageSlide>
              );
            })
          }
        </ImageSwiper>
      </View>
    );
  }
}

function NewsItemComponent({news, onPress, onSharePress}: {news: News; onPress: () => void; onSharePress: () => void}) {
  let {title, createdAt, author, foto, urlImageRss} = news;
  return (
    <ThumbnailCard shape="square" onPress={onPress} image={foto || urlImageRss}>
      <View style={styles.contentWrapper}>
        <Text style={styles.title}>{title}</Text>
        <View style={styles.additionalInfo}>
          <Text>{datePrettify(createdAt)}</Text>
          <View style={styles.authorWrapper}>
            <Text>oleh: </Text>
            <Text style={styles.author}>{author}</Text>
          </View>
          <View style={styles.flexRow}>
            <View style={styles.flex} />
            <IconButton
              icon="share" text="Bagikan"
              style={styles.blackFont} textStyle={styles.blackFont} containerStyle={styles.endAlignedSelf}
              onPress={onSharePress}
            />
          </View>
        </View>
      </View>
    </ThumbnailCard>
  );
}
