//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View, ScrollView, Text, Alert} from 'react-native';
import {
  Dropdown,
  TextField,
  Button,
  ResponsiveImage,
  Icon,
  LoadingIndicator,
} from '../core-ui';
import ImagePicker from 'react-native-image-picker';
import styles from './NewThreadPage-style';
import mapObjectToDropdownOptions from '../helpers/mapObjectToDropdownOptions';

import type {Thread, ThreadCategory, ThreadSubCategory} from '../types/Forum';
import type {ImageData} from '../types/ImageData';
import type {Value} from '../core-ui/Dropdown';

type Props = {
  categories: Map<number, ThreadCategory>;
  subCategories: Map<number, ThreadSubCategory>;
  isSubmitThreadLoading: boolean;
  thread?: Thread;
  onCategorySelected: (categoryID: Value) => void;
  onSubmitPressed: (categoryID: Value, subCategoryID: Value, title: string, description: string, photo: ?ImageData, threadID?: number) => void;
};

type State = {
  selectedCategoryID: Value;
  selectedSubCategoryID: Value;
  title: string;
  description: string;
  displayPhoto: boolean;
  photo: ?ImageData;
};

export default class ThreadForm extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    let {thread} = this.props;
    if (thread) {
      this.state = {
        selectedCategoryID: 0,
        selectedSubCategoryID: 0,
        title: thread.title,
        description: thread.description,
        displayPhoto: thread.photo ? true : false,
        photo: null,
      };
    } else {
      this.state = {
        selectedCategoryID: 0,
        selectedSubCategoryID: 0,
        title: '',
        description: '',
        displayPhoto: false,
        photo: null,
      };
    }
  }

  render() {
    let {categories, subCategories, isSubmitThreadLoading, thread} = this.props;
    let {selectedCategoryID, selectedSubCategoryID, photo, title, description, displayPhoto} = this.state;
    let photoURI;
    if (photo) {
      photoURI = {uri: photo.uri};
    } else if (thread && thread.photo) {
      if (typeof thread.photo === 'object' && !Array.isArray(thread.photo)) {
        photoURI = {uri: thread.photo.uri};
      }
    }
    let categoryOptions = mapObjectToDropdownOptions(categories, {value: 'id', label: 'categoryName'});
    let subCategoryOptions = mapObjectToDropdownOptions(subCategories, {value: 'id', label: 'subCategoryName'});
    return (
      <ScrollView style={[styles.flex, styles.scrollWrapper]} showsVerticalScrollIndicator={false} keyboardShouldPersistTaps>
        <Text style={[styles.topPadding, styles.bold]}>
          Kategori <Text style={styles.redFont}>*</Text>
        </Text>
        <Dropdown
          emptyOptionsText="Data kategori belum tersedia"
          placeholder="Pilih Kategori"
          style={styles.bottomPadding}
          selectedValue={selectedCategoryID}
          options={categoryOptions}
          onSelect={this._onCategorySelected}
        />
        <Dropdown
          emptyOptionsText="Date subkategori belum tersedia"
          placeholder="Pilih Subkategori"
          style={styles.bottomPadding}
          selectedValue={selectedSubCategoryID}
          options={subCategoryOptions}
          onSelect={this._onSubCategorySelected}
        />
        <Text style={[styles.topPadding, styles.bold]}>
          Judul <Text style={styles.redFont}>*</Text>
        </Text>
        <TextField
          placeholder="Judul Thread"
          autoCapitalize="words"
          value={title}
          onChangeText={this._onTitleTextChanged}
        />
        <Text style={[styles.topPadding, styles.bold]}>
          Isi Thread <Text style={styles.redFont}>*</Text>
        </Text>
        <TextField
          placeholder="Isi Thread"
          autoCapitalize="sentences"
          style={styles.multilineTextInput}
          value={description}
          onChangeText={this._onDescriptionTextChanged}
          numberOfLines={3}
          multiline
        />
        {
          displayPhoto ? (
            <View>
              <ResponsiveImage
                renderIndicator={() => <LoadingIndicator />}
                source={photoURI}
              />
              <View style={styles.overlay}>
                <Icon
                  name="cancel"
                  hitSlop={{top: 10, bottom: 10, left: 10, right: 10}}
                  style={styles.cancel}
                  onPress={this._onRemoveAttachedPhoto}
                />
              </View>
            </View>
          ) : null
        }
        <Button
          text="UNGGAH FOTO"
          style={styles.topMargin}
          onPress={this._onAddPhotoPressed}
          inverted
        />
        {
          isSubmitThreadLoading ? (
            <Button style={styles.bottomMostMargin} disabled inverted>
              <LoadingIndicator style={styles.smallLoader} small />
            </Button>
          ) : (
            <Button
              text="SELESAI"
              style={styles.bottomMostMargin}
              onPress={this._onSubmitPressed}
            />
          )
        }
      </ScrollView>
    );
  }

  _onTitleTextChanged(title: string) {
    this.setState({title});
  }

  _onDescriptionTextChanged(description: string) {
    this.setState({description});
  }

  _onCategorySelected(selectedCategoryID: Value) {
    this.setState({selectedCategoryID});
    this.props.onCategorySelected(selectedCategoryID);
  }

  _onSubCategorySelected(selectedSubCategoryID: Value) {
    this.setState({selectedSubCategoryID});
  }

  _onAddPhotoPressed() {
    ImagePicker.showImagePicker(null, (image) => {
      if (!image.didCancel) {
        this.setState({photo: image, displayPhoto: true});
      }
    });
  }

  _onRemoveAttachedPhoto() {
    this.setState({displayPhoto: false});
  }

  _onSubmitPressed() {
    let {onSubmitPressed, thread} = this.props;
    let {selectedCategoryID, selectedSubCategoryID, title, description, photo} = this.state;
    if (selectedCategoryID === 0 || selectedSubCategoryID === 0 || title === '' || description === '') {
      Alert.alert('Data Tidak Lengkap', 'Mohon isi data yang diberi tanda bintang');
    } else {
      let threadID;
      if (thread) {
        threadID = thread.id;
      }
      onSubmitPressed(selectedCategoryID, selectedSubCategoryID, title, description, photo, threadID);
    }
  }
}
