// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  htmlView: {
    flex: 1,
    paddingHorizontal: 10,
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderLeftColor: '#AAA',
    borderRightColor: '#AAA',
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
  },
  accordionHeader: {
    paddingVertical: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
    alignItems: 'center',
  },
  accordionHeaderText: {
    fontWeight: 'bold',
  },
  loadingContainer: {
    padding: 15,
    borderBottomWidth: 1,
    borderBottomColor: '#CCC',
    borderLeftWidth: 10,
    borderRightWidth: 10,
    borderLeftColor: '#AAA',
    borderRightColor: '#AAA',
  },
});
