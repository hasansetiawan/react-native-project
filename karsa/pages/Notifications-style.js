//@flow
import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: '#fff',
  },
  listWrapper: {
    marginLeft: 5,
  },
  itemWrapper: {
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  userText: {
    fontWeight: 'bold',
    fontSize: 16,
    color: PRIMARY_GREEN,
  },
  content: {
    flex: 1,
    flexWrap: 'wrap',
  },
});
