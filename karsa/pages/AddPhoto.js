// @flow
import React, {Component} from 'react';
import {
  Text,
  View,
  Image,
} from 'react-native';
import {
  ButtonRow,
  Button,
  Dropdown,
  TitleBar,
} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import styles from './LoginPage-style';
import autobind from 'class-autobind';

import type {Option} from '../core-ui/Dropdown';
import type {GalleryData} from '../types/Plant';

type State = GalleryData;
type Props = {
  isLoading: boolean;
  onPressCancel: () => void;
  onPicturePress: () => void;
  onPressSave: () => void;
  plants: Array<Option>;
  newPhoto: GalleryData;
  selectedPlant: number;
  isAddPhoto: boolean;
};

export default class AddPhotoPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    let {selectedPlant} = this.props;
    this.state = {
      ...this.props.newPhoto,
      plantID: selectedPlant ? selectedPlant : 0,
    };
  }
  render() {
    let {isLoading, onPressCancel, onPicturePress, plants, newPhoto, isAddPhoto} = this.props;
    let addPhotoState = this.state;
    if (isLoading) {
      return <LoadingIndicator />;
    }
    const propsPlantsDropdown = {
      defaultValue: '0',
      options: plants,
      animationType: 'none',
      label: 'Jenis Tanaman',
    };
    let photo = newPhoto.photo ? <Image source={{uri: newPhoto.photo}} style={styles.photoWrap} resizeMode="stretch" /> : <Text></Text>;
    let PlantsDropdown = isAddPhoto ? <Dropdown {...propsPlantsDropdown} onSelect={this._onSelectPlant} selectedValue={addPhotoState.plantID} /> : null;
    return (
      <View>
        <TitleBar iconButtonName="menu" title="Tambah Foto Tanaman" />
        <View style={styles.mainContainer}>
          <View style={styles.field}>
            <View style={styles.photoContainer}>
              {photo}
            </View>
            <Text style={{fontWeight: 'bold'}}>Foto</Text>
            <Button
              text="PILIH FOTO"
              onPress={() => onPicturePress()}
            />
            {PlantsDropdown}
            <View style={{height: 10}} />
            <ButtonRow>
              <Button
                text="CANCEL"
                textStyle={styles.centeredButton}
                onPress={() => onPressCancel()}
                inverted
              />
              <Button text="SELESAI" textStyle={styles.centeredButton} onPress={() => this._onSubmit()} />
            </ButtonRow>
          </View>
        </View>
      </View>
    );
  }
  _onSelectPlant(value: string | number): void {
    this.setState({
      plantID: value,
    });
  }
  _onSubmit(): void {
    let {newPhoto, onPressSave} = this.props;
    let {plantID} = this.state;
    let photoData = {
      ...this.state,
      ...newPhoto,
      plantID: plantID,
      photo: newPhoto.photo,
    };
    onPressSave(photoData);
  }
}
