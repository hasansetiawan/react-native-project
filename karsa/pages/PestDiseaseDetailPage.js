// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  ToastAndroid,
  Share,
} from 'react-native';

import {
  Accordion,
  AccordionContent,
  ImageSwiper,
  ImageSlide,
  SwipeableThumbnail,
  ProductDetail,
  ProductHighlight,
  LoadingIndicator,
  TitleBar,
  IconButton,
} from '../core-ui';
import autobind from 'class-autobind';
import formatPriceRange from '../helpers/formatPriceRange';
import slugify from '../helpers/slugify';
import styles from './PestDiseaseDetailPage-style';

import type {PestDiseaseDetailWithCountermeasure} from '../types/PestDisease';

type Props = {
  detail: PestDiseaseDetailWithCountermeasure;
  isPestDiseaseDetailLoading: boolean;
  onProductPress: (id: number) => any;
};
type State = {
  isCausesCollapsed: boolean;
  isSymptomsCollapsed: boolean;
  isTreatmentsCollapsed: boolean;
};

export default class PestDiseaseDetail extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isCausesCollapsed: true,
      isSymptomsCollapsed: true,
      isTreatmentsCollapsed: true,
    };
  }
  _onShareClick() {
    let detailURL = '';
    if (this.props.detail.type === 'Hama') {
      detailURL = 'hama';
    } else if (this.props.detail.type === 'Penyakit') {
      detailURL = 'penyakit';
    }
    let textURL = slugify(this.props.detail.name);
    let url = `http://ingkarsa.com/dashboard/${detailURL}-detail/${textURL}`;

    Share.share({
      message: `Lihat detail dari produk ${detailURL} di ${url}`,
    })
    .catch(() => ToastAndroid.show(`Gagal membagikan data ${this.props.detail.type}`, ToastAndroid.SHORT));
  }
  render() {
    let {detail, isPestDiseaseDetailLoading, onProductPress} = this.props;
    if (isPestDiseaseDetailLoading) {
      return (
        <LoadingIndicator />
      );
    }
    let {gallery, type, name, description, causes, symptoms, treatments,
      countermeasures,
    } = detail;
    let {isCausesCollapsed, isSymptomsCollapsed, isTreatmentsCollapsed} = this.state;
    let highlightBadge = <ProductHighlight icon="bookmark" text="Highlight" />;
    let swiperImages = gallery.map((item, index) => {
      if (typeof item.photoThumb === 'string') {
        let photoThumb = {uri: item.photoThumb};
        return (<ImageSlide key={index} picture={photoThumb} />);
      } else {
        return (<ImageSlide key={index} picture={item.photoThumb} />);
      }
    });
    return (
      <View style={styles.wrapper}>
        <TitleBar title={type} iconButtonName="menu">
          <IconButton icon="share" onPress={() => this._onShareClick()} />
        </TitleBar>
        <ScrollView>
          <ImageSwiper>
            {swiperImages}
          </ImageSwiper>
          <View style={styles.infoWrapper}>
            <View style={styles.headerWrapper}>
              <Text style={styles.headerText}>{name}</Text>
            </View>
            <View style={styles.headerWrapper}>
              <Text>{description}</Text>
            </View>
            <Accordion textStyle={{fontWeight: '500'}} title="Penyebab" isCollapsed={isCausesCollapsed} onPress={this._onCausesPress}>
              <AccordionContent>
                <Text>{causes}</Text>
              </AccordionContent>
            </Accordion>
            <View style={styles.accordionSpacer} />
            <Accordion textStyle={{fontWeight: '500'}} title="Gejala Serangan" isCollapsed={isSymptomsCollapsed} onPress={this._onSymptomsPress}>
              <AccordionContent>
                {symptoms.map((symptom, index) => this._renderInfoRow(symptom, index))}
              </AccordionContent>
            </Accordion>
            <View style={styles.accordionSpacer} />
            <Accordion textStyle={{fontWeight: '500'}} title="Pengendalian" isCollapsed={isTreatmentsCollapsed} onPress={this._onTreatmentsPress}>
              <AccordionContent>
                {treatments.map((treatment, index) => this._renderInfoRow(treatment, index))}
              </AccordionContent>
            </Accordion>
            <View style={styles.accordionSpacer} />
            {
              countermeasures != null && countermeasures.length !== 0 ? (
                <SwipeableThumbnail>
                  {
                    countermeasures.map((product, index) => {
                      let {highlight, priceMin, priceMax, ...others} = product;
                      let priceRange = formatPriceRange(priceMin, priceMax);
                      let onPress = () => {
                        onProductPress(product.id);
                      };
                      return highlight
                        ? <ProductDetail {...others} priceRange={priceRange} key={index} onPress={onPress} highlight={highlightBadge} style={styles.products} />
                        : <ProductDetail {...others} priceRange={priceRange} key={index} onPress={onPress} style={styles.products} />;
                    })
                  }
                </SwipeableThumbnail>
              ) : <Text>Produk tidak ditemukan</Text>
            }
          </View>
        </ScrollView>
      </View>
    );
  }

  _onCausesPress() {
    let {isCausesCollapsed} = this.state;
    let flag = !isCausesCollapsed;
    this.setState({
      isCausesCollapsed: flag,
    });
  }

  _onSymptomsPress() {
    let {isSymptomsCollapsed} = this.state;
    this.setState({
      isSymptomsCollapsed: !isSymptomsCollapsed,
    });
  }

  _onTreatmentsPress() {
    let {isTreatmentsCollapsed} = this.state;
    this.setState({
      isTreatmentsCollapsed: !isTreatmentsCollapsed,
    });
  }

  _renderInfoRow(text: string, index: number) {
    return (
      <View key={index} style={styles.rowWrapper}>
        <View style={styles.bullet} />
        <Text style={styles.textWrapper}>{text}</Text>
      </View>
    );
  }
}
