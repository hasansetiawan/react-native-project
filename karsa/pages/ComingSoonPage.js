// @flow
import React from 'react';
import {View, Text, Image} from 'react-native';
import {connect} from 'react-redux';

import Button from '../core-ui/Button';

import styles from './ComingSoonPage-style';

type Props = {
  onBackButtonPress: () => void;
};

export function ComingSoonPage(props: Props) {
  return (
    <View style={styles.root}>
      <View style={styles.imageWrapper}>
        <Image
          source={require('../images/weather-soon.png')}
          style={styles.image}
        />
      </View>
      <View style={styles.textWrapper}>
        <Text style={styles.title}>Akan Segera Hadir</Text>
        <Text style={styles.description}>
          Halaman yang anda tuju akan segera hadir.
        </Text>
        <Button
          text="Kembali ke Dashboard"
          style={styles.button}
          onPress={props.onBackButtonPress}
        />
      </View>
    </View>
  );
}

const mapDispatchToProps = (dispatch) => {
  return {
    onBackButtonPress: () => {
      dispatch({type: 'RESET_ROUTE', key: 'homepage'});
    },
  };
};

export default connect(null, mapDispatchToProps)(ComingSoonPage);
