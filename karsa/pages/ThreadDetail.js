// @flow
import React, {Component} from 'react';
import {
  View,
  ScrollView,
  ListView,
  Text,
  Image,
  TouchableOpacity,
  Alert,
  Modal,
  ToastAndroid,
  Share,
} from 'react-native';
import autobind from 'class-autobind';
import {
  Button,
  IconButton,
  Icon,
  LikeListModal,
  LoadingIndicator,
  TextField,
  ThreadCard,
  TitleBar,
  ReportDropdown,
} from '../core-ui';
import datePrettify from '../helpers/datePrettify';
import styles from './ThreadDetail-style';
import listViewDataSource from '../helpers/listViewDataSource';

import type {Thread, Comment} from '../types/Forum';
import type {User, UserLike} from '../types/User';

type Props = {
  currentUser: User;
  selectedThreadID: number;
  thread: Thread;
  forum: 'user' | 'other';
  isThreadDetailLoading: boolean;
  isLikeThreadLoading: boolean;
  isLikeCommentLoading: boolean;
  isPostCommentLoading: boolean;
  isThreadCommentsLoading: boolean;
  isReportForumLoading: boolean;
  enterByPressingComment?: boolean;
  onLikeThread: (threadID: number) => void;
  onLikeComment: (commentID: number) => void;
  onPostComment: (threadID: number, post: string) => void;
  onFetchComments: (threadID: number, page: number) => void;
  onEditThread: (thread: Thread) => void;
  onDeleteThread: (threadID: number) => void;
};

type State = {
  commentsDataSource: ListView.DataSource;
  comment: string;
  likingCommentID: number;
  likeListVisible: boolean;
  commentLikeListVisible: boolean;
  likes: ?Array<UserLike>;
  commentLikes: ?Array<UserLike>;
  commentPage: number;
};

const hitSlop = {
  top: 10,
  bottom: 10,
  left: 10,
  right: 10,
};

export default class ThreadDetail extends Component {
  props: Props;
  state: State;
  scrollRef: ScrollView;
  scrollEnd: number = 0;

  constructor() {
    super(...arguments);
    const ds = listViewDataSource();
    this.state = {
      commentsDataSource: ds.cloneWithRows([]),
      comment: '',
      likingCommentID: -1,
      likeListVisible: false,
      commentLikeListVisible: false,
      likes: null,
      commentLikes: null,
      commentPage: 2,
    };
    autobind(this);
  }

  componentWillReceiveProps(newProps: Props) {
    let {thread} = newProps;
    if (thread) {
      let {comments} = thread;
      let {likeListVisible, commentLikeListVisible, likingCommentID} = this.state;
      if (comments) {
        let commentArray = Array.from(comments.values()).sort((a, b) => a.id - b.id);
        const ds = listViewDataSource();
        this.setState({
          commentsDataSource: ds.cloneWithRows(commentArray),
        });
      }
      if (likeListVisible) {
        if (thread.userLikes.length === 0) {
          this.setState({likeListVisible: false});
        } else {
          this.setState({likes: thread.userLikes});
        }
      }
      if (commentLikeListVisible) {
        let comment = (comments) ? comments.get(likingCommentID) : null;
        let likes = (comment) ? comment.userLikes : [];
        if (likes.length === 0) {
          this.setState({commentLikeListVisible: false});
        } else {
          this.setState({commentLikes: likes});
        }
      }
    }
  }

  render() {
    let {
      isLikeThreadLoading, isLikeCommentLoading, isThreadDetailLoading, isPostCommentLoading,
      currentUser, thread, isReportForumLoading,
    } = this.props;
    let {likeListVisible, commentLikeListVisible, likes, commentLikes} = this.state;
    let iconButtons = [<IconButton icon="share" onPress={this._onSharePress} key={1} />];
    if (thread && currentUser.id === thread.userID) {
      iconButtons.push(<IconButton icon="edit" onPress={this._onEditPress} key={2} />);
      iconButtons.push(<IconButton icon="delete" onPress={this._onDeletePress} key={3} />);
    }
    if (isThreadDetailLoading) {
      return (
        <View style={[styles.mainWrapper, styles.flex]}>
          <TitleBar title="Forum Diskusi" iconButtonName="menu" >
            {iconButtons}
          </TitleBar>
          <View style={[styles.flex, styles.center]}>
            <LoadingIndicator />
          </View>
        </View>
      );
    }
    if (!thread || !thread.title) {
      return (
        <View style={[styles.mainWrapper, styles.flex]}>
          <TitleBar title="Forum Diskusi" iconButtonName="menu" >
            {iconButtons}
          </TitleBar>
          <View style={[styles.flex, styles.center]}>
            <Text style={[styles.textBold, styles.textItalic]}>Thread tidak ditemukan.</Text>
          </View>
        </View>
      );
    }
    return (
      <View style={[styles.mainWrapper, styles.flex]}>
        <Modal
          visible={isReportForumLoading}
          supportedOrientations={['portrait']}
          onRequestClose={() => {}}
          transparent
        >
          <View style={[styles.flex, styles.backdrop]}>
            <LoadingIndicator />
          </View>
        </Modal>
        <TitleBar title="Forum Diskusi" iconButtonName="menu" >
          {iconButtons}
        </TitleBar>
        <LikeListModal userLikes={likes} onClose={this._onCloseModal} visible={!isLikeThreadLoading && likeListVisible} />
        <LikeListModal userLikes={commentLikes} onClose={this._onCloseModal} visible={!isLikeCommentLoading && commentLikeListVisible} />
        <ListView
          stle={styles.flex}
          contentContainerStyle={styles.commentsWrapper}
          enableEmptySections={true}
          dataSource={this.state.commentsDataSource}
          renderHeader={this._renderHeader}
          renderRow={this._renderComment}
          renderFooter={this._renderFooter}
          onContentSizeChange={this._onContentSizeChanged}
          showsVerticalScrollIndicator={false}
          ref={this._onScrollRef}
        />
        <View style={[styles.rowWrapper, styles.commentInputWrapper]}>
          <View style={styles.flex}>
            <TextField
              placeholder="Tulis Komentar"
              value={this.state.comment}
              onChangeText={this._onCommentTextChange}
            />
          </View>
          {
            isPostCommentLoading ? (
              <Button style={[styles.zeroFlex, styles.commentButton]} inverted>
                <LoadingIndicator style={styles.smallLoader} small />
              </Button>
            ) : (
              <Button
                text="KIRIM"
                style={[styles.zeroFlex, styles.commentButton, this.state.comment === '' ? styles.disabled : null]}
                disabled={this.state.comment === ''}
                onPress={this._onPostComment}
              />
            )
          }
        </View>
      </View>
    );
  }

  _renderHeader() {
    let {currentUser, thread, isLikeThreadLoading, isThreadCommentsLoading} = this.props;
    let {categoryName, subCategoryName, latestComment} = thread;
    let lastComment = (
      <Text>
        Balasan Terakhir: Belum ada komentar
      </Text>
    );
    if (latestComment) {
      let {firstName, date} = latestComment;
      lastComment = (
        <Text>
          Balasan Terakhir: <Text style={styles.green}>{firstName}</Text>, pada {date}
        </Text>
      );
    }
    let loadMoreComments;
    if (thread.comments && thread.comments.size < thread.commentCount) {
      loadMoreComments = (
        <TouchableOpacity style={[styles.padding, styles.greyBackground, styles.center]} onPress={this._onFetchMoreComments}>
          <Text style={[styles.textBold]}>Muat komentar sebelumnya</Text>
        </TouchableOpacity>
      );
    }
    if (isThreadCommentsLoading) {
      loadMoreComments = (
        <LoadingIndicator style={[styles.padding, styles.greyBackground]} small />
      );
    }
    return (
      <View>
        <View style={styles.paddingWithoutBottom}>
          <Text>
            Kategory: <Text style={styles.green}>{categoryName}, {subCategoryName}</Text>
          </Text>
          {lastComment}
        </View>
        <ThreadCard
          isDetail
          thread={thread}
          isOwn={currentUser.id === thread.userID}
          isLikeThreadLoading={isLikeThreadLoading}
          onLikePress={this._onLikePress}
          onCommentPress={this._onCommentPress}
          isLiked={thread.userLikes.filter((item) => (item.userID === currentUser.id)).length > 0}
        />
        <Text style={styles.commentHeaderText}>Komentar</Text>
        <View style={[styles.line, styles.flex]} />
        {loadMoreComments}
      </View>
    );
  }

  _renderFooter() {
    if (this.state.commentsDataSource.getRowCount() === 0) {
      return (
        <View style={[styles.flex, styles.noComment, styles.marginTop]}>
          <Text>Belum ada komentar.</Text>
        </View>
      );
    }
  }

  _onCommentTextChange(comment: string) {
    this.setState({comment});
  }

  _renderComment(commentItem: Comment, sectionID, rowID: number) {
    let {id, userID, firstName, avatar, avatarFacebook, post, createdAt, likeCount, userLikes} = commentItem;
    let {thread, currentUser, isLikeCommentLoading} = this.props;
    let {likingCommentID} = this.state;
    let userAvatar;
    if (avatar != null) {
      userAvatar = (
        <Image
          resizeMethod="resize"
          style={styles.avatar}
          source={avatar}
        />
      );
    } else if (avatarFacebook != null) {
      userAvatar = (
        <Image
          resizeMethod="resize"
          style={styles.avatar}
          source={avatarFacebook}
        />
      );
    }
    let likeIconStyle = [styles.marginRight];
    if (userLikes.filter((like) => like.userID === currentUser.id).length > 0) {
      likeIconStyle.push(styles.green);
    }
    return (
      <View key={rowID} style={styles.paddingWithoutBottom}>
        <View style={styles.rowWrapper}>
          <View style={[styles.columnWrapper, styles.avatarWrapper, styles.marginRight]}>
            {userAvatar}
          </View>
          <View style={[styles.columnWrapper, styles.flex]}>
            <View style={styles.rowWrapper}>
              <View style={styles.flex}>
                <Text style={styles.green}>{firstName}</Text>
              </View>
              <Text style={styles.commentPostDate}>{datePrettify(createdAt)}</Text>
            </View>
            <Text style={styles.flex}>{post}</Text>
            <View style={styles.rowWrapper}>
              {
                (isLikeCommentLoading && likingCommentID === id) ? (
                  <LoadingIndicator style={styles.smallLoader} small />
                ) : (
                  <TouchableOpacity hitSlop={hitSlop} onPress={() => this._onLikeComment(id)}>
                    <View style={[styles.rowWrapper, styles.verticalMargin]}>
                      <Icon style={likeIconStyle} name="thumb-up" />
                      <Text style={likeIconStyle}>Suka {likeCount}</Text>
                    </View>
                  </TouchableOpacity>
                )
              }
              <View style={styles.flex} />
              {
                currentUser.id !== userID ? (
                  <ReportDropdown threadID={thread.id} commentID={id} />
                ) : null
              }
            </View>
          </View>
        </View>
        <View style={[styles.flex, styles.marginTop, styles.line]} />
      </View>
    );
  }

  _onLikeComment(id: number) {
    this.props.onLikeComment(id);
    this.setState({likingCommentID: id, commentLikeListVisible: true});
  }

  _onCloseModal() {
    this.setState({
      likeListVisible: false,
      commentLikeListVisible: false,
      likes: null,
      commentLikes: null,
    });
  }

  _onScrollRef(ref) {
    this.scrollRef = ref;
  }

  _onContentSizeChanged(width, height) {
    this.scrollEnd = height;
    if (this.props.enterByPressingComment) {
      this._onCommentPress();
    }
  }

  _onFetchMoreComments() {
    let {thread, onFetchComments} = this.props;
    let {commentPage} = this.state;
    onFetchComments(thread.id, commentPage);
    this.setState({commentPage: commentPage + 1});
  }

  _onSharePress() {
    let {thread} = this.props;
    let url = `http://www.ingkarsa.com/dashboard/forum/detail/${thread.slug}`;
    Share.share({
      message: `Baca forum diskusi "${thread.title}" selengkapnya di ${url}`,
    })
    .catch(() => ToastAndroid.show(`Gagal membagikan forum diskusi`, ToastAndroid.SHORT));
  }

  _onLikePress() {
    let {thread, onLikeThread} = this.props;
    onLikeThread(thread.id);
    this.setState({likeListVisible: true});
  }

  _onCommentPress() {
    this.scrollRef.scrollTo({y: this.scrollEnd});
  }

  _onPostComment() {
    let {thread, onPostComment} = this.props;
    onPostComment(thread.id, this.state.comment);
    this.setState({comment: ''});
  }

  _onEditPress() {
    let {thread, onEditThread} = this.props;
    onEditThread(thread);
  }

  _onDeletePress() {
    let {thread, forum, onDeleteThread} = this.props;
    Alert.alert('Topik', 'Hapus Topik?', [
      {text: 'Iya', onPress: () => onDeleteThread(thread.id, forum)},
      {text: 'Tidak'},
    ]);
  }
}
