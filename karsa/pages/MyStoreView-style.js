// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
  },
  banner: {
    height: 100,
  },
  listWrapper: {
    flex: 1,
    marginVertical: 5,
  },
});
