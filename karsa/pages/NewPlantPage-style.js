//@flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  body: {
    paddingHorizontal: 10,
  },
  recommendation: {
    flexDirection: 'row',
    backgroundColor: '#8E8',
    paddingVertical: 15,
    paddingLeft: 10,
    marginVertical: 5,
  },
  lowerBody: {
    paddingHorizontal: 10,
  },
  buttons: {
    marginVertical: 10,
    paddingHorizontal: 10,
    flexDirection: 'row',
    justifyContent: 'space-between',
  },
  fieldArea: {
    flexDirection: 'row',
    alignItems: 'flex-end',
  },
  fieldAreaInput: {
    flex: 1,
  },
  fieldAreaUnit: {
    flexDirection: 'row',
  },
  fieldAreaMeter: {
    marginLeft: 10,
    paddingBottom: 8,
  },
  fieldAreaExponent: {
    fontSize: 10,
  },
  dateContainer: {
    marginTop: 10,
  },
  inputLabel: {
    fontWeight: 'bold',
  },
  inputItem: {
    paddingLeft: 15,
  },
  dateField: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  dateText: {
    flex: 3,
    paddingLeft: 15,
  },
  datePickerButton: {
    flex: 2,
  },
  location: {
    marginTop: 8,
  },
  locationTextContainer: {
    paddingLeft: 28,
    marginTop: 10,
  },
  locationText: {
  },
  photoContainer: {
    marginTop: 10,
  },
  photo: {
    marginHorizontal: 25,
    marginVertical: 10,
  },
});
