//@flow
import React, {Component} from 'react';
import {View, Text} from 'react-native';
import {ContentStepper, IconButton, TitleBar} from '../../core-ui';
import styles from './RegisterStep3Farmer-style';
import PlantInfoForm from './PlantInfoForm';

const steps = [
  {isActive: false, label: 'Data Pribadi'},
  {isActive: true, label: 'Data Pekerjaan'},
];

export default class RegisterStep3Farmer extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="Daftar">
          <IconButton icon="keyboard-arrow-right" text="Lewati tahap ini" />
        </TitleBar>
        <ContentStepper steps={steps} />
        <View style={styles.desc}>
          <Text style={styles.descText}>Silahkan masukkan data tanaman (jika ada) atau Anda bisa melewati tahap ini dan mengisinya kembali nanti</Text>
        </View>
        <PlantInfoForm />
      </View>
    );
  }
}
