//@flow

import React from 'react';
import {Dropdown} from '../../core-ui';
import {View} from 'react-native';

const propsDropdown = {
  defaultValue: '1',
  options: [
    {value: '1', label: 'Laki-laki'},
    {value: '2', label: 'Perempuan'},
  ],
  label: 'Jenis Kelamin',
  animationType: 'none',
};

type Props = {
  selectedValue?: string | number;
  onSelect?: () => void;
};

export default function GenderDropdown(props: Props) {
  let {selectedValue, onSelect} = props;
  let numSelected = selectedValue === 'Laki-laki' ? '1' : '2';
  return (
    <View>
      <Dropdown {...propsDropdown}
        onSelect={onSelect}
        selectedValue={numSelected}
        placeholder="Pilih jenis kelamin"
        label="Jenis Kelamin"
      />
    </View>
  );
}
