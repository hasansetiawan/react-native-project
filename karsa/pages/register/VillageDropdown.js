//@flow

import React, {Component} from 'react';
import {Dropdown} from '../../core-ui';
import {View} from 'react-native';

const propsDropdown = {
  defaultValue: '1',
  options: [
    {value: '1', label: 'Curug'},
    {value: '2', label: 'Pagedangan'},
  ],
  label: 'Desa',
  animationType: 'none',
};

type State = {
  selectedValue: string | number;
};

export default class VillageDropdown extends Component {
  state: State;
  constructor() {
    super(...arguments);
    this.state = {
      selectedValue: propsDropdown.defaultValue,
    };
  }

  render() {
    let {selectedValue} = this.state;
    return (
      <View>
        <Dropdown {...propsDropdown}
          onSelect={this._onSelect.bind(this)}
          selectedValue={selectedValue}
        />
      </View>
    );
  }
  _onSelect(item: string | number): void {
    this.setState({
      selectedValue: item,
    });
  }
}
