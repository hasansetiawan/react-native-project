//@flow

import React from 'react';
import {Dropdown} from '../../core-ui';
import {View} from 'react-native';

type Props = {
  selectedValue?: number;
  allCitiesData: Array<Object>;
  city: number;
  onSelect?: () => void;
};

export default function CityDropdown(props: Props) {
  let {allCitiesData, city, onSelect} = props;
  let propsDropdown = {
    defaultValue: '1',
    label: 'Kota/Kabupaten',
    options: allCitiesData,
    animationType: 'none',
  };

  return (
    <View>
      <Dropdown {...propsDropdown}
        onSelect={onSelect}
        selectedValue={city}
      />
    </View>
  );
}
