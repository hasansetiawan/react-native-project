//@flow

import React from 'react';
import {Dropdown} from '../../core-ui';
import {View} from 'react-native';

type Props = {
  selectedValue?: number;
  allProvinceData: Array<Object>;
  province: number;
  onSelect?: () => void;
};

export default function ProvinceDropdown(props: Props) {
  let {allProvinceData, province, onSelect} = props;
  let propsDropdown = {
    defaultValue: '1',
    label: 'Provinsi',
    options: allProvinceData,
    animationType: 'none',
  };

  return (
    <View>
      <Dropdown {...propsDropdown}
        onSelect={onSelect}
        selectedValue={province}
      />
    </View>
  );
}
