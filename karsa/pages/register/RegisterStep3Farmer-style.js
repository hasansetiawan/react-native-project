//@flow

import {StyleSheet} from 'react-native';

const marginField = {
  marginLeft: 15,
  marginRight: 15,
};

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  formContainer: {
    ...marginField,
  },
  field: {
    height: 60,
    marginBottom: 10,
  },
  button: {
    height: 60,
    marginBottom: 10,
    marginTop: 20,
  },
  desc: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#eff2ed',
    height: 60,
    marginBottom: 10,
  },
  recomended: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#a1ddc0',
    height: 30,
    marginBottom: 10,
  },
  descText: {
    fontStyle: 'italic',
    fontSize: 12,
  },
});
