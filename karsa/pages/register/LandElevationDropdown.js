//@flow

import React, {Component} from 'react';
import {Dropdown} from '../../core-ui';

const propsDropdown = {
  defaultValue: '1',
  options: [
    {value: '1', label: 'Rendah (0-500 mdpl)'},
    {value: '2', label: 'Rendah (500-1500 mdpl)'},
  ],
  label: 'Ketinggian Dataran Lahan',
  animationType: 'none',
};

type State = {
  selectedValue: string | number;
};

export default class LandElevationDropdown extends Component {
  state: State;
  constructor() {
    super(...arguments);
    this.state = {
      selectedValue: propsDropdown.defaultValue,
    };
  }

  render() {
    let {selectedValue} = this.state;
    return (
      <Dropdown {...propsDropdown}
        onSelect={this._onSelect.bind(this)}
        selectedValue={selectedValue}
      />
    );
  }
  _onSelect(item: string | number): void {
    this.setState({
      selectedValue: item,
    });
  }
}
