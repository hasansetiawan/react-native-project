//@flow
import React, {Component} from 'react';
import {Dropdown} from '../../core-ui';
import {View, Text} from 'react-native';
import AddressData from '../../data/addressData.json';
import autobind from 'class-autobind';

type State = {
  selectedProvince: ?number;
  selectedCity: ?number;
  selectedDistrict: ?number;
};

export default class AddressDropdown extends Component {
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      selectedProvince: null,
      selectedCity: null,
      selectedDistrict: null,
    };
  }
  render() {
    let {selectedProvince, selectedCity, selectedDistrict, ...otherProps} = this.state;

    let defaultOptions = [{value: '', label: ''}];
    let provinceOptions = getProvinces();
    let cityOptions = getCities(selectedProvince) || defaultOptions;
    let districtOptions = getDistricts(selectedProvince, selectedCity) || defaultOptions;

    return (
      <View {...otherProps}>
        <Text style={{fontWeight: 'bold'}}>Alamat</Text>
        <Dropdown
          placeholder="Pilih Provinsi"
          onSelect={this._onSelect('selectedProvince')}
          options={provinceOptions}
          selectedValue={selectedProvince}
        />
        <Dropdown
          placeholder="Pilih Kota"
          onSelect={this._onSelect('selectedCity')}
          options={cityOptions}
          selectedValue={selectedCity}
        />
        <Dropdown
          placeholder="Pilih Kecamatan"
          onSelect={this._onSelect('selectedDistrict')}
          options={districtOptions}
          selectedValue={selectedDistrict}
        />
        <Dropdown
          placeholder="Pilih Kecamatan"
          options={defaultOptions}
          onSelect={() => {}}
        />
      </View>
    );
  }

  _onSelect(selectedItem) {
    return (item: string | number) => {
      this.setState({
        [selectedItem]: item,
      });
    };
  }
}

function getProvinces() {
  return Object.keys(AddressData).map((key) => ({value: key, label: AddressData[key].name}));
}

function getCities(selectedProvince) {
  if (selectedProvince) {
    return AddressData[selectedProvince].cities.map((city) => ({value: city.id, label: city.name}));
  }
}

function getDistricts(selectedProvince, selectedCity) {
  let list = [];
  if (selectedProvince && selectedCity) {
    AddressData[selectedProvince].cities.forEach((city) => {
      if (city.id === selectedCity) {
        list = city.districts.map((district) => ({value: Number(district.id), label: district.name}));
      }
    });
  }
  return list;
}
