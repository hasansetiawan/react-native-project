//@flow

import React, {Component} from 'react';
import {Dropdown} from '../../core-ui';

const propsDropdown = {
  defaultValue: '1',
  options: [
    {value: '1', label: 'Tanaman 1'},
    {value: '2', label: 'Tanaman 2'},
  ],
  label: 'Kategori Tanaman',
  animationType: 'none',
};

type State = {
  selectedValue: string | number;
};

export default class PlantsCategoryDropdown extends Component {
  state: State;
  constructor() {
    super(...arguments);
    this.state = {
      selectedValue: propsDropdown.defaultValue,
    };
  }

  render() {
    let {selectedValue} = this.state;
    return (
      <Dropdown {...propsDropdown}
        onSelect={this._onSelect.bind(this)}
        selectedValue={selectedValue}
      />
    );
  }
  _onSelect(item: string | number): void {
    this.setState({
      selectedValue: item,
    });
  }
}
