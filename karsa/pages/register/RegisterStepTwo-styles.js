//@flow

import {StyleSheet} from 'react-native';

const marginField = {
  marginLeft: 15,
  marginRight: 15,
};

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  formContainer: {
    flex: 0.5,
    ...marginField,
  },
  button: {
    height: 40,
  },
  field: {
    height: 80,
  },
  profile: {
    margin: 5,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 10,
  },
});
