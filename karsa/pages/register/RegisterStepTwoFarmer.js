//@flow
import React, {Component} from 'react';
import {View, ScrollView} from 'react-native';
import {TextField, Button, ProfilePicture, TitleBar, ContentStepper} from '../../core-ui';
import styles from './RegisterStepTwo-styles';
import GenderDropdown from './GenderDropdown';
import ProvinceDropdown from './ProvinceDropdown';
import CityDropdown from './CityDropdown';
import DistrictDropdown from './DistrictDropdown';
import VillageDropdown from './VillageDropdown';
import autobind from 'class-autobind';
import NormalCheckbox from '../../core-ui/Checkbox';

const steps = [
  {isActive: true, label: 'Data Pribadi'},
  {isActive: false, label: 'Data Pekerjaan'},
];

type State = {
  isAgreement: boolean;
};

export default class RegisterStepTwo extends Component {
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      isAgreement: false,
    };
  }

  _toggleAgreementCB() {
    this.setState({
      isAgreement: !this.state.isAgreement,
    });
  }

  render() {
    let {isAgreement} = this.state;
    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="Daftar" />
        <ContentStepper steps={steps} />
        <View style={styles.profile}>
          <ProfilePicture />
        </View>
        <View style={styles.formContainer}>
          <ScrollView>
            <View style={styles.field}>
              <TextField placeholder="Masukan nama lengkap Anda" label="Nama Lengkap" />
            </View>
            <View style={styles.field}>
              <GenderDropdown />
            </View>
            <View style={styles.field}>
              <TextField placeholder="Masukan Alamat Anda" multiline={true} label="Alamat" />
            </View>
            <View style={styles.field}>
              <ProvinceDropdown allProvinceData={[{value: 4, label: ''}]} province={1} />
            </View>
            <View style={styles.field}>
              <CityDropdown allCitiesData={[{value: 1, label: ''}]} city={1} />
            </View>
            <View style={styles.field}>
              <DistrictDropdown />
            </View>
            <View style={styles.field}>
              <VillageDropdown />
            </View>
            <View style={styles.field}>
              <TextField placeholder="Masukan Nomor HP Anda" label="Nomor HP" />
            </View>
            <View style={styles.field}>
              <NormalCheckbox
                isChecked={isAgreement}
                description="Saya menyetujui syarat dan ketentuan"
                onChange={this._toggleAgreementCB}
              />
            </View>
            <View style={styles.button}>
              <Button text="Lanjutkan" />
            </View>
          </ScrollView>
        </View>
      </View>
    );
  }
}
