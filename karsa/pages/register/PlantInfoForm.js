//@flow
import React, {Component} from 'react';
import {View, ScrollView, Text} from 'react-native';
import {TextField, Button} from '../../core-ui';
import styles from './RegisterStep3Farmer-style';
import PlantsDropdown from './PlantsDropdown';
import PlantingPhaseDropdown from './PlantingPhaseDropdown';
import LandElevationDropdown from './LandElevationDropdown';
import PlantsCategoryDropdown from './PlantsCategoryDropdown';
import PlantsVarietasDropdown from './PlantsVarietasDropdown';

export default class PlantInfoForm extends Component {
  render() {
    return (
      <ScrollView>
        <View style={styles.formContainer}>
          <View style={styles.field}>
            <PlantsDropdown />
          </View>
          <View style={styles.field}>
            <PlantsCategoryDropdown />
          </View>
          <View style={styles.field}>
            <PlantsVarietasDropdown />
          </View>
          <View style={styles.field}>
            <LandElevationDropdown />
          </View>
        </View>
        <View style={styles.recomended}>
          <Text style={styles.descText}>Rekomendasi Tanam: Direkomendasikan</Text>
        </View>
        <View style={styles.formContainer}>
          <View style={styles.field}>
            <PlantingPhaseDropdown />
          </View>
          <View style={styles.field}>
            <TextField placeholder="Luas lahan (m)" label="Luas Lahan" />
          </View>
          <View style={styles.button}>
            <Button text="Lanjutkan" />
          </View>
        </View>
      </ScrollView>
    );
  }
}
