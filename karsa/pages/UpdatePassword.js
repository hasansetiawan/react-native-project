// @flow
import React, {Component} from 'react';
import {
  Text,
  View,
  Alert,
} from 'react-native';
import {
  ButtonRow,
  Button,
  TitleBar,
  TextField,
} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import styles from './LoginPage-style';
import autobind from 'class-autobind';

type State = {
  oldPassword: string;
  newPassword: string;
  reenterNewPassword: string;
  middlePass: string;
};
type Props = {
  isLoading: boolean;
  onPressCancel: () => void;
  onPressSave: () => void;
  isSuccess: boolean;
  onAlertClick: () => void;
};

export default class UpdatePasswordPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      oldPassword: '',
      newPassword: '',
      reenterNewPassword: '',
      middlePass: '',
    };
  }
  render() {
    let {isLoading, onPressCancel, onAlertClick, isSuccess} = this.props;
    let {newPassword, oldPassword, reenterNewPassword, middlePass} = this.state;
    if (isLoading) {
      return <LoadingIndicator />;
    } else {
      if (isSuccess) {
        Alert.alert(
          'Ubah Password',
          'Selamat password berhasil dirubah!',
          [
            {text: 'OK', onPress: () => onAlertClick()},
          ],
        );
      }
    }
    return (
      <View>
        <TitleBar iconButtonName="menu" title="Ubah Password" />
        <View style={styles.mainContainer}>
          <View style={styles.field}>
            <TextField
              placeholder="Masukan Password Lama Anda"
              label="Password Lama"
              isPassword={true}
              value={oldPassword}
              onChangeText={this._onOldPasswordChange}
            />
          </View>
          <View style={styles.field}>
            <TextField
              placeholder="Masukan Password Baru Anda"
              label="Password Baru"
              isPassword={true}
              value={newPassword}
              onChangeText={this._onNewPasswordChange}
            />
          </View>
          <View style={styles.field}>
            {newPassword !== middlePass ? <Text style={styles.error}>Password yang dimasukan ulang tidak sama dengan sebelumnya</Text> : <Text></Text>}
            <TextField
              placeholder="Masukan Ulang Password Baru Anda"
              label="Ulangi Password Baru"
              isPassword={true}
              value={reenterNewPassword}
              onChangeText={this._onReenterNewPasswordChange}
            />
          </View>
          <View style={styles.field}>
            <View style={{height: 10}} />
            <ButtonRow>
              <Button
                text="CANCEL"
                textStyle={styles.centeredButton}
                onPress={() => onPressCancel()}
                inverted
              />
              <Button text="SELESAI" textStyle={styles.centeredButton} onPress={this._onSave} />
            </ButtonRow>
          </View>
        </View>
      </View>
    );
  }
  _onOldPasswordChange(value: string): void {
    this.setState({
      oldPassword: value,
    });
  }
  _onNewPasswordChange(value: string): void {
    this.setState({
      newPassword: value,
      middlePass: value,
    });
  }
  _onReenterNewPasswordChange(value: string): void {
    this.setState({
      reenterNewPassword: value,
      middlePass: value,
    });
  }
  _onSave(): void {
    this.props.onPressSave(this.state);
    this.setState({
      reenterNewPassword: '',
      newPassword: '',
      oldPassword: '',
      middlePass: '',
    });
  }
}
