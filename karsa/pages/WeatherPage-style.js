// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  wrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  altRow: {
    backgroundColor: '#f4f4f4',
  },
  header: {
    flex: 1,
    flexDirection: 'row',
    backgroundColor: '#1ec659',
    alignItems: 'center',
  },
  headerIcon: {
    color: '#fff',
    paddingHorizontal: 5,
  },
  headerText: {
    color: '#fff',
  },
  dropdown: {
    backgroundColor: '#1ec659',
    paddingBottom: 7,
  },
  blur: {
    flex: 1,
    justifyContent: 'space-between',
    padding: 10,
    borderColor: '#0000',
    backgroundColor: '#0009',
  },
  currentWeather: {
    flex: 3,
  },
  currentWeatherDate: {
    fontSize: 12,
    color: 'white',
    backgroundColor: 'transparent',
  },
  currentWeatherMain: {
    flexDirection: 'row',
    justifyContent: 'center',
  },
  currentWeatherIcon: {
    marginRight: 5,
  },
  currentWeatherContent: {
    justifyContent: 'center',
    marginLeft: 5,
  },
  currentWeatherTemperature: {
    flexDirection: 'row',
  },
  currentWeatherTemperatureRange: {
    fontSize: 28,
    color: 'white',
    backgroundColor: 'transparent',
  },
  currentDegree: {
    fontSize: 10,
    color: 'white',
    backgroundColor: 'transparent',
  },
  currentCelcius: {
    fontSize: 28,
    color: 'white',
    backgroundColor: 'transparent',
    alignSelf: 'center',
  },
  degree: {
    fontSize: 8,
  },
  celcius: {
    fontSize: 12,
    alignSelf: 'center',
  },
  description: {
    fontSize: 16,
    fontWeight: 'bold',
    backgroundColor: 'transparent',
    color: 'white',
  },
  temperatureRange: {
    fontWeight: 'bold',
    alignSelf: 'center',
    marginHorizontal: 5,
  },
  currentWeatherFooter: {
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  currentWeatherFooterTitle: {
    fontSize: 10,
    backgroundColor: 'transparent',
    color: 'white',
  },
  currentWeatherFooterValue: {
    fontSize: 14,
    color: 'white',
    backgroundColor: 'transparent',
    fontWeight: 'bold',
  },
  verticalSeparator: {
    backgroundColor: '#68a8bc',
    width: 0.5,
  },
  weatherList: {
    flex: 3,
  },
  weatherListItem: {
    padding: 10,
    flexDirection: 'row',
  },
  weatherListItemDay: {
    fontSize: 12,
    fontWeight: 'bold',
  },
  weatherListItemDate: {
    fontSize: 10,
  },
  weatherListItemTime: {
    flex: 1,
  },
  weatherIcon: {
    alignSelf: 'center',
  },
  weatherListItemTemperature: {
    flex: 1,
    flexDirection: 'row',
  },
  weatherListItemDescription: {
    flex: 1,
  },
  weatherListItemDescriptionText: {
    fontSize: 10,
  },
});
