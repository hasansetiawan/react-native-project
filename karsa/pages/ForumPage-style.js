// @flow

import {StyleSheet} from 'react-native';
import {PRIMARY_GREEN} from '../constants/color';

let rowWrapper = {
  flexDirection: 'row',
};

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
    backgroundColor: 'white',
  },
  contentPadding: {
    paddingLeft: 3,
    paddingRight: 1,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  rowWrapper,
  headerRow: {
    ...rowWrapper,
    alignItems: 'center',
    backgroundColor: PRIMARY_GREEN,
    justifyContent: 'space-between',
    paddingRight: 20,
  },
  headerText: {
    color: '#fff',
    fontSize: 15,
  },
  filterFormWrapper: {
    ...rowWrapper,
    paddingHorizontal: 20,
  },
  filterForm: {
    flex: 4,
  },
  headerDropdown: {
    borderBottomWidth: 0,
  },
  headerDropdownText: {
    color: '#fff',
    flex: 0,
  },
  listWrapper: {
    flex: 1,
  },
  bannerImage: {
    height: 100,
  },
  horizontalPadding: {
    paddingHorizontal: 20,
  },
  negativeTopMargin: {
    marginTop: -10,
  },
  topMargin: {
    marginTop: 3,
  },
  verticalMargin: {
    marginVertical: 10,
  },
  rightMargin: {
    marginRight: 10,
  },
  flex: {
    flex: 1,
  },
});
