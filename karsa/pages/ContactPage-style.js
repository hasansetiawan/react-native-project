// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  contactView: {
    flex: 1,
    backgroundColor: '#fff',
  },
  horizontalSeparator: {
    flexDirection: 'row',
    borderWidth: 0.5,
    borderColor: '#ccc',
  },
  officeAddress: {
    flex: 1,
    padding: 10,
  },
  icon: {
    paddingTop: 4,
  },
  addressName: {
    fontSize: 16,
    fontWeight: 'bold',
    marginBottom: 5,
  },
  addressPhone: {
    flexDirection: 'row',
    marginTop: 5,
  },
  addressEmail: {
    flexDirection: 'row',
    marginTop: 5,
  },
});
