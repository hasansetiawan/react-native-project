// @flow

import React, {Component} from 'react';
import {
  View,
  ListView,
  TouchableOpacity,
} from 'react-native';
import {
  TitleBar,
  IconButton,
  StoreProfileCard,
} from '../core-ui';
import autobind from 'class-autobind';
import styles from './MyStoreView-style';
import listViewDataSource from '../helpers/listViewDataSource';
import type {Shop} from '../types/Shop';

type Props = {
  shops: Array<Shop>;
};
type State = {
  dataSource: ListView.DataSource;
};

export default class MyStoreView extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    const ds = listViewDataSource();
    this.state = {
      dataSource: ds.cloneWithRows(this.props.shops),
    };
  }

  render() {
    return (
      <View style={styles.mainWrapper}>
        <TitleBar iconButtonName="menu" title="Toko Saya">
          <IconButton icon="add" />
        </TitleBar>
        <ListView
          style={styles.listWrapper}
          dataSource={this.state.dataSource}
          renderRow={this._renderShop}
        />
      </View>
    );
  }

  _renderShop(shop: Shop) {
    return (
      <TouchableOpacity onPress={() => {/* TODO: route to store detail */}}>
        <StoreProfileCard {...shop} />
      </TouchableOpacity>
    );
  }
}
