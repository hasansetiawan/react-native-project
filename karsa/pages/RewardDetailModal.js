// @flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import styles from './RewardDetailModal-style';
import shovel from '../images/shovel.png';

import {Modal, View, ScrollView, Text, Image} from 'react-native';
import {Dialog, Button, SwipeableThumbnail, LoadingIndicator} from '../core-ui';

type Props = {
  reward: Object; // TODO: define reward type
  isPurchaseRewardLoading: boolean;
  onPurchase: (rewardID: number) => void;
  onClose: () => void;
  navigateToMyRewards: () => void;
  navigateToRewardList: () => void;
};

type State = {
  purchased: boolean;
  activePhoto: ?ImageSource;
};

export default class RewardDetailModal extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      purchased: false,
      activePhoto: this.props.reward.photos[0],
    };
  }

  render() {
    let {reward, isPurchaseRewardLoading, onClose} = this.props;
    let {purchased} = this.state;
    let content;
    if (purchased && !isPurchaseRewardLoading) {
      content = this._renderSuccess();
    } else {
      content = this._renderDetail();
    }
    return (
      <Modal
        animationType="fade"
        transparent={true}
        visible={reward != null}
        onRequestClose={onClose}
      >
        <View style={[styles.flex, styles.overlay, styles.center]}>
          {content}
        </View>
      </Modal>
    );
  }

  _onPurchaseReward() {
    let {reward, onPurchase} = this.props;
    this.setState({purchased: true});
    onPurchase(reward.id);
  }

  _renderDetail() {
    let {reward, isPurchaseRewardLoading, onClose} = this.props;
    let {activePhoto} = this.state;
    return (
      <View style={[styles.dialogWidth, styles.dialogHeight]}>
        <Dialog
          title="Detail Hadiah"
          onClose={onClose}
          flexContent
        >
          <ScrollView contentContainerStyle={styles.padding} showsVerticalScrollIndicator={false}>
            <Image source={activePhoto} style={[styles.photo, styles.bottomMargin]} resizeMode="contain" resizeMethod="resize" />
            <SwipeableThumbnail>
              {
                reward.photos.map((photo, index) =>
                  <Image
                    style={styles.photoThumbnail}
                    source={photo}
                    key={index}
                    onPress={() => this.setState({activePhoto: photo})}
                    resizeMethod="resize"
                  />
                )
              }
            </SwipeableThumbnail>
            <Text style={[styles.header, styles.bold]}>{reward.name}</Text>
            <Text style={styles.slightlyBiggerFont}>{reward.manufacturer}, {reward.manufacturerAddress}</Text>
            <Text style={[styles.biggerFont, styles.greenFont, styles.bold, styles.smallVerticalPadding]}>Rp. {reward.price}</Text>
            <Text>Deskripsi produk</Text>
            <Text style={styles.slightlyBiggerFont}>{reward.description}</Text>
          </ScrollView>
          <View style={styles.footer}>
            <View style={styles.center}>
              <Text style={styles.slightlyBiggerFont}>Anda yakin ingin membeli barang ini?</Text>
              <View style={[styles.flexRow, styles.center]}>
                <Text style={[styles.header, styles.bold]}>{reward.points} </Text>
                <RewardCoin />
              </View>
            </View>
            {
              !isPurchaseRewardLoading ? (
                <Button text="Beli" onPress={this._onPurchaseReward} style={styles.buttonFix} />
              ) : (
                <Button style={styles.buttonFix} inverted>
                  <LoadingIndicator style={styles.zeroPadding} small />
                </Button>
              )
            }
          </View>
        </Dialog>
      </View>
    );
  }

  _renderSuccess() {
    let {reward, navigateToMyRewards, navigateToRewardList, onClose} = this.props;
    return (
      <View style={[styles.dialogWidth, styles.dialogSmallerHeight]}>
        <Dialog
          title="Pembelian Sukses"
          onClose={onClose}
        >
          <View style={[styles.padding, styles.center]}>
            <Text style={[styles.header, styles.bold]}>Anda telah membeli</Text>
            <Image source={reward.photos[0]} style={[styles.photo, styles.photoWidth, styles.verticalPadding]} resizeMode="contain" resizeMethod="resize" />
            <Text style={[styles.header, styles.bold]}>{reward.name}</Text>
            <Text style={styles.slightlyBiggerFont}>{reward.manufacturer}, {reward.manufacturerAddress}</Text>
          </View>
          <View style={styles.footer}>
            <Text style={[styles.slightlyBiggerFont, styles.smallPadding]}>Anda dapat menukarkan kode/barcode yang terdapat pada 'HadiahKu' ke Indomaret terdekat untuk mengambil barang yang telah dibeli.</Text>
            <View style={styles.flexRow}>
              <Button text="Ke HadiahKu" onPress={navigateToMyRewards} style={styles.buttonFix} />
              <Button text="Kembali ke Daftar Hadiah" onPress={navigateToRewardList} style={styles.buttonFix} inverted />
            </View>
          </View>
        </Dialog>
      </View>
    );
  }
}

// TODO: make this its own component to make sure all coins are uniform across pages/components?
function RewardCoin() {
  return (
    <View style={{backgroundColor: 'gold', width: 20, height: 20, borderRadius: 10}}>
      <Image source={shovel} style={{width: 15, height: 15}} />
    </View>
  );
}
