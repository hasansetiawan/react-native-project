// @flow

//Sarana Tani View

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View, ToastAndroid, Alert} from 'react-native';
import {TileButton, TitleBar} from '../core-ui';
import Permissions from 'react-native-permissions';
import {LIGHT_CYAN, LIGHTER_GREEN, DARK_GREEN} from '../constants/color';

import image1 from '../images/icon_nearby_farm_shop.png';
import image2 from '../images/icon_farmer_media_info.png';
import image3 from '../images/icon_pest.png';
import styles from './FarmerMediaPage-style';

import type {Coordinates} from '../types/Location';

type State = {
  locationPermission: string;
};

type Props = {
  navigateToNearbyStore: () => void;
  navigateToFarmerMediaInfo: () => void;
  navigateToPestDisease: () => void;
  fetchNearbyStore: (lat: number, lon: number, page: number) => void;
  fetchGPSAddress: (coordinates: Coordinates) => void;
};

export default class FarmerMediaPage extends Component {
  state: State;
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      locationPermission: 'undetermined',
    };
  }

  componentDidMount() {
    Permissions.getPermissionStatus('location')
      .then((response) => {
        //response is one of: 'authorized', 'denied', 'restricted', or 'undetermined'
        this.setState({locationPermission: response});
      });
  }

  render() {
    let desc = [
      'Melihat toko-toko pertanian terdekat sesuai dengan lokasi anda.',
      'Informasi terkait dengan sarana tani seperti peralatan tani, pupuk, dan pestisida.',
      'Terdiri dari berbagai informasi mengenai hama dan penyakit pada tanaman.',
    ];
    let {navigateToFarmerMediaInfo, navigateToPestDisease} = this.props;
    return (
      <View style={styles.root}>
        <TitleBar title="Sarana Tani" iconButtonName="menu" />
        <View style={styles.wrapper}>
          <View style={styles.flex}>
            <TileButton
              isHorizontal={true}
              onPress={navigateToPestDisease}
              tileName="Hama dan Penyakit"
              tileDescription={desc[2]}
              image={image3}
              colorStyle={{backgroundColor: LIGHTER_GREEN, borderColor: '#008D39'}}
            />
          </View>
          <View style={styles.flex}>
            <TileButton
              isHorizontal={true}
              onPress={navigateToFarmerMediaInfo}
              tileName="Info Sarana Tani"
              tileDescription={desc[1]}
              image={image2}
              colorStyle={{backgroundColor: DARK_GREEN, borderColor: '#2B7800'}}
            />
          </View>
          <View style={styles.flex}>
            <TileButton
              isHorizontal={true}
              onPress={this._onNearbyStorePress}
              tileName="Toko Pertanian Terdekat"
              tileDescription={desc[0]}
              image={image1}
              colorStyle={{backgroundColor: LIGHT_CYAN, borderColor: '#038B70'}}
            />
          </View>
        </View>
      </View>
    );
  }

  _onNearbyStorePress() {
    let {fetchNearbyStore, navigateToNearbyStore, fetchGPSAddress} = this.props;
    let {locationPermission} = this.state;
    if (locationPermission !== 'authorized') {
      this._requestPermission(locationPermission);
    } else {
      navigator.geolocation.getCurrentPosition(
        (position) => {
          let latitude = position.coords.latitude;
          let longitude = position.coords.longitude;
          fetchGPSAddress({latitude, longitude});
          fetchNearbyStore(latitude, longitude, 1);
          navigateToNearbyStore();
        },
        () => {
          ToastAndroid.show('Gagal Mendapatkan Lokasi GPS. Silakan Coba sesaat lagi.', ToastAndroid.SHORT);
        },
      );
    }
  }

  _requestPermission(locationPermission) {
    try {
      Alert.alert(
        'Akses GPS',
        'Karsa memerlukan izin untuk mengakses GPS',
        [
          {text: 'Tolak'},
          locationPermission === 'undetermined' ?
            {text: 'Izinkan', onPress: this._authorizePermission}
            : {text: 'Buka Settings', onPress: Permissions.openSettings},
        ]
      );
    } catch (e) {
      Alert.alert('Error', 'Gagal mengakses GPS. Mohon periksa pengaturan Anda.');
    }
  }

  _authorizePermission() {
    Permissions.requestPermission('location')
      .then((res) => {
        if (res !== 'authorized') {
          Alert.alert(
            'Otorisasi Gagal',
            'Terjadi masalah dalam mendapatkan izin. Anda dapat memberikan izin secara manual di dalam menu Settings > Permissions.',
            [
              {text: 'Batal', style: 'cancel'},
              {text: 'Buka Settings', onPress: Permissions.openSettings},
            ]
          );
        } else {
          this.setState({locationPermission: res});
        }
      }).catch(() => Alert.alert('Error', 'Gagal mengakses GPS. Mohon periksa pengaturan Anda.'));
  }
}
