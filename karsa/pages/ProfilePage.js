// @flow

import React from 'react';
import {View} from 'react-native';

import styles from './ProfilePage-style';

import TitleBar from '../core-ui/TitleBar';
import ProfileHeader from '../core-ui/ProfileHeader';
import ProfileHeaderText from '../core-ui/ProfileHeaderText';
import IconButton from '../core-ui/IconButton';
import TextCard from '../core-ui/TextCard';
import {Tab, StatefulTabGroup} from '../core-ui';

type Props = {
  user: {
    name: string;
    role: string;
    email: string;
    phoneNumber: string;
    address: string;
    avatar: ImageSource;
    avatarFb: ImageSource;
  };
  onEditProfilePress: () => void;
};

export default function ProfilePage(props: Props) {
  let {user, onEditProfilePress} = props;
  return (
    <View style={styles.root}>
      <TitleBar title="Profile" iconButtonName="menu" >
        <IconButton icon="settings" text="Edit" onPress={onEditProfilePress} />
      </TitleBar>
      <ProfileHeader image={user.avatarFb} headerType="large">
        <ProfileHeaderText
          profileType="person"
          profileTitle={user.name}
          userRole={user.role}
        />
      </ProfileHeader>
      <StatefulTabGroup styling="primary">
        <Tab title="INFO">
          <TextCard
            style={styles.textStyle}
            title="Email"
            textItems={[user.email]}
          />
          <TextCard
            style={styles.textStyleDark}
            title="Nomor HP"
            textItems={[user.phoneNumber]}
          />
          <TextCard
            style={styles.textStyle}
            title="Alamat"
            textItems={[user.address]}
          />
        </Tab>
        <Tab title="THREAD">

        </Tab>
      </StatefulTabGroup>
    </View>
  );
}
