import {StyleSheet} from 'react-native';
import {SECONDARY_GREY} from '../constants/color';

export default StyleSheet.create({
  flex: {
    flex: 1,
  },
  absolute: {
    bottom: 0,
    left: 0,
    position: 'absolute',
    right: 0,
    top: 0,
  },
  backdrop: {
    backgroundColor: 'rgba(0, 0, 0, 0.5)',
  },
  groupImage: {
    flex: 1,
    height: 200,
  },
  center: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  centerJustified: {
    justifyContent: 'center',
  },
  verticalSeparator: {
    width: 1,
    borderWidth: 0.5,
    borderColor: 'white',
  },
  whiteBackground: {
    backgroundColor: 'white',
  },
  white: {
    color: 'white',
  },
  bold: {
    fontWeight: 'bold',
  },
  textCenter: {
    textAlign: 'center',
  },
  greyBackground: {
    backgroundColor: SECONDARY_GREY,
  },
  row: {
    flexDirection: 'row',
  },
  allPadding: {
    padding: 20,
  },
  horizontalPadding: {
    paddingHorizontal: 10,
  },
  bottomPadding: {
    paddingBottom: 10,
  },
  topPadding: {
    paddingTop: 10,
  },
  bigText: {
    fontSize: 16,
  },
  biggerText: {
    fontSize: 18,
  },
  smallTopPadding: {
    paddingTop: 5,
  },
});
