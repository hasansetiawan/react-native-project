// @flow

import React, {Component} from 'react';
import {
  View,
  Text,
  ScrollView,
  Image,
  Alert,
  ToastAndroid,
  Modal,
  TouchableOpacity,
  TouchableWithoutFeedback,
  Share,
} from 'react-native';

import {
  TitleBar,
  IconButton,
  StatefulTabGroup,
  LinearGradient,
  Tab,
  GridView,
  TextCard,
  ProductDetail,
  ProductHighlight,
  SwipeableThumbnail,
  Icon,
  ItemList,
  ItemHeaderWithLink,
  ItemContent,
  Button,
} from '../core-ui';
import {MyPage} from '../core-image';
import {DARK_YELLOW} from '../constants/color';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import FavoriteButton from '../core-ui/FavoriteButton';
import autobind from 'class-autobind';
import modalStyles from '../core-ui/NewItemModal-style';
import ShareToForumFormContainer from '../containers/ShareToForumFormContainer';

import type {PlantDetailWithPhotos, PlantingInformation, PhaseSuggestion} from '../types/Plant';
import type {CostData} from '../types/Cost';
import {GRADIENT} from '../constants/color';
import dateStringToLocale from '../helpers/dateStringToLocale';
import styles from './MyPlantDetail-style';
import formatNumber from '../helpers/formatNumber';
import formatPriceRange from '../helpers/formatPriceRange';
import type {Product} from '../types/Product';

type PlantProductProps = {
  onNavigateToProductList: () => null;
  onProductPress: (productID: number) => any;
  productList: Array<Product>;
  onLookProductsPress: () => void;
};

type PlantDataProps = {
  plant: PlantDetailWithPhotos;
  onAddPhotoPress: () => void;
};

type Props = {
  plantData: PlantDataProps;
  costData: {
    costList: Array<CostData>;
  };
  onDeletePlant: (plantID: number) => void;
  plantingInfoData: PlantingInformation;
  isLoading: boolean;
  onCostAnalysisPress: (costID: number) => void;
  onProductPress: (productID: number) => any;
  plantProducts: PlantProductProps;
  onAddPhotoPress: () => void;
  onLookProductsPress: () => void;
  fetchForumCategories: () => void;
};

type CostAnalysisProps = {
  onCostAnalysisPress: (costID: number) => void;
  costList: Array<CostData>;
};

type PlantingInfoProps = {
  plantInfo: PlantingInformation;
};

type PlantDataState = {
  activePhoto: ?ImageSource;
};

export class PlantData extends Component {
  props: PlantDataProps;
  state: PlantDataState;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      activePhoto: null,
    };
  }

  _onImagePress(image: ImageSource) {
    this.setState({activePhoto: image});
  }

  _renderPhaseSuggestions(phase: PhaseSuggestion) {
    let {phaseDescription, phaseSuggestions} = phase;
    return phaseSuggestions.length === 1 ? (
      <View>
        <Text style={styles.subHeaderText}>{phaseDescription}:</Text>
        {
          phaseSuggestions[0].suggestions.map((message, idx) => (
            <Text key={idx} style={styles.defaultText}>
              <Icon name="play-arrow" />
              {message}
            </Text>
          ))
        }
      </View>
    ) : (
      <StatefulTabGroup styling="primary">
        {
          phaseSuggestions.map((item, idx) => (
            <Tab title={item.suggestionName == null ? idx.toString() : item.suggestionName} key={idx}>
              {
                item.suggestions.map((message, idx) => (
                  <Text key={idx} style={styles.defaultText}>
                    <Icon name="play-arrow" />
                    {message}
                  </Text>
                ))
              }
            </Tab>
          ))
        }
      </StatefulTabGroup>
    );
  }
  componentWillMount() {
    let {photo, photos} = this.props.plant;
    if (photos && photos.length !== 0) {
      this.setState({
        activePhoto: [...photos].pop().photo,
      });
    } else if (photo != null) {
      this.setState({
        activePhoto: photo,
      });
    }
  }
  render() {
    let {plant, onAddPhotoPress} = this.props;
    let {plantName, plantCategory, plantVariant, plantingMethod, description, soilPH, photos} = plant;
    let {watering, plantAge, plantDate, plantAddressGps, harvestDate, harvestPotential} = plant;
    let {area, unit, fertility, landHeight, isSeedBed, dolomiteRequirement, disclaimer, phase} = plant;
    if (phase == null || plant == null) {
      return (
        <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
          <Text style={{fontWeight: '700', fontStyle: 'italic', fontSize: 16}}>
            Terjadi Kesalahan Ketika Mengambil Data.
          </Text>
          <Text style={{fontWeight: '700', fontStyle: 'italic', fontSize: 16}}>
            Periksa Kembali Koneksi Anda.
          </Text>
        </View>
      );
    }
    let {phaseDescription, nextPhaseTime, seedBed} = phase;
    let lastPhotoDate = photos.length !== 0 ? photos.slice(-1)[0].createdAt : 'Unknown';
    return (
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.lastPhoto}>
          {
            this.state.activePhoto ? (
              <Image resizeMode="cover" style={styles.lastPhotoImage}
                source={this.state.activePhoto}
              >
                <LinearGradient colors={GRADIENT} style={styles.gradient}>
                  <Text style={styles.lastPhotoDate}>Update {lastPhotoDate}</Text>
                </LinearGradient>
              </Image>
            ) : null
          }
        </View>
        <View style={styles.dataSegment}>
          <Text style={styles.greyText}>Foto Sebelumnya</Text>
          <SwipeableThumbnail>
            <View style={styles.addPhotoWrapper} onPress={() => onAddPhotoPress()}>
              <Icon name="add-a-photo" style={styles.addPhoto} />
            </View>
            {[...photos].reverse().map(({photoThumb}, index) =>
              <Image
                style={styles.photoImage}
                source={photoThumb}
                key={index}
                onPress={() => this._onImagePress(photoThumb)}
              />
            )}
          </SwipeableThumbnail>
          <Text style={styles.headerText}>{plantName}</Text>
          <Text style={styles.defaultText}>{plantCategory}, {plantVariant}</Text>
          <Text style={styles.greyText}>Usia tanam: {plantAge}</Text>
        </View>
        <View style={styles.dataSegment}>
          <TextCard
            title="Fase Tanaman"
            style={{flex: 1}}
            textItems={[phaseDescription]}
          />
        </View>
        <View style={styles.dataSegment}>
          <TextCard
            title="Waktu Fase Selanjutnya"
            style={{flex: 1}}
            textItems={[nextPhaseTime]}
          />
        </View>
        <View style={styles.dataSegment}>
          <Text style={styles.headerText}>Saran</Text>
          {this._renderPhaseSuggestions(phase)}
          {
            seedBed.phaseDescription !== 'Data tidak ada' ?
              this._renderPhaseSuggestions(seedBed) :
              null
          }
        </View>
        <View style={styles.dataSegment}>
          <Text style={styles.headerText}>Estimasi Panen</Text>
          <View style={styles.horizontalTextWrapper}>
            <TextCard
              title="Tanggal"
              style={{flex: 1}}
              textItems={[harvestDate]}
            />
            <TextCard
              title="Hasil Panen"
              style={{flex: 1}}
              textItems={[harvestPotential + ' ton']}
            />
          </View>
          <View style={styles.disclaimer}>
            <Text style={styles.disclaimerText}>Perhatian: {disclaimer}</Text>
          </View>
        </View>
        <View style={styles.dataSegment}>
          <Text style={styles.headerText}>Data Tanaman</Text>
          <View style={styles.detailDataWrapper}>
            <TextCard
              title="Luas Tanah"
              style={{flex: 1}}
              textItems={[area + ' ' + unit]}
            />
            <TextCard
              title="Tanggal Tanam"
              style={{flex: 1}}
              textItems={[dateStringToLocale(plantDate)]}
            />
          </View>
          <View style={styles.detailDataWrapper}>
            <TextCard
              title="Metode Penanaman"
              style={{flex: 1}}
              textItems={[plantingMethod]}
            />
            {
              watering ? (
                <TextCard
                  title="Pengairan"
                  style={{flex: 1}}
                  textItems={[watering]}
                />
              ) : null
            }
          </View>
          <View style={styles.detailDataWrapper}>
            <TextCard
              title="Menggunakan Persemian"
              style={{flex: 1}}
              textItems={[isSeedBed ? 'ya' : 'tidak']}
            />
            {
              fertility ? (
                <TextCard
                  title="Kesuburan"
                  style={{flex: 1}}
                  textItems={[fertility]}
                />
              ) : null
            }
          </View>
          <View style={styles.detailDataWrapper}>
            <TextCard
              title="Lokasi Penanaman"
              style={{flex: 1}}
              textItems={[plantAddressGps]}
            />
            <TextCard
              title="Ketinggian Tanah"
              style={{flex: 1}}
              textItems={[landHeight]}
            />
          </View>
          <View style={styles.detailDataWrapper}>
            <TextCard
              title="pH Tanah"
              style={{flex: 1}}
              textItems={[soilPH]}
            />
            <TextCard
              title="Kebutuhan Dolomit (Ton/Ha)"
              style={{flex: 1}}
              textItems={[dolomiteRequirement]}
            />
          </View>
          <View style={styles.detailDataWrapperNoBorder}>
            {
              description ? (
                <TextCard
                  title="Keterangan"
                  style={{flex: 1}}
                  textItems={[description]}
                />
              ) : null
            }
          </View>
        </View>
      </ScrollView>
    );
  }
}

export function CostAnalysis(props: CostAnalysisProps) {
  let {costList, onCostAnalysisPress} = props;
  if (costList.length === 0) {
    return (
      <View style={styles.emptyAnalysisContainer}>
        <Text style={styles.emptyAnalysis}>Belum ada Riwayat Analisis Biaya</Text>
      </View>
    );
  }
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      {costList.map((costData, idx) => {
        let {analysisTitle, expense, income, analysisDate} = costData;
        return (
          <ItemList key={idx}>
            <ItemHeaderWithLink title={analysisTitle} onPress={() => onCostAnalysisPress(costData.id)}>
              <Text style={styles.greyText}>{analysisDate}</Text>
            </ItemHeaderWithLink>
            <ItemContent>
              <TextCard
                title="Total Pengeluaran"
                textItems={[formatNumber(expense.toString(), {prefix: 'Rp.', separator: ','})]}
              />
              <TextCard
                title="Total Pendapatan"
                textItems={[formatNumber(income.toString(), {prefix: 'Rp.', separator: ','})]}
              />
            </ItemContent>
          </ItemList>
        );
      })}
    </ScrollView>

  );
}
export function PlantingInfo(props: PlantingInfoProps) {
  let {plantInfo} = props;
  let {seedRequirement, manureRequirement, chemicalRequirement, simplePlanting} = plantInfo;
  let {landElevation, temperature, productionResult, rainfallRate, pestsAndDiseasesInfo} = plantInfo;
  return (
    <ScrollView showsVerticalScrollIndicator={false}>
      <View style={styles.dataSegment}>
        <Text style={[styles.headerText, {marginBottom: 16}]}>Tips</Text>
        <TextCard
          title="Kebutuhan Benih/Gram/1000 m2"
          textItems={[seedRequirement]}
        />
      </View>
      <View style={styles.dataSegment}>
        <View style={styles.horizontalTextWrapper}>
          <TextCard
            title="Kebutuhan Pupuk/1000 m2"
            textItems={['Kandang: ' + manureRequirement]}
            style={{flex: 1}}
          />
          <TextCard
            textItems={chemicalRequirement.split('\n')}
            style={{flex: 1}}
          />
        </View>
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Penanaman Sederhana"
          textItems={simplePlanting.split('_')}
        />
      </View>
      <View style={styles.dataSegment}>
        <View style={styles.horizontalTextWrapper}>
          <TextCard
            title="Ketinggian Lahan"
            textItems={[landElevation]}
            style={{flex: 1}}
          />
          <TextCard
            title="Suhu"
            textItems={[temperature]}
            style={{flex: 1}}
          />
        </View>
      </View>
      <View style={styles.dataSegment}>
        <View style={styles.horizontalTextWrapper}>
          <TextCard
            title="Curah hujan"
            textItems={[rainfallRate]}
            style={{flex: 1}}
          />
          <TextCard
            title="Hasil Produksi"
            textItems={[productionResult]}
            style={{flex: 1}}
          />
        </View>
      </View>
      <View style={styles.dataSegment}>
        <TextCard
          title="Hama dan penyakit secara umum"
          textItems={[pestsAndDiseasesInfo]}
        />
      </View>
    </ScrollView>
  );
}
export function PlantProductList(props: PlantProductProps) {
  let {productList, onProductPress, onLookProductsPress} = props;
  if (productList.length === 0) {
    return (
      <View style={styles.flex}>
        <Text style={styles.noProductText}>
          Anda Belum Menggunakan Produk Untuk Tanaman Ini
        </Text>
        <Button
          text="LIHAT PRODUK KAMI"
          onPress={onLookProductsPress}
        />
      </View>
    );
  }
  let highlightBadge = <ProductHighlight icon="bookmark" text="Highlight" />;
  return (
    <View style={styles.flex}>
      <GridView
        style={{margin: 5}}
        items={productList}
        itemsPerRow={2}
        showsVerticalScrollIndicator={false}
        renderItem={
          (product, index) => {
            let {highlight, priceMin, priceMax, ...others} = product;
            let priceRange = formatPriceRange(priceMin, priceMax);
            let onPress = () => {
              onProductPress(product.id);
            };
            return highlight
              ? <ProductDetail {...others} priceRange={priceRange} key={index} onPress={onPress} highlight={highlightBadge} />
              : <ProductDetail {...others} priceRange={priceRange} key={index} onPress={onPress} />;
          }
        }
      />
      <Button
        text="GUNAKAN PRODUK LAIN"
        onPress={onLookProductsPress}
        style={{maxHeight: 40, minHeight: 40, marginBottom: 40}}
      />
    </View>
  );
}

function sharePlantDetail(plantID: number, imageUrl: string, plantName: string) {
  Share.share({
    message: `Lihat detail dari tanaman ${plantName} saya di http://ingkarsa.com/dashboard/tanamanku/${plantID}`,
  })
  .catch(() => ToastAndroid.show(`Gagal membagikan data tanaman`, ToastAndroid.SHORT));
}

type State = {
  shareModalVisible: boolean;
  shareToForumVisible: boolean;
};

export default class MyPlantDetail extends Component {
  props: Props;
  state: State;

  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      shareModalVisible: false,
      shareToForumVisible: false,
    };
  }

  componentWillMount() {
    this.props.fetchForumCategories();
  }

  render() {
    let {onLookProductsPress} = this.props;
    let {plantData, costData, plantingInfoData, isLoading, onCostAnalysisPress, plantProducts, onAddPhotoPress, onProductPress} = this.props;
    let content = null;
    if (isLoading) {
      content = <LoadingIndicator />;
    } else {
      content = (
        <StatefulTabGroup styling="primary" scrollable={true}>
          <Tab title="DATA TANAMAN">
            <PlantData
              {...plantData}
              onAddPhotoPress={onAddPhotoPress}
            />
          </Tab>
          <Tab title="ANALISIS BIAYA">
            <CostAnalysis
              costList={costData.costList}
              onCostAnalysisPress={onCostAnalysisPress}
            />
          </Tab>
          <Tab title="INFO TANAMAN">
            <PlantingInfo
              plantInfo={plantingInfoData}
            />
          </Tab>
          <Tab title="PENGGUNAAN PRODUK">
            <PlantProductList
              productList={plantProducts.productList}
              onNavigateToProductList={plantProducts.onNavigateToProductList}
              onProductPress={onProductPress}
              onLookProductsPress={onLookProductsPress}
            />
          </Tab>
        </StatefulTabGroup>
      );
    }
    return (
      <View style={styles.root}>
        <TitleBar title="Panduan Mingguan" iconButtonName="menu" >
          <IconButton icon="share" onPress={this._onSharePress} />
          <IconButton icon="delete" onPress={this._onDeletePress} />
          <FavoriteButton
            id={`myPlantDetail-${plantData.plant.id}`}
            description={`Tanamanku - ${plantData.plant.plantName}`}
            actions={[
              {type: 'PLANT_SELECTED', plantID: plantData.plant.id},
              {type: 'PUSH_ROUTE', key: 'myPlantDetail'},
              {type: 'FETCH_ANALYSIS_REQUESTED'},
              {type: 'FETCH_PLANT_DETAIL_REQUESTED', plantID: plantData.plant.id},
              {type: 'FETCH_PRODUCTS_PLANT_REQUESTED', plantID: plantData.plant.id},
            ]}
          />
        </TitleBar>
        <ShareModal
          visible={this.state.shareModalVisible}
          share={this._onShareToOthers}
          shareToForum={this._onShareToForumPress}
          onClose={() => this.setState({shareModalVisible: false})}
        />
        <ShareToForumFormContainer
          isVisible={this.state.shareToForumVisible}
          plant={plantData.plant}
          onClose={() => this.setState({shareToForumVisible: false})}
        />
        {content}
      </View>
    );
  }

  _onDeletePress() {
    let {onDeletePlant, plantData} = this.props;
    Alert.alert('Karsa', 'Hapus Tanaman Ini?', [
      {text: 'Iya', onPress: () => onDeletePlant(plantData.plant.id)},
      {text: 'Tidak'},
    ]);
  }

  _onSharePress() {
    this.setState({shareModalVisible: true});
  }

  _onShareToForumPress() {
    this.setState({shareToForumVisible: true});
  }

  _onShareToOthers() {
    let {plantData} = this.props;
    let {id, plantCategory} = plantData.plant;
    let photo = typeof plantData.plant.photo === 'string' ? plantData.plant.photo : '';
    sharePlantDetail(id, photo, plantCategory);
  }
}

type ShareModalProps = {
  visible: boolean;
  share: () => any;
  shareToForum: () => any;
  onClose: () => any;
};

function ShareModal(props: ShareModalProps) {
  let {visible, share, shareToForum, onClose} = props;
  let onShare = () => {
    onClose();
    share();
  };
  let onShareToForum = () => {
    onClose();
    shareToForum();
  };
  return (
    <Modal
      animationType="fade"
      transparent={true}
      visible={visible}
      onRequestClose={onClose}
    >
      <TouchableWithoutFeedback onPress={onClose}>
        <View style={[modalStyles.root, modalStyles.modalContainer]}>
          <View style={modalStyles.flexRow}>
            <View style={modalStyles.flex}>
              <TouchableOpacity onPress={onShare}>
                <View style={modalStyles.iconContainer}>
                  <View style={[modalStyles.iconButton, {backgroundColor: '#3b5998'}]}>
                    <Icon name="share" style={{fontSize: 25, color: 'white'}} />
                  </View>
                  <Text style={modalStyles.text}>SHARE</Text>
                </View>
              </TouchableOpacity>
            </View>
            <View style={modalStyles.flex}>
              <TouchableOpacity onPress={onShareToForum}>
                <View style={modalStyles.iconContainer}>
                  <View style={[modalStyles.iconButton, {backgroundColor: DARK_YELLOW}]}>
                    <Image source={MyPage} style={modalStyles.image} resizeMode="contain" />
                  </View>
                  <Text style={modalStyles.text}>FORUM DISKUSI</Text>
                </View>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </TouchableWithoutFeedback>
    </Modal>
  );
}
