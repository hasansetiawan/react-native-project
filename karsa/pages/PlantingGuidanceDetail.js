//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  ScrollView,
  View,
  Text,
} from 'react-native';
import {
  TitleBar,
  LoadingIndicator,
} from '../core-ui';
import styles from './PlantingGuidanceDetail-style';
import {ContentView} from './ContentView';
import getContentFromHTML from '../selectors/getContentFromHTML';
import Accordion from 'react-native-collapsible/Accordion';

import type {PlantingGuideDetail} from '../types/PlantingGuide';

type Props = {
  plantDetailNames: Array<string>;
  isPlantingGuideDetailLoading: boolean;
  fetchedPlantingGuideDetails: Map<string, PlantingGuideDetail>;
  fetchPlantingGuideDetail: (plantDetail: string) => void;
};

export default class PlantingGuidanceDetail extends Component {
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  render() {
    return (
      <View style={styles.flex}>
        <TitleBar title="Panduan Penanaman" />
        {this._renderBody()}
      </View>
    );
  }

  _renderBody() {
    let {plantDetailNames} = this.props;
    return (
      <ScrollView style={styles.flex}>
        <Accordion
          sections={plantDetailNames}
          renderHeader={this._renderAccordionHeader}
          renderContent={this._renderAccordionContent}
          underlayColor="#0000"
        />
      </ScrollView>
    );
  }

  _renderAccordionHeader(plantDetail) {
    return (
      <View style={styles.accordionHeader}>
        <Text style={styles.accordionHeaderText}>{plantDetail.toUpperCase()}</Text>
      </View>
    );
  }

  _renderAccordionContent(plantDetail, index, isActive) {
    if (isActive) {
      return <AccordionContent {...this.props} plantDetail={plantDetail} />;
    } else {
      return (
        <View style={styles.loadingContainer}>
          <LoadingIndicator />
        </View>
      );
    }
  }

}

class AccordionContent extends Component {
  props: {
    plantDetail: string;
    isPlantingGuideDetailLoading: boolean;
    fetchedPlantingGuideDetails: Map<string, PlantingGuideDetail>;
    fetchPlantingGuideDetail: (plantDetail: string) => void;
  };

  componentDidMount() {
    let {plantDetail, fetchedPlantingGuideDetails, fetchPlantingGuideDetail} = this.props;
    if (!fetchedPlantingGuideDetails.has(plantDetail)) {
      fetchPlantingGuideDetail(plantDetail);
    }
  }

  render() {
    let {plantDetail, fetchedPlantingGuideDetails} = this.props;
    let selectedGuideByDetail = fetchedPlantingGuideDetails.get(plantDetail);
    if (selectedGuideByDetail) {
      let {content} = getContentFromHTML(selectedGuideByDetail.htmlValue);
      return (
        <View style={styles.htmlView}>
          <ContentView content={content} />
        </View>
      );
    } else {
      return (
        <View style={styles.loadingContainer}>
          <LoadingIndicator />
        </View>
      );
    }
  }
}
