//@flow

import React, {Component} from 'react';
import autobind from 'class-autobind';
import {View} from 'react-native';
import {Dropdown, TitleBar, GradientCard, GridView} from '../core-ui';
import styles from './MyProducts-style';

import type {Value} from '../core-ui/Dropdown';
import type {Product} from '../types/Product';

const propsDropdownSort = {
  defaultValue: '2',
  options: [
    {value: '1', label: 'Produk Termurah'},
    {value: '2', label: 'Produk Terbaru'},
  ],
  animationType: 'none',
  label: 'Urutkan',
};

type Props = {
  items: Array<Product>;
};

type State = {
  selectedSortItem: Value;
};

export default class MyProducts extends Component {
  props: Props;
  state: State = {
    selectedSortItem: propsDropdownSort.defaultValue,
  };
  constructor() {
    super(...arguments);
    autobind(this);
  }
  render() {
    let {selectedSortItem} = this.state;
    let renderProduct = (product, index) => (
      <GradientCard
        {...product}
        key={index}
        type="product"
        onPress={() => {}}
      />
    );
    let productCardProps = {
      items: this.props.items,
      itemsPerRow: 2,
      renderItem: renderProduct,
      style: {margin: 5},
    };
    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="My Products" />
        <View style={styles.content}>
          <Dropdown
            {...propsDropdownSort}
            onSelect={this._onSelectSortOption}
            selectedValue={selectedSortItem}
          />
          <GridView
            {...productCardProps}
          />
        </View>
      </View>
    );
  }
  _onSelectSortOption(item: Value): void {
    this.setState({
      selectedSortItem: item,
    });
  }
}
