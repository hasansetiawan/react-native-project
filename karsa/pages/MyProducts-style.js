//@flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  mainContainer: {
    flex: 1,
  },
  content: {
    margin: 10,
  },
  productItem: {
    flexDirection: 'row',
    alignItems: 'stretch',
  },
});
