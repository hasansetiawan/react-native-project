// @flow
import React, {Component} from 'react';
import {
  Text,
  View,
  TouchableOpacity,
  ScrollView,
  AsyncStorage,
} from 'react-native';
import {
  TextField,
  Button,
} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import styles from './LoginPage-style';
import autobind from 'class-autobind';
import {decrypt} from '../helpers/encryption';

type State = {
  email: string;
  password: string;
};
type Props = {
  isLoading: boolean;
  onLoginSubmit: (user: {email: string; password: string}) => null;
  onFacebookLoginButtonPress: () => void;
  onGuestLoginButtonPress: () => void;
  onPressForgot?: () => void;
};

export default class LoginPage extends Component {
  state: State;
  props: Props;

  _onEmailChange(text: string) {
    this.setState({
      email: text,
    });
  }
  _onPasswordChange(text: string) {
    this.setState({
      password: text,
    });
  }
  _onLoginSubmit() {
    this.props.onLoginSubmit({
      email: this.state.email,
      password: this.state.password,
    });
  }
  constructor() {
    super(...arguments);
    autobind(this);
    this.state = {
      email: '',
      password: '',
    };
  }
  componentDidMount() {
    AsyncStorage.multiGet(['@email', '@password'], (error, result) => {
      let [[, email], [, password]] = result;
      if (result != null) {
        this.setState({
          email: email ? email : '',
          password: password ? decrypt(password) : '',
        });
      }
    });
  }
  render() {
    let {isLoading, onPressForgot} = this.props;
    let pressForgot = onPressForgot ? onPressForgot : () => {};
    if (isLoading) {
      return <LoadingIndicator />;
    }
    return (
      <ScrollView
        keyboardShouldPersistTaps
      >
        <View style={styles.mainContainer}>
          <View style={styles.field}>
            <TextField
              placeholder="Masukkan email atau no hp anda"
              label="Email atau No HP"
              value={this.state.email}
              onChangeText={this._onEmailChange}
            />
          </View>
          <View style={styles.field}>
            <TextField
              placeholder="Masukkan kata sandi anda"
              label="Kata Sandi"
              isPassword={true}
              value={this.state.password}
              onChangeText={this._onPasswordChange}
            />
          </View>
          <View style={styles.forgot}>
            <TouchableOpacity onPress={() => pressForgot()}>
              <Text style={styles.labelForgot}>Lupa Kata Sandi?</Text>
            </TouchableOpacity>
          </View>
          <View>
            <Button
              text="MASUK"
              onPress={this._onLoginSubmit}
            />
            <View style={styles.divider}>
              <View style={styles.hr}></View>
              <Text style={styles.textDivider}>atau</Text>
              <View style={styles.hr}></View>
            </View>
            <Button
              text="MASUK DENGAN FACEBOOK"
              iconEntypo="facebook"
              iconStyle={{fontSize: 25, marginRight: 10}}
              style={{backgroundColor: '#3b5998', borderColor: '#3b5998'}}
              textStyle={styles.centeredButton}
              onPress={this.props.onFacebookLoginButtonPress}
            />
            <Button
              text="MASUK SEBAGAI TAMU"
              textStyle={styles.centeredButton}
              inverted
              onPress={this.props.onGuestLoginButtonPress}
            />
          </View>
        </View>
      </ScrollView>
    );
  }
}
