// @flow

import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  root: {
    flex: 1,
    backgroundColor: 'white',
  },
  header: {
    marginHorizontal: 10,
    marginTop: 10,
    fontSize: 14,
  },
  subHeader: {
    marginHorizontal: 10,
    fontSize: 12,
    fontStyle: 'italic',
  },
  bottomMargin: {
    marginBottom: 10,
  },
  notFound: {
    alignSelf: 'center',
    justifyContent: 'center',
    fontSize: 14,
    fontWeight: 'bold',
  },
  chart: {
    margin: 12,
    flexDirection: 'row',
    width: 1500,
  },
  flex: {
    flex: 1,
  },
  wrapper: {
    flex: 1,
    marginHorizontal: 10,
    backgroundColor: 'transparent',
  },
  wrapperChild: {
    flex: 1,
    marginLeft: 50,
  },
  graphRenderer: {
    marginTop: -300,
    flex: 1,
  },
  wrapperLabel: {
    textAlign: 'left',
    marginLeft: -5,
    color: '#447053',
  },
  wrapperDivider: {
    justifyContent: 'flex-end',
    borderRightWidth: 2,
    borderRightColor: '#282928',
    width: 50,
  },
  wrapperContainer: {
    flexDirection: 'row',
    flex: 1,
    height: 50,
    marginLeft: -50,
    borderBottomWidth: 1,
    borderBottomColor: '#b1b4b1',
  },
  dropdownProvince: {
    marginHorizontal: 10,
  },
});
