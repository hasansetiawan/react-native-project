//@flow
import React, {Component} from 'react';
import {View} from 'react-native';
import {TitleBar} from '../core-ui';
import styles from './PlantInfo-style';
import PlantInfoForm from './register/PlantInfoForm';

export default class PlantInfo extends Component {
  render() {
    return (
      <View style={styles.mainContainer}>
        <TitleBar iconButtonName="menu" title="Tambahkan Tanaman" />
        <View style={styles.form}>
          <PlantInfoForm />
        </View>
      </View>
    );
  }
}
