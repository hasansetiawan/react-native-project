// @flow
import {StyleSheet} from 'react-native';

export default StyleSheet.create({
  container: {
    flex: 1,
  },
  textItalic: {
    fontStyle: 'italic',
  },
  textBlock: {
    marginTop: 10,
    paddingHorizontal: 10,
    paddingTop: 20,
  },
});
