//@flow
import React, {Component} from 'react';
import autobind from 'class-autobind';
import {
  View,
  Text,
  Image,
  ScrollView,
} from 'react-native';
import {
  FlatButton,
  TileButtonWrapper,
  TitleBar,
} from '../core-ui';
import LoadingIndicator from '../core-ui/LoadingIndicator';
import {ImageSwiper, ImageSlide, ImageSlideOverlay} from '../core-ui/ImageSwiper';
import {MyPlants, MyPage, Weather, FarmerMedia, GamepadIcon, MarketPrice, News, BusinessAnalysis} from '../core-image';
import defaultHighlightImage from '../images/header.jpg';
import styles from './HomePage-style';

import {FULL_GRADIENT} from '../constants/color';

import type {Highlight} from '../types/Highlight';

type Props = {
  guestMode: boolean;
  navigateTo: () => () => void;
  onFavorite: () => void;
  homeHighlights: Array<Highlight>;
  fetchHighlights: () => void;
  onHighlightSelect: () => void;
  fetchLevels: () => void;
};

export default class Homepage extends Component {
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentWillMount() {
    this.props.fetchHighlights();
    this.props.fetchLevels();
  }

  render() {
    let {guestMode, navigateTo, homeHighlights, onHighlightSelect} = this.props;
    if (homeHighlights.length === 0) {
      return <LoadingIndicator />;
    }
    return (
      <View style={styles.body}>
        <TitleBar
          title="Karsa"
          iconButtonName="menu"
        >
        </TitleBar>
        <ScrollView style={styles.menus}>
          <View style={styles.headerSlide}>
            <Swiper height={135} highlights={homeHighlights} onSelect={onHighlightSelect} onHighlightSelect={onHighlightSelect} />
          </View>
          {guestMode ? null : (
            <TileButtonWrapper>
              <FlatButton style={[styles.tile, styles.lightCyan]} onPress={navigateTo('cultivation')}>
                {this._renderButton(MyPlants, 'Teknis Budidaya')}
              </FlatButton>
              <FlatButton style={[styles.tile, styles.lightGreen]} onPress={navigateTo('thread')}>
                {this._renderButton(MyPage, 'Forum Diskusi')}
              </FlatButton>
            </TileButtonWrapper>
          )}
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.green]} onPress={navigateTo('weather')}>
              {this._renderButton(Weather, 'Cuaca')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={navigateTo('farmerMedia')}>
              {this._renderButton(FarmerMedia, 'Sarana Tani')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkerGreen]} onPress={navigateTo('gamification')}>
              {this._renderButton(GamepadIcon, 'Prestasi')}
            </FlatButton>
          </TileButtonWrapper>
          <TileButtonWrapper>
            <FlatButton style={[styles.tile, styles.lightGreen]} onPress={navigateTo('marketPrice')}>
              {this._renderButton(MarketPrice, 'Harga Pasar')}
            </FlatButton>
            <FlatButton style={[styles.tile, styles.darkGreen]} onPress={navigateTo('news')}>
              {this._renderButton(News, 'Berita')}
            </FlatButton>
            {guestMode ? null : (
              <FlatButton style={[styles.tile, styles.green]} onPress={navigateTo('analysis')}>
                {this._renderButton(BusinessAnalysis, 'Analisis Biaya')}
              </FlatButton>
            )}
          </TileButtonWrapper>
          <View style={styles.spacer} />
        </ScrollView>
      </View>
    );
  }

  _renderButton(image, text) {
    return (
      <View style={styles.button}>
        <Image source={image} style={styles.image} />
        <Text style={styles.buttonText}>{text}</Text>
      </View>
    );
  }
}

type NewsHighlightProps = {
  highlights: Array<Highlight>;
  onHighlightSelect: () => void;
};

function Swiper({highlights, onHighlightSelect, ...otherProps}: NewsHighlightProps) {
  return (
    <ImageSwiper {...otherProps}>
      {
      highlights.map(({threadID, newsID, type, title, writter, by, date}) =>
        /* TODO: ask Arief to send image for highlightcontent endpoint */
        <ImageSlide key={threadID || newsID} picture={defaultHighlightImage} onPress={() => onHighlightSelect(threadID || newsID, type)} gradient={FULL_GRADIENT}>
          <ImageSlideOverlay date={date} source={writter || by} title={title} />
        </ImageSlide>
      )
    }
    </ImageSwiper>
  );
}
