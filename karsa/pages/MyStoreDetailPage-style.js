// @flow

import {StyleSheet} from 'react-native';

let rowWrapper = {
  flexDirection: 'row',
  alignItems: 'center',
};

export default StyleSheet.create({
  mainWrapper: {
    flex: 1,
  },
  infoTitle: {
    fontWeight: 'bold',
  },
  addressText: {
    flexWrap: 'wrap',
  },
  rowWrapper,
  addressInfoWrapper: {
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  contactInfoWrapper: {
    backgroundColor: '#eee',
    paddingHorizontal: 20,
    paddingVertical: 10,
  },
  icon: {
    marginRight: 7,
  },
  filterWrapper: {
    ...rowWrapper,
    backgroundColor: '#eee',
    padding: 10,
  },
  filterContent: {
    paddingHorizontal: 10,
  },
  filterDropdown: {
    flex: 1,
  },
});
