// @flow
import React, {Component} from 'react';
import {View, Text, Image} from 'react-native';
import Swiper from 'react-native-swiper';
import Button from '../core-ui/Button';

import styles from './WalkthroughPage-style';
import {getScreenSize} from '../helpers/getSize';

type Slide = {
  image: ImageSource;
  title: string;
  description: string;
};

type Props = {
  slides: Array<Slide>;
  onNext: () => void;
  onSkip: () => void;
  onStart: () => void;
};

type State = {
  currentSlideIndex: number;
};

export default class WalkthroughView extends Component {
  state: State

  constructor(props: Props) {
    super(props);
    this.state = {
      currentSlideIndex: 0,
    };
    (this: any).handleNext = this.handleNext.bind(this);
    (this: any).handleMomentumScrollEnd = this.handleMomentumScrollEnd.bind(this);
  }

  handleNext() {
    let {currentSlideIndex} = this.state;
    if (currentSlideIndex < (this.props.slides.length - 1)) {
      let nextSlideIndex = currentSlideIndex + 1;
      this.setState({currentSlideIndex: nextSlideIndex});
      this.refs.swiper.scrollBy(1, false);
    }
    if (currentSlideIndex === (this.props.slides.length - 1)) {
      this.props.onStart();
    }
  }

  handleMomentumScrollEnd(event: Object, swiperState: Object) {
    this.setState({currentSlideIndex: swiperState.index});
  }

  render() {
    let {slides, onSkip} = this.props;
    let isLastSlide = this.state.currentSlideIndex === (this.props.slides.length - 1);
    return (
      <View style={styles.walktroughView}>
        <View style={styles.swiperWrapper}>
          <Swiper
            ref="swiper"
            dot={<View style={styles.dot} />}
            activeDot={<View style={styles.activeDot} />}
            loop={false}
            onMomentumScrollEnd={this.handleMomentumScrollEnd}
            width={getScreenSize().width * 0.9}
            height={getScreenSize().height * 0.7}
          >
            {slides.map((slide, index) =>
              <View key={index} style={styles.slide}>
                <View style={styles.slideContent}>
                  <View source={styles.slideImageWrapper}>
                    <Image
                      source={slide.image}
                      style={{width: 250, height: 250}}
                    />
                  </View>
                  <Text style={styles.slideTitle}>{slide.title}</Text>
                  <Text style={styles.slideDescription}>{slide.description}</Text>
                </View>
              </View>
            )}
          </Swiper>
        </View>
        <View style={styles.buttons}>
          <Button text={isLastSlide ? 'Mulai' : 'Selanjutnya'} onPress={this.handleNext} />
          <Button text="Lewati" inverted onPress={onSkip} />
        </View>
      </View>
    );
  }
}
