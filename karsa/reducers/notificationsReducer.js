//@flow
import type {NotificationsState as Notification} from '../types/Notifications';
import type {Action} from '../types/Action';

let initialState = new Map();

export default function notificationReducer(state: Notification = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_NOTIFICATION_SUCCEED': {
      let {notifications, page} = action;
      if (page === 1) {
        return notifications;
      } else {
        return new Map(function*() {
          yield* state;
          yield* notifications;
        }());
      }
    }
    default: return state;
  }
}
