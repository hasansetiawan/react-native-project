// @flow

import type {Action} from '../types/Action';
import type {PestDisease, PestDiseaseDetail, PestDiseaseDetailWithCountermeasure} from '../types/PestDisease';

type DiseaseState = Map<number, PestDisease | PestDiseaseDetail | PestDiseaseDetailWithCountermeasure>;

export default function diseasesReducer(state: DiseaseState, action: Action) {
  if (state == null) {
    return new Map();
  } else {
    switch (action.type) {
      case 'FETCH_PEST_DISEASE_SUCCESS': {
        let newState = new Map(state);
        let {diseases} = action;
        for (let disease of diseases) {
          newState.set(disease.id, disease);
        }
        return newState;
      }
      case 'FETCH_MORE_DISEASE_SUCCESS': {
        let newState = new Map(state);
        let {diseases} = action;
        for (let disease of diseases) {
          newState.set(disease.id, disease);
        }
        return newState;
      }
      case 'DISEASE_COMMODITY_SELECTED': {
        return new Map();
      }
      case 'FETCH_FILTERED_DISEASE_SUCCESS': {
        let {diseases} = action;
        let newState = new Map(state);
        for (let disease of diseases) {
          newState.set(disease.id, disease);
        }
        return newState;
      }
      case 'FETCH_MORE_DISEASE_FAILED': {
        // TODO add fail handler
        return state;
      }
      case 'FETCH_DISEASE_DETAIL_SUCCESS': {
        let {diseaseDetail} = action;
        let newState = new Map(state);
        newState.set(diseaseDetail.id, {...diseaseDetail, type: 'Penyakit'});
        return newState;
      }
      case 'FETCH_DISEASE_DETAIL_FAILED': {
        return state;
      }
      case 'FETCH_SEARCHED_DISEASE_REQUESTED': {
        return new Map();
      }
      case 'FETCH_SEARCHED_DISEASE_SUCCESS': {
        let {diseases} = action;
        let newState = new Map(state);
        for (let disease of diseases) {
          newState.set(disease.id, disease);
        }
        return newState;
      }
      default: {
        return state;
      }
    }
  }
}
