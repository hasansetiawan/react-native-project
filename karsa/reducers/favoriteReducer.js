// @flow
import type {FavoriteState} from '../types/Favorite';
import type {Action} from '../types/Action';

let initialState = {
  loading: false,
  favorites: [],
  selectedFavoriteId: '',
};

export default function favoriteReducer(state: FavoriteState, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'INIT_FAVORITES': {
      return {
        ...state,
        favorites: action.favorites,
      };
    }
    case 'ADD_FAVORITE_SUCCESS': {
      return {
        ...state,
        favorites: action.favorites,
      };
    }
    case 'DELETE_FAVORITE_SUCCESS': {
      return {
        ...state,
        favorites: action.favorites,
      };
    }
    case 'SELECT_FAVORITE': {
      return {
        ...state,
        selectedFavoriteId: action.favorite,
      };
    }
    default: {
      return state;
    }
  }
}
