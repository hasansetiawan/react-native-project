//@flow

import type {MarketPrice} from '../types/MarketPrice';
import type {Action} from '../types/Action';

export default function marketPriceReducer(state: MarketPrice, action: Action) {
  if (state == null) {
    return {
      nationalPriceInfo: {
        priceList: [],
        currDate: '',
      },
      provincePriceInfo: {
        priceList: [],
        provinceName: '',
        currDate: '',
      },
      commodityPriceInfo: {
        priceList: [],
        commodityName: '',
      },
      currentDate: '',
      activeCommodity: '',
      activeProvince: '',
      provinces: [],
      commodities: [],
    };
  } else {
    switch (action.type) {
      case 'SELECTED_PROVINCE_CHANGED': {
        return {
          ...state,
          activeProvince: action.province,
        };
      }
      case 'SELECTED_COMMODITY_CHANGED': {
        return {
          ...state,
          activeCommodity: action.commodityName,
        };
      }
      case 'FETCH_COMMODITY_PRICE_SUCCESS': {
        let {commodityPrice} = action;
        return {
          ...state,
          commodityPriceInfo: {
            priceList: commodityPrice.commodityPriceList,
            commodityName: commodityPrice.commodityName,
          },
          activeCommodity: commodityPrice.commodityName,
        };
      }
      case 'FETCH_MARKET_PRICE_INITIAL_SUCCESS': {
        let {commodities, provinces, nationalPriceInfo, provincePriceInfo} = action;
        return {
          ...state,
          provinces: provinces,
          activeProvince: provincePriceInfo.provinceName,
          commodities: commodities,
          nationalPriceInfo: nationalPriceInfo,
          provincePriceInfo: provincePriceInfo,
          currentDate: nationalPriceInfo.currDate,
        };
      }
      case 'FETCH_NATIONAL_PRICE_SUCCESS': {
        let {nationalPrice} = action;
        return {
          ...state,
          nationalPriceInfo: {
            priceList: nationalPrice.priceList,
            currDate: nationalPrice.currDate,
          },
        };
      }
      case 'FETCH_PROVINCE_PRICE_SUCCESS': {
        let {provincePrice} = action;
        return {
          ...state,
          provincePriceInfo: {
            priceList: provincePrice.priceList,
            currDate: provincePrice.currDate,
            provinceName: provincePrice.provinceName,
          },
          activeProvince: provincePrice.provinceName,
        };
      }
      default: {
        return state;
      }
    }
  }
}
