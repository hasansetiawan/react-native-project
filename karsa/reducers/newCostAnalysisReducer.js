// @flow
import type {Action} from '../types/Action';
import type {AnalyticForm} from '../types/Analytic';

export default function newCostAnalysisReducer(state: AnalyticForm, action: Action) {
  if (state == null) {
    state = {};
  }
  switch (action.type) {
    case 'NEW_COST_ANALYSIS': {
      return action.newAnalysis;
    }
    default: {
      return state;
    }
  }
}
