// @flow

import type {Action} from '../types/Action';
import type {LocationState} from '../types/Location';

export function locationReducer(state: LocationState, action: Action) {
  if (state == null) {
    return {};
  } else {
    switch (action.type) {
      case 'FETCH_GPS_ADDRESS_SUCCESS': {
        let {gpsAddress} = action;
        return {
          ...state,
          gpsAddress,
        };
      }
      case 'FETCH_PROVINCE_LIST_SUCCEED': {
        let {provinceList} = action;
        return {
          ...state,
          provinceList,
        };
      }
      case 'FETCH_CITY_LIST_SUCCEED': {
        let {cityList} = action;
        return {
          ...state,
          cityList,
        };
      }
      case 'FETCH_SUBDISTRICT_LIST_SUCCEED': {
        let {subDistrictList} = action;
        return {
          ...state,
          subDistrictList,
        };
      }
      case 'FETCH_VILLAGE_LIST_SUCCEED': {
        let {villageList} = action;
        return {
          ...state,
          villageList,
        };
      }
      default: return state;
    }
  }
}
