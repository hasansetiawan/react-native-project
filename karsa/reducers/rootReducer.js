// @flow
import {combineReducers} from 'redux';

import currentUserReducer from './currentUserReducer';
import languageReducer from './languageReducer';
import selectedAnalysisReducer from './selectedAnalysisReducer';
import navigationReducer from './navigationReducer';
import plantsReducer, {selectedPlantReducer} from './plantsReducer';
import aboutReducer from './aboutReducer';
import newCostAnalysisReducer from './newCostAnalysisReducer';
import {costAnalysisReducer} from './costAnalysisReducer';
import privacyPolicyReducer from './privacyPolicyReducer';
import marketPriceReducer from './marketPriceReducer';
import plantingInformationReducer from './plantingInformationReducer';
import loadingIndicatorReducer from './loadingIndicatorReducer';
import tempProfileReducer from './tempProfileReducer';
import newsReducer from './newsReducer';
import pestsReducer from './pestsReducer';
import diseasesReducer from './diseasesReducer';
import pestDiseaseCommodityReducer from './pestDiseaseCommodityReducer';
import threadReducer from './threadReducer';
import contactUsReducer from './contactUsReducer';
import selectedPestDiseaseReducer from './selectedPestDiseaseReducer';
import notificationsReducer from './notificationsReducer';
import productsReducer from './productsReducer';
import guestModeReducer from './guestModeReducer';
import addPhotoReducer from './addPhotoReducer';
import seedProductsReducer from './seedProductsReducer';
import fertilizerProductsReducer from './fertilizerProductsReducer';
import pesticideProductsReducer from './pesticideProductsReducer';
import selectedThreadReducer from './selectedThreadReducer';
import newPlantReducer from './newPlantReducer';
import {newItemModalReducer, tutorialModalReducer} from './modalReducer';
import favoriteReducer from './favoriteReducer';
import selectedProductReducer from './selectedProductReducer';
import {locationReducer} from './locationReducer';
import toolsProductsReducer from './toolsProductsReducer';
import threadCategoryReducer from './threadCategoryReducer';
import forumCategoryReducer from './forumCategoryReducer';
import productCommodityReducer from './productCommodityReducer';
import drawerReducer from './drawerReducer';
import productListReducer from './productListReducer';
import nearbyStoreReducer from './nearbyStoreReducer';
import groupReducer from './groupReducer';
import selectedStoreReducer from './selectedStoreReducer';
import selectedGroupReducer from './selectedGroupReducer';
import codePushReducer from './codePushReducer';
import homeHighlightReducer from './homeHighlightReducer';
import plantingGuideReducer from './plantingGuideReducer';
import gamificationReducer from './gamification/gamificationReducer';

export default combineReducers({
  selectedStore: selectedStoreReducer,
  productList: productListReducer,
  productCommodity: productCommodityReducer,
  selectedProduct: selectedProductReducer,
  products: productsReducer,
  seedProducts: seedProductsReducer,
  toolsProducts: toolsProductsReducer,
  fertilizerProducts: fertilizerProductsReducer,
  pesticideProducts: pesticideProductsReducer,
  pestDiseaseCommodities: pestDiseaseCommodityReducer,
  loadingIndicator: loadingIndicatorReducer,
  currentUser: currentUserReducer,
  selectedLanguage: languageReducer,
  navigation: navigationReducer,
  pests: pestsReducer,
  diseases: diseasesReducer,
  plants: plantsReducer,
  about: aboutReducer,
  analysis: costAnalysisReducer,
  selectedAnalysis: selectedAnalysisReducer,
  newCostAnalysisReducer: newCostAnalysisReducer,
  privacypolicy: privacyPolicyReducer,
  marketPrice: marketPriceReducer,
  plantingInformation: plantingInformationReducer,
  selectedPlant: selectedPlantReducer,
  tempProfile: tempProfileReducer,
  news: newsReducer,
  threads: threadReducer,
  selectedThreadID: selectedThreadReducer,
  contactUs: contactUsReducer,
  selectedPestDisease: selectedPestDiseaseReducer,
  notifications: notificationsReducer,
  guestMode: guestModeReducer,
  addPhoto: addPhotoReducer,
  newPlant: newPlantReducer,
  newItemModal: newItemModalReducer,
  tutorialModal: tutorialModalReducer,
  favorite: favoriteReducer,
  threadCategory: threadCategoryReducer,
  forumCategory: forumCategoryReducer,
  location: locationReducer,
  isDrawerOpen: drawerReducer,
  nearbyStore: nearbyStoreReducer,
  groups: groupReducer,
  selectedGroup: selectedGroupReducer,
  codePush: codePushReducer,
  homeHighlights: homeHighlightReducer,
  plantingGuide: plantingGuideReducer,
  gamificationState: gamificationReducer,
});
