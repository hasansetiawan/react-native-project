//@flow
import type {NewsState} from '../types/News';
import type {Action} from '../types/Action';

let initialState = {
  selectedNewsID: 0,
  selectedHighlight: 0,
  allNews: new Map(),
};

export default function newsReducer(state: NewsState = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_NEWS_SUCCEED': {
      let {news} = action;
      let {allNews} = state;
      return {
        ...state,
        allNews: new Map(function*() {
          yield* allNews;
          yield* news;
        }()),
      };
    }
    case 'FETCH_MORE_NEWS_SUCCEED': {
      let {news} = action;
      let {allNews} = state;
      return {
        ...state,
        allNews: new Map(function* () {
          yield* allNews;
          yield* news;
        }()),
      };
    }
    case 'FETCH_NEWS_FAILED': {
      return {
        ...state,
      };
    }
    case 'SAVE_SELECTED_NEWS': {
      let {id} = action;
      return {
        ...state,
        selectedNewsID: id,
      };
    }
    case 'SAVE_SELECTED_HIGHLIGHT': {
      let {id} = action;
      return {
        ...state,
        selectedHighlight: id,
      };
    }
    default: return state;
  }
}
