// @flow
import type {Action} from '../types/Action';

let initialState = false;

export default function guestModeReducer(state: boolean, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'GUEST_LOGIN': {
      return true;
    }
    case 'LOGIN_SUCCESS': {
      return false;
    }
    case 'LOGOUT': {
      return false;
    }
  }
  return state;
}
