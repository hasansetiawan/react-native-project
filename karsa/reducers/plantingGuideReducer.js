// @flow

import type {Action} from '../types/Action';
import type {PlantingGuideState} from '../types/PlantingGuide';

export default function plantingGuideReducer(state: PlantingGuideState, action: Action) {
  if (state == null) {
    return {
      plantingGuides: [],
      fetchedPlantingGuideDetails: new Map(),
    };
  } else {
    switch (action.type) {
      case 'FETCH_PLANTING_GUIDES_SUCCEED': {
        let {plantingGuides} = action;
        return {
          ...state,
          plantingGuides,
        };
      }
      case 'FETCH_PLANTING_GUIDE_DETAIL_SUCCEED': {
        let {fetchedPlantingGuideDetail} = action;
        let newMap = new Map(state.fetchedPlantingGuideDetails);
        newMap.set(fetchedPlantingGuideDetail.plantDetailName, fetchedPlantingGuideDetail);
        return {
          ...state,
          fetchedPlantingGuideDetails: newMap,
        };
      }
      default: return state;
    }
  }
}
