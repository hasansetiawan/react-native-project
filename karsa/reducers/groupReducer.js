// @flow

import type {Action} from '../types/Action';
import type {Group} from '../types/Forum';

let initialState = new Map();

export default function groupReducer(state: Map<number, $Shape<Group>> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_USER_GROUP_LIST_SUCCESS':
    case 'FETCH_GROUP_LIST_SUCCESS':
    case 'FETCH_GROUP_INVITATION_LIST_SUCCESS': {
      let {groups} = action;
      if (state.size > 0) {
        return new Map(function*() { // NOTE: cool way to merge Map objects, using IIGFE (Immediately-Invoked Generator Function Expression)
          yield* state;
          yield* groups;
        }());
      }
      return groups;
    }
    case 'FETCH_SEARCHED_GROUPS_REQUESTED': {
      let {groupCategory} = action;
      let newState = new Map(state);
      for (let [key, value] of newState) {
        if (value.groupCategory === groupCategory) {
          newState.delete(key);
        }
      }
      return newState;
    }
    case 'FETCH_SEARCHED_GROUPS_SUCCESS': {
      let {groups} = action;
      return new Map(function*() {
        yield* state;
        yield* groups;
      }());
    }
    case 'JOIN_GROUP_SUCCESS': {
      let {groupID} = action;
      let group = state.get(groupID);
      let newState = new Map(state);
      if (group) {
        let newGroup = Object.assign(group, {groupCategory: 'joined', statusRequest: false});
        newState.set(newGroup.id, newGroup);
      }
      return newState;
    }
    case 'CONFIRM_GROUP_INVITATION_SUCCESS': {
      // TODO: check if it's correctly working bc right now we don't have any group invitations
      let {groupID} = action;
      let group = state.get(groupID);
      let newState = new Map(state);
      if (group) {
        let newGroup = Object.assign(group, {groupCategory: 'joined'});
        newState.set(newGroup.id, newGroup);
      }
      return newState;
    }
    case 'FETCH_GROUP_DETAIL_SUCCESS': {
      let {group} = action;
      let newState = new Map(state);
      let oldGroup = state.get(group.id);
      if (oldGroup) {
        let {threads, members} = oldGroup;
        group.members = members;
        group.threads = threads;
      }
      newState.set(group.id, group);
      return newState;
    }
    case 'FETCH_GROUP_MEMBERS_SUCCESS': {
      let {groupID, members} = action;
      let group = state.get(groupID);
      let newState = new Map(state);
      if (group) {
        let newGroupMembers = Array.from(members.values());
        if (group.members) {
          newGroupMembers = [...group.members, ...newGroupMembers];
        }
        let newGroup = Object.assign(group, {members: newGroupMembers});
        newState.set(newGroup.id, newGroup);
      }
      return newState;
    }
    case 'FETCH_GROUP_THREADS_SUCCESS': {
      let {groupID, threads} = action;
      let group = state.get(groupID);
      let newState = new Map(state);
      if (group) {
        let newGroupThreads = Array.from(threads.values());
        if (group.threads) {
          newGroupThreads = [...group.threads, ...newGroupThreads];
        }
        let newGroup = Object.assign(group, {threads: newGroupThreads});
        newState.set(newGroup.id, newGroup);
      }
      return newState;
    }
    default: {
      return state;
    }
  }
}
