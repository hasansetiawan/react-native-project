// @flow

import type {Action} from '../types/Action';
import type {Product} from '../types/Product';
type State = {
  products: Map<number, Product>;
  activeList: 'normal' | 'filter' | 'search';
};
export default function productListReducer(state: State, action: Action) {
  if (state == null) {
    return {
      activeList: 'normal',
      products: new Map(),
    };
  } else {
    switch (action.type) {
      case 'EMPTY_PRODUCT_LIST': {
        let {target} = action;
        return {
          activeList: target,
          products: new Map(),
        };
      }
      case 'FETCH_PRODUCT_LIST_SUCCESS': {
        let {target} = action;
        let newProducts = state.activeList !== target ? new Map() : new Map(state.products);
        let {products} = action;
        for (let product of products) {
          newProducts.set(product.id, product);
        }
        return {
          activeList: target,
          products: newProducts,
        };
      }
      case 'FETCH_PRODUCT_LIST_FAILED': {
        // return {
        //   activeList: state.activeList,
        //   products: new Map(),
        // };
        return state;
      }
      default: {
        return state;
      }
    }
  }
}
