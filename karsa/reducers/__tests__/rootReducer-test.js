// @flow
const {describe, it} = global;
import expect from 'expect';

import rootReducer from '../rootReducer';

describe('rootReducer', () => {

  it('should not be null', () => {
    expect(rootReducer).toExist();
  });

});
