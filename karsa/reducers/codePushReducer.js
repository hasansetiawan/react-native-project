// @flow
import type {Action} from '../types/Action';
import type {CodePushState} from '../types/CodePushState';


const initialState = {
  downloadProgress: 0,
  totalDownloadSize: 0,
  isAppUpdating: false,
};

export default function codePushReducer(codePushState: CodePushState, action: Action) {
  if (codePushState == null) {
    return initialState;
  }
  switch (action.type) {
    case 'DOWNLOAD_PROGRESS_UPDATED': {
      return {
        ...codePushState,
        downloadProgress: action.downloadProgress,
        totalDownloadSize: action.totalDownloadSize,
      };
    }
    case 'UPDATING_APP': {
      return {
        ...codePushState,
        isAppUpdating: true,
      };
    }
    case 'UPDATE_FINISHED': {
      return initialState;
    }
    case 'RESET': {
      return initialState;
    }
    default: return codePushState;
  }
}
