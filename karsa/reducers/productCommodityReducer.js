// @flow

import type {Action} from '../types/Action';
import type {ProductCategory} from '../types/Product';

export default function productCategoryReducer(state: Array<ProductCategory>, action: Action) {
  if (state == null) {
    return [];
  } else {
    switch (action.type) {
      case 'FETCH_PRODUCT_COMMODITY_SUCCESS': {
        let {productCommodity} = action;
        return productCommodity;
      }
      default: return state;
    }
  }
}
