// @flow

import type {Action} from '../types/Action';
import type {NewPlantState} from '../types/NewPlant';

export default function newPlantReducer(state: NewPlantState, action: Action) {
  if (state == null) {
    return {};
  } else {
    switch (action.type) {
      case 'FETCH_PLANT_TYPES_SUCCESS': {
        let {plantTypes} = action;
        return {
          ...state,
          plantTypes,
        };
      }
      case 'FETCH_PLANT_CATEGORIES_SUCCESS': {
        let {plantCategories} = action;
        return {
          ...state,
          plantCategories,
        };
      }
      case 'FETCH_PLANT_VARIETIES_SUCCESS': {
        let {plantVarieties} = action;
        return {
          ...state,
          plantVarieties,
        };
      }
      case 'FETCH_PLANTING_METHODS_SUCCESS': {
        let {plantingMethods} = action;
        return {
          ...state,
          plantingMethods,
        };
      }
      case 'FETCH_PLANTING_RECOMMENDATION_SUCCESS': {
        let {plantingRecommendation} = action;
        return {
          ...state,
          plantingRecommendation,
        };
      }
      default: return state;
    }
  }
}
