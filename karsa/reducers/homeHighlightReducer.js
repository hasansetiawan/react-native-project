//@flow

import type {Action} from '../types/Action';
import type {Highlight} from '../types/Highlight';
type State = Array<Highlight>;

let initialState = [];

export default function homeHighlightReducer(state: State = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_HOME_HIGHLIGHT_SUCCEED': {
      return action.highlights;
    }
  }
  return state;
}
