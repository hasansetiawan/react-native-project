// @flow

import type {Action} from '../types/Action';

export default function selectedStoreReducer(state: number, action: Action) {
  if (state == null) {
    return -1;
  } else {
    switch (action.type) {
      case 'NEARBY_STORE_SELECTED': {
        let {storeID} = action;
        return storeID;
      }
      default: {
        return state;
      }
    }
  }
}
