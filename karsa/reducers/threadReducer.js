//@flow

import type {Thread} from '../types/Forum.js';
import type {Action} from '../types/Action';

type State = Map<number, $Shape<Thread>>;
let initialState = new Map();

export default function threadReducer(state: State, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'FETCH_GROUP_THREADS_SUCCESS':
    case 'FETCH_OTHER_THREADS_SUCCESS':
    case 'FETCH_USER_THREADS_SUCCESS': {
      let {threads, page} = action;
      if (page === 1) {
        return threads;
      }
      let newState = new Map(state);
      for (let [key, value] of threads) {
        newState.set(key, value);
      }
      return newState;
    }
    case 'FETCH_THREAD_DETAIL_SUCCESS': {
      let {thread} = action;
      let newState = new Map(state);
      let oldThread = state.get(thread.id);
      let newThread = {...thread};
      if (oldThread) {
        let comments = new Map(oldThread.comments);
        if (comments.size > 0) {
          newThread.comments = new Map(comments);
        }
      }
      newState.set(thread.id, newThread);
      return newState;
    }
    case 'FETCH_COMMENTS_SUCCESS': {
      let {threadID, comments} = action;
      let updatedThread = state.get(threadID);
      let newState = new Map(state);
      if (updatedThread) {
        let oldComments = new Map(updatedThread.comments);
        if (oldComments.size > 0) {
          for (let [key, comment] of comments) {
            oldComments.set(key, comment);
          }
        } else {
          oldComments = new Map(comments);
        }
        updatedThread.comments = new Map(oldComments);
        newState.set(updatedThread.id, updatedThread);
      } else {
        newState.set(threadID, {comments});
      }
      return newState;
    }
    case 'LIKE_THREAD_SUCCESS': {
      let {threadID, userLikes} = action;
      let updatedThread = state.get(threadID);
      let newState = new Map(state);
      if (updatedThread) {
        updatedThread.userLikes = userLikes;
        updatedThread.likeCount = userLikes.length;
        newState.set(updatedThread.id, updatedThread);
      }
      return newState;
    }
    case 'LIKE_COMMENT_SUCCESS': {
      let {threadID, commentID, userLikes} = action;
      let updatedThread = state.get(threadID);
      let newState = new Map(state);
      if (updatedThread && updatedThread.comments) {
        let comment = updatedThread.comments.get(commentID);
        if (comment) {
          comment.userLikes = userLikes;
          comment.likeCount = userLikes.length;
          newState.set(updatedThread.id, updatedThread);
        }
      }
      return newState;
    }
    case 'POST_COMMENT_SUCCESS': {
      let {threadID, comments} = action;
      let updatedThread = state.get(threadID);
      let newState = new Map(state);
      if (updatedThread) {
        updatedThread.comments = new Map(comments);
        newState.set(updatedThread.id, updatedThread);
      }
      return newState;
    }
    case 'FETCH_USER_THREADS_REQUESTED':
    case 'FETCH_OTHER_THREADS_REQUESTED':
    case 'FETCH_FILTERED_THREADS_REQUESTED':
    case 'FETCH_SEARCHED_THREADS_REQUESTED': {
      let {page} = action;
      if (page === 1) {
        return initialState;
      }
      return state;
    }
    case 'FETCH_FILTERED_THREADS_SUCCESS':
    case 'FETCH_SEARCHED_THREADS_SUCCESS': {
      let {threads, page} = action;
      if (page === 1) {
        return threads;
      }
      let newState = new Map(state);
      for (let [key, value] of threads) {
        newState.set(key, value);
      }
      return newState;
    }
  }
  return state;
}
