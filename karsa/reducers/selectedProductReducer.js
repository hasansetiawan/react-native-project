// @flow

import type {Action} from '../types/Action';

export default function selectedProductReducer(state: number, action: Action) {
  if (state == null) {
    return -1;
  } else {
    switch (action.type) {
      case 'FETCH_PRODUCT_DETAIL_REQUESTED': {
        let {productID} = action;
        return productID;
      }
      default: {
        return state;
      }
    }
  }
}
