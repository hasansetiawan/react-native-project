import type {Action} from '../types/Action';
import type {NearbyStoreState} from '../types/NearbyStore';

export default function nearbyStoreReducer(state: NearbyStoreState, action: Action) {
  if (state == null) {
    return {
      nearbyStores: new Map(),
    };
  } else {
    switch (action.type) {
      case 'FETCH_STORE_BY_PRODUCT_SUCCESS': {
        let {stores} = action;
        let newState = new Map(state.nearbyStores);
        for (let store of stores) {
          newState.set(store.id, store);
        }
        return {
          nearbyStores: newState,
        };
      }
      case 'FETCH_NEARBY_STORE_SUCCEED': {
        let {nearbyStores} = action;
        let newState = new Map(state.nearbyStores);
        for (let [key, value] of nearbyStores) {
          newState.set(key, value);
        }
        return {
          nearbyStores: newState,
        };
      }
      default: return state;
    }
  }
}
