// @flow
import type {Action} from '../types/Action';
import type {PlantingInformation} from '../types/Plant';

export default function plantingInformation(state: PlantingInformation, action: Action) {
  if (state == null) {
    return {};
  } else {
    switch (action.type) {
      case 'FETCH_PLANT_DETAIL_SUCCESS': {
        let {plantingInfoDetail} = action;
        return {
          ...state,
          ...plantingInfoDetail,
        };
      }
      default: {
        return state;
      }
    }
  }
}
