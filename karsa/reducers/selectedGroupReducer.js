// @flow

import type {Action} from '../types/Action';

export default function selectedStoreReducer(state: number = -1, action: Action) {
  switch (action.type) {
    case 'GROUP_SELECTED': {
      let {groupID} = action;
      return groupID;
    }
    default: {
      return state;
    }
  }
}
