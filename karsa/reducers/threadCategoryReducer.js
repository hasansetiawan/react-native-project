// @flow

import type {Action} from '../types/Action';
import type {ThreadCategoryState} from '../types/Forum';

let initialState = {
  categories: new Map(),
  subCategories: new Map(),
  filterCategories: new Map(),
};

export default function threadCategoryReducer(state: ThreadCategoryState = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_THREAD_CATEGORIES_SUCCESS': {
      let {categories} = action;
      return {
        ...state,
        categories: new Map(categories),
        subCategories: new Map(),
      };
    }
    case 'FETCH_THREAD_SUB_CATEGORIES_SUCCESS': {
      let {subCategories} = action;
      return {
        ...state,
        subCategories: new Map(subCategories),
      };
    }
    case 'FETCH_THREAD_CATEGORIES_FILTER_SUCCESS': {
      let {threadCategories} = action;
      return {
        ...state,
        filterCategories: new Map(threadCategories),
      };
    }
    default: {
      return state;
    }
  }
}
