// @flow

import type {Action} from '../types/Action';
import type {Forum} from '../types/Forum';

let initialState = new Map();

export default function forumCategoryReducer(state: Map<number, Forum> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_FORUM_CATEGORIES_SUCCESS': {
      let {forumCategories} = action;
      return new Map(forumCategories);
    }
    default: {
      return state;
    }
  }
}
