// @flow

import type {Action} from '../types/Action';
import type {Product, ProductDetail} from '../types/Product';

export default function productsReducer(state: Map<number, Product | ProductDetail>, action: Action) {
  if (state == null) {
    return new Map();
  } else {
    switch (action.type) {
      case 'FETCH_PRODUCTS_PEST_SUCCESS': {
        let newState = new Map(state);
        let {products} = action;
        for (let product of products) {
          newState.set(product.id, product);
        }
        return newState;
      }
      case 'FETCH_PRODUCTS_PEST_FAILED': {
        return state;
        // TODO Handle Error
      }
      case 'FETCH_PRODUCTS_PLANT_SUCCESS': {
        let newState = new Map(state);
        let {products} = action;
        for (let product of products) {
          newState.set(product.id, product);
        }
        return newState;
      }
      case 'FETCH_PRODUCTS_DISEASE_SUCCESS': {
        let newState = new Map(state);
        let {products} = action;
        for (let product of products) {
          newState.set(product.id, product);
        }
        return newState;
      }
      case 'FETCH_PRODUCTS_DISEASE_FAILED': {
        return state;
        // TODO Handle Error
      }
      case 'FETCH_SEED_SUCCESSFUL': {
        let newState = new Map(state);
        let {allProducts} = action.payload;
        for (let product of allProducts) {
          newState.set(product.id || Math.random(), product);
        }
        return newState;
      }
      case 'FETCH_FERTILIZER_SUCCESSFUL': {
        let newState = new Map(state);
        let {allProducts} = action.payload;
        for (let product of allProducts) {
          newState.set(product.id || Math.random(), product);
        }
        return newState;
      }
      case 'FETCH_PESTICIDE_SUCCESSFUL': {
        let newState = new Map(state);
        let {allProducts} = action.payload;
        for (let product of allProducts) {
          newState.set(product.id || Math.random(), product);
        }
        return newState;
      }
      case 'FETCH_TOOLS_SUCCESSFUL': {
        let newState = new Map(state);
        let {allProducts} = action.payload;
        for (let product of allProducts) {
          newState.set(product.id, product);
        }
        return newState;
      }
      case 'FETCH_PRODUCT_DETAIL_SUCCESS': {
        let {productDetail} = action;
        let newState = new Map(state);
        let prevProduct = newState.get(productDetail.id);
        newState.set(productDetail.id, {...prevProduct, ...productDetail});
        return newState;
      }
      case 'FETCH_PRODUCT_REVIEW_SUCCESS': {
        let {productID, reviews} = action;
        let newState = new Map(state);
        let tempProduct = newState.get(productID);
        newState.set(productID, {...tempProduct, reviews});
        return newState;
      }
      case 'FETCH_PRODUCT_REVIEW_FAILED': {
        let {productID} = action;
        let newState = new Map(state);
        let tempProduct = newState.get(productID);
        let reviews = [];
        if (tempProduct) {
          if (tempProduct.reviews) {
            reviews = tempProduct.reviews;
          }
        }
        newState.set(productID, {...tempProduct, reviews});
        return newState;
      }
      case 'POST_REVIEW_SUCCESS': {
        let {productID, reviews} = action;
        let newState = new Map(state);
        let tempProduct = newState.get(productID);
        newState.set(productID, {...tempProduct, reviews});
        return newState;
      }
      case 'FETCH_STORE_PRODUCTS_SUCCESS': {
        let {products} = action;
        let newState = new Map(state);
        for (let product of products) {
          newState.set(
            product.id,
            newState.get(product.id) ? {...newState.get(product.id), ...product} : product
          );
        }
        return newState;
      }
      default: {
        return state;
      }
    }
  }
}
