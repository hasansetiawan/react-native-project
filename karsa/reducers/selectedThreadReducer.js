//@flow

import type {Action} from '../types/Action';

type State = ?number;
let initialState = -1;

export default function selectedThreadReducer(state: State, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'THREAD_SELECTED': {
      return action.threadID;
    }
    default: {
      return state;
    }
  }
}
