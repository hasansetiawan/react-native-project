// @flow
import type {Action} from '../types/Action';
import type {GalleryData} from '../types/Plant';

export default function addPhotoReducer(state: GalleryData, action: Action) {
  if (state == null) {
    state = {plantID: '', photo: ''};
  }
  switch (action.type) {
    case 'CLEAR_TEMPORARY_PHOTO': {
      return {
        ...state,
        photo: '',
        tempImage: {},
      };
    }
    case 'PICK_PHOTO': {
      return {
        ...state,
        photo: action.image.uri,
        tempImage: action.image,
      };
    }
    case 'ADD_PHOTO_POST_SUCCESS': {
      let setEmptyState = {plantID: '', photo: ''};
      return {
        ...setEmptyState,
      };
    }
    default: {
      return state;
    }
  }
}
