// @flow

import type {UserScore} from '../../types/Gamification';
import type {Action} from '../../types/Action';

const initialState = [];

export default function levelReducer(state: Array<UserScore> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_USER_SCORES_SUCCEED': {
      return action.userScores;
    }
    default: {
      return state;
    }
  }
}
