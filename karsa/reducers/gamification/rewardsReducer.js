// @flow

import type {Reward} from '../../types/Reward';
import type {Action} from '../../types/Action';

const initialState = [];

export default function rewardsReducer(state: Array<Reward> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_REWARDS_SUCCESS': {
      let {rewards} = action;
      return rewards;
    }
    //TODO FILTER ACTION TYPE HERE
    default: return state;
  }
}
