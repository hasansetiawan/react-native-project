// @flow

import {combineReducers} from 'redux';

import levelsReducer from './levelsReducer';
import rewardsReducer from './rewardsReducer';
import actionsReducer from './actionsReducer';
import userActionsReducer from './userActionsReducer';
import userScoresReducer from './userScoresReducer';
import activitiesReducer from './activitiesReducer';

export default combineReducers({
  levels: levelsReducer,
  rewards: rewardsReducer,
  actions: actionsReducer,
  userActions: userActionsReducer,
  userScores: userScoresReducer,
  activities: activitiesReducer,
});
