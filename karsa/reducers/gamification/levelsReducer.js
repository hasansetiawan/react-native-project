// @flow

import type {Level} from '../../types/Gamification';
import type {Action} from '../../types/Action';

const initialState = [];

export default function levelReducer(state: Array<Level> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_LEVELS_SUCCESS': {
      let {levels} = action;
      return levels;
    }
    default: return state;
  }
}
