// @flow

import type {UserGamificationAction} from '../../types/Gamification';
import type {Action} from '../../types/Action';

const initialState = [];

export default function levelReducer(state: Array<UserGamificationAction> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_USER_GAMIFICATION_ACTIONS_SUCCEED': {
      return action.userActions;
    }
    default: {
      return state;
    }
  }
}
