// @flow

import type {GamificationAction} from '../../types/Gamification';
import type {Action} from '../../types/Action';

const initialState = [];

export default function levelReducer(state: Array<GamificationAction> = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_GAMIFICATION_ACTIONS_SUCCEED': {
      return action.actions;
    }
    default: {
      return state;
    }
  }
}
