// @flow

import type {Activity} from '../../types/Activity';
import type {Action} from '../../types/Action';

const initialState = [];

export default function levelReducer(state: Array<Activity> = initialState, action: Action) {
  switch (action.type) {
    case 'SAVE_ACTIVITIES': {
      return action.activities;
    }
    default: {
      return state;
    }
  }
}
