//@flow

import type {CostDetail} from '../types/Cost';
import type {Action} from '../types/Action';

type CostAnalysisState = Map<number, CostDetail>;

let costDetail = {
  plantID: 0,
  userID: 0,
  id: 0,
  title: 'LOADING DATA...',
  farmingProductionCost: 0,
  seedsCost: 0,
  fertilizerCost: 0,
  labourCost: 0,
  totalExpense: 0,
  firstHarvest: 0,
  endHarvest: '',
  price: 0,
  grossIncome: '',
  netIncome: '',
  createdAt: '',
  depreciation: 0,
  depreciationPercent: '',
  linkPdf: 'link', // TODO
};

let initialCostAnalysis = new Map();
initialCostAnalysis.set(0, costDetail);

export function costAnalysisReducer(state: CostAnalysisState = initialCostAnalysis, action: Action) {
  if (state == null) {
    return initialCostAnalysis;
  }
  switch (action.type) {
    case 'FETCH_ANALYSIS_SUCCESS': {
      let {analysis} = action;
      let newState = new Map();
      for (let item of analysis) {
        newState.set(item.id, item);
      }
      return newState;
    }
  }
  return state;
}
