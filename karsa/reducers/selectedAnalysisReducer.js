// @flow
import type {Action} from '../types/Action';

export default function selectedAnalysisReducer(state: number, action: Action) {
  if (state == null) {
    state = 0;
  }
  switch (action.type) {
    case 'SELECTED_ANALYSIS': {
      return action.selectedAnalysis;
    }
    default: {
      return state;
    }
  }
}
