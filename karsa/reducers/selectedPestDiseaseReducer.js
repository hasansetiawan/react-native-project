// @flow


type State = {
  id: number;
  type: 'pest' | 'disease';
};
import type {Action} from '../types/Action';

export default function selectedPestDiseaseReducer(state: State, action: Action) {
  if (state == null) {
    return {
      pestID: null,
      diseaseID: null,
    };
  } else {
    switch (action.type) {
      case 'FETCH_PEST_DETAIL_REQUESTED': {
        let {id} = action;
        return {
          type: 'pest',
          id,
        };
      }
      case 'FETCH_DISEASE_DETAIL_REQUESTED': {
        let {id} = action;
        return {
          id,
          type: 'disease',
        };
      }
      default: {
        return state;
      }
    }
  }
}
