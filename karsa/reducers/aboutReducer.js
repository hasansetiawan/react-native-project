//@flow

import type {About} from '../types/About.js';
import type {Action} from '../types/Action';

type State = Array<About>;
let initialState = {
  id: 0,
  title: '',
  description: '',
  picture: '',
  createdAt: '',
  updatedAt: '',
  urlPicture: '',
};

export default function aboutReducer(state: State, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'FETCH_ABOUT_SUCCESS': {
      return {
        ...action.about,
      };
    }
  }
  return state;
}
