// @flow

import type {Action} from '../types/Action';
import type {Plant, PlantDetail, PlantDetailWithPhotos, PlantHeader} from '../types/Plant';

type PlantState = Map<number, Plant | PlantDetail | PlantDetailWithPhotos | PlantHeader>;

export default function plantsReducer(state: PlantState, action: Action) {
  if (state == null) {
    return new Map();
  } else {
    switch (action.type) {
      case 'FETCH_ALL_PLANTS_SUCCESS': {
        let newState = new Map(state);
        let {plants} = action;
        for (let plant of plants) {
          newState.set(plant.id, plant);
        }
        return newState;
      }
      case 'FETCH_ALL_PLANTS_FAILED': {
        return state;
      }
      case 'FETCH_PLANTS_SUCCESS': {
        let {page, plants} = action;
        if (page === 1) {
          let newState = new Map();
          for (let plant of plants) {
            newState.set(plant.id, plant);
          }
          return newState;
        }
        let newState = new Map(state);
        for (let plant of plants) {
          newState.set(plant.id, plant);
        }
        return newState;
      }
      case 'FETCH_PLANT_FOR_PRODUCT_SUCCESS': {
        let newState = new Map(state);
        let {plants} = action;
        for (let plant of plants) {
          let temp = newState.get(plant.id);
          newState.set(plant.id, {...temp, ...plant});
        }
        return newState;
      }
      case 'FETCH_PLANT_DETAIL_SUCCESS': {
        let newState = new Map(state);
        let {plantDetail} = action;
        newState.set(plantDetail.id, plantDetail);
        return newState;
      }
      case 'POST_USE_PRODUCT_SUCCESS': {
        let {plantListID} = action;
        let newState = new Map(state);
        for (let id of plantListID) {
          let temp = newState.get(id);
          if (temp) {
            temp.status = 'use';
            newState.set(id, temp);
          }
        }
        return newState;
      }
      case 'FETCH_PLANT_DETAIL_FAILED': {
        return state;
      }
      case 'SEARCH_PLANT_SUCCEED': {
        let newState = new Map();
        let {plants} = action;
        for (let plant of plants) {
          newState.set(plant.id, plant);
        }
        return newState;
      }
      case 'SEARCH_PLANT_EMPTY': {
        return new Map();
      }
      default: return state;
    }
  }
}

export function selectedPlantReducer(state: number, action: Action) {
  if (state == null) {
    return 0;
  } else {
    switch (action.type) {
      case 'PLANT_SELECTED': {
        return action.plantID;
      }
      default: {
        return state;
      }
    }
  }
}
