//@flow
import type {Action} from '../types/Action';
import type {TempProfile} from '../types/User';

export default function tempProfile(state: TempProfile, action: Action) {
  if (state == null) {
    return generateInitialState();
  }
  switch (action.type) {
    case 'INITIALIZE_TEMP_REDUCER': return action.currentUser;
    case 'RESET_TEMP_REDUCER': return action.currentUser;
    case 'FETCH_ADDRESS_SUCCEED': {
      return {
        ...state,
        ...action.payload,
      };
    }
    case 'UPDATE_CURRENTLY_SELECTED_ADDRESS': {
      let {currentSelectedSubAddress, subAddressData} = action;
      let subAddressID = subAddressData.value;
      let subAddressName = subAddressData.label;
      return {
        ...state,
        cityName: '',
        city: -1,
        [currentSelectedSubAddress]: subAddressID,
        [currentSelectedSubAddress + 'Name']: subAddressName,
      };
    }
    case 'EDIT_PROFILE_CHANGED': return {...state, [action.fieldName]: action.fieldValue};
    case 'CHANGE_AVATAR': {
      return {
        ...state,
        avatar: action.image.uri,
        tempImage: action.image,
      };
    }
    default: return state;
  }
}

function generateInitialState() {
  return {
    name: '',
    email: '',
    avatar: '',
    avatarFacebook: '',
    gender: '',
    noHp: '',
    provinceName: '',
    province: '',
    allProvinceData: [{value: '', label: ''}],
    cityName: '',
    city: '',
    allCitiesData: [{value: '', label: ''}],
    fullAddress: '',
    tempImage: null,
  };
}
