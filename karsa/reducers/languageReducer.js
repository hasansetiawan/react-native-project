// @flow
import type {Action} from '../types/Action';

export default function languageReducer(selectedLanguage: string, action: Action) {
  if (selectedLanguage == null) {
    selectedLanguage = 'id';
  }
  switch (action.type) {
    case 'LANGUAGE_SELECTED': {
      return action.language;
    }
    default: {
      return selectedLanguage;
    }
  }
}
