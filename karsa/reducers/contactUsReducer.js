//@flow

import type {ContactUs} from '../types/Office';
import type {Action} from '../types/Action';

let initalState = {
  mainOffice: {
    id: '',
    office: 'Kantor tidak tersedia',
    address: 'Alamat tidak tersedia',
    phone: 'Nomor handphone tidak tersedia',
    email: 'Email tidak tersedia',
    createdAt: '',
    updatedAt: '',
  },
  branchOffices: [],
};

export default function contactUsReducer(state: ContactUs = initalState, action: Action) {
  switch (action.type) {
    case 'FETCH_CONTACT_DATA_SUCCEED': {
      let {contactData} = action;
      let mainOffice = contactData.filter((data) => data.office === 'Kantor Utama').pop();
      let branchOffices = contactData.filter((data) => data.office !== 'Kantor Utama');

      Object.keys(mainOffice)
        .forEach((key) => {
          if (mainOffice[key] === '') {
            mainOffice[key] = 'Data tidak tersedia';
            if (key === 'phone') {
              mainOffice[key] = '021 66607368 ext 102';
            }
          }
        });

      return {
        ...state,
        mainOffice: {
          ...mainOffice,
        },
        branchOffices: [
          ...branchOffices,
        ],
      };
    }
    case 'FETCH_CONTACT_DATA_FAILED': {
      //TODO
      return state;
    }
    case 'SUGGESTION_SUBMISSION_SUCCEED': {
      //TODO whether the submission succeed or not
      return state;
    }
    default: return state;
  }
}
