//@flow
import type {Action} from '../types/Action';
import type {ProductState} from '../types/Product';

let initialState = {
  allId: [],
  byHighlight: [],
  byPopularity: [],
  byLatest: [],
};

export default function fertilizerProductsReducer(state: ProductState = initialState, action: Action) {
  switch (action.type) {
    case 'FETCH_FERTILIZER_SUCCESSFUL': {
      let {
        allId,
        byHighlight,
        byPopularity,
        byLatest,
      } = action.payload;
      return {
        ...state,
        allId,
        byHighlight,
        byPopularity,
        byLatest,
      };
    }
    case 'FETCH_FERTILIZER_FAILED': {
      //TODO: handle error
      return state;
    }
    default: return state;
  }
}
