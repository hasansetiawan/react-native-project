//@flow

import type {PrivacyPolicy} from '../types/About.js';
import type {Action} from '../types/Action';

type State = Array<PrivacyPolicy>;
let initialState = {
  desc: 'Loading data ...',
};

export default function privacyPolicyReducer(state: State, action: Action) {
  if (state == null) {
    return initialState;
  }
  switch (action.type) {
    case 'FETCH_PRIVACY_POLICY_SUCCESS': {
      return {
        ...action.privacypolicy,
      };
    }
  }
  return state;
}
