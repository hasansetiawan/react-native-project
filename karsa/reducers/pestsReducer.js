// @flow

import type {Action} from '../types/Action';
import type {PestDisease, PestDiseaseDetail, PestDiseaseDetailWithCountermeasure} from '../types/PestDisease';

type PestState = Map<number, PestDisease | PestDiseaseDetail | PestDiseaseDetailWithCountermeasure>;

export default function pestsReducer(state: PestState, action: Action) {
  if (state == null) {
    return new Map();
  } else {
    switch (action.type) {
      case 'FETCH_PEST_DISEASE_SUCCESS': {
        let {pests} = action;
        let newState = new Map(state);
        for (let pest of pests) {
          newState.set(pest.id, pest);
        }
        return newState;
      }
      case 'FETCH_MORE_PEST_SUCCESS': {
        let newState = new Map(state);
        let {pests} = action;
        for (let pest of pests) {
          newState.set(pest.id, pest);
        }
        return newState;
      }
      case 'PEST_COMMODITY_SELECTED': {
        return new Map();
      }
      case 'FETCH_FILTERED_PEST_SUCCESS': {
        let {pests} = action;
        let newState = new Map(state);
        for (let pest of pests) {
          newState.set(pest.id, pest);
        }
        return newState;
      }
      case 'FETCH_MORE_PEST_FAILED': {
        // TODO: Add fail handler
        return state;
      }
      case 'FETCH_PEST_DETAIL_SUCCESS': {
        let {pestDetail} = action;
        let newState = new Map(state);
        newState.set(pestDetail.id, {...pestDetail, type: 'Hama'});
        return newState;
      }
      case 'FETCH_SEARCHED_PEST_REQUESTED': {
        return new Map();
      }
      case 'FETCH_SEARCHED_PEST_SUCCESS': {
        let {pests} = action;
        let newState = new Map(state);
        for (let pest of pests) {
          newState.set(pest.id, pest);
        }
        return newState;
      }
      default: {
        return state;
      }
    }

  }
}
