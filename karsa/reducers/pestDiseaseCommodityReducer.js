// @flow

import type {Action} from '../types/Action';
import type {PestDiseaseCommodity} from '../types/PestDisease';

export default function pestDiseaseCommodityReducer(state: Map<number, PestDiseaseCommodity>, action: Action) {
  if (state == null) {
    return new Map();
  } else {
    switch (action.type) {
      case 'FETCH_PEST_DISEASE_SUCCESS': {
        let newState = new Map(state);
        let {commodities} = action;
        for (let commodity of commodities) {
          newState.set(commodity.id, commodity);
        }
        return newState;
      }
      default: {
        return state;
      }
    }
  }
}
