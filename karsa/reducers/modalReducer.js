// @flow

import type {Action} from '../types/Action';
import type {ModalState} from '../types/Modal';

export function newItemModalReducer(state: ModalState, action: Action) {
  if (state == null) {
    return {
      ...state,
      isShown: false,
    };
  } else {
    switch (action.type) {
      case 'SHOW_NEW_ITEM_MODAL': {
        return {
          ...state,
          isShown: true,
        };
      }
      case 'HIDE_NEW_ITEM_MODAL': {
        return {
          ...state,
          isShown: false,
        };
      }
      default: return state;
    }
  }
}

export function tutorialModalReducer(state: ModalState, action: Action) {
  if (state == null) {
    return {
      ...state,
      isShown: false,
    };
  } else {
    switch (action.type) {
      case 'SHOW_TUTORIAL_MODAL': {
        return {
          ...state,
          isShown: true,
        };
      }
      case 'HIDE_TUTORIAL_MODAL': {
        return {
          ...state,
          isShown: false,
        };
      }
      default: return state;
    }
  }
}
