// @flow

export const SERVER_API = 'http://www.ingkarsa.com/api/v2';
export const STAGING_API = 'http://karsa-staging.livepreview.org/api/v2';
export const GOOGLE_MAPS_API = 'https://maps.googleapis.com/maps/api';
export const GAMIFY_API = 'http://karsa-gamify.livepreview.org/api';

export default STAGING_API;
