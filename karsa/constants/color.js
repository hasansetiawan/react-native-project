// @flow

export const PRIMARY_GREEN = '#00c853';
export const SECONDARY_GREEN = '#139E13';
export const LIGHT_CYAN = '#33BBA0';
export const LIGHTER_GREEN = '#2CBD69';
export const LIGHT_GREEN = '#78C444';
export const GREEN = '#1FB835';
export const DARK_GREEN = '#5BA82F';
export const DARKER_GREEN = '#1E975C';
export const DARK_YELLOW = '#B89815';
export const PRIMARY_GREY = '#D4D4D4';
export const SECONDARY_GREY = '#EBEBEB';
export const ALTERNATE_GREY = '#EDEDED';
export const LIGHT_GREY = '#999';
export const GREY = '#666';
export const DARK_GREY = '#444';
export const GRADIENT = [
  'transparent',
  'transparent',
  'transparent',
  'rgba(0, 0, 0, 0.2)',
  'rgba(0, 0, 0, 0.6)',
  'rgba(0, 0, 0, 0.8)',
];
export const FULL_GRADIENT = [
  'transparent',
  'rgba(0, 0, 0, 0.2)',
  'rgba(0, 0, 0, 0.4)',
  'rgba(0, 0, 0, 0.6)',
  'rgba(0, 0, 0, 0.8)',
  'rgba(0, 0, 0, 1)',
];
