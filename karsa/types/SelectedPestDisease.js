// @flow

export type SelectedPestDisease = {
  type: 'pest' | 'disease';
  id: number;
};
