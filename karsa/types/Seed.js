//@flow

export type Seed = {
  id: number;
  seedName: string;
  createdAt: string;
  updatedAt: string;
};
