//@flow
export type Government = {
  id: number;
  userID: number;
  name: string;
  address: string;
  phoneNumber: string;
  createdAt: string;
  updatedAt: string;
};
