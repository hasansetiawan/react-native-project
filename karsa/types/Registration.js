// @flow

export type Registration = {
  email: string;
  fullName: string;
  password: string;
};

export type FBUserRegistration = {
  id: string;
  email: string;
  name: string;
  role: string;
  avatar: string;
};
