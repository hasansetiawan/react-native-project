// @flow

export type GalleryPhoto = {
  primary: boolean;
  photo?: ImageSource;
  photoThumb: ImageSource;
};
