//@flow

export type Shop = {
  id: number;
  userID: number;
  name: string;
  address: string;
  province: number;
  city: number;
  phoneNumber: string;
  productID: string;
  productCount: number;
  picture: ImageSource;
};
