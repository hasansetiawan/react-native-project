//@flow

export type Ads = {
  id: number;
  name: string;
  desc: string;
  url: string;
  page: ?string;
  image: ?string;
  createdAt: string;
  updatedAt: string;
};
