// @flow
import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';

import CompanyProfileAPI from '../API/CompanyProfileAPI';

function* fetchPrivacyPolicy() {
  try {
    let privacypolicy = yield call(CompanyProfileAPI.getPrivacyPolicy);
    yield put({type: 'FETCH_PRIVACY_POLICY_SUCCESS', privacypolicy: privacypolicy});
  } catch (error) {
    yield put({type: 'FETCH_PRIVACY_POLICY_FAILED', error: error});
  }
}

export function* watchPrivacyPolicySaga(): Object {
  const pattern = (action) => {
    let isPushRoute = action.type === 'PUSH_ROUTE';
    let isPrivacyPolicy = action.key === 'privacypolicy';
    return isPushRoute && isPrivacyPolicy;
  };
  yield takeLatest(pattern, fetchPrivacyPolicy);
}
