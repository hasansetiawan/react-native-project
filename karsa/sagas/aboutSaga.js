// @flow

import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';

import CompanyProfileAPI from '../API/CompanyProfileAPI';

export function* fetchAbout(): Generator<*, *, *> {
  try {
    let about = yield call(CompanyProfileAPI.getAbout);
    yield put({type: 'FETCH_ABOUT_SUCCESS', about: about});
  } catch (error) {
    yield put({type: 'FETCH_ABOUT_FAILED', error: error});
  }
}

export function* watchAboutSaga(): Object {
  const pattern = (action) => {
    let isPushRoute = action.type === 'PUSH_ROUTE';
    let isAbout = action.key === 'about';
    return isPushRoute && isAbout;
  };
  yield takeLatest(pattern, fetchAbout);
}
