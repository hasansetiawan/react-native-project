const {describe, it} = global;

import expect from 'expect';
import {put} from 'redux-saga/effects';

import CompanyProfileAPI from '../../API/CompanyProfileAPI';
import {fetchAbout} from '../aboutSaga';

describe('aboutSaga test', () => {
  it('Fetch About Saga Success', () => {
    let gen = fetchAbout();
    let mockAbout = {
      test: '123',
    };
    let {CALL} = gen.next().value;
    expect(CALL.fn).toEqual(CompanyProfileAPI.getAbout);
    expect(CALL.args).toEqual([]);
    expect(gen.next(mockAbout).value).toEqual(put({type: 'FETCH_ABOUT_SUCCESS', about: mockAbout}));

  });
  it('Fetch About Saga Failed', () => {
    let gen = fetchAbout();
    gen.next();
    expect(gen.throw('fail').value).toEqual(put({type: 'FETCH_ABOUT_FAILED', error: 'fail'}));
  });
});
