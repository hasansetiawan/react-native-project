// @flow
/*
const {describe, it} = global;

import {select, put, call} from 'redux-saga/effects';
import expect from 'expect';

import PlantsAPI from '../../API/PlantsAPI';

import formatToCamelCase from '../../helpers/formatToCamelCase';
import formatObjectKeys from '../../helpers/formatObjectKeys';
// import {validatePlants} from '../../server-types/Plant';
import {fetchPlant} from '../plantSaga';
import getToken from '../../selectors/getToken';

let options = {
  isReplaceLiteral: true,
  isAddPrefix: false,
  textReplacement: [{
    targetKey: 'agePlant',
    replaceWith: 'plantAge',
  }, {
    targetKey: 'foto',
    replaceWith: 'photo',
  }, {
    targetKey: 'fotoThumb',
    replaceWith: 'photoThumb',
  }],
};

describe('plant Saga Test', () => {
  let gen = fetchPlant();
  let result = {
    result: 'sukses',
    message: [{
      id: 1,
      photo: '/foto.jpg',
      photoThumb: '/fotoThumb.jpg',
    }],
  };
  let gen2 = fetchPlant();
  let token = '123123';
  let validated = {
    message: [{
      id: 1,
      photo: '/foto.jpg',
      photoThumb: '/fotoThumb.jpg',
    }],
  };
  let formated = {
    id: 1,
    photo: '/foto.jpg',
    photoThumb: '/fotoThumb.jpg',
  };
  let final = {
    id: 1,
    photo: {uri: '/foto.jpg'},
    photoThumb: {uri: '/fotoThumb.jpg'},
  };
  it('Should fetch all plants based on token', () => {
    expect(gen.next().value)
      .toEqual(select(getToken));
    expect(gen.next(token).value)
      .toEqual(call(PlantsAPI.getAllPlant, token));
    // expect(gen.next(result).value)
    //   .toEqual(call(validatePlants, result));
    expect(gen.next(result).value)
      .toEqual(call(formatToCamelCase, validated.message[0]));
    expect(gen.next(result).value)
      .toEqual(call(formatObjectKeys, result, options));
    expect(gen.next(formated).value)
      .toEqual(put({type: 'FETCH_PLANTS_SUCCESS', plants: [final]}));
  });
  it('Should throw when failed', () => {
    let fail = {
      result: 'gagal',
      message: 'failed',
    };
    expect(gen2.next().value)
      .toEqual(select(getToken));
    expect(gen2.next(token).value)
      .toEqual(call(PlantsAPI.getAllPlant, token));
    expect(gen2.next(fail.message).value)
      .toEqual(put({type: 'FETCH_PLANTS_FAILED', error: new Error('failed')}));
  });
});
*/
