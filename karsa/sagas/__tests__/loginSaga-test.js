
const {describe, it} = global;

import expect from 'expect';
import {put, call, take} from 'redux-saga/effects';

import {watchLoginSaga} from '../loginSaga';
import formatToCamelCase from '../../helpers/formatToCamelCase';
import UserAPI from '../../API/UserAPI';


describe('Login Saga Test', () => {
  let userAuth = {
    email: 'petani@test.com',
    password: '12345678',
  };
  let result = {
    result: 'sukses',
    data: {
      id: '123',
      user_name: '123',
    },
  };
  it('Should handle login logic', () => {
    let mockUser = {
      id: 123,
      coins: 100,
      level: 2,
      name: 'asd',
      experience: 1000,
    };
    let gen = watchLoginSaga();
    expect(gen.next({type: 'LOGIN', userAuth: userAuth}).value)
      .toEqual(take('LOGIN'));
    expect(gen.next({userAuth}).value)
      .toEqual(call(UserAPI.postLogin, userAuth));
    expect(gen.next(result).value)
      .toEqual(call(formatToCamelCase, result.data));
    expect(gen.next(mockUser).value)
      .toEqual(put({type: 'LOGIN_SUCCESS', currentUser: mockUser}));
  });
  it('Should throw error', () => {
    let gen2 = watchLoginSaga();
    expect(gen2.next().value)
      .toEqual(take('LOGIN'));
    expect(gen2.next({result: 'gagal', message: 'Failed'}).value)
      .toEqual(call(UserAPI.postLogin, undefined));
    expect(gen2.next({result: 'gagal', message: 'Failed'}).value)
      .toEqual(put({type: 'LOGIN_FAILED', error: new Error('Failed')}));
  });
});
