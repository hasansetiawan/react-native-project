
import {takeLatest} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';
import getToken from '../selectors/getToken';

import UserAPI from '../API/UserAPI';

function* updatePasswordPost(action) {
  try {
    let token = yield select(getToken);
    let {passwordData} = action;
    yield call(UserAPI.updatePassword, token, passwordData);
    yield put({type: 'UPDATE_PASSWORD_POST_SUCCESS'});
  } catch (err) {
    yield put({type: 'UPDATE_PASSWORD_POST_FAILED', error: err});
  }
}

export function* watchUpdatePasswordPostSaga(): any {
  yield takeLatest('UPDATE_PASSWORD_POST', updatePasswordPost);
}
