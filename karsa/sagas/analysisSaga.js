
import {takeLatest} from 'redux-saga';
import {call, put, select} from 'redux-saga/effects';

import CostAPI from '../API/CostAPI';

function getToken(state) {
  return state.currentUser.token;
}

function* fetchCost() {
  try {
    let token = yield select(getToken);
    let analysis = yield call(CostAPI.getCostAnalyticTimeLine, token);
    yield put({type: 'FETCH_ANALYSIS_SUCCESS', analysis: analysis});
  } catch (error) {
    yield put({type: 'FETCH_ANALYSIS_FAILED', error: error});
  }
}

export function* watchAnalysisSaga() {
  const pattern = (action) => {
    let isPushRoute = action.type === 'PUSH_ROUTE';
    let isFetchCost = action.key === 'analysis';
    return (isPushRoute && isFetchCost) || (action.type === 'FETCH_ANALYSIS_REQUESTED');
  };
  yield takeLatest(pattern, fetchCost);
}
