import {takeEvery} from 'redux-saga';
import {call, put, select} from 'redux-saga/effects';
import {getHtmlParsed} from '../selectors/getContentFromHTML';
import formatToCamelCase from '../helpers/formatToCamelCase';
import NotificationAPI from '../API/NotificationsAPI';

//TODO: add pull to refresh implementation

export function* watchNotificationsSaga() {
  yield takeEvery('FETCH_NOTIFICATION_REQUESTED', notificationsSaga);
}

export function* notificationsSaga({page}) {
  try {
    let token = yield select((state) => state.currentUser.token);
    let res = yield call(NotificationAPI.getNotifications, token, page);
    if (res.result === 'sukses') {
      let notifArray = [];
      for (let notification of res.data) {
        let camelCased = yield call(formatToCamelCase, notification);
        let htmlParsed = yield call(getHtmlParsed, notification.message);
        camelCased.message = htmlParsed.overview.text;
        notifArray.push(camelCased);
      }
      let notifications = new Map();
      notifArray.forEach((notif) => {
        notifications.set(notif.id, notif);
      });
      yield put({type: 'FETCH_NOTIFICATION_SUCCEED', notifications, page});
    } else {
      throw new Error(res.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_NOTIFICATION_FAILED'});
  }
}
