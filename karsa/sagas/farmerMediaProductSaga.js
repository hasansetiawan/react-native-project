import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import FarmerMediaAPI from '../API/FarmerMediaAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';

let {fetchProductHighlight, fetchPopularProduct, fetchNewProduct} = FarmerMediaAPI;

export function* watchFarmerMediaProductSaga(): any {
  yield takeLatest(({type, key}) => {
    let isKeyMatch = (
      key === 'SeedProducts' ||
      key === 'FertilizerProducts' ||
      key === 'PesticideProducts' ||
      key === 'ToolsProducts'
    );
    return type === 'PUSH_ROUTE' && isKeyMatch;
  }, productSaga);
}
function* productSaga({key}): any {
  try {
    yield* processProductSaga(key);
  } catch (err) {
    yield put({type: 'Fetching farmer media product saga failed:', message: err});
  }
}

function* processProductSaga(currentRouteKey) {
  let commodity = getCommodityName(currentRouteKey);
  let [productHighlight, popularProduct, newProduct] = yield [
    call(fetchProductHighlight, commodity.API_NAME),
    call(fetchPopularProduct, commodity.API_NAME),
    call(fetchNewProduct, commodity.API_NAME),
  ];
  let payload = yield call(
    constructPayload,
    productHighlight.message,
    popularProduct.message,
    newProduct.message
  );
  yield put({
    type: `FETCH_${commodity.NAME}_SUCCESSFUL`,
    payload,
  });
}

async function constructPayload(highlightProducts, popularProducts, newProducts) {
  let allProducts = [...highlightProducts, ...popularProducts, ...newProducts];
  allProducts = allProducts.map((product) => {
    let reformatProductKeys = formatObjectKeys(product, reformatSpecification);
    let productCamelCased = formatToCamelCase(reformatProductKeys);
    let photoToImageSource = {
      ...productCamelCased,
      photo: {uri: productCamelCased.photo},
    };
    return photoToImageSource;
  });
  let allId = allProducts.map((product) => product.id);
  let byHighlight = highlightProducts.map((product) => product.product_id);
  let byPopularity = popularProducts.map((product) => product.product_id);
  let byLatest = newProducts.map((product) => product.product_id);

  return {
    allProducts,
    allId,
    byHighlight,
    byPopularity,
    byLatest,
  };
}

function getCommodityName(currentRouteKey) {
  let commodityNames = {
    SeedProducts: {
      NAME: 'SEED',
      API_NAME: 'bibit',
    },
    FertilizerProducts: {
      NAME: 'FERTILIZER',
      API_NAME: 'pupuk',
    },
    PesticideProducts: {
      NAME: 'PESTICIDE',
      API_NAME: 'pestisida',
    },
    ToolsProducts: {
      NAME: 'TOOLS',
      API_NAME: 'alat-alat-pertanian',
    },
  };
  return commodityNames[currentRouteKey];
}

let reformatSpecification = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'product_id',
    replaceWith: 'id',
  }, {
    targetKey: 'product_name',
    replaceWith: 'name',
  }, {
    targetKey: 'desc',
    replaceWith: 'description',
  }, {
    targetKey: 'user_id',
    replaceWith: 'producerID',
  }, {
    targetKey: 'produsen_name',
    replaceWith: 'producerName',
  }, {
    targetKey: 'produsen_address',
    replaceWith: 'producerAddress',
  }, {
    targetKey: 'province',
    replaceWith: 'producerProvince',
  }, {
    targetKey: 'city',
    replaceWith: 'producerCity',
  }, {
    targetKey: 'count_comment',
    replaceWith: 'reviewCount',
  }, {
    targetKey: 'product_photo',
    replaceWith: 'photo',
  }],
};
