import {ToastAndroid} from 'react-native';
import {takeLatest, takeEvery} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';
import {threadOptions, userLikeOptions} from './threadSaga';

import getToken from '../selectors/getToken';
import ThreadAPI from '../API/ThreadAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';

import defaultAvatar from '../images/default-avatar.png';
import defaultImage from '../images/header.jpg'; // TODO: get the image without karsa logo & letters
import defaultThumbImage from '../images/no_image.png';

const formatGroupKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'userId',
    replaceWith: 'userID',
  }, {
    targetKey: 'avatarFb',
    replaceWith: 'avatarFacebook',
  }, {
    targetKey: 'avatarOwner',
    replaceWith: 'avatar',
  }, {
    targetKey: 'avatarFbOwner',
    replaceWith: 'avatarFacebook',
  }, {
    targetKey: 'name',
    replaceWith: 'groupName',
  }, {
    targetKey: 'desc',
    replaceWith: 'groupDescription',
  }, {
    targetKey: 'photo',
    replaceWith: 'groupImage',
  }, {
    targetKey: 'photoThumb',
    replaceWith: 'groupImageThumbnail',
  }, {
    targetKey: 'totalMember',
    replaceWith: 'groupMemberCount',
  }, {
    targetKey: 'totalThread',
    replaceWith: 'groupTopicCount',
  }, {
    targetKey: 'province',
    replaceWith: 'provinceID',
  }, {
    targetKey: 'city',
    replaceWith: 'cityID',
  }, {
    targetKey: 'subdistrict',
    replaceWith: 'subdistrictID',
  }, {
    targetKey: 'village',
    replaceWith: 'villageID',
  }, {
    targetKey: 'statusReq',
    replaceWith: 'statusRequest',
  }],
};

export function* watchExpertForumSaga() {
  yield [
    takeLatest('FETCH_USER_GROUP_LIST_REQUESTED', fetchUserGroupList),
    takeLatest('FETCH_GROUP_LIST_REQUESTED', fetchGroupList),
    takeLatest('FETCH_GROUP_INVITATION_LIST_REQUESTED', fetchGroupInvitationList),
    takeLatest('FETCH_SEARCHED_GROUPS_REQUESTED', fetchSearchedGroups),
    takeEvery('JOIN_GROUP_REQUESTED', joinGroup),
    takeEvery('CONFIRM_GROUP_INVITATION_REQUESTED', confirmGroupInvitation),
    takeEvery('FETCH_GROUP_DETAIL_REQUESTED', fetchGroupDetail),
    takeEvery('FETCH_GROUP_MEMBERS_REQUESTED', fetchGroupMembers),
    takeEvery('FETCH_GROUP_THREADS_REQUESTED', fetchGroupThreads),
  ];
}

export function* fetchUserGroupList() {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getUserGroupList, token);
    let groups = new Map();
    for (let group of result) {
      let camelCased = yield call(formatToCamelCase, group);
      let formatted = yield call(formatObjectKeys, camelCased, formatGroupKeys);
      formatted.groupCategory = 'joined';
      formatted.statusReq = Boolean(formatted.statusReq);
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      if (!formatted.groupImage && !formatted.groupImageThumbnail) {
        formatted.groupImage = defaultImage;
        formatted.groupImageThumbnail = defaultThumbImage;
      } else {
        formatted.groupImage = formatted.groupImage ? {uri: formatted.groupImage} : null;
        formatted.groupImageThumbnail = formatted.groupImageThumbnail ? {uri: formatted.groupImageThumbnail} : null;
      }
      groups.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_USER_GROUP_LIST_SUCCESS', groups});
  } catch (error) {
    yield put({type: 'FETCH_USER_GROUP_LIST_FAILED', error});
  }
}

export function* fetchGroupList() {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getGroupList, token);
    let groups = new Map();
    for (let group of result) {
      let camelCased = yield call(formatToCamelCase, group);
      let formatted = yield call(formatObjectKeys, camelCased, formatGroupKeys);
      formatted.groupCategory = 'not-joined';
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      if (!formatted.groupImage && !formatted.groupImageThumbnail) {
        formatted.groupImage = defaultImage;
        formatted.groupImageThumbnail = defaultThumbImage;
      } else {
        formatted.groupImage = formatted.groupImage ? {uri: formatted.groupImage} : null;
        formatted.groupImageThumbnail = formatted.groupImageThumbnail ? {uri: formatted.groupImageThumbnail} : null;
      }
      groups.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_GROUP_LIST_SUCCESS', groups});
  } catch (error) {
    yield put({type: 'FETCH_GROUP_LIST_FAILED', error});
  }
}

export function* fetchGroupInvitationList() {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getInvitationGroupList, token);
    let groups = new Map();
    for (let group of result) {
      let camelCased = yield call(formatToCamelCase, group);
      let formatted = yield call(formatObjectKeys, camelCased, formatGroupKeys);
      formatted.groupCategory = 'invite';
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      if (!formatted.groupImage && !formatted.groupImageThumbnail) {
        formatted.groupImage = defaultImage;
        formatted.groupImageThumbnail = defaultThumbImage;
      } else {
        formatted.groupImage = formatted.groupImage ? {uri: formatted.groupImage} : null;
        formatted.groupImageThumbnail = formatted.groupImageThumbnail ? {uri: formatted.groupImageThumbnail} : null;
      }
      groups.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_GROUP_INVITATION_LIST_SUCCESS', groups});
  } catch (error) {
    yield put({type: 'FETCH_GROUP_INVITATION_LIST_FAILED', error});
  }
}

export function* fetchSearchedGroups(action) {
  try {
    let token = yield select(getToken);
    let {query, groupCategory} = action;
    let result = yield call(ThreadAPI.searchGroups, token, query, groupCategory);
    let groups = new Map();
    for (let group of result) {
      let camelCased = yield call(formatToCamelCase, group);
      let formatted = yield call(formatObjectKeys, camelCased, formatGroupKeys);
      formatted.groupCategory = groupCategory;
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      if (!formatted.groupImage && !formatted.groupImageThumbnail) {
        formatted.groupImage = defaultImage;
        formatted.groupImageThumbnail = defaultThumbImage;
      } else {
        formatted.groupImage = formatted.groupImage ? {uri: formatted.groupImage} : null;
        formatted.groupImageThumbnail = formatted.groupImageThumbnail ? {uri: formatted.groupImageThumbnail} : null;
      }
      groups.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_SEARCHED_GROUPS_SUCCESS', groups});
  } catch (error) {
    yield put({type: 'FETCH_SEARCHED_GROUPS_FAILED', error});
  }
}

export function* joinGroup(action) {
  try {
    let token = yield select(getToken);
    let {groupID} = action;
    let result = yield call(ThreadAPI.requestJoinGroup, token, groupID);
    ToastAndroid.show(result, ToastAndroid.SHORT);
    yield put({type: 'JOIN_GROUP_SUCCESS', groupID});
  } catch (error) {
    if (error.message.endsWith('sebelumnya')) {
      ToastAndroid.show(error, ToastAndroid.SHORT);
    }
    yield put({type: 'JOIN_GROUP_FAILED', error});
  }
}

export function* confirmGroupInvitation(action) {
  try {
    let token = yield select(getToken);
    let {groupID} = action;
    let result = yield call(ThreadAPI.confirmInvite, token, groupID);
    ToastAndroid.show(result, ToastAndroid.SHORT);
    yield put({type: 'CONFIRM_GROUP_INVITATION_SUCCESS', groupID});
  } catch (error) {
    ToastAndroid.show(error, ToastAndroid.SHORT);
    yield put({type: 'CONFIRM_GROUP_INVITATION_FAILED', error});
  }
}

export function* fetchGroupDetail(action) {
  try {
    let token = yield select(getToken);
    let {groupID} = action;
    let result = yield call(ThreadAPI.getGroupDetail, token, groupID);
    let camelCased = yield call(formatToCamelCase, result);
    let formatted = yield call(formatObjectKeys, camelCased, formatGroupKeys);
    if (!formatted.avatar && !formatted.avatarFacebook) {
      formatted.avatar = defaultAvatar;
    } else {
      formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
      formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
    }
    if (!formatted.groupImage && !formatted.groupImageThumbnail) {
      formatted.groupImage = defaultImage;
      formatted.groupImageThumbnail = defaultThumbImage;
    } else {
      formatted.groupImage = formatted.groupImage ? {uri: formatted.groupImage} : null;
      formatted.groupImageThumbnail = formatted.groupImageThumbnail ? {uri: formatted.groupImageThumbnail} : null;
    }
    yield put({type: 'FETCH_GROUP_DETAIL_SUCCESS', group: formatted});
  } catch (error) {
    yield put({type: 'FETCH_GROUP_DETAIL_FAILED', error});
  }
}

export function* fetchGroupMembers(action) {
  const options = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'avatarFb',
      replaceWith: 'avatarFacebook',
    }],
  };
  try {
    let token = yield select(getToken);
    let {groupID, page} = action;
    let result = yield call(ThreadAPI.getGroupMembers, token, groupID, page);
    let members = new Map();
    for (let member of result) {
      let camelCased = yield call(formatToCamelCase, member);
      let formatted = yield call(formatObjectKeys, camelCased, options);
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      members.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_GROUP_MEMBERS_SUCCESS', groupID, members});
  } catch (error) {
    yield put({type: 'FETCH_GROUP_MEMBERS_FAILED', error});
  }
}

export function* fetchGroupThreads(action) {
  try {
    let token = yield select(getToken);
    let {groupID, page} = action;
    let result = yield call(ThreadAPI.getGroupThreads, token, groupID, page);
    let threads = new Map();
    for (let thread of result) {
      let camelCased = yield call(formatToCamelCase, thread);
      let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
      formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
      formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
      let userLikes = [];
      for (let userLike of formatted.userLikes) {
        let camelCasedUserLike = yield call(formatToCamelCase, userLike);
        let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
        userLikes.push(formattedUserLike);
      }
      formatted.userLikes = userLikes;
      if (!formatted.avatar && !formatted.avatarFacebook) {
        formatted.avatar = defaultAvatar;
      } else {
        formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
        formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
      }
      threads.set(formatted.id, formatted);
    }
    yield put({type: 'FETCH_GROUP_THREADS_SUCCESS', groupID, threads});
  } catch (error) {
    yield put({type: 'FETCH_GROUP_THREADS_FAILED', error});
  }
}
