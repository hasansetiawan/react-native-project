// @flow

import {ToastAndroid} from 'react-native';
import {takeLatest, takeEvery} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';

import getToken from '../selectors/getToken';
import ThreadAPI from '../API/ThreadAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';

import defaultAvatar from '../images/default-avatar.png';

let userLikeOptions = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'userId',
    replaceWith: 'userID',
  }, {
    targetKey: 'commentId',
    replaceWith: 'commentID',
  }, {
    targetKey: 'threadId',
    replaceWith: 'threadID',
  }, {
    targetKey: 'avatarFb',
    replaceWith: 'avatarFacebook',
  }],
};

let threadOptions = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'userId',
    replaceWith: 'userID',
  }, {
    targetKey: 'desc',
    replaceWith: 'description',
  }, {
    targetKey: 'foto',
    replaceWith: 'photo',
  }, {
    targetKey: 'fotoShare',
    replaceWith: 'photoShare',
  }, {
    targetKey: 'avatarFb',
    replaceWith: 'avatarFacebook',
  }, {
    targetKey: 'countComment',
    replaceWith: 'commentCount',
  }, {
    targetKey: 'like',
    replaceWith: 'likeCount',
  }, {
    targetKey: 'thread',
    replaceWith: 'title',
  }],
};

let commentOptions = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'userId',
    replaceWith: 'userID',
  }, {
    targetKey: 'avatarFb',
    replaceWith: 'avatarFacebook',
  }, {
    targetKey: 'forumId',
    replaceWith: 'forumID',
  }, {
    targetKey: 'like',
    replaceWith: 'likeCount',
  }],
};

export {threadOptions, userLikeOptions, commentOptions};

export function* watchThreadSaga(): any {
  yield [
    takeLatest('FETCH_USER_THREADS_REQUESTED', fetchUserThreads),
    takeLatest('FETCH_THREAD_DETAIL_REQUESTED', fetchThreadDetail),
    takeEvery('FETCH_COMMENTS_REQUESTED', fetchComments),
    takeLatest('LIKE_THREAD_REQUESTED', likeThread),
    takeLatest('LIKE_COMMENT_REQUESTED', likeComment),
    takeLatest('POST_COMMENT_REQUESTED', postComment),
    takeLatest('DELETE_THREAD_REQUESTED', deleteThread),
    takeLatest('EDIT_THREAD_REQUESTED', editThread),
    takeLatest('FETCH_THREAD_CATEGORIES_FILTER_REQUESTED', fetchFilterThreadCategories),
    takeLatest('FETCH_FILTERED_THREADS_REQUESTED', fetchFilteredThreads),
    takeLatest('FETCH_SEARCHED_THREADS_REQUESTED', fetchSearchedThreads),
    takeLatest('FETCH_OTHER_THREADS_REQUESTED', fetchOtherThreads),
    takeLatest('REPORT_FORUM_ITEM_REQUESTED', reportForumItem),
  ];
}

type FetchThreadsAction = {
  type: 'FETCH_THREADS_REQUESTED';
  page: number;
  forum?: string;
};

export function* fetchUserThreads(action: FetchThreadsAction): any {
  try {
    let token = yield select(getToken);
    let {page} = action;
    let result = yield call(ThreadAPI.getUserThreads, token, page);
    if (result) { //TODO: remove if (result) when flow-typed is in place
      let threads = new Map();
      for (let thread of result) {
        let camelCased = yield call(formatToCamelCase, thread);
        let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
        if (formatted) {
          formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
          formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
          let userLikes = [];
          for (let userLike of formatted.userLikes) {
            let camelCasedUserLike = yield call(formatToCamelCase, userLike);
            let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
            userLikes.push(formattedUserLike);
          }
          formatted.userLikes = userLikes;
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          threads.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_USER_THREADS_SUCCESS', threads, page});
    }
  } catch (error) {
    yield put({type: 'FETCH_USER_THREADS_FAILED', error});
  }
}

type FetchThreadDetailAction = {
  type: 'FETCH_THREAD_DETAIL_REQUESTED';
  threadID: number;
};

export function* fetchThreadDetail(action: FetchThreadDetailAction): any {
  try {
    let token = yield select(getToken);
    let {threadID} = action;
    let result = yield call(ThreadAPI.getThreadDetail, token, threadID);
    if (result) {
      let camelCased = yield call(formatToCamelCase, result);
      let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
      if (formatted) {
        formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
        formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
        let userLikes = [];
        for (let userLike of formatted.userLikes) {
          let camelCasedUserLike = yield call(formatToCamelCase, userLike);
          let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
          userLikes.push(formattedUserLike);
        }
        formatted.userLikes = userLikes;
        if (!formatted.avatar && !formatted.avatarFacebook) {
          formatted.avatar = defaultAvatar;
        } else {
          formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
          formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
        }
        let comments = new Map();
        for (let comment of formatted.comments) {
          let camelCasedComment = yield call(formatToCamelCase, comment);
          let formattedComment = yield call(formatObjectKeys, camelCasedComment, commentOptions);
          let commentLikes = [];
          if (formattedComment) {
            if (!formattedComment.avatar && !formattedComment.avatarFacebook) {
              formattedComment.avatar = defaultAvatar;
            } else {
              formattedComment.avatar = formattedComment.avatar ? {uri: formattedComment.avatar} : null;
              formattedComment.avatarFacebook = formattedComment.avatarFacebook ? {uri: formattedComment.avatarFacebook} : null;
            }
            for (let commentLike of formattedComment.userLikes) {
              let camelCasedCommentLike = yield call(formatToCamelCase, commentLike);
              let formattedCommentLike = yield call(formatObjectKeys, camelCasedCommentLike, userLikeOptions);
              commentLikes.push(formattedCommentLike);
            }
            formattedComment.userLikes = commentLikes;
            comments.set(formattedComment.id, formattedComment);
          }
        }
        formatted.comments = new Map(comments);
      }
      yield put({type: 'FETCH_THREAD_DETAIL_SUCCESS', thread: formatted, threadID});
    }
  } catch (error) {
    yield put({type: 'FETCH_THREAD_DETAIL_FAILED', error});
  }
}

type FetchCommentsAction = {
  type: 'FETCH_COMMENTS_REQUESTED';
  threadID: number;
  page: number;
};

export function* fetchComments(action: FetchCommentsAction): any {
  try {
    let token = yield select(getToken);
    let {threadID, page} = action;
    let result = yield call(ThreadAPI.threadComments, token, threadID, page);
    if (result) {
      let comments = new Map();
      for (let comment of result) {
        let camelCased = yield call(formatToCamelCase, comment);
        let formatted = yield call(formatObjectKeys, camelCased, commentOptions);
        if (formatted) {
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          if (formatted.userLikes.length > 0) {
            let userLikes = [];
            for (let like of formatted.userLikes) {
              let camelCasedUserLike = yield call(formatToCamelCase, like);
              let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
              userLikes.push(formattedUserLike);
            }
            formatted.userLikes = userLikes;
          }
          comments.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_COMMENTS_SUCCESS', threadID, comments});
    }
  } catch (error) {
    yield put({type: 'FETCH_COMMENTS_FAILED', error});
  }
}

type LikeThreadAction = {
  type: 'LIKE_THREAD_REQUESTED';
  threadID: number;
};

export function* likeThread(action: LikeThreadAction): any {
  try {
    let token = yield select(getToken);
    let {threadID} = action;
    let item = {
      type: 'thread',
      id: threadID,
    };
    let result = yield call(ThreadAPI.likeForumItem, token, item);
    if (result) {
      let userLikes = [];
      for (let item of result.user_likes) {
        let camelCased = yield call(formatToCamelCase, item);
        let formatted = yield call(formatObjectKeys, camelCased, userLikeOptions);
        if (formatted) {
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
        }
        userLikes.push(formatted);
      }
      yield put({type: 'LIKE_THREAD_SUCCESS', threadID, userLikes});
    }
  } catch (err) {
    yield put({type: 'LIKE_THREAD_FAILED', error: err});
  }
}

type LikeCommentAction = {
  type: 'LIKE_COMMENT_REQUESTED';
  commentID: number;
};

export function* likeComment(action: LikeCommentAction): any {
  try {
    let token = yield select(getToken);
    let threadID = yield select((state) => state.selectedThreadID);
    let {commentID} = action;
    let item = {
      type: 'comment',
      id: commentID,
    };
    let result = yield call(ThreadAPI.likeForumItem, token, item);
    if (result) {
      let userLikes = [];
      for (let item of result.user_likes) {
        let camelCased = yield call(formatToCamelCase, item);
        let formatted = yield call(formatObjectKeys, camelCased, userLikeOptions);
        if (formatted) {
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
        }
        userLikes.push(formatted);
      }
      yield put({type: 'LIKE_COMMENT_SUCCESS', threadID, commentID, userLikes});
    }
  } catch (error) {
    yield put({type: 'LIKE_COMMENT_FAILED', error});
  }
}

type PostCommentAction = {
  type: 'POST_COMMENT_REQUESTED';
  threadID: number;
  post: string;
};

export function* postComment(action: PostCommentAction): any {
  try {
    let token = yield select(getToken);
    let {threadID, post} = action;
    let result = yield call(ThreadAPI.postComment, token, {post, thread_id: threadID});
    if (result) {
      let comments = new Map();
      for (let comment of result.comments) {
        let camelCased = yield call(formatToCamelCase, comment);
        let formatted = yield call(formatObjectKeys, camelCased, commentOptions);
        if (formatted) {
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          if (formatted.userLikes.length > 0) {
            let userLikes = [];
            for (let like of formatted.userLikes) {
              let camelCasedUserLike = yield call(formatToCamelCase, like);
              let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
              userLikes.push(formattedUserLike);
            }
            formatted.userLikes = userLikes;
          }
          comments.set(formatted.id, formatted);
        }
      }
      yield put({type: 'POST_COMMENT_SUCCESS', threadID, comments});
    }
  } catch (error) {
    yield put({type: 'POST_COMMENT_FAILED', error});
  }
}

type DeleteThreadAction = {
  type: 'DELETE_THREAD_REQUESTED';
  threadID: number;
  forum: 'user' | 'other';
};

export function* deleteThread(action: DeleteThreadAction): any {
  try {
    let token = yield select(getToken);
    let {threadID, forum} = action;
    let result = yield call(ThreadAPI.deleteThread, token, threadID);
    if (result) {
      yield put({type: 'POP_ROUTE'});
      yield put({type: 'DELETE_THREAD_SUCCESS'});
      ToastAndroid.show('Topik berhasil dihapus', ToastAndroid.SHORT);
      if (forum === 'user') {
        yield put({type: 'FETCH_USER_THREADS_REQUESTED', page: 1});
      } else {
        yield put({type: 'FETCH_OTHER_THREADS_REQUESTED', forum: 'petani', page: 1});
      }
    }
  } catch (error) {
    ToastAndroid.show('Topik gagal dihapus', ToastAndroid.SHORT);
    yield put({type: 'DELETE_THREAD_FAILED', error});
  }
}

type EditThreadAction = {
  type: 'EDIT_THREAD_REQUESTED';
  threadID: number;
  categoryID: number;
  subCategoryID: number;
  title: string;
  description: string;
  photo: ?ImageData;
};

export function* editThread(action: EditThreadAction): any {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.updateThread, token, action);
    if (result) {
      yield put({type: 'EDIT_THREAD_SUCCESS'});
      ToastAndroid.show('Topik berhasil diubah', ToastAndroid.SHORT);
      yield put({type: 'FETCH_THREAD_DETAIL_REQUESTED', threadID: action.threadID});
      yield put({type: 'POP_ROUTE'});
    }
  } catch (error) {
    ToastAndroid.show('Topik gagal diubah', ToastAndroid.SHORT);
    yield put({type: 'EDIT_THREAD_FAILED', error});
  }
}

type FetchFilterThreadCategoriesAction = {
  type: 'FETCH_THREAD_CATEGORIES_FILTER_REQUESTED';
  slug: string;
};

export function* fetchFilterThreadCategories(action: FetchFilterThreadCategoriesAction): any {
  try {
    let {slug} = action;
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getFilterThreadCategories, token, slug);
    if (result) {
      let threadCategories = new Map();
      for (let category of result) {
        let camelCased = yield call(formatToCamelCase, category);
        if (camelCased) {
          threadCategories.set(camelCased.id, camelCased);
        }
      }
      yield put({type: 'FETCH_THREAD_CATEGORIES_FILTER_SUCCESS', threadCategories});
    }
  } catch (error) {
    yield put({type: 'FETCH_THREAD_CATEGORIES_FILTER_FAILED', error});
  }
}

type FetchFilteredThreadsAction = {
  type: 'FETCH_FILTERED_THREADS_REQUESTED';
  forumCategory: string;
  threadCategory: string;
  sortBy: 'all' | 'latest' | 'popular';
  page: number;
};

export function* fetchFilteredThreads(action: FetchFilteredThreadsAction): any {
  try {
    let {forumCategory, threadCategory, sortBy, page} = action;
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.filterThreads, token, forumCategory, threadCategory, sortBy, page);
    if (result) {
      let threads = new Map();
      for (let thread of result) {
        let camelCased = yield call(formatToCamelCase, thread);
        let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
        if (formatted) {
          formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
          formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
          let userLikes = [];
          for (let userLike of formatted.userLikes) {
            let camelCasedUserLike = yield call(formatToCamelCase, userLike);
            let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
            userLikes.push(formattedUserLike);
          }
          formatted.userLikes = userLikes;
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          threads.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_FILTERED_THREADS_SUCCESS', threads});
    }
  } catch (error) {
    yield put({type: 'FETCH_FILTERED_THREADS_FAILED', error});
  }
}

type FetchSearchedThreadsAction = {
  type: 'FETCH_SEARCHED_THREADS_REQUESTED';
  forumCategory: string;
  query: string;
  page: number;
};

export function* fetchSearchedThreads(action: FetchSearchedThreadsAction): any {
  try {
    let {forumCategory, query, page} = action;
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.searchThreads, token, forumCategory, query, page);
    if (result) {
      let threads = new Map();
      for (let thread of result) {
        let camelCased = yield call(formatToCamelCase, thread);
        let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
        if (formatted) {
          formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
          formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
          let userLikes = [];
          for (let userLike of formatted.userLikes) {
            let camelCasedUserLike = yield call(formatToCamelCase, userLike);
            let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
            userLikes.push(formattedUserLike);
          }
          formatted.userLikes = userLikes;
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          threads.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_SEARCHED_THREADS_SUCCESS', threads});
    }
  } catch (error) {
    yield put({type: 'FETCH_SEARCHED_THREADS_FAILED', error});
  }
}

type FetchOtherThreadsAction = {
  type: 'FETCH_OTHER_THREADS_REQUESTED';
  page: number;
  forum: string;
};

export function* fetchOtherThreads(action: FetchOtherThreadsAction): any {
  try {
    let {page, forum} = action;
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getOtherThreads, token, page, forum);
    if (result) {
      let threads = new Map();
      for (let thread of result) {
        let camelCased = yield call(formatToCamelCase, thread);
        let formatted = yield call(formatObjectKeys, camelCased, threadOptions);
        if (formatted) {
          formatted.photo = formatted.photo ? {uri: formatted.photo} : null;
          formatted.photoShare = formatted.photoShare ? {uri: formatted.photoShare} : null;
          let userLikes = [];
          for (let userLike of formatted.userLikes) {
            let camelCasedUserLike = yield call(formatToCamelCase, userLike);
            let formattedUserLike = yield call(formatObjectKeys, camelCasedUserLike, userLikeOptions);
            userLikes.push(formattedUserLike);
          }
          formatted.userLikes = userLikes;
          if (!formatted.avatar && !formatted.avatarFacebook) {
            formatted.avatar = defaultAvatar;
          } else {
            formatted.avatar = formatted.avatar ? {uri: formatted.avatar} : null;
            formatted.avatarFacebook = formatted.avatarFacebook ? {uri: formatted.avatarFacebook} : null;
          }
          threads.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_OTHER_THREADS_SUCCESS', threads, forum});
    }
  } catch (error) {
    yield put({type: 'FETCH_OTHER_THREADS_FAILED', error});
  }
}

type ReportForumItemAction = {
  type: 'REPORT_FORUM_ITEM_REQUESTED';
  report: string;
  threadID: number;
  commentID?: number;
};

export function* reportForumItem(action: ReportForumItemAction): any {
  try {
    let {report, threadID, commentID} = action;
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.reportForumItem, token, report, threadID, commentID);
    if (result) {
      ToastAndroid.show(result, ToastAndroid.SHORT);
      yield put({type: 'REPORT_FORUM_ITEM_SUCCESS'});
    }
  } catch (error) {
    yield put({type: 'REPORT_FORUM_ITEM_FAILED'});
  }
}
