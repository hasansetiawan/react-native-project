import {takeLatest} from 'redux-saga';
import {call, put, select, fork} from 'redux-saga/effects';

import UserAPI from '../API/UserAPI';
import RegionAPI from '../API/RegionAPI';

import {
  UserAction,
  getTempProfile,
  getUserAddress,
  formatData,
  getAddressFromState,
  copyState,
} from '../helpers/editProfileSagaHelpers';

export function* watchEditProfile(): any {
  yield takeLatest(UserAction.openEditProfilePage, fetchAllAddressData);
  yield takeLatest(UserAction.changeAddress, changeUserAddress);
  yield takeLatest(UserAction.submitNewProfile, postEditProfile);
  yield takeLatest([UserAction.logIn, UserAction.cancelEditProfile], rehydrateTempProfile);
}

function* rehydrateTempProfile(action) {
  let isUserLoggingIn = action.type === UserAction.logIn;
  let isUserCancelEditing = action.type === UserAction.cancelEditProfile;

  if (isUserLoggingIn) {
    let duplicateCurrentUser = copyState('currentUser', 'tempProfile')(action.currentUser);
    yield put({type: 'INITIALIZE_TEMP_REDUCER', currentUser: duplicateCurrentUser});
  } else if (isUserCancelEditing) {
    let currentUser = yield select((state) => {
      return copyState('currentUser', 'tempProfile')(state.currentUser);
    });
    yield put({type: 'RESET_TEMP_REDUCER', currentUser});
    yield put({type: 'RESET_ROUTE', key: 'settings'});
  }
}

export function* postEditProfile() {
  try {
    yield* processPostEditProfile();
  } catch (error) {
    yield put({type: 'FETCH_EDIT_PROFILE_FAILED', error});
  }
}

function* processPostEditProfile() {
  let token = yield select((state) => state.currentUser.token);
  let {profileToServer, profileToReducer} = yield select(getTempProfile);
  let response = yield call(UserAPI.editUserProfile, token, profileToServer);
  let {result, error} = response;
  if (result) {
    let tempProfile = copyState('tempProfile', 'currentUser')(profileToReducer);
    yield put({type: 'EDIT_PROFILE_SUBMITTED', tempProfile});
    yield put({type: 'RESET_ROUTE', key: 'settings'});
  } else {
    throw new Error(error);
  }
}

function* changeUserAddress(action) {
  let {subAddress, value} = action;
  let subAddressItem = yield getAddressFromState(subAddress, value);
  yield put({
    type: 'UPDATE_CURRENTLY_SELECTED_ADDRESS',
    currentSelectedSubAddress: subAddress,
    subAddressData: subAddressItem,
  });
  yield fork(fetchAllAddressData);
}

export function* fetchAllAddressData() {
  try {
    yield* processFetchAddress();
  } catch (error) {
    yield put({type: 'FETCH_ADDRESS_FAILED', error});
  }
}

//TODO only fetch address once!
//TODO only fetch cities when province changed.
function* processFetchAddress() {
  let {provinceID} = yield select(getUserAddress);
  let res = yield [call(RegionAPI.getProvince), call(RegionAPI.getCities, provinceID)];
  let allProvinceData;
  let allCitiesData;
  try {
    if (res[0].result === 'sukses') {
      allProvinceData = formatData(res[0].message, 'province');
    }
    if (res[1].result === 'sukses') {
      allCitiesData = formatData(res[1].message, 'city');
    }
    let payload = {
      allProvinceData,
      allCitiesData,
    };
    yield put({type: 'FETCH_ADDRESS_SUCCEED', payload});
  } catch (error) {
    throw error;
  }
}
