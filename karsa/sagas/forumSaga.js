// @flow

import {takeLatest} from 'redux-saga';
import {put, call} from 'redux-saga/effects';

import ThreadAPI from '../API/ThreadAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';

const forumFormatOptions = {
  isReplaceLiteral: true,
  isAddPrefix: false,
  textReplacement: [{
    targetKey: 'desc',
    replaceWith: 'description',
  }],
};

export function* watchForumSaga(): any {
  yield [
    takeLatest('FETCH_FORUM_CATEGORIES_REQUESTED', fetchForumCategories),
  ];
}

export function* fetchForumCategories(): any {
  try {
    let result = yield call(ThreadAPI.getAllForum);
    if (result) {
      let forumCategories = new Map();
      for (let item of result) {
        let camelCased = yield call(formatToCamelCase, item);
        let formatted = yield call(formatObjectKeys, camelCased, forumFormatOptions);
        if (formatted) {
          forumCategories.set(formatted.id, formatted);
        }
      }
      yield put({type: 'FETCH_FORUM_CATEGORIES_SUCCESS', forumCategories});
    }
  } catch (error) {
    yield put({type: 'FETCH_FORUM_CATEGORIES_FAILED', error});
  }
}
