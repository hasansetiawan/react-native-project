import ThreadAPI from '../API/ThreadAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';
import getToken from '../selectors/getToken';

import {ToastAndroid} from 'react-native';
import {takeLatest} from 'redux-saga';
import {select, put, call} from 'redux-saga/effects';

export function* watchNewThreadSaga(): any {
  yield [
    takeLatest('FETCH_THREAD_CATEGORIES_REQUESTED', fetchForumCategories),
    takeLatest('FETCH_THREAD_SUB_CATEGORIES_REQUESTED', fetchForumSubCategories),
    takeLatest('CREATE_THREAD_REQUESTED', createNewThread),
  ];
}

export function* fetchForumCategories() {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.getForumCategories, token);
    if (result) {
      let categories = new Map();
      for (let cat of result) {
        let camelCased = yield call(formatToCamelCase, cat);
        categories.set(camelCased.id, camelCased);
      }
      yield put({type: 'FETCH_THREAD_CATEGORIES_SUCCESS', categories});
    }
  } catch (error) {
    yield put({type: 'FETCH_THREAD_CATEGORIES_FAILED', error});
  }
}

type FetchForumSubCategoriesAction = {
  type: 'FETCH_THREAD_SUB_CATEGORIES_REQUESTED';
  categoryID: number;
};

export function* fetchForumSubCategories(action: FetchForumSubCategoriesAction) {
  let subCategoryKeyOptions = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [{
      targetKey: 'categoryId',
      replaceWith: 'categoryID',
    }],
  };
  try {
    let token = yield select(getToken);
    let {categoryID} = action;
    let result = yield call(ThreadAPI.getForumSubCategories, token, categoryID);
    if (result) {
      let subCategories = new Map();
      for (let subCat of result) {
        let camelCased = yield call(formatToCamelCase, subCat);
        let formatted = yield call(formatObjectKeys, camelCased, subCategoryKeyOptions);
        subCategories.set(formatted.id, formatted);
      }
      yield put({type: 'FETCH_THREAD_SUB_CATEGORIES_SUCCESS', subCategories});
    }
  } catch (error) {
    yield put({type: 'FETCH_THREAD_SUB_CATEGORIES_FAILED', error});
  }
}

type CreateNewThreadAction = {
  type: 'CREATE_THREAD_REQUESTED';
  categoryID: number;
  subCategoryID: number;
  title: string;
  description: string;
  photo: ?ImageData;
};

export function* createNewThread(action: CreateNewThreadAction) {
  try {
    let token = yield select(getToken);
    let result = yield call(ThreadAPI.postThread, token, action);
    if (result) {
      yield put({type: 'CREATE_THREAD_SUCCESS'});
      ToastAndroid.show('Berhasil membuat topik', ToastAndroid.SHORT);
      yield put({type: 'FETCH_USER_THREADS_REQUESTED'});
      yield put({type: 'RESET_ROUTE', key: 'homepage'});
      yield put({type: 'PUSH_ROUTE', key: 'thread'});
    }
  } catch (error) {
    yield put({type: 'CREATE_THREAD_FAILED', error});
    ToastAndroid.show('Gagal membuat topik', ToastAndroid.SHORT);
  }
}
