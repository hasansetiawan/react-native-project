import {call, put, take} from 'redux-saga/effects';

import UserAPI from '../API/UserAPI';
import formatToCamelCase from '../helpers/formatToCamelCase';
import {ToastAndroid} from 'react-native';
export function* watchRegisterUserSaga(): Object {
  while (true) { // eslint-disable-line
    let action = yield take('REGISTER_USER');
    try {
      let {registration} = action;
      let result = yield call(UserAPI.registerUser, registration);
      let user = formatToCamelCase(result.data);
      if (result.result === 'sukses') {
        yield put({type: 'REGISTER_USER_SUCCESS', user: user});
      } else {
        yield put({type: 'REGISTER_USER_FAILED', message: result.message});
        ToastAndroid.show('Gagal melakukan pendaftaran', ToastAndroid.SHORT);
      }
    } catch (error) {
      ToastAndroid.show('Mohon isi email atau nomor handphone dengan benar', ToastAndroid.SHORT);
      yield put({type: 'REGISTER_USER_ERROR', error: error});
    }
    yield take('LOGOUT');
  }
}
