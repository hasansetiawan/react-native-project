
import {takeLatest} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';
import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';
import getToken from '../selectors/getToken';

import PlantsAPI from '../API/PlantsAPI';

function* addPhotoPost(action) {
  try {
    let token = yield select(getToken);
    let selectedPlant = yield select((state) => state.selectedPlant);
    let {addPhoto, isUpdatePlant} = action;
    yield call(PlantsAPI.updateGalleryPlants, token, addPhoto);
    yield put({type: 'ADD_PHOTO_POST_SUCCESS'});
    if (isUpdatePlant) {
      yield put({
        type: 'FETCH_PLANT_DETAIL_REQUESTED',
        plantID: selectedPlant,
      });
    }
    yield put({type: 'POP_ROUTE'});
  } catch (err) {
    yield put({type: 'ADD_PHOTO_POST_FAILED', error: err});
  }
}

export function* watchAddPhotoPostSaga(): any {
  yield takeLatest('ADD_PHOTO_POST', addPhotoPost);
}

function* addPhoto() {
  let plants = yield select((state) => state.plants);
  if (plants.size === 0) {
    try {
      let token = yield select(getToken);
      let result = yield call(PlantsAPI.getAllPlant, token);
      let options = {
        isReplaceLiteral: true,
        isAddPrefix: false,
        textReplacement: [{
          targetKey: 'agePlant',
          replaceWith: 'plantAge',
        }, {
          targetKey: 'foto',
          replaceWith: 'photo',
        }, {
          targetKey: 'fotoThumb',
          replaceWith: 'photoThumb',
        }],
      };
      let camelCasedResult = [];
      for (let singlePlant of result) {
        let plant = yield call(formatToCamelCase, singlePlant);
        let modifiedPlant = yield call(formatObjectKeys, plant, options);
        let finalPlant = {
          ...modifiedPlant,
          photo: {uri: modifiedPlant.photo},
          photoThumb: {uri: modifiedPlant.photoThumb},
        };
        camelCasedResult.push(finalPlant);
      }
      yield put({type: 'FETCH_PLANTS_SUCCESS', plants: camelCasedResult});
    } catch (err) {
      yield put({type: 'ADD_PHOTO_FAILED', error: err});
    }
  }
}

export function* watchAddPhotoSaga(): any {
  yield takeLatest('ADD_PHOTO', addPhoto);
}
