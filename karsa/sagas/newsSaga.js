import formatToCamelCase from '../helpers/formatToCamelCase';

import {takeLatest, takeEvery} from 'redux-saga';
import {call, put, select} from 'redux-saga/effects';

import {getNews, getNewsHighlights, getNewsDetail} from '../API/NewsAPI';

export function* watchNewsSaga() {
  let isFetchNews = ({type, key}) => (type === 'PUSH_ROUTE' && key === 'news');
  yield takeLatest(isFetchNews, fetchNews);
  yield takeEvery('FETCH_NEWS_REQUESTED', fetchNews);
  yield takeEvery('FETCH_MORE_NEWS', fetchMoreNews);
  yield takeLatest('NEWS_SELECTED', selectNewsSaga);
  yield takeLatest('NEWS_HIGHLIGHT_SELECTED', selectNewsHighlight);
}

export function* selectNewsHighlight(action) {
  let {id} = action;
  yield put({type: 'SAVE_SELECTED_HIGHLIGHT', id});
  yield put({type: 'PUSH_ROUTE', key: 'newsHighlight'});
}

export function* selectNewsSaga(action) {
  let {id} = action;
  let allNews = yield select((state) => state.news.allNews);
  let selectedNews = allNews.get(id);
  if (!selectedNews) {
    selectedNews = yield call(fetchNewsDetail, id);
  }
  if (selectedNews.url) {
    yield put({type: 'SAVE_SELECTED_NEWS', id});
    yield put({type: 'PUSH_ROUTE', key: 'newsDetail'});
  } else {
    yield put({type: 'SAVE_SELECTED_HIGHLIGHT', id});
    yield put({type: 'PUSH_ROUTE', key: 'newsHighlight'});
  }
}

export function* fetchNewsDetail(id) {
  let result = yield call(getNewsDetail, id);
  let camelCased = yield call(formatToCamelCase, result.message);
  camelCased.foto = camelCased.foto ? {uri: camelCased.foto} : null;
  camelCased.urlImageRss = camelCased.urlImageRss ? {uri: camelCased.urlImageRss} : null;
  let news = new Map().set(camelCased.id, camelCased);
  yield put({type: 'FETCH_MORE_NEWS_SUCCEED', news});
  return camelCased;
}

export function* fetchNews() {
  try {
    let res = yield [call(getNews, 1), call(getNewsHighlights)];
    let [news, newsHighlight] = res;
    if (news.result === 'sukses' && newsHighlight.result === 'sukses') {
      let finalNews = new Map();
      for (let item of news.message) {
        let camelCased = yield call(formatToCamelCase, item);
        camelCased.foto = camelCased.foto ? {uri: camelCased.foto} : null;
        camelCased.urlImageRss = camelCased.urlImageRss ? {uri: camelCased.urlImageRss} : null;
        finalNews.set(camelCased.id, camelCased);
      }
      for (let item of newsHighlight.message) {
        let camelCased = yield call(formatToCamelCase, item);
        camelCased.foto = camelCased.foto ? {uri: camelCased.foto} : null;
        camelCased.urlImageRss = camelCased.urlImageRss ? {uri: camelCased.urlImageRss} : null;
        finalNews.set(camelCased.id, camelCased);
      }
      yield put({type: 'FETCH_NEWS_SUCCEED', news: finalNews});
    } else {
      throw new Error(res.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_NEWS_FAILED', error});
  }
}

export function* fetchMoreNews(action) {
  let {page} = action;
  try {
    let res = yield call(getNews, page);
    if (res.result === 'sukses') {
      let finalNews = new Map();
      for (let item of res.message) {
        let camelCased = yield call(formatToCamelCase, item);
        camelCased.foto = camelCased.foto ? {uri: camelCased.foto} : null;
        camelCased.urlImageRss = camelCased.urlImageRss ? {uri: camelCased.urlImageRss} : null;
        finalNews.set(camelCased.id, camelCased);
      }
      yield put({type: 'FETCH_MORE_NEWS_SUCCEED', news: finalNews});
    } else {
      throw new Error(res.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_NEWS_FAILED', error});
  }
}
