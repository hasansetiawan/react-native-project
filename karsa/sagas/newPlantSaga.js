import {Alert} from 'react-native';
import {takeLatest} from 'redux-saga';
import {select, call, put} from 'redux-saga/effects';

import PlantsAPI from '../API/PlantsAPI';

export function* watchNewPlantSaga(): Object {
  yield [
    takeLatest('FETCH_PLANT_TYPES_REQUESTED', fetchPlantTypes),
    takeLatest('FETCH_PLANT_CATEGORIES_REQUESTED', fetchPlantCategories),
    takeLatest('FETCH_PLANT_VARIETIES_REQUESTED', fetchPlantVarieties),
    takeLatest('FETCH_PLANTING_METHODS_REQUESTED', fetchPlantingMethods),
    takeLatest('FETCH_PLANTING_RECOMMENDATION_REQUESTED', fetchPlantingRecommendation),
    takeLatest('SUBMIT_NEW_PLANT_REQUESTED', submitNewPlant),
  ];
}

export function* fetchPlantTypes(): any {
  try {
    let APIplantTypes = yield call(PlantsAPI.getTypePlant);
    let plantTypes = [];
    for (let plantType of APIplantTypes) {
      plantTypes.push({
        value: plantType.id,
        label: plantType.name,
      });
    }
    yield put({
      type: 'FETCH_PLANT_TYPES_SUCCESS',
      plantTypes,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANT_TYPES_FAILED', error});
  }
}

export function* fetchPlantCategories(action: Action) {
  try {
    let {plantType} = action;
    let token = yield select(getToken);
    let APIplantCategories = yield call(PlantsAPI.getPlantCategories, token, plantType);
    let plantCategories = [];
    for (let plantCategory of APIplantCategories) {
      plantCategories.push({
        value: plantCategory.id,
        label: plantCategory.name,
      });
    }
    yield put({
      type: 'FETCH_PLANT_CATEGORIES_SUCCESS',
      plantCategories,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANT_CATEGORIES_FAILED', error});
  }
}

export function* fetchPlantVarieties(action: Action) {
  try {
    let {plantType, plantCategory} = action;
    let token = yield select(getToken);
    let APIplantVarieties = yield call(PlantsAPI.getPlantVarieties, token, plantCategory, plantType);
    let plantVarieties = [];
    for (let plantVariant of APIplantVarieties) {
      plantVarieties.push({
        value: plantVariant.id,
        label: plantVariant.name,
      });
    }
    yield put({
      type: 'FETCH_PLANT_VARIETIES_SUCCESS',
      plantVarieties,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANT_VARIETIES_FAILED', error});
  }
}

export function* fetchPlantingMethods(action: Action) {
  try {
    let {plantType, plantCategory} = action;
    let token = yield select(getToken);
    let APIPlantingMethods = yield call(PlantsAPI.getPlantingMethods, token, plantCategory, plantType);
    let plantingMethods = [];
    for (let method of APIPlantingMethods) {
      plantingMethods.push({
        value: method.id,
        label: method.cropping_name,
      });
    }
    yield put({
      type: 'FETCH_PLANTING_METHODS_SUCCESS',
      plantingMethods,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANTING_METHODS_FAILED', error});
  }
}

export function* fetchPlantingRecommendation(action: Action) {
  let {plantType, plantCategory, plantVariant, fieldAltitude} = action;
  try {
    let token = yield select(getToken);
    let plantingRecommendation = yield call(PlantsAPI.getPlantingRecommendation, token, plantType, plantCategory, plantVariant, fieldAltitude);
    yield put({
      type: 'FETCH_PLANTING_RECOMMENDATION_SUCCESS',
      plantingRecommendation,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANTING_RECOMMENDATION_FAILED', error});
  }
}

export function* submitNewPlant(action: Action) {
  let {plant} = action;
  try {
    let token = yield select(getToken);
    let submitNewPlantResult = yield call(PlantsAPI.addNewPlant, token, plant);
    if (submitNewPlantResult === 'data tanaman berhasil disimpan') {
      submitNewPlantResult = 'Data tanaman berhasil disimpan.';
    }
    Alert.alert('Tanamanku', submitNewPlantResult);
    yield put({type: 'POP_ROUTE'});
  } catch (error) {
    Alert.alert('Tanamanku', 'Penyimpanan data tanaman gagal');
    yield put({type: 'SUBMIT_NEW_PLANT_FAILED', error});
  }
}

export function getToken(state) {
  return state.currentUser.token;
}
