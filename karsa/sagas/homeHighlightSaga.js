import HomeHighlightAPI from '../API/HomeHighlightAPI';
import formatObjectKeys from '../helpers/formatObjectKeys';

import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';

export function* watchHomeHighlightSaga(): Object {
  yield takeLatest('FETCH_HOME_HIGHLIGHT_REQUESTED', fetchHomeHighlight);
}

function* fetchHomeHighlight() {
  const options = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'id_thread',
      replaceWith: 'threadID',
    }, {
      targetKey: 'id_news',
      replaceWith: 'newsID',
    }],
  };
  try {
    let result = yield call(HomeHighlightAPI.getHighlightContent);
    let highlights = [];
    for (let highlight of result) {
      let formatted = yield call(formatObjectKeys, highlight, options);
      highlights.push(formatted);
    }
    yield put({type: 'FETCH_HOME_HIGHLIGHT_SUCCEED', highlights});
  } catch (error) {
    yield put({type: 'FETCH_HOME_HIGHLIGHT_FAILED', error: error});
  }
}
