
import {put, call} from 'redux-saga/effects';

import GamificationAPI from '../../API/GamificationAPI';
import formatObjectKeys from '../../helpers/formatObjectKeys';

export default function* levelSaga() {
  let levelKeys = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'nama',
      replaceWith: 'name',
    }, {
      targetKey: 'level_points',
      replaceWith: 'requirement',
    }, {
      targetKey: 'bonus_points',
      replaceWith: 'bonusPoints',
    }, {
      targetKey: 'exchange_points',
      replaceWith: 'coins',
    }],
  };
  try {
    let levels = yield GamificationAPI.fetchLevelsInfo();
    let formated = [];
    for (let level of levels) {
      let temp = yield call(formatObjectKeys, level, levelKeys);
      formated.push(temp);
    }
    yield put({
      type: 'FETCH_LEVELS_SUCCESS',
      levels: formated,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_LEVELS_FAILED',
      error,
    });
  }
}
