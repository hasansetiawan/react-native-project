// @flow

import GamificationAPI from '../../API/GamificationAPI';
import formatObjectKeys from '../../helpers/formatObjectKeys';
import getToken from '../../selectors/getToken';

import {ToastAndroid} from 'react-native';
import {put, call, select} from 'redux-saga/effects';

export function* actionsSaga(): Generator<any, void, any> {
  let actionKeys = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'point',
      replaceWith: 'coin',
    }, {
      targetKey: 'details',
      replaceWith: 'detail',
    }],
  };
  try {
    let rawActions = yield GamificationAPI.fetchActions();
    let formattedActions = [];
    if (rawActions) {
      for (let action of rawActions) {
        let formatted = yield call(formatObjectKeys, action, actionKeys);
        formattedActions.push(formatted);
      }
    }
    yield put({type: 'FETCH_GAMIFICATION_ACTIONS_SUCCEED', actions: formattedActions});
  } catch (error) {
    ToastAndroid.show(error.message, ToastAndroid.SHORT);
    yield put({type: 'FETCH_GAMIFICATION_ACTIONS_FAILED', error});
  }
}

export function* userActionsSaga(): Generator<any, void, any> {
  let userActionKeys = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'givenPoint',
      replaceWith: 'givenCoin',
    }, {
      targetKey: 'created_at',
      replaceWith: 'createdAt',
    }],
  };
  try {
    let token = yield select(getToken);
    let rawUserActions = yield call(GamificationAPI.fetchUserActions, token);
    let formattedUserActions = [];
    if (rawUserActions) {
      for (let userAction of rawUserActions) {
        let formatted = yield call(formatObjectKeys, userAction, userActionKeys);
        formattedUserActions.push(formatted);
      }
    }
    yield put({type: 'FETCH_USER_GAMIFICATION_ACTIONS_SUCCEED', userActions: formattedUserActions});
  } catch (error) {
    if (error.message.startsWith('Gagal')) {
      ToastAndroid.show(error.message, ToastAndroid.SHORT);
    }
    yield put({type: 'FETCH_USER_GAMIFICATION_ACTIONS_FAILED', error});
  }
}

export function* userScoresSaga(): Generator<any, void, any> {
  let userScoreKeys = {
    isAddPrefix: false,
    isReplaceLiteral: true,
    textReplacement: [{
      targetKey: 'lifetime',
      replaceWith: 'oneTime',
    }, {
      targetKey: '4xlifetime',
      replaceWith: 'fourTimes',
    }],
  };
  try {
    let token = yield select(getToken);
    let rawUserScores = yield call(GamificationAPI.fetchUserScores, token);
    let formattedUserScores = [];
    if (rawUserScores) {
      for (let userScore of rawUserScores) {
        let formatted = yield call(formatObjectKeys, userScore, userScoreKeys);
        formattedUserScores.push(formatted);
      }
    }
    yield put({type: 'FETCH_USER_SCORES_SUCCEED', userScores: formattedUserScores});
  } catch (error) {
    if (error.message.startsWith('Gagal')) {
      ToastAndroid.show(error.message, ToastAndroid.SHORT);
    }
    yield put({type: 'FETCH_USER_SCORES_FAILED', error});
  }
}
