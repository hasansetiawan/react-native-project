// @flow
import {call, put} from 'redux-saga/effects';

import GamificationAPI from '../../API/GamificationAPI';

export default function* fetchRewards(): Generator<*, *, *> {
  try {
    let rewards = yield call(GamificationAPI.fetchRewards);
    yield put({type: 'FETCH_REWARDS_SUCCESS', rewards: rewards});
  } catch (error) {
    yield put({type: 'FETCH_REWARDS_FAILED', error: error});
  }
}
