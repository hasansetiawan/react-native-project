import {takeLatest} from 'redux-saga';

import gamificationLevelSaga from './levelSaga';
import gamificationRewardsSaga from './rewardsSaga';

import {actionsSaga, userActionsSaga, userScoresSaga} from './actionsSaga';

export default function* gamifySaga() {
  yield [
    takeLatest('FETCH_LEVELS_REQUESTED', gamificationLevelSaga),
    takeLatest('FETCH_REWARDS_REQUESTED', gamificationRewardsSaga),
    takeLatest('FETCH_GAMIFICATION_ACTIONS_REQUESTED', actionsSaga),
    takeLatest('FETCH_USER_GAMIFICATION_ACTIONS_REQUESTED', userActionsSaga),
    takeLatest('FETCH_USER_SCORES_REQUESTED', userScoresSaga),
  ];
}
