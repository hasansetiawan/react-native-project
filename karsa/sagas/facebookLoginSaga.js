import {AsyncStorage} from 'react-native';
import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';
import {
  LoginManager,
  GraphRequest,
  GraphRequestManager,
} from 'react-native-fbsdk';
import formatToCamelCase from '../helpers/formatToCamelCase';
import UserAPI from '../API/UserAPI';

type FBUserData = {
  id: string;
  email: string;
  name: string;
  picture: {
    data: {
      is_silhouette: boolean;
      url: string;
    };
  };
};

function* facebookLogin() {
  try {
    let logInResult = yield call(LoginManager.logInWithReadPermissions, ['public_profile', 'email']);
    if (logInResult.isCancelled) {
      yield put({type: 'FACEBOOK_LOGIN_CANCEL'});
      return;
    }
    let graphPromise = () => {
      return new Promise((resolve, reject) => {
        let infoRequest = new GraphRequest('/me?fields=email,name,picture', null, (error, result) => {
          if (error) {
            reject(error);
          } else {
            resolve(result);
          }
        });
        new GraphRequestManager().addRequest(infoRequest).start();
      });
    };
    let fbUserData: FBUserData = yield call(graphPromise);
    let registerResult = yield call(UserAPI.registerWithFacebook, {
      id: fbUserData.id,
      email: fbUserData.email,
      name: fbUserData.name,
      role: 'Petani',
      avatar: fbUserData.picture.data.url,
    });
    if (registerResult.result === 'sukses') {
      const currentUser = formatToCamelCase(registerResult.data);
      yield put({type: 'LOGIN_SUCCESS', currentUser});
    }
    yield put({type: 'FETCH_NEWS_REQUESTED'});
    try {
      let result = yield call(AsyncStorage.getItem, '@favorites');
      if (result != null) {
        let favorites = JSON.parse(result);
        yield put({type: 'INIT_FAVORITES', favorites});
      }
    } catch (error) {
      yield put({type: 'INIT_FAVORITES_FAILED', error});
    }
  } catch (error) {
    yield put({type: 'FACEBOOK_LOGIN_ERROR', error: error});
  }
}

export function* watchFacebookLoginSaga(): Object {
  yield takeLatest('FACEBOOK_LOGIN', facebookLogin);
}
