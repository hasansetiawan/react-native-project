
import {call, put, select} from 'redux-saga/effects';
import {takeLatest, takeEvery} from 'redux-saga';
import {Alert} from 'react-native';

import formatObjectKeys from '../helpers/formatObjectKeys';
import formatToCamelCase from '../helpers/formatToCamelCase';
import noImage from '../images/no_image.png';
import getToken from '../selectors/getToken';
import ProductAPI from '../API/ProductAPI';
import formatGallery from '../helpers/formatGallery';

import type {Action} from '../types/Action';

export let productKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'product_id',
    replaceWith: 'id',
  }, {
    targetKey: 'brand',
    replaceWith: 'name',
  }, {
    targetKey: 'photo_product',
    replaceWith: 'photo',
  }, {
    targetKey: 'product_name',
    replaceWith: 'name',
  }, {
    targetKey: 'desc',
    replaceWith: 'description',
  }, {
    targetKey: 'price_min',
    replaceWith: 'priceMin',
  }, {
    targetKey: 'price_max',
    replaceWith: 'priceMax',
  }, {
    targetKey: 'user_id',
    replaceWith: 'producerID',
  }, {
    targetKey: 'produsen_name',
    replaceWith: 'producerName',
  }, {
    targetKey: 'produsen_address',
    replaceWith: 'producerAddress',
  }, {
    targetKey: 'province',
    replaceWith: 'producerProvince',
  }, {
    targetKey: 'city',
    replaceWith: 'producerCity',
  }, {
    targetKey: 'count_comment',
    replaceWith: 'reviewCount',
  }, {
    targetKey: 'count_reviews',
    replaceWith: 'reviewCount',
  }, {
    targetKey: 'product_photo',
    replaceWith: 'photo',
  }, {
    targetKey: 'seeds',
    replaceWith: 'seed',
  }, {
    targetKey: 'product_cat',
    replaceWith: 'productCategory',
  }, {
    targetKey: 'price_1',
    replaceWith: 'priceMin',
  }, {
    targetKey: 'price_2',
    replaceWith: 'priceMax',
  }, {
    targetKey: 'photo_product',
    replaceWith: 'photo',
  }, {
    targetKey: 'product_owner',
    replaceWith: 'producerName',
  }, {
    targetKey: 'store_owner',
    replaceWith: 'producerName',
  }, {
    targetKey: 'address',
    replaceWith: 'producerAddress',
  }, {
    targetKey: 'province_name',
    replaceWith: 'producerProvince',
  }, {
    targetKey: 'city_name',
    replaceWith: 'producerCity',
  }, {
    targetKey: 'store_id',
    replaceWith: 'storeID',
  }],
};
let pesticideKeys = {
  isReplaceLiteral: true,
  isAddPrefix: false,
  textReplacement: [{
    targetKey: 'pesticide_formulations',
    replaceWith: 'formulations',
  }, {
    targetKey: 'pesticide_color',
    replaceWith: 'color',
  }, {
    targetKey: 'pesticide_purity',
    replaceWith: 'purity',
  }, {
    targetKey: 'pesticide_toxix_type',
    replaceWith: 'toxicType',
  }, {
    targetKey: 'pesticide_license_number',
    replaceWith: 'licenseNumber',
  }, {
    targetKey: 'pesticide_pesticide',
    replaceWith: 'type',
  }, {
    targetKey: 'pesticide_desc',
    replaceWith: 'description',
  }, {
    targetKey: 'pesticide_how_app',
    replaceWith: 'howToUse',
  }, {
    targetKey: 'pesticide_packing',
    replaceWith: 'packing',
  }, {
    targetKey: 'pesticide_dose',
    replaceWith: 'dose',
  }, {
    targetKey: 'pesticide_procedure',
    replaceWith: 'procedure',
  }, {
    targetKey: 'pesticide_commodity',
    replaceWith: 'commodity',
  }, {
    targetKey: 'pesticide_pest',
    replaceWith: 'pest',
  }, {
    targetKey: 'pesticide_materials',
    replaceWith: 'materials',
  }],
};
let fertilizerKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'fert_composition',
    replaceWith: 'composition',
  }, {
    targetKey: 'fert_purity',
    replaceWith: 'purity',
  }, {
    targetKey: 'fert_classification',
    replaceWith: 'classification',
  }, {
    targetKey: 'fert_color',
    replaceWith: 'color',
  }, {
    targetKey: 'fert_license_number',
    replaceWith: 'licenseNumber',
  }, {
    targetKey: 'fert_group',
    replaceWith: 'group',
  }, {
    targetKey: 'fert_excellence',
    replaceWith: 'excellence',
  }, {
    targetKey: 'fert_how_app',
    replaceWith: 'howToUse',
  }, {
    targetKey: 'fert_dose',
    replaceWith: 'dose',
  }, {
    targetKey: 'fert_procedure',
    replaceWith: 'procedure',
  }, {
    targetKey: 'fert_netto',
    replaceWith: 'netto',
  }, {
    targetKey: 'fert_warning',
    replaceWith: 'warning',
  }],
};
let seedKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'seeds_variety',
    replaceWith: 'variety',
  }, {
    targetKey: 'seeds_recomendation',
    replaceWith: 'recomendation',
  }, {
    targetKey: 'seeds_sprout',
    replaceWith: 'sprout',
  }, {
    targetKey: 'seeds_purity',
    replaceWith: 'purity',
  }, {
    targetKey: 'seeds_license_number',
    replaceWith: 'licenseNumber',
  }, {
    targetKey: 'seeds_age',
    replaceWith: 'age',
  }, {
    targetKey: 'seeds_weight',
    replaceWith: 'weight',
  }, {
    targetKey: 'seeds_durability',
    replaceWith: 'durability',
  }, {
    targetKey: 'seeds_potency',
    replaceWith: 'potency',
  }, {
    targetKey: 'seeds_netto',
    replaceWith: 'netto',
  }, {
    targetKey: 'seeds_plant_type_name',
    replaceWith: 'plantType',
  }],
};
let equipmentKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'eq_sni_iso',
    replaceWith: 'sniISO',
  }, {
    targetKey: 'eq_type_product',
    replaceWith: 'type',
  }, {
    targetKey: 'eq_classification',
    replaceWith: 'classification',
  }, {
    targetKey: 'eq_cat_product',
    replaceWith: 'category',
  }, {
    targetKey: 'eq_dimention',
    replaceWith: 'dimension',
  }, {
    targetKey: 'eq_how_app',
    replaceWith: 'howToUse',
  }, {
    targetKey: 'eq_weight',
    replaceWith: 'weight',
  }, {
    targetKey: 'eq_materials',
    replaceWith: 'materials',
  }, {
    targetKey: 'eq_usefulness',
    replaceWith: 'usefulness',
  }, {
    targetKey: 'eq_warning',
    replaceWith: 'warning',
  }],
};
let plantForProductKeys = {
  isReplaceLiteral: true,
  isAddPrefix: false,
  textReplacement: [{
    targetKey: 'plant_name',
    replaceWith: 'plantName',
  }, {
    targetKey: 'plant_category_name',
    replaceWith: 'plantCategory',
  }, {
    targetKey: 'plant_varieties_name',
    replaceWith: 'plantVariant',
  }, {
    targetKey: 'plant_address',
    replaceWith: 'plantAddressGps',
  }, {
    targetKey: 'foto',
    replaceWith: 'photo',
  }, {
    targetKey: 'foto_thumb',
    replaceWith: 'photoThumb',
  }, {
    targetKey: 'province_name',
    replaceWith: 'provinceName',
  }, {
    targetKey: 'city_name',
    replaceWith: 'cityName',
  }, {
    targetKey: 'subdistrict_name',
    replaceWith: 'subdistrictName',
  }, {
    targetKey: 'village_name',
    replaceWith: 'villageName',
  }],
};

export function* fetchProductForPest(action: Action) {
  let {pestID} = action;
  try {
    let pestProductResult = yield call(ProductAPI.getProductPest, pestID);
    let pestProducts = [];
    for (let pestProduct of pestProductResult) {
      let formated = yield call(formatObjectKeys, pestProduct, productKeys);
      let camelcased = yield call(formatToCamelCase, formated);
      camelcased.gallery = formatGallery(camelcased.gallery);
      camelcased.pestID = pestID;
      let photo = camelcased.photo ? {uri: camelcased.photo} : noImage;
      camelcased.photo = photo;
      pestProducts.push(camelcased);
    }
    yield put({
      type: 'FETCH_PRODUCTS_PEST_SUCCESS',
      products: pestProducts,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCTS_PEST_FAILED',
      error,
    });
  }
}
export function* fetchProductForDisease(action: Action) {
  let {diseaseID} = action;
  try {
    let diseaseProductResult = yield call(ProductAPI.getProductDisease, diseaseID);
    let diseaseProducts = [];
    for (let diseaseProduct of diseaseProductResult) {
      let formated = yield call(formatObjectKeys, diseaseProduct, productKeys);
      let camelcased = yield call(formatToCamelCase, formated);
      camelcased.gallery = formatGallery(camelcased.gallery);
      camelcased.diseaseID = diseaseID;
      let photo = camelcased.photo ? {uri: camelcased.photo} : noImage;
      camelcased.photo = photo;
      diseaseProducts.push(camelcased);
    }
    yield put({
      type: 'FETCH_PRODUCTS_DISEASE_SUCCESS',
      products: diseaseProducts,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCTS_DISEASE_FAILED',
      error,
    });
  }
}
export function* fetchProductForPlant(action: Action) {
  let {plantID} = action;
  let token = yield select(getToken);
  try {
    let plantProductResult = yield call(ProductAPI.getProductOfPlant, plantID, token);
    let plantProducts = [];
    for (let plantProduct of plantProductResult) {
      let formated = yield call(formatObjectKeys, plantProduct, productKeys);
      let camelcased = yield call(formatToCamelCase, formated);
      camelcased.gallery = formatGallery(camelcased.gallery);
      let photo = camelcased.photo ? {uri: camelcased.photo} : noImage;
      camelcased.photo = photo;
      camelcased.plantID = plantID;
      plantProducts.push(camelcased);
    }
    yield put({
      type: 'FETCH_PRODUCTS_PLANT_SUCCESS',
      products: plantProducts,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCTS_PLANT_FAILED',
      error,
    });
  }
}
export function* fetchProductDetail(action: Action) {
  let {productID} = action;
  let token = yield select(getToken);
  try {
    let detail = yield call(ProductAPI.getProductDetail, productID, token);
    let formated = yield call(formatObjectKeys, detail, productKeys);
    let equipment = yield call(formatObjectKeys, formated.equipment, equipmentKeys);
    let fertilizer = yield call(formatObjectKeys, formated.fertilizer, fertilizerKeys);
    let seed = yield call(formatObjectKeys, formated.seed, seedKeys);
    let pesticide = yield call(formatObjectKeys, formated.pesticide, pesticideKeys);
    let gallery = yield call(formatGallery, formated.gallery);
    formated.photo = formated.photo ? {uri: formated.photo} : noImage;
    formated = {
      ...formated,
      seed,
      pesticide,
      fertilizer,
      gallery,
      equipment,
    };
    yield put({
      type: 'FETCH_PRODUCT_DETAIL_SUCCESS',
      productDetail: formated,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCT_DETAIL_FAILED',
      error,
    });
  }
}
export function* fetchProductReview(action: Action) {
  let {page} = action;
  let productID = yield select((state) => state.selectedProduct);
  try {
    let reviews = yield call(ProductAPI.getReviewsProduct, productID, page);
    let formatedReviews = [];
    for (let review of reviews) {
      let reviewCamelCased = yield call(formatToCamelCase, review);
      let {avatar, avatarFB} = reviewCamelCased;
      reviewCamelCased.avatar = avatar ? {uri: avatar} : null;
      reviewCamelCased.avatarFB = avatarFB ? {uri: avatarFB} : null;
      formatedReviews.push(reviewCamelCased);
    }
    yield put({
      type: 'FETCH_PRODUCT_REVIEW_SUCCESS',
      reviews: formatedReviews,
      productID,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCT_REVIEW_FAILED',
      error,
      productID,
    });
  }
}
export function* fetchPlantForProduct(action: Action) {
  try {
    let token = yield select(getToken);
    let {productID} = action;
    let plants = yield call(ProductAPI.getPlantForProduct, productID, token);
    let formated = [];
    for (let plant of plants) {
      let temp = yield call(formatObjectKeys, plant, plantForProductKeys);
      temp.photo = temp.photo === '' ? noImage : {uri: temp.photo};
      temp.photoThumb = temp.photoThumb === '' ? noImage : {uri: temp.photoThumb};
      formated.push(temp);
    }

    yield put({
      type: 'FETCH_PLANT_FOR_PRODUCT_SUCCESS',
      plants: formated,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PLANT_FOR_PRODUCT_FAILED',
      error,
    });
  }
}
export function* postReviewForProduct(action: Action) {
  let token = yield select(getToken);
  let reviews = yield select((state) => state.products.get(state.selectedProduct).reviews);
  let currentUserID = yield select((state) => state.currentUser.id);
  let productID = yield select((state) => state.selectedProduct);
  let isSelfReviewExist = false;
  let reviewID = '';
  for (let review of reviews) {
    if (review.userID === currentUserID) {
      isSelfReviewExist = true;
      reviewID = review.id;
      break;
    }
  }
  let {review} = action;
  let newReviews = null;
  try {
    if (isSelfReviewExist) {
      newReviews = yield call(
        ProductAPI.updateReviewProduct,
        token, productID, reviewID, review.review, review.rate,
      );
    } else {
      newReviews = yield call(
        ProductAPI.postReviewProduct, token, productID,
        review.rate, review.review
      );
    }
    if (newReviews) {
      let formatedReviews = [];
      for (let review of newReviews) {
        let reviewCamelCased = yield call(formatToCamelCase, review);
        let {avatar, avatarFB} = reviewCamelCased;
        reviewCamelCased.avatar = avatar ? {uri: avatar} : null;
        reviewCamelCased.avatarFB = avatarFB ? {uri: avatarFB} : null;
        formatedReviews.push(reviewCamelCased);
      }
      yield put({
        type: 'POST_REVIEW_SUCCESS',
        reviews: formatedReviews,
        productID,
      });
    }
  } catch (error) {
    yield put({
      type: 'POST_REVIEW_FAILED',
      error,
    });
  }
}
export function* postPlantForProduct(action: Action) {
  let {plantData} = action;
  let token = yield select(getToken);

  try {
    let {productID, plantList} = plantData;
    let plantListID = plantList.map((plant) => plant.id);
    yield call(ProductAPI.postPlantForProduct, token, plantListID, productID);
    Alert.alert('Tambah Produk', 'Produk berhasil ditambahkan');
    yield put({
      type: 'POST_USE_PRODUCT_SUCCESS',
      plantListID,
    });
  } catch (error) {
    Alert.alert('Tambah Produk', 'Produk gagal ditambahkan');
    yield put({
      type: 'POST_USE_PRODUCT_FAILED',
      error,
    });
  }
}
export function* fetchProductList(action: Action) {
  let {target} = action;
  switch (target) {
    case 'normal': {
      let {category, page} = action;
      try {
        let products = yield call(ProductAPI.getAllProductList, category, page);
        let formated = [];
        for (let product of products) {
          let temp = yield call(formatObjectKeys, product, productKeys);
          temp.gallery = formatGallery(temp.gallery);
          temp.photo = temp.photo ? {uri: temp.photo} : noImage;
          formated.push(temp);
        }
        yield put({
          type: 'FETCH_PRODUCT_LIST_SUCCESS',
          products: formated,
          target: 'normal',
        });
      } catch (error) {
        yield put({type: 'FETCH_PRODUCT_LIST_FAILED', error});
      }
      break;
    }
    case 'filter': {
      let {category, subCategory, page} = action;
      try {
        let products = yield call(ProductAPI.getFilteredProductList, category, subCategory, page);
        let formated = [];
        for (let product of products) {
          let temp = yield call(formatObjectKeys, product, productKeys);
          temp.gallery = formatGallery(temp.gallery);
          temp.photo = temp.photo ? {uri: temp.photo} : noImage;
          formated.push(temp);
        }
        yield put({
          type: 'FETCH_PRODUCT_LIST_SUCCESS',
          products: formated,
          target: 'filter',
        });
      } catch (error) {
        yield put({type: 'FETCH_PRODUCT_LIST_FAILED', error});
      }
      break;
    }
    case 'search': {
      let {category, searchKey, page} = action;
      try {
        let products = yield call(ProductAPI.getSearchedProductList, category, searchKey, page);
        let formated = [];
        for (let product of products) {
          let temp = yield call(formatObjectKeys, product, productKeys);
          temp.gallery = formatGallery(temp.gallery);
          temp.photo = temp.photo ? {uri: temp.photo} : noImage;
          formated.push(temp);
        }
        yield put({
          type: 'FETCH_PRODUCT_LIST_SUCCESS',
          products: formated,
          target: 'search',
        });
      } catch (error) {
        yield put({type: 'FETCH_PRODUCT_LIST_FAILED', error});
      }
    }
  }
}
// TODO make helper function to auto format results came from API to reduce repetition

export function* fetchProductCommodity(action: Action) {
  let {category} = action;
  try {
    let commodities = yield call(ProductAPI.getProductDropdown, category);
    let formated = [];
    for (let commodity of commodities) {
      formated.push({
        id: commodity.product_subcategory_id,
        categoryName: commodity.subcategory_name,
      });
    }
    yield put({
      type: 'FETCH_PRODUCT_COMMODITY_SUCCESS',
      productCommodity: formated,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PRODUCT_COMMODITY_FAILED',
      error,
    });
  }
}
export function* watchProductSaga() {
  yield [
    takeLatest('FETCH_PRODUCTS_PEST_REQUESTED', fetchProductForPest),
    takeLatest('FETCH_PRODUCTS_DISEASE_REQUESTED', fetchProductForDisease),
    takeLatest('FETCH_PRODUCTS_PLANT_REQUESTED', fetchProductForPlant),
    takeLatest('FETCH_PRODUCT_DETAIL_REQUESTED', fetchProductDetail),
    takeEvery('FETCH_PRODUCT_REVIEW_REQUESTED', fetchProductReview),
    takeLatest('FETCH_PLANT_FOR_PRODUCT_REQUESTED', fetchPlantForProduct),
    takeLatest('POST_REVIEW_REQUESTED', postReviewForProduct),
    takeLatest('POST_USE_PRODUCT_REQUESTED', postPlantForProduct),
    takeEvery('FETCH_PRODUCT_LIST_REQUESTED', fetchProductList),
    takeLatest('FETCH_PRODUCT_COMMODITY_REQUESTED', fetchProductCommodity),
  ];
}
