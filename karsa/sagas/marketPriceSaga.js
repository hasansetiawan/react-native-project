import {takeLatest} from 'redux-saga';
import {put, call} from 'redux-saga/effects';

import MarketPriceAPI from '../API/MarketPriceAPI';

import formatObjectKeys from '../helpers/formatObjectKeys';

export function* watchMarketPriceSaga(): any {
  let pattern = (action) => {
    let isPushRoute = action.type === 'PUSH_ROUTE';
    let isMarketPrice = action.key === 'marketPrice';
    return (isPushRoute && isMarketPrice) || (action.type === 'FETCH_MARKET_PRICE_INITIAL_REQUESTED');
  };
  yield takeLatest(pattern, fetchInitialMarketPrice);
  yield takeLatest('FETCH_NATIONAL_PRICE_REQUESTED', fetchNationalPrice);
  yield takeLatest('FETCH_PROVINCE_PRICE_REQUESTED', fetchProvincePrice);
  yield takeLatest('FETCH_COMMODITY_PRICE_REQUESTED', fetchCommodityPrice);
}
let options = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'price_1',
    replaceWith: 'prevPrice',
  }, {
    targetKey: 'price_2',
    replaceWith: 'currPrice',
  }, {
    targetKey: 'date',
    replaceWith: 'currDate',
  }],
};
export function* fetchCommodityPrice(action): any {
  let {commodityName} = action;
  try {
    let result = yield call(MarketPriceAPI.marketpricemonth, commodityName);
    if (result.result === 'sukses') {
      let commodityPriceList = [];
      for (let i = 0; i < result.message.date.length; i++) {
        commodityPriceList.push({
          date: result.message.date[i],
          price: result.message.price[i],
        });
      }
      yield put({
        type: 'FETCH_COMMODITY_PRICE_SUCCESS',
        commodityPrice: {
          commodityName,
          commodityPriceList,
        },
      });
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_COMMODITY_PRICE_FAILED', error});
  }
}
export function* fetchProvincePrice(action: Object): any {
  let {province} = action;
  try {
    let result = yield call(MarketPriceAPI.marketPriceByCity, province);
    if (result.result === 'sukses') {
      let provincePriceList = result.message.market_data.map((data) => {
        let change = 0;
        if (data.price_up_down !== '') {
          change = parseFloat(data.price_up_down.replace(/[%()]/g, ''));
        }
        let prevPrice = parseInt(data.price + (data.price * change / 100), 10);
        let roundedPrevPrice = 50 * Math.round(prevPrice / 50);
        return {
          commodity: data.comodity,
          prevPrice: roundedPrevPrice,
          currPrice: data.price,
        };
      });
      let currDate = result.message.market_date;
      let provincePrice = {
        priceList: provincePriceList,
        currDate: currDate,
        provinceName: province,
      };
      yield put({type: 'FETCH_PROVINCE_PRICE_SUCCESS', provincePrice});
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_PROVINCE_PRICE_FAILED', error});
  }
}
export function* fetchInitialMarketPrice(): any {
  try {
    const [provincesResult, commoditiesResult, nationalPriceInfoResult, defaultProvincePriceInfoResult] = yield [
      call(MarketPriceAPI.cityMarketPrice),
      call(MarketPriceAPI.comodity),
      call(MarketPriceAPI.nationalMarketPrice),
      call(MarketPriceAPI.defaultMarketPrice),
    ];
    if (provincesResult.result === 'sukses') {
      if (commoditiesResult.result === 'sukses') {
        if (nationalPriceInfoResult.result === 'sukses') {
          if (defaultProvincePriceInfoResult.result === 'sukses') {
            let provinces = provincesResult.message.map((data) => data.city);
            let commodities = commoditiesResult.message.map((data) => data.comodity);
            let nationalPriceInfo = {
              priceList: nationalPriceInfoResult.message.market_data.map((data) => {
                return {
                  commodity: data.comodity,
                  prevPrice: data.price_1,
                  currPrice: data.price_2,
                };
              }),
              currDate: nationalPriceInfoResult.message.market_date,
            };
            let provincePriceInfo = {
              priceList: defaultProvincePriceInfoResult.message.data.map((data) => {
                return {
                  commodity: data.comodity,
                  prevPrice: data.price_1,
                  currPrice: data.price_2,
                };
              }),
              provinceName: defaultProvincePriceInfoResult.message.city,
              currDate: defaultProvincePriceInfoResult.message.market_date,
            };
            yield put({
              type: 'FETCH_MARKET_PRICE_INITIAL_SUCCESS',
              commodities,
              provinces,
              nationalPriceInfo,
              provincePriceInfo,
            });
          }
        }
      } else {
        throw new Error(commoditiesResult.message);
      }
    } else {
      throw new Error(provincesResult.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_MARKET_PRICE_INITIAL_FAILED', error});
  }
}
export function* fetchNationalPrice(): any {
  try {
    let result = yield call(MarketPriceAPI.nationalMarketPrice);
    if (result.result === 'sukses') {
      let nationalPriceList = [];
      let currDate = '';
      for (let item of result.message.market_data) {
        let nationalPriceItem = yield call(formatObjectKeys, item, options);
        nationalPriceList.push(nationalPriceItem);
        currDate = item.currDate;
      }
      let nationalPrice = {
        priceList: nationalPriceList,
        currDate,
      };
      yield put({type: 'FETCH_NATIONAL_PRICE_SUCCESS', nationalPrice});
    } else {
      throw new Error(result.message);
    }
  } catch (error) {
    yield put({type: 'FETCH_NATIONAL_PRICE_FAILED', error});
  }
}
