import {put, call} from 'redux-saga/effects';
import {takeLatest} from 'redux-saga';
import CompanyProfileAPI from '../API/CompanyProfileAPI';

export function* watchContactSaga() {
  yield takeLatest('FETCH_CONTACT_DATA_REQUESTED', fetchContactPage);
  yield takeLatest('SUGGESTION_SUBMISSION_REQUESTED', submitEmailMessage);
}

export function* submitEmailMessage(action) {
  let {emailMessage} = action;
  try {
    let res = yield call(CompanyProfileAPI.sendSuggestion, emailMessage);
    if (res.result === 'sukses') {
      yield put({type: 'SUGGESTION_SUBMISSION_SUCCEED'});
      yield put({type: 'POP_ROUTE'});
    } else {
      throw new Error(res.message);
    }
  } catch (error) {
    yield put({
      type: 'SUGGESTION_SUBMISSION_FAILED',
      error,
    });
  }
}

export function* fetchContactPage() {
  try {
    let res = yield call(CompanyProfileAPI.getBranchOffices);
    if (res.result === 'sukses') {
      yield put({
        type: 'FETCH_CONTACT_DATA_SUCCEED',
        contactData: res.data,
      });
    } else {
      throw new Error(res.message);
    }
  } catch (err) {
    yield put({type: 'FETCH_CONTACT_DATA_FAILED'});
  }
}
