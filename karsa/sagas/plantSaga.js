import {takeLatest, takeEvery} from 'redux-saga';
import {ToastAndroid} from 'react-native';

import {select, put, call} from 'redux-saga/effects';

import PlantsAPI from '../API/PlantsAPI';

import formatToCamelCase from '../helpers/formatToCamelCase';
import formatObjectKeys from '../helpers/formatObjectKeys';
import getToken from '../selectors/getToken';
import getCurrentPlantID from '../selectors/getCurrentPlantID';

import type {Action} from '../types/Action';

export function* watchPlantDetailSaga(): any {
  yield takeLatest('FETCH_PLANT_DETAIL_REQUESTED', fetchDetailPlant);
  yield takeLatest('DELETE_PLANT_REQUESTED', deletePlant);
  yield takeLatest('SHARE_PLANT_TO_FORUM_REQUESTED', sharePlantToForum);
}

export function* sharePlantToForum(action) {
  try {
    let token = yield select(getToken);
    yield call(PlantsAPI.sharePlantToForum, token, action.shareItem);
    yield put({type: 'SHARE_PLANT_TO_FORUM_SUCCEED'});
    ToastAndroid.show('Berhasil membagikan tanaman', ToastAndroid.SHORT);
  } catch (error) {
    yield put({type: 'SHARE_PLANT_TO_FORUM_FAILED', error});
    ToastAndroid.show('Gagal membagikan tanaman', ToastAndroid.SHORT);
  }
}

export function* fetchDetailPlant() {
  let options = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [{
      targetKey: 'plant_category_name',
      replaceWith: 'plantCategory',
    }, {
      targetKey: 'plant_varieties_name',
      replaceWith: 'plantVariant',
    }, {
      targetKey: 'cropping_system',
      replaceWith: 'plantingMethod',
    }, {
      targetKey: 'province',
      replaceWith: 'provinceID',
    }, {
      targetKey: 'city',
      replaceWith: 'cityID',
    }, {
      targetKey: 'subdistrict',
      replaceWith: 'subdistrictID',
    }, {
      targetKey: 'village',
      replaceWith: 'villageID',
    }, {
      targetKey: 'foto',
      replaceWith: 'photo',
    }, {
      targetKey: 'foto_thumb',
      replaceWith: 'photoThumb',
    }, {
      targetKey: 'type_land',
      replaceWith: 'landType',
    }, {
      targetKey: 'using_persemian',
      replaceWith: 'isSeedBed',
    }, {
      targetKey: 'height_land',
      replaceWith: 'landHeight',
    }, {
      targetKey: 'reaction',
      replaceWith: 'soilReaction',
    }, {
      targetKey: 'dolomit',
      replaceWith: 'dolomiteRequirement',
    }, {
      targetKey: 'estimates',
      replaceWith: 'harvestDate',
    }, {
      targetKey: 'potential',
      replaceWith: 'harvestPotential',
    }, {
      targetKey: 'plants_id',
      replaceWith: 'plantID',
    }, {
      targetKey: 'img_thumb',
      replaceWith: 'photoThumb',
    }, {
      targetKey: 'desc',
      replaceWith: 'description',
    }],
  };
  let phaseFormatOptions = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [{
      targetKey: 'desc',
      replaceWith: 'phaseDescription',
    }, {
      targetKey: 'time',
      replaceWith: 'nextPhaseTime',
    }, {
      targetKey: 'suggest',
      replaceWith: 'phaseSuggestions',
    }, {
      targetKey: 'persemaian',
      replaceWith: 'seedBed',
    }],
  };
  let suggestFormatOptions = {
    textReplacement: [{
      targetKey: 'name',
      replaceWith: 'suggestionName',
    }, {
      targetKey: 'hst',
      replaceWith: 'daysAfterPlanting',
    }, {
      targetKey: 'suggest',
      replaceWith: 'suggestions',
    }],
    isAddPrefix: false,
    isReplaceLiteral: true,
  };
  try {
    let plantID = yield select(getCurrentPlantID);
    let token = yield select(getToken);
    let [detailResult, photoResult, infoResult] = yield [
      yield call(PlantsAPI.getPlantDetail, token, plantID),
      yield call(PlantsAPI.getPlantGalleries, token, plantID),
      yield call(PlantsAPI.getPlantingInfo, token, plantID),
    ];
    let plantDetailPhase = yield call(formatObjectKeys, detailResult.fase, phaseFormatOptions);
    let plantDetailSuggestion = [];
    for (let suggest of plantDetailPhase.phaseSuggestions) {
      let suggestion = yield call(formatObjectKeys, suggest, suggestFormatOptions);
      plantDetailSuggestion.push(suggestion);
    }
    let formatedPhotoList = [];
    for (let photoItem of photoResult) {
      photoItem.photo = {uri: photoItem.photo};
      photoItem.img_thumb = {uri: photoItem.img_thumb};
      let formated = yield call(formatObjectKeys, photoItem, options);
      let camelCased = yield call(formatToCamelCase, formated);
      formatedPhotoList.push(camelCased);
    }
    detailResult.detail.foto = detailResult.detail.foto ? {uri: detailResult.detail.foto} : null;
    detailResult.detail.foto_thumb = detailResult.detail.foto_thumb ? {uri: detailResult.detail.foto_thumb} : null;
    plantDetailPhase.phaseSuggestions = plantDetailSuggestion;
    plantDetailPhase.seedBed = yield call(formatObjectKeys, plantDetailPhase.seedBed, phaseFormatOptions);
    let seedBedSuggest = [];
    for (let seedBedSuggestItem of plantDetailPhase.seedBed.phaseSuggestions) {
      let formated = yield call(formatObjectKeys, seedBedSuggestItem, suggestFormatOptions);
      seedBedSuggest.push(formated);
    }
    plantDetailPhase.seedBed.phaseSuggestions = seedBedSuggest;
    let plantDetail = {
      ...detailResult.detail,
      estimates: detailResult.estimates,
      potential: detailResult.potential,
      photos: formatedPhotoList,
      phase: plantDetailPhase,
    };
    plantDetail = yield call(formatObjectKeys, plantDetail, options);
    plantDetail = yield call(formatToCamelCase, plantDetail);
    let oldPlantDetail = yield select((state) => {
      return state.plants.get(plantDetail.id);
    });
    plantDetail = {...oldPlantDetail, ...plantDetail};
    let plantingInfoDetail = yield call(formatObjectKeys, infoResult, options);
    plantingInfoDetail = yield call(formatToCamelCase, plantingInfoDetail);
    yield put({
      type: 'FETCH_PLANT_DETAIL_SUCCESS',
      plantDetail,
      plantingInfoDetail,
    });
  } catch (error) {
    yield put({type: 'FETCH_PLANT_DETAIL_FAILED', error});
  }
}

export function* watchPlantSaga(): any {
  let pattern = (action) => {
    let isPushRoute = action.type === 'PUSH_ROUTE';
    let isMyPlant = action.key === 'weeklyGuidance';
    return (isPushRoute && isMyPlant) || (action.type === 'FETCH_PLANTS_REQUESTED');
  };
  yield [
    takeEvery(pattern, fetchPlant),
    takeEvery('SEARCH_PLANT_REQUESTED', searchPlant),
    takeLatest('FETCH_ALL_PLANTS_REQUESTED', fetchAllPlant),
  ];
}

let plantKeys = {
  isReplaceLiteral: true,
  isAddPrefix: false,
  textReplacement: [{
    targetKey: 'agePlant',
    replaceWith: 'plantAge',
  }, {
    targetKey: 'plantCategoryName',
    replaceWith: 'plantCategory',
  }, {
    targetKey: 'plantVarietiesName',
    replaceWith: 'plantVariant',
  }, {
    targetKey: 'foto',
    replaceWith: 'photo',
  }, {
    targetKey: 'fotoThumb',
    replaceWith: 'photoThumb',
  }],
};

export function* fetchAllPlant() {
  let token = yield select(getToken);
  let results = [];
  try {
    for (let page = 1; true; page++) { // eslint-disable-line no-constant-condition
      let plants = yield call(PlantsAPI.getAllPlant, token, page);
      results.push(...plants);
    }
    // will throw error if no more page to fetch so it will always jump to error handler
  } catch (error) {
    if (results.length === 0) {
      yield put({
        type: 'FETCH_ALL_PLANTS_FAILED',
        error,
      });
    } else {
      let camelCasedResult = [];
      for (let singlePlant of results) {
        let plant = yield call(formatToCamelCase, singlePlant);
        let modifiedPlant = yield call(formatObjectKeys, plant, plantKeys);
        let finalPlant = {
          ...modifiedPlant,
          photo: modifiedPlant.photo ? {uri: modifiedPlant.photo} : null,
          photoThumb: modifiedPlant.photoThumb ? {uri: modifiedPlant.photoThumb} : null,
        };
        camelCasedResult.push(finalPlant);
      }
      yield put({type: 'FETCH_ALL_PLANTS_SUCCESS', plants: camelCasedResult});
    }
  }
}

export function* fetchPlant(action: Action): any {
  try {
    let token = yield select(getToken);
    let {page} = action;
    let result = yield call(PlantsAPI.getAllPlant, token, page);
    let camelCasedResult = [];
    for (let singlePlant of result) {
      let plant = yield call(formatToCamelCase, singlePlant);
      let modifiedPlant = yield call(formatObjectKeys, plant, plantKeys);
      let finalPlant = {
        ...modifiedPlant,
        photo: modifiedPlant.photo ? {uri: modifiedPlant.photo} : null,
        photoThumb: modifiedPlant.photoThumb ? {uri: modifiedPlant.photoThumb} : null,
      };
      camelCasedResult.push(finalPlant);
    }
    yield put({type: 'FETCH_PLANTS_SUCCESS', plants: camelCasedResult, page});
  } catch (err) {
    yield put({type: 'FETCH_PLANTS_FAILED', error: err});
  }
}

export function* deletePlant(action: Action) {
  let {plantID} = action;
  let token = yield select(getToken);
  try {
    yield call(PlantsAPI.deletePlant, token, plantID);
    yield put({
      type: 'DELETE_PLANT_SUCCESS',
      plantID,
    });
    ToastAndroid.show('Berhasil Menghapus Tanaman', ToastAndroid.SHORT);
  } catch (error) {
    yield put({
      type: 'DELETE_PLANT_FAILED',
      error,
    });
    ToastAndroid.show('Gagal Menghapus Tanaman', ToastAndroid.SHORT);
  }
}

export function* searchPlant(action: Action): any {
  let {keyword} = action;
  let options = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [{
      targetKey: 'agePlant',
      replaceWith: 'plantAge',
    }, {
      targetKey: 'foto',
      replaceWith: 'photo',
    }, {
      targetKey: 'fotoThumb',
      replaceWith: 'photoThumb',
    }],
  };
  try {
    let token = yield select(getToken);
    let result = yield call(PlantsAPI.searchPlant, token, keyword);
    let camelCasedResult = [];
    for (let singlePlant of result) {
      let plant = yield call(formatToCamelCase, singlePlant);
      let modifiedPlant = yield call(formatObjectKeys, plant, options);
      let finalPlant = {
        ...modifiedPlant,
        photo: modifiedPlant.photo ? {uri: modifiedPlant.photo} : null,
        photoThumb: modifiedPlant.photoThumb ? {uri: modifiedPlant.photoThumb} : null,
      };
      camelCasedResult.push(finalPlant);
    }
    camelCasedResult ?
    yield put({type: 'SEARCH_PLANT_SUCCEED', plants: camelCasedResult}) :
    yield put({type: 'SEARCH_PLANT_EMPTY'});
  } catch (err) {
    yield put({type: 'SEARCH_PLANT_FAILED', error: err});
  }
}
