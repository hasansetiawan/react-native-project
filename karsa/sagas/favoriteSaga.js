import {AsyncStorage} from 'react-native';
import {takeEvery} from 'redux-saga';
import {select, call, put} from 'redux-saga/effects';

import type {RootState} from '../types/RootState';
import type {Action} from '../types/Action';

export function* addFavorite(action: Action): Generator<*, *, *> {
  if (action.type === 'ADD_FAVORITE_REQUESTED') {
    let favoriteState = yield select((state: RootState) => state.favorite);
    if (!favoriteState.favorites.some((favorite) => favorite.id === action.favorite.id)) {
      let favorites = [...favoriteState.favorites, action.favorite];
      yield call(AsyncStorage.setItem, '@favorites', JSON.stringify(favorites));
      yield put({type: 'ADD_FAVORITE_SUCCESS', favorites});
    }
  }
}

export function* deleteFavorite(action: Action): Generator<*, *, *> {
  if (action.type === 'DELETE_FAVORITE_REQUESTED') {
    let favoriteState = yield select((state: RootState) => state.favorite);
    let favorites = favoriteState.favorites.filter((favorite) => (favorite.id !== action.favoriteID));
    yield call(AsyncStorage.setItem, '@favorites', JSON.stringify(favorites));
    yield put({type: 'DELETE_FAVORITE_SUCCESS', favorites: favorites});
  }
}

export function* watchFavoriteSaga(): Generator<*, *, *> {
  yield [
    takeEvery('ADD_FAVORITE_REQUESTED', addFavorite),
    takeEvery('DELETE_FAVORITE_REQUESTED', deleteFavorite),
  ];
}
