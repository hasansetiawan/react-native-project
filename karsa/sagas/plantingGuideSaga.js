import {takeLatest} from 'redux-saga';
import {put, call} from 'redux-saga/effects';

import PlantingGuideAPI from '../API/PlantingGuideAPI';
import formatObjectKeys from '../helpers/formatObjectKeys';


export function* watchPlantingGuideSaga() {
  yield takeLatest('FETCH_PLANTING_GUIDES_REQUESTED', fetchPlantingGuides);
  yield takeLatest('FETCH_PLANTING_GUIDE_DETAIL_REQUESTED', fetchPlantingGuideDetail);
}

export function* fetchPlantingGuides() {
  const plantGuidesFormatOptions = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [
      {
        targetKey: 'type_plant_id',
        replaceWith: 'plantTypeID',
      },
      {
        targetKey: 'name',
        replaceWith: 'plantDetailName',
      },
    ],
  };
  try {
    let result = yield call(PlantingGuideAPI.getAllPlantingGuide);
    if (result) {
      let plantingGuides = [];
      for (let item of result) {
        let formatted = yield call(formatObjectKeys, item, plantGuidesFormatOptions);
        if (formatted) {
          plantingGuides.push(formatted);
        }
      }
      yield put({type: 'FETCH_PLANTING_GUIDES_SUCCEED', plantingGuides});
    }
  } catch (error) {
    yield put({type: 'FETCH_PLANTING_GUIDES_FAILED', error});
  }
}

export function* fetchPlantingGuideDetail(action: Action) {
  const plantGuideDetailFormatOptions = {
    isReplaceLiteral: true,
    isAddPrefix: false,
    textReplacement: [
      {
        targetKey: 'type_plant_id',
        replaceWith: 'plantTypeID',
      },
      {
        targetKey: 'name',
        replaceWith: 'plantDetailName',
      },
      {
        targetKey: 'description',
        replaceWith: 'htmlValue',
      },
    ],
  };
  try {
    let {plantDetail} = action;
    let result = yield call(PlantingGuideAPI.getPlantingGuideDetail, plantDetail.toLowerCase());
    if (result) {
      let formatted = yield call(formatObjectKeys, result, plantGuideDetailFormatOptions);
      if (formatted) {
        yield put({type: 'FETCH_PLANTING_GUIDE_DETAIL_SUCCEED', fetchedPlantingGuideDetail: formatted});
      }
    }
  } catch (error) {
    yield put({type: 'FETCH_PLANTING_GUIDE_DETAIL_FAILED', error});
  }
}
