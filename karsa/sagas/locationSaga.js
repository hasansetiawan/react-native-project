import {takeLatest} from 'redux-saga';
import {call, put} from 'redux-saga/effects';

import RegionAPI from '../API/RegionAPI';

type SubAddress = 'province' | 'city' | 'subdistrict' | 'village';

export function* watchLocationSaga(): Object {
  yield [
    takeLatest('FETCH_GPS_ADDRESS_REQUESTED', fetchFullAddress),
    takeLatest('FETCH_PROVINCE_LIST_REQUESTED', fetchProvinceList),
    takeLatest('FETCH_CITY_LIST_REQUESTED', fetchCityList),
    takeLatest('FETCH_SUBDISTRICT_LIST_REQUESTED', fetchSubDistrictList),
    takeLatest('FETCH_VILLAGE_LIST_REQUESTED', fetchVillageList),
  ];
}

export function* fetchFullAddress(action: Action): any {
  try {
    let {coordinates} = action;
    let {longitude, latitude} = coordinates;
    let APIresults = yield call(RegionAPI.getFullAddress, longitude, latitude);
    let filteredResult = APIresults.filter((result) => {
      return result.types[0] === 'route';
    })[0];
    let addressComponents = filteredResult.address_components;
    let formattedAddress = filteredResult.formatted_address;
    let province = addressComponents.filter((component) => {
      return component.types[0] === 'administrative_area_level_1';
    })[0].long_name;
    let city = addressComponents.filter((component) => {
      return component.types[0] === 'administrative_area_level_2';
    })[0].long_name;
    let subdistrict = addressComponents.filter((component) => {
      return component.types[0] === 'administrative_area_level_3';
    })[0].long_name;
    let village = addressComponents.filter((component) => {
      return component.types[0] === 'administrative_area_level_4';
    })[0].long_name;

    let gpsAddress = {
      province,
      city,
      subdistrict,
      village,
      fullAddress: formattedAddress,
      lat: latitude.toString(),
      long: longitude.toString(),
    };

    yield put({
      type: 'FETCH_GPS_ADDRESS_SUCCESS',
      gpsAddress, // TODO edit this to a state object
    });
  } catch (error) {
    yield put({type: 'FETCH_GPS_ADDRESS_FAILED', error});
  }
}

export function* fetchProvinceList(): any {
  let res = yield call(RegionAPI.getProvince);
  if (res.result === 'sukses') {
    let provinceList = formatData(res.message, 'province');
    yield put({type: 'FETCH_PROVINCE_LIST_SUCCEED', provinceList});
  } else {
    yield put({type: 'FETCH_PROVINCE_LIST_FAILED'});
  }
}

export function* fetchCityList(action: Action): any {
  let res = yield call(RegionAPI.getCities, action.provinceID);
  if (res.result === 'sukses') {
    let cityList = formatData(res.message, 'city');
    yield put({type: 'FETCH_CITY_LIST_SUCCEED', cityList});
  } else {
    yield put({type: 'FETCH_CITY_LIST_FAILED'});
  }
}

export function* fetchSubDistrictList(action: Action): any {
  let res = yield call(RegionAPI.getSubDistricts, action.cityID);
  if (res.result === 'sukses') {
    let subDistrictList = formatData(res.message, 'subdistrict');
    yield put({type: 'FETCH_SUBDISTRICT_LIST_SUCCEED', subDistrictList});
  } else {
    yield put({type: 'FETCH_SUBDISTRICT_LIST_FAILED'});
  }
}

export function* fetchVillageList(action: Action): any {
  let res = yield call(RegionAPI.getVillages, action.subDistrictID);
  if (res.result === 'sukses') {
    let villageList = formatData(res.message, 'village');
    yield put({type: 'FETCH_VILLAGE_LIST_SUCCEED', villageList});
  } else {
    yield put({type: 'FETCH_VILLAGE_LIST_FAILED'});
  }
}

export function formatData(data: Array<Object>, subAddress: SubAddress) {
  return data.map((item) => {
    return {
      value: item.id,
      label: item[subAddress + '_name'],
    };
  });
}

export function getToken(state) {
  return state.currentUser.token;
}
