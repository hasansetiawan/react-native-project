import {takeLatest, takeEvery} from 'redux-saga';
import {select, call, put} from 'redux-saga/effects';

import NearbyStoreAPI from '../API/NearbyStoreAPI';
import formatObjectKeys from '../helpers/formatObjectKeys';
import formatGallery from '../helpers/formatGallery';
import {productKeys} from './productSaga';
let storeKeys = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'store_id',
    replaceWith: 'id',
  }, {
    targetKey: 'profile_store_pic',
    replaceWith: 'storeProfilePicture',
  }, {
    targetKey: 'profile_store_background',
    replaceWith: 'storeProfileBackground',
  }, {
    targetKey: 'user_id',
    replaceWith: 'userID',
  }, {
    targetKey: 'store_name',
    replaceWith: 'name',
  }, {
    targetKey: 'store_address',
    replaceWith: 'address',
  }, {
    targetKey: 'store_owner',
    replaceWith: 'ownerName',
  }],
};
export function* watchNearbyStoreSaga(): Object {
  yield [
    takeLatest('FETCH_NEARBY_STORE_REQUESTED', fetchNearbyStore),
    takeEvery('FETCH_STORE_PRODUCTS_REQUESTED', fetchStoreProduct),
    takeLatest('FETCH_STORE_BY_PRODUCT_REQUESTED', fetchProductStore),
  ];
}

export function* fetchProductStore(action: Action) {
  let productID = yield select((state) => state.selectedProduct);
  let {lat, lon} = action;
  try {
    let result = yield call(NearbyStoreAPI.getStoresByProduct, productID, lat, lon);
    let nearestStores = [];
    for (let store of result) {
      let temp = yield call(formatObjectKeys, store, storeKeys);
      nearestStores.push(temp);
    }
    yield put({
      type: 'FETCH_STORE_BY_PRODUCT_SUCCESS',
      stores: nearestStores,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_STORE_BY_PRODUCT_FAILED',
      error,
    });
  }
}

export function* fetchStoreProduct(action: Action) {
  let token = yield select(getToken);
  let storeID = yield select((state) => state.selectedStore);
  let {page} = action;
  try {
    let products = yield call(NearbyStoreAPI.getProductsOfStore, token, storeID, page);
    let formattedProducts = [];
    for (let product of products) {
      let temp = yield call(formatObjectKeys, product, productKeys);
      temp.gallery = formatGallery(temp.gallery);
      temp.storeID = Number(temp.storeID);
      formattedProducts.push(temp);
    }
    yield put({
      type: 'FETCH_STORE_PRODUCTS_SUCCESS',
      products: formattedProducts,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_STORE_PRODUCTS_FAILED',
      error,
    });
  }
}
export function* fetchNearbyStore(action: Action) {
  try {
    let {lat, lon, page} = action;
    let token = yield select(getToken);
    let APINearbyStores = yield call(NearbyStoreAPI.getNearbyStore, token, lat, lon, page);
    let nearbyStores = new Map();
    APINearbyStores.map((nearbyStore) => {
      nearbyStores.set(nearbyStore.store_id, {
        id: nearbyStore.store_id,
        userID: nearbyStore.user_id,
        storeProfilePicture: nearbyStore.profile_store_pic,
        storeProfileBackground: nearbyStore.profile_store_background,
        name: nearbyStore.store_name,
        address: nearbyStore.store_address,
        location: {
          provinceID: nearbyStore.province,
          province: nearbyStore.province_name,
          cityID: nearbyStore.city,
          city: nearbyStore.city_name,
          subdistrictID: nearbyStore.subdistrict,
          subdistrict: nearbyStore.subdistrict_name,
          villageID: nearbyStore.village,
          village: nearbyStore.village_name,
        },
        phone: nearbyStore.phone,
        email: nearbyStore.store_email,
        ownerName: nearbyStore.store_owner,
        createdAt: new Date(nearbyStore.created_at),
        updatedAt: new Date(nearbyStore.updated_at),
        coordinate: {
          longitude: nearbyStore.longitude,
          latitude: nearbyStore.latitude,
        },
        distance: nearbyStore.distance,
      });
    });
    yield put({
      type: 'FETCH_NEARBY_STORE_SUCCEED',
      nearbyStores,
    });
  } catch (error) {
    yield put({type: 'FETCH_NEARBY_STORE_FAILED', error});
  }
}

export function getToken(state) {
  return state.currentUser.token;
}
