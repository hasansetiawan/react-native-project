// @flow
import {eventChannel} from 'redux-saga';
import {put, take, select} from 'redux-saga/effects';
import PushNotification from 'react-native-push-notification';

import type {RootState} from '../types/RootState';

type NotificationChannelData = {
  type: string;
  tokenData: {
    token: string;
    os: string;
  };
  notificationData: Object;
};

function createPushNotificationChannel() {
  return eventChannel((emitter) => {
    PushNotification.configure({
      onRegister: (tokenData) => {
        emitter({
          type: 'TOKEN',
          tokenData,
        });
      },
      onNotification: (notificationData) => {
        emitter({
          type: 'NOTIFICATION',
          notificationData,
        });
      },
      senderID: '1067936094617',
      permissions: {
        alert: true,
        badge: true,
        sound: true,
      },
      popInitialNotification: true,
      requestPermissions: true,
    });
    return () => {
      // should turn PusihNotification listening off
    };
  });
}

export default function* pushNotificationSaga(): Generator<*, *, *> {
  const channel = createPushNotificationChannel();
  try {
    for (;;) {
      // $FlowFixMe
      let data: NotificationChannelData = yield take(channel);
      if (data.type === 'TOKEN') {
        let {tokenData} = data;
        yield put({type: 'GCM_TOKEN_RECEIVED', tokenData});
        // TODO: send token to Karsa API
      }
      if (data.type === 'NOTIFICATION') {
        let {notificationData} = data;
        if (notificationData.userInteraction === false) {
          PushNotification.localNotification(notificationData);
        } else {
          // TODO: bring up page based received notification data
          let currentUser = yield select((state: RootState) => state.currentUser);
          // $FlowFixMe
          if (Object.keys(currentUser).length !== 0) {
            yield put({type: 'RESET_ROUTE', key: 'notifications'});
          }
        }
        yield put({type: 'GCM_NOTIFICATION_RECEIVED', notificationData});
      }
    }
  } finally {
    channel.close();
  }
}
