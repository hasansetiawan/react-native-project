import {takeLatest, takeEvery} from 'redux-saga';
import {call, put} from 'redux-saga/effects';

import PestDiseasesAPI from '../API/PestDiseasesAPI';
import formatObjectKeys from '../helpers/formatObjectKeys';
import type {Action} from '../types/Action';

let options = {
  isAddPrefix: false,
  isReplaceLiteral: true,
  textReplacement: [{
    targetKey: 'penyebab',
    replaceWith: 'causes',
  }, {
    targetKey: 'gejala',
    replaceWith: 'symptoms',
  }, {
    targetKey: 'pengendalian',
    replaceWith: 'treatments',
  }, {
    targetKey: 'deskripsi',
    replaceWith: 'description',
  }, {
    targetKey: 'hama',
    replaceWith: 'name',
  }, {
    targetKey: 'penyakit',
    replaceWith: 'name',
  }, {
    targetKey: 'galleries',
    replaceWith: 'gallery',
  }],
};
export function* fetchPestByPage(action): any {
  try {
    let {pageNumber} = action;
    let pests = yield call(PestDiseasesAPI.getPestsByPage, pageNumber.toString());
    let formatedPests = [];
    for (let pest of pests) {
      let formated = yield call(formatObjectKeys, pest, options);
      formated.photo = {uri: formated.photo};
      formatedPests.push(formated);
    }
    yield put({type: 'FETCH_MORE_PEST_SUCCESS', pests: formatedPests});
  } catch (error) {
    yield put({type: 'FETCH_MORE_PEST_FAILED', error: error});
  }
}
export function* fetchPestWithFilter(action): any {
  try {
    let {pageNumber, commodityID} = action;
    let pests = yield call(
        PestDiseasesAPI.filterPestDiseases,
        'hama',
        commodityID.toString(),
        pageNumber.toString(),
      );
    let formatedPests = [];
    for (let pest of pests) {
      let formated = yield call(formatObjectKeys, pest, options);
      formated.photo = {uri: formated.photo};
      formatedPests.push(formated);
    }
    yield put({type: 'FETCH_FILTERED_PEST_SUCCESS', pests: formatedPests});
  } catch (error) {
    yield put({type: 'FETCH_FILTERED_PEST_FAILED', error: error});
  }
}
export function* fetchDiseaseByPage(action): any {
  try {
    let {pageNumber} = action;
    let diseases = yield call(PestDiseasesAPI.getDiseasesByPage, pageNumber.toString());
    let formatedDiseases = [];
    for (let disease of diseases) {
      let formated = yield call(formatObjectKeys, disease, options);
      formated.photo = {uri: formated.photo};
      formatedDiseases.push(formated);
    }
    yield put({type: 'FETCH_MORE_DISEASE_SUCCESS', diseases: formatedDiseases});
  } catch (error) {
    yield put({type: 'FETCH_MORE_DISEASE_FAILED', error: error});
  }
}
export function* fetchDiseaseWithFilter(action): any {
  try {
    let {pageNumber, commodityID} = action;
    let diseases = yield call(
      PestDiseasesAPI.filterPestDiseases,
      'penyakit',
      commodityID.toString(),
      pageNumber.toString(),
    );
    let formatedDiseases = [];
    for (let disease of diseases) {
      let formated = yield call(formatObjectKeys, disease, options);
      formated.photo = {uri: formated.photo};
      formatedDiseases.push(formated);
    }
    yield put({type: 'FETCH_FILTERED_DISEASE_SUCCESS', diseases: formatedDiseases});
  } catch (error) {
    yield put({type: 'FETCH_FILTERED_DISEASE_FAILED', error: error});
  }
}
export function* fetchInitial(): any {
  try {
    let [diseases, pests, commodities] = yield [
      yield call(PestDiseasesAPI.getDiseasesByPage, 1),
      yield call(PestDiseasesAPI.getPestsByPage, 1),
      yield call(PestDiseasesAPI.getCommodityPestDiseases, 'hama'),
    ];
    let formatedDiseases = [];
    let formatedPests = [];
    for (let disease of diseases) {
      let formated = yield call(formatObjectKeys, disease, options);
      formated.photo = {uri: formated.photo};
      formatedDiseases.push(formated);
    }
    for (let pest of pests) {
      let formated = yield call(formatObjectKeys, pest, options);
      formated.photo = {uri: formated.photo};
      formatedPests.push(formated);
    }
    yield put({
      type: 'FETCH_PEST_DISEASE_SUCCESS',
      diseases: formatedDiseases,
      pests: formatedPests,
      commodities,
    });
  } catch (error) {
    yield put({type: 'FETCH_PEST_DISEASE_FAILED', error});
  }
}
export function* fetchPestDetail(action: Action) {
  let {id} = action;
  try {
    let pestDetail = yield call(PestDiseasesAPI.getPestDetail, id);
    let formatedPestDetail = yield call(formatObjectKeys, pestDetail, options);
    let galleries = [];
    for (let picture of pestDetail.galleries) {
      let photo = picture.primary ? {uri: picture.photo} : {uri: picture.photo_thumb};
      let formatedPicture = {
        photo,
        primary: picture.primary,
        photoThumb: picture.photo_thumb,
      };
      galleries.push(formatedPicture);
    }
    formatedPestDetail.gallery = galleries;
    formatedPestDetail.photo = {uri: formatedPestDetail.photo};
    yield put({
      type: 'FETCH_PEST_DETAIL_SUCCESS',
      pestDetail: formatedPestDetail,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_PEST_DETAIL_FAILED',
      error,
    });
  }
}
export function* fetchDiseaseDetail(action: Action) {
  let {id} = action;
  try {
    let diseaseDetail = yield call(PestDiseasesAPI.getDiseaseDetail, id);
    let formatedDiseaseDetail = yield call(formatObjectKeys, diseaseDetail, options);
    let galleries = [];
    for (let picture of diseaseDetail.galleries) {
      let photo = picture.primary ? {uri: picture.photo} : {uri: picture.photo_thumb};
      let formatedPicture = {
        photo,
        primary: picture.primary,
        photoThumb: picture.photo_thumb,
      };
      galleries.push(formatedPicture);
    }
    formatedDiseaseDetail.gallery = galleries;
    formatedDiseaseDetail.photo = {uri: formatedDiseaseDetail.photo};
    yield put({
      type: 'FETCH_DISEASE_DETAIL_SUCCESS',
      diseaseDetail: formatedDiseaseDetail,
    });
  } catch (error) {
    yield put({
      type: 'FETCH_DISEASE_DETAIL_FAILED',
      error,
    });
  }
}
export function* fetchSearchedPest(action: Action) {
  let {query} = action;
  try {
    let pests = yield call(PestDiseasesAPI.searchPestDisease, 'hama', query);
    let formatedPests = [];
    for (let pest of pests) {
      let formated = yield call(formatObjectKeys, pest, options);
      formated.photo = {uri: formated.photo};
      formatedPests.push(formated);
    }
    yield put({type: 'FETCH_SEARCHED_PEST_SUCCESS', pests: formatedPests});
  } catch (error) {
    yield put({
      type: 'FETCH_SEARCHED_PEST_FAILED',
      error,
    });
  }
}
export function* fetchSearchedDisease(action: Action) {
  let {query} = action;
  try {
    let diseases = yield call(PestDiseasesAPI.searchPestDisease, 'penyakit', query);
    let formatedDiseases = [];
    for (let disease of diseases) {
      let formated = yield call(formatObjectKeys, disease, options);
      formated.photo = {uri: formated.photo};
      formatedDiseases.push(formated);
    }
    yield put({type: 'FETCH_SEARCHED_DISEASE_SUCCESS', diseases: formatedDiseases});
  } catch (error) {
    yield put({
      type: 'FETCH_SEARCHED_DISEASE_FAILED',
      error,
    });
  }
}
export function* watchPestDiseaseSaga(): Object {
  yield [
    takeLatest('FETCH_MORE_DISEASE_REQUESTED', fetchDiseaseByPage),
    takeLatest('FETCH_MORE_PEST_REQUESTED', fetchPestByPage),
    takeLatest('FETCH_PEST_DISEASE_REQUESTED', fetchInitial),
    takeEvery('FETCH_FILTERED_PEST_REQUESTED', fetchPestWithFilter),
    takeEvery('FETCH_FILTERED_DISEASE_REQUESTED', fetchDiseaseWithFilter),
    takeLatest('FETCH_PEST_DETAIL_REQUESTED', fetchPestDetail),
    takeLatest('FETCH_DISEASE_DETAIL_REQUESTED', fetchDiseaseDetail),
    takeLatest('FETCH_SEARCHED_PEST_REQUESTED', fetchSearchedPest),
    takeLatest('FETCH_SEARCHED_DISEASE_REQUESTED', fetchSearchedDisease),
  ];
}
