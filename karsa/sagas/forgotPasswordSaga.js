
import {takeLatest} from 'redux-saga';
import {put, call} from 'redux-saga/effects';

import UserAPI from '../API/UserAPI';

function* forgotPasswordPost(action) {
  try {
    let {email} = action;
    yield call(UserAPI.forgotPassword, email);
    yield put({type: 'FORGOT_PASSWORD_POST_SUCCESS'});
  } catch (err) {
    yield put({type: 'FORGOT_PASSWORD_POST_FAILED', error: err});
  }
}

export function* watchForgotPasswordPostSaga(): any {
  yield takeLatest('FORGOT_PASSWORD_POST', forgotPasswordPost);
}
