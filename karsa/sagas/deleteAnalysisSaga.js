
import {takeLatest} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';

import CostAPI from '../API/CostAPI';

function getToken(state) {
  return state.currentUser.token;
}

function* deleteAnalysis(action) {
  try {
    let token = yield select(getToken);
    let {analysisID} = action;
    let result = yield call(CostAPI.deleteAnalytic, token, analysisID);
    if (result.result === 'sukses') {
      let analysis = yield call(CostAPI.getCostAnalyticTimeLine, token);
      yield put({type: 'FETCH_ANALYSIS_SUCCESS', analysis: analysis});
    } else {
      throw new Error(result.message);
    }
  } catch (err) {
    yield put({type: 'DELETE_COST_ANALYSIS_FAILED', error: err});
  }
}

export function* watchDeleteAnalysisSaga(): any {
  yield takeLatest('DELETE_ANALYSIS', deleteAnalysis);
}
