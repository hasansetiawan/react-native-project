
import {takeLatest} from 'redux-saga';
import {put, call, select} from 'redux-saga/effects';

import CostAPI from '../API/CostAPI';

function getToken(state) {
  return state.currentUser.token;
}

function* addNewAnalysis(action) {
  try {
    let token = yield select(getToken);
    let {newAnalysis} = action;
    let result = yield call(CostAPI.addNewAnalysis, token, newAnalysis);
    if (result.result === 'sukses') {
      let analysis = yield call(CostAPI.getCostAnalyticTimeLine, token);
      yield put({type: 'FETCH_ANALYSIS_SUCCESS', analysis: analysis});
    } else {
      throw new Error(result.message);
    }
  } catch (err) {
    yield put({type: 'NEW_COST_ANALYSIS_FAILED', error: err});
  }
}

export function* watchNewAnalysisSaga(): any {
  yield takeLatest('NEW_COST_ANALYSIS', addNewAnalysis);
}
