const {describe, it} = global;

import expect from 'expect';
import {mapStateToProps} from '../MarketPriceContainer';

describe('MarketPrice Container Test', () => {
  it('Should map state to props', () => {
    let state = {
      loadingIndicator: {
        isMarketPriceLoading: false,
        isProvincePriceLoading: false,
        isCommodityPriceLoading: false,
      },
      marketPrice: {
        title: '123',
        description: 'long text',
        picture: 'https://aws.cloud.somewhere/myface',
      },
    };
    expect(mapStateToProps(state)).toEqual({
      isMarketPriceLoading: false,
      isProvincePriceLoading: false,
      isCommodityPriceLoading: false,
      title: '123',
      description: 'long text',
      picture: 'https://aws.cloud.somewhere/myface',
    });
  });
});
