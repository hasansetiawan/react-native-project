const {describe, it} = global;

import {mapStateToProps} from '../NewsPageContainer';

import expect from 'expect';

describe('News Page Container test', () => {
  let state = {
    news: {
      allNews: new Map([
        ['1023', {
          id: 1,
          title: '123',
          createdAt: '123',
          author: 'John',
          foto: 'photo/from/heaven',
          highlight: false,
        }],
        ['2918', {
          id: 1,
          title: '123',
          createdAt: '123',
          author: 'John',
          foto: 'photo/from/heaven',
          highlight: true,
        }],
      ]),
    },
  };

  it('Should map state to props', () => {
    let props = mapStateToProps(state);
    expect(props).toNotEqual({});
    expect(props.news[0]).toEqual({
      id: 1,
      title: '123',
      createdAt: '123',
      author: 'John',
      foto: 'photo/from/heaven',
      highlight: false,
    });
    expect(props.newsHighlight[0]).toEqual({
      id: 1,
      title: '123',
      createdAt: '123',
      author: 'John',
      foto: 'photo/from/heaven',
      highlight: true,
    });
  });
});
