// @flow

const {describe, it} = global;
import expect from 'expect';
import {mapDispatchToProps} from '../FarmerMediaInfoPageContainer';

describe('Farmer Media Info Page Container', () => {
  it('should dispatch PUSH_ROUTE with route name', () => {
    let dispatchSpy = expect.createSpy();
    let props = mapDispatchToProps(dispatchSpy);
    props.navigateTo('foo');
    expect(dispatchSpy.calls.length).toBe(1);
    expect(dispatchSpy.calls[0].arguments[0]).toEqual({
      type: 'PUSH_ROUTE',
      key: 'foo',
    });
  });
});
