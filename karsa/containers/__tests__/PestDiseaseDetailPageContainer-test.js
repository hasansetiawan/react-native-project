const {describe, it} = global;

import expect from 'expect';
import {mapStateToProps} from '../PestDiseaseDetailPageContainer';
import pestImage1 from '../images/karsa-01.png';
import diseaseImage1 from '../images/karsa-02.png';

const pest = {
  id: 123,
  type: 'pest',
  photo: pestImage1,
  name: 'Hama Walang Sangit',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem enim, tristique quis maximus non, rhoncus in erat. Sed tempus neque a urna mollis, facilisis.',
  causes: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit.',
  symptoms: [
    'Aliquam a nisl finibus, congue augue fermentum, congue lacus.',
    'Cras venenatis dolor vestibulum eros rutrum, sit amet venenatis leo aliquam.',
    'Fusce accumsan augue a bibendum bibendum.',
    'Curabitur tristique massa sed quam placerat consectetur eget eu urna.',
    'Nam at quam pulvinar, mattis libero sit amet, posuere dolor.',
    'Nulla vitae felis ac sem venenatis dictum.',
  ],
  treatments: [
    'Integer dapibus purus auctor tempus eleifend.',
    'Phasellus volutpat purus eget molestie finibus.',
    'Mauris nec augue a ex consectetur auctor ut eu massa.',
    'Duis sed mauris pulvinar, efficitur libero non, cursus enim.',
    'Cras sit amet est in sem volutpat mollis quis ut erat.',
    'Vestibulum scelerisque dolor ac libero fermentum euismod.',
  ],
};
const disease = {
  id: 271,
  type: 'disease',
  photo: diseaseImage1,
  name: 'Penyakit Blas',
  description: 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nulla sem enim, tristique quis maximus non, rhoncus in erat. Sed tempus neque a urna mollis, facilisis.',
  causes: 'Lorem ipsum dolor sit amet',
  symptoms: [
    'Aliquam a nisl finibus, congue augue fermentum, congue lacus.',
    'Cras venenatis dolor vestibulum eros rutrum, sit amet venenatis leo aliquam.',
    'Fusce accumsan augue a bibendum bibendum.',
    'Curabitur tristique massa sed quam placerat consectetur eget eu urna.',
    'Nam at quam pulvinar, mattis libero sit amet, posuere dolor.',
    'Nulla vitae felis ac sem venenatis dictum.',
  ],
  treatments: [
    'Integer dapibus purus auctor tempus eleifend.',
    'Phasellus volutpat purus eget molestie finibus.',
    'Mauris nec augue a ex consectetur auctor ut eu massa.',
    'Duis sed mauris pulvinar, efficitur libero non, cursus enim.',
    'Cras sit amet est in sem volutpat mollis quis ut erat.',
    'Vestibulum scelerisque dolor ac libero fermentum euismod.',
  ],
};

let pests = new Map();
pests.set(123, pest);
pests.set(123, {
  ...pest,
  gallery: [],
  countermeasures: [],
});

let diseases = new Map();
diseases.set(234, disease);
diseases.set(234, {
  ...disease,
  gallery: [],
  countermeasures: [],
});

let selectedPestDisease = {
  type: 'pest',
  id: 123,
};

let mockState = {
  selectedPestDisease,
  pests,
  diseases,
  products: [],
  loadingIndicator: {
    isPestDiseaseDetailLoading: false,
  },
};

describe('Pest Disease Detail Page Container', () => {
  it('should return the correct data for Pest Disease Detail Page', () => {
    let actual = mapStateToProps(mockState);
    let expected = {detail: pests.get(123), isPestDiseaseDetailLoading: false};
    expect(actual).toEqual(expected);

    selectedPestDisease = {
      type: 'disease',
      id: 234,
    };
    mockState = {
      ...mockState,
      selectedPestDisease,
    };

    actual = mapStateToProps(mockState);
    expected = {detail: diseases.get(234), isPestDiseaseDetailLoading: false};
    expect(actual).toEqual(expected);
  });
});
