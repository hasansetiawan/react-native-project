import expect from 'expect';

import {mapStateToProps} from '../CostAnalysisPageContainer';

const {describe, it} = global;

describe('Cost Analysis Page Container Test', () => {
  let mockState = {
    analysis: [],
  };
  mockState.analysis.push({
    userID: 4,
    title: 'Perhitungan Percobaan 1',
    farmingProductionCost: 1000000,
    seedCost: 1000000,
    fertilizerCost: 1000000,
    labourCost: 1000000,
    totalExpense: 100000,
    firstHarvest: '1000 kg',
    endHarvest: '900 kg',
    depreciation: '100 kg',
    depreciationPercent: '10%',
    price: 40000,
    grossIncome: '36000000',
    netIncome: '32000000',
    createdAt: '22/10/2015',
    plantID: 1,
  });
  it('should map State To Props', () => {
    let result = mapStateToProps(mockState);
    expect(result).toNotBe(null);
    expect(result).toEqual({
      costsAnalysis: [{
        userID: 4,
        title: 'Perhitungan Percobaan 1',
        farmingProductionCost: 1000000,
        seedCost: 1000000,
        fertilizerCost: 1000000,
        labourCost: 1000000,
        totalExpense: 100000,
        firstHarvest: '1000 kg',
        endHarvest: '900 kg',
        depreciation: '100 kg',
        depreciationPercent: '10%',
        price: 40000,
        grossIncome: '36000000',
        netIncome: '32000000',
        createdAt: '22/10/2015',
        plantID: 1,
      }],
    });
  });
});
