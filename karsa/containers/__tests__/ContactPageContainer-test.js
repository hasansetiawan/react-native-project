import expect from 'expect';

import {mapStateToProps, mapDispatchToProps} from '../ContactPageContainer';

const {describe, it} = global;

describe('Contact Page Container test', () => {
  let state = {
    loadingIndicator: {
      isFetchContactLoading: true,
      isSubmitEmailLoading: false,
    },
    contactUs: {
      mainOffice: {
        name: 'Kantor Utama',
        address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
        city: 'Jakarta',
        phone: '(021) 2083633',
        email: 'info@qarsa.com',
      },
      branchOffices: [
        {
          name: 'Kantor Cabang Bandung',
          address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
          city: 'Bandung',
          phone: '(022) 23130123',
          email: 'info@qarsa.com',
        },
        {
          name: 'Kantor Cabang Surabaya',
          address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
          city: 'Surabaya',
          phone: '(025) 304082340',
          email: 'info@qarsa.com',
        },
      ],
    },
  };
  it('Should map state to props', () => {
    let props = mapStateToProps(state);
    expect(props.mainOffice).toEqual({
      name: 'Kantor Utama',
      address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
      city: 'Jakarta',
      phone: '(021) 2083633',
      email: 'info@qarsa.com',
    });
    expect(Array.isArray(props.branchOffices)).toBe(true);
    expect(props.branchOffices.length).toBe(2);
    expect(props.branchOffices).toEqual([
      {
        name: 'Kantor Cabang Bandung',
        address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
        city: 'Bandung',
        phone: '(022) 23130123',
        email: 'info@qarsa.com',
      },
      {
        name: 'Kantor Cabang Surabaya',
        address: 'Komplek Ruko Pluit Junction SH-03, Jl. Pluit Raya No.1 Jakarta Utara, Indonesia 14440',
        city: 'Surabaya',
        phone: '(025) 304082340',
        email: 'info@qarsa.com',
      },
    ]);
  });
  it('Should map dispatch to props', () => {
    let spy = expect.createSpy();
    let emailMessage = {
      name: 'john',
      email: 'because@karsa.is.the.best',
      phone: '0123123123',
      message: 'why karsa is the best??',
    };
    let {onEmailSubmit} = mapDispatchToProps(spy);
    onEmailSubmit(emailMessage);
    expect(spy.calls.length).toBe(1);
    expect(spy.calls[0].arguments[0]).toEqual({
      type: 'SUGGESTION_SUBMISSION_REQUESTED',
      emailMessage: emailMessage,
    });
  });
});
