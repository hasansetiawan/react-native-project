let {describe, it} = global;

import expect from 'expect';
import {mapStateToProps} from '../EditProfilePageContainer';

describe('EditProfileContainer', () => {
  it('Should map state to props', () => {
    let state = {
      tempProfile: {
        name: 'John Doe',
        profilePicture: 'https://dummyavatar.com/myavatar',
        gender: 'male',
        address: 'Jl. Karsa, Kebumen',
        phone: '02519232494',
      },
    };
    expect(mapStateToProps(state)).toEqual({
      tempProfile: {
        name: 'John Doe',
        profilePicture: 'https://dummyavatar.com/myavatar',
        gender: 'male',
        address: 'Jl. Karsa, Kebumen',
        phone: '02519232494',
      },
    });
  });
});
