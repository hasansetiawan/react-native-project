//
// import {mapStateToProps} from '../AnalysisDetailContainer';
// import expect from 'expect';
//
// const {describe, it} = global;
//
// describe('Analysis Detail Container Test', () => {
//   let state = {
//     selectedPlantCost: 1,
//     plants: new Map(),
//     plantCosts: [],
//   };
//   state.plants.set(1, {
//     plantID: 1,
//     plantName: 'padi',
//   });
//   state.plantCosts.push({
//     id: 1,
//     userID: 4,
//     title: 'Perhitungan Percobaan 1',
//     farmingProductionCost: 1000000,
//     seedCost: 1000000,
//     fertilizerCost: 1000000,
//     labourCost: 1000000,
//     firstHarvest: '1000 kg',
//     endHarvest: '900 kg',
//     depreciation: '100 kg',
//     depreciationPercent: '10%',
//     price: 40000,
//     grossIncome: '36000000',
//     netIncome: '32000000',
//     createdAt: '22/10/2015',
//     plantID: 1,
//   });
//   it('Should mapStateToProps', () => {
//     let props = mapStateToProps(state);
//     expect(props).toNotEqual({});
//     expect(props).toEqual({
//       id: 1,
//       userID: 4,
//       title: 'Perhitungan Percobaan 1',
//       farmingProductionCost: 1000000,
//       seedCost: 1000000,
//       fertilizerCost: 1000000,
//       labourCost: 1000000,
//       firstHarvest: '1000 kg',
//       endHarvest: '900 kg',
//       depreciation: '100 kg',
//       depreciationPercent: '10%',
//       price: 40000,
//       grossIncome: '36000000',
//       netIncome: '32000000',
//       createdAt: '22/10/2015',
//       plantID: 1,
//       plantName: 'padi',
//     });
//   });
// });
