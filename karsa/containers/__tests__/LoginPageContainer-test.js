// @flow

const {describe, it} = global;

import expect from 'expect';

import {mapDispatchToProps} from '../LoginPageContainer';

describe('LoginPageContainer', () => {
  it('Should dispatch a user object', () => {
    let dispatchSpy = expect.createSpy();
    let props = mapDispatchToProps(dispatchSpy);
    props.onLoginSubmit({email: '123', password: '123'});
    expect(dispatchSpy.calls.length).toBe(1);
    // expect(dispatchSpy.calls[0].arguments[0]).toEqual({
    //   type: 'LOGIN',
    //   userAuth: {
    //     email: '123',
    //     password: '123',
    //   },
    // });
  });
});
