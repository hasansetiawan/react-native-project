const {describe, it} = global;

import expect from 'expect';
import {mapStateToProps} from '../AboutContainer';

describe('About Container Test', () => {
  it('Should map state to props', () => {
    let state = {
      about: {
        title: '123',
        description: 'long text',
        urlPicture: 'https://aws.cloud.somewhere/myface',
      },
    };
    expect(mapStateToProps(state)).toEqual({
      title: '123',
      description: 'long text',
      image: {uri: 'https://aws.cloud.somewhere/myface'},
    });
  });
});
