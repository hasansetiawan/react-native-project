// @flow

import {connect} from 'react-redux';

import CultivationPage from '../pages/CultivationPage';
import type {Dispatch} from '../types/Store';

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateToWeeklyGuidance() {
      dispatch({type: 'PUSH_ROUTE', key: 'weeklyGuidance'});
    },
    navigateToPlantingGuidance() {
      dispatch({type: 'PUSH_ROUTE', key: 'plantingGuidance'});
    },
    fetchPlantingGuides() {
      dispatch({type: 'FETCH_PLANTING_GUIDES_REQUESTED'});
    },
  };
}

export default connect(null, mapDispatchToProps)(CultivationPage);
