// @flow
import {connect} from 'react-redux';
import ImagePicker from 'react-native-image-picker';

import EditProfilePage from '../pages/EditProfilePage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {tempProfile} = state;
  return {
    tempProfile: {
      ...tempProfile,
    },
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onSelect(subAddress: string) {
      return (value: string) => {
        dispatch({
          type: 'SUB_ADDRESS_SELECTED',
          subAddress,
          value,
        });
      };
    },
    onSelectGender(gender: string | number) {
      dispatch({
        type: 'EDIT_PROFILE_CHANGED',
        fieldName: 'gender',
        fieldValue: gender === '1' ? 'Laki-laki' : 'Perempuan',
      });
    },
    onAddressChange(fullAddress: string) {
      dispatch({
        type: 'EDIT_PROFILE_CHANGED',
        fieldName: 'fullAddress',
        fieldValue: fullAddress,
      });
    },
    onPhoneInput(noHp: string) {
      dispatch({
        type: 'EDIT_PROFILE_CHANGED',
        fieldName: 'noHp',
        fieldValue: noHp,
      });
    },
    onNameChange(name: string) {
      dispatch({
        type: 'EDIT_PROFILE_CHANGED',
        fieldName: 'name',
        fieldValue: name,
      });
    },
    onSave() {
      dispatch({
        type: 'POST_EDIT_PROFILE',
      });
      dispatch({
        type: 'POP_ROUTE',
      });
    },
    onCancel() {
      dispatch({
        type: 'EDIT_PROFILE_CANCELED',
      });
    },
    onPicturePress() {
      ImagePicker.showImagePicker(null, (image) => {
        if (!image.didCancel) {
          dispatch({type: 'CHANGE_AVATAR', image});
        }
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(EditProfilePage);
