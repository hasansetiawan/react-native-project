// @flow

import {connect} from 'react-redux';
import type {Dispatch} from '../types/Store';
import AnalysisDetail from '../pages/AnalysisDetail';

import type {RootState} from '../types/RootState';

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateToEditAnalysis(id: number) {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'analysisEdit',
      });
      dispatch({
        type: 'SELECTED_ANALYSIS',
        selectedAnalysis: id,
      });
    },
    deleteAnalysis(id: string) {
      dispatch({
        type: 'POP_ROUTE',
      });
      dispatch({
        type: 'DELETE_ANALYSIS',
        analysisID: id,
      });
    },
  };
}

export function mapStateToProps(state: RootState) {
  let {analysis, selectedAnalysis} = state;
  let plantCost = analysis.get(selectedAnalysis);
  if (plantCost == null) {
    return {};
  } else {
    return {
      plantCost: plantCost,
      selectedAnalysis: selectedAnalysis,
    };
  }
}
// TODO Add dispatch for title bar
export default connect(mapStateToProps, mapDispatchToProps)(AnalysisDetail);
