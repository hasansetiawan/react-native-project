// @flow
import {connect} from 'react-redux';
import NewsPage from '../pages/NewsPage';

import type {NewsState} from '../types/News';
import type {Dispatch} from '../types/Store';

export function mapStateToProps({news}: {news: NewsState}) {
  let newsArray = Array.from(news.allNews.values());
  let newsHighlight = newsArray.filter((news) => news.highlight);
  let regularNews = newsArray.filter((news) => !news.highlight);
  return {
    news: regularNews,
    newsHighlight,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onNewsSelect(id: number) {
      dispatch({
        type: 'NEWS_SELECTED',
        id,
      });
    },
    onHighlightSelect(id: number) {
      dispatch({
        type: 'NEWS_HIGHLIGHT_SELECTED',
        id,
      });
    },
    fetchMoreNews(page: number) {
      dispatch({
        type: 'FETCH_MORE_NEWS',
        page: page + 1,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsPage);
