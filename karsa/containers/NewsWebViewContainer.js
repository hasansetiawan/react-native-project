//@flow
import {connect} from 'react-redux';
import NewsWebView from '../pages/NewsWebView';

import type {NewsState} from '../types/News';

export function mapStateToProps({news}: {news: NewsState}) {
  let {selectedNewsID, allNews} = news;
  let selectedNews = allNews.get(selectedNewsID);
  let newsURL = selectedNews ? selectedNews.url : '';
  let title = selectedNews ? selectedNews.title : '';
  return {
    newsURL,
    title,
  };
}

export function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsWebView);
