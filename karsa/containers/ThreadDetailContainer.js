// @flow

import {connect} from 'react-redux';

import ThreadDetail from '../pages/ThreadDetail';
import type {Thread} from '../types/Forum';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {threads, selectedThreadID, loadingIndicator, currentUser} = state;
  let {
    isLikeThreadLoading, isLikeCommentLoading, isPostCommentLoading,
    isThreadCommentsLoading, isThreadDetailLoading, isReportForumLoading,
  } = loadingIndicator;
  let selectedThread = threads.get(selectedThreadID);
  return {
    currentUser,
    selectedThreadID,
    thread: selectedThread,
    isThreadDetailLoading,
    isLikeThreadLoading,
    isLikeCommentLoading,
    isPostCommentLoading,
    isThreadCommentsLoading,
    isReportForumLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLikeThread(threadID: number) {
      dispatch({
        type: 'LIKE_THREAD_REQUESTED',
        threadID,
      });
    },
    onLikeComment(commentID: number) {
      dispatch({
        type: 'LIKE_COMMENT_REQUESTED',
        commentID,
      });
    },
    onPostComment(threadID: number, post: string) {
      dispatch({
        type: 'POST_COMMENT_REQUESTED',
        threadID,
        post,
      });
    },
    onFetchComments(threadID: number, page: number) {
      dispatch({
        type: 'FETCH_COMMENTS_REQUESTED',
        threadID,
        page,
      });
    },
    onEditThread(thread: Thread) {
      dispatch({type: 'FETCH_FORUM_CATEGORIES_REQUESTED'});
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'editThread',
        props: {thread},
      });
    },
    onDeleteThread(threadID: number, forum: 'user' | 'other') {
      dispatch({
        type: 'DELETE_THREAD_REQUESTED',
        threadID,
        forum,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ThreadDetail);
