// @flow

import {connect} from 'react-redux';

import ProductDetailPage from '../pages/ProductDetailPage';

import type {Dispatch} from '../types/Store';
import type {RootState} from '../types/RootState';

export function mapStateToProps(state: RootState) {
  let {selectedProduct, loadingIndicator, products, plants} = state;
  let productDetail = products.get(selectedProduct);
  let plantArray = [];
  for (let plant of plants) {
    if (plant[1].status !== 'use') {
      plantArray.push(plant[1]);
    }
  }
  let {isProductDetailLoading, isLoadMoreReviewLoading} = loadingIndicator;
  return {
    product: productDetail,
    isProductDetailLoading,
    isLoadMoreReviewLoading,
    plants: plantArray,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLoadMoreReview(page: number) {
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page,
      });
    },
    onUseProductPress(plantData: {productID: number; plantList: Array<any>}) {
      if (plantData.plantList.length > 0) {
        dispatch({
          type: 'POST_USE_PRODUCT_REQUESTED',
          plantData,
        });
      }
    },
    onReviewSubmit(review: {rate: number; review: string}) {
      dispatch({
        type: 'POST_REVIEW_REQUESTED',
        review,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductDetailPage);
