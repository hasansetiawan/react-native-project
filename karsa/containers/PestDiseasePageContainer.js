// @flow

import {connect} from 'react-redux';
import PestDiseasePage from '../pages/PestDiseasePage';
import {
  getFirstStagePests,
  getFirstStageDiseases,
} from '../selectors/getFirstStagePestsDiseases';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {pestDiseaseCommodities} = state;
  let {isFetchingMorePestData, isFetchingMoreDiseaseData, isPestDiseaseLoading} = state.loadingIndicator;
  let commodities = [];
  for (let commodity of pestDiseaseCommodities) {
    commodities.push(commodity[1]);
  }
  let pests = getFirstStagePests(state);
  let diseases = getFirstStageDiseases(state);
  return {
    commodities,
    pests,
    diseases,
    isFetchingMorePestData,
    isFetchingMoreDiseaseData,
    isPestDiseaseLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    selectedPestCommodityChanged() {
      dispatch({
        type: 'PEST_COMMODITY_SELECTED',
      });
    },
    selectedDiseaseCommodityChanged() {
      dispatch({
        type: 'DISEASE_COMMODITY_SELECTED',
      });
    },
    fetchMorePestData(page: number, filterCommodity?: string | number) {
      if (filterCommodity === -1) {
        dispatch({
          type: 'FETCH_MORE_PEST_REQUESTED',
          pageNumber: page,
        });
      } else {
        dispatch({
          type: 'FETCH_FILTERED_PEST_REQUESTED',
          pageNumber: page,
          commodityID: filterCommodity,
        });
      }
    },
    fetchMoreDiseaseData(page: number, filterCommodity?: string | number) {
      if (filterCommodity === -1) {
        dispatch({
          type: 'FETCH_MORE_DISEASE_REQUESTED',
          pageNumber: page,
        });
      } else {
        dispatch({
          type: 'FETCH_FILTERED_DISEASE_REQUESTED',
          pageNumber: page,
          commodityID: filterCommodity,
        });
      }
    },
    navigateToPestDetail(pestID: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'pestDiseaseDetail'});
      dispatch({
        type: 'FETCH_PEST_DETAIL_REQUESTED',
        id: pestID,
      });
      dispatch({
        type: 'FETCH_PRODUCTS_PEST_REQUESTED',
        pestID: pestID,
      });
    },
    navigateToDiseaseDetail(diseaseID: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'pestDiseaseDetail'});
      dispatch({
        type: 'FETCH_DISEASE_DETAIL_REQUESTED',
        id: diseaseID,
      });
      dispatch({
        type: 'FETCH_PRODUCTS_DISEASE_REQUESTED',
        diseaseID,
      });
    },
    searchPest(query: string) {
      dispatch({
        type: 'FETCH_SEARCHED_PEST_REQUESTED',
        query,
      });
    },
    searchDisease(query: string) {
      dispatch({
        type: 'FETCH_SEARCHED_DISEASE_REQUESTED',
        query,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PestDiseasePage);
