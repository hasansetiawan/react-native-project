// @flow

import {connect} from 'react-redux';
import type {Dispatch} from '../types/Store';
import type {RootState} from '../types/RootState';
import type {EmailMessage} from '../types/Office';

import ContactPage from '../pages/ContactPage';

export function mapStateToProps(state: RootState) {
  let {branchOffices, mainOffice} = state.contactUs;
  let {isFetchContactLoading, isSubmitEmailLoading} = state.loadingIndicator;
  return {
    branchOffices,
    mainOffice,
    isFetchContactLoading,
    isSubmitEmailLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    getContactData() {
      dispatch({type: 'FETCH_CONTACT_DATA_REQUESTED'});
    },
    onEmailSubmit(emailMessage: EmailMessage) {
      dispatch({
        type: 'SUGGESTION_SUBMISSION_REQUESTED',
        emailMessage,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactPage);
