// @flow

import {connect} from 'react-redux';
import type {RootState} from '../types/RootState';
import UpdatePasswordPage from '../pages/UpdatePassword';

type PasswordData = {
  oldPassword: string;
  newPassword: string;
};

import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  return {
    isLoading: state.loadingIndicator.isUpdatePaswordLoading,
    isSuccess: state.loadingIndicator.isUpdatePaswordSuccess,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onPressCancel() {
      dispatch({type: 'POP_ROUTE'});
    },
    onPressSave(passwordDataForm: PasswordData) {
      let passwordData = {
        password_baru: passwordDataForm.newPassword,
        password_lama: passwordDataForm.oldPassword,
      };
      dispatch({type: 'UPDATE_PASSWORD_POST', passwordData});
    },
    onAlertClick() {
      dispatch({type: 'UPDATE_PASSWORD'});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UpdatePasswordPage);
