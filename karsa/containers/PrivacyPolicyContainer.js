// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import PrivacyPolicy from '../pages/PrivacyPolicy';

export function mapStateToProps(state: RootState) {
  let {privacypolicy} = state;
  let {desc} = privacypolicy;
  return {
    desc,
  };
}

export function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(PrivacyPolicy);
