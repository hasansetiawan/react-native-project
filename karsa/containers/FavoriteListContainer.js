// @flow
import {connect} from 'react-redux';

import FavoriteList from '../core-ui/FavoriteList';

const mapStateToProps = (state) => {
  return {
    favorites: state.favorite.favorites,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onFavoriteSelect: (favorite) => {
      favorite.actions.forEach((action) => dispatch(action));
    },
    onFavoriteDelete: (favoriteID) => {
      dispatch({type: 'DELETE_FAVORITE_REQUESTED', favoriteID});
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(FavoriteList);
