import {AsyncStorage} from 'react-native';
import {connect} from 'react-redux';
import WalkthroughPage from '../pages/WalkthroughPage';
import type {RootState} from '../types/RootState';

let slideImage = require('../images/walkthrough_slide.jpg');

const mapStateToProps = (state: RootState) => { // eslint-disable-line
  return {
    slides: [{
      image: slideImage,
      title: 'Cermati Panduan Pertanian Anda',
      description: 'Kami menyediakan panduan perminggu yang akan membantu untuk meningkatkan hasil tanaman anda',
    }, {
      image: slideImage,
      title: 'Dapatkan Informasi Produk Terkini',
      description: 'Pilih produk favorit anda dari yang terbaru hingga terpopuler',
    }, {
      image: slideImage,
      title: 'Harga Komoditas Pasar',
      description: 'Dapatkan Informasi harga komoditas pasar yang selalu terbaru setiap hari secara kota dan nasional',
    }, {
      image: slideImage,
      title: 'Informasi Berita Terbaru Tentang Pertanian Dari Seluruh Penjuru Nusantara',
      description: 'Berita teraktual setiap hari hadir untuk memperluas wawasan anda mengenai dunia pertanian Indonesia',
    }, {
      image: slideImage,
      title: 'Informasi Cuaca',
      description: 'Dapatkan informasi cuaca yang berguna untuk kegiatan pertanian anda hari ini',
    }, {
      image: slideImage,
      title: 'Informasi Jual Beli hasil bumi anda disini',
      description: 'Anda dapat membuat forum untuk saling memasarkan hasil bumi anda langsung pada pembeli',
    }],
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    onSkip: () => {
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
      AsyncStorage.setItem('@walkthrough:show', 'yes');
    },
    onStart: () => {
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
      AsyncStorage.setItem('@walkthrough:show', 'yes');
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(WalkthroughPage);
