// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import About from '../pages/About';

export function mapDispatchToProps() {
  return {};
}

export function mapStateToProps(state: RootState) {
  let {about} = state;
  let {title, description, urlPicture} = about;
  return {
    title,
    description,
    image: {uri: urlPicture},
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(About);
