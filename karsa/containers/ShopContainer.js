// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import MyStore from '../pages/MyStoreView';

export function mapStateToProps(state: RootState) {
  let {shops} = state;
  return {
    shops: Array.from(shops),
  };
}
export function mapDispatchToProps() {
  return {};
}
export default connect(mapStateToProps, {})(MyStore);
