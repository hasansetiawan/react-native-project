// @flow
import {connect} from 'react-redux';
import PlantingGuidance from '../pages/PlantingGuidance';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {plantingGuide, loadingIndicator} = state;
  let {plantingGuides} = plantingGuide;
  let {isPlantingGuidesLoading} = loadingIndicator;
  return {
    isPlantingGuidesLoading,
    plantingGuides: plantingGuides ? plantingGuides : [],
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateToPlantingGuidanceDetail(plantDetailNames: Array<string>) {
      dispatch({type: 'PUSH_ROUTE', key: 'plantingGuidanceDetail', props: {
        plantDetailNames,
      }});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlantingGuidance);
