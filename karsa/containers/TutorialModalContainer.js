// @flow
import {connect} from 'react-redux';

import TutorialModal from '../components/TutorialModal';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';


export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onRequestClose() {
      dispatch({type: 'HIDE_TUTORIAL_MODAL'});
    },
    onAddPlantPress() {
      dispatch({type: 'RESET_ROUTE', key: 'homepage'});
      dispatch({type: 'CLEAR_TEMPORARY_PHOTO'});
      dispatch({type: 'FETCH_PLANT_TYPES_REQUESTED'});
      dispatch({type: 'PUSH_ROUTE', key: 'newPlant'});
      dispatch({type: 'HIDE_TUTORIAL_MODAL'});
    },
  };
}

export function mapStateToProps(state: RootState) {
  let {isShown} = state.tutorialModal;
  return {
    isShown,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(TutorialModal);
