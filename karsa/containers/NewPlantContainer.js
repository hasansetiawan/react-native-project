// @flow

import {connect} from 'react-redux';
import type {Dispatch} from '../types/Store';
import NewPlantPage from '../pages/NewPlantPage';
import ImagePicker from 'react-native-image-picker';

import type {RootState} from '../types/RootState';
import type {PlantOptions} from '../types/NewPlant';
import type {Coordinates} from '../types/Location';
import type {PlantForm} from '../types/Plant';

const DROPDOWN_OPTIONS = {
  fieldAltitudeOptions: [
    {value: 'rendah', label: 'Rendah (0-500 MDPL)'},
    {value: 'sedang', label: 'Sedang (500-800 MDPL)'},
    {value: 'tinggi', label: 'Tinggi (>800 MDPL)'},
  ],
  plantingPhaseOptions: [
    {value: 'Perlakuan Benih', label: 'Perlakuan Benih'},
    {value: 'Pengolahan Lahan', label: 'Pengolahan Lahan'},
    {value: 'Persemaian', label: 'Persemaian'},
    {value: 'Pindah Tanam', label: 'Pindah Tanam'},
  ],
  soilPHOptions: [
    {value: '4,6', label: '4,6'},
    {value: '4,8', label: '4,8'},
    {value: '5,0', label: '5,0'},
    {value: '5,2', label: '5,2'},
    {value: '5,4', label: '5,4'},
    {value: '5,6', label: '5,6'},
    {value: '5,8', label: '5,8'},
    {value: '6,0', label: '6,0'},
    {value: '6,1', label: '6,1 - 6,4'},
    {value: '6,5', label: '6,5 - 7'},
    {value: '7,5', label: '7,5 - 8,5'},
  ],
};

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    dropdownOnSelect: {
      onPlantTypeSelect(plantType: PlantOptions) {
        dispatch({type: 'FETCH_PLANT_CATEGORIES_REQUESTED', plantType});
      },
      onPlantCategorySelect(plantType: PlantOptions, plantCategory: PlantOptions) {
        dispatch({type: 'FETCH_PLANT_VARIETIES_REQUESTED', plantType, plantCategory});
        dispatch({type: 'FETCH_PLANTING_METHODS_REQUESTED', plantType, plantCategory});
      },
    },
    onDismissPage() {
      dispatch({type: 'POP_ROUTE'});
    },
    fetchPlantingRecommendation(plantType: PlantOptions, plantCategory: PlantOptions, plantVariant: PlantOptions, fieldAltitude: PlantOptions) {
      dispatch({
        type: 'FETCH_PLANTING_RECOMMENDATION_REQUESTED',
        plantType,
        plantCategory,
        plantVariant,
        fieldAltitude,
      });
    },
    fetchGPSAddress(coordinates: Coordinates) {
      dispatch({type: 'FETCH_GPS_ADDRESS_REQUESTED', coordinates});
    },
    fetchProvinceList() {
      dispatch({type: 'FETCH_PROVINCE_LIST_REQUESTED'});
    },
    onPictureSelect() {
      ImagePicker.showImagePicker(null, (image) => {
        if (!image.didCancel) {
          dispatch({type: 'PICK_PHOTO', image});
        }
      });
    },
    submitNewPlant(plant: PlantForm) {
      dispatch({type: 'SUBMIT_NEW_PLANT_REQUESTED', plant});
    },
  };
}

export function mapStateToProps(state: RootState) {
  let {newPlant, location, addPhoto, currentUser} = state;
  let {plantTypes, plantCategories, plantVarieties, plantingMethods, plantingRecommendation} = newPlant;
  let {gpsAddress, provinceList, cityList, subDistrictList, villageList} = location;

  return {
    dropdownOptions: {
      ...DROPDOWN_OPTIONS,
      plantTypesOptions: plantTypes ? plantTypes : null,
      plantCategoriesOptions: plantCategories ? plantCategories : null,
      plantVarietiesOptions: plantVarieties ? plantVarieties : null,
      plantingMethodsOptions: plantingMethods ? plantingMethods : null,
      provinceOptions: provinceList ? provinceList : null,
      cityOptions: cityList ? cityList : null,
      subDistrictOptions: subDistrictList ? subDistrictList : null,
      villageOptions: villageList ? villageList : null,
    },
    plantingRecommendation,
    gpsAddress: (gpsAddress) ? gpsAddress : {},
    newPhoto: addPhoto,
    userEmail: currentUser.email,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewPlantPage);
