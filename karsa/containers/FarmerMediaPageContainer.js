// @flow

import {connect} from 'react-redux';

import FarmerMediaPage from '../pages/FarmerMediaPage';
import type {Dispatch} from '../types/Store';
import type {Coordinates} from '../types/Location';

export function mapStateToProps() {
  return {};
}
export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateToPestDisease() {
      dispatch({type: 'PUSH_ROUTE', key: 'pestDisease'});
      dispatch({type: 'FETCH_PEST_DISEASE_REQUESTED'});
    },
    navigateToFarmerMediaInfo() {
      dispatch({type: 'PUSH_ROUTE', key: 'farmerMediaInfo'});
    },
    navigateToNearbyStore() {
      dispatch({type: 'PUSH_ROUTE', key: 'nearbyStore'});
    },
    fetchNearbyStore(lat: number, lon: number, page: number) {
      dispatch({type: 'FETCH_NEARBY_STORE_REQUESTED', lat, lon, page});
    },
    fetchGPSAddress(coordinates: Coordinates) {
      dispatch({type: 'FETCH_GPS_ADDRESS_REQUESTED', coordinates});
    },
    onMenuPress() {
      dispatch({
        type: 'OPEN_DRAWER',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(FarmerMediaPage);
