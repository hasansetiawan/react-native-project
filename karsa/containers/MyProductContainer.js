//@flow
import {connect} from 'react-redux';
import MyProducts from '../pages/MyProducts';
import type {RootState} from '../types/RootState';

function mapStateToProps(state: RootState) {
  let {products} = state;
  return {
    products: products,
  };
}

function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(MyProducts);
