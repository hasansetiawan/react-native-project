// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';
import LoginPage from '../pages/LoginPage';

type UserLogin = {
  email: string;
  password: string;
};

export function mapStateToProps(state: RootState) {
  return {
    isLoading: state.loadingIndicator.isLoginLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onLoginSubmit(user: UserLogin) { // eslint-disable-line no-unused-vars
      user = {
        email: user.email.trim(),
        password: user.password,
      };
      dispatch({type: 'LOGIN', userAuth: user});
      // dispatch({type: 'RESET_ROUTE', key: 'homepage'}); // Temporary for dismiss login page
    },
    onPressForgot() { // eslint-disable-line no-unused-vars
      dispatch({type: 'PUSH_ROUTE', key: 'forgotPassword'});
    },
    onFacebookLoginButtonPress() {
      dispatch({type: 'FACEBOOK_LOGIN'});
    },
    onGuestLoginButtonPress() {
      dispatch({type: 'GUEST_LOGIN'});
      // TODO: change NEWS to HIGHLIGHTED
      dispatch({type: 'FETCH_NEWS_REQUESTED'});
    },
  };
}
// TODO add dispatch to props for title bar
export default connect(mapStateToProps, mapDispatchToProps)(LoginPage);
