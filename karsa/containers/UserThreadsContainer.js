// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import UserThreadsPage from '../pages/UserThreadsPage';
import type {Dispatch} from '../types/Store';

type ShouldGoToCommentsProps = {
  enterByPressingComment: boolean;
};

export function mapStateToProps(state: RootState) {
  let {threads, loadingIndicator, currentUser} = state;
  return {
    currentUser,
    threads,
    isThreadLoading: loadingIndicator.isThreadLoading,
    isLikeThreadLoading: loadingIndicator.isLikeThreadLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchUserThreads(page: number) {
      dispatch({
        type: 'FETCH_USER_THREADS_REQUESTED',
        page,
      });
    },
    likeThread(threadID: number) {
      dispatch({
        type: 'LIKE_THREAD_REQUESTED',
        threadID,
      });
    },
    onThreadSelected(threadID: number, props?: ShouldGoToCommentsProps) {
      dispatch({
        type: 'FETCH_THREAD_DETAIL_REQUESTED',
        threadID,
      });
      dispatch({
        type: 'FETCH_COMMENTS_REQUESTED',
        threadID,
        page: 1,
      });
      dispatch({
        type: 'THREAD_SELECTED',
        threadID,
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'threadDetail',
        props: {...props, forum: 'user'},
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(UserThreadsPage);
