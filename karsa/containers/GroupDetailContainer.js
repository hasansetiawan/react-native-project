// @flow

import {connect} from 'react-redux';

import GroupDetailPage from '../pages/GroupDetailPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

type ShouldGoToCommentsProps = {
  enterByPressingComment: boolean;
};

function mapStateToProps(state: RootState) {
  let {currentUser, groups, selectedGroup, loadingIndicator} = state;
  let group = groups.get(selectedGroup);
  let {isLikeThreadLoading, isFetchingGroupMembersLoading, isFetchingGroupThreadsLoading} = loadingIndicator;
  return {
    currentUser,
    group,
    isLikeThreadLoading,
    isFetchingGroupMembersLoading,
    isFetchingGroupThreadsLoading,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchGroupMembers(groupID: number, page: number) {
      dispatch({type: 'FETCH_GROUP_MEMBERS_REQUESTED', groupID, page});
    },
    fetchGroupThreads(groupID: number, page: number) {
      dispatch({type: 'FETCH_GROUP_THREADS_REQUESTED', groupID, page});
    },
    likeThread(threadID: number) {
      dispatch({
        type: 'LIKE_THREAD_REQUESTED',
        threadID,
      });
    },
    onThreadSelected(threadID: number, props?: ShouldGoToCommentsProps) {
      dispatch({
        type: 'FETCH_THREAD_DETAIL_REQUESTED',
        threadID,
      });
      dispatch({
        type: 'FETCH_COMMENTS_REQUESTED',
        threadID,
        page: 1,
      });
      dispatch({
        type: 'THREAD_SELECTED',
        threadID,
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'threadDetail',
        props,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(GroupDetailPage);
