// @flow

import {connect} from 'react-redux';

import type {Dispatch} from '../types/Store';
import type {ImageData} from '../types/ImageData';

import NewThreadPage from '../pages/NewThreadPage';

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onCreateThread: (categoryID: number, subCategoryID: number, title: string, description: string, photo: ?ImageData) => {
      dispatch({type: 'CREATE_THREAD_REQUESTED', categoryID, subCategoryID, title, description, photo});
    },
  };
}

export default connect(null, mapDispatchToProps)(NewThreadPage);
