// @flow
import {connect} from 'react-redux';
import PlantingGuidanceDetail from '../pages/PlantingGuidanceDetail';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {plantingGuide, loadingIndicator} = state;
  let {fetchedPlantingGuideDetails} = plantingGuide;
  let {isPlantingGuideDetailLoading} = loadingIndicator;
  return {
    isPlantingGuideDetailLoading,
    fetchedPlantingGuideDetails: fetchedPlantingGuideDetails ? fetchedPlantingGuideDetails : new Map(),
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchPlantingGuideDetail(plantDetail: string) {
      dispatch({type: 'FETCH_PLANTING_GUIDE_DETAIL_REQUESTED', plantDetail});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(PlantingGuidanceDetail);
