// @flow
import {connect} from 'react-redux';
import GuestModeInfo from '../pages/GuestModeInfo';

import type {Dispatch} from '../types/Store';

let mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onButtonPress: () => {
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
    },
  };
};

export default connect(null, mapDispatchToProps)(GuestModeInfo);
