// @flow

import {connect} from 'react-redux';

import StoreDetailPage from '../pages/StoreDetailPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {selectedStore, nearbyStore, products} = state;
  let {nearbyStores} = nearbyStore;

  let storeProducts = [];
  for (let [, value] of products) {
    if (value.storeID === selectedStore) {
      storeProducts.push(value);
    }
  }
  let store = nearbyStores.get(selectedStore);
  return {
    store,
    products: storeProducts,
    isLoading: false, // TODO: decide to add reducers for this or remove
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onProductPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
    navigateToStoreProducts() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'storeproducts',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreDetailPage);
