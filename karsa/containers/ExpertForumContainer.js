// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import ExpertForumPage from '../pages/ExpertForumPage';
import type {Dispatch} from '../types/Store';

function mapStateToProps(state: RootState) {
  let {groups, loadingIndicator} = state;
  let {isUserGroupLoading, isOtherGroupLoading, isInvitedGroupLoading, isProcessGroupLoading} = loadingIndicator;
  return {
    groupThreads: Array.from(groups.values()),
    isUserGroupLoading,
    isOtherGroupLoading,
    isInvitedGroupLoading,
    isProcessGroupLoading,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchUserGroupList() {
      dispatch({type: 'FETCH_USER_GROUP_LIST_REQUESTED'});
    },
    fetchGroupList() {
      dispatch({type: 'FETCH_GROUP_LIST_REQUESTED'});
    },
    fetchGroupInvitationList() {
      dispatch({type: 'FETCH_GROUP_INVITATION_LIST_REQUESTED'});
    },
    onGroupPress(id: number) {
      dispatch({type: 'FETCH_GROUP_MEMBERS_REQUESTED', groupID: id, page: 1});
      dispatch({type: 'FETCH_GROUP_THREADS_REQUESTED', groupID: id, page: 1});
      dispatch({type: 'GROUP_SELECTED', groupID: id});
      dispatch({type: 'PUSH_ROUTE', key: 'groupDetail'});
    },
    onJoinPress(id: number) {
      dispatch({type: 'JOIN_GROUP_REQUESTED', groupID: id});
    },
    onConfirmPress(id: number) {
      dispatch({type: 'CONFIRM_GROUP_INVITATION_REQUESTED', groupID: id});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ExpertForumPage);
