// @flow
import {connect} from 'react-redux';
import ForgotPasswordModal from '../pages/ForgotPasswordPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  return {
    guestMode: state.guestMode,
    isLoading: state.loadingIndicator.isForgotPaswordLoading,
    isSuccess: state.loadingIndicator.isForgotPaswordSuccess,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onRequestClose() {
      dispatch({type: 'POP_ROUTE'});
    },
    onSubmitPress(email: string) {
      dispatch({type: 'FORGOT_PASSWORD_POST', email: email});
    },
    onAlertClick() {
      dispatch({type: 'POP_ROUTE'});
      dispatch({type: 'FORGOT_PASSWORD'});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForgotPasswordModal);
