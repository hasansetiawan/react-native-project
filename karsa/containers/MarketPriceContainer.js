// @flow

import {connect} from 'react-redux';
import MarketPricePage from '../pages/MarketPricePage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {loadingIndicator, marketPrice} = state;
  return {
    isMarketPriceLoading: loadingIndicator.isMarketPriceLoading,
    isProvincePriceLoading: loadingIndicator.isProvincePriceLoading,
    isCommodityPriceLoading: loadingIndicator.isCommodityPriceLoading,
    ...marketPrice,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onProvinceSelect: (item: string) => {
      dispatch({
        type: 'FETCH_PROVINCE_PRICE_REQUESTED',
        province: item,
      });
    },
    onCommoditySelect: (item: string) => {
      dispatch({
        type: 'FETCH_COMMODITY_PRICE_REQUESTED',
        commodityName: item,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MarketPricePage);
