import {connect} from 'react-redux';
import NewAnalysis from '../pages/NewAnalysis';
import type {AnalyticForm} from '../types/AnalyticForm';

import type {Dispatch} from '../types/Store';

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onNewAnalysisSubmit(newAnalysis: AnalyticForm) {
      dispatch({type: 'NEW_COST_ANALYSIS', newAnalysis: newAnalysis});
      dispatch({
        type: 'POP_ROUTE',
      });
    },
    onPressCancel() {
      dispatch({
        type: 'POP_ROUTE',
      });
    },
  };
}

export function mapStateToProps(state: RootState) {
  let {plants} = state;
  let plantOptions = [{value: 0, label: 'Pilih Tanaman'}];
  if (plants.size > 0) {
    for (let plant of plants) {
      let addresses = [
        plant[1].plantAddressGps,
        plant[1].villageName,
        plant[1].subdistrictName,
        plant[1].cityName,
        plant[1].provinceName,
      ];
      let label = `${plant[0]} - ${plant[1].plantName}`;
      let description = '';
      addresses.forEach((address) => {
        if ((address != null) && (['Tidak bisa mendeteksi lokasi', 'null', 'undefined', ''].indexOf(address) === -1)) {
          description += `${address}, `;
        }
      });
      description = description.slice(0, -2);
      plantOptions.push({value: plant[1].id, label, description});
    }
  }
  return {
    plants: plantOptions,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewAnalysis);
