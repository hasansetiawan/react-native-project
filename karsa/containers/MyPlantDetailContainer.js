// @flow

import {connect} from 'react-redux';

import MyPlantDetail from '../pages/MyPlantDetail';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {plants, selectedPlant, analysis, plantingInformation, loadingIndicator} = state;
  let {products} = state;
  let {isPlantDetailLoading} = loadingIndicator;
  let plant = plants.get(selectedPlant);
  let costInfo = [];
  for (let costData of analysis) {
    if (costData[1].plantID === selectedPlant) {
      costInfo.push({
        id: costData[1].id,
        analysisTitle: costData[1].title,
        expense: costData[1].totalExpense,
        income: costData[1].netIncome,
        analysisDate: costData[1].createdAt,
      });
    }
  }
  let productData = [];
  for (let product of products) {
    if (product[1].plantID === selectedPlant) {
      productData.push(product[1]);
    }
  }
  return {
    plantData: {
      plant: {
        ...plant,
        plantDate: plant ? plant.date : '',
      },
    },
    costData: {
      costList: costInfo,
    },
    plantingInfoData: plantingInformation,
    isLoading: isPlantDetailLoading,
    plantProducts: {
      productList: productData,
    },
  };
}
export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchForumCategories() {
      dispatch({type: 'FETCH_THREAD_CATEGORIES_REQUESTED'});
    },
    onAddPhotoPress() {
      dispatch({type: 'PUSH_ROUTE', key: 'addPhoto', props: {isUpdatePlant: true}});
    },
    onCostAnalysisPress: (costID: number) => {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'analysisDetail',
      });
      dispatch({
        type: 'SELECTED_ANALYSIS',
        selectedAnalysis: costID,
      });
    },
    onProductPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
    onLookProductsPress() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'farmerMediaInfo',
      });
    },
    onDeletePlant(plantID: number) {
      dispatch({
        type: 'POP_ROUTE',
      });
      dispatch({
        type: 'DELETE_PLANT_REQUESTED',
        plantID,
      });
      dispatch({
        type: 'FETCH_PLANTS_REQUESTED',
        page: 1,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPlantDetail);
