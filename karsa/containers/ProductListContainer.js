// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

import ProductListPage from '../pages/ProductListPage';

export function mapStateToProps(state: RootState) {
  let {productList} = state;
  let {products} = productList;
  let tempProducts = Array.from(products.values());
  let {isFetchingMoreProductData} = state.loadingIndicator;

  return {
    products: tempProducts,
    isFetchingMoreData: isFetchingMoreProductData,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {dispatch};
}
export function mergeProps(stateProps: Object, {dispatch}: Dispatch, ownProps: Object) {
  let {productCategory} = ownProps;
  return {
    onProductPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
    onDropdownSelect(value: string | number, page: number) {
      if (value === -1) {
        dispatch({
          type: 'EMPTY_PRODUCT_LIST',
          target: 'normal',
        });
        dispatch({
          type: 'FETCH_PRODUCT_LIST_REQUESTED',
          target: 'normal',
          category: productCategory,
          page,
        });
      } else {
        dispatch({
          type: 'EMPTY_PRODUCT_LIST',
          target: 'filter',
        });
        dispatch({
          type: 'FETCH_PRODUCT_LIST_REQUESTED',
          target: 'filter',
          category: productCategory,
          subCategory: value,
          page,
        });
      }
    },
    onSearchPressed(searchKey: string, page: number) {
      dispatch({
        type: 'EMPTY_PRODUCT_LIST',
        target: 'search',
      });
      dispatch({
        type: 'FETCH_PRODUCT_LIST_REQUESTED',
        target: 'search',
        category: productCategory,
        searchKey,
        page,
      });
    },
    fetchMoreData(pageNumber: number, options: Object) {
      let {activeList} = options;
      switch (activeList) {
        case 'normal': {
          dispatch({
            type: 'FETCH_PRODUCT_LIST_REQUESTED',
            target: 'normal',
            category: productCategory,
            page: pageNumber,
          });
          break;
        }
        case 'filter': {
          let {filterKey} = options;
          dispatch({
            type: 'FETCH_PRODUCT_LIST_REQUESTED',
            target: 'filter',
            category: productCategory,
            subCategory: filterKey,
            page: pageNumber,
          });
          break;
        }
        case 'search': {
          let {searchKey} = options;
          dispatch({
            type: 'FETCH_PRODUCT_LIST_REQUESTED',
            target: 'search',
            category: productCategory,
            searchKey,
            page: pageNumber,
          });
          break;
        }
      }
    },
    ...stateProps,
    productCategory,
  };
}
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ProductListPage);
