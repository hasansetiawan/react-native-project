// @flow

import {connect} from 'react-redux';

import type {Dispatch} from '../types/Store';
import type {ImageData} from '../types/ImageData';

import EditThreadPage from '../pages/EditThreadPage';

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onEditThread: (categoryID: number, subCategoryID: number, title: string, description: string, photo: ?ImageData, threadID: number) => {
      dispatch({type: 'EDIT_THREAD_REQUESTED', categoryID, subCategoryID, title, description, photo, threadID});
    },
  };
}

export default connect(null, mapDispatchToProps)(EditThreadPage);
