// @flow

import mapObjectToDropdownOptions from '../helpers/mapObjectToDropdownOptions';
import ShareToForumForm from '../pages/ShareToForumForm';

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

function mapStateToProps(state: RootState) {
  let {threadCategory, loadingIndicator} = state;
  let {categories, subCategories} = threadCategory;
  let {isSubmitThreadLoading} = loadingIndicator;
  return {
    forumCategoryOptions: mapObjectToDropdownOptions(categories, {value: 'id', label: 'categoryName'}),
    forumSubCategoryOptions: mapObjectToDropdownOptions(subCategories, {value: 'id', label: 'subCategoryName'}),
    isSubmitThreadLoading,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onCategorySelected: (categoryID: number) => {
      dispatch({type: 'FETCH_THREAD_SUB_CATEGORIES_REQUESTED', categoryID});
    },
    onSubmit: (plantID: number, categoryID: number, subCategoryID: number, title: string, description: string) => {
      let shareItem = {plantID, categoryID, subCategoryID, title, description};
      dispatch({type: 'SHARE_PLANT_TO_FORUM_REQUESTED', shareItem});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ShareToForumForm);
