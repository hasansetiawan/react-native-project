// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

import StoreProductsPage from '../pages/StoreProductsPage';

export function mapStateToProps(state: RootState) {
  let {products, selectedStore, loadingIndicator} = state;
  let {isFetchingMoreStoreProduct} = loadingIndicator;
  let tempProducts = [];
  for (let [, value] of products) {
    if (value.storeID === selectedStore) {
      tempProducts.push(value);
    }
  }
  return {
    products: tempProducts,
    isFetchingMoreData: isFetchingMoreStoreProduct,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onProductPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
    fetchMoreData(page: number) {
      dispatch({
        type: 'FETCH_STORE_PRODUCTS_REQUESTED',
        page,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(StoreProductsPage);
