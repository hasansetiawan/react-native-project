// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

import ThreadForm from '../pages/ThreadForm';

function mapStateToProps(state: RootState) {
  let {threadCategory, loadingIndicator} = state;
  let {isSubmitThreadLoading} = loadingIndicator;
  return {
    ...threadCategory,
    isSubmitThreadLoading,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onCategorySelected: (categoryID: number) => {
      dispatch({type: 'FETCH_THREAD_SUB_CATEGORIES_REQUESTED', categoryID});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ThreadForm);
