// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import OtherThreadsPage from '../pages/OtherThreadsPage';
import type {Dispatch} from '../types/Store';

type ShouldGoToCommentsProps = {
  enterByPressingComment: boolean;
};

export function mapStateToProps(state: RootState) {
  let {threads, loadingIndicator, currentUser} = state;
  return {
    currentUser,
    threads,
    isThreadLoading: loadingIndicator.isThreadLoading,
    isLikeThreadLoading: loadingIndicator.isLikeThreadLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchOtherThreads(page: number, forum: string) {
      dispatch({
        type: 'FETCH_OTHER_THREADS_REQUESTED',
        page,
        forum,
      });
    },
    likeThread(threadID: number) {
      dispatch({
        type: 'LIKE_THREAD_REQUESTED',
        threadID,
      });
    },
    onThreadSelected(threadID: number, props?: ShouldGoToCommentsProps) {
      dispatch({
        type: 'FETCH_THREAD_DETAIL_REQUESTED',
        threadID,
      });
      dispatch({
        type: 'FETCH_COMMENTS_REQUESTED',
        threadID,
        page: 1,
      });
      dispatch({
        type: 'THREAD_SELECTED',
        threadID,
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'threadDetail',
        props: {...props, forum: 'other'},
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(OtherThreadsPage);
