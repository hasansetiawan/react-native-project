// @flow
import {connect} from 'react-redux';

import SettingsPage from '../pages/SettingsPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

function mapStateToProps(state: RootState) {
  return {
    guestMode: state.guestMode,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onChangePasswordPress: () => {
      dispatch({type: 'PUSH_ROUTE', key: 'updatePassword'});
      dispatch({type: 'UPDATE_PASSWORD'});
    },
    onAboutPress: () => {
      dispatch({type: 'PUSH_ROUTE', key: 'about'});
    },
    onContactPress: () => {
      dispatch({
        type: 'PUSH_ROUTE', key: 'contactUs',
      });
    },
    onTermsPress: () => {
      dispatch({type: 'PUSH_ROUTE', key: 'privacypolicy'});
    },
    onLogoutPress: () => {
      dispatch({type: 'LOGOUT'});
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
    },
    onPrivacyPress: () => {},
    onTutorialPress: () => {
      dispatch({type: 'SHOW_TUTORIAL_MODAL'});
    },
    navigateToProfile() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'editProfile',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(SettingsPage);
