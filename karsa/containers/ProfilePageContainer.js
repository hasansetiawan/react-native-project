// @flow
import {connect} from 'react-redux';

import ProfilePage from '../pages/ProfilePage';

import type {Dispatch} from '../types/Store';
import type {RootState} from '../types/RootState';

function mapStateToProps(state: RootState) {
  let {currentUser} = state;
  return {
    user: currentUser,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onEditProfilePress: () => {
      dispatch({type: 'PUSH_ROUTE', key: 'editProfile'});
      dispatch({type: 'FETCH_ADDRESS'});
    },
  };
}

export default connect(
  mapStateToProps,
  null,
)(ProfilePage);
