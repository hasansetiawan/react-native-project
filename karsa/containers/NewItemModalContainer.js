// @flow
import {connect} from 'react-redux';
import NewItemModal from '../core-ui/NewItemModal';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onRequestClose() {
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
    },
    onAddPhotoPress() {
      dispatch({type: 'CLEAR_TEMPORARY_PHOTO'});
      dispatch({type: 'PUSH_ROUTE', key: 'addPhoto'});
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
      dispatch({type: 'ADD_PHOTO'});
    },
    onAddPlantPress() {
      dispatch({type: 'CLEAR_TEMPORARY_PHOTO'});
      dispatch({type: 'FETCH_PLANT_TYPES_REQUESTED'});
      dispatch({type: 'PUSH_ROUTE', key: 'newPlant'});
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
    },
    onAddThreadPress() {
      dispatch({type: 'FETCH_THREAD_CATEGORIES_REQUESTED'});
      dispatch({type: 'PUSH_ROUTE', key: 'newThread'});
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
    },
    onAddCostAnalysisPress() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'newAnalysis',
      });
      dispatch({type: 'FETCH_ALL_PLANTS_REQUESTED'});
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
    },
    onLoginPress() {
      dispatch({type: 'HIDE_NEW_ITEM_MODAL'});
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
    },
  };
}

export function mapStateToProps(state: RootState) {
  let {newItemModal} = state;
  return {
    isShown: newItemModal.isShown,
    guestMode: state.guestMode,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NewItemModal);
