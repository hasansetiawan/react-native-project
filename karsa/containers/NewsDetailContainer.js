//@flow
import {connect} from 'react-redux';
import NewsHighlight from '../pages/NewsDetailPage';

import type {NewsState} from '../types/News';

export function mapStateToProps({news}: {news: NewsState}) {
  let {selectedHighlight, allNews} = news;
  let selectedNews = allNews.get(selectedHighlight);
  return {
    news: selectedNews,
  };
}

export function mapDispatchToProps() {
  return {};
}

export default connect(mapStateToProps, mapDispatchToProps)(NewsHighlight);
