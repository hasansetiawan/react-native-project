//@flow
import {connect} from 'react-redux';
import MyPlants from '../pages/MyPlants';
import type {RootState} from '../types/RootState';

function mapStateToProps(state: RootState) {
  let {plants, loadingIndicator} = state;
  let {isFetchingMorePlants} = loadingIndicator;
  return {
    plants: plants ? Array.from(plants.values()) : [],
    isFetchingMoreData: isFetchingMorePlants,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    onPress: (plantID: number) => {
      dispatch({
        type: 'PLANT_SELECTED',
        plantID,
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'myPlantDetail',
      });
      dispatch({
        type: 'FETCH_ANALYSIS_REQUESTED',
      });
      dispatch({
        type: 'FETCH_PLANT_DETAIL_REQUESTED',
        plantID,
      });
      dispatch({
        type: 'FETCH_PRODUCTS_PLANT_REQUESTED',
        plantID,
      });
    },
    onSearch: (keyword: string) => {
      dispatch({
        type: 'SEARCH_PLANT_REQUESTED',
        keyword,
      });
    },
    fetchMoreData(page: number) {
      dispatch({
        type: 'FETCH_PLANTS_REQUESTED',
        page,
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(MyPlants);
