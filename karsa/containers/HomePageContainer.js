import uuid from 'uuid';
import {connect} from 'react-redux';
import HomePage from '../pages/HomePage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

function mapStateToProps(state: RootState) {
  let {homeHighlights, guestMode} = state;
  return {
    guestMode: guestMode,
    homeHighlights,
  };
}

function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchHighlights() {
      dispatch({type: 'FETCH_HOME_HIGHLIGHT_REQUESTED'});
    },
    fetchLevels() {
      dispatch({type: 'FETCH_LEVELS_REQUESTED'});
    },
    navigateTo(key) {
      return () => {
        dispatch({
          type: 'PUSH_ROUTE',
          key,
        });
      };
    },
    onHighlightSelect(id: string, type: 'news' | 'thread') {
      if (type === 'news') {
        dispatch({type: 'NEWS_SELECTED', id});
      } else {
        dispatch({type: 'THREAD_SELECTED', threadID: id});
        dispatch({type: 'FETCH_THREAD_DETAIL_REQUESTED', threadID: id});
        dispatch({type: 'PUSH_ROUTE', key: 'threadDetail'});
      }
    },
    onFavorite() {
      dispatch({
        type: 'FAVORITE_ADD',
        favorite: {
          id: uuid.v4(),
          description: 'Home Page',
          key: 'homepage',
          payload: {},
        },
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(HomePage);
