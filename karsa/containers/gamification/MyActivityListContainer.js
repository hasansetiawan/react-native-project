// @flow
import {connect} from 'react-redux';

import MyActivityList from '../../core-ui/gamification/MyActivityList';

import type {Activity} from '../../types/Activity';
import type {RootState} from '../../types/RootState';
import type {Dispatch} from '../../types/Store';

const mapStateToProps = (state: RootState) => {
  let {activities, actions, userActions, userScores} = state.gamificationState;
  let {isFetchingGamificationActions, isFetchingUserGamificationActions, isFetchingUserScores} = state.loadingIndicator;
  return {
    loading: isFetchingGamificationActions || isFetchingUserGamificationActions || isFetchingUserScores,
    activities,
    actions,
    userActions,
    userScores,
  };
};

const mapDispatchToProps = (dispatch: Dispatch) => (
  {
    fetchActions: () => {
      dispatch({type: 'FETCH_GAMIFICATION_ACTIONS_REQUESTED'});
    },
    fetchUserActions: () => {
      dispatch({type: 'FETCH_USER_GAMIFICATION_ACTIONS_REQUESTED'});
    },
    fetchUserScores: () => {
      dispatch({type: 'FETCH_USER_SCORES_REQUESTED'});
    },
    saveActivities: (activities: Array<Activity>) => {
      dispatch({type: 'SAVE_ACTIVITIES', activities});
    },
  }
);

export default connect(mapStateToProps, mapDispatchToProps)(MyActivityList);
