// @flow
import {connect} from 'react-redux';

import GamificationPageTitleBar from '../../core-ui/gamification/GamificationPageTitleBar';
import type {Dispatch} from '../../types/Store';

const mapDispatchToProps = (dispatch: Dispatch) => {
  return {
    onIconPress() {
      dispatch({type: 'FETCH_REWARDS_REQUESTED'});
      dispatch({type: 'PUSH_ROUTE', key: 'rewardList'});
    },
  };
};

export default connect(null, mapDispatchToProps)(GamificationPageTitleBar);
