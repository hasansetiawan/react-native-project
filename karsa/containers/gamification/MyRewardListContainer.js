// @flow
import {connect} from 'react-redux';

import MyRewardList from '../../core-ui/gamification/MyRewardList';

const mockRewards = [{
  id: 1,
  thumbnail: 'https://faedahjaya.com/gambar/pestisida/pupuk-daun/Ultradap.jpg',
  name: 'Natura Bibit Hayati Unggul',
  distributorName: 'PT. Prima Agrotech, Jember',
  isClaimed: true,
}, {
  id: 2,
  thumbnail: 'https://faedahjaya.com/gambar/pestisida/pupuk-daun/Ultradap.jpg',
  name: 'Natura Bibit Hayati',
  distributorName: 'PT. Prima Agrotech, Jember',
  isClaimed: false,
}, {
  id: 3,
  thumbnail: 'https://faedahjaya.com/gambar/pestisida/pupuk-daun/Ultradap.jpg',
  name: 'Natura Bibit Hayati',
  distributorName: 'PT. Prima Agrotech, Jember',
  isClaimed: false,
}, {
  id: 4,
  thumbnail: 'https://faedahjaya.com/gambar/pestisida/pupuk-daun/Ultradap.jpg',
  name: 'Natura Bibit Hayati',
  distributorName: 'PT. Prima Agrotech, Jember',
  isClaimed: false,
}, {
  id: 5,
  thumbnail: 'https://faedahjaya.com/gambar/pestisida/pupuk-daun/Ultradap.jpg',
  name: 'Natura Bibit Hayati',
  distributorName: 'PT. Prima Agrotech, Jember',
  isClaimed: false,
}];

const mapStateToProps = () => {
  return {
    rewards: mockRewards,
  };
};

export default connect(mapStateToProps, null)(MyRewardList);
