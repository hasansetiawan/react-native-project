// @flow

import {connect} from 'react-redux';

import type {RootState} from '../../types/RootState';
import type {Dispatch} from '../../types/Store';

import BonusTabContent from '../../core-ui/gamification/BonusTabContent';

export function mapStateToProps(state: RootState) {
  let {gamificationState, loadingIndicator} = state;
  let {levels} = gamificationState;
  let {isGamificationBonusLoading} = loadingIndicator;
  let levelBonus = levels.slice(1).map(({coins, name}) => {
    return {
      coins, name,
    };
  });
  return {
    bonusList: levelBonus,
    isLoading: isGamificationBonusLoading,
  };
}
export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchLevels: () => dispatch({type: 'FETCH_LEVELS_REQUESTED'}),
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(BonusTabContent);
