// @flow
import {connect} from 'react-redux';

import GamificationHeader from '../../core-ui/gamification/GamificationHeader';

import type {RootState} from '../../types/RootState';

export function mapStateToProps(state: RootState) {
  let {name, role, coins, level, experience, avatar} = state.currentUser;
  let {levels} = state.gamificationState;
  let currentRequirement;
  for (let {id, requirement} of levels) {
    if (id - 1 === level) {
      currentRequirement = requirement;
      break;
    }
  }
  return {
    user: {
      avatar, name, role, experience, coins,
      requirement: currentRequirement,
    },
  };
}

export default connect(mapStateToProps, null)(GamificationHeader);
