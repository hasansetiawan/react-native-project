// @flow

import {connect} from 'react-redux';

import type {RootState} from '../../types/RootState';
import RewardListPage from '../../pages/RewardListPage';

export function mapDispatchToProps() {
  return {};
}

export function mapStateToProps(state: RootState) {
  let {gamificationState, loadingIndicator} = state;
  let {isRewardListLoading} = loadingIndicator;
  let {rewards} = gamificationState;
  return {
    isRewardListLoading,
    rewards,
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RewardListPage);
