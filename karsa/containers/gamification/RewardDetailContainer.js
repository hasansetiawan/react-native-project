// @flow
import {connect} from 'react-redux';

import RewardDetail from '../../core-ui/gamification/RewardDetail';

const mockRewardDetail = {
  id: 1,
};

const mapStateToProps = () => {
  return {
    reward: mockRewardDetail,
  };
};

export default connect(mapStateToProps, null)(RewardDetail);
