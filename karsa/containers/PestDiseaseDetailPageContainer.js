// @flow

import {connect} from 'react-redux';
import PestDiseaseDetailPage from '../pages/PestDiseaseDetailPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {pests, diseases, selectedPestDisease, products} = state;
  let detail = (selectedPestDisease.type === 'pest')
    ? pests.get(selectedPestDisease.id)
    : diseases.get(selectedPestDisease.id);
  let {
    isPestDiseaseDetailLoading,
  } = state.loadingIndicator;
  let countermeasures = [];
  for (let product of products) {
    if (selectedPestDisease.type === 'pest') {
      if (product[1].pestID === selectedPestDisease.id) {
        countermeasures.push(product[1]);
      }
    } else {
      if (product[1].diseaseID === selectedPestDisease.id) {
        countermeasures.push(product[1]);
      }
    }
  }
  return {
    detail: {
      ...detail,
      countermeasures,
    },
    isPestDiseaseDetailLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onProductPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
  };
}
export default connect(mapStateToProps, mapDispatchToProps)(PestDiseaseDetailPage);
