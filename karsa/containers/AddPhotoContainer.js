// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import AddPhoto from '../pages/AddPhoto';
import ImagePicker from 'react-native-image-picker';

import type {GalleryData} from '../types/Plant';

export function mapStateToProps(state: RootState) {
  let {plants, addPhoto, selectedPlant, isAddPhoto} = state;
  let plantOptions = [{value: 0, label: 'Pilih Tanaman'}];
  if (plants.size > 0) {
    for (let plant of plants) {
      plantOptions.push({value: plant[1].id, label: plant[1].plantName});
    }
  }
  return {
    plants: plantOptions,
    newPhoto: addPhoto,
    selectedPlant: selectedPlant,
    isAddPhoto: isAddPhoto,
  };
}

export function mergeProps(stateProps: Object, {dispatch}: Object, ownProps: any) {
  let {isUpdatePlant} = ownProps;
  return {
    ...stateProps,
    onPressCancel() {
      dispatch({type: 'POP_ROUTE'});
    },
    onPressSave(addPhoto: GalleryData) {
      if (isUpdatePlant) {
        dispatch({type: 'ADD_PHOTO_POST', addPhoto, isUpdatePlant});
      } else {
        dispatch({type: 'ADD_PHOTO_POST', addPhoto});
      }
    },
    onPicturePress() {
      ImagePicker.showImagePicker(null, (image) => {
        if (!image.didCancel) {
          dispatch({type: 'PICK_PHOTO', image});
        }
      });
    },
  };
}

export default connect(mapStateToProps, null, mergeProps)(AddPhoto);
