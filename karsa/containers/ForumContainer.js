// @flow

import {connect} from 'react-redux';

import type {RootState} from '../types/RootState';
import ForumPage from '../pages/ForumPage';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {forumCategory, threadCategory} = state;
  let {filterCategories} = threadCategory;
  return {
    forumCategory,
    threadFilterCategory: filterCategories,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchForumCategories() {
      dispatch({type: 'FETCH_FORUM_CATEGORIES_REQUESTED'});
    },
    fetchThreadFilterCategories(slug: string) {
      dispatch({type: 'FETCH_THREAD_CATEGORIES_FILTER_REQUESTED', slug});
    },
    fetchFilteredThreads(forumCategory: string, threadCategory: string, sortBy: 'all' | 'latest' | 'popular', page: number) {
      dispatch({type: 'FETCH_FILTERED_THREADS_REQUESTED', forumCategory, threadCategory, sortBy, page});
    },
    fetchSearchedThreads(forumCategory: string, query: string, page: number) {
      dispatch({type: 'FETCH_SEARCHED_THREADS_REQUESTED', forumCategory, query, page});
    },
    fetchSearchedGroups(groupCategory: 'joined' | 'not-joined' | 'invite', query: string) {
      dispatch({type: 'FETCH_SEARCHED_GROUPS_REQUESTED', groupCategory, query});
    },
    onAddNewForum() {
      dispatch({type: 'FETCH_THREAD_CATEGORIES_REQUESTED'});
      dispatch({type: 'PUSH_ROUTE', key: 'newThread'});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(ForumPage);
