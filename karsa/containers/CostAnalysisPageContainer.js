// @flow

import {connect} from 'react-redux';

import CostAnalysisPage from '../pages/CostAnalysisPage';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

//TODO add dispatch for navigation and title bar later
export function mapStateToProps(state: RootState) {
  let {analysis} = state;
  return {
    costsAnalysis: analysis,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateToNewAnalysis() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'newAnalysis',
      });
      dispatch({
        type: 'FETCH_ALL_PLANTS_REQUESTED',
      });
    },
    navigateToAnalysisDetail(id: number) {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'analysisDetail',
      });
      dispatch({
        type: 'SELECTED_ANALYSIS',
        selectedAnalysis: id,
      });
    },
    deleteAnalysis(id: string) {
      dispatch({
        type: 'DELETE_ANALYSIS',
        analysisID: id,
      });
    },
    navigateBack() {
      dispatch({
        type: 'POP_ROUTE',
      });
    },
    navigateToEditAnalysis(id: number) {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'analysisEdit',
      });
      dispatch({
        type: 'SELECTED_ANALYSIS',
        selectedAnalysis: id,
      });
      dispatch({
        type: 'FETCH_ALL_PLANTS_REQUESTED',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(CostAnalysisPage);
