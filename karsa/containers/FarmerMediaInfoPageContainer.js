// @flow

import {connect} from 'react-redux';
import FarmerMediaInfoPage from '../pages/FarmerMediaInfoPage';

import type {Dispatch} from '../types/Store';

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    navigateTo: (routeName: string) => {
      dispatch({
        type: 'PUSH_ROUTE',
        key: routeName,
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(FarmerMediaInfoPage);
