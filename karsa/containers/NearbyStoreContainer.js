// @flow
import {connect} from 'react-redux';
import NearbyStorePage from '../pages/NearbyStorePage';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';


export function mapStateToProps(state: RootState) {
  let {nearbyStore, location} = state;
  let {nearbyStores} = nearbyStore;
  let {gpsAddress} = location;
  return {
    nearbyStores: nearbyStores ? nearbyStores : new Map(),
    gpsAddress: gpsAddress ? gpsAddress : {},
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchNearbyStore(lat: number, lon: number, page: number) {
      dispatch({type: 'FETCH_NEARBY_STORE_REQUESTED', lat, lon, page});
    },
    onPress(id: number) {
      dispatch({
        type: 'NEARBY_STORE_SELECTED',
        storeID: id,
      });
      dispatch({
        type: 'FETCH_STORE_PRODUCTS_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'storedetail',
      });
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NearbyStorePage);
