//@flow
import {connect} from 'react-redux';
import NotificationsPage from '../pages/Notifications';

import type {Action} from '../types/Action';
import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

export function mapStateToProps(state: RootState) {
  let {notifications, guestMode, loadingIndicator} = state;
  let {isNotificationLoading} = loadingIndicator;
  return {
    notifications,
    guestMode,
    isNotificationLoading,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    fetchNotifications(page: number) {
      dispatch({type: 'FETCH_NOTIFICATION_REQUESTED', page});
    },
    onPress(actions: Array<Action>) {
      for (let action of actions) {
        dispatch(action);
      }
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(NotificationsPage);
