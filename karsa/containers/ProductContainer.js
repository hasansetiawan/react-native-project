//@flow
import {connect} from 'react-redux';
import ProductPage from '../pages/ProductPage';

import type {RootState} from '../types/RootState';
import type {Dispatch} from '../types/Store';

function mapRouteString(route: string) {
  switch (route) {
    case 'SeedProducts': return 'bibit';
    case 'FertilizerProducts': return 'pupuk';
    case 'PesticideProducts': return 'pestisida';
    case 'ToolsProducts': return 'alat-alat-pertanian';
    default: return 'bibit';
  }
}
export function mapStateToProps(state: RootState) {
  let {products, navigation} = state;
  let currentRoute = navigation.routes[navigation.index].key;

  let currentState;
  switch (currentRoute) {
    case 'SeedProducts': currentState = state.seedProducts; break;
    case 'FertilizerProducts': currentState = state.fertilizerProducts; break;
    case 'PesticideProducts': currentState = state.pesticideProducts; break;
    case 'ToolsProducts': currentState = state.toolsProducts; break;
    default: currentState = state.seedProducts;
  }
  let {byHighlight, byPopularity, byLatest} = currentState;
  let productsHighlight = (byHighlight && byHighlight.map((id) => products.get(id))) || [];
  let productsLatest = (byPopularity && byPopularity.map((id) => products.get(id))) || [];
  let productsPopular = (byLatest && byLatest.map((id) => products.get(id))) || [];

  return {
    currentRoute,
    productsHighlight,
    productsLatest,
    productsPopular,
  };
}

export function mapDispatchToProps(dispatch: Dispatch) {
  return {
    onPress(id: number) {
      dispatch({type: 'PUSH_ROUTE', key: 'productDetail'});
      dispatch({
        type: 'FETCH_PRODUCT_DETAIL_REQUESTED',
        productID: id,
      });
      dispatch({
        type: 'FETCH_PRODUCT_REVIEW_REQUESTED',
        page: 1,
      });
      dispatch({
        type: 'FETCH_PLANT_FOR_PRODUCT_REQUESTED',
        productID: id,
      });
    },
    navigateToProductList(route: string) {
      dispatch({
        type: 'FETCH_PRODUCT_COMMODITY_REQUESTED',
        category: mapRouteString(route),
      });
      dispatch({
        type: 'EMPTY_PRODUCT_LIST',
        target: 'normal',
      });
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'productList',
        props: {
          productCategory: mapRouteString(route),
        },
      });
      dispatch({
        type: 'FETCH_PRODUCT_LIST_REQUESTED',
        target: 'normal',
        page: 1,
        category: mapRouteString(route),
      });
    },
  };
}
export function mergeProps(stateProps: Object, dispatchProps: Object) {
  let {currentRoute, ...others} = stateProps;
  let {navigateToProductList, ...otherDispatches} = dispatchProps;
  return {
    ...others,
    ...otherDispatches,
    navigateToProductList: () => navigateToProductList(currentRoute),
  };
}
export default connect(mapStateToProps, mapDispatchToProps, mergeProps)(ProductPage);
