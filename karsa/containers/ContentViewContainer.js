// @flow

import {connect} from 'react-redux';
import WebView from '../core-ui/WebView';
import {ContentView} from '../pages/ContentView';


function mapDispatchToProps(dispatch) {
  return {
    onLinkPress: (uri: string) => {
      dispatch({
        type: 'ROUTE_PUSHED',
        route: {
          key: 'WebView',
          title: 'WebView',
          Component: WebView,
          props: {uri},
        },
      });
    },
  };
}

export default connect(null, mapDispatchToProps)(ContentView);
