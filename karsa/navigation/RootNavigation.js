//@flow
import React, {Component} from 'react';
import {connect} from 'react-redux';
import {
  AsyncStorage,
  View,
  NavigationExperimental,
  BackAndroid,
  Alert,
  StatusBar,
} from 'react-native';
import autobind from 'class-autobind';

import AboutContainer from '../containers/AboutContainer';
import AddPhotoContainer from '../containers/AddPhotoContainer';
import ContactPageContainer from '../containers/ContactPageContainer';
import CostAnalysisDetailContainer from '../containers/AnalysisDetailContainer';
import CostAnalysisEditContainer from '../containers/AnalysisEditContainer';
import CostAnalysisPageContainer from '../containers/CostAnalysisPageContainer';
import EditProfileContainer from '../containers/EditProfilePageContainer';
import EditThreadContainer from '../containers/EditThreadContainer';
import FarmerMediaInfoPageContainer from '../containers/FarmerMediaInfoPageContainer';
import FarmerMediaPageContainer from '../containers/FarmerMediaPageContainer';
import FavoritePage from '../pages/FavoritePage';
import ForgotPasswordContainer from '../containers/ForgotPasswordContainer';
import ForumContainer from '../containers/ForumContainer';
import GamificationPage from '../pages/GamificationPage';
import HomePageContainer from '../containers/HomePageContainer';
import InitialPage from '../pages/InitialPage';
import LoginPageContainer from '../containers/LoginPageContainer';
import MarketPriceContainer from '../containers/MarketPriceContainer';
import MyPlantDetailContainer from '../containers/MyPlantDetailContainer';
import MyPlantsContainer from '../containers/MyPlantsContainer';
import NavBarContainer from '../containers/NavBarContainer';
import NearbyStoreContainer from '../containers/NearbyStoreContainer';
import NewAnalysisContainer from '../containers/NewAnalysisContainer';
import NewItemModalContainer from '../containers/NewItemModalContainer';
import NewPlantContainer from '../containers/NewPlantContainer';
import NewsDetailContainer from '../containers/NewsDetailContainer';
import NewsPageContainer from '../containers/NewsPageContainer';
import NewsWebViewContainer from '../containers/NewsWebViewContainer';
import NewThreadContainer from '../containers/NewThreadContainer';
import NotificationsContainer from '../containers/NotificationsContainer';
import PestDiseaseContainer from '../containers/PestDiseasePageContainer';
import PestDiseaseDetailContainer from '../containers/PestDiseaseDetailPageContainer';
import PlantAddContainer from '../containers/PlantAddContainer';
import PlantInfoContainer from '../containers/PlantInfoContainer';
import PrivacyPolicyContainer from '../containers/PrivacyPolicyContainer';
import ProductContainer from '../containers/ProductContainer';
import ProductDetailContainer from '../containers/ProductDetailContainer';
import RegisterStep3FarmerContainer from '../containers/RegisterStep3FarmerContainer';
import RegisterStepTwoContainer from '../containers/RegisterStepTwoContainer';
import RegisterStepTwoFarmerContainer from '../containers/RegisterStepTwoFarmerContainer';
import RewardListContainer from '../containers/gamification/RewardListContainer';
import SettingsPageContainer from '../containers/SettingsPageContainer';
import ThreadDetailContainer from '../containers/ThreadDetailContainer';
import ProductListContainer from '../containers/ProductListContainer';
import UpdatePasswordContainer from '../containers/UpdatePasswordContainer';
import WalkthroughPageContainer from '../containers/WalkthroughPageContainer';
import ComingSoonPage from '../pages/ComingSoonPage';
import StoreDetailPageContainer from '../containers/StoreDetailPageContainer';
import StoreProductsPageContainer from '../containers/StoreProductsPageContainer';
import GroupDetailContainer from '../containers/GroupDetailContainer';
import CultivationContainer from '../containers/CultivationContainer';
import PlantingGuidanceContainer from '../containers/PlantingGuidanceContainer';
import PlantingGuidanceDetailContainer from '../containers/PlantingGuidanceDetailContainer';

let {CardStack: NavigationCardStack} = NavigationExperimental;

import Drawer from 'react-native-drawer';
import {DrawerComponent, DownloadIndicator} from '../core-ui';
import {decrypt} from '../helpers/encryption';

import type {NavigationState} from '../types/Navigation';
import type {CodePushState} from '../types/CodePushState';

type Props = {
  codePush: CodePushState;
  navigation: NavigationState;
  isDrawerOpen: boolean;
  popRoute: () => void;
  backToHome: () => void;
  showWalkthrough: () => void;
  onPressClose: () => void;
  onPressSearch: () => void;
  onPressProfile: () => void;
  onPressLogout: () => void;
  onPressContactUs: () => void;
  autoLogin: (userAuth: {email: string; password: string}) => void;
};

class RootNavigation extends Component {
  props: Props;

  constructor() {
    super(...arguments);
    autobind(this);
  }

  componentDidMount() {
    BackAndroid.addEventListener('hardwareBackPress', this._handleBackAction);
    AsyncStorage.getItem('@walkthrough:show').then((value) => {
      if (value !== 'yes') {
        this.props.showWalkthrough();
      }
    });
    AsyncStorage.multiGet(['@email', '@password', '@isLogin']).then((values) => {
      // destruct array of arrays and immediately assigning to variables
      let [[, email], [, password], [, isLogin]] = values;
      if (isLogin === 'yes') {
        this.props.autoLogin({email, password: decrypt(password)});
      }
    });
  }

  componentWillUnmount() {
    BackAndroid.removeEventListener('hardwareBackPress', this._handleBackAction);
  }

  _handleBackAction() {
    if (this.props.navigation.index === 0) {
      let firstRouteKey = this.props.navigation.routes[0].key;
      if (['walkthrough', 'initial', 'homepage'].indexOf(firstRouteKey) !== -1) {
        Alert.alert('Karsa', 'Keluar dari aplikasi?', [
          {text: 'Iya', onPress: () => BackAndroid.exitApp()},
          {text: 'Tidak'},
        ]);
      } else {
        this.props.backToHome();
      }
    } else {
      this.props.popRoute();
    }
    return true;
  }
  _renderScene({scene}) {
    let {route} = scene;
    let {key, props} = route;
    switch (key) {
      case 'storeproducts': return <StoreProductsPageContainer />;
      case 'storedetail': return <StoreDetailPageContainer />;
      case 'productList': return <ProductListContainer {...props} />;
      case 'productDetail': return <ProductDetailContainer />;
      case 'walkthrough': return <WalkthroughPageContainer />;
      case 'login': return <LoginPageContainer />;
      case 'pestDisease': return <PestDiseaseContainer />;
      case 'pestDiseaseDetail': return <PestDiseaseDetailContainer />;
      case 'homepage': return <HomePageContainer />;
      case 'notifications': return <NotificationsContainer />;
      case 'settings': return <SettingsPageContainer />;
      case 'cultivation': return <CultivationContainer />;
      case 'weeklyGuidance': return <MyPlantsContainer />;
      case 'plantingGuidance': return <PlantingGuidanceContainer />;
      case 'myPlantDetail': return <MyPlantDetailContainer />;
      case 'thread': return <ForumContainer {...props} />;
      case 'weather': return <ComingSoonPage />;
      case 'farmerMedia': return <FarmerMediaPageContainer />;
      case 'farmerMediaInfo': return <FarmerMediaInfoPageContainer />;
      case 'marketPrice': return <MarketPriceContainer />;
      case 'news': return <NewsPageContainer />;
      case 'about': return <AboutContainer />;
      case 'addPhoto': return <AddPhotoContainer {...props} />;
      case 'analysis': return <CostAnalysisPageContainer />;
      case 'analysisDetail': return <CostAnalysisDetailContainer />;
      case 'analysisEdit': return <CostAnalysisEditContainer />;
      case 'contactUs': return <ContactPageContainer />;
      case 'editProfile': return <EditProfileContainer />;
      case 'editThread': return <EditThreadContainer {...props} />;
      case 'favorites': return <FavoritePage />;
      case 'FertilizerProducts': return <ProductContainer />;
      case 'forgotPassword': return <ForgotPasswordContainer />;
      case 'gamification': return <GamificationPage />;
      case 'initial': return <InitialPage />;
      case 'nearbyStore': return <NearbyStoreContainer />;
      case 'newAnalysis': return <NewAnalysisContainer />;
      case 'newPlant': return <NewPlantContainer />;
      case 'newsDetail': return <NewsWebViewContainer />;
      case 'newsHighlight': return <NewsDetailContainer />;
      case 'newThread': return <NewThreadContainer />;
      case 'PesticideProducts': return <ProductContainer />;
      case 'plantAdd': return <PlantAddContainer />;
      case 'plantInfo': return <PlantInfoContainer />;
      case 'privacypolicy': return <PrivacyPolicyContainer />;
      case 'registerStep3Farmer': return <RegisterStep3FarmerContainer />;
      case 'registerStepTwo': return <RegisterStepTwoContainer />;
      case 'registerStepTwoFarmer': return <RegisterStepTwoFarmerContainer />;
      case 'rewardList': return <RewardListContainer />;
      case 'SeedProducts': return <ProductContainer />;
      case 'threadDetail': return <ThreadDetailContainer {...props} />;
      case 'ToolsProducts': return <ProductContainer />;
      case 'updatePassword': return <UpdatePasswordContainer />;
      case 'groupDetail': return <GroupDetailContainer />;
      case 'comingSoon': return <ComingSoonPage />;
      case 'plantingGuidanceDetail': return <PlantingGuidanceDetailContainer {...props} />;
    }
  }

  render() {
    let firstRouteKey = this.props.navigation.routes[0].key;
    let showNavBar = ['initial', 'walkthrough'].indexOf(firstRouteKey) === -1;
    let {codePush} = this.props;
    return (
      <View style={{flex: 1}}>
        <Drawer
          open={this.props.isDrawerOpen}
          type="overlay"
          content={<DrawerComponent {...this.props} />}
          panCloseMask={0.5}
          tapToClose={true}
          openDrawerOffset={0.3}
          closedDrawerOffset={-3}
          tweenDuration={200}
          onCloseStart={this.props.onPressClose}
          tweenHandler={(ratio) => ({
            mainOverlay: {backgroundColor: `rgba(0, 0, 0, ${ratio / 2})`},
          })}
        >
          <StatusBar
            backgroundColor="#139E13"
          />
          <NavigationCardStack
            direction="horizontal"
            navigationState={this.props.navigation}
            onNavigateBack={this._handleBackAction}
            renderScene={this._renderScene}
          />
          {showNavBar ?
            <NavBarContainer isTextShown={false} selectedIndex={'homepage'} />
            : null
          }
          <NewItemModalContainer />
          {
            (codePush.isAppUpdating) ?
              <DownloadIndicator isShowing={codePush.isAppUpdating} downloadProgress={codePush.downloadProgress} totalDownloadSize={codePush.totalDownloadSize} /> : null
          }
        </Drawer>
      </View>
    );
  }
}

function mapStateToProps(state) {
  let {navigation, isDrawerOpen, codePush, guestMode} = state;
  return {
    navigation,
    isDrawerOpen,
    codePush,
    guestMode,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    popRoute: () => {
      dispatch({
        type: 'POP_ROUTE',
      });
    },
    backToHome: () => {
      dispatch({
        type: 'RESET_ROUTE',
        key: 'homepage',
      });
    },
    showWalkthrough: () => {
      dispatch({
        type: 'RESET_ROUTE',
        key: 'walkthrough',
      });
    },
    onPressClose() {
      dispatch({
        type: 'CLOSE_DRAWER',
      });
    },
    onPressLogout: () => {
      dispatch({type: 'LOGOUT'});
      dispatch({type: 'RESET_ROUTE', key: 'initial'});
      dispatch({
        type: 'CLOSE_DRAWER',
      });
    },
    onPressContactUs: () => {
      dispatch({
        type: 'PUSH_ROUTE', key: 'contactUs',
      });
      dispatch({
        type: 'CLOSE_DRAWER',
      });
    },
    onPressProfile() {
      dispatch({
        type: 'PUSH_ROUTE',
        key: 'editProfile',
      });
      dispatch({
        type: 'CLOSE_DRAWER',
      });
    },
    onPressSearch() {
    },
    autoLogin(userAuth) {
      dispatch({type: 'LOGIN', userAuth});
    },
  };
}

export default connect(mapStateToProps, mapDispatchToProps)(RootNavigation);
